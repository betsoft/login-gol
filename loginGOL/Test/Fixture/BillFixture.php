<?php
/**
 * BillFixture
 *
 */
class BillFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'ID' => array('type' => 'integer', 'null' => false, 'default' => null),
		'ID2' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Oggetto' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Quantità' => array('type' => 'float', 'null' => false, 'default' => null),
		'Prezzo' => array('type' => 'float', 'null' => false, 'default' => null),
		'Iva' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'ID' => 1,
			'ID2' => 'Lorem ipsum dolor sit amet',
			'Oggetto' => 'Lorem ipsum dolor sit amet',
			'Quantità' => 1,
			'Prezzo' => 1,
			'Iva' => 1
		),
	);

}
