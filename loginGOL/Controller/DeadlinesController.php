<?php
App::uses('AppController', 'Controller');

class DeadlinesController extends AppController
{
    public function indexscadatt()
    {
        $this->loadModel('Utilities');

        $conditionsArray = ['Deadline.state' => ATTIVO, 'Bill.state' => ATTIVO, 'Bill.tipologia in ' => [1, 3, 7], 'Deadline.company_id' => MYCOMPANY];

        $conditionsArray['date >='] = date('Y-01-01');
        $conditionsArray['date <='] = date('Y-12-31');
        $conditionsArray['deadline >='] = date('Y-01-01');
        $conditionsArray['deadline <='] = date('Y-12-31');
        $this->set('date1', date('01-01-Y'));
        $this->set('date2', date('31-12-Y'));
        $this->set('date3', date('01-01-Y'));
        $this->set('date4', date('31-12-Y'));

        $clientConditionsArray = [];
        $paymentConditionArray = [];

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
        {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        }

        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            $filterableFields = ['Bill__number', '#htmlElements[0]', '#htmlElements[1]', 'Client__ragionesociale',null, null, null, null, 'Payment__metodo', '#htmlElements[2]', null];
            $sortableFields = [['Bill.numero_fattura', 'N. fattura'], ['Bill.date', 'Data fattura'], ['deadline', 'Scadenza'], ['Client.ragionesociale', 'Cliente'], [null, 'Totale scadenza'], [null, 'Pagato'],[null, 'Da pagare'], [null, 'Data ultimo pagamento'], [null, 'Metodo di pagamento'], [null, 'Stato scadenza'], ['#actions']];
        } else {
            $filterableFields = ['Bill__number', '#htmlElements[0]', '#htmlElements[1]', 'Client__ragionesociale', null, null, null, 'Payment__metodo', '#htmlElements[2]', null];
            $sortableFields = [['Bill.numero_fattura', 'N. fattura'], ['Bill.date', 'Data fattura'], ['deadline', 'Scadenza'], ['Client.ragionesociale', 'Cliente'], [null, 'Totale scadenza'], [null, 'Pagato'], [null, 'Data ultimo pagamento'], [null, 'Metodo di pagamento'], [null, 'Stato scadenza'], ['#actions']];
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            $conditionsArray = ['Deadline.state' => ATTIVO, 'Bill.state' => ATTIVO, 'Bill.tipologia in' => [1, 3, 7], 'Deadline.company_id' => MYCOMPANY];

            if (isset($this->request->data['filters']['tipologia']) && $this->request->data['filters']['tipologia'] != '')
            {
                switch ($this->request->data['filters']['tipologia'])
                {
                    case 'A':
                        $this->set('saldacconto', 'A');
                        break;
                    case 'D':
                        $this->set('saldacconto', 'D');
                        break;
                    case 'S':
                        $this->set('saldacconto', 'S');
                        break;
                    case 'I':
                        $this->set('saldacconto', 'I');
                        break;
                    case 'P':
                        $this->set('saldacconto', 'P');
                        break;
                    case 'R':
                        $this->set('saldacconto', 'R');
                        break;
                }
            }

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['Bill.date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
                $this->set('date1', date('d-m-Y', strtotime($this->request->data['filters']['date1'])));
            }

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['Bill.date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
                $this->set('date2', date('d-m-Y', strtotime($this->request->data['filters']['date2'])));
            }

            if (isset($this->request->data['filters']['date3']) && $this->request->data['filters']['date3'] != '')
            {
                $conditionsArray['Deadline.deadline >='] = date('Y-m-d', strtotime($this->request->data['filters']['date3']));
                $this->set('date3', date('d-m-Y', strtotime($this->request->data['filters']['date3'])));
            }

            if (isset($this->request->data['filters']['date4']) && $this->request->data['filters']['date4'] != '')
            {
                $conditionsArray['Deadline.deadline <='] = date('Y-m-d', strtotime($this->request->data['filters']['date4']));
                $this->set('date4', date('d-m-Y', strtotime($this->request->data['filters']['date4'])));
            }

            if(isset($this->request->data['filters']['Client__ragionesociale']))
                //$clientConditionsArray['Client.ragionesociale LIKE'] = "%".$this->request->data['filters']['Client__ragionesociale']."%";
                $conditionsArray['Bill.client_name LIKE'] = "%".$this->request->data['filters']['Client__ragionesociale']."%";
            if(isset($this->request->data['filters']['Bill__number']))
                $conditionsArray['Bill.numero_fattura LIKE'] = "%".$this->request->data['filters']['Bill__number']."%";

            if(isset($this->request->data['filters']['Payment__metodo']))
                $paymentConditionArray['Payment.metodo LIKE'] = "%".$this->request->data['filters']['Payment__metodo']."%";

            if(isset($this->request->data['filters']['Payment__metodo']))
                $paymentConditionArray['Payment.metodo LIKE'] = "%".$this->request->data['filters']['Payment__metodo']."%";

            /* Start fix per metodo di pagamento */
            $this->loadModel('Payment');
            $paymentMethods = $this->Payment->find('list',['fields'=>['id','metodo'] ,'conditions'=>$paymentConditionArray]);
            $arrayOfPayment=[];

            foreach($paymentMethods as $key => $payment)
                $arrayOfPayment[] = $key;

            $conditionsArray['Bill.payment_id IN'] = $arrayOfPayment;
            /* End fix per metodo di pagamento */

            $deadlines = $this->Deadline->Find('all', ['contain' => ['Bill' => ['Client' => ['conditions' => $clientConditionsArray], 'Payment' ], 'Deadlinepayment' => ['conditions' => ['state' => 1]]], 'conditions' => $conditionsArray, 'order' => 'deadline desc', 'whitelist' => ['Client.ragionesociale', 'Payment.metodo']]);

            foreach ($deadlines as $key => $deadline)
            {
                $totalPaymentDeadline = 0;
                if (count($deadline['Deadlinepayment']) > 0)
                {
                    foreach ($deadline['Deadlinepayment'] as $payment)
                    {
                        $totalPaymentDeadline += $payment['payment_amount'];
                        $lastPayment = $payment['payment_date'];
                    }
                }

                switch ($this->request->data['filters']['tipologia'])
                {
                    case 'A':
                        if ((($totalPaymentDeadline < ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat'])) && $totalPaymentDeadline > 0) && $deadline['Deadline']['unpaid'] == 0) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'D':
                        if (($deadline['Deadline']['unpaid'] == 0) && $deadline['Deadlinepayment'] == null && $deadline['Bill']['ribaemitted'] == 0) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'S':
                        if ($totalPaymentDeadline == $deadline['Deadline']['amount'] + $deadline['Deadline']['vat']) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'I':
                        if ($deadline['Deadline']['unpaid'] == 1 && $deadline['Deadlinepayment'] == null) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'P':
                        if ((($totalPaymentDeadline < ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat'])) && $totalPaymentDeadline > 0) && $deadline['Deadline']['unpaid'] == 1) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'R':
                        if ($deadline['Deadline']['unpaid'] == 0 && $deadline['Deadlinepayment'] == null && $deadline['Bill']['ribaemitted'] == 1) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                }
            }

            if (isset($arrayDeadline))
            {
                if ($arrayDeadline != null)
                    $conditionsArray['Deadline.id IN'] = $arrayDeadline;
            }

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        $this->loadmodel('Deadline');

        $dds = $this->Deadline->find('all', [
            'contain' => ['Bill' => ['Client' /*=> ['conditions'=>$clientConditionsArray]*/, 'Payment'], 'Deadlinepayment'],
            'conditions' => $conditionsArray,
            'order' => 'deadline desc',
            'field' => 'Client.ragionesociale',
            'whitelist' => ['Client.ragionesociale', 'Payment.metodo'],
        ]);

        $arrayOfResult[] = '-1';
        foreach ($dds as $key => $dd)
        {
            $passed = true;

            /*if (isset($this->request->data['filters']))
            {
                if ($this->request->data['filters']['Client__ragionesociale'] != "")
                {
                    if (strpos(strtoupper($dd['Bill']['Client']['ragionesociale']), strtoupper($this->request->data['filters']['Client__ragionesociale'])) !== false)
                    {
                    }
                    else
                    {
                        $passed = false;
                    }
                }

                if ($this->request->data['filters']['Bill__number'] != "")
                {
                    if (strpos(strtoupper($dd['Bill']['numero_fattura']), strtoupper($this->request->data['filters']['Bill__number'])) !== false)
                    {
                    }
                    else
                    {
                        $passed = false;
                    }
                }
            }*/

            if($passed == true)
                $arrayOfResult[] = $dd['Deadline']['id'];
        }

        $this->loadModel('Deadline');
        $this->set('unbalanced' , $this->Deadline->getUnbalancedBill());

        if($arrayOfResult != null)
        {
            $this->paginate = [
                'contain' => ['Bill' => ['Client', 'Payment'], 'Deadlinepayment'],
                'conditions' => ['Deadline.id IN' => $arrayOfResult],
                'order' => 'deadline desc',
                'limit' => 500,
                'field' => 'Client.ragionesociale',
                'whitelist' => ['Client.ragionesociale', 'Payment.metodo'],
            ];
        }
        else
        {
            $this->paginate = [
                'contain' => ['Bill' => ['Client', 'Payment'], 'Deadlinepayment'],
                'conditions' => $conditionsArray,
                'order' => 'deadline desc',
                'limit' => 500,
                'field' => 'Client.ragionesociale',
                'whitelist' => ['Client.ragionesociale', 'Payment.metodo'],
            ];
        }

        $this->loadModel('Deadline');
        $this->set('unbalanced', $this->Deadline->getUnbalancedBill());
        $this->set('deadlines', $this->paginate());
        $this->set('filterableFields', $filterableFields);
        $this->set('sortableFields', $sortableFields);
        $this->set('tipologia', ['A' => 'Accontata', 'D' => 'Da pagare', 'S' => 'Saldata', 'I' => 'Insoluto', 'P' => 'Insoluto parziale', 'R' => 'In pagamento (RI.BA.)']);

    }

    public function indexscadpass()
    {
        $this->loadModel('Utilities');
        $this->loadmodel('Deadline');

        $conditionsArray = ['Deadline.state' => ATTIVO, 'Bill.state' => ATTIVO, 'Bill.tipologia' => 2, 'Deadline.company_id' => MYCOMPANY];

        $conditionsArray['date >='] = date('Y-01-01');
        $conditionsArray['date <='] = date('Y-12-31');
        $conditionsArray['deadline >='] = date('Y-01-01');
        $conditionsArray['deadline <='] = date('Y-12-31');
        $this->set('date1', date('01-01-Y'));
        $this->set('date2', date('31-12-Y'));
        $this->set('date3', date('01-01-Y'));
        $this->set('date4', date('31-12-Y'));

        $filterableFields = ['Bill__number', '#htmlElements[0]', '#htmlElements[1]', 'Supplier__name', null, null, null, null, '#htmlElements[2]', null];
        $sortableFields = [['Bill.numero_fattura', 'N. fattura'], ['Bill.date', 'Data fattura'], ['deadline', 'Scadenza'], ['Supplier__name', 'Fornitore'], [null, 'Totale scadenza'], [null, 'Pagato'], [null, 'Metodo di pagamento'], [null, 'Data ultimo pagamento'], [null, 'Stato scadenza'], ['#actions']];

        $supplierConditionsArray = [];
        $paymentConditionArray = [];

        $automaticFilter = $this->Session->read('arrayOfFilters');

        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);

            $conditionsArray = ['Deadline.state'=>ATTIVO,'Bill.state'=>ATTIVO,'Bill.tipologia'=>2 ,'Deadline.company_id' => MYCOMPANY];

            if(isset($this->request->data['filters']['tipologia']) && $this->request->data['filters']['tipologia'] != '')
            {
                switch($this->request->data['filters']['tipologia'])
                {
                    case 'A': $this->set('saldacconto','A'); break;
                    case 'D': $this->set('saldacconto','D'); break;
                    case 'S': $this->set('saldacconto','S'); break;
                    case 'I': $this->set('saldacconto','I'); break;
                    case 'P': $this->set('saldacconto','P'); break;
                }
            }

            if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['date >='] = date('Y-m-d',strtotime($this->request->data['filters']['date1']));
                $this->set('date1', date('d-m-Y', strtotime($this->request->data['filters']['date1'])));
            }

            if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['date <='] =  date('Y-m-d',strtotime($this->request->data['filters']['date2'])) ;
                $this->set('date2', date('d-m-Y', strtotime($this->request->data['filters']['date2'])));
            }

            if(isset($this->request->data['filters']['date3']) && $this->request->data['filters']['date3'] != '')
            {
                $conditionsArray['deadline >='] = date('Y-m-d',strtotime($this->request->data['filters']['date3']));
                $this->set('date3', date('d-m-Y', strtotime($this->request->data['filters']['date3'])));
            }

            if(isset($this->request->data['filters']['date4']) && $this->request->data['filters']['date4'] != '')
            {
                $conditionsArray['deadline <='] =  date('Y-m-d',strtotime($this->request->data['filters']['date4'])) ;
                $this->set('date4', date('d-m-Y', strtotime($this->request->data['filters']['date4'])));
            }

            if(isset($this->request->data['filters']['Bill__number']))
                $conditionsArray['Bill.numero_fattura LIKE'] = "%".$this->request->data['filters']['Bill__number']."%";

            if(isset($this->request->data['filters']['Supplier__name']))
                $supplierConditionsArray['Supplier.name LIKE'] = "%".$this->request->data['filters']['Supplier__name']."%";

            if(isset($this->request->data['filters']['Payment__metodo']))
                $paymentConditionArray['Payment.metodo LIKE'] = "%".$this->request->data['filters']['Payment__metodo']."%";

            $deadlines = $this->Deadline->Find('all',['contain'=> ['Bill'=>['Supplier' => ['conditions' => $supplierConditionsArray],'Payment'=>['conditions'=>$paymentConditionArray]],
                'Deadlinepayment'=>['conditions'=>['state' => 1]]],'order'=>'deadline desc','conditions' => $conditionsArray, 'limit' => 1000,'whitelist' => ['Client.ragionesociale','Payment.metodo']]);

            $totalPaymentDeadline = 0;
            foreach ($deadlines as $key => $deadline)
            {
                if (count($deadline['Deadlinepayment']) > 0)
                {
                    foreach ($deadline['Deadlinepayment'] as $payment)
                    {
                        $totalPaymentDeadline += $payment['payment_amount'];
                        $lastPayment = $payment['payment_date'];
                    }
                }

                switch ($this->request->data['filters']['tipologia'])
                {
                    case 'A':
                        if ((($totalPaymentDeadline < ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat'])) && $totalPaymentDeadline > 0) && $deadline['Deadline']['unpaid'] == 0) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'D':
                        if (($deadline['Deadline']['unpaid'] == 0) && $deadline['Deadlinepayment'] == null) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'S':
                        if ($deadline['Deadline']['unpaid'] == 1 && $deadline['Deadlinepayment'] != null)
                        {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'I':
                        if ($deadline['Deadline']['unpaid'] == 1 && $deadline['Deadlinepayment'] == null) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                    case 'P':
                        if ((($totalPaymentDeadline < ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat'])) && $totalPaymentDeadline > 0) && $deadline['Deadline']['unpaid'] == 1) {
                            $arrayDeadline[] = $deadline['Deadline']['id'];
                        }
                        break;
                }
            }

            if (isset($arrayDeadline))
            {
                if ($arrayDeadline != null)
                {
                    $conditionsArray['Deadline.id IN'] = $arrayDeadline;
                }
            }
        }

        $dds = $this->Deadline->find('all', [
            'contain'=>['Bill'=>['Supplier','Payment'], 'Deadlinepayment'],
            'conditions' => $conditionsArray, // Fatture , fatture elettroniche
            'order'=>'deadline desc',
            'field'=>'Client.ragionesociale',
            'whitelist' => ['Client.ragionesociale','Payment.metodo'],
        ]);

        $arrayOfResult[] = '-1';

        foreach($dds as $key => $dd)
        {
            $passed = true;

            if(isset($this->request->data['filters']))
            {
                if ($this->request->data['filters']['Supplier__name'] != "")
                {
                    if(isset($dd['Bill']['Supplier']['name']))
                    {
                        if (strpos(strtoupper($dd['Bill']['Supplier']['name']), strtoupper($this->request->data['filters']['Supplier__name'])) !== false)
                        {
                        }
                        else
                        {
                            $passed = false;
                        }
                    }
                    else
                    {
                        $passed =false;
                    }
                }

                if ($this->request->data['filters']['Bill__number'] != "")
                {
                    if (strpos(strtoupper($dd['Bill']['numero_fattura']), strtoupper($this->request->data['filters']['Bill__number'])) !== false)
                    {
                    }
                    else
                    {
                        $passed = false;
                    }
                }
            }

            if($passed == true)
            {
                $arrayOfResult[] = $dd['Deadline']['id'];
            }
        }

        if($arrayOfResult != null)
        {
            $this->paginate = [
                'contain' => ['Bill' => ['Supplier', 'Payment'], 'Deadlinepayment'],
                'conditions' => ['Deadline.id IN' => $arrayOfResult],
                'order' => 'deadline desc',
                'limit' => 500,
                'field' => 'Client.ragionesociale',
                'whitelist' => ['Client.ragionesociale', 'Payment.metodo'],
            ];
        }
        else
        {
            $this->paginate = [
                'contain' => ['Bill' => ['Supplier', 'Payment'], 'Deadlinepayment'],
                'conditions' => $conditionsArray,
                'order' => 'deadline desc',
                'limit' => 500,
                'field' => 'Client.ragionesociale',
                'whitelist' => ['Client.ragionesociale', 'Payment.metodo'],
            ];
        }

        $this->set('deadlines', $this->paginate());

        $this->set('sortableFields',$sortableFields);
        $this->set('filterableFields',$filterableFields);
        $this->set('tipologia',['A'=>'Accontata', 'D'=>'Da pagare', 'S'=>'Saldata', 'I'=>'Insoluto', 'P'=>'Insoluto parziale']);
        $this->set('paymentmethodscodes', $this->Utilities->getEinvoicePaymentmethodCode());
    }

    public function addPayment()
    {
        $this->autoRender =false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Deadlinepayment']);
        $newPaymentDeadline = $this->Deadlinepayment->create();
        $newPaymentDeadline['Deadlinepayment']['payment_date'] = date("Y-m-d",strtotime($_POST['payment_date']));
        $newPaymentDeadline['Deadlinepayment']['payment_amount'] = $_POST['payment_amount'];
        $newPaymentDeadline['Deadlinepayment']['deadline_id'] = $_POST['payment_deadline_id'];
        $newPaymentDeadline['Deadlinepayment']['note'] = $_POST['payment_note'];
        $this->Deadlinepayment->save($newPaymentDeadline);
    }

    public function showPayment()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadlinepayment']);
        $deadlinePayments = $this->Deadlinepayment->find('all', ['conditions' => ['Deadlinepayment.deadline_id' => $_POST['payment_deadline_id'], 'Deadlinepayment.state' => ATTIVO]]);
        $arrayPayment = [];
        foreach ($deadlinePayments as $payment) {
            $payment['Deadlinepayment']['payment_date'] = date('d-m-Y', strtotime($payment['Deadlinepayment']['payment_date']));
            $arrayPayment[] = $payment['Deadlinepayment'];
        }
        print_r(json_encode($arrayPayment));
        die;
    }

    public function setDeadlineUnpaidTrue()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadline']);
        $this->Deadline->setUnpaid($_POST['payment_deadline_id'], true);
    }

    public function setDeadlineUnpaidFalse()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadline']);
        $this->Deadline->setUnpaid($_POST['payment_deadline_id'], false);
    }

    public function deletePayment()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadlinepayment']);
        $this->Deadlinepayment->deletePayment($_POST['paymentid']);
    }

    // Controlla il numero di pagamenti di una fattura
    public function hasPayments()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadline']);
        return $this->Deadline->getCountPayments($_POST['deadlineId']);
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Deadline']);
        $this->Deadline->id = $id;
        if (!$this->Deadline->exists()) {
            throw new NotFoundException(__('Scadenza non valida'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Deadline']['deadline'] = date("Y-m-d", strtotime($this->request->data['Deadline']['deadline']));
            if ($this->Deadline->save($this->request->data['Deadline'])) {
                $this->Session->setFlash(__('Scadenza modificata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'indexscadatt']);
            } else {
                $this->Session->setFlash(__('La scadenza non è stata salvata, riprovare'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Deadline->read(null, $id);

        }
    }
}
