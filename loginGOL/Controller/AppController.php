<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');
App::uses('ConnectionManager', 'Model');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

//date_default_timezone_set('America/Los_Angeles');

class AppController extends Controller
{
    public $helpers = ['Js' => ['Jquery'], 'Graphic'];

    public $components =
    [
        'Session',
        'Auth' => [
            'loginRedirect' => ['controller' => 'bills', 'action' => 'index'],
            'logoutRedirect' => ['controller' => 'users', 'action' => 'login'],
        ]
    ];

    function beforeRender()
    {
    }

    public function beforeFilter()
    {
        /*
        if($this->Auth->user('id') != null)
        {
            $this->loadModel('Utilities');
            $this->Utilities->updateLastLoginDate($this->Auth->user('id'));
        }
        */

        $this->set('autenticato', $this->Auth->user('id'));

        if ($this->name == 'CakeError')
        {
            $this->layout = 'errorColored';
            /** LOGO */
            $this->loadModel('Setting');

            if (isset($_SESSION['MYCOMPANY']))
            {
                $LOGOMENU = $this->Setting->find('all', ['conditions' => ['Setting.company_id' => $_SESSION['MYCOMPANY']]]);
            }
            else
            {
                $LOGOMENU = $this->Setting->find('all', ['conditions' => ['Setting.company_id' => $this->Auth->User('company_id')]]);
            }

            $this->set('LOGOMENU', $LOGOMENU);
        }

        // Per megaform
        $this->set('AJAX', $this->request->is('ajax'));

        if ($this->request->is('ajax'))
        {
            $this->layout = 'ajaxVoidLayout';
        }

        $this->Auth->allow('login');
        $this->Auth->authError = __("Prima di accedere al software e' necessario autenticarsi", 'custom-flash');

        $this->loadModel('Permissions'); // Carico model dei permessi

        $dataSource = ConnectionManager::getDataSource('default');

        // Definiso il main DB
        !defined(('MAINDB')) ? define("MAINDB", ConnectionManager::getDataSource('default')->config['database']) : null;
        !defined(('MAINPASSWORD')) ? define("MAINPASSWORD", ConnectionManager::getDataSource('default')->config['password']) : null;
        !defined(('MAINUSER')) ? define("MAINUSER", ConnectionManager::getDataSource('default')->config['login']) : null;

        if ($this->Auth->User('usercompany')) {

            $this->loadModel('Users');
            $user = $this->Users->find('first', ['conditions' => ['id' => $this->Auth->User('id')]]);

            $settings = [
                'datasource' => 'Database/Mysql',
                'persistent' => false,
                'host' => 'localhost',
                'login' => $user['Users']['dbuser'],
                'password' => $user['Users']['dbpassword'],
                'database' => $user['Users']['dbname'],
                'prefix' => '',
                'encoding' => 'utf8',
            ];

            try {
                ConnectionManager::getDataSource('default')->disconnect();
                ConnectionManager::drop('default');
                ConnectionManager::create('default', $settings);
                ConnectionManager::getDataSource('default')->connect();
                $db = ConnectionManager::getDataSource('default');
                if ($this->Auth->User('usercompany') != null) {
                    $_SESSION['Auth']['User']['company_id'] = $this->Auth->User('usercompany');
                }
            } catch (MissingDatabaseException $e) {
                $this->Session->setFlash($e->getMessage());
            }
        }

        if (null !== $this->Auth->User('company_id'))
        {
            $this->loadModel('Configurations'); // Carico model dei permessi
            $this->loadModel('Setting');

            // Definisco la variabile di sessione MYCOMPANY
            if (isset($_SESSION['MYCOMPANY']))
            {
                !defined(('MYCOMPANY')) ? define("MYCOMPANY", $_SESSION['MYCOMPANY']) : null;
            }
            else
            {
                !defined(('MYCOMPANY')) ? define("MYCOMPANY", $this->Auth->User('company_id')) : null;
                $_SESSION['MYCOMPANY'] = $this->Auth->User('company_id');
            }

            !defined(('ATTIVO')) ? define("ATTIVO", 1) : null;
            !defined(('APPVER')) ? define("APPVER", 'Gestionale-online 1.0.3') : null;
            !defined(('SCSV')) ? define("SCSV", ";") : null;

            $patterBasicLatin = "[A-Za-z0-9!\"#$%&'()*+,-./:;<=>?@[\]^_{}~ àèéìòù]+";
            !defined("PATTERNBASICLATIN") ? define("PATTERNBASICLATIN", $patterBasicLatin) : null;

            try
            {
                /** CORE **/
                // CONFIGURAZIONE
                if (!defined('ADMIN_CONFIGURATION'))
                {
                    define("ADMIN_CONFIGURATION", $this->Configurations->find('first', ['conditions' => ['description' => 'admin_configuration']])['Configurations']['settingvalue']);
                }

                // FATTURAZIONE
                !defined('RIBA_FLOW') ? define("RIBA_FLOW", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'riba_flow']])['Configurations']['settingvalue']) : null;

                !defined('BILLS_RECEIPT') ? define("BILLS_RECEIPT", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'bills_receipt']])['Configurations']['settingvalue']) : null;

                // BOLLE - Serve per definire se da fatture e bolle vengano creati in automatico in magazzino quelli mancanti
                if (!defined('DDT_BILL_FOR_PERIOD'))
                {
                    define("DDT_BILL_FOR_PERIOD", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'ddt_bill_for_period']])['Configurations']['settingvalue']);
                }

                // STAMPA BOLLE
                if (!defined('BILL_WITH_DDT_DETAIL_FIRST'))
                {
                    define("BILL_WITH_DDT_DETAIL_FIRST", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'bill_with_ddt_detail_first']])['Configurations']['settingvalue']);
                }

                if (!defined('BILL_WITH_DDT_DETAIL_EACH_ROW'))
                {
                    define("BILL_WITH_DDT_DETAIL_EACH_ROW", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'bill_with_ddt_detail_each_row']])['Configurations']['settingvalue']);
                }

                // MODULO MAGAZZINO
                !defined('ADVANCED_STORAGE_ENABLED') ? define("ADVANCED_STORAGE_ENABLED", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'advanced_storage_enabled']])['Configurations']['settingvalue']) : null;

                // MODULO IXFE
                !defined('MODULE_IXFE') ? define("MODULE_IXFE", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'module_ixfe']])['Configurations']['settingvalue']) : null;

                // ABILITAZIONE MULTIVALUTA
                !defined('MULTICURRENCY') ? define("MULTICURRENCY", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'multicurrency']])['Configurations']['settingvalue']) : null;

                // ABILITAZIONE PAYPAL
                !defined('PAYPAL') ? define("PAYPAL", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'paypal']])['Configurations']['settingvalue']) : null;

                /** PERSONALIZZAZIONI **/

                // FATTURAZIONE SCONTO ( Fascendini)
                !defined('FIXED_COST_PERCENTAGE') ? define("FIXED_COST_PERCENTAGE", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'fixed_cost_percentage_enabled']])['Configurations']['settingvalue']) : null;

                // PERSONALIZZAZIONI RICHIESTE ( BOFFI - VIOLA )
                !defined('AUTOMATICSELLPRICEINLOADGOOD') ? define("AUTOMATICSELLPRICEINLOADGOOD", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'automaticsellpriceinloadgood']])['Configurations']['settingvalue']) : null;

                // PERSONALIZZAZIONI NON CONFIGURABILI DA PANNELLO
                !defined('MULTICSV') ? define("MULTICSV", $this->Configurations->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'description' => 'multicsv']])['Configurations']['settingvalue']) : null;

                /* MODULO */

                // PRIMO MODULO CANTIERI
                !defined('MODULO_CANTIERI') ? define("MODULO_CANTIERI", $this->Configurations->find('first',['conditions'=>['company_id'=>MYCOMPANY, 'description' =>  'constructionsites']])['Configurations']['settingvalue']) : null;

                // SECONDO MODULO CONTABILITA'
                !defined('MODULO_CONTI') ? define("MODULO_CONTI", $this->Configurations->find('first',['conditions'=>['company_id'=>MYCOMPANY, 'description' =>  'accounting']])['Configurations']['settingvalue']) : null;

            }
            catch (Exception $exc)
            {
                $this->Auth->logout();
            }
        }

        /* Se siamo autenticati controlliamo qual'è la vista da vedere nei settaggi */
        if (isset($_SESSION['Auth']['User']['company_id']))
        {
            $conditionsArray = [
                'controller' => $this->request->params['controller'],
                'action' => $this->request->params['action'],
                'company_id' => $_SESSION['Auth']['User']['company_id'],
                'role_id' => $_SESSION['Auth']['User']['group_id']
            ];

            $permissionCount = $this->Permissions->find('count', ['conditions' => $conditionsArray]);

            if ($permissionCount == 0)
            {
                throw new NotFoundException('Stai cercando di accedere ad una pagina inesistente.');
            }
        }
        else
        {
            $conditionsArray = [
                'plugin' => $this->request->params['plugin'],
                'controller' => $this->request->params['controller'],
                'action' => $this->request->params['action'],
            ];
        }

        if (null !== $this->Auth->User('company_id'))
        {
            !isset($_SESSION['MYCOMPANY']) ? $_SESSION['MYCOMPANY'] == $this->Auth->User('company_id') : null;

            /** FUNZIONE MENU **/
            $this->loadModel('Menus');
            $MENU = $this->Menus->find('all', ['conditions' => ['company_id' => $_SESSION['MYCOMPANY'], 'menu_level ' => 0, 'state' => 0,'role_id'=>$_SESSION['Auth']['User']['group_id']], 'order' => 'sort_index']);
            $menu = [];

            foreach ($MENU as $zeroLevel)
            {
                $zeroLevel = $zeroLevel['Menus'];

                $menu[$zeroLevel['menu_voice']]['plugin_name'] = null;
                $menu[$zeroLevel['menu_voice']]['controller'] = $zeroLevel['controller'];
                $menu[$zeroLevel['menu_voice']]['action'] = $zeroLevel['action'];

                $firstLevels = $this->Menus->find('all', ['order' => ['sort_index'], 'conditions' => ['company_id' => $_SESSION['MYCOMPANY'], 'menu_level' => 1, 'parent' => $zeroLevel['menu_voice'], 'state' => 0,$_SESSION['Auth']['User']['group_id']]]);

                foreach ($firstLevels as $firstLevel)
                {
                    $firstLevel = $firstLevel['Menus'];

                    $plugin = null;
                    $controller = $firstLevel['controller'];
                    $action = $firstLevel['action'];

                    $menu[$zeroLevel['menu_voice']]['subs'][$firstLevel['menu_voice']]['name'] = $firstLevel['menu_voice'];
                    $menu[$zeroLevel['menu_voice']]['subs'][$firstLevel['menu_voice']]['plugin_name'] = $plugin;
                    $menu[$zeroLevel['menu_voice']]['subs'][$firstLevel['menu_voice']]['controller'] = $controller;
                    $menu[$zeroLevel['menu_voice']]['subs'][$firstLevel['menu_voice']]['action'] = $action;
                }
            }
        }

        // se sono loggato
        if (null !== $this->Auth->User('company_id'))
        {
            $this->set('PLUGINS_DB_MENU', $menu);

            /** LOGO */
            $this->loadModel('Setting');

            $LOGOMENU = $this->Setting->find('all', ['conditions' => ['company_id' => $_SESSION['MYCOMPANY']]]);
            $_SESSION['companyName'] = $LOGOMENU[0]['Setting']['name'];
            $this->set('LOGOMENU', $LOGOMENU);

            // Passo la lista delle settings
            $settingsList = $this->Setting->find('all', ['order' => ['Setting.id' => 'asc'], 'conditions' => ['Setting.state' => ATTIVO]]);
            $this->set('settingsList', $settingsList);

            // Passo il nome dell utente
            if($_SESSION['Auth']['User']['group_id'] == 20)
            {
                $this->loadModel('User');
                $currentUser = $this->User->find('first',['conditions'=>['User.company_id' => $_SESSION['MYCOMPANY'], 'username'=>$_SESSION['Auth']['User']['username']]]);

                $this->loadModel('Technician');
                $currentTecnichian = $this->Technician->find('first', ['conditions' => ['company_id' => $_SESSION['MYCOMPANY'], 'user_id' => $currentUser['User']['id']]]);

                $this->set('tennichianname', $currentTecnichian['Technician']['name'].' '. $currentTecnichian['Technician']['surname']);
            }

            /** PARAMETRI DI CONFIGURAZIONE **/
            $this->loadModel('Configurations');
            $myConfigurationArray = ['company_id' => $_SESSION['MYCOMPANY']];

            /** DEFINISCO LE ICONE **/
            $constant = $this->webroot;

            $this->Set('iconaElimina', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-elimina.svg" alt="Elimina"  class="golIcon"  title = "Elimina">');
            $this->Set('iconaEliminaOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-elimina-off.svg" alt="Elimina"  class="golIcon"  title = "Fattura non eliminabile">');
            $this->Set('iconaModifica', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-modifica.svg" alt="Modifica"  class="golIcon"  title = "Modifica">');
            $this->Set('iconaModificaOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica"  class="golIcon"  title = "Modifica">');
            $this->Set('iconaPdf', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-pdf-scarica.svg" alt="pdf"  class="golIcon"  title = "Visualizza pdf">');
            $this->Set('iconaPdfOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-pdf-off.svg" alt="pdf"  class="golIcon"  title = "Visualizza pdf">');
            $this->Set('iconaPlus', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-carico.svg" alt="Aggiungi"  class="golIcon"  title = "Aggiungi">');
            $this->Set('iconaPlusOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-carico-off.svg" alt="Aggiungi"  class="golIcon"  title = "Aggiungi">');
            $this->Set('iconaMinus', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-scarico.svg" alt="Rimuovi"  class="golIcon"  title = "Rimuovi">');
            $this->Set('iconaMinusOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-scarico-off.svg" alt="Rimuovi"  class="golIcon"  title = "Rimuovi">');
            $this->set('iconaDettaglioMagazzino', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-dettaglio.svg" alt="Dettaglio"  class="golIcon"  title = "Dettaglio magazzino">');
            $this->set('iconaDettaglioMagazzinoOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-dettaglio-off.svg" alt="Dettaglio"  class="golIcon"  title = "Dettaglio magazzino">');
            $this->set('iconaVarianti', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-varianti.svg" alt="Gestione varianti"  class="golIcon"  title = "Gestione varianti">');
            $this->set('iconaVariantiOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-varianti-off.svg" alt="Gestione varianti"  class="golIcon"  title = "Gestione varianti">');
            $this->Set('iconaAggiungiVariante', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-carico.svg" alt="Aggiungi"  class="golIcon"  title = "Aggiungi">');
            $this->Set('iconaAggiungiVarianteOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-magazzino-carico-off.svg" alt="Aggiungi"  class="golIcon"  title = "Aggiungi">');
            $this->set('iconaLisitini', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-listino.svg" alt="Listino"  class="golIcon"  title = "Listino">');
            $this->set('iconaLisitiniOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-listino-off.svg" alt="Listino"  class="golIcon"  title = "Listino">');
            $this->Set('iconaFatturaElettronica', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-elettronica-genera.svg" alt="Genera Fattura Elettronica"  class="golIcon"  title = "Genera Fattura Elettronica">');
            $this->set('iconaDestini', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-destini.svg" alt="Destini diversi"  class="golIcon"  title = "Destini diversi">');
            $this->set('iconaDestiniOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-destini-off.svg" alt="Destini diversi"  class="golIcon"  title = "Destini diversi">');
            $this->set('iconaGeneraFattura', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera.svg" alt="Genera fattura"  class="golIcon"  title = "Genera fattura">');
            $this->set('iconaGeneraNotaCredito', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera.svg" alt="Genera nota di credito"  class="golIcon"  title = "Genera nota di credito">');
            $this->set('iconaGeneraFatturaOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera-off.svg" alt="Genera fattura"  class="golIcon"  title = "Genera fattura">');
            $this->set('iconaGeneraFatturaAcquisto', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera.svg" alt="Genera fattura d\'acquisto"  class="golIcon"  title = "Genera fattura d\'acquisto">');
            $this->set('iconaGeneraFatturaAcquistoOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera.svg" alt="Genera fattura d\'acquisto"  class="golIcon"  title = "Genera fattura d\'acquisto">');
            $this->set('iconaGeneraEntrataMerce', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera.svg" alt="Genera entrata merce"  class="golIcon"  title = "Genera entrata merce">');
            $this->set('iconaGeneraEntrataMerceOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-fattura-genera-off.svg" alt="Entrata merce già creata"  class="golIcon"  title = "Etrata merce già creata">');
            $this->set('iconaMailInviata', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-inviata.svg" alt="mail inviata"  class="golIcon"  title = "Email inviata">');
            $this->set('iconaMailInviataOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-inviata-off.svg" alt="mail inviata"  class="golIcon"  title = "Email inviata">');
            $this->set('iconaMailNonInviata', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-non-inviata.svg" alt="mail non inviata"  class="golIcon"  title = "Email non inviata">');
            $this->set('iconaMailNonInviataOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-non-inviata-off.svg" alt="mail non inviata"  class="golIcon"  title = "Email non inviata">');
            $this->set('iconaMailLetta', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-letta.svg" alt="mail inviata"  class="golIcon"  title = "Email letta">');
            $this->set('iconaMailLettaOff', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-mail-letta-off.svg" alt="mail inviata"  class="golIcon"  title = "Email letta">');
            $this->set('iconaQr', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-qrcode.svg" alt="mail inviata"  class="golIcon"  title = "Scarica QrCode">');

            $this->set('ixfeAttesa', 'gestionaleonlineicon/gestionale-online.net-ixfe-attesa.svg');
            $this->set('ixfeWarning', 'gestionaleonlineicon/gestionale-online.net-ixfe-warning.svg');
            $this->set('ixfeOk', 'gestionaleonlineicon/gestionale-online.net-ixfe-ok.svg');
            $this->set('ixfeOkBlue', 'gestionaleonlineicon/gestionale-online.net-ixfe-okBlue.svg');
            $this->set('ixfeErrore', 'gestionaleonlineicon/gestionale-online.net-ixfe-errore.svg');

            $this->set('ixfeReload', 'gestionaleonlineicon/gestionale-online.net-reload.svg');
            $this->set('ixfeReloadBlack', 'gestionaleonlineicon/gestionale-online.net-reloadBlack.svg');
            $this->set('ixfeReloadBlue', 'gestionaleonlineicon/gestionale-online.net-reloadBlue.svg');

            $this->Set('iconaExcelCsvComma', '<img src="' . $constant . 'img/gestionaleonlineicon/gestionale-online.net-excel.svg" alt="Genera excel" class="golIcon performOnSelectedOnly createExcelcomma"  title = "Genera excel con separatore modificato" style="color:#577400;font-size:26px;width:40px;height:40px;cursor:pointer;float:right;">');
            $this->Set('iconaExcel', '<img src="' . $constant . 'img//gestionaleonlineicon/gestionale-online.net-excel.svg" alt="Genera excel" class="golIcon performOnSelectedOnly createExcel"  title = "Genera excel" style="color:#577400;font-size:26px;width:40px;height:40px;cursor:pointer;float:right;">');
        }
    }
}
