<?php
App::uses('AppController', 'Controller');

class XmlsController extends AppController 
{
    public function createNewEbillFromXml()
    {
        $this->loadModel('Xml');
        $this->loadModel('Utilities');
        $this->autoRender = false;
        
        $xmlStriped = $this->Utilities->stripP7MData($_POST["xmlEbill"]);
	    $xmlStriped = $this->Utilities->sanitizeXML($xmlStriped);
        if($xmlStriped == '')
	    {
	        // Potrebbero essere state inviate in base64
	        $xmlStriped = $this->Utilities->stripP7MData(base64_decode($response));
	        $xmlStriped = $this->Utilities->sanitizeXML($xmlStriped);
	    }
		
		// Fix per cdata 
        $xmlStriped = str_replace("<![CDATA[","",$xmlStriped);
        $xmlStriped = str_replace("]]>","",$xmlStriped);
        
        $this->Xml->createNewEbillFromXml($xmlStriped);        
    }        
}
