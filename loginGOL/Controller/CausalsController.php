<?php
App::uses('AppController', 'Controller');

class CausalsController extends AppController {


	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Graphicsutilities']);
		$conditionsArray = ['Causal.company_id' => MYCOMPANY, 'Causal.state'=>ATTIVO];
		
		
		$filterableFields = ['name',null,null];
		$sortableFields = [['name','Causale'],['clfo','CLFO'],['#actions']];
		$xlsTitle = 'Nome;Cliente fornitore'."\r\n";
	
		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		
		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
			
			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}
		
		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Causals','Csv']);
			$dataForXls = $this->Causal->find('all',['conditions'=>$conditionsArray,'order' => ['Causals.name' => 'asc']]); 			
			echo $xlsTitle; 
			echo $xlsRow['Causals']['name']. ';'. $xlsRow['Causals']['iban']. ';'."\r\n";
		}
		else
		{
			$this->paginate = ['conditions' =>$conditionsArray,'order'=> 'name']; 
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->set('causals', $this->paginate());
		}
	}

	public function add($redirect = 'index') 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Causal']);
		$datasource = $this->Causal->getDataSource();
		try 
		{
			$datasource->begin();
			if ($this->request->is('post')) 
			{
				$this->Causal->create();
				$this->request->data['Causal']['company_id']=MYCOMPANY;
				if (!$variabile = $this->Causal->save($this->request->data)) 
				{
					//throw new Exception('Errore durante la creazione della nuova banca.');
					$this->Session->setFlash(__('Errore durante la creazione della nuova banca'), 'custom-danger');
				}
				else
				{
					$this->Session->setFlash(__('Banca salvata'), 'custom-flash');
					$datasource->commit();
				}
				
				if($this->request->is('ajax'))
				{
					$this->layout = false;
					$this->set('data', $variabile['Causal']); 
					// $this->viewBuilder()->layout(false);
					$this->render('Ajax/flash');
    				// print(json_encode($dataArray));
					// die;
				}
				
				
					$this->redirect(['action' => $redirect]);
			}
		} 
		catch(Exception $e) 
		{
    		$datasource->rollback();
			$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
		}
	}
	
	public function edit($id = null, $redirect = 'index') 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Causal']);
		$this->Causal->id = $id;
		if (!$this->Causal->exists()) 
		{
			throw new NotFoundException(__('Banca non valida'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			$datasource = $this->Causal->getDataSource();
			try 
			{
				$datasource->begin();
				if (!$this->Causal->save($this->request->data)) 
				{
					throw new Exception('Errore durante la modifica della banca.');
				}
				else
				{
					$this->Session->setFlash(__('Banca modificata correttamente'), 'custom-flash');
					$datasource->commit();
					$this->redirect(array('action' => $redirect));
				}
			} 
			catch(Exception $e) 
			{
	    		$datasource->rollback();
	    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
			}
		} 
		else 
		{
			$this->request->data = $this->Causal->read(null, $id);
		}
	}
	
	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Causal','Messages']);
        $asg =  ["la","causale","F"];
		if($this->Causal->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Causal->find('first',['conditions'=>['Causal.id'=>$id,'Causal.company_id'=>MYCOMPANY]]);
        if ($this->Causal->hide($currentDeleted['Causal']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}
	
	
	public function getCausalType()
	{
		$this->autoRender = false;
		$this->loadModel('Causal');
		return $this->Causal->getCausalType($_POST['causalid']);
	}
}
