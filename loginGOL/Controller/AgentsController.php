<?php
App::uses('AppController', 'Controller');

class AgentsController extends AppController
{
    public function index()
    {
            $this->loadModel('Utilities');

            $conditionsArray = ['Agent.company_id' => MYCOMPANY, 'Agent.state' => ATTIVO];
            $filterableFields = ['name', 'surname', 'deposit_id'];
            $sortableFields = [['name', 'Nome'], ['surname', 'Cognome'], ['Deposit.code', 'Targa Furgone'], ['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            $this->paginate = ['conditions' => $conditionsArray];
            $this->set('agents', $this->paginate());
    }

    public function add()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technicians', 'Messages']);
            $messageParameter = ["il", "tecnico", "M"];
            if ($this->request->is('post')) {

                /** Salvo firma tecnico */
                if($this->request->data['Technicians']['sign']['tmp_name'])
                {
                    $path = $this->request->data['Technicians']['sign']['tmp_name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);
                    $this->request->data['Technicians']['sign'] =   base64_encode($data);
                }
                /** fine salvataggio firma tecnico */

                $this->Technicians->create();
                $this->request->data['Technicians']['company_id'] = MYCOMPANY;
                if($this->request->data['Technicians']['sign']['tmp_name'] == ''){ $this->request->data['Technicians']['sign'] = null;}
                if ($this->Technicians->save($this->request->data)) {
                    //$this->Session->setFlash(__('Tecnico salvato'), 'custom-flash');
                    $this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    //$this->Session->setFlash(__('Il tecnico è stato salvato'), 'custom-danger');
                    $this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                    //$this->Message->successOfAdd($messageParameter))
                }
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Agents', 'Messages']);
            $this->Agents->id = $id;

            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->Agents->save($this->request->data)) {
                    $this->Session->setFlash("L'agente è stato aggiornato correttamente.");
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash("Si è verificato un errore, riprova.");
                }
            } else {
                $this->request->data = $this->Agents->read(null, $id);
                $deposits = $this->Utilities->getDepositsList();
                $this->set('agent',$this->request->data);
                $this->set('deposits', $deposits);
            }
    }

    public function delete($id = null)
    {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Agents']);

            $messageParameter = ["l'", "agente", "M"];
            if ($this->Agent->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Agents->find('first', ['conditions' => ['Agents.id' => $id, 'Agents.company_id' => MYCOMPANY]]);
            if ($this->Agent->hide($currentDeleted['Agents']['id']))
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
            return $this->redirect(['action' => 'index']);
    }

    public function orderAgents($id = null){
        $this->loadModel('Utilities');
        $this->Utilities->loadmodels($this, ['Orders']);
        if($id != null){
            $conditionsArray = ['Orders.agent_id'=>$id,'Orders.company_id' => MYCOMPANY, 'Orders.state' => ATTIVO];
            $filterableFields = ['Order.numero_ordine', 'Order.delivery_date', null, null];
            $sortableFields = [['numero_ordine', 'Numero Ordine'], ['delivery_date', 'Data di consegna'], [null, 'Totale'], ['#actions']];
            $orders = $this->Orders->find('all', ['conditions'=>$conditionsArray]);
            $this->set('orders', $orders);
            $this->set('sortableFields', $sortableFields);
            $this->set('filterableFields', $filterableFields);
            $this->set('Utilities', $this->Utilities);
        }else{
            return $this->redirect(['action' => 'index']);
        }
    }

    public function techniciandetails($id = null)
    {
        if (MODULO_CANTIERI)
        {
            if($id != null)
            {
                $_SESSION['TechnicianId'] = $id;
            }

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technician']);

            $conditionsArray = ['Maintenance.state' => ATTIVO, 'Maintenance.maintenance_date >=' => date("Y-m") . '-01', 'Maintenance.maintenance_date <=' => date("Y") . '-12-31'];

            $filterableFields = ['maintenance_number', '#htmlElements[0]', 'intervention_description', null, null, null, null, null, null];
            $sortableFields = [[null, 'Numero scheda'], [null, 'Data scheda'], [null, 'Descrizione intervento'], [null, 'Ora inizio'], [null, 'Ora fine'], [null, 'Totale'], [null, 'Weekend'], [null, 'Notturno'], [null, 'Cantiere']];

            // Filtri automatici
            $automaticFilter = $this->Session->read('arrayOfFilters');
            if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
            {
                $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
                $startDate = $this->request->data['filters']['date1'];
                $endDate = $this->request->data['filters']['date2'];
            }
            else
            {
                //null;
                $startDate = '01'.date("-m-Y");
                $endDate = '31-12-'.date("Y");
            }

            $this->set('startDate', $startDate);
            $this->set('endDate', $endDate);

            if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
            {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                    $conditionsArray['Maintenance.maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                    $conditionsArray['Maintenance.maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

                $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
                $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
                $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
            }

            $this->set('TechnicianName', $this->Technician->getSurnameAndName($_SESSION['TechnicianId']));
            $this->set('technicianHours', $this->Technician->getWorkedHour($_SESSION['TechnicianId'], $conditionsArray));
            $this->set('technicinaId', $_SESSION['TechnicianId']);
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('utilities', $this->Utilities);
        }
    }
}
