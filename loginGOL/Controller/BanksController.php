<?php
App::uses('AppController', 'Controller');

class BanksController extends AppController {


	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Graphicsutilities']);
		$conditionsArray = ['Bank.company_id' => MYCOMPANY, 'Bank.state'=>ATTIVO];
		
		if (RIBA_FLOW) 
		{
			$filterableFields = ['description','Abi','Cab','iban',null];
			$sortableFields = [['description','Banca'],['abi','Abi'],['cab','Cab'],['iban','iban (bonifici in ingresso)'],['#actions']];
			$xlsTitle = 'Banca;Abi;Cab;Iban (bonifici in ingresso)'."\r\n";
		} 
		else
		{	
			$filterableFields = ['description','iban',null];
			$sortableFields = [['description','Banca'],['abi','Abi'],['cab','Cab'],['iban','iban (bonifici in ingresso)'],['#actions']];
			$xlsTitle = 'Banca;Iban (bonifici in ingresso)'."\r\n";
		}
		
		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		
		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
			
			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}
		
			// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Banks','Csv']);
			$dataForXls = $this->Banks->find('all',['conditions'=>$conditionsArray,'order' => ['Bank.description' => 'asc']]); 			
			echo $xlsTitle; 
			foreach ($dataForXls as $xlsRow)
			{
				if(RIBA_FLOW)
				{
					echo $xlsRow['Banks']['description']. ';' .$xlsRow['Banks']['abi']. ';'.$xlsRow['Banks']['cab'].';'.$xlsRow['Bank']['iban']. ';'."\r\n";
				}
				else
				{
					echo $xlsRow['Banks']['description']. ';'. $xlsRow['Banks']['iban']. ';'."\r\n";
				}
			}
		}
		else
		{
			$this->paginate = ['conditions' =>$conditionsArray,'order'=> 'description']; // Uso bank non banks
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->set('banks', $this->paginate());
		}
	}

	public function add($redirect = 'index') 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Bank']);
		$datasource = $this->Bank->getDataSource();
		try 
		{
			$datasource->begin();
			if ($this->request->is('post')) 
			{
				$this->Bank->create();
				$this->request->data['Bank']['company_id']=MYCOMPANY;
				if (!$variabile = $this->Bank->save($this->request->data)) 
				{
					//throw new Exception('Errore durante la creazione della nuova banca.');
					$this->Session->setFlash(__('Errore durante la creazione della nuova banca'), 'custom-danger');
				}
				else
				{
					$this->Session->setFlash(__('Banca salvata'), 'custom-flash');
					$datasource->commit();
				}
				
				if($this->request->is('ajax'))
				{
					$this->layout = false;
					$this->set('data', $variabile['Bank']); 
					// $this->viewBuilder()->layout(false);
					$this->render('Ajax/flash');
					// $dataArray = ['entityData' => $variabile['Bank'] ];
    				// print(json_encode($dataArray));
					// die;
				}
				
				
					$this->redirect(['action' => $redirect]);
			}
		} 
		catch(Exception $e) 
		{
    		$datasource->rollback();
			$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
		}
	}
	
	public function edit($id = null, $redirect = 'index') 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Bank']);
		$this->Bank->id = $id;
		if (!$this->Bank->exists()) 
		{
			throw new NotFoundException(__('Banca non valida'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			$datasource = $this->Bank->getDataSource();
			try 
			{
				$datasource->begin();
				if (!$this->Bank->save($this->request->data)) 
				{
					throw new Exception('Errore durante la modifica della banca.');
				}
				else
				{
					$this->Session->setFlash(__('Banca modificata correttamente'), 'custom-flash');
					$datasource->commit();
					$this->redirect(array('action' => $redirect));
				}
			} 
			catch(Exception $e) 
			{
	    		$datasource->rollback();
	    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
			}
		} 
		else 
		{
			$this->request->data = $this->Bank->read(null, $id);
		}
	}
	
	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Bank','Messages']);
        $asg =  ["la","banca","F"];
		if($this->Bank->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Bank->find('first',['conditions'=>['Bank.id'=>$id,'Bank.company_id'=>MYCOMPANY]]);
        if ($this->Bank->hide($currentDeleted['Bank']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}
}
