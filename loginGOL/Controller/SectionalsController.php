<?php
App::uses('AppController', 'Controller');

class SectionalsController extends AppController
{

	public function index()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Sectional','Csv']);

		$conditionsArray = ['Sectional.company_id' => MYCOMPANY,'Sectional.state'=>ATTIVO];

		if(MODULE_IXFE)
		{
			$sortableFields = [['null','Descrizione'],['null', 'Contatore'],['null', 'Suffisso'],['null','Sezionale ixfe'],['null', 'Predefinito per fatture'],['null', 'Predefinito per note di credito'],['null', 'Predefinito per pro-forma'],['null', 'Predefinito per bolle'],['null', 'Predefinito per ordini'],['null','Tipo sezionale'],['#actions']];
			$filterableFields = ['description',null,null,null,null,null,null,null,null,null,null];
		}
		else
		{
			$sortableFields = [['null','Descrizione'],['null', 'Contatore'],['null', 'Suffisso'],['null', 'Predefinito per fatture'],['null', 'Predefinito per note di credito'],['null', 'Predefinito per pro-forma'],['null', 'Predefinito per bolle'],['null', 'Predefinito per ordini'],['null','Tipo sezionale'],['#actions']];
			$filterableFields = ['description',null,null,null,null,null,null,null,null,null];
		}

		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }


		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}

		$this->set('filterableFields',$filterableFields);
		$this->set('sortableFields',$sortableFields);

		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Iva->find('all',['conditions'=>$conditionsArray,'order' => ['Sectional.description' => 'asc']]);
			echo 'Descrizione;Suffisso;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				// echo $xlsRow['Sectional']['description']. ';' .$xlsRow['Sectional']['prefix']. ';'.$xlsRow['Sectional']['suffix']. ';'."\r\n";
				echo $xlsRow['Sectional']['description']. ';' .$xlsRow['Sectional']['suffix']. ';'."\r\n";
			}
		}
		else
		{
			$this->paginate = ['conditions' => $conditionsArray];
			$this->set('sectionals', $this->paginate());
		}


		$this->set('utilities',$this->Utilities);

	}

	public function add()
	{
		$gender = 'M'; $article = 'il'; $title = 'sezionale';
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Messages']);
		$this->set('sectionalTypes',$this->Utilities->getSectionalTypes());

		if ($this->request->is('post'))
		{
			$this->Sectional->create();
			$this->request->data['Sectional']['company_id']=MYCOMPANY;

			$this->request->data['Sectional']['sectional_type'] == 'transport_sectional' ? $this->request->data['Sectional']['transport_sectional'] = 1 : null;
            $this->request->data['Sectional']['sectional_type'] == 'order_sectional' ? $this->request->data['Sectional']['order_sectional'] = 1 : null;
            $this->request->data['Sectional']['sectional_type'] == 'proforma_sectional' ? $this->request->data['Sectional']['proforma_sectional'] = 1 : null;
			$this->request->data['Sectional']['sectional_type'] == 'bill_sectional' ? $this->request->data['Sectional']['bill_sectional'] = 1 : null;
			$this->request->data['Sectional']['sectional_type'] == 'creditnote_sectional' ? $this->request->data['Sectional']['creditnote_sectional'] = 1 : null;

			if ($this->Sectional->save($this->request->data))
			{
				$this->Session->setFlash(__($this->Messages->successOfAdd($article, $title,$gender)), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				//$this->Session->setFlash(__($this->Messages->failOfAdd($article, $title,$gender)), 'custom-danger');
				$this->Session->setFlash(__('Errore durante la creazione del sezionale.'), 'custom-danger');
			}
		}
	}


	public function edit($id = null)
	{
		$gender = 'M'; $article = 'il'; $title = 'sezionale';
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Messages','Sectional']);
		$this->set('sectionalTypes',$this->Utilities->getSectionalTypes());
		$this->Sectional->id = $id;
		$currentSectional = $this->Sectional->find('first',['conditions'=>['Sectional.company_id'=>MYCOMPANY, 'Sectional.id'=>$id]]);
		$currentSectional['Sectional']['transport_sectional'] == 1 || $currentSectional['Sectional']['default_transport']  == 1 ?  $currentSectionaltoSend = 'transport_sectional' : null;
        $currentSectional['Sectional']['order_sectional'] == 1 || $currentSectional['Sectional']['default_order']  == 1 ?  $currentSectionaltoSend = 'order_sectional' : null;
        $currentSectional['Sectional']['proforma_sectional'] == 1 || $currentSectional['Sectional']['default_proforma'] == 1  ?  $currentSectionaltoSend = 'proforma_sectional' : null;
		$currentSectional['Sectional']['bill_sectional'] == 1 || $currentSectional['Sectional']['default_bill'] == 1? $currentSectionaltoSend = 'bill_sectional' : null;
		$currentSectional['Sectional']['creditnote_sectional'] == 1 || $currentSectional['Sectional']['default_creditnote'] == 1?  $currentSectionaltoSend = 'creditnote_sectional' : null;
		$this->set('currentSectional',$currentSectionaltoSend);

		if (!$this->Sectional->exists())
		{
			throw new NotFoundException(__($this->Messages->notFound($article, $title,$gender), 'custom-danger'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {


			if ($this->Sectional->save($this->request->data)) {
				$this->Session->setFlash(__($this->Messages->successOfUpdate($article, $title,$gender)), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Session->setFlash(__('Errore durante la modifica del sezionale.'), 'custom-danger');
			}
		} else
		{
			$this->request->data = $this->Sectional->read(null, $id);
		}
	}


	public function delete($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Sectional','Messages']);
        $asg =  ["il","sezionale","M"];
		if($this->Sectional->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Sectional->find('first',['conditions'=>['Sectional.id'=>$id,'Sectional.company_id'=>MYCOMPANY]]);
        if ($this->Sectional->hide($currentDeleted['Sectional']['id']))
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}


	public function setDefaultBillSectional()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$billSectional = $_POST['id'];
		$this->Utilities->setDefaultBillSectional($billSectional);
	}

	public function setDefaultCreditnoteSectional()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$creditnoteSectional = $_POST['id'];
		$this->Utilities->setDefaultCreditnoteSectional($creditnoteSectional);
	}

	public function setDefaultProformaSectional()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$proformaSectional = $_POST['id'];
		$this->Utilities->setDefaultProformaSectional($proformaSectional);
	}

	public function setDefaultTransportSectional()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$transportSectional = $_POST['id'];
		$this->Utilities->setDefaultTransportSectional($transportSectional);
	}

    public function setDefaultOrderSectional()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $ordersectional = $_POST['id'];
        $this->Utilities->setDefaultOrderSectional($ordersectional);
    }

    // Restituisce il prosso numero di un sezionale
	public function getSectionalNextNumber()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$nextNumber = $this->Utilities->getNextSectionalNumber($_POST['sectionalId']);
		return $nextNumber;
	}

}
