<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'mpdf/mpdf');
App::import('Vendor', 'tpmacn/csvhelper');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('Routing', 'Router');
App::uses('Xml', 'Utility');

require 'PHPMailer/PHPMailerAutoload.php';

class BillsController extends AppController
{
    public $components = ['Mpdf'];
    var $uses = ['Bill', 'Client', 'Good', 'Iva', 'Setting', 'Storage', 'User', 'Company', 'Payment'];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function indexproforma()
    {
        $this->index(8);
    }

    /* FATTURA VENDITA */
    public function index($tipologia = 1)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Csv']);

        // Recupero l'array degli stati per IXFE
        if (MODULE_IXFE)
        {
            $this->set('tokenIXFE', $tokenIXFE = $this->Utilities->checkIXFEAccessToken());
            $this->set('statoIXFE', $statoIXFE = $this->Utilities->getArrayStatoIXFE());
            $this->set('statoElaborazioneIXFE', $statoElaborazioneIXFE = $this->Utilities->getArrayStatoElaborazioneIXFE());
            $this->set('statoNotificaIXFE', $statoNotificaIXFE = $this->Utilities->getArrayNotificaIXFE());
        }

        $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia IN' => [$tipologia], 'date >= ' => date("Y-m") . '-01', 'date <= ' => date("Y") . '-12-31'];
        $conditionsArray2 = ['1 = 1'];

        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            $filterableFields = ['numero_fattura', '#htmlElements[0]', 'ragionesociale', 'descrizioni', null, null, null, null, null, 'payment_id', null];
            $sortableFields = [['numero_fattura', 'N° Fattura'], ['date', 'Data'], ['Client.ragionesociale', 'Cliente'], [null, 'Descrizione articoli'], [null, 'Imponibile'], [null, 'Tot. Ivato'], [null, 'Rit. Acc.'], [null, 'A Pagare'], [null, 'Tipo fattura'], ['Payment__metodo', 'Metodo di pagamento'], ['#actions']];
        }else{
            $filterableFields = ['numero_fattura', '#htmlElements[0]', 'ragionesociale', 'descrizioni', null, null, null, null, null, null];
            $sortableFields = [['numero_fattura', 'N° Fattura'], ['date', 'Data'], ['Client.ragionesociale', 'Cliente'], [null, 'Descrizione articoli'], [null, 'Imponibile'], [null, 'Tot. Ivato'], [null, 'Rit. Acc.'], [null, 'A Pagare'], [null, 'Tipo fattura'], ['#actions']];
        }
        // Filtri automatici
        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
        {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
            $startDate = $this->request->data['filters']['date1'];
            $endDate = $this->request->data['filters']['date2'];
        }
        else
        {
            $startDate = '01' . date("-m-Y");
            $endDate = '31-12-' . date("Y");
        }

        $this->set('startDate', $startDate);
        $this->set('endDate', $endDate);

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO];
            $conditionsArray['Bill.numero_fattura like'] = $this->request->data['filters']['numero_fattura'] . '%';
            $conditionsArray['Bill.tipologia IN'] = [$tipologia];

            if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                if(isset($this->request->data['filters']['payment_id']) && $this->request->data['filters']['payment_id'] != ''){
                    $paymentConditionArray['Payment.metodo like '] = '%'.$this->request->data['filters']['payment_id'].'%';
                    $paymentMethods = $this->Payment->find('list',['fields'=>['id','metodo'] ,'conditions'=>$paymentConditionArray]);
                    $arrayOfPayment=[];

                    foreach($paymentMethods as $key => $payment)
                        $arrayOfPayment[] = $key;

                    $conditionsArray['Bill.payment_id IN'] = $arrayOfPayment;
                }
            }

            if (isset($this->request->data['filters']['descrizioni']) && $this->request->data['filters']['descrizioni'] != '')
                $conditionsArray2 = [
                    'OR' => [
                        'Good.oggetto like' => '%' . $this->request->data['filters']['descrizioni'] . '%',
                        'Good.customdescription like' => '%' . $this->request->data['filters']['descrizioni'] . '%']
                ];

            if (isset($this->request->data['filters']['ragionesociale']) && $this->request->data['filters']['ragionesociale'] != '')
                $conditionsArray['Bill.client_name like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Seleziono tutto

        $bills = $this->Bill->find('all', [
            'contain' => [
                'Good' => ['conditions' => $conditionsArray2],
            ],
            'conditions'=>$conditionsArray
        ]);

        // Aggiorno il token prima di ciclare le fatture
        /*
        if (MODULE_IXFE)
        {
            $this->Utilities->resetSettingsIXFE();
            $arrayForIXFEBill = [];
        }
        */

        // Rielaboro per filtrare
        $arrayBills = [];
        $arrayCreditnotes = [];

        foreach ($bills as $key => $B)
        {
            if (count($B['Good']) == 0 && isset($this->request->data['filters']['descrizioni']) && $this->request->data['filters']['descrizioni'] != '')
            {
            }
            else
            {
                $arrayBills[] = $B['Bill']['id'];
            }
        }

        $this->set('arrayCreditnotes', $arrayCreditnotes);

        if ($arrayBills != null)
            $conditionsArray['Bill.id IN'] = $arrayBills;
        else
            $conditionsArray['Bill.id'] = -1;

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
        {
            ini_set('max_execution_time', 600);
            set_time_limit(600);
            $this->autoRender = false;

            $dataForXls = $this->Bill->find('all', ['conditions' => $conditionsArray, 'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc']]);

            echo 'N° Fattura;Data;Cliente;Imponibile;Tot. Ivato;Rit. Acc.;A Pagare;' . "\r\n";

            $totaleImponibile = 0;
            $totaleIvato = 0;
            $totaleWithholdingtax = 0;
            $totaleAPagare = 0;
            foreach ($dataForXls as $xlsRow) {
                $billId = $xlsRow['Bill']['id'];

                // Imponibile
                $totalWelfareBox = $this->Utilities->getWelfareBoxTotal($billId);
                $taxableIncome = $this->Utilities->getBillTaxable($billId);
                $taxableIncome += $totalWelfareBox;
                $taxableIncome = number_format($taxableIncome, 2, '.', '');
                $totaleImponibile += $taxableIncome;

                // Totale ivato
                $ivato = $this->Utilities->getBillTotal($billId);
                $ivato += $totalWelfareBox; // Aggiungo la cassa
                $xlsRow['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal = $this->Utilities->getVatFromId($xlsRow['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox / 100 : $welfarVatTotal = 0;
                $ivato += $welfarVatTotal;
                $total = number_format($ivato, 2, '.', '');
                $totaleIvato += $total;

                // Totale ritenuta d'acconto
                $withholdingtax = $this->Utilities->getWitholdingTaxTotal($billId);
                $withholdingtax = number_format($withholdingtax, 2, '.', '');
                $totaleWithholdingtax += $withholdingtax;

                // A Pagare
                $daPagareFattura = $this->Utilities->getBillTotalToPay($billId);
                $daPagareFattura += $welfarVatTotal + $totalWelfareBox;
                $daPagareFattura = number_format($daPagareFattura, 2, '.', '');
                $totaleAPagare += $daPagareFattura;

                echo $xlsRow['Bill']['numero_fattura'] . SCSV . date('d-m-Y', strtotime($xlsRow['Bill']['date'])) . SCSV . $xlsRow['Bill']['client_name'] . SCSV . $taxableIncome . SCSV . $total . SCSV . $withholdingtax . SCSV . $daPagareFattura . SCSV . "\r\n";
            }

            echo "\r\n" . 'Totale Imponibile' . SCSV . $totaleImponibile . SCSV;
            echo "\r\n" . 'Totale Ivato' . SCSV . $totaleIvato . SCSV;
            echo "\r\n" . 'Totale Rit. Acc' . SCSV . $totaleWithholdingtax . SCSV;
            echo "\r\n" . 'Totale A Pagare' . SCSV . $totaleAPagare . SCSV . "\r\n";
        }
        else {
            // E Poi pagino
            if ($_SESSION['Auth']['User']['dbname'] == "login_GE0047") {
                $this->paginate = [
                    'contain' => [
                        'Client',
                        'Good' => ['fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount', 'Good.Oggetto', 'Good.customdescription'], 'Iva'],
                        'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                        'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
                        'Deadline' => ['Deadlinepayment'],
                    ],
                    'conditions' => $conditionsArray,
                    'order' => ['Bill.date' => 'desc', 'Bill.id' => 'desc'], // 'Bill.numero_fattura' => 'desc'
                    'limit' => 20
                ];
            } else{
                $this->paginate = [
                    'contain' => [
                        'Client',
                        'Good' => ['fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount', 'Good.Oggetto', 'Good.customdescription'], 'Iva'],
                        'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                        'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
                        'Deadline' => ['Deadlinepayment'],
                    ],
                    'conditions' => $conditionsArray,
                    'order' => ['Bill.date' => 'desc', 'Bill.id' => 'desc'],
                    'limit' => 20
                ];
            }
            //if (MODULE_IXFE)
            //$this->set('arrayForIXFEBill', $arrayForIXFEBill);

            $this->set('tipologia', $tipologia);
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('bills', $this->paginate());
            $this->set('Utilities', $this->Utilities);
            $this->set('sectionals', $this->Utilities->getSectionalsArray());

            $this->render('index');
        }
    }

    /* FATTURA ACQUIISTO */
    public function indexExtendedbuy()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Csv']);

        $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia IN' => [2], 'date >= ' => date("Y-m") . '-01', 'date <= ' => date("Y") . '-12-31'];
        $filterableFields = ['numero_fattura', '#htmlElements[0]', 'name', null, null, null, null, null];
        $sortableFields = [['numero_fattura', 'N° Fattura'], ['date', 'Data'], ['Supplier__name', 'Fornitore'], [null, 'Imponibile'], [null, 'Tot. Ivato'], [null, 'Rit. Acc.'], [null, 'A Pagare'], ['#actions']];

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
            $startDate = $this->request->data['filters']['date1'];
            $endDate = $this->request->data['filters']['date2'];
        } else {
            $startDate = '01' . date("-m-Y");
            $endDate = '31-12-' . date("Y");
        }

        $this->set('startDate', $startDate);
        $this->set('endDate', $endDate);

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia' => 2];

            if (isset($this->request->data['filters']['numero_fattura']) && $this->request->data['filters']['numero_fattura'] != '')
                $conditionsArray['numero_fattura like'] = '%' . $this->request->data['filters']['numero_fattura'] . '%';

            if (isset($this->request->data['filters']['name']) && $this->request->data['filters']['name'] != '')
                $conditionsArray['Supplier.name like'] = '%' . $this->request->data['filters']['name'] . '%';

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        $this->paginate = [
            'contain' => [
                'Supplier',
                'Billgestionaldata',
                'Good' => ['fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount', 'Good.Oggetto', 'Good.customdescription'], 'Iva'],
                'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
                'Deadline' => ['Deadlinepayment'],
            ],
            'conditions' => $conditionsArray,
            'order' => ['Bill.date' => 'desc', 'Bill.id' => 'desc'],
            'limit' => 20
        ];

        // Xls
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            ini_set('max_execution_time', 600);
            set_time_limit(600);
            $this->autoRender = false;

            $dataForXls = $this->Bill->find('all', ['conditions' => $conditionsArray, 'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc']]);

            echo 'N° Fattura;Data;Fornitore;Imponibile;Tot. Ivato;Rit. Acc.;A Pagare;' . "\r\n";

            $totaleImponibile = 0;
            $totaleIvato = 0;
            $totaleWithholdingtax = 0;
            $totaleAPagare = 0;
            foreach ($dataForXls as $xlsRow) {
                $billId = $xlsRow['Bill']['id'];

                if ($xlsRow['Bill']['imported_bill'] == 0) {
                    // Imponibile
                    $totalWelfareBox = $this->Utilities->getWelfareBoxTotal($billId);
                    $taxableIncome = $this->Utilities->getBillTaxable($billId);
                    $taxableIncome += $totalWelfareBox;
                    $taxableIncome = number_format($taxableIncome, 2, '.', '');
                    $totaleImponibile += $taxableIncome;

                    // Totale ivato
                    $ivato = $this->Utilities->getBillTotal($billId);
                    $ivato += $totalWelfareBox; // Aggiungo la cassa
                    $xlsRow['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal = $this->Utilities->getVatFromId($xlsRow['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox / 100 : $welfarVatTotal = 0;
                    $ivato += $welfarVatTotal;
                    $total = number_format($ivato, 2, '.', '');
                    $totaleIvato += $total;

                    // Totale ritenuta d'acconto
                    $withholdingtax = $this->Utilities->getWitholdingTaxTotal($billId);
                    $withholdingtax = number_format($withholdingtax, 2, '.', '');
                    $totaleWithholdingtax += $withholdingtax;

                    // A Pagare
                    $daPagareFattura = $this->Utilities->getBillTotalToPay($billId);
                    $daPagareFattura = number_format($daPagareFattura, 2, '.', '');
                    $totaleAPagare += $daPagareFattura;
                } else {
                    switch ($xlsRow['Bill']['typeofdocument']) {
                        case 'TD04':
                            $coefficienteMoltiplicativo = -1;
                            break;
                        default:
                            $coefficienteMoltiplicativo = 1;
                            break;
                    }

                    // Imponibile
                    $imponibile = 0.00;
                    foreach ($this->Utilities->getBillGestionalData($billId) as $row) {
                        $imponibile += $coefficienteMoltiplicativo * $row['Billgestionaldata']['imponibileimporto'];
                    }

                    $taxableIncome = number_format($imponibile, 2, '.', '');
                    $totaleImponibile += $taxableIncome;

                    // Totale ivato
                    $total = number_format($coefficienteMoltiplicativo * $xlsRow['Bill']['importo'], 2, '.', '');
                    $totaleIvato += $total;

                    // Totale ritenuta d'acconto
                    $ritenuta = 0.00;
                    foreach ($this->Utilities->getBillGestionalData($billId) as $row) {
                        $ritenuta += $coefficienteMoltiplicativo * $row['Billgestionaldata']['importedwithholdingvalue'];
                    }

                    $withholdingtax = number_format($ritenuta, 2, '.', '');
                    $totaleWithholdingtax += $withholdingtax;

                    // A Pagare
                    $this->Utilities->getDeadlinesTotal($billId) != null ? $apagareImportato = $this->Utilities->getDeadlinesTotal($billId) : $apagareImportato = $coefficienteMoltiplicativo * $xlsRow['Bill']['importo'];
                    $daPagareFattura = number_format($coefficienteMoltiplicativo * $apagareImportato, 2, '.', '');
                    $totaleAPagare += $daPagareFattura;
                }

                echo $xlsRow['Bill']['numero_fattura'] . SCSV . date('d-m-Y', strtotime($xlsRow['Bill']['date'])) . SCSV . $xlsRow['Supplier']['name'] . SCSV . $taxableIncome . SCSV . $total . SCSV . $withholdingtax . SCSV . $daPagareFattura . SCSV . "\r\n";
            }

            echo "\r\n" . 'Totale Imponibile' . SCSV . $totaleImponibile . SCSV;
            echo "\r\n" . 'Totale Ivato' . SCSV . $totaleIvato . SCSV;
            echo "\r\n" . 'Totale Rit. Acc' . SCSV . $totaleWithholdingtax . SCSV;
            echo "\r\n" . 'Totale A Pagare' . SCSV . $totaleAPagare . SCSV . "\r\n";
        } else {
            $this->Bill->recursive = 0;
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('bills', $this->paginate());
            $this->set('Utilities', $this->Utilities);

            $this->render('index_extendedbuy');
        }
    }

    /* NOTE DI CREDITO */
    public function indexExtendedcreditnotes()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Csv']);

        $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia IN' => [3], 'date >= ' => date("Y") . '-01-01', 'date <= ' => date("Y") . '-12-31'];

        $filterableFields = ['numero_fattura', '#htmlElements[0]', 'ragionesociale', null, null, null, null, null, null];
        $sortableFields = [['numero_fattura', 'N° nota di credito'], ['date', 'Data nota di credito'], ['ragionesociale', 'Cliente'], [null, 'Descrizione articoli'], [null, 'Imponibile'], [null, 'Tot. Ivato'], [null, 'Rit. Acc.'], [null, 'A. Pagare'], ["#actions"]];

        // Recupero l'array degli stati per IXFE
        if (MODULE_IXFE) {
            $this->set('statoIXFE', $statoIXFE = $this->Utilities->getArrayStatoIXFE());
            $this->set('statoElaborazioneIXFE', $statoElaborazioneIXFE = $this->Utilities->getArrayStatoElaborazioneIXFE());
            $this->set('statoNotificaIXFE', $statoNotificaIXFE = $this->Utilities->getArrayNotificaIXFE());
        }

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia' => 3, 'Bill.state' => ATTIVO];
            if (isset($this->request->data['filters']['ragionesociale']))
                $conditionsArray['ragionesociale like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

            if (isset($this->request->data['filters']['numero_fattura']))
                $conditionsArray['Bill.numero_fattura like'] = '%' . $this->request->data['filters']['numero_fattura'] . '%';

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        $this->paginate = [
            'contain' => [
                'Client',
                'Good' => ['fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount', 'Good.Oggetto', 'Good.customdescription'], 'Iva'],
                'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
                'Deadline' => ['Deadlinepayment']
            ],
            'conditions' => $conditionsArray,
            'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc', 'Bill.id' => 'desc'],
            'limit' => 100
        ];

        // Xls
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Bill->find('all', ['conditions' => $conditionsArray, 'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc']]);

            echo 'N° nota di credito;Data nota di credito;Cliente;Imponibile;Tot. Ivato;Rit. Acc.;A. Pagare;' . "\r\n";

            foreach ($dataForXls as $xlsRow) {
                $billId = $xlsRow['Bill']['id'];
                $taxableIncome = number_format($this->Utilities->getBillTaxable($billId), 2, ',', '.');
                $totalVat = number_format($this->Utilities->getBillVat($billId), 2, ',', '.');
                $total = number_format($this->Utilities->getBillTotal($billId), 2, ',', '.');
                $withholdingtax = number_format($this->Utilities->getWitholdingTaxTotal($billId), 2, ',', '.');
                $getToBillToPay = number_format($this->Utilities->getBillTotalToPay($billId), 2, ',', '.');

                echo $xlsRow['Bill']['numero_fattura'] . '/' . substr($xlsRow['Bill']['date'], 0, 4) . ';' . date('d-m-Y', strtotime($xlsRow['Bill']['date'])) . ';' . $xlsRow['Client']['ragionesociale'] . ';' . $taxableIncome . ';' . $total . ';' . $withholdingtax . ';' . $getToBillToPay . ';' . "\r\n";
            }
        } else {
            $this->Bill->recursive = 0;
            $this->Set('tipologia', ['1' => 'Fattura', '4' => 'Fattura elettronica', '5' => 'Split Payment', '6' => 'Fattura accompagnatoria']);
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('bills', $this->paginate());
            $this->set('Utilities', $this->Utilities);
            $this->set('sectionals', $this->Utilities->getSectionalsArray());
            $this->render('index_extendedcreditnotes');
        }
    }

    /*SCONTRINI */
    public function indexReceipt()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Csv']);

        $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia IN' => [7], 'date >= ' => date("Y") . '-01-01', 'date <= ' => date("Y") . '-12-31'];
        $filterableFields = [null, '#htmlElements[0]', 'ragionesociale', 'descrizioni', null, null, null, null];
        $sortableFields = [['numero_fattura', 'N° Scontrino'], ['date', 'Data'], ['ragionesociale', 'Cliente'], [null, 'Articoli'], [null, 'Imponibile'], [null, 'Iva'], [null, 'Tot. Ivato'], ['#actions']];
        $conditionsArray2 = ['1 = 1'];

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = ['Bill.company_id' => MYCOMPANY, 'Bill.tipologia' => 7, 'Bill.state' => ATTIVO];

            if (isset($this->request->data['filters']['ragionesociale']))
                $conditionsArray['ragionesociale like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

            if (isset($this->request->data['filters']['descrizioni']))
                $conditionsArray2 = [
                    'OR' => [
                        'Good.oggetto like' => '%' . $this->request->data['filters']['descrizioni'] . '%',
                        'Good.customdescription like' => '%' . $this->request->data['filters']['descrizioni'] . '%']
                ];

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        $this->paginate = [
            'contain' => [
                'Client',
                'Good' => ['conditions' => $conditionsArray2, 'fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount', 'Good.Oggetto', 'Good.customdescription'], 'Iva'],
                'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
                'Deadline' => ['Deadlinepayment'],
            ],
            'conditions' => $conditionsArray,
            'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc', 'Bill.id' => 'desc'],
            'limit' => 100
        ];

        // Xls
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Bill->find('all', ['conditions' => $conditionsArray, 'order' => ['Bill.date' => 'desc', 'Bill.numero_fattura' => 'desc']]);

            echo 'N° Scontrino;Data;Cliente;Imponibile;Iva;Tot. Ivato;' . "\r\n";

            foreach ($dataForXls as $xlsRow) {
                $billId = $xlsRow['Bill']['id'];
                $taxableIncome = number_format($this->Utilities->getBillTaxable($billId), 2, ',', '.');
                $total = number_format($this->Utilities->getBillTotal($billId), 2, ',', '.');
                $vat = $this->Utilities->getBillTaxable($billId) - $this->Utilities->getBillTaxable($billId);
                echo $xlsRow['Bill']['numero_fattura'] . '/' . substr($xlsRow['Bill']['date'], 0, 4) . ';' . date('d-m-Y', strtotime($xlsRow['Bill']['date'])) . ';' . $xlsRow['Client']['ragionesociale'] . ';' . $taxableIncome . ';' . $vat . ';' . $total . ';' . "\r\n";
            }
        } else {
            $this->Bill->recursive = 0;
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('bills', $this->paginate());
            $this->set('Utilities', $this->Utilities);
            $this->render('index_receipt');
        }
    }

    // Non cancellare (?)
    public function paypalimport() { }

    // Non cancellare (?)
    public function ebillimport() { }

    // Non cancellare chiama vista
    public function sentbillimport() { }

    public function statistics() { }

    public function downloadFatturato() { }

    public function fatturePerPeriodo() { }

    public function articoliAnnuo() { }

    public function downloadArticoli()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Pdf']);

        $anno = $_POST['data']['Anno']['year'];
        $prodotti = $this->Bill->query("SELECT storages.id, descrizione, units.description FROM storages INNER JOIN units ON storages.unit_id = units.id WHERE storages.state = 1");

        $data = array();
        foreach ($prodotti as $prodotto) {
            $pass = $this->Bill->query("SELECT SUM(quantita) as qta FROM goods INNER JOIN bills ON goods.bill_id = bills.id WHERE YEAR(bills.date) = '" . $anno . "' AND storage_id = '" . $prodotto['storages']['id'] . "'");

            if ($pass[0][0]['qta'] != null) {
                $arrayMesi = $this->Bill->query("SELECT SUM(quantita) as qta, SUM((goods.prezzo + (goods.prezzo*(ivas.percentuale/100)))*quantita) as totale, MONTH(bills.date) as mese FROM `goods` INNER JOIN ivas on goods.iva_id = ivas.id INNER JOIN storages on goods.storage_id = storages.id INNER JOIN bills ON goods.bill_id = bills.id WHERE bills.state = 1 AND YEAR(bills.date) = '" . $anno . "' AND storage_id = '" . $prodotto['storages']['id'] . "' GROUP BY MONTH(bills.date) ORDER BY MONTH(bills.date)");
                $completo = ["nome" => $prodotto['storages']['descrizione'], "id" => $prodotto['storages']['id'], "unita" => $prodotto['units']['description'], "calcoli" => $arrayMesi];
                array_push($data, $completo);
            }
        }

        $this->set('definitivo', $data);
        $this->set('anno', $anno);
        $this->Pdf->setMpdf('Statistiche Fatturato per articolo', '1', $this->Mpdf);
    }

    public function downloadFatturePeriodo()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Pdf']);
        $dataIn = date('Y-m-d', strtotime($_POST['dateFrom']));
        $dataFi = date('Y-m-d', strtotime($_POST['dateTo']));
        $fatture = $this->Bill->query("SELECT bills.id as id, numero_fattura, client_id, date, metodo from bills INNER JOIN payments ON bills.payment_id = payments.id WHERE bills.state = '1' AND date between '" . $dataIn . "' AND '" . $dataFi . "'");

        $dati = array();
        foreach ($fatture as $key) {
            $cliente = $key['bills']['client_id'];
            if ($cliente != null) {
                $result = $this->Bill->query("SELECT SUM(prezzo*quantita) as merce,SUM((prezzo + (prezzo*(ivas.percentuale/100)))*quantita) as totale, SUM((prezzo*(ivas.percentuale/100)*quantita)) as IVA from goods INNER JOIN ivas on goods.iva_id = ivas.id WHERE bill_id = '" . $key['bills']['id'] . "'");
                $result1 = $this->Bill->query("SELECT ragionesociale FROM clients WHERE id = '" . $cliente . "'");
                $nome = $result1[0]['clients']['ragionesociale'];
                $totale = $result[0][0]['totale'];
                $iva = $result[0][0]['IVA'];
                $merce = $result[0][0]['merce'];
                if ($totale != null) {
                    $dato = ["nDoc" => $key['bills']['numero_fattura'], "date" => $key['bills']['date'], "cliente" => $nome, "merce" => $merce, "iva" => $iva, "totale" => (double)$totale, "metodo" => $key['payments']['metodo']];
                    array_push($dati, $dato);
                }
            }
        }
        for ($i = 0; $i < count($dati); $i++) {
            $low = $i;
            for ($j = $i + 1; $j < count($dati); $j++) {
                if ($dati[$j]['date'] > $dati[$low]['date']) {
                    $low = $j;
                }
            }

            // swap the minimum value to $ith node
            if ($dati[$i]['date'] < $dati[$low]['date']) {
                $tmp = $dati[$i];
                $dati[$i] = $dati[$low];
                $dati[$low] = $tmp;
            }
        }
        $totMerce = 0;
        $totIva = 0;
        $totTot = 0;
        foreach ($dati as $riga) {
            $totMerce += $riga['merce'];
            $totTot += $riga['totale'];
            $totIva += $riga['iva'];
        }
        $this->set('definitivo', $dati);
        $this->set('dataInizio', $dataIn);
        $this->set('dataFine', $dataFi);
        $this->set('totale', $totTot);
        $this->set('iva', $totIva);
        $this->set('merce', $totMerce);
        $this->Pdf->setMpdf('Stampe Fatture del Protocollo', '1', $this->Mpdf);
    }

    public function downloadVeroFatturato()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Pdf']);
        $dataIn = date('Y-m-d', strtotime($_POST['dateFrom']));
        $dataFi = date('Y-m-d', strtotime($_POST['dateTo']));
        $fatturatoFake = $this->Bill->query("SELECT SUM((prezzo + (prezzo*(ivas.percentuale/100)))*quantita) as fatturato from goods INNER JOIN ivas on goods.iva_id = ivas.id where bill_id in (SELECT id from bills where state = '1' AND `date` between '" . $dataIn . "' and '" . $dataFi . "')");
        $fatturato = (double)$fatturatoFake[0][0]['fatturato'];
        $clients = $this->Bill->query('SELECT client_id from bills GROUP BY client_id');
        $dati = array();
        foreach ($clients as $key) {
            $cliente = $key['bills']['client_id'];
            if ($cliente != null) {
                $result = $this->Bill->query("SELECT SUM(quantita*prezzo) as 'imponibile' from goods where bill_id in (SELECT id from bills where state = '1' AND client_id = '" . $cliente . "' AND `date` between '" . $dataIn . "' and '" . $dataFi . "'  )");
                $result1 = $this->Bill->query("SELECT code, ragionesociale, provincia, citta FROM clients WHERE id = '" . $cliente . "'");
                $nome = $result1[0]['clients']['ragionesociale'];
                $imponibile = $result[0][0]['imponibile'];
                $codice = $result1[0]['clients']['code'];
                $provincia = $result1[0]['clients']['provincia'];
                $citta = $result1[0]['clients']['citta'];
                if ($imponibile != null) {
                    $dato = ["codice" => $codice, "cliente" => $nome, "provincia" => $provincia, "citta" => $citta, "imponibile" => (double)$imponibile];
                    array_push($dati, $dato);
                }
            }
        }
        for ($i = 0; $i < count($dati); $i++) {
            $low = $i;
            for ($j = $i + 1; $j < count($dati); $j++) {
                if ($dati[$j]['imponibile'] > $dati[$low]['imponibile']) {
                    $low = $j;
                }
            }

            // swap the minimum value to $ith node
            if ($dati[$i]['imponibile'] < $dati[$low]['imponibile']) {
                $tmp = $dati[$i];
                $dati[$i] = $dati[$low];
                $dati[$low] = $tmp;
            }
        }
        $definitivo = array();
        foreach ($dati as $cPer) {
            $per = round(($cPer['imponibile'] * 100) / ((double)$fatturato), 2);
            $temp = ["codice" => $cPer['codice'], "cliente" => $cPer['cliente'], "provincia" => $cPer['provincia'], "citta" => $cPer['citta'], "imponibile" => $cPer['imponibile'], "percentuale" => $per];
            array_push($definitivo, $temp);
        }

        $this->set('definitivo', $definitivo);
        $this->set('fatturato', round($fatturato, 2));
        $this->Pdf->setMpdf('Fatturato Annuo', '1', $this->Mpdf);
    }

    public function fattArticoli() { }

    public function addproforma()
    {
        $this->add(8); // Proforma
        $this->Render('add');
    }

    // Aggiunta estese
    public function add($tipologia = 1)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Deadline', 'Ivas', 'Units', 'Clientdestination', 'Setting', 'Currencies', 'Client', 'Bank']);

        $this->request->data['Bill']['tipologia'] = $tipologia;

        // Fatture di vendita
        if ($tipologia == 1)
        {
            $redirect = 'index';

            $defaultSectional = $this->Utilities->getDefaultBillSectional();
            $this->set('defaultSectional', $defaultSectional);

            $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
            $this->set('nextBillNumber', $nextSectionalNumber);
            $this->set('sectionals', $this->Utilities->getBillSectionalList());
        }

        // Fatture pro -forma
        if ($tipologia == 8)
        {
            $redirect = 'indexproforma';

            $defaultSectional = $this->Utilities->getDefaultProformaSectional();
            $this->set('defaultSectional', $defaultSectional);

            $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
            $this->set('nextBillNumber', $nextSectionalNumber);
            $this->set('sectionals', $this->Utilities->getProformaSectionalList());
        }

        if ($this->request->is(['post', 'put']))
        {
            if (!isset($this->request->data['Bill']['accompagnatoria']))
                $accompagnatoria = false;
            else
                $this->request->data['Bill']['accompagnatoria'] == 1 ? $accompagnatoria = true : $accompagnatoria = false;

            $this->request->data['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;
            if (!isset($this->request->data['Bill']['electronic_invoice']))
                $pa = false;
            else
                $this->request->data['Bill']['electronic_invoice'] == 1 ? $pa = true : $pa = false;

            $cliente = $this->Client->find('first', ['conditions' => ['Client.id' => $this->request->data['Bill']['client_id'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id', 'Client.ragionesociale', 'Client.currency_id']]);
            $this->request->data['Bill']['company_id'] = MYCOMPANY;

            // Salva se è una fattura accompagnatoria o no [ in edit non lo faccio tanto è già definita ] - ridondanza inutile perché adesso tipologia = 6 è accompagnatoria
            $numberOfMovement = 0;

            // Se il cliente non esiste allora lo creo
            if (empty($cliente['Client']['id']))
            {
                $clientAddress = $this->request->data['Bill']['client_address'];
                $clientCap = $this->request->data['Bill']['client_cap'];
                $clientCity = $this->request->data['Bill']['client_city'];
                $clientProvince = $this->request->data['Bill']['client_province'];
                isset($this->request->data['Bill']['client_nation']) ? $clientNation = $this->request->data['Bill']['client_nation'] : $clientNation = null;
                // Setto il nome utente
                $this->request->data['Bill']['client_name'] = $cliente['Client']['ragionesociale'];
                // Poi client id diventa id
                $this->request->data['Bill']['client_id'] = $this->Utilities->createClient($this->request->data['Bill']['client_id'], $clientAddress, $clientCap, $clientCity, $clientProvince, $clientNation);
                // La definizione della currencies (default EUR)
                $this->request->data['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');
                $this->request->data['Bill']['changevalue'] = 1;
            }
            else
            {
                // Setto il nome utente
                $this->request->data['Bill']['client_name'] = $cliente['Client']['ragionesociale'];
                // Salvo valuta sulla fattura
                $this->request->data['Bill']['currency_id'] = $cliente['Client']['currency_id'];
            }

            $this->request->data['Bill']['date'] = date("Y-m-d", strtotime($this->request->data['Bill']['date']));

            if (isset($this->request->data['Bill']['billaccdate']))
                $this->request->data['Bill']['billaccdate'] = date("Y-m-d", strtotime($this->request->data['Bill']['billaccdate']));

            if (isset($_POST['data']['Bill']['mainrifddtdate']))
            {
                // Data per riferimento ddt
                if ($_POST['data']['Bill']['mainrifddtdate'] == '' || $_POST['data']['Bill']['mainrifddtdate'] == '01-01-1970')
                {
                }
                else
                {
                    $this->request->data['Bill']['mainrifddtdate'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['mainrifddtdate'])));
                }
            }

            isset($this->request->data['Bill']['cig_date']) ? $this->request->data['Bill']['cig_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['cig_date']))) : null;

            // Se è selezionato alternative address lo salvo come destino
            if (isset($this->request->data['Bill']['alternativeaddress_id']))
            {
                $shippingAddress = $this->Clientdestination->find('first', ['conditions' => ['Clientdestination.id' => $this->request->data['Bill']['alternativeaddress_id']]]);
                if (count($shippingAddress) > 0) {
                    $this->request->data['Bill']['client_shipping_name'] = $shippingAddress['Clientdestination']['name'];
                    $this->request->data['Bill']['client_shipping_address'] = $shippingAddress['Clientdestination']['address'];
                    $this->request->data['Bill']['client_shipping_cap'] = $shippingAddress['Clientdestination']['cap'];
                    $this->request->data['Bill']['client_shipping_city'] = $shippingAddress['Clientdestination']['city'];
                    $this->request->data['Bill']['client_shipping_province'] = $shippingAddress['Clientdestination']['province'];
                    if (isset($shippingAddress['Clientdestination']['nation_id'])) {
                        $this->request->data['Bill']['client_shipping_nation'] = $this->Utilities->getNationName($shippingAddress['Clientdestination']['nation_id'])['Nation']['name'];
                    }
                }
            }

            // Aggiungo valori per cassa previdenziale ( Nell'edit non vado a modificarla per ora )
            /*			$this->request->data['Bill']['welfare_box_code'] =  $settings['Setting']['welfare_box_code'];
			$this->request->data['Bill']['welfare_box_vat_id'] = $settings['Setting']['welfare_box_vat_id'];
			$this->request->data['Bill']['welfare_box_percentage'] = $settings['Setting']['welfare_box_percentage'] ;
			$this->request->data['Bill']['welfare_box_withholding_appliance'] = $settings['Setting']['welfare_box_withholding_appliance'];
			*/

            if ($newBill = $this->Bill->save($this->request->data['Bill']))
            {
                // Setto a zero i valori per i calcoli delle scadenze
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = $taxableIncomeForDeadline = $vatForDeadline = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];
                foreach ($this->request->data['Good'] as $key => $good) {
                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                    $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);

                    // Se esiste lo storage id
                    if ($good['storage_id'] != '') {
                        if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1) {
                            $theStorageId = $good['storage_id'];
                            if (ADVANCED_STORAGE_ENABLED) {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                            } else {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                            }
                        }else if($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 0){
                            $theStorageId = $good['storage_id'];
                            if (ADVANCED_STORAGE_ENABLED) {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                            } else {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                            }
                        } else if($_SESSION['Auth']['User']['dbname'] == "login_GE0050" && $newBill['Bill']['tipologia'] == 8){
                            $theStorageId = $good['storage_id'];
                            if (ADVANCED_STORAGE_ENABLED) {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura pro forma n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                            } else {
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura pro forma n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                            }
                        }
                        $this->Good->updateAll(['Good.storage_id' => $good['storage_id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                    } else {
                        if (!$this->Utilities->isANote($good, 'bill')) {
                            if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                                $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                                if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1) {
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        if (isset($newStorage['Storage']['id'])) {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                                        }
                                    } else {
                                        if (isset($newStorage['Storage']['id'])) {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                                        }
                                    }
                                }
                            }
                        }
                    }

                    isset($good['iva_id']) ? $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]) : null;



                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                        if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                            $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount( $good['quantita'], $good['currencyprice'], $good['discount']);
                        } else {
                            $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                        }
                        if($good['complimentaryQuantity'] > 0 ){
                            $imponibile_iva += ($prezzo_riga - ($good['complimentaryQuantity']*$good['prezzo']));
                        }else{
                            $imponibile_iva += $prezzo_riga;

                        }

                        isset($newVat['Iva']['percentuale']) ? $importo_iva += ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;

                        // Ho messo nullable il calcolo iva
                        isset($newVat['Iva']['percentuale']) ? $calcoloIva = ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;

                        if (isset($good['iva_id'])) {
                            isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                            if($good['complimentaryQuantity'] > 0){
                                isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += ($prezzo_riga - ($good['complimentaryQuantity']*$good['prezzo'])) : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;
                            }else{
                                isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;
                            }
                        }

                        // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                        isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                        if (isset($good['withholdingapplied'])) {
                            if ($good['withholdingapplied'] == 0) {
                                if ($good['quantita'] != 0) {
                                    $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                                }
                            }
                        }
                    }
                    else{
                        if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                            $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                        } else {
                            $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                        }
                        isset($newVat['Iva']['percentuale']) ? $importo_iva += ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;
                        $imponibile_iva += $prezzo_riga;

                        // Ho messo nullable il calcolo iva
                        isset($newVat['Iva']['percentuale']) ? $calcoloIva = ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;

                        if (isset($good['iva_id'])) {
                            isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                            isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;
                        }

                        // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                        isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                        if (isset($good['withholdingapplied'])) {
                            if ($good['withholdingapplied'] == 0) {
                                if ($good['quantita'] != 0) {
                                    $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                                }
                            }
                        }
                    }


                }

                /** INIZIO BLOCCO DA COPIARE **/
                isset($this->request->data['Bill']['collectionfees']) ? $collectionFees = $this->request->data['Bill']['collectionfees'] : $collectionFees = 0;


                if ($this->request->data['Bill']['collectionfeesvatid'] > 0) {
                    $collectionfeesvat = $this->Utilities->getVatPercentageFromId($this->request->data['Bill']['collectionfeesvatid']);
                    $collectionfeesvat = $collectionFees * ($collectionfeesvat / 100);
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                    $importo_iva += $collectionfeesvat;
                } else {

                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                }


                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    // $welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                // Bolli
                isset($this->request->data['Bill']['seal']) ? $seal = $this->request->data['Bill']['seal'] : $seal = 0;
                // Arrotondamento
                (isset($this->request->data['Bill']['rounding']) && $this->request->data['Bill']['rounding'] != null) ? $rounding = $this->request->data['Bill']['rounding'] : $rounding = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($this->request->data['Bill']['withholding_tax'])) {
                    if ($this->request->data['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $this->request->data['Bill']['withholding_tax'] * $imponibile_iva / 100;

                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if (isset($newBill['Bill']['welfare_box_withholding_appliance'])) {
                            if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                                $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($this->request->data['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                            }
                        }

                        // Risoluzione ritenuta d'acconto (RACC)
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }
                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // SEAL OLD VS SEAL NEW
                if (strtotime(date("Y-m-d", strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                } else {
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
                }

                /** FINE BLOCCO DA COPIARE **/

                $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

                // Recupero se è abilitato il flag agosto dicembre
                $enableAugustDecember = $this->Utilities->getDefaultClientDeadlineAugustSeptemberFlag($this->request->data['Bill']['client_id']);
                // Scadenze
                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $newBill['Bill']['id'], $enableAugustDecember);

                if ($newBill['Bill']['tipologia'] == 1) {
                    $this->Utilities->updateBillSectional($newBill['Bill']['id']);
                }

                if ($newBill['Bill']['tipologia'] == 8) {
                    $this->Utilities->updateProformaSectional($newBill['Bill']['id']);
                }

                $this->Session->setFlash(__('La fattura è stata salvata'), 'custom-flash');
                $this->redirect(['action' => $redirect]);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare la fattura, riprovare'), 'custom-danger');
            }
        }

        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            $this->set('deposits', $this->Utilities->getDepositsList());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        }
        else
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->Setting->getCompanyWithholdingTax() > 0 ? $withholdingtax = $this->Setting->getCompanyWithholdingTax() : $withholdingtax = '';

        $this->set('companywithholdingtax', $withholdingtax);
        $this->set('units', $this->Units->getList());
        $this->set('clients', $this->Client->getAdvancedList());
        $this->set('carriers', $this->Utilities->getCarriersList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('einvoiceVatEsigibility', $this->Utilities->getEinvoiceVatEsigibility());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('settings', $this->Setting->GetMySettings());
        $this->set('tipologia', $tipologia);
        $this->set('causali', $this->Utilities->getCausals('clienti'));
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('banks', $this->Bank->getBanksListWithAbiAndCab());
        $this->set('redirect', $redirect);

        $this->Render('add');
    }

    // Fatture d'acquisto
    public function addExtendedbuy()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Deadline', 'Ivas', 'Suppliers', 'Units']);
        $this->set('n_fatt', $this->Utilities->getNextBillNumber(date("Y"), '2'));

        if ($this->request->is(['post', 'put'])) {
            $fornitore = $this->Suppliers->find('first', ['conditions' => ['Suppliers.name' => $this->request->data['Bill']['supplier_id'], 'Suppliers.company_id' => MYCOMPANY], 'fields' => ['Suppliers.id']]);
            $this->request->data['Bill']['company_id'] = MYCOMPANY;

            // Se il cliente non esiste allora lo creo
            if (empty($fornitore['Suppliers']['id'])) {
                $supplierAddress = $this->request->data['Bill']['supplier_address'];
                $supplierCap = $this->request->data['Bill']['supplier_cap'];
                $supplierCity = $this->request->data['Bill']['supplier_city'];
                $supplierProvince = $this->request->data['Bill']['supplier_province'];
                isset($this->request->data['Bill']['supplier_nation']) ? $supplierNation = $this->request->data['Bill']['supplier_nation'] : $supplierNation = null;
                // Setto il nome utente
                $this->request->data['Bill']['supplier_name'] = $this->request->data['Bill']['supplier_id']; // In realta client_id è la ragsociale
                // Poi client id diventa id
                $this->request->data['Bill']['supplier_id'] = $this->Utilities->createSupplier($this->request->data['Bill']['supplier_id'], $supplierAddress, $supplierCap, $supplierCity, $supplierProvince, $supplierNation);
            } else {
                // Setto il nome utente
                $this->request->data['Bill']['supplier_name'] = $this->request->data['Bill']['supplier_id']; // In realta client_id è la ragsociale
                // Poi client id diventa id
                $this->request->data['Bill']['supplier_id'] = $fornitore['Suppliers']['id'];
            }

            $time = strtotime($this->request->data['Bill']['date']);
            $this->request->data['Bill']['date'] = date("Y-m-d", $time);
            $this->request->data['Bill']['receive_date'] = date("Y-m-d", $time);
            $newBill = $this->Bill->create();
            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];
                $numberOfMovement = 0;
                foreach ($this->request->data['Good'] as $key => $good) {
                    $numberOfMovement++;
                    $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);
                    $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                    if (isset($newVat['Iva']['percentuale'])) {
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                    }
                    $imponibile_iva += $prezzo_riga;

                    if (isset($newVat['Iva']['percentuale'])) {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                    }
                    isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                        if ($good['storage_id'] != '') {
                            $theStorageId = $good['storage_id'];
                            $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico Fattura acquisto n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $good['prezzo'],1 , $numberOfMovement, 'SC_AD');
                        }
                    }

                    if (!$this->Utilities->isANote($good, 'bill')) {
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            $newStorage = $this->Utilities->createNewStorageFromGood($good, null, $newBill['Bill']['supplier_id']);
                            $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                        }
                    }

                    // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                    if (isset($good['withholdingapplied'])) {
                        if ($good['withholdingapplied'] == 0) {
                            if ($good['quantita'] != 0) {
                                $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                            }
                        }
                    }
                }

                /** INIZIO BLOCCO DA COPIARE **/
                isset($this->request->data['Bill']['collectionfees']) ? $collectionFees = $this->request->data['Bill']['collectionfees'] : $collectionFees = 0;


                if ($this->request->data['Bill']['collectionfeesvatid'] > 0) {
                    $collectionfeesvat = $this->Utilities->getVatPercentageFromId($this->request->data['Bill']['collectionfeesvatid']);
                    $collectionfeesvat = $collectionFees * ($collectionfeesvat / 100);
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                    $importo_iva += $collectionfeesvat;
                } else {
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                };


                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    // $welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                isset($this->request->data['Bill']['seal']) ? $seal = $this->request->data['Bill']['seal'] : $seal = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($this->request->data['Bill']['withholding_tax'])) {
                    if ($this->request->data['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $this->request->data['Bill']['withholding_tax'] * $imponibile_iva / 100;
                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if (isset($newBill['Bill']['welfare_box_withholding_appliance'])) {
                            if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                                $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($this->request->data['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                            }
                        }

                        // Risoluzione ritenuta d'acconto
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }

                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // SEAL OLD VS SEAL NEW !! NON CE LO SPLIT PAYMENT
                if (strtotime(date("Y-m-d", strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                    $totalForDeadline = $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                } else {
                    $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
                }

                /** FINE BLOCCO DA COPIARE **/

                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], 0, false);
                $this->Session->setFlash(__('La fattura è stata salvata'), 'custom-flash');
                $this->redirect(['action' => 'indexExtendedbuy']);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare la fattura, riprovare'), 'custom-danger');
            }
        }

        // $this->set('units',$this->Utilities->getUnitsList());
        $this->set('units', $this->Units->getList());
        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());

        if (ADVANCED_STORAGE_ENABLED) {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        } else {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->Render('add_extendedbuy');
    }

    /** AGGIUNTA NOTE DI CREDITO **/
    public function addExtendedcreditnotes()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Storage', 'Deadline', 'Ivas', 'Units', 'Currencies']);

        // Sezionali
        $defaultSectional = $this->Utilities->getDefaultCreditnoteSectional();
        $this->set('defaultSectional', $defaultSectional);

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $this->set('nextCreditNoteNumber', $nextSectionalNumber);
        $this->set('sectionals', $this->Utilities->getCreditnoteSectionalList());

        if ($this->request->is(['post', 'put'])) {
            $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Bill']['client_id'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id', 'Client.currency_id']]);
            $this->request->data['Bill']['company_id'] = MYCOMPANY;

            // Se il cliente non esiste allora lo creo
            if (empty($cliente['Client']['id'])) {
                $clientAddress = $this->request->data['Bill']['client_address'];
                $clientCap = $this->request->data['Bill']['client_cap'];
                $clientCity = $this->request->data['Bill']['client_city'];
                $clientProvince = $this->request->data['Bill']['client_province'];
                isset($this->request->data['Bill']['client_nation']) ? $clientNation = $this->request->data['Bill']['client_nation'] : $clientNation = null;
                // Setto il nome utente
                $this->request->data['Bill']['client_name'] = $this->request->data['Bill']['client_id']; // In realta client_id è la ragsociale
                // Poi client id diventa id
                $this->request->data['Bill']['client_id'] = $this->Utilities->createClient($this->request->data['Bill']['client_id'], $clientAddress, $clientCap, $clientCity, $clientProvince, $clientNation);
                // La definizione della currencies (default EUR)
                $this->request->data['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');
                $this->request->data['Bill']['changevalue'] = 1;
            } else {
                // Setto il nome utente
                $this->request->data['Bill']['client_name'] = $this->request->data['Bill']['client_id']; // In realta client_id è la Ragione sociale
                // Poi client id diventa id
                $this->request->data['Bill']['client_id'] = $cliente['Client']['id'];
                // Salvo valuta sulla fattura
                $this->request->data['Bill']['currency_id'] = $cliente['Client']['currency_id'];
            }

            // Aggiungo valori per cassa previdenziale ( Nell'edit non vado a modificarla per ora )
            /*	$this->request->data['Bill']['welfare_box_code'] =  $settings['Setting']['welfare_box_code'];
			$this->request->data['Bill']['welfare_box_vat_id'] = $settings['Setting']['welfare_box_vat_id'];
			$this->request->data['Bill']['welfare_box_percentage'] = $settings['Setting']['welfare_box_percentage'] ;
			$this->request->data['Bill']['welfare_box_withholding_appliance'] = $settings['Setting']['welfare_box_withholding_appliance'];*/

            $this->request->data['Bill']['date'] = date("Y-m-d", strtotime($this->request->data['Bill']['date']));
            $this->request->data['Bill']['bill_referred_date'] = date("Y-m-d", strtotime($this->request->data['Bill']['bill_referred_date']));
            if (isset($this->request->data['Bill']['billaccdate'])) {
                $this->request->data['Bill']['billaccdate'] = date("Y-m-d", strtotime($this->request->data['Bill']['billaccdate']));
            }

            $this->request->data['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

            $newBill = $this->Bill->create();
            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {
                // Setto a zero i valori per i calcoli delle scadenze
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];

                foreach ($this->request->data['Good'] as $key => $good) {
                    $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

                    // Calcolo i totali
                    if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                    } else {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                    }

                    if (isset($newVat['Iva']['percentuale'])) {
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                    }
                    $imponibile_iva += $prezzo_riga;
                    if (isset($newVat['Iva']['percentuale'])) {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                    }
                    isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

                    if (!$this->Utilities->isANote($good, 'bill')) {
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                            $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                        }
                    }

                    // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                    if (isset($good['withholdingapplied'])) {
                        if ($good['withholdingapplied'] == 0) {
                            if ($good['quantita'] != 0) {
                                $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                            }
                        }
                    }
                }


                /** INIZIO BLOCCO DA COPIARE **/
                // Non metto le spese d'incasso sulla nota di credito ?
                /*isset($this->request->data['Bill']['collectionfees']) ?  $collectionFees = $this->request->data['Bill']['collectionfees'] : $collectionFees = 0;


				if($this->request->data['Bill']['collectionfeesvatid'] > 0)
				{
					$collectionfeesvat = $this->Utilities->getVatPercentageFromId($this->request->data['Bill']['collectionfeesvatid']) ;
				 	$collectionfeesvat = 	$collectionFees * ($collectionfeesvat/100) ;
				 	$imponibile_iva  += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
					$importo_iva += $collectionfeesvat;
				}
				else
				{
			  		$imponibile_iva  += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
				};*/

                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    //$welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;

                // Arrotondamento
                (isset($this->request->data['Bill']['rounding']) && $this->request->data['Bill']['rounding'] != null) ? $rounding = $this->request->data['Bill']['rounding'] : $rounding = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($newBill['Bill']['withholding_tax'])) {
                    if ($newBill['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $newBill['Bill']['withholding_tax'] * $imponibile_iva / 100;
                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                            $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($newBill['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                        }

                        // Risoluzione ritenuta d'acconto
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }
                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // SEAL OLD VS SEAL NEW !! NON CE LO SPLIT PAYMENT
                if (strtotime(date("Y-m-d", strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                    // $totalForDeadline = $importo_iva + $imponibile_iva   + $seal - $totaleRitenutaAcconto;
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                } else {
                    // $totalForDeadline = $importo_iva + $imponibile_iva   - $totaleRitenutaAcconto ;
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
                }


                $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

                /** FINE BLOCCO DA COPIARE **/
                // Note di credito non ha ne bolli ne ritenuta d'acconto
                // Nelle note di credit non sposto le scadenze 31/08 e 31/12
                //	$totale = $importo_iva + $imponibile_iva;
                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], 0, false);

                // Aggiorno il sezionale
                $this->Utilities->updateCreditNoteSectional($newBill['Bill']['id']);

                $this->Session->setFlash(__('La nota di credito è stata salvata'), 'custom-flash');
                $this->redirect(['action' => 'indexExtendedcreditnotes']);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare la nota di credito, riprovare'), 'custom-danger');
            }
        }

        $this->set('units', $this->Units->getList());
        $this->set('clients', $this->Client->getAdvancedList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('einvoiceVatEsigibility', $this->Utilities->getEinvoiceVatEsigibility());
        $this->set('settings', $this->Setting->GetMySettings());
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());

        if (ADVANCED_STORAGE_ENABLED)
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        else
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

        $this->Render('add_extendedcreditnotes');
    }

    /** AGGIUNTA SCONTRINI - ATTENZIONE L'IVA VIENE SCORPORATA  **/
    public function addReceipt()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Deadline', 'Ivas', 'Units', 'Currencies']);
        $this->set('n_fatt', $this->Utilities->getNextReceiptNumber(date("Y-m-d")));

        if ($this->request->is(['post', 'put'])) {
            $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Bill']['client_id'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id', 'Client.currency_id']]);
            $this->request->data['Bill']['company_id'] = MYCOMPANY;

            if (empty($cliente['Client']['id'])) {
                $this->request->data['Bill']['client_id'] = $this->Utilities->createClient($this->request->data['Bill']['client_id'], null, null, null, null, null);
                // La definizione della currencies (default EUR)
                $this->request->data['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');
                $this->request->data['Bill']['changevalue'] = 1;
            } else {
                // Setto il nome utente
                $this->request->data['Bill']['client_name'] = $this->request->data['Bill']['client_id']; // In realta client_id è la ragsociale

                $this->request->data['Bill']['client_id'] = $cliente['Client']['id'];

                // Salvo valuta sulla fattura
                $this->request->data['Bill']['currency_id'] = $cliente['Client']['currency_id'];
            }

            $time = strtotime($this->request->data['Bill']['date']);
            $this->request->data['Bill']['date'] = date("Y-m-d", $time);

            $newBill = $this->Bill->create();


            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                $taxableIncome = $vat = 0;

                $numberOfMovement = 0;

                foreach ($this->request->data['Good'] as $key => $good) {
                    $numberOfMovement++;

                    // Crea il nuovo articolo dello scontrino
                    $newBillGood = $this->Utilities->createReceiptGood($newBill['Bill']['id'], $good);

                    // Il totale scontrino è il prezzo * riga
                    $vat += number_format($this->Utilities->calculateVatIsolationFromVatId($good['prezzo'], $good['quantita'], $good['iva_id']), 2);
                    $taxableIncome += $this->Utilities->calculateRowPrice($good['quantita'], $good['prezzo']) - $vat;

                    if ($good['storage_id'] != '') {
                        $theStorageId = $good['storage_id'];
                        // Scarico di magazzino
                        if (ADVANCED_STORAGE_ENABLED) {
                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), 1, $numberOfMovement, 'SC_AD');
                        } else {
                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'SC_AD');
                        }
                    } else {
                        $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                        $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                        $theStorageId = $newStorage['Storage']['id'];
                        if (ADVANCED_STORAGE_ENABLED) {
                            if (isset($newStorage['Storage']['id'])) {
                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId),1 , $numberOfMovement, 'SC_AD');
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'SC_AD');
                            }
                        } else {
                            if (isset($newStorage['Storage']['id'])) {
                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'SC_AD');
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico   (oggetto nuovo di magazzino) scontrino. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'SC_AD');
                            }
                        }
                    }
                }

                // Emetto lo scontrino - lo scontrino pagano le scadenze immediate quindi anche al 31/8 e 31/12
                //$this->Utilities->setDeadlinesWithVat($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $taxableIncome, $vat, $newBill['Bill']['id'], false);
                $this->Session->setFlash(__('Lo scontrino è stata salvata'), 'custom-flash');
                $this->redirect(['action' => 'indexReceipt']);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare lo scontrino, riprovare'), 'custom-danger');
            }
        }

        // $this->set('units',$this->Utilities->getUnitsList());
        $this->set('units', $this->Units->getList());
        $this->loadModel('Client');
        $this->set('clients', $this->Client->getList());

        if (ADVANCED_STORAGE_ENABLED) {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'true'));
        } else {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('magazziniList', $this->Utilities->getStoragesList());
        $this->set('payments', $this->Utilities->getPaymentsList(0));
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('deposits', $this->Utilities->getDepositsList());
        $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        $this->set('defaultReceiptPaymentMethod', $this->Utilities->getDefaultReceiptPaymentMethodId());
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());

        $this->Render('add_receipt');
    }

    /** FINE AGIGUNTA SCONTRINI **/
    public function editproforma($id = null)
    {
        $this->edit($id, 8); // Proforma
        $this->Render('edit');
    }

    public function edit($id = null, $tipologia = 1)
    {
        ini_set('max_execution_time', 600);
        set_time_limit(600);

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Storage', 'Deadline', 'Goods', 'Ivas', 'Units', 'Clientdestination', 'Setting', 'Currencies', 'Bank']);

        $this->Utilities->setMenuAsActive("Fatture"); // Fix per il menu

        $this->Bill->id = $id;

        if (!$this->Bill->exists())
            throw new NotFoundException(__('Fattura non valida'));

        if ($tipologia == 1)
        {
            $redirect = 'index';
            $this->set('sectionals', $this->Utilities->getBillSectionalList());
        }

        if ($tipologia == 8)
        {
            $redirect = 'indexproforma';
            $this->set('sectionals', $this->Utilities->getProformaSectionalList());
        }

        if ($this->request->is('post') || $this->request->is('put') || $this->request->is('patch'))
        {
            $old_date = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

            if ($_POST['data']['Bill']['date'] == '' || $_POST['data']['Bill']['date'] == '01-01-1970')
                $this->request->data['Bill']['date'] = $old_date['Bill']['date'];
            else
                $this->request->data['Bill']['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['date'])));

            /** INIZIA MODIFICA CLIENTE */

            // Controllo ragione sociale se viene cambiato cliente
            if ($_POST['data']['Bill']['client_id'] != $old_date['Bill']['client_id'])
                $this->request->data['Bill']['client_name'] = $this->Utilities->getCustomerName($_POST['data']['Bill']['client_id']);

            /** FINE MODIFICA CLIENTE */

            if (isset($_POST['data']['Bill']['billaccdate']))
            {
                if ($_POST['data']['Bill']['billaccdate'] == '' || $_POST['data']['Bill']['billaccdate'] == '01-01-1970')
                    $this->request->data['Bill']['billaccdate'] = $old_date['Bill']['billaccdate'];
                else
                    $this->request->data['Bill']['billaccdate'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['billaccdate'])));
            }

            if (isset($_POST['data']['Bill']['mainrifddtdate']))
            {
                if ($_POST['data']['Bill']['mainrifddtdate'] == '' || $_POST['data']['Bill']['mainrifddtdate'] == '01-01-1970')
                    $this->request->data['Bill']['mainrifddtdate'] = $old_date['Bill']['mainrifddtdate'];
                else
                    $this->request->data['Bill']['mainrifddtdate'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['mainrifddtdate'])));
            }

            isset($this->request->data['Bill']['cig_date']) ? $this->request->data['Bill']['cig_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Bill']['cig_date']))) : null;

            // Recupero i vecchi articoli della bolla
            $oldGood = $this->Goods->find('all', ['conditions' => ['company_id' => MYCOMPANY, 'bill_id' => $id]]);
            // Se è selezionato alternative address lo salvo come destino
            if (isset($this->request->data['Bill']['alternativeaddress_id']))
            {
                $shippingAddress = $this->Clientdestination->find('first', ['conditions' => ['Clientdestination.id' => $this->request->data['Bill']['alternativeaddress_id']]]);

                if (count($shippingAddress) > 0)
                {
                    $this->request->data['Bill']['client_shipping_name'] = $shippingAddress['Clientdestination']['name'];
                    $this->request->data['Bill']['client_shipping_address'] = $shippingAddress['Clientdestination']['address'];
                    $this->request->data['Bill']['client_shipping_cap'] = $shippingAddress['Clientdestination']['cap'];
                    $this->request->data['Bill']['client_shipping_city'] = $shippingAddress['Clientdestination']['city'];
                    $this->request->data['Bill']['client_shipping_province'] = $shippingAddress['Clientdestination']['province'];

                    if (isset($shippingAddress['Clientdestination']['nation_id']))
                        $this->request->data['Bill']['client_shipping_nation'] = $this->Utilities->getNationName($shippingAddress['Clientdestination']['nation_id'])['Nation']['name'];
                }
            }

            $settings = $this->Setting->GetMySettings();

            // Aggiorno lo stato ad 1 lo utilizzo per il duplica che di default è 0
            $this->request->data['Bill']['state'] = 1;

            if ($newBill = $this->Bill->save($this->request->data['Bill']))
            {
                $newBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $newBill['Bill']['id']]]);

                $newBill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

                // Per ogni articolo vecchio scarico da magazzino
                $numberOfMovement = 0;

                foreach ($oldGood as $oldBillGood)
                {
                    $numberOfMovement++;
                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0050" && $newBill['Bill']['tipologia'] == 8){
                    $theStorageId = $oldBillGood['storage_id'];
                    if (ADVANCED_STORAGE_ENABLED) {
                        $this->Utilities->storageUnload($theStorageId, $oldBillGood['quantita'], 'Scarico fattura pro forma n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                    } else {
                        $this->Utilities->storageUnload($theStorageId, $oldBillGood['quantita'], 'Scarico fattura pro forma n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                    }
                }
                    // Se è una fattura ed è accompagnatoria
                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                        if($newBill['Bill']['tipologia'] == 1 ){
                            /*if ($oldBillGood['Goods']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                            {
                                if (ADVANCED_STORAGE_ENABLED) {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];
                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], '[BC1598] Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    }
                                } else {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];
                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], '[BC1602] Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', 0, 1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                            }*/ if ($oldBillGood['Goods']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                            {
                                if (ADVANCED_STORAGE_ENABLED) {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];
                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], 'Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    }
                                } else {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];
                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], 'Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', 0, 1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                            }

                        }else if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 0) {
                            if ($oldBillGood['Goods']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                            {
                                if (ADVANCED_STORAGE_ENABLED) {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];
                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], '[BC1598] Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    }
                                } else {
                                    if (isset($oldBillGood['Goods']['storage_id'])) {
                                        $theStorageId = $oldBillGood['Goods']['storage_id'];

                                        $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], '[BC1602] Carico fattura  n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', 0, 1, $numberOfMovement, 'BI_ED');

                                    }
                                }
                            }
                        }}
                    // Aggiorno i vecchi valore
                    $this->Good->delete($oldBillGood['Goods']['id']);
                }

                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];


                foreach ($this->request->data['Good'] as $key => $good) {
                    $numberOfMovement++;
                    $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);

                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

                    if (!isset($good['discount']))
                        $good['discount'] = 0;

                    if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null)
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                    else
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);

                    if (isset($newVat['Iva']['percentuale']))
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);

                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047")
                        $good['complimentaryQuantity'] > 0 ? $imponibile_iva += $prezzo_riga - ($good['complimentaryQuantity']*$good['prezzo']) : $imponibile_iva += $prezzo_riga;
                    else
                        $imponibile_iva += $prezzo_riga;

                    if (isset($newVat['Iva']['percentuale'])) {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                    }

                    isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

                    // Il carico e scarico lo faccio solo se è presente nel magazzino

                    if ($good['storage_id'] != '') {
                        // Se esiste l'oggetto (potrei aver cambiato la descrizione quindi è un controllo aggiuntivo)
                        if ($this->Storage->find('count', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.id' => $good['storage_id']]]) > 0) {
                            // Se è una fattura ed è accompagnatoria
                            if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                                if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1) {
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        $theStorageId = $good['storage_id'];
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    } else {
                                        $theStorageId = $good['storage_id'];
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                                else if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 0) {
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        $theStorageId = $good['storage_id'];
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    } else {
                                        $theStorageId = $good['storage_id'];
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                            }
                        }
                    } else {
                        if (!$this->Utilities->isANote($good, 'bill')) {
                            if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                                $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                                if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                                    if ($newBill['Bill']['tipologia'] == 1) {
                                        if (ADVANCED_STORAGE_ENABLED) {
                                            if (isset($newStorage['Storage']['id'])) {
                                                $theStorageId = $newStorage['Storage']['id'];
                                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BE_AD');
                                            }
                                        } else {
                                            if (isset($newStorage['Storage']['id'])) {
                                                $theStorageId = $newStorage['Storage']['id'];
                                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                            }
                                        }
                                    }
                                }else if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1) {
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        if (isset($newStorage['Storage']['id'])) {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BE_AD');
                                        }
                                    } else {
                                        if (isset($newStorage['Storage']['id'])) {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                        }
                                    }
                                }
                            } else  // altrimenti scarico
                            {
                                $currentGood = $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'descrizione' => $good['oggetto']]]);
                                $this->Good->updateAll(['Good.storage_id' => $currentGood['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $good['id']]);
                                if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                                    $numberOfMovement++;
                                    if ($newBill['Bill']['tipologia'] == 1&& $newBill['Bill']['accompagnatoria'] == 0)  // fattura accompagnatoria
                                    {
                                        $theStorageId = $storageId;
                                        if (ADVANCED_STORAGE_ENABLED) {
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                        } else {
                                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                                        }
                                    }
                                }else if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1)  // fattura accompagnatoria
                                {
                                    $theStorageId = $storageId;
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    } else {
                                        $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                            }
                        }
                    }

                    // bolli
                    isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;
                    // Arrotondamento
                    isset($newBill['Bill']['rounding']) && $newBill['Bill']['rounding'] != null ? $rounding = $newBill['Bill']['rounding'] : $rounding = 0;

                    // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;

                    if (isset($good['withholdingapplied'])) {
                        if ($good['withholdingapplied'] == 0) {
                            if ($good['quantita'] != 0)
                                $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                        }
                    }
                }

                isset($newBill['Bill']['collectionfees']) ? $collectionFees = $newBill['Bill']['collectionfees'] : $collectionFees = 0;

                if ($newBill['Bill']['collectionfeesvatid'] > 0) {
                    $collectionfeesvat = $this->Utilities->getVatPercentageFromId($newBill['Bill']['collectionfeesvatid']);
                    $collectionfeesvat = $collectionFees * ($collectionfeesvat / 100);
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                    $importo_iva += $collectionfeesvat;
                } else {
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                }

                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($newBill['Bill']['withholding_tax'])) {
                    if ($newBill['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $newBill['Bill']['withholding_tax'] * $imponibile_iva / 100;
                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                            $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($newBill['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                        }

                        // Risoluzione ritenuta d'acconto
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }
                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // Totale fattura nel caso di split payment o no

                // SEAL OLD VS SEAL NEW

                $importo_iva = number_format($importo_iva, 2, '.', '');
                $imponibile_iva = number_format($imponibile_iva, 2, '.', '');

                if (strtotime(date("Y-m-d", strtotime($newBill['Bill']['date']))) < strtotime(date('2019-01-01')))
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                else
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;

                $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

                // Recupero se è abilitato il flag agosto dicembre
                $enableAugustDecember = $this->Utilities->getDefaultClientDeadlineAugustSeptemberFlag($newBill['Bill']['client_id']);

                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $id, $enableAugustDecember);

                $this->Session->setFlash(__('Fattura salvata correttamente'), 'custom-flash');
                $this->Utilities->setBillMailNotSent($id);
                $this->redirect(['action' => $redirect]);
            } else {
                $this->Session->setFlash(__('La fattura non è stata salvata correttamente.'), 'custom-danger');
            }
        }
        else
        {
            $this->request->data = $this->Bill->find('first', ['contain' => ['Client' => ['Bank'], 'Good' => ['Iva'], 'Deposit'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);
        }

        $bills = $this->Bill->find('first', ['contain' => ['Client' => ['Bank'], 'Good' => ['Iva', 'Storage'], 'Deposit'], 'recursive' => 3, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        $this->set('bills', $bills);
        $this->set('clients', $this->Client->getList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('units', $this->Units->getList());
        $this->set('einvoiceVatEsigibility', $this->Utilities->getEinvoiceVatEsigibility());
        $this->set('myCompany', MYCOMPANY);
        $this->set('getAviableQuantity', $this->Utilities);
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('carriers', $this->Utilities->getCarriersList());
        $this->set('settings', $this->Setting->GetMySettings());
        $this->set('tipologia', $tipologia);
        $this->set('currencyName', $this->Currencies->getCurrencyName($bills['Bill']['currency_id']));
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('banks', $this->Bank->getBanksListWithAbiAndCab());
        $this->set('redirect', $redirect);

        if (ADVANCED_STORAGE_ENABLED)
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        else
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

        // Se è selezionato alternative address lo salvo come destino
        $arrayOfAlternativeAddress = [];
        $alternativeAddresses = $this->Utilities->getCustomerDifferentAddress($bills['Bill']['client_id']);
        foreach ($alternativeAddresses as $alternativeAddress)
        {
            isset($alternativeAddress['Clientdestination']['nation_id']) ? $nationName = ' - ' . $this->Utilities->getNationName($alternativeAddress['Clientdestination']['nation_id'])['Nation']['name'] : $nationName = '';
            $arrayOfAlternativeAddress[$alternativeAddress['Clientdestination']['id']] = $alternativeAddress['Clientdestination']['name'] . ' - ' . $alternativeAddress['Clientdestination']['address'] . ' - ' . $alternativeAddress['Clientdestination']['cap'] . ' - ' . $alternativeAddress['Clientdestination']['city'] . ' - ' . $alternativeAddress['Clientdestination']['province'] . $nationName;
        }

        $this->set('alternativeAddress', $arrayOfAlternativeAddress);

        // START CONTROLLARE POTREBBE ESERE CODICE OBSOLETO
        $arrayOfReferredAddress = [];
        $referredClientAddresses = $this->Utilities->getResellerClient($bills['Bill']['client_id']);

        foreach ($referredClientAddresses as $referredClientAddress)
            $arrayOfReferredAddress[$referredClientAddress['Client']['id']] = $referredClientAddress['Client']['ragionesociale'] . ' - ' . $referredClientAddress['Client']['indirizzo'] . ' - ' . $referredClientAddress['Client']['cap'] . ' - ' . $referredClientAddress['Client']['citta'] . ' - ' . $referredClientAddress['Client']['provincia'];

        $this->set('referredClientAddress', $arrayOfReferredAddress);
        // FINE

        $causals = $this->Utilities->getCausals();
        unset($causals[3]);
        $this->set('causali', $causals);

        // Se avanza
        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('deposits', $this->Utilities->getDepositsList());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        }

        $this->Render('edit');
    }

    public function editExtendedbuy($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Goods', 'Units']);

        // Fix per il menu
        $this->Utilities->setMenuAsActive("Fatture");

        !empty($this->params['pass'][0]) ? $this->Bill->id = $this->params['pass'][0] : $this->Bill->id = $id;

        if (!$this->Bill->exists()) {
            throw new NotFoundException(__('Fattura non valida'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $old_date = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
            if (empty($this->request->data['Bill']['date'])) {
                if(!isset($old_date['Bill']['id_ixfe']) || $old_date['Bill']['id_ixfe'] == null)
                    $this->request->data['Bill']['receive_date'] = $old_date['Bill']['date'];
                $this->request->data['Bill']['date'] = $old_date['Bill']['date'];
            } else {
                $time = strtotime($this->request->data['Bill']['date']);
                if(!isset($old_date['Bill']['id_ixfe']) || $old_date['Bill']['id_ixfe'] == null)
                    $this->request->data['Bill']['receive_date'] = date("Y-m-d", $time);
                $this->request->data['Bill']['date'] = date("Y-m-d", $time);
            }

            // Aggiorno lo stato ad 1 lo utilizzo per il duplica che di default è 0
            $this->request->data['Bill']['state'] = 1;

            /** INIZIA MODIFICA FORNITORE */

            // Controllo ragione sociale se viene cambiato cliente
            $this->loadModel('Supplier');
            if ($_POST['data']['Bill']['supplier_id'] != $old_date['Bill']['supplier_id']) {
                $this->request->data['Bill']['supplier_name'] = $this->Supplier->getSupplierName($_POST['data']['Bill']['supplier_id']);
            }

            /** FINE MODIFICA FORNITORE */

            // Recupero i vecchi articoli della fattura di acquisto
            $oldGoods = $this->Goods->find('all', ['conditions' => ['company_id' => MYCOMPANY, 'bill_id' => $id]]);
            $numberOfMovement = 0;
            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {

                foreach ($oldGoods as $oldBillGood) {

                    $numberOfMovement++;

                    // Aggiorno i vecchi valore
                    $this->Goods->delete($oldBillGood['Goods']['id']);
                }

                // Sostituisco con le nuove
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];
                $numberOfMovement = 0;

                foreach ($this->request->data['Good'] as $key => $good) {

                    /** CALCOLO TOTALI FATTURA **/
                    $numberOfMovement++;

                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

                    if (!isset($good['discount'])) {
                        $good['discount'] = 0;
                    }

                    if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                    } else {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                    }

                    if (isset($newVat['Iva']['percentuale'])) {
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                    }
                    $imponibile_iva += $prezzo_riga;


                    if (isset($newVat['Iva']['percentuale'])) {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                    }

                    isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;
                    /** FINE CALCOLO TOTALE FATTURA */

                    if ($oldGood = $this->Goods->find('first', ['conditions' => ['Goods.id' => $good['id']]])) {   // Se è un vecchio articolo lo aggiorno
                        $this->Utilities->setBillGoodValues($oldGood, $newBill['Bill']['id'], $good);
                    } else {
                        $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
                    }

                    if (!$this->Utilities->isANote($good, 'bill')) {
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            $newStorage = $this->Utilities->createNewStorageFromGood($good, null, $newBill['Bill']['supplier_id']);
                            $this->Goods->updateAll(['Goods.storage_id' => $newStorage['Storage']['id'], 'Goods.company_id' => MYCOMPANY], ['Goods.id' => $good['id']]);
                        }
                    }

                    // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                    if (isset($good['withholdingapplied'])) {
                        if ($good['withholdingapplied'] == 0) {
                            if ($good['quantita'] != 0) {
                                $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                            }
                        }
                    }
                }

                /** INIZIO BLOCCO DA COPIARE **/
                isset($this->request->data['Bill']['collectionfees']) ? $collectionFees = $this->request->data['Bill']['collectionfees'] : $collectionFees = 0;

                if ($this->request->data['Bill']['collectionfeesvatid'] > 0) {
                    $collectionfeesvat = $this->Utilities->getVatPercentageFromId($this->request->data['Bill']['collectionfeesvatid']);
                    $collectionfeesvat = $collectionFees * ($collectionfeesvat / 100);
                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                    $importo_iva += $collectionfeesvat;
                } else {

                    $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
                };

                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    // $welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                isset($this->request->data['Bill']['seal']) ? $seal = $this->request->data['Bill']['seal'] : $seal = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($this->request->data['Bill']['withholding_tax'])) {
                    if ($this->request->data['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $this->request->data['Bill']['withholding_tax'] * $imponibile_iva / 100;
                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                            $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($this->request->data['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                        }

                        // Risoluzione ritenuta d'acconto
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }
                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // SEAL OLD VS SEAL NEW
                if (strtotime(date("Y-m-d", strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                    $totalForDeadline = $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                } else {
                    $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
                }

                /** FINE BLOCCO DA COPIARE **/

                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $id, $enableAugustDecember = false);
                /* FINE SALVATAGGIO SCADENZE */

                $this->Session->setFlash(__('Fattura salvata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'indexExtendedbuy']);
            } else {
                $this->Session->setFlash(__('La fattura non è stata salvata correttamente.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Bill->find('first', ['contain' => ['Supplier', 'Good' => ['Iva']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);
        }

        $bills = $this->Bill->find('first', ['contain' => ['Supplier', 'Good' => ['Iva', 'Storage']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);

        $this->set('bills', $bills);
        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());

        if (ADVANCED_STORAGE_ENABLED) {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        } else {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('einvoiceVatEsigibility', $this->Utilities->getEinvoiceVatEsigibility());
        $this->set('units', $this->Units->getList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set(compact('clients', 'ivas', 'ivaSelect', 'payments'));
        $this->Render('edit_extendedbuy');
    }

    public function editExtendedcreditnotes($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Goods', 'Currencies', 'Units', 'Clientdestination']);

        !empty($this->params['pass'][0]) ? $this->Bill->id = $this->params['pass'][0] : $this->Bill->id = $id;

        if (!$this->Bill->exists())
            throw new NotFoundException(__('Nota di credito non valida'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $old_date = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
            $this->request->data['Bill']['bill_referred_date'] = date("Y-m-d", strtotime($this->request->data['Bill']['bill_referred_date']));

            if (empty($this->request->data['Bill']['date'])) {
                //$old_date = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
                $this->request->data['Bill']['date'] = $old_date['Bill']['date'];
            } else {
                $time = strtotime($this->request->data['Bill']['date']);
                $this->request->data['Bill']['date'] = date("Y-m-d", $time);
            }

            // Aggiorno lo stato ad 1 lo utilizzo per il duplica che di default è 0
            $this->request->data['Bill']['state'] = 1;

            /** INIZIA MODIFICA CLIENTE */

            // Controllo ragione sociale se viene cambiato cliente

            if ($_POST['data']['Bill']['client_id'] != $old_date['Bill']['client_id']) {
                $this->request->data['Bill']['client_name'] = $this->Utilities->getCustomerName($_POST['data']['Bill']['client_id']);
            }

            /** FINE MODIFICA CLIENTE */


            // Recupero i vecchi articoli della bolla
            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {
                $newBill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

                $oldGood = $this->Goods->deleteall(['company_id' => MYCOMPANY, 'bill_id' => $id]);

                /*foreach($this->request->data['Good'] as $key => $good)
				{
					if($oldGood = $this->Good->find('first',['conditions' => ['Good.id'=>$good['id']]]))
					{
						$this->Utilities->setBillGoodValues($oldGood,$newBill['Bill']['id'],$good);
					}
					else
					{
						$this->Utilities->createBillGood($newBill['Bill']['id'],$good);
					}
					if(!$this->Utilities->isANote($good,'bill'))
					{
						if($this->Utilities->notExistInStorage($good['storage_id']))
						{
							$newStorage = $this->Utilities->createNewStorageFromGood($good,$newBill['Bill']['client_id']);
							$this->Good->updateAll(['Good.storage_id'=>$newStorage['Storage']['id'],'Good.company_id'  => MYCOMPANY],['Good.id'=>$good['id']]);
						}
					}
				}*/

                /** START NEW EDIT NC **/
                $numberOfMovement = $importo_iva = $imponibile_iva = 0;
                foreach ($this->request->data['Good'] as $key => $good) {

                    $numberOfMovement++;
                    $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);

                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

                    if (!isset($good['discount'])) {
                        $good['discount'] = 0;
                    }

                    if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                    } else {
                        $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                    }

                    if (isset($newVat['Iva']['percentuale'])) {
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                    }
                    $imponibile_iva += $prezzo_riga;


                    if (isset($newVat['Iva']['percentuale'])) {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                    }

                    isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

                    // Il carico e scarico lo faccio solo se è presente nel magazzino

                    if (!$this->Utilities->isANote($good, 'bill')) {
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                            $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                        } else  // altrimenti scarico
                        {
                            $currentGood = $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'descrizione' => $good['oggetto']]]);
                            $this->Good->updateAll(['Good.storage_id' => $currentGood['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $good['id']]);
                        }
                    }

                    isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;

                    // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                    if (isset($good['withholdingapplied'])) {
                        if ($good['withholdingapplied'] == 0) {
                            if ($good['quantita'] != 0) {
                                $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                            }
                        }
                    }
                }

                isset($newBill['Bill']['collectionfees']) ? $collectionFees = $newBill['Bill']['collectionfees'] : $collectionFees = 0;

                $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019

                // Inizio : cassa previdenziale
                if (isset($newBill['Bill']['welfare_box_percentage'])) {
                    $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                    //$welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                    $imponibile_iva += $welfareBoxTotal;
                    $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
                }
                // Fine : cassa previdenziale

                // Bolli
                isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;
                // Arrotondamento
                (isset($newBill['Bill']['rounding']) && $newBill['Bill']['rounding'] != null) ? $rounding = $newBill['Bill']['rounding'] : $rounding = 0;

                //  Calcolo della ritenuta d'acconto
                if (isset($newBill['Bill']['withholding_tax'])) {
                    if ($newBill['Bill']['withholding_tax'] > 0) {
                        $totaleRitenutaAcconto = $newBill['Bill']['withholding_tax'] * $imponibile_iva / 100;
                        // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                        if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                            $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($newBill['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                        }

                        // Risoluzione ritenuta d'acconto
                        if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                            $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);
                        }
                    } else {
                        $totaleRitenutaAcconto = 0;
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }

                // Totale fattura nel caso di split payment o no

                // SEAL OLD VS SEAL NEW

                $importo_iva = number_format($importo_iva, 2, '.', '');
                $imponibile_iva = number_format($imponibile_iva, 2, '.', '');

                if (strtotime(date("Y-m-d", strtotime($newBill['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                    // $totalForDeadline = $importo_iva + $imponibile_iva   + $seal - $totaleRitenutaAcconto;
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
                } else {
                    // $totalForDeadline = $importo_iva + $imponibile_iva   - $totaleRitenutaAcconto;
                    $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
                }

                $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

                // Recupero se è abilitato il flag agosto dicembre
                $enableAugustDecember = $this->Utilities->getDefaultClientDeadlineAugustSeptemberFlag($newBill['Bill']['client_id']);

                $this->Utilities->setDeadlines($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $id, $enableAugustDecember);

                /** END NEW EDIT NC **/

                $this->Session->setFlash(__('Nota di credito salvata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'indexExtendedcreditnotes']);
            } else {
                $this->Session->setFlash(__('La nota di credito non è stata salvata correttamente.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Bill->find('first', ['contain' => ['Client' => ['Bank'], 'Good' => ['Iva']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);
        }

        $bills = $this->Bill->find('first', ['contain' => ['Client', 'Good' => ['Iva', 'Storage']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);

        if (ADVANCED_STORAGE_ENABLED)
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        else
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

        $alternativeAddress = null;

        if ($bills['Bill']['alternativeaddress_id'] != null && $bills['Bill']['alternativeaddress_id'] > 0) {
            $alternativeAddress = $this->Clientdestination->find('first', ['conditions' => ['Clientdestination.id' => $bills['Bill']['alternativeaddress_id']]]);
            $alternativeAddress = $alternativeAddress['Clientdestination']['name'] . ' - ' . $alternativeAddress['Clientdestination']['address'] . ' - ' . $alternativeAddress['Clientdestination']['cap'] . ' - ' . $alternativeAddress['Clientdestination']['city'] . ' - ' . $alternativeAddress['Clientdestination']['province'] . ' - ' . $alternativeAddress['Clientdestination']['nation_id'];
        }

        $this->set('alternativeAddress', $alternativeAddress);
        $this->set('bills', $bills);
        $this->set('clients', $this->Client->getList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('sectionals', $this->Utilities->getCreditnoteSectionalList());
        $this->set('units', $this->Units->getList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('einvoiceVatEsigibility', $this->Utilities->getEinvoiceVatEsigibility());
        $this->set('currencyName', $this->Currencies->getCurrencyName($bills['Bill']['currency_id']));
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('settings', $this->Setting->GetMySettings());

        $this->Render('edit_extendedcreditnotes');
    }

    public function editReceipt($id = null)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Goods');
        $this->loadModel('Units');
        !empty($this->params['pass'][0]) ? $this->Bill->id = $this->params['pass'][0] : $this->Bill->id = $id;

        if (!$this->Bill->exists())
            throw new NotFoundException(__('Scontrino non valida'));

        if ($this->request->is('post') || $this->request->is('put')) {
            if (empty($this->request->data['Bill']['date'])) {
                $old_date = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
                $this->request->data['Bill']['date'] = $old_date['Bill']['date'];
            } else {
                $time = strtotime($this->request->data['Bill']['date']);
                $this->request->data['Bill']['date'] = date("Y-m-d", $time);
            }


            // Recupero i vecchi articoli della bolla
            $oldGood = $this->Goods->find('all', ['conditions' => ['company_id' => MYCOMPANY, 'bill_id' => $id]]);


            if ($newBill = $this->Bill->save($this->request->data['Bill'])) {
                $vat = $taxableIncome = 0;

                $numberOfMovement = 0;

                // Elimino i vecchi articoli
                foreach ($oldGood as $oldBillGood) {
                    $numberOfMovement++;

                    // Se è una fattura ed è accompagnatoria
                    if ($oldBillGood['Goods']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                    {
                        if (ADVANCED_STORAGE_ENABLED) {
                            if (isset($oldBillGood['Goods']['storage_id'])) {
                                $theStorageId = $oldBillGood['Goods']['storage_id'];
                                $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], 'carico  scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'SC_ED');
                            }
                        } else {
                            if (isset($oldBillGood['Goods']['storage_id'])) {
                                $theStorageId = $oldBillGood['Goods']['storage_id'];
                                $this->Utilities->storageLoad($theStorageId, $oldBillGood['Goods']['quantita'], 'carico  scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])) . ' per modifica ', 0, -1, $numberOfMovement, 'SC_ED');
                            }
                        }
                    }

                    // Aggiorno i vecchi valore
                    $this->Good->delete($oldBillGood['Goods']['id']);
                }

                foreach ($this->request->data['Good'] as $key => $good) {

                    /** TO DO **/
                    if ($good['storage_id'] != '') {
                        // Se esiste l'oggetto (potrei aver cambiato la descrizione quindi è un controllo aggiuntivo)
                        if (ADVANCED_STORAGE_ENABLED) {
                            $theStorageId = $good['storage_id'];
                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                        } else {
                            $theStorageId = $good['storage_id'];
                            $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                        }
                    } else {
                        $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                        $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $good['id']]);
                        if (ADVANCED_STORAGE_ENABLED) {
                            if (isset($newStorage['Storage']['id'])) {
                                $theStorageId = $newStorage['Storage']['id'];
                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_AD');
                            }
                        } else {
                            if (isset($newStorage['Storage']['id'])) {
                                $theStorageId = $newStorage['Storage']['id'];
                                $this->Utilities->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                                $this->Utilities->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) scontrino n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_AD');
                            }
                        }
                    }
                    /** END TO DO **/

                    if ($oldGood = $this->Good->find('first', ['conditions' => ['Good.id' => $good['id']]])) {
                        // Salvo gli oggetti senza saveAssocaited
                        $oldgood = $this->Utilities->setReceiptGoodValues($oldGood, $newBill['Bill']['id'], $good);
                    } else {
                        $newBillGood = $this->Utilities->createReceiptGood($newBill['Bill']['id'], $good);
                    }

                    // Il carico e scarico lo faccio solo se è presente nel magazzino
                    // ATTENZIONE !!! NON SO SE QUESTO ANDAVA RIMOSSO !!!! //
                    /*	if($this->Utilities->notExistInStorage($good['storage_id']))
					{
						// Creo nuovo articolo
						$newStorage = $this->Utilities->createNewStorageFromGood($good,$newBill['Bill']['client_id']);

						// Questo è solo un fix per evitare l'errore che non trovava il Good -  Aggiorno lo storage_id sul good
						if(isset($good['Good']['id']))
						{
							$this->Good->updateAll(['Good.storage_id'=>$newStorage['Storage']['id'],'Good.company_id'  => MYCOMPANY],['Good.id'=>$good['Good']['id']]);
						}
						else
						{
							$this->Good->updateAll(['Good.storage_id'=>$newStorage['Storage']['id'],'Good.company_id'  => MYCOMPANY],['Good.id'=>$good['id']]);
						}
					}*/

                    // Aggiornamento scadenze scontrino
                    $vat += number_format($this->Utilities->calculateVatIsolationFromVatId($good['prezzo'], $good['quantita'], $good['iva_id']), 2);
                    $taxableIncome += $this->Utilities->calculateRowPrice($good['quantita'], $good['prezzo']) - $vat;
                }

                // Emetto lo scontrino	- le scadenze vengono pagte immediatamente quindi anche il 31/08 e il 31/12
                //$this->Utilities->setDeadlinesWithVat($this->request->data['Bill']['date'], $this->request->data['Bill']['payment_id'], $taxableIncome, $vat, $newBill['Bill']['id'], $id, false);

                $this->Session->setFlash(__('Scontrino salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => 'indexReceipt']);
            } else {
                $this->Session->setFlash(__('Lo scontrino non è stata salvata correttamente.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Bill->find('first', ['contain' => ['Client' => ['Bank'], 'Good' => ['Iva']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);
        }

        $bills = $this->Bill->find('first', ['contain' => ['Client' => ['Bank'], 'Good' => ['Iva', 'Storage']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id]]);

        $this->set('bills', $bills);
        $this->loadModel('Client');
        $this->set('clients', $this->Client->getList());

        if (ADVANCED_STORAGE_ENABLED) {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        } else {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        // $this->set('units',$this->Utilities->getUnitsList());
        $this->set('units', $this->Units->getList());
        $this->set('deposits', $this->Utilities->getDepositsList());
        $this->set('getAviableQuantity', $this->Utilities);
        $this->set(compact('clients', 'ivas', 'ivaSelect', 'payments'));
        $this->Render('edit_receipt');
    }

    public function delete($id = null, $redirect = 'index')
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Transports', 'Bills', 'Goods', 'Messages', 'Transportgood', 'Order']);

        if (!$this->request->is('post'))
            throw new MethodNotAllowedException();

        $this->Bill->id = $id;
        $goods = $this->Good->find('all', ['conditions' => ['Good.bill_id' => $id]]);
        $oldBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $numberOfMovement = 0;

        foreach ($goods as $good) {
            $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $good['Good']['storage_id']]]);

            if ($oldBill['Bill']['tipologia'] == 1 && $oldBill['Bill']['accompagnatoria'] == 0 && isset($storage['Storage']['id'])) {
                $numberOfMovement++;
                if (isset($storage['Storage']['id'])) {
                    $theStorageId = $storage['Storage']['id'];
                    $this->Utilities->storageLoad($theStorageId, $good['Good']['quantita'], 'Carico per eliminazione fattura  n.' . $oldBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($oldBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $oldBill['Bill']['deposit_id'], $numberOfMovement, 'BI_DE' . '_' . $oldBill['Bill']['id']);
                }
                else {
                    $numberOfMovement++;
                    if (isset($storage['Storage']['id'])) {
                        $theStorageId = $storage['Storage']['id'];
                        $this->Utilities->storageLoad($theStorageId, $good['Good']['quantita'], 'Carico per eliminazione fattura accompagnatoria n.' . $oldBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($oldBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $oldBill['Bill']['deposit_id'], $numberOfMovement, 'BI_DE' . '_' . $oldBill['Bill']['id']);
                    }
                }

            }
        }

        $gender = 'F';
        $article = 'la';

        switch ($oldBill['Bill']['tipologia']) {
            case 1:
                $billname = 'fattura';
                $redirect = 'index';
                break;
            case 2:
                $billname = 'fattura d\'acquisto';
                $redirect = 'indexExtendedBuy';
                break;
            case 3:
                $billname = 'nota di credito';
                $redirect = 'indexExtendedcreditnotes';
                break;
            case 4:
                $billname = 'fattura elettronica';
                $redirect = 'index';
                break;
            case 7:
                $billname = 'Scontrino';
                $redirect = 'indexReceipt';
                $gender = 'M';
                $article = 'lo';
                break;
            case 8:
                $billname = 'Fattura pro forma';
                $redirect = 'indexproforma';
                $gender = 'F';
                $article = 'la';
                break;
        }

        $asg = [$article, $billname, $gender];

        if ($this->Bill->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);


        $currentDeleted = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO]]);
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            if($currentDeleted['Bill']['order_id'] != null){
                $this->Order->updateAll(['Order.bill_id'=> NULL], ['Order.bill_id' => $currentDeleted['Bill']['id']]);
            }
        }
        if ($this->Bill->hide($currentDeleted['Bill']['id'])) {
            $this->loadModel('Sectional');
            $this->Sectional->reduceSectionalValue($currentDeleted['Bill']['sectional_id'], $currentDeleted['Bill']['numero_fattura']);

            $this->Transports->updateAll(['bill_id' => null], ['company_id' => MYCOMPANY, 'bill_id' => $id]);


            $this->Transportgood->updateAll(['Transportgood.bill_id' => null], ['Transportgood.bill_id' => $id]);
            $this->Goods->deleteAll(['Goods.bill_id' => $id]);
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
            $this->redirect(['action' => $redirect]);
        } else {
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
            $this->redirect(['action' => $redirect]);
        }
    }

    // Mostra fatture fornitori
    public function showReceivedBill($receivedBillId)
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->loadModel('Currencies');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Supplier', 'Setting', 'Currencies']);
        $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Carrier', 'Billgestionaldata', 'Supplier' => ['Nation'], 'Payment' => ['Bank'], 'Deadline', 'Good' => ['Goodgestionaldata', 'Iva' => ['Einvoicevatnature'], 'Storage' => ['Units'], 'Units', 'Transports'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $receivedBillId, 'Bill.company_id' => MYCOMPANY]]);

        $i = 0;
        while ($i < count($bill['Good']) - 2) {
            $j = $i + 1;
            while ($j < count($bill['Good']) - 1) {
                if (array_key_exists(0, $bill['Good'][$i]['Goodgestionaldata']) && array_key_exists(0, $bill['Good'][$j]['Goodgestionaldata']) && $bill['Good'][$i]['Goodgestionaldata'][0]['rifddt'] != $bill['Good'][$j]['Goodgestionaldata'][0]['rifddt']) {
                    $k = $j + 1;
                    while ($k < count($bill['Good']) - 1) {
                        if ($bill['Good'][$i]['Goodgestionaldata'][0]['rifddt'] === $bill['Good'][$k]['Goodgestionaldata'][0]['rifddt']) {
                            $tmp = $bill['Good'][$j];
                            $bill['Good'][$j] = $bill['Good'][$k];
                            $bill['Good'][$k] = $tmp;
                        }
                        $k++;
                    }
                } else if (array_key_exists(0, $bill['Good'][$i]['Goodgestionaldata']) == false) {
                    $arr1 = $bill['Good'][$i];
                    array_unshift($bill['Good'], $arr1);
                    array_splice($bill['Good'], $i, 1);
                    break;
                }
                $j++;
            }
            $i++;
        }

        $this->set('bill', $bill);
        $billTransports = $this->Utilities->getBillTransports($bill['Bill']['id'], $receivedBillId);
        $this->set('billTransports', $billTransports);
        $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $settings = $this->Setting->GetMySettings();
        $this->set('settings', $settings);

        $billPdfTitle = 'Fattura n° ' . $bill['Bill']['numero_fattura'] . ' del ' . date('d/m/Y', strtotime($bill['Bill']['date']));

        // Passaggio parametri
        $this->set('nation', $this->Utilities->getNationShortcodeFromId($bill['Bill']['supplier_nation']));
        $this->set('clientnation', $this->Utilities->getNationShortcodeFromId($bill['Bill']['client_nation']));
        $this->set('currency', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));

        // Recupero l'array degli imponibili con iva
        $vatValues = $this->Utilities->getVatPassiveBillValue($receivedBillId);

        // Totale Ritenuta d'acconto ( comprende anche quello della cassa previdenziale )
        $withHoldingTotal = $this->Utilities->getWitholdingTaxTotal($receivedBillId);

        // Evenutale ritenuta Applicata sulla cassa previdenziale
        // $welfareBoxWitholdingTax = $this->Utilities->getWelfareBoxWithholdingTaxTotal($receivedBillId);
        // Sommo la withholding Tax
        // $withHoldingTotal += $welfareBoxWitholdingTax;

        // Totale cassa previdenziale
        $welfareBoxTotal = $this->Utilities->getWelfareBoxTotal($receivedBillId);

        // Recupero i riepiloghi
        $datiRiepilogo = $this->Utilities->getBillGestionalData($receivedBillId);
        $this->set('datiriepilogo', $datiRiepilogo);

        // Se esiste un'iva sulla ritenuta d'acconto
        if ($bill['Bill']['welfare_box_vat_id'] > 0) {
            $vatValues[$bill['Bill']['welfare_box_vat_id']]['imponibile'] += $welfareBoxTotal;
            $vatValues[$bill['Bill']['welfare_box_vat_id']]['vat'] += number_format($welfareBoxTotal * $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] / 100, 2, '.', '');
        }

        $this->set('vatValues', $vatValues);
        $this->set('withHoldingTaxTotal', $withHoldingTotal);
        $this->set('welfareBoxTotal', $welfareBoxTotal);

        $this->Pdf->setMpdf($billPdfTitle, $receivedBillId, $this->Mpdf);
    }

    public function fatturaPdfExtendedvfs($ragioneSociale, $id, $companyId = null, $realCompanyId = null, $token = null)
    {
        $nomefile = str_replace(' ', '_', $ragioneSociale);

        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Bank', 'Client', 'Setting', 'Currencies']);

        $CID = MYCOMPANY;
        $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Carrier', 'Client' => ['Clientdestination' => ['Nation'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units', 'Transports'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
        $banca = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Bill']['bank_id'])));
        $this->set('banca', $banca);
        if ($bill['Bill']['alternative_bank_id'] != 0) {
            $alternativeBank = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Bill']['alternative_bank_id'])));
            $this->set('alternativeBank', $alternativeBank);
        } else if ($bill['Payment']['alternative_bank_id'] != 0) {
            $alternativeBank = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Payment']['alternative_bank_id'])));
            $this->set('alternativeBank', $alternativeBank);
        }
        $billTransports = $this->Utilities->getBillTransports($bill['Bill']['id'], $CID);
        $this->set('billTransports', $billTransports);
        $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $settings = $this->Setting->GetMySettings($CID);
        $this->set('settings', $settings);

        $this->set('bill', $bill);

        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]); // Questa riga seve ?
        $this->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => $CID]]));
        $this->set('deadlines', $this->Utilities->getDeadlines($id));
        $this->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
        $this->set('pdfBody', $this->Pdf->getBillBody($CID));
        $this->set('pdfFooter', $this->Pdf->getBillFooter($CID));

        if ($bill['Bill']['tipologia'] == 1) {
            if ($bill['Bill']['accompagnatoria'] == 1) {
                $this->set('billTitle', 'Fattura accompagnatoria');
                $this->set('pdfFooter', $this->Pdf->getBillTransportFooter($CID));
            } else {
                if ($bill['Bill']['debitnote'] == 1) {
                    $this->set('billTitle', 'Nota di debito');
                } else {
                    $this->set('billTitle', 'Fattura');
                }
                $this->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
            }
        } else if ($bill['Bill']['tipologia'] == 8) {
            $this->set('billTitle', 'Fattura pro forma');
        }

        $bill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

        $this->set('sp', $splitPayment);
        $this->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $this->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));

        // Casse di previdenza
        if ($bill['Bill']['welfare_box_vat_id'] > 0) // Se è settata una cassa di previdenza
        {
            $this->set('welfareVat', $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id']));
        }

        // Iva delle spese d'incasso
        if ($bill['Bill']['collectionfeesvatid'] > 0) // Se è settata una spesa d'incasso
        {
            $this->set('collectionVat', $this->Utilities->getVatFromId($bill['Bill']['collectionfeesvatid']));
        }

        $suffix = isset($bill['Sectional']['suffix']) ? $bill['Sectional']['suffix'] : '';
        $billPdfTitle = 'Fattura n° ' . $bill['Bill']['numero_fattura'] . $suffix . ' del ' . date('d/m/Y', strtotime($bill['Bill']['date']));
        $this->Pdf->setMpdf($billPdfTitle, $nomefile, $this->Mpdf);
        if ($companyId != null) {
            $this->Bill->updateAll(['Bill.mailsent' => '2'], ['Bill.id' => $id, 'Bill.company_id' => $realCompanyId]);
        }
    }

    public function sendfatturaPdfExtendedvfs($id, $type)
    {
        $view = new View(null, false);

        /* Questo non ha crypt e decrypt e restituisce un file da scaricare */
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(0);
        $nomefile = str_replace(' ', '_', $ragioneSociale);

        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Transport', 'Setting', 'Currencies', 'Bank']);

        $configuration = array('win-1252', 'A4', 0, 0, 0, 0, 0, 0, 0, 0);
        $this->Mpdf->init($configuration);
        $this->Mpdf->showImageErrors = true;
        $this->Mpdf->setFilename($nomefile . '.pdf');
        $this->Mpdf->SetTitle("Fattura");
        $this->Mpdf->SetAuthor("");
        $this->Mpdf->SetDisplayMode('fullpage');

        $billFileName = APP . 'tmp' . DS . 'fattura_' . time() . '_' . $id . '.pdf';

        $viewPath = 'Bills';
        $view->viewPath = $viewPath;  //folder to look in in Views directory

        $CID = MYCOMPANY;
        $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Carrier', 'Client' => ['Clientdestination' => ['Nation'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units', 'Transports'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
        $banca = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Bill']['bank_id'])));
        $view->set('banca', $banca);
        if ($bill['Bill']['alternative_bank_id'] != 0) {
            $alternativeBank = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Bill']['alternative_bank_id'])));
            $view->set('alternativeBank', $alternativeBank);
        } else if ($bill['Payment']['alternative_bank_id'] != 0) {
            $alternativeBank = $this->Bank->find('first', array('conditions' => array('Bank.id' => $bill['Payment']['alternative_bank_id'])));
            $view->set('alternativeBank', $alternativeBank);
        }

        $billTransports = $this->Utilities->getBillTransports($bill['Bill']['id'], $CID);
        $view->set('billTransports', $billTransports);

        $view->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $settings = $this->Setting->GetMySettings($CID);
        $view->set('settings', $settings);

        $view->set('bill', $bill);

        $view->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => $CID]]));
        $view->set('deadlines', $this->Utilities->getDeadlines($id));
        $view->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
        $view->set('pdfBody', $this->Pdf->getBillBody($CID));
        $view->set('pdfFooter', $this->Pdf->getBillFooter($CID));

        if ($bill['Bill']['tipologia'] == 1) {
            if ($bill['Bill']['accompagnatoria'] == 1) {
                $view->set('billTitle', 'Fattura accompagnatoria');
                $view->set('pdfFooter', $this->Pdf->getBillTransportFooter($CID));
            } else {
                $view->set('billTitle', 'Fattura');
                $view->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
            }
        } else if ($bill['Bill']['tipologia'] == 8) {
            $view->set('billTitle', 'Fattura pro forma');
        }

        $bill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

        $view->set('sp', $splitPayment);
        $view->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $view->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));

        // Casse di previdenza
        if ($bill['Bill']['welfare_box_vat_id'] > 0) // Se è settata una cassa di previdenza
        {
            $view->set('welfareVat', $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id']));
        }

        // Iva delle spese d'incasso
        if ($bill['Bill']['collectionfeesvatid'] > 0) // Se è settata una spesa d'incasso
        {
            $view->set('collectionVat', $this->Utilities->getVatFromId($bill['Bill']['collectionfeesvatid']));
        }

        $output = $view->render('fattura_pdf_extended_vfs', 'pdf');

        $this->Mpdf->WriteHTML($output);
        $this->Mpdf->Output($billFileName, 'F');
        return $billFileName;
    }

    // Stampa modulo note di credito
    public function sendfatturaPdfExtendedcreditnotes($id, $type)
    {
        $view = new View(null, false);

        /* Questo non ha crypt e decrypt e restituisce un file da scaricare */
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(0);
        $nomefile = str_replace(' ', '_', $ragioneSociale);

        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Transport', 'Setting', 'Currencies']);

        $configuration = array('win-1252', 'A4', 0, 0, 0, 0, 0, 0, 0, 0);
        $this->Mpdf->init($configuration);
        $this->Mpdf->showImageErrors = true;
        $this->Mpdf->setFilename($nomefile . '.pdf');
        $this->Mpdf->SetTitle("Fattura");
        $this->Mpdf->SetAuthor("");
        $this->Mpdf->SetDisplayMode('fullpage');

        $billFileName = APP . 'tmp' . DS . 'nota_di_credito_' . time() . '_' . $id . '.pdf';
        $viewPath = 'Bills';

        $view->viewPath = $viewPath;

        $CID = MYCOMPANY;
        $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Client' => ['Clientdestination' => ['Nation'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
        $view->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $settings = $this->Setting->GetMySettings($CID);

        $view->set('settings', $settings);
        $view->set('bill', $bill);

        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $view->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => $CID]]));
        $view->set('deadlines', $this->Utilities->getDeadlines($id));
        $view->set('billTitle', 'Nota di credito');
        $logo = $this->Setting->find('first');

        // $prefix = isset($bill['Sectional']['prefix']) ? $bill['Sectional']['prefix']  : '' ;
        $suffix = isset($bill['Sectional']['suffix']) ? $bill['Sectional']['suffix'] : '';
        // $billPdfTitle = 'Fattura n '.$prefix . $bill['Bill']['numero_fattura']. $suffix.' del '.date('d/m/Y',strtotime($bill['Bill']['date']));
        $billPdfTitle = 'Fattura n ' . $bill['Bill']['numero_fattura'] . $suffix . ' del ' . date('d/m/Y', strtotime($bill['Bill']['date']));
        $this->Pdf->setMpdf($billPdfTitle, $nomefile, $this->Mpdf);

        $view->set('pdfBody', $this->Pdf->getBillBody($CID));
        $view->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
        $view->set('pdfFooter', $this->Pdf->getBillFooter($CID));
        $settings = $this->Setting->GetMySettings($CID);
        $view->set('settings', $settings);

        $view->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $view->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));

        // Casse di previdenza
        if ($bill['Bill']['welfare_box_vat_id'] > 0) // Se è settata una cassa di previdenza
        {
            $view->set('welfareVat', $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id']));
        }

        // Iva delle spese d'incasso
        if ($bill['Bill']['collectionfeesvatid'] > 0) // Se è settata una spesa d'incasso
        {
            $view->set('collectionVat', $this->Utilities->getVatFromId($bill['Bill']['collectionfeesvatid']));
        }

        $output = $view->render('fattura_pdf_extendedcreditnotes', 'pdf');

        $bill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;
        $this->set('sp', $splitPayment);

        $this->Mpdf->WriteHTML($output);

        $this->Mpdf->Output($billFileName, 'F');
        return $billFileName;

    }

    // Stampa modulo note di credito
    public function fatturaPdfExtendedcreditnotes($ragioneSociale, $id, $companyId = null, $realCompanyId = null, $token = null)
    {
        $nomefile = str_replace(' ', '_', $ragioneSociale);

        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Setting', 'Currencies', 'Bills']);

        // Not Logged
        if ($companyId != null) {
            $id = str_replace("nrt18", "%2F", $id);
            $ragioneSociale = str_replace("nrt18", "%2F", $ragioneSociale);
            $realCompanyId = str_replace("nrt18", "%2F", $realCompanyId);
            $token = str_replace("nrt18", "%2F", $token);

            $id = $this->Utilities->mydecrypt(str_replace(' ', '+', urldecode($id)), $cipher = 'aes-256-cbc', $key = "noratech2018!!CR@@", $option = 0, $iv = '12@@56#90!!34X6i');
            $ragioneSociale = $this->Utilities->mydecrypt(str_replace(' ', '+', urldecode($ragioneSociale)), $cipher = 'aes-256-cbc', $key = "noratech2018!!CR@@", $option = 0, $iv = '12@@56#90!!34X6i');
            $realCompanyId = $this->Utilities->mydecrypt(str_replace(' ', '+', urldecode($realCompanyId)), $cipher = 'aes-256-cbc', $key = "noratech2018!!CR@@", $option = 0, $iv = '12@@56#90!!34X6i');
            $token = $this->Utilities->mydecrypt(str_replace(' ', '+', urldecode($token)), $cipher = 'aes-256-cbc', $key = "noratech2018!!CR@@", $option = 0, $iv = '12@@56#90!!34X6i');

            $CID = $realCompanyId;

            if (isset($this->request->data['Bill']['alternativeaddress_id']))
            {
                $shippingAddress = $this->Clientdestination->find('first', ['conditions' => ['Clientdestination.id' => $this->request->data['Bill']['alternativeaddress_id']]]);
                if (count($shippingAddress) > 0) {
                    $this->request->data['Bill']['client_shipping_name'] = $shippingAddress['Clientdestination']['name'];
                    $this->request->data['Bill']['client_shipping_address'] = $shippingAddress['Clientdestination']['address'];
                    $this->request->data['Bill']['client_shipping_cap'] = $shippingAddress['Clientdestination']['cap'];
                    $this->request->data['Bill']['client_shipping_city'] = $shippingAddress['Clientdestination']['city'];
                    $this->request->data['Bill']['client_shipping_province'] = $shippingAddress['Clientdestination']['province'];
                    if (isset($shippingAddress['Clientdestination']['nation_id'])) {
                        $this->request->data['Bill']['client_shipping_nation'] = $this->Utilities->getNationName($shippingAddress['Clientdestination']['nation_id'])['Nation']['name'];
                    }
                }
            }

            /** Find Token su Db principale **/
            if ($token = $this->Utilities->validateToken($token, $id, $realCompanyId, $ragioneSociale)) {
                // Script il nome db
                $decryptDbName = $this->Utilities->mydecrypt($token['Token']['dbname'], $cipher = 'aes-256-cbc', $key = "TokenEncryptionDatabse", $option = 0, $iv = '12@@@@#905634X7K');
                $this->Utilities->connectToDb($decryptDbName);
                $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Client' => ['Clientdestination' => 'Nation'], 'Bank', 'Nation', 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.Client_id' => $ragioneSociale]]);
                if ($bill == null) {
                    die;
                }
                $this->Set('companyLogo', $this->Setting->getCompanyLogo($bill['Bill']['company_id'])['Setting']['header']);
                $settings = $this->Setting->GetMySettings($CID);
            } else {
                throw new  NotFoundException();
            }
        } else {
            $CID = MYCOMPANY;
            $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Client' => ['Clientdestination' => ['Nation'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
            $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
            $settings = $this->Setting->GetMySettings($CID);
            $this->set('settings', $settings);
        }
        $this->set('bill', $bill);
        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $this->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => $CID]]));
        $this->set('deadlines', $this->Utilities->getDeadlines($id));
        $this->set('billTitle', 'Nota di credito');
        $logo = $this->Setting->find('first');
        // $prefix = isset($bill['Sectional']['prefix']) ? $bill['Sectional']['prefix']  : '' ;
        $suffix = isset($bill['Sectional']['suffix']) ? $bill['Sectional']['suffix'] : '';
        // $creditnotePdfTitle = 'Nota di credito n° '.$prefix . $bill['Bill']['numero_fattura']. $suffix.' del '.date('d/m/Y',strtotime($bill['Bill']['date']));
        $creditnotePdfTitle = 'Nota di credito n° ' . $bill['Bill']['numero_fattura'] . $suffix . ' del ' . date('d/m/Y', strtotime($bill['Bill']['date']));
        $this->Pdf->setMpdf($creditnotePdfTitle, $nomefile, $this->Mpdf);
        $this->set('pdfBody', $this->Pdf->getBillBody($CID));
        $this->set('pdfHeader', $this->Pdf->getBillHeader($CID, $settings['Setting']['pdf_header_version']));
        $this->set('pdfFooter', $this->Pdf->getBillFooter($CID));
        $settings = $this->Setting->GetMySettings($CID);
        $this->set('settings', $settings);
        $this->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $this->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));

        $bill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;
        $this->set('sp', $splitPayment);

        // Casse di previdenza
        if ($bill['Bill']['welfare_box_vat_id'] > 0) // Se è settata una cassa di previdenza
        {
            $this->set('welfareVat', $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id']));
        }

        // Iva delle spese d'incasso
        if ($bill['Bill']['collectionfeesvatid'] > 0) // Se è settata una spesa d'incasso
        {
            $this->set('collectionVat', $this->Utilities->getVatFromId($bill['Bill']['collectionfeesvatid']));
        }

        if ($companyId != null) {
            $this->Bill->updateAll(['Bill.mailsent' => '2'], ['Bill.id' => $id, 'Bill.company_id' => $realCompanyId]);
        }
    }

    /* Fattura barelli con lista ddt all'inizio **/
    /* NON USATA PER ORA */
    public function fatturaPdfExtendedWithDdtList($ragioneSociale, $id)
    {
        $nomefile = str_replace(' ', '_', $ragioneSociale);
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Transports', 'Pdf']);
        $bill = $this->Bill->find('first', ['contain' => ['Client' => ['Catalog' => ['CatalogStorage'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Units'], 'Iva', 'Nation'], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        foreach ($bill['Good'] as $billdetails) {
            $ddt = $this->Transports->find('first', ['conditions' => ['Transports.id' => $billdetails['transport_id'], 'company_id' => MYCOMPANY]]);
            if (count($ddt) != 0) {
                $arrayDDT[$billdetails['transport_id']]['bolla'] = $ddt['Transports']['transport_number'];
                $arrayDDT[$billdetails['transport_id']]['data'] = date("d-m-Y", strtotime($ddt['Transports']['date']));
            }
        }

        $this->set('bill', $bill);

        isset($arrayDDT) ? $this->set('ddt', $arrayDDT) : $this->set('ddt', null);

        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $this->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]));
        $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $this->set('deadlines', $this->Utilities->getDeadlines($id));
        $this->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $this->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));
        $logo = $this->Setting->find('first');

        $this->Pdf->setMpdf("Fattura", $nomefile, $this->Mpdf);
    }

    /* Fattura barelli col dettaglio ddt per ogni voce */
    /* NON USATA PER ORA */
    public function fatturaPdfExtendedWithDdtDetail($ragioneSociale, $id)
    {
        $nomefile = str_replace(' ', '_', $ragioneSociale);
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Transports', 'Pdf']);
        $bill = $this->Bill->find('first', ['contain' => ['Client' => ['Catalog' => ['CatalogStorage'], 'Bank', 'Nation'], 'Payment' => ['Bank'], 'Good' => ['Iva', 'Storage' => ['Units'], 'Transports']], 'recursive' => 2, 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        $this->set('bill', $bill);
        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $this->set('impostazioni', $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]));
        $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
        $this->set('deadlines', $this->Utilities->getDeadlines($id));
        $logo = $this->Setting->find('first');
        $this->set('currencySymbol', $this->Currencies->getCurrencySymbol($bill['Bill']['currency_id']));
        $this->set('currencyCode', $this->Currencies->getCurrencyName($bill['Bill']['currency_id']));
        $this->Pdf->setMpdf("Fattura", $nomefile, $this->Mpdf);
    }

    public function calculateFixedTotalPercentage()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Setting']);
        $this->autoRender = false;
        $percentage = $this->Setting->find('first', ['fields' => ['fixed_cost_percentage_enabled', 'fixed_cost_percentage_value'], 'conditions' => ['Setting.company_id' => MYCOMPANY]]);
        if ($percentage['Setting']['fixed_cost_percentage_enabled']) {
            return number_format($_POST['totale'] * $percentage['Setting']['fixed_cost_percentage_value'] / 100, 2, '.', '');
        } else {
            return 0;
        }
    }

    public function getFixedTotalPercentageDescription()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Setting']);

        $this->autoRender = false;
        $percentage = $this->Setting->find('first', ['fields' => ['fixed_cost_percentage_enabled', 'fixed_cost_percentage_description'], 'conditions' => ['Setting.company_id' => MYCOMPANY]]);
        if ($percentage['Setting']['fixed_cost_percentage_enabled']) {
            return $percentage['Setting']['fixed_cost_percentage_description'];
        } else {
            return '';
        }
    }

    public function getCurrentBillStorageQuantity()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $quantity = $this->Utilities->getCurrentBillStorageQuantity($_POST['billId'], $_POST['storageId']);
        return $quantity;
    }

    // Questa è quella per il commercialista
    public function fattura_xml() { }

    public function ixfesyncro($billId, $actionName)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Perché il token viene aggiornato ed è soggetto a scadenza
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $this->Utilities->updateSettingsTokenIXFE($token = $mySettings['Setting']['tokenIXFE']);
        // Cancello e salvo l'aoos
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        $billSyncroIdIxfe = $this->Utilities->getSyncroResponse($billId, $token, $IdentificativoAoos);
        switch ($billSyncroIdIxfe) {
            case 'nessuna':
                $this->Bill->updateAll(['id_ixfe' => null], ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]);
                break;
            case '':
                // Nothing
                break;
            default:
                $this->Bill->updateAll(['id_ixfe' => '\'' . $billSyncroIdIxfe . '\''], ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]);
                break;
        }

        $this->redirect(['controller' => 'bills', 'action' => $actionName]);
    }

    // Aggiorna icone degli stati ixfe
    public function refreshIXFEStates($billId, $actionName)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Perché il token viene aggiornato ed è soggetto a scadenza
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $this->Utilities->updateSettingsTokenIXFE($token = $mySettings['Setting']['tokenIXFE']);
        // Cancello e salvo l'aoos
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        $this->Utilities->UpdateBillsIXFEStates($billId);
        $this->redirect(['controller' => 'bills', 'action' => $actionName]);
    }

    /*
    public function refreshIXFEStates()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        $this->Utilities->UpdateBillsIXFEStates($_POST['billId']);
    }
    */

    // Reinvio in conservazione sostitutiva
    public function resendInConservation($billId, $actionName)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Perché il token viene aggiornato ed è soggetto a scadenza
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $this->Utilities->updateSettingsTokenIXFE($token = $mySettings['Setting']['tokenIXFE']);
        // Cancello e salvo l'aoos
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        // Recupero sezionali
        $conservationREsponse = $this->Utilities->sendToConservation($IdentificativoAoos, $token);

        switch ($conservationREsponse) {
            case "Error009":
                $errorMessage = "Errore durante il tentativo di conservazione elettronica. Sezionale non censito.";
                $this->Session->setFlash(($errorMessage), 'custom-danger');
                break;
            default:
                $this->Utilities->UpdateBillsIXFEStates($billId);
                break;
        }

        $this->redirect(['controller' => 'bills', 'action' => $actionName]);
    }

    public function getIxfePassiveBills()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Perché il token viene aggiornato ed è soggetto a scadenza
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $this->Utilities->updateSettingsTokenIXFE($token = $mySettings['Setting']['tokenIXFE']);
        // Cancello e salvo l'aoos
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        $passiveResult = $this->Utilities->getIxfePassiveBills($IdentificativoAoos, $token, $_POST['dateFrom'], $_POST['dateTo'], $_POST['download'], $_POST['filterreceived']);

        if ($passiveResult != -1)
            return $passiveResult;
        else
            $this->redirect(['controller' => 'bills', 'action' => 'ebillimport']);
    }

    public function getIxfeActiveBills()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Perché il token viene aggiornato ed è soggetto a scadenza
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $this->Utilities->updateSettingsTokenIXFE($token = $mySettings['Setting']['tokenIXFE']);
        // Cancello e salvo l'aoos
        $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        $activeResult = $this->Utilities->getIxfeActiveBills($IdentificativoAoos, $token, $_POST['dateFrom'], $_POST['dateTo']);
        if ($activeResult != -1) {
            return $activeResult;
        } else {
            $this->redirect(['controller' => 'bills', 'action' => 'sentbillimport']);
        }
    }

    // Invio fatture ad ixfe
    public function sendBillToIxfe($id, $redirect = true)
    {
        // Non devo disegnare niente
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->loadModel('Log');
        $this->Utilities->loadModels($this, ['Setting']);

        // Resetto i valori
        $this->Bill->updateAll(['id_ixfe' => '\'\'', 'validation_ixfe' => null, 'state_ixfe' => null, 'processing_state_ixfe' => null], ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]);

        // Variabile utilizzati
        $errorMessage = null;
        $identificativo = null;

        // Recupero Access Token
        //$this->Utilities->updateSettingsTokenIXFE($token = $this->Utilities->getIXFEAccessToken());
        // Cancello e salvo l'aoos
        //$this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        //$this->Utilities->UpdateBillsIXFEStates($id);


        // Genero il nome file xml
        $mySettings = $this->Setting->GetMySettings();
        // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
        $token = $mySettings['Setting']['tokenIXFE'];

        $mySettings['Setting']['piva'] != '' ? $pivacf = $mySettings['Setting']['piva'] : $pivacf = $mySettings['Setting']['cf'];

        // Partita iva errata
        if ((strlen($pivacf) != 11) && (strlen($pivacf) != 16))
        {
            $errorMessage = "Errore partita IVA inserita non corretta! Reinseire la partita IVA nell'anagrafica cliente.";
            $this->Session->setFlash(($errorMessage), 'custom-danger');
            $this->redirect(['controller' => 'bills', 'action' => 'index']);
        }

        $bill = $this->Bill->find('first', ['contain' => ['Client' => ['Nation', 'Bank'], 'Good' => ['Iva' => ['Einvoicevatnature'], 'Storage', 'Units'], 'Payment' => ['Bank', 'Einvoicepaymenttype', 'Einvoicepaymentmethod']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        $nomeFile = 'IT' . $pivacf . '_' . strtoupper(str_pad(base_convert($bill['Bill']['id'], 10, 32), 5, '0', STR_PAD_LEFT)) . '.xml';

        $billNumber = strtoupper(str_pad(base_convert($bill['Bill']['id'], 10, 32), 5, '0', STR_PAD_LEFT)); //str_pad(base_convert($bill['Bill']['id'],10,32),5,'0',STR_PAD_LEFT);

        // Errore durante il recupero del token
        if ($token == -1 || $token == null)
        {
            $errorMessage = "Errore durante l'autenticazione al servizio IXFE (controllare le credenziali d'accesso)";
            $this->Log->savelog('ConnectionError', 'Errore richiesta token', $token);
            $this->Session->setFlash(($errorMessage), 'custom-danger');
            $this->redirect(['controller' => 'bills', 'action' => 'index']);
        }

        // Recupero Access Aoos
        if ($mySettings['Setting']['aoosIXFE'] == null)
            $this->Utilities->updateSettingsAoosIXFE($IdentificativoAoos = $this->Utilities->getIXFEAoos($token));
        else
            $IdentificativoAoos = $mySettings['Setting']['aoosIXFE'];

        // Errore durante il recupero dell'aaos
        // Da quando l'ho spostato sul setting non dovrebbero esserci problemi.
        if ($IdentificativoAoos == -1)
        {
            $errorMessage = "Errore durante il recupero dell'identificativo AOOS! Reinseire l'identificativo in maniera corretta.";
            $this->Log->savelog('ConnectionError', 'Errore identificativo aoos', $IdentificativoAoos);
            $this->Session->setFlash(($errorMessage), 'custom-danger');
            $this->redirect(['controller' => 'bills', 'action' => 'index']);
        }

        // Recupero la fattura elettronica
        $electronicInvoice = $this->fattura_xmlvfs($id, $id, true, $redirect);
        if($electronicInvoice == false){
            return false;
        }

        // La converto base 64
        $electronic_invoice_base64 = base64_encode($electronicInvoice);

        // Manda la fattura elettronica a IXFE
        $identificativoFatturaIXFE = $this->Utilities->sendIXFEBill($token, $IdentificativoAoos, $electronic_invoice_base64, $nomeFile, $billNumber, $this->Utilities->getSectionalIxfe($bill['Bill']['id']));

        if($redirect)
        {
            switch ($identificativoFatturaIXFE)
            {
                case "ERR001":
                    $errorMessage = "Errore durante la trasmissione della fattura elettronica";
                    $this->Session->setFlash(($errorMessage), 'custom-danger');
                    break;
                case "ERR002":
                    $errorMessage = "Errore durante la trasmissione della fattura elettronica: identificativo esterno della fattura già presente in IXFE";
                    $this->Session->setFlash(($errorMessage), 'custom-danger');
                    break;
                case "ERR010":
                    $errorMessage = "IXFE non risponde in maniera corretta, contattare l'assistenza.";
                    $this->Session->setFlash(($errorMessage), 'custom-danger');
                    break;
                case "ERR012":
                    $errorMessage = "La fattura non può essere inviata ad IXFE in quanto non è stato correttamente configurato il sezionale IXFE";
                    $this->Session->setFlash(($errorMessage), 'custom-danger');
                    break;
                case "ERR101":
                    $errorMessage = "IXFE non ha risposto correttamente";
                    $this->Bill->updateAll(['id_ixfe' => '\'' . 'CURLE_OPERATION_TIMEDOUT' . '\''], ['Bill.id' => $bill['Bill']['id'], 'Bill.company_id' => MYCOMPANY]);
                    $this->Session->setFlash(($errorMessage), 'custom-danger');
                    break;
                default:
                    $this->Bill->updateAll(['id_ixfe' => '\'' . $identificativoFatturaIXFE . '\''], ['Bill.id' => $bill['Bill']['id'], 'Bill.company_id' => MYCOMPANY]);
                    break;
            }

            if ($bill['Bill']['tipologia'] == 1)
                $this->redirect(['controller' => 'bills', 'action' => 'index']);

            if ($bill['Bill']['tipologia'] == 3)
                $this->redirect(['controller' => 'bills', 'action' => 'indexExtendedcreditnotes']);
        }

        $result = true;

        switch ($identificativoFatturaIXFE)
        {
            case "ERR001":
            case "ERR002":
            case "ERR010":
            case "ERR012":
            case "ERR101":
                $result = false;
                break;
            default:
                $this->Bill->updateAll(['id_ixfe' => '\'' . $identificativoFatturaIXFE . '\''], ['Bill.id' => $bill['Bill']['id'], 'Bill.company_id' => MYCOMPANY]);
                break;
        }
        return $result;
    }

    /* FATTURA ELETTRONICA */
    public function fattura_xmlvfs($x = 0, $id, $ixfe = false, $return = true)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Setting', 'Currencies']);

        $this->autoRender = false;

        $error = [];

        // Codice Paese + iban/cf diffa + '-' + progressivo + '.xml'
        $mySettings = $this->Setting->GetMySettings();
        $pivacf = '';
        $mySettings['Setting']['piva'] != '' ? $pivacf = $mySettings['Setting']['piva'] : $pivacf = $mySettings['Setting']['cf'];

        // Partita iva errata
        // if((strlen($pivacf) != 11 ) && (strlen($pivacf) != 16)) { array_push($error,1); } // Rimossa causa clienti esteri

        $bill = $this->Bill->find('first', ['contain' => ['Sectional', 'Client' => ['Nation', 'Bank'], 'Carrier' => ['Nation'], 'Good' => ['Iva' => ['Einvoicevatnature'], 'Storage', 'Units'], 'Payment' => ['Bank', 'Einvoicepaymenttype', 'Einvoicepaymentmethod']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        $nomefile = 'IT' . $pivacf . '_' . strtoupper(str_pad(base_convert($bill['Bill']['id'], 10, 32), 5, '0', STR_PAD_LEFT)) . '.xml';

        $settings = $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]);
        $importoRitenuta = $this->Utilities->getWitholdingTaxTotal($id);

        // Aggiunta storno importo ritenuta
        $stornoRitenuta = $this->Utilities->stornoImportoWitholdingTax($id);
        $stornoImponibileRitenuta = $this->Utilities->stornoImponibileWithholdingTax($id);
        $importoRitenuta = $importoRitenuta - $stornoRitenuta;

        $importoTotaleFattura = $this->Utilities->getBillTotal($id);
        $vatDetails = $this->Utilities->getVatValues($id);
        $deadlines = $this->Utilities->getDeadlines($id);


        $currencyCode = $this->Currencies->getCurrencyName($bill['Bill']['currency_id']);
        if ($currencyCode == '')
            $currencyCode = 'EUR';

        // Controllo i riferimenti ddt
        $arrayOfTransportReference = [];
        $counter = 0;
        $this->loadModel('Transport');

        foreach ($bill['Good'] as $billGood) {
            if ($billGood['transport_id'] > 0) {
                // Controllo che non sia una nota
                if ($billGood['quantita'] != null && $billGood['prezzo'] != null && $billGood['iva_id'] != null) {
                    $counter++;

                    if (!isset($arrayOfTransportReference[$billGood['transport_id']]))
                        $arrayOfTransportReference[$billGood['transport_id']]['rows'] = $counter;
                    else
                        $arrayOfTransportReference[$billGood['transport_id']]['rows'] = $arrayOfTransportReference[$billGood['transport_id']]['rows'] . '|' . $counter;
                }
            }
        }

        // Controllo
        foreach ($arrayOfTransportReference as $key => $ddtReference) {
            $currentDDT = $this->Transport->getTransportFromId($key);
            // Fix se viene cancellata la bolla (?) Non dovrebbe poter essere cancellata
            if ($currentDDT != []) {
                $arrayOfTransportReference[$key]['date'] = $currentDDT['Transport']['date'];
                $arrayOfTransportReference[$key]['number'] = $currentDDT['Transport']['transport_number'];
            }
        }

        $this->set('rifddt', $arrayOfTransportReference);
        // Fine creazione riferimenti ddt

        // START Modifiche per cassa previdenziale
        $welfareBox = $this->Utilities->getWelfareBoxTotal($id) - number_format($stornoImponibileRitenuta * $bill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
        $welfareBoxTaxable = $this->Utilities->getBillTaxable($id) - $stornoImponibileRitenuta;
        $welfareBoxWithholdingTax = $this->Utilities->getWelfareBoxWithholdingTaxTotal($id);
        $welfareBoxVat = number_format($welfareBox * $this->Utilities->getVatPercentageFromId($bill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
        $welfareBoxVatPercentage = $this->Utilities->getVatPercentageFromId($bill['Bill']['welfare_box_vat_id']);

        if (isset($bill['Bill']['welfare_box_vat_id'])) {
            if (isset($this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['einvoicenature_id'])) {
                $welfareBoxVatNature = $this->Utilities->getEinvoiceVatNatureCodeFromId($this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['einvoicenature_id']);
                $this->set('welfareBoxVatNature', $welfareBoxVatNature);
            }
        }
        // FINE CASSA PREVIDENZIALE

        $vatWelfareBox = null; // Aggiunto
        if ($bill['Bill']['welfare_box_vat_id'] > 0) {
            if (isset($vatDetails[$bill['Bill']['welfare_box_vat_id']])) {
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['imponibile'] += $welfareBox;
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['vat'] += $welfareBoxVat;
            } else {
                $vatWelfareBox = $this->Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id']);
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['imponibile'] = $welfareBox;
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['vat'] = $welfareBoxVat;
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['codice'] = $this->Utilities->getEinvoiceVatNatureCodeFromId($vatWelfareBox['Ivas']['einvoicenature_id']);//$vatWelfareBox['Ivas']['codice'];
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['description'] = $vatWelfareBox['Ivas']['descrizione'];
                $vatDetails[$bill['Bill']['welfare_box_vat_id']]['percentuale'] = $vatWelfareBox['Ivas']['percentuale'];
            }
        }

        if ($bill['Bill']['collectionfeesvatid'] > 0) // Se è settata una spesa d'incasso passo l'iva delle spese d'incasso
        {
            $collectionVat = $this->Utilities->getVatFromId($bill['Bill']['collectionfeesvatid']);
            $this->set('collectionVat', $collectionVat);
            $collectionVatInvoiceNature = $this->Utilities->getEinvoiceVatNatureCodeFromId($collectionVat['Ivas']['einvoicenature_id']);
            $this->set('collectionVatInvoiceNature', $collectionVatInvoiceNature);
        }

        // Se fattura accompagnatoria
        if ($bill['Bill']['accompagnatoria'] == 1) {
            $bill['Bill']['causal_id'] > 0 ? $billCausal = $this->Utilities->getTransportCausalNameFromId($bill['Bill']['causal_id']) : $billCausal = '';
            $this->set('billCausal', $billCausal);
        }

        $this->set('welfareBox', $welfareBox);
        $this->set('welfareBoxWithholdingTax', $welfareBoxWithholdingTax);
        $this->set('welfareBoxVat', $welfareBoxVat);
        $this->set('welfareBoxVatPercentage', $welfareBoxVatPercentage);
        $this->set('welfareBoxTaxable', $welfareBoxTaxable);
        $this->set('vatWelfareBox', $vatWelfareBox);
        $suffix = isset($bill['Sectional']['suffix']) ? $bill['Sectional']['suffix'] : '';
        $this->set('suffix', $suffix);

        $rounding = $bill['Bill']['rounding'];

        if (isset($bill['Bill']['client_nation'])) {
            $clientnationshortcode = $this->Utilities->getNationShortcodeFromId($bill['Bill']['client_nation']);
            $this->set('clientnationshortcode', $clientnationshortcode);
        }

        // END Modifiche per cassa previdenziale

        /** Controlli prima di effettuare la fattura elettronica */

        // Provincia errata
        if (strlen($settings['Setting']['prov']) > 2)
            array_push($error, 2);

        if (strlen($bill['Bill']['client_province']) > 2)
            array_push($error, 3);

        // Controllo tipologia di pagamento [è un campo required]
        if (!isset($bill['Payment']['Einvoicepaymenttype']['code']))
            array_push($error, 4);

        // Controllo tipologia di pagamento [è un campo required]
        if (!isset($bill['Payment']['Einvoicepaymentmethod']['code']))
            array_push($error, 5);

        if (!isset($settings['Setting']['transmitter_nationality_code']) || ($settings['Setting']['transmitter_nationality_code'] == ''))
            array_push($error, 6);

        if (!isset($settings['Setting']['transmitter_fiscal_identification']) || ($settings['Setting']['transmitter_fiscal_identification'] == ''))
            array_push($error, 7);

        if ((!$bill['Client']['codiceDestinatario']) || ($bill['Client']['codiceDestinatario'] == ''))
            array_push($error, 8);

        if (($bill['Client']['codiceDestinatario']) && (strlen($bill['Client']['codiceDestinatario']) < 6))
            array_push($error, 81);

        if (!isset($settings['Setting']['provider_nationality_code']) || ($settings['Setting']['provider_nationality_code'] == ''))
            array_push($error, 9);

        if (!isset($settings['Setting']['provider_fiscal_identification']) || ($settings['Setting']['provider_fiscal_identification'] == ''))
            array_push($error, 10);

        if (!isset($settings['Setting']['fiscal_regime']) || ($settings['Setting']['fiscal_regime'] == ''))
            array_push($error, 11);

        if ($settings['Setting']['rea_registered']) {
            if (!isset($settings['Setting']['rea_province']) || ($settings['Setting']['rea_province'] == ''))
                array_push($error, 12);

            if (!isset($settings['Setting']['rea_number']) || ($settings['Setting']['rea_number'] == ''))
                array_push($error, 13);

            if (!isset($settings['Setting']['closure_state']) || ($settings['Setting']['closure_state'] == ''))
                array_push($error, 15);
        }

        if (!isset($bill['Bill']['client_address']) || ($bill['Bill']['client_address'] == ''))
            array_push($error, 16);

        if (!isset($bill['Bill']['client_cap']) || ($bill['Bill']['client_cap'] == '') || (strlen($bill['Bill']['client_cap']) != 5))
            array_push($error, 17);

        if (!isset($bill['Bill']['client_city']) || ($bill['Bill']['client_city'] == ''))
            array_push($error, 18);

        // Da valorizzare se la nazione è italia
        if ($bill['Bill']['client_nation'] > 0) {
            if ($this->Utilities->getNationShortcodeFromId($bill['Bill']['client_nation']) == 'IT') {
                if (!isset($bill['Bill']['client_province']) || ($bill['Bill']['client_province'] == ''))
                    array_push($error, 19);
            }
        } else
            array_push($error, 191);

        if ($bill['Bill']['client_nation'] > 0) {
            if ($this->Utilities->getNationShortcodeFromId($bill['Bill']['client_nation']) == 'IT') {
                if (($bill['Client']['piva'] == '' && $bill['Client']['cf'] == '') || (strlen($bill['Client']['cf']) < 11 && strlen($bill['Client']['piva']) < 11))
                    array_push($error, 20);
            }
        }

        // Errori per ritenuta d'acconto solo se è presente una ritenuta d'acconto
        if (isset($bill['Bill']['withholding_tax']) && ($bill['Bill']['withholding_tax'] > 0)) {
            if (!isset($settings['Setting']['type_of_person']) || ($settings['Setting']['type_of_person'] == ''))
                array_push($error, 22);

            if (!isset($settings['Setting']['withholding_tax_causal_id']) || ($settings['Setting']['withholding_tax_causal_id'] == ''))
                array_push($error, 23);
        }

        // Controllo se è stato settato un ixfe per il sectional
        if ($ixfe) {
            if (!isset($bill['Sectional']['ixfe_sectional']) || ($bill['Sectional']['ixfe_sectional'] == ''))
                array_push($error, 24);
        }

        // Controllo che sia corretta la lunghezza del codice destinatario e la tipologia di destinatario
        if ($bill['Client']['pa'] == 1 && strlen($bill['Client']['codiceDestinatario']) != 6)
            array_push($error, 27);

        // Controllo che sia corretta la lunghezza del codice destinatario e la tipologia di destinatario
        if ($bill['Client']['pa'] == 0 && strlen($bill['Client']['codiceDestinatario']) != 7)
            array_push($error, 28);

        // Se viene definita un iva nulla, deve essere indicato obbligatoriamente anche il riferimento normativo
        $error30 = 0;

        foreach ($bill['Good'] as $good) {
            if ($good['prezzo'] != null && $good['Iva'] != null) {
                if ($good['Iva']['percentuale'] == 0 && !isset($good['Iva']['Einvoicevatnature']['id']))
                    $error30 = 1;
            }
        }

        $error30 == 1 ? array_push($error, 30) : null;

        if (!isset($bill['Bill']['einvoicevatesigibility']) || $bill['Bill']['einvoicevatesigibility'] == '')
            array_push($error, 25);

        if (($bill['Bill']['welfare_box_code'] == null || $bill['Bill']['welfare_box_code'] == '') && $bill['Bill']['welfare_box_vat_id'] == null && ($bill['Bill']['welfare_box_percentage'] == null || $bill['Bill']['welfare_box_percentage'] == 0) && ($bill['Bill']['welfare_box_withholding_appliance'] == null || $bill['Bill']['welfare_box_withholding_appliance'] == '')) {
        } else {
            if ($bill['Bill']['welfare_box_code'] != null && $bill['Bill']['welfare_box_vat_id'] != null && $bill['Bill']['welfare_box_percentage'] != null && $bill['Bill']['welfare_box_withholding_appliance'] != null) {
            } else
                array_push($error, 26);
        }

        /** Fine controlli */
        if (count($error) == 0)
        {
            if ($ixfe == false)
            {
                ob_start();
                $this->set(compact('bill', 'settings', 'ivaSelect', 'payment', 'importoTotaleFattura', 'vatDetails', 'deadlines', 'importoRitenuta', 'currencyCode', 'rounding'));
                require(ROOT . DS . 'loginGol' . DS . 'View' . DS . 'Bills' . DS . 'fattura_xml_vfs.ctp'); //in locale al posto di app mettere loginGol
                $var = ob_get_clean();
                header("Content-type: applcation/xml");
                header("Content-Length: " . strlen($var));
                header('Content-disposition: attachment; filename="' . $nomefile . '"');
                echo($var);
                exit;
            }

            if ($ixfe == true)
            {
                ob_start();
                $this->set(compact('bill', 'settings', 'ivaSelect', 'payment', 'importoTotaleFattura', 'vatDetails', 'deadlines', 'importoRitenuta', 'currencyCode', 'rounding'));
                require(ROOT . DS . 'loginGol' . DS . 'View' . DS . 'Bills' . DS . 'fattura_xml_vfs.ctp');//in locale al posto di app mettere loginGol
                $var = ob_get_clean();
                return $var;
            }
        }
        else
        {
            foreach ($error as $err) {
                $errorMessage = '';

                if ($bill['Bill']['tipologia'] == 1)
                    $documento = 'fattura';

                if ($bill['Bill']['tipologia'] == 3)
                    $documento = 'nota di credito';

                switch ($err) {
                    case 2:
                        $errorMessage = 'Attenzione, nelle impostazioni la provincia della ditta deve essere due caratteri';
                        break;
                    case 3:
                        $errorMessage = 'Attenzione, la sigla della provincia del cliente deve essere di due caratteri';
                        break;
                    case 4:
                        $errorMessage = 'Attenzione, non è stato compilato il campo "tipo di pagamento" per la fatturazione elettronica nel metodo di pagamento utilizzato';
                        break;
                    case 5:
                        $errorMessage = 'Attenzione, non è stato compilato il campo "metodo di pagamento" per la fatturazione elettronica nel metodo di pagamento utilizzato';
                        break;
                    case 6:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "codice paese trasmittente" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 7:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "identificazione fiscale trasmittente" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 8:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il codice cliente all\'interno dell\'anagrafica cliente nella sezione fatturazione elettronica.';
                        break;
                    case 81:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il codice cliente all\'interno dell\'anagrafica cliente la lunghezza deve essere almeno di 6 caratteri.';
                        break;
                    case 9:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "codice paese cedente/prestatore" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 10:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "identificazione fiscale cedente/prestatore" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 11:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "regime fiscale" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 13:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "Numero REA" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 12:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "Sigla provincia del registro imprese" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 15:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "stato liquidazione" nella sezione impostazioni/configurazione fatturazione elettronica';
                        break;
                    case 16:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente campo "indirizzo" nella ' . $documento . '.';
                        break;
                    case 17:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "codice di avviamento postale" nella  ' . $documento . '.';
                        break;
                    case 18:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "città" nella  ' . $documento . '.';
                        break;
                    case 19:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "provincia" nella  ' . $documento . '.';
                        break;
                    case 191:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo "nazione" nella  ' . $documento . '.';
                        break;
                    case 20:
                        $errorMessage = 'Attenzione, non è stato compilato correttamente il campo partita iva (o codice fiscale) nell\'anagrafica cliente.';
                        break;
                    case 22:
                        $errorMessage = 'Attenzione non è stato compilato correttamente il campo "persona fisica \ giuridica" nella sezione impostazioni/dati anagrafici.';
                        break;
                    case 23:
                        $errorMessage = 'Attenzione non è stato compilato correttamente il campo "causale ritenuta d\'acconto" nella sezione impostazioni/causale ritenuta d\'acconto.';
                        break;
                    case 24:
                        $errorMessage = 'Attenzione non è stato compilato correttamente il campo "sezionale ixfe" nel sezionale utilizzato per inviare la ' . $documento . ' elettronica a ixfe.';
                        break;
                    case 25:
                        $errorMessage = 'Attenzione non è stata selezionata la tipologia di esigibilità iva all\'interno della ' . $documento . '.';
                        break;
                    case 26:
                        $errorMessage = 'Attenzione non sono stati correttamente compilati tutti i campi riguardanti la cassa previdenziale all\'interno della fattura.';
                        break;
                    case 27:
                        $errorMessage = 'Attenzione il codice destinatario di una pubblica amministrazione deve essere di 6 caratteri.';
                        break;
                    case 28:
                        $errorMessage = 'Attenzione il codice destinatario di una privato deve essere di 7 caratteri.';
                        break;
                    case 30;
                        $errorMessage = 'Attenzione nella fattura sono stati utilizzati regimi iva con percentuale nulla, ma in cui non è indicato il riferimento normativo per fatturazione elettronica. Aggiungere il riferimento normativo nella sezione tabelle/regimi iva.';
                        break;
                }
                if($return == true){
                    if ($errorMessage != '')
                        $this->Session->setFlash(($errorMessage), 'custom-danger');
                }else{
                    return false;
                }


            }
            if($return == true)
            {
                // Fatture
                if ($bill['Bill']['tipologia'] == 1)
                    $this->redirect(['controller' => 'bills', 'action' => 'index']);
                // Note di credito
                if ($bill['Bill']['tipologia'] == 3)
                    $this->redirect(['controller' => 'bills', 'action' => 'indexExtendedcreditnotes']);
            }
        }
    }

    public function download_xml()
    {
        $id = $this->params['pass'][1];
    }

    // Invio allegato fattura
    public function sendMailBill()
    {
        $this->autoRender = false;
        $this->send_fatturapdf_mail('FAT');
    }

    // Invio allegato fattura pro-forma
    public function sendMailBillPro()
    {
        $this->autoRender = false;
        $this->send_fatturapdf_mail('PRO');
    }

    // Mail nota di credito
    public function sendMailBillCreditnote()
    {
        $this->autoRender = false;
        $this->send_fatturapdf_mail('NOTACREDITO');
    }

    // Funzione che manda la mail (NUOVA)
    public function send_fatturapdf_mail($type)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $id = $this->params['pass'][0];
        isset($this->params['pass'][1]) ? $extraMail = true : $extraMail = false;
        $bill = $this->Bill->find('first', ['contain' => ['Client' => ['Bank', 'Nation'],'Payment' => ['Bank'], 'Good' => ['Iva']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);
        $numero = $this->Bill->find('first', ['conditions' => ['Bill.id' => $id]]);
        $impostazioni = $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]);
        $dataFattura = date('d-m-Y', strtotime($bill['Bill']['date']));

        $currentDbName = ConnectionManager::getDataSource('default')->config['database'];

        switch ($type)
        {
            case 'FAT':
                $message = "<h3>Cordiale " . $bill['Client']['ragionesociale'] . "</h3><br/> E' stata emessa la vostra fattura che trovate in allegato. <br/><br/>Cordiali Saluti";
                if($_SESSION['Auth']['User']['dbname'] == "login_GE0051")
                    $message = "<h3>Cordiale " . $bill['Client']['ragionesociale'] . "</h3><br/> E' stata emessa la vostra fattura che trovate in allegato. <br/><br/>Ti chiedo di controllare che tutti i dati siano corretti, specialmente il codice fiscale.<br/><br/> Se non hai effettuato il pagamento durante la visita ecco i dati per procedere a un pagamento tramite bonifico: <br/> Intestato a: Raschetti Elisa<br/> Causale: Fattura n. XX<br/> Iban: IT13N0310401625000000220482 <br/><br/> La fattura e' detraibile nella categoria spese sanitarie. E' preferibile che il pagamento venga effettuato dall'intestatario della fattura. <br/><br/> Grazie mille! <br/> A presto! <br/><br/> <strong>Ostetrica Elisa Raschetti</strong><br/> Cell: 3498531108 <br/><br/> _________________________ <br/><br/> Maia - Studio di Arte Ostetrica <br/> Via Empio n.6, Ardenno <br/> www.ostetricamaia.it<br/> ";
                else if($_SESSION['Auth']['User']['dbname'] == 'login_GE0052')
                    $message = "<h3>Cordiale " . $bill['Client']['ragionesociale'] . "</h3><br/> E' stata emessa la vostra fattura che trovate in allegato. <br/><br/>Ti chiedo di controllare che tutti i dati siano corretti, specialmente il codice fiscale.<br/><br/> Se non hai effettuato il pagamento durante la visita ecco i dati per procedere al pagamento tramite bonifico bancario: <br/> Destinatario: BALCONI MARTA<br/> IBAN: IT89J0890151810000000350790 <br/> Causale: Pagamento ricevuta n. (n.ricevuta) <br/><br/> Ti ricordo che la fattura e' detraibile nella categoria spese sanitarie.<br/><br/> E' preferibile che il pagamento venga effettuato dall'intestatario della fattura.  <br/><br/> Grazie mille! <br/> A presto! <br/><br/> <strong>Ost. Marta Balconi</strong><br/> Ostetrica libera professionista,<br/> Consulente Babywearing <br/> Cell: 351 613 0356 <br/><br/> _________________________ <br/><br/> Maia - Studio di Arte Ostetrica <br/> Via Empio n.6, Ardenno <br/> www.ostetricamaia.it <br/> ";
                $okMessage = 'La fattura è stata spedita correttamente via mail al cliente';
            break;
            case 'PRO':
                $message = "<h3>Cordiale " . $bill['Client']['ragionesociale'] . "</h3><br/> E' stata emessa la vostra fattura pro forma che trovate in allegato. <br/><br/>Cordiali Saluti";
                $okMessage = 'La fattura pro-forma è stata spedita correttamente via mail al cliente';
            break;
            case 'NOTACREDITO':
                $message = "<h3>Cordiale " . $bill['Client']['ragionesociale'] . "</h3><br/> E' stata emessa la vostra nota di credito fattura che trovate in allegato. <br/><br/>Cordiali Saluti";
                $okMessage = 'La nota di credito forma è stata spedita correttamente via mail al cliente';
            break;
        }

        try
        {
            $mail = new PHPMailer;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $impostazioni['Setting']['host'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $impostazioni['Setting']['username'];                 // SMTP username
            $mail->Password = $this->Utilities->mydecrypt($impostazioni['Setting']['password'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');
            $mail->SMTPSecure = $impostazioni['Setting']['smtpSecure'];                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $impostazioni['Setting']['port'];                                    // TCP port to connect to
            if (isset($impostazioni['Setting']['email']) && $impostazioni['Setting']['email'] != '') {
                $mail->SetFrom($impostazioni['Setting']['email'], $impostazioni['Setting']['name']);
            } else {
                $mail->SetFrom($impostazioni['Setting']['username'], $impostazioni['Setting']['name']);
            }
            $mail->Timeout = 5;

            // Add a recipient
            if ($extraMail) {
                $mail->addAddress($bill['Client']['extra_mail']);
            } else {
                $mail->addAddress($bill['Client']['mail']);
            }

            $mail->isHTML(true);        // Set email format to HTML
            $mail->Subject = $impostazioni['Setting']['name'] . ' Invio fattura a: ' . $bill['Client']['ragionesociale'];
            $mail->Body = $message;

            if ($type == 'NOTACREDITO') {
                $createdFileName = $this->sendfatturaPdfExtendedcreditnotes($id, $type);
            } else {
                $createdFileName = $this->sendFatturaPdfExtendedvfs($id, $type);
            }

            $mail->addAttachment($createdFileName);

            if ($mail->send() == 1) {
                $this->Utilities->setBillMailSent($bill['Bill']['id']);
                $this->Session->setFlash(($okMessage), 'custom-flash');
            } else {
                $this->Session->setFlash(("Errore durante l'invio della fattura, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
            }
        } catch (Exception $ecc) {
            $this->Session->setFlash(($ecc), 'custom-danger');
            $this->Session->setFlash(("Errore durante l'invio della fattura, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
        }

        unlink($createdFileName);
        $this->redirect($this->referer());
    }

    // Ma serve questa o è sporcizia ? To control
    public function getDefaultClientPaymentMethodId()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        $defaultPaymentMethod = $this->Utilities->getCustomerId($clientId);
        return $defaultPaymentMethod;
    }

    public function getClientCatalog()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->loadModel('Storage');

        // Se siamo nella sezione bolle non devo prendere le voci di descrizione fattura
        // TO DO controllare se comporta errori
        if ($_POST['clientInputId'] == "#TransportClientId") {
            $movable = 'true';
        } else {
            $movable = 'all';
        }

        // Se è uno scontrino l'articolo è per forza movimentabile
        if (isset($_POST['action'])) {
            if ($_POST['action'] == 'addReceipt' || $_POST['action'] == 'editReceipt') {
                $movable = 'true';
            }
        }

        //$clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        //$storages = $this->Utilities->getStoragesWithCatalog($clientId, $movable);

        $arrayReturn = [];

        /*if ($storages != null) // Non è definito un catalogo ne articoli è un fix per quando viene creato un cliente dinamicamente
        {

            foreach ($storages as $storage) {

                $arrayReturn[] =
                    [
                        'label' => $storage['descrizione'],
                        'descrizione' => $storage['descrizione'],
                        'value' => $storage['id'],
                        'prezzo' => $storage['prezzo'],
                        'codice' => $storage['codice'],
                        'unit' => $storage['unit_id'],
                        'vat' => $storage['vat_id'],
                        'maxStorages' => $this->Utilities->getAvailableQuantity($storage['id']),
                        'movable' => $storage['movable'],
                    ];
            }
        } else {
            $storages = $this->Utilities->getStoragesWithVariations(null, $movable);

            foreach ($storages as $storage) {
                $arrayReturn[] =
                    [
                        'label' => $storage['Storages']['descrizione'],
                        'descrizione' => $storage['Storages']['descrizione'],
                        'value' => $storage['Storages']['id'],
                        'prezzo' => $storage['Storages']['prezzo'],
                        'codice' => $storage['Storages']['codice'],
                        'unit' => $storage['Storages']['unit_id'],
                        'vat' => $storage['Storages']['vat_id'],
                        'maxStorages' => $this->Utilities->getAvailableQuantity($storage['Storages']['id']),
                        'movable' => $storage['Storages']['movable'],
                    ];
            }
        }*/

        $storages = $this->Storage->find('all', ['recursive' => -1, 'order' => ['Storage.descrizione' => 'asc'], 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO],'fields'=>['id', 'codice', 'descrizione']]);

        foreach ($storages as $storage){
            $arrayReturn[] = [
                'label' => $storage['Storage']['descrizione'],
                'descrizione' => $storage['Storage']['descrizione'],
                'value' => $storage['Storage']['id'],
                'codice' => $storage['Storage']['codice'],
            ];
        }

        return json_encode($arrayReturn);
    }

    public function getClientAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        is_numeric($_POST['clientId']) ? $clientId = $_POST['clientId'] : $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        $client = $this->Client->find('first', ['conditions' => ['Client.id' => $clientId, 'Client.state' => ATTIVO]]);
        $arrayReturn = [];

        if ($client != null) {

            $arrayReturn[] =
                [
                    'name' => $client['Client']['ragionesociale'],
                    'address' => $client['Client']['indirizzo'],
                    'cap' => $client['Client']['cap'],
                    'city' => $client['Client']['citta'],
                    'province' => $client['Client']['provincia'],
                    'nation' => $client['Client']['nation_id'],
                ];
        } else {
            $arrayReturn[] = ['name' => '', 'address' => '', 'cap' => '', 'city' => '', 'province' => '', 'nation' => 0,];
        }
        return json_encode($arrayReturn);
    }

    public function getCustomerAddressFromId()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        $client = $this->Client->find('first', ['conditions' => ['Client.id' => $_POST['clientId'], 'Client.state' => ATTIVO]]);
        $arrayReturn = [];
        if ($client != null) {
            $arrayReturn[] =
                [
                    'address' => $client['Client']['indirizzo'],
                    'cap' => $client['Client']['cap'],
                    'city' => $client['Client']['citta'],
                    'province' => $client['Client']['provincia'],
                    'nation' => $client['Client']['nation_id'],
                ];
        } else {
            $arrayReturn[] = ['name' => '', 'address' => '', 'cap' => '', 'city' => '', 'province' => '', 'nation' => 0,];
        }
        return json_encode($arrayReturn);
    }

    public function getSupplierAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier']);
        $arrayReturn = [];

        if (isset($_POST['clientId'])) {
            $supplierId = $this->Utilities->getSupplierId($_POST['clientId']);
            $supplier = $this->Supplier->find('first', ['conditions' => ['Supplier.id' => $supplierId, 'company_id' => MYCOMPANY]]);

            if (isset($supplier['Supplier'])) {
                $arrayReturn[] =
                    [
                        'name' => $supplier['Supplier']['name'],
                        'address' => $supplier['Supplier']['indirizzo'],
                        'cap' => $supplier['Supplier']['cap'],
                        'city' => $supplier['Supplier']['city'],
                        'province' => $supplier['Supplier']['province'],
                        'nation' => $supplier['Supplier']['nation_id'],
                    ];
            }
        }

        return json_encode($arrayReturn);
    }

    public function getClientDifferentAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        $addresses = $this->Utilities->getCustomerDifferentAddress($clientId);

        $arrayReturn = [];

        if ($addresses != null) // Non è definito un catalogo ne articoli è un fix per quando viene creato un cliente dinamicamente
        {
            foreach ($addresses as $address) {
                $arrayReturn[] =
                    [
                        'id' => $address['Clientdestination']['id'],
                        'name' => $address['Clientdestination']['name'],
                        'address' => $address['Clientdestination']['address'],
                        'cap' => $address['Clientdestination']['cap'],
                        'city' => $address['Clientdestination']['city'],
                        'province' => $address['Clientdestination']['province'],
                        'nation' => $address['Nation']['name'],
                    ];
            }
        } else {
            /* nothing */
        }

        return json_encode($arrayReturn);
    }

    public function getSupplierDifferentAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $supplierId = $this->Utilities->getSupplierId($_POST['supplierId']);
        $addresses = $this->Utilities->getSupplierDifferentAddress($supplierId);

        $arrayReturn = [];

        if ($addresses != null) // Non è definito un catalogo ne articoli è un fix per quando viene creato un cliente dinamicamente
        {
            foreach ($addresses as $address) {
                $arrayReturn[] =
                    [
                        'id' => $address['Supplierdestination']['id'],
                        'name' => $address['Supplierdestination']['name'],
                        'address' => $address['Supplierdestination']['address'],
                        'cap' => $address['Supplierdestination']['cap'],
                        'city' => $address['Supplierdestination']['city'],
                        'province' => $address['Supplierdestination']['province'],
                        'nation' => $address['Nation']['name'],
                    ];
            }
        } else {
            /* nothing */
        }

        return json_encode($arrayReturn);
    }

    public function getClientDifferentReferredAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        // Recupero i clienti del rivenditore
        $ResellerClient = $this->Utilities->getResellerClient($clientId);

        $arrayReturn = [];

        if ($ResellerClient != null) // Non è definito un catalogo ne articoli è un fix per quando viene creato un cliente dinamicamente
        {
            foreach ($ResellerClient as $clientAddress) {

                isset($clientAddress['Client']['Nation']['name']) ? $nation = $clientAddress['Client']['Nation']['name'] : $nation = '';

                $arrayReturn[] =
                    [
                        'id' => $clientAddress['Client']['id'],
                        'name' => $clientAddress['Client']['ragionesociale'],
                        'address' => $clientAddress['Client']['indirizzo'],
                        'cap' => $clientAddress['Client']['cap'],
                        'city' => $clientAddress['Client']['citta'],
                        'province' => $clientAddress['Client']['provincia'],
                        'nazionalita' => $nation,
                    ];
            }
        } else {
            /* nothing */
        }

        return json_encode($arrayReturn);
    }

    // Recupero tutte le funzioni con metoto di pagamento riba che non siano state emesse e che non siano accontate/saldat
    public function createRibaFlow()
    {
        // Recupero tutte le fatture con metodo di pagamento riba che non siano stat
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bills', 'Deadline']);
        $bills = $this->Utilities->getBills(1, $riba = 1, $emitted = 0);
        $i = -1;
        foreach ($bills as $bill) {
            $i++;
            $deadlines = $this->Utilities->getDeadlines($bill['Bill']['id']);
            $hasPayment = 0;
            foreach ($deadlines as $deadline) {
                $hasPayment += $this->Deadline->getCountPayments($deadline['Deadlines']['id']);
            }
            if ($hasPayment > 0) {
                unset($bills[$i]);
            } else {
                null;
            }
        }
        $this->set('bills', $bills);
        $this->set('banks', $this->Utilities->getBanksList(true));
        $this->set('Utilities', $this->Utilities);
        $this->render('ribaFlow');
    }

    public function createAccountantFlow()
    {
        // Recupero tutte le fatture con metodo di pagamento riba
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bills']);
        $this->paginate($this->set('bills', $this->Bill->find('all', ['contain' => ['Payment' => 'Bank', 'Client' => 'Bank', 'Good' => 'Iva'], 'conditions' => ['Bill.company_id' => MYCOMPANY]])));
        $this->render('accountantFlow');
    }

    // Controllo duplicato fattura
    public function checkBillDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        isset($_POST['sectional']) ? $sectional = $_POST['sectional'] : $sectional = null;
        isset($_POST['supplierName']) ? $supplierName = $_POST['supplierName'] : $supplierName = null;
        isset($_POST['supplierId']) ? $supplierId = $_POST['supplierId'] : $supplierId = null;
        $return = $this->Utilities->checkBillDuplicate($_POST['billnumber'], $_POST['date'], $_POST['type'], $sectional, $supplierName, $supplierId);
        return json_encode($return);
    }

    // Controllo duplicato fattura
    public function checkReceiptDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $return = $this->Utilities->checkReceiptDuplicate($_POST['receiptnumber'], $_POST['date']);
        return json_encode($return);
    }

    public function createAccountantCsv()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bills', 'Clients', 'Suppliers']);

        $enclosure = $fileName = '';

        $clients = $this->Utilities->getCustomers();
        $suppliers = $this->Utilities->getSuppliers();
        // Prendo tutte
        $sellBills = $this->Utilities->getBills(1);

        $buyBills = $this->Utilities->getBuyBills();
        $xBills = $yBills = null;

        if (empty($fileName)) {
            $fileName = "export_" . date("Y-m-d") . ".csv";
        }

        $csvFile = fopen('php://output', 'w');
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        /* Inizio corpo del csv */
        $intestazione = 'I00400000000021790000022820161231V12' . str_repeat(' ', 148) . 'FCLIEFORN' . str_repeat(' ', 191);

        // [1] Tipo di record
        $intestazione = "I";
        // [3] Numero di file trasferiti (numerico 3) // Indica il numero di gruppi trasferiti (tipo facq + fvend + corrisp indicare 003)
        $intestazione .= "NNN";
        // [13] Numero totale record - 2
        $intestazione .= "NNNNNNNNNNNNN";
        // [6] Codice Ditta - deve essere uguale al codice ditta che dà il nome al file se file mono-ditta
        $intestazione .= "000002";
        // [2] Numero spedizione 2 caratteri (numero di spedizione progressiva)
        $intestazione .= "01";
        // [8] Data di spedizione
        $intestazione .= "YYYYMMDD";
        // [1] V [contabilità] o D [ritorno dal cliente]
        $intestazione .= "V";
        // [1] Utente provenienza NOTA: non viene gestito [ metto 1 fisso ]
        $intestazione .= "1";
        // [1] Utente destinatario: [Contabilità ordinaria  metto 2 ]
        $intestazione .= "2";
        // [4]  Num release cont. ordinaria [non viene usato] [4 spazi]
        $intestazione .= str_repeat(' ', 4);
        // 160 Spazi bianchi
        $intestazione .= str_repeat(' ', 160);

        fputcsv($csvFile, [$intestazione]);

        /* CLIENTE */
        foreach ($clients as $client) {
            // [1] Identificativo cliente
            $row = 'D';
            // [2] Identificativo cliente/Fornitori parte I
            $row = '10';
            // [1] C Cliente / F Fornitore
            $row = 'C';
            // [6] Codice cliente fornitore
            $row = str_pad($client['Clients']['Code'], 6, '0', STR_PAD_LEFT);
            // [8] CodSoggCliForn (facoltativo)  corrisponde al codice soggetto da attribuire o già attribuito all'anagrafico unico clienti/fornitori delle contabilità
            $row .= str_repeat(' ', 8);  // Per ora lo lascio vuoto
            // [50] Ragione sociale cliente [ 25 nome ][ 25 congnome ] <-- Lascio ragione sociale
            $row .= str_pad($client['Clients']['ragionesociale'], 50, ' ', STR_PAD_RIGHT);
            // [16] Codice fiscale (spazi a destra perché ci sono le piva)
            $row .= str_pad($client['Clients']['cf'], 16, ' ', STR_PAD_RIGHT);
            $row .= '?'; // [1]  VFCodFisc Indica se il codice fiscale è corretto " " , o no "E"
            $row .= '?'; // [1]  VFSoggIntra "S" Soggettro comunitario " " Soggetto non intracomunitario
            $row .= '?'; // [1]  VFDesDenmn Indica se il campo DesDemn è stato compilato suyddividendo il cognome dal nome oppure se è stata compilata la denominaizone per esteso S (esteso) " " No è stata inserita la denominaizone ma cognome /nome  "S" Non è ammesso se CodTipoSogg (D11) è privato
            $row .= str_repeat(' ', 113); // Spazi obbligatori
            $row .= 'D'; // [1]
            $row .= '11'; // [2]
            $row .= str_pad($client['Clients']['indirizzo'], 32, ' ', STR_PAD_RIGHT); // [32] Nome Via
            $row .= str_pad($client['Clients']['numerocivico'], 5, ' ', STR_PAD_RIGHT); // [5] Numero civico
            $row .= str_pad($client['Clients']['codicecomune'], 4, ' ', STR_PAD_RIGHT); // [4] Codice catastale comune
            $row .= str_pad($client['Clients']['cap'], 5, ' ', STR_PAD_RIGHT); // [5] Cap
            $row .= str_pad($client['Clients']['citta'], 23, ' ', STR_PAD_RIGHT); // [23] Comune di residenza
            $row .= str_pad($client['Clients']['provincia'], 2, ' ', STR_PAD_RIGHT); // [2] Provincia di residenza
            $row .= str_pad($client['Clients']['piva'], 11, ' ', STR_PAD_RIGHT); // Partita iva
            $row .= '?'; //  [1] Partita iva errata " " (corretta)  "E" ( errata )
            $row .= ''; // [1] Tipo soggetto
            $row .= ''; // [1] Flag san marino [S => si] [ " " => NO]
            $row .= ''; // [1] Bolla doganale [S => si] [ " " => NO]
            $row .= ''; // [1] Codice Movim. DR770 1/2/3/4/5  [ 1 => Lavoro autonomo , 2 => Provv.sogg senza dipendenti ,.. 3 ... 4 ... 5]
            $row .= ''; // [1] Flag. Art.110 C.10
            $row .= ''; // [1] Flag. Cliente / Fornitore Fittizzio
            $row .= 'YYYYMMDD'; // [4] Data di nascita

            fputcsv($csvFile, [$row]);
        }


        foreach ($suppliers as $supplier) {
            $row = 'D100F03'; // Identificativo cliente
            $row .= '????'; // Id fornitore
            $row .= str_pad($supplier['Suppliers']['code'], 8, ' ', STR_PAD_RIGHT); // Codice cliente
            $row .= str_pad($supplier['Suppliers']['name'], 50, ' ', STR_PAD_RIGHT); // Ragione sociale cliente
            $row .= str_pad($supplier['Suppliers']['cf'], 16, ' ', STR_PAD_RIGHT); // Ragione sociale cliente
            $row .= ' ' . '?' . '?'; // ?? ( sono due S ma non so che vogliono dire)
            $row .= str_repeat(' ', 113); // Spazi
            $row .= str_pad($supplier['Suppliers']['indirizzo'], 41, ' ', STR_PAD_RIGHT);
            $row .= str_pad($supplier['Suppliers']['cap'], 5, ' ', STR_PAD_RIGHT);
            $row .= str_pad($supplier['Suppliers']['city'], 23, ' ', STR_PAD_RIGHT);
            $row .= str_pad($supplier['Suppliers']['province'], 2, ' ', STR_PAD_RIGHT);
            $row .= str_pad($supplier['Suppliers']['piva'], 11, ' ', STR_PAD_RIGHT); // piva
            $row .= ' ' . '?'; // ?? ( P, S o D o E) Privato ? Società ? Ditta ? Esente
            $row .= '    N00000000';

            fputcsv($csvFile, [$row]);
        }

        /* intestazione clie fornitori */
        $intestazioneFatture = 'ECLIEFORN00001874' . str_repeat(' ', 183) . 'FFATTACQU' . str_repeat(' ', 201);
        fputcsv($csvFile, [$intestazioneFatture]);

        /* FATTURE */
        if ($sellBills != null) {

            foreach ($sellBills as $bill) {
                $imponibile = $iva = $totale = $numeroX1 = 0;

                $row = 'D3000'; // Identificativo fattura di vendita
                $row .= 'YYYYMMYYYYMM' . '???' . '  '; // ??? = a '001' o '011'
                $row .= str_pad($bill['Bill']['numero_fattura'], 7, 0, STR_PAD_LEFT); // piva $bill['Bills']['numero_fattura'] // credo sia il numero fattura
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad('?????????', 7, ' ', STR_PAD_RIGHT);  // 9 Nove numeri ????
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad('YYYYMMDDYYYYMMDD', 34, '0', STR_PAD_RIGHT);  // Due date e 26 0 ????
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad($bill['Bill']['importo'], 10, 0, STR_PAD_LEFT); // Importo <-- Calcolare importo
                $row .= str_repeat(' ', 48); // Spazi
                $row .= str_repeat('0', 13); // Zeri ??
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_repeat('0', 11); // Zeri ??
                $row .= str_repeat(' ', 2); // Spazi
                $row .= 'G1'; // Spazi
                $row .= str_repeat(' ', 3); // Spazi
                $row .= 'YYYY'; // Anno
                $row .= str_pad($bill['Bill']['client_id'], 6, ' ', STR_PAD_LEFT); // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 16); // Spazi
                /* Qui devo ciclare queste scadenze per ogni tipologia di iva della fattura  */
                $row .= 'D31';
                $row .= 'XX'; // Valore iva
                $row .= str_pad($imponibile, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_pad($iva, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= '??'; // Questo due cifre non capisco cosa sono
                $row .= str_pad($numeroX1, 10, '0', STR_PAD_LEFT); // Non so cosa sia questo numero
                $row .= str_pad($totale, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 5); // Spazi
                $row .= str_repeat('0', 15); // Spazi
                $row .= 'N'; // Spazi
                /*  */
                fputcsv($csvFile, [$row]);
            }
        }

        /* intestazione clie fornitori */
        $intestazioneFatture = 'EFATTACQU00000231' . str_repeat(' ', 183) . 'FFATTVEND' . str_repeat(' ', 201);
        fputcsv($csvFile, [$intestazioneFatture]);


        /* FATTURE */
        if ($buyBills != null) {
            foreach ($buyBills as $bill) {
                $imponibile = $iva = $totale = $numeroX1 = 0;

                $row = 'D3000'; // Identificativo fattura di vendita
                $row .= 'YYYYMMYYYYMM' . '???' . '  '; // ??? = a '001' o '011'
                $row .= str_pad($bill['Bill']['numero_fattura'], 7, 0, STR_PAD_LEFT); // piva $bill['Bills']['numero_fattura'] // credo sia il numero fattura
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad('?????????', 7, ' ', STR_PAD_RIGHT);  // 9 Nove numeri ????
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad('YYYYMMDDYYYYMMDD', 34, '0', STR_PAD_RIGHT);  // Due date e 26 0 ????
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_pad($bill['Bill']['importo'], 10, 0, STR_PAD_LEFT); // Importo <-- Calcolare importo
                $row .= str_repeat(' ', 48); // Spazi
                $row .= str_repeat('0', 13); // Zeri ??
                $row .= str_repeat(' ', 1); // Spazi
                $row .= str_repeat('0', 11); // Zeri ??
                $row .= str_repeat(' ', 2); // Spazi
                $row .= 'G1'; // Spazi
                $row .= str_repeat(' ', 3); // Spazi
                $row .= 'YYYY'; // Anno
                $row .= str_pad($bill['Bill']['client_id'], 6, ' ', STR_PAD_LEFT); // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 16); // Spazi
                /* Per ogni iva della fattura di acquisto */
                $row .= 'D31';
                $row .= 'XX'; // Valore iva
                $row .= str_pad($imponibile, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_pad($iva, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= '??'; // Questo due cifre non capisco cosa sono
                $row .= str_pad($numeroX1, 10, '0', STR_PAD_LEFT); // Non so cosa sia questo numero
                $row .= str_pad($totale, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 5); // Spazi
                $row .= str_repeat('0', 15); // Spazi
                $row .= '?'; // N o S

                fputcsv($csvFile, [$row]);
            }
        }

        /* intestazione ???*/
        $intestazioneFatture = 'EFATTVEND00000057' . str_repeat(' ', 183) . 'FCORRNORM' . str_repeat(' ', 201);
        fputcsv($csvFile, [$intestazioneFatture]);

        /* FATTURE */
        if ($xBills != null) {

            foreach ($xBills as $bill) {
                $imponibile = 0;
                $iva = 0;
                $numerox1 = 0;
                $numerox2 = '0000032601';

                $row = 'D4000'; // Identificativo ???
                $row .= 'YYYYMMYYYYMM' . '00CN' . 'YYYYMMDD'; // ??? = a '001' o '011'
                $row .= str_pad($imponibile, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_pad($iva, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 2); // Spazi
                $row .= str_pad($numerox1, 11, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_pad($numerox2, 9, '0', STR_PAD_LEFT) . '+'; // Questo dovrebbe essere il codice cliente
                $row .= str_repeat(' ', 42); // Spazi
                $row .= 'E'; // ????
                $row .= str_repeat(' ', 80); // Spazi
            }

        }

        /* intestazione ???*/
        $intestazioneFatture = 'ECORRNORM00000009' . str_repeat(' ', 183);
        fputcsv($csvFile, [$intestazioneFatture]);

        $numeroX1 = $numeroX2 = 0;

        /* FATTURE */
        if ($yBills != null) {
            foreach ($yBills as $bill) {
                $row = 'C0400'; // Identificativo ??
                $row .= str_pad($numeroX1, 11, '0', STR_PAD_LEFT) . '+'; // ???
                $row .= str_pad($numeroX2, 8, '0', STR_PAD_LEFT) . '+'; // ???
                $row .= 'YYYYMMDD'; // ???
                $row .= 'V12'; // Può essere qualunque cosa
                $row .= str_repeat(' ', 80); // Spazi
            }
        }

        // Ultima riga
        fputcsv($csvFile, [' EF51J430306909051717000007                  00000180000000003015270000000000000000000128                        E']);
        /* Fine corpo del csv */
        fclose($csvFile);
    }

    public function flattenArray($array, &$flatArray, $parentKeys = '')
    {
        foreach ($array as $key => $value) {
            $chainedKey = ($parentKeys !== '') ? $parentKeys . '.' . $key : $key;
            if (is_array($value)) {
                $this->flattenArray($value, $flatArray, $chainedKey);
            } else {
                $flatArray[$chainedKey] = $value;
            }
        }
    }

    public function getKeysForHeaderRow($data)
    {
        $headerRow = [];
        foreach ($data as $key => $value) {
            foreach ($value as $fieldName => $fieldValue) {
                if (array_search($fieldName, $headerRow) === false) {
                    $headerRow[] = $fieldName;
                }
            }
        }

        return $headerRow;
    }

    public function mapAllRowsToHeaderRow($headerRow, $data)
    {
        $newData = array();
        foreach ($data as $intKey => $rowArray) {
            foreach ($headerRow as $headerKey => $columnName) {
                if (!isset($rowArray[$columnName])) {
                    //$rowArray[$columnName] = '';
                    $newData[$intKey][$columnName] = '';
                } else {
                    $newData[$intKey][$columnName] = $rowArray[$columnName];
                }
            }
        }

        return $newData;
    }

    /** DUPLICAZIONE FATTURA */

    public function billDuplicate($id, $type)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Good');
        $this->loadModel('Deadline');
        $conditionArray = ['Bill.company_id' => MYCOMPANY, 'Bill.id' => $id, 'Bill.state' => 1];
        $currentBill = $this->Bill->find('first', ['conditions' => $conditionArray]);
        $currentBill['Bill']['id'] = null;

        $defaultSectional = null;

        if ($type == 1)
            $defaultSectional = $this->Utilities->getDefaultBillSectional();

        if ($type == 3)
            $defaultSectional = $this->Utilities->getDefaultCreditnoteSectional();

        if ($type == 8)
            $defaultSectional = $this->Utilities->getDefaultProformaSectional();

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $currentBill['Bill']['numero_fattura'] = $nextSectionalNumber;

        $currentBill['Bill']['sectional_id'] = $defaultSectional['Sectionals']['id'];
        $currentBill['Bill']['tipologia'] = $type;
        $currentBill['Bill']['state'] = 0;

        /* Fix duplicate */
        $currentBill['Bill']['id_ixfe'] = null;
        $currentBill['Bill']['validation_ixfe'] = null;
        $currentBill['Bill']['processing_state_ixfe'] = null;
        $currentBill['Bill']['state_ixfe'] = null;
        $currentBill['Bill']['mailsent'] = 0;
        $currentBill['Bill']['date'] = date("Y-m-d");
        $currentBill['Bill']['ribaemitted'] = 0;

        $newBillClient = $this->Client->create();
        $newBillClient['Client']['id'] = null;
        $newBillClient['Client']['id'] = $currentBill['Bill']['client_id'];
        $this->Client->save($newBillClient);

        $currentBill['Bill']['Client_id'] = $newBillClient;

        /* fine Fix duplicate */

        $newBill = $this->Bill->Save($currentBill);


        // Duplico le righe
        $conditionGoodArray = ['Good.company_id' => MYCOMPANY, 'Good.bill_id' => $id, 'Good.state' => 1];
        $currentGoodRows = $this->Good->find('all', ['conditions' => $conditionGoodArray]);

        foreach ($currentGoodRows as $row) {
            $newBillGoodRow = $this->Good->create();
            $newBillGoodRow = $row;
            $newBillGoodRow['Good']['id'] = null;
            $newBillGoodRow['Good']['bill_id'] = $newBill['Bill']['id'];
            $newBillGoodRow['Good']['transport_id'] = null;
            $newbillGoodRow['Good']['state'] = 1;
            $this->Good->save($newBillGoodRow);
        }

        // Duplico le scadenze
        $conditionBillDeadlines = ['Deadline.company_id' => MYCOMPANY, 'Deadline.bill_id' => $id, 'Deadline.state' => 1];
        $currentDeadlines = $this->Deadline->find('all', ['conditions' => $conditionBillDeadlines]);

        foreach ($currentDeadlines as $deadline) {
            $newBillDeadline = $this->Deadline->create();
            $newBillDeadline = $deadline;
            $newBillDeadline['Deadline']['id'] = null;
            $newBillDeadline['Deadline']['bill_id'] = $newBill['Bill']['id'];
            $newBillDeadline['Deadline']['state'] = 1;
            $this->Deadline->save($newBillDeadline);
        }

        if ($newBill['Bill']['tipologia'] == 1)
            $this->Utilities->updateBillSectional($newBill['Bill']['id']);

        if ($newBill['Bill']['tipologia'] == 3)
            $this->Utilities->updateCreditnoteSectional($newBill['Bill']['id']);

        if ($newBill['Bill']['tipologia'] == 8)
            $this->Utilities->updateProformaSectional($newBill['Bill']['id']);

        // Mi sposto sulla modifica delle fatture
        switch ($type) {
            case 1:
                $this->redirect(['controller' => 'bills', 'action' => 'edit', $newBill['Bill']['id']]);
            case 2:
                $this->redirect(['controller' => 'bills', 'action' => 'editExtendedBuy', $newBill['Bill']['id']]);
            case 3:
                $this->redirect(['controller' => 'bills', 'action' => 'editExtendedcreditnotes', $newBill['Bill']['id']]);
            case 8:
                $this->redirect(['controller' => 'bills', 'action' => 'editproforma', $newBill['Bill']['id']]);
        }
    }

    /** FINE DUPLICA FATTURA */

    /** RIEPILOGO CATEGORIZZAZIONI  */

    public function resumecategorizesell() { }

    public function resumecategorizebuy() { }

    public function CreateResumeCategorizeSell()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Pdf']);

        if ($_POST) {
            $conditionsArray = ['Bill.date >= ' => date('Y-m-d', strtotime($this->request->data['dateFrom'])), 'Bill.date <= ' => date('Y-m-d', strtotime($this->request->data['dateTo'])), 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia in' => ['1', '3']];
            $bills = $this->Bill->find('all', ['contain' => ['Good' => ['Category', 'Iva' => ['Einvoicevatnature'], 'Storage' => ['Units'], 'Units', 'Transports']], 'recursive' => 3, 'conditions' => $conditionsArray, 'order' => 'date']);
            $this->set('bills', $bills);

            $setting = $this->Setting->GetMySettings();
            $this->set('setting', $setting);

            $this->Pdf->setMpdf("Riepilogo registrazioni vendite", "", $this->Mpdf);
            $this->set('dateFrom', $this->request->data['dateFrom']);
            $this->set('dateTo', $this->request->data['dateTo']);
            $this->set('utilities', $this->Utilities);
        }
    }

    public function CreateResumeCategorizeBuy()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Pdf']);

        if ($_POST) {
            if ($_POST['tipo'] == "datafatt")
                $conditionsArray = ['Bill.date >= ' => date('Y-m-d', strtotime($this->request->data['dateFrom'])), 'Bill.date <= ' => date('Y-m-d', strtotime($this->request->data['dateTo'])), 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia' => '2'];
            else
                $conditionsArray = ['Bill.receive_date >= ' => date('Y-m-d', strtotime($this->request->data['dateFrom'])), 'Bill.receive_date <= ' => date('Y-m-d', strtotime($this->request->data['dateTo'])), 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia' => '2'];

            $bills = $this->Bill->find('all', ['contain' => ['Good' => ['Category', 'Iva' => ['Einvoicevatnature'], 'Storage' => ['Units'], 'Units', 'Transports']], 'recursive' => 3, 'conditions' => $conditionsArray, 'order' => 'date']);
            $setting = $this->Setting->GetMySettings();
            $this->set('bills', $bills);
            $this->set('setting', $setting);
            $this->Pdf->setMpdf("Riepilogo registrazioni acquisti", "", $this->Mpdf);
            $this->set('dateFrom', $this->request->data['dateFrom']);
            $this->set('dateTo', $this->request->data['dateTo']);
            $this->set('utilities', $this->Utilities);
        }
    }

    /** FINE RIEPILOGO CATEGORIZZAZIONI */

    public function getClientDiscount()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        is_numeric($_POST['clientId']) ? $clientId = $_POST['clientId'] : $clientId = $this->Utilities->getCustomerId($_POST['clientId']);
        $client = $this->Client->find('first', ['conditions' => ['Client.id' => $clientId, 'Client.state' => ATTIVO]]);
        $arrayReturn = [];

        if ($client != null) {

            $arrayReturn[] =
                [
                    'name' => $client['Client']['ragionesociale'],
                    'discount' => $client['Client']['discount'],
                ];
        } else {
            $arrayReturn[] = ['name' => '', 'discount' => ''];
        }
        return json_encode($arrayReturn);
    }

    public function createCreditnoteFromBill($billId)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Deadline', 'Ivas', 'Units', 'Clientdestination', 'Setting', 'Currencies', 'Client']);

        $settings = $this->Setting->GetMySettings();

        // Sezionali
        $defaultSectional = $this->Utilities->getDefaultCreditnoteSectional();
        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $nextCreditnoteNumber = $nextSectionalNumber;

        $newCreditnote = $this->Bill->find('first', ['contain' => ['Good' => ['Iva', 'Storage']], 'conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);

        unset($newCreditnote['Bill']['id']);

        $newCreditnote['Bill']['bill_referred_number'] = $newCreditnote['Bill']['numero_fattura'];
        $newCreditnote['Bill']['numero_fattura'] = $nextCreditnoteNumber;
        $newCreditnote['Bill']['bill_referred_date'] = $newCreditnote['Bill']['date'];
        $newCreditnote['Bill']['date'] = date('Y-m-d');
        $newCreditnote['Bill']['tipologia'] = 3;
        $newCreditnote['Bill']['id_ixfe'] = null;
        $newCreditnote['Bill']['validation_ixfe'] = null;
        $newCreditnote['Bill']['state_ixfe'] = null;
        $newCreditnote['Bill']['processing_state_ixfe'] = null;
        $newCreditnote['Bill']['sectional_id'] = $defaultSectional['Sectionals']['id'];

        if ($newBill = $this->Bill->save($newCreditnote['Bill'])) {
            $newBill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

            $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
            $arrayIva = $arrayImponibili = [];

            foreach ($newCreditnote['Good'] as $key => $good) {
                $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
                $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

                // Calcolo i totali
                if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null) {
                    $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
                } else {
                    $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);
                }

                if (isset($newVat['Iva']['percentuale'])) {
                    $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                }
                $imponibile_iva += $prezzo_riga;
                if (isset($newVat['Iva']['percentuale'])) {
                    $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                    isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
                }
                isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

                if (!$this->Utilities->isANote($good, 'bill')) {
                    if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                        $newStorage = $this->Utilities->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                        $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $newBillGood['Good']['id']]);
                    }
                }

                // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
                isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
                if (isset($good['withholdingapplied'])) {
                    if ($good['withholdingapplied'] == 0) {
                        if ($good['quantita'] != 0) {
                            $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                        }
                    }
                }
            }

            // Non metto le spese d'incasso sulla nota di credito ?

            // Inizio : cassa previdenziale
            if (isset($newBill['Bill']['welfare_box_percentage'])) {
                $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
                //$welfareBoxTotal = number_format($imponibile_iva * $newBill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                $imponibile_iva += $welfareBoxTotal;
                $importo_iva += number_format($welfareBoxTotal * $this->Utilities->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
            }
            // Fine : cassa previdenziale

            isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;

            // Arrotondamento
            (isset($newCreditnote['Bill']['rounding']) && $newCreditnote['Bill']['rounding'] != null) ? $rounding = $newCreditnote['Bill']['rounding'] : $rounding = 0;

            //  Calcolo della ritenuta d'acconto
            if (isset($newBill['Bill']['withholding_tax'])) {
                if ($newBill['Bill']['withholding_tax'] > 0) {
                    $totaleRitenutaAcconto = $newBill['Bill']['withholding_tax'] * $imponibile_iva / 100;
                    // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                    if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal)) {
                        $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($newBill['Bill']['withholding_tax'] * $welfareBoxTotal / 100);
                    }

                    // Risoluzione ritenuta d'acconto
                    if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0) {
                        $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $newCreditnote['Bill']['withholding_tax'] / 100);
                    }
                } else {
                    $totaleRitenutaAcconto = 0;
                }
            } else {
                $totaleRitenutaAcconto = 0;
            }

            // SEAL OLD VS SEAL NEW !! NON CE LO SPLIT PAYMENT
            if (strtotime(date("Y-m-d", strtotime($newCreditnote['Bill']['date']))) < strtotime(date('2019-01-01'))) {
                // $totalForDeadline = $importo_iva + $imponibile_iva   + $seal - $totaleRitenutaAcconto;
                $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
            } else {
                // $totalForDeadline = $importo_iva + $imponibile_iva   - $totaleRitenutaAcconto ;
                $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;
            }

            $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

            $this->Utilities->setDeadlines($newCreditnote['Bill']['date'], $newCreditnote['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], 0, false);

            // Aggiorno il sezionale
            $this->Utilities->updateCreditNoteSectional($newBill['Bill']['id']);

            $this->Session->setFlash(__('La nota di credito è stata salvata'), 'custom-flash');
            $this->redirect(['action' => 'editExtendedcreditnotes', $newBill['Bill']['id']]);
        }
    }

    public function ebillImportFromXml()
    {
        $this->loadModel('Utilities');
        $this->loadModel('Currencies');
        $this->loadModel('Good');

        if ($this->request->is('post'))
        {
            $filename = $this->data['Bills']['file']['name'];
            $tmpext = explode(".", $filename);
            $ext = end($tmpext);

            $file = file_get_contents($this->request->data['Bills']['file']['tmp_name']);
            $xmlelement = new SimpleXMLElement($file);
            $bills = $xmlelement->Invoices->Invoice;

            foreach ($bills as $billxml)
            {
                $conditionsArray = [
                    'Bill.numero_fattura' => (string)$billxml->attributes()->invoicenumber,
                    'Bill.date' => (string)$billxml->InvoiceDate,
                    'Bill.state' => 1,
                ];

                $existBill = $this->Bill->find('all', ['conditions' => $conditionsArray]);

                if(count($existBill) == 0)
                {
                    $bill = array();
                    $this->Bill->create();
                    $bill['Bill']['numero_fattura'] = (string)$billxml->attributes()->invoicenumber;
                    $bill['Bill']['date'] = (string)$billxml->InvoiceDate;
                    $bill['Bill']['note'] = (string)$billxml->Description;
                    $bill['Bill']['document_causal'] = (string)$billxml->YourRef;
                    //$bill['Bill']['client_id'] = (string)$billxml->InvoiceTo->attributes()->code;
                    $clientId = $this->Utilities->getClientIdFromClientName((string)$billxml->InvoiceTo->Name);
                    $bill['Bill']['client_id'] = $clientId;
                    $bill['Bill']['importo'] = (string)$billxml->ForeignAmount->Value;
                    $totalForDeadline = $bill['Bill']['importo'];
                    if(isset($billxml->EntryDiscount->Percentage)){
                        $bill['Bill']['discount'] = (string)$billxml->EntryDiscount->Percentage;
                    }else{
                        $bill['Bill']['discount'] = null;
                    }
                    if ((string)$billxml->attributes()->type == 8020)
                        $bill['Bill']['tipologia'] = 1;
                    else
                        $bill['Bill']['tipologia'] = 3;
                    $bill['Bill']['company_id'] = MYCOMPANY;
                    $bill['Bill']['payment_id'] = (string)$billxml->PaymentCondition->attributes()->code;

                    $client = $this->Client->find('first', ['conditions' => ['Client.id' => $bill['Bill']['client_id']]]);
                    if (isset($client['Client'])) {
                        $bill['Bill']['client_name'] = str_replace("'", " ", $client['Client']['ragionesociale']);
                        $bill['Bill']['client_address'] = str_replace("'", " ", $client['Client']['indirizzo']);
                        $bill['Bill']['client_cap'] = $client['Client']['cap'];
                        $bill['Bill']['client_city'] = str_replace("'", " ", $client['Client']['citta']);
                        $bill['Bill']['client_province'] = $client['Client']['provincia'];
                        $bill['Bill']['client_nation'] = $client['Client']['nation_id'];
                    } else {
                        $bill['Bill']['client_name'] = str_replace("'", " ", (string)$billxml->InvoiceTo->Name);
                        $bill['Bill']['client_address'] = str_replace("'", " ", (string)$billxml->DeliveryAddress->attributes()->AddressLine1);
                        $bill['Bill']['client_cap'] = (string)$billxml->DeliveryAddress->attributes()->PostalCode;
                        $bill['Bill']['client_city'] = str_replace("'", " ", (string)$billxml->DeliveryAddress->attributes()->City);
                        $bill['Bill']['client_province'] = (string)$billxml->DeliveryAddress->attributes()->StateCode;
                        $bill['Bill']['client_nation'] = $this->Utilities->getNationIdFromShortCode((string)$billxml->DeliveryAddress->attributes()->CountryCode);
                    }

                    $bill['Bill']['client_nation'] = $this->Utilities->getNationIdFromShortCode((string)$billxml->DeliveryAddress->attributes()->CountryCode);
                    $sectional = $this->Utilities->getDefaultBillSectional();
                    $bill['Bill']['sectional_id'] = $sectional['Sectionals']['id'];
                    $bill['Bill']['electronic_invoice'] = 1;
                    $bill['Bill']['einvoicevatesigibility'] = 'I';
                    $bill['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode((string)$billxml->ForeignAmount->Currency->attributes()->code);
                    $invoiceLines = $billxml->InvoiceLine;
                    $newBill = $this->Bill->save($bill['Bill']);
                    $prova = false;
                    foreach ($invoiceLines as $line) {
                        $good1 = array();

                        if(!$prova && $bill['Bill']['document_causal'] != '')
                        {
                            $good1['oggetto'] = (string)$billxml->YourRef;
                            $good1['quantita'] = 0;
                            $good1['prezzo'] = 0;
                            $good1['iva_id'] = 99;
                            $good1['bill_id'] = $newBill['Bill']['id'];
                            $good1['codice'] = 0;
                            $good1['company_id'] = MYCOMPANY;
                            $good1['state'] = ATTIVO;
                            $good1['line_number'] = 1;
                            $prova = true;
                            $newBillGood1 = $this->Utilities->createBillGood($newBill['Bill']['id'], $good1);
                        }

                        $good = array();
                        $good['oggetto'] = (string)$line->Item->Description;
                        $good['quantita'] = abs((int)$line->Quantity);
                        $good['prezzo'] = (string)$line->ForeignAmount->Value / (string)$line->Quantity;

                        if(isset($line->UnitPrice->VAT)) { //controllo per evitare di duplicare fatture senza iva_id
                            $good['iva_id'] = (string)$line->UnitPrice->VAT->attributes()->code;
                        }
                        else{
                            $good['iva_id'] = 0;
                        }

                        if($good['iva_id'] == 0){
                            $good['iva_id'] = 18;
                        }
                        $good['bill_id'] = $newBill['Bill']['id'];
                        $storageId = $this->Utilities->getStorageByCode((string)$line->Item->attributes()->code);
                        if(isset($storageId['Storages'])){
                            $good['storage_id'] = $storageId['Storages']['id'];
                        }

                        $good['codice'] = (string)$line->Item->attributes()->code;
                        /*if(strpos((string)$line->DiscountPercentage, 'E-') == false){
                            if((double)$line->DiscountPercentage > 0){
                                if($line->DiscountPercentage > 1){
                                    $good['customdescription'] = '. Applicato sconto del: '.number_format((double)$line->DiscountPercentage, 2, ',', '').'%';
                                }else{
                                    $scontoReale = number_format((double)$line->DiscountPercentage*100, 2, ',', '');
                                    $good['customdescription'] = '. Applicato sconto del: '.$scontoReale.'%';
                                }
                            }
                        }*/

                        $good['discount'] = (string)$billxml->EntryDiscount->Percentage;
                        $good['unita'] = $this->Utilities->getUnitsIdFromName((string)$line->Unit->attributes()->code);
                        $good['company_id'] = MYCOMPANY;
                        $good['state'] = ATTIVO;
                        $good['line_number'] = (string)$line->attributes()->line;

                        $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
                    }
                    $this->Utilities->setDeadlines($newBill['Bill']['date'], $newBill['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $newBill['Bill']['id'], false);
                }
            }

            $this->Session->setFlash(__('Le fatture sono state importate'), 'custom-flash');
            $this->redirect(['action' => 'index']);

        }
    }

    public function createInvoiceFromXML($billxml)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Currencies');
        $this->Render(false);
        $bill = array();
        $this->Bill->create();
        $bill['Bill']['numero_fattura'] = (string)$billxml->attributes()->invoicenumber;
        $bill['Bill']['date'] = (string)$billxml->InvoiceDate;
        $bill['Bill']['note'] = (string)$billxml->Description;
        $bill['Bill']['document_causal'] = (string)$billxml->YourRef;
        $bill['Bill']['client_id'] = (string)$billxml->InvoiceTo->attributes()->code;
        $bill['Bill']['importo'] = (string)$billxml->ForeignAmount->Value;
        if ((string)$billxml->attributes()->type == 8020)
            $bill['Bill']['tipologia'] = 1;
        else
            $bill['Bill']['tipologia'] = 3;
        $bill['Bill']['company_id'] = MYCOMPANY;
        $bill['Bill']['payment_id'] = (string)$billxml->PaymentCondition->attributes()->code;

        $client = $this->Client->find('first', ['conditions' => ['Client.id' => $bill['Bill']['client_id']]]);
        if (isset($client['Client'])) {
            $bill['Bill']['client_name'] = str_replace("'", " ", $client['Client']['ragionesociale']);
            $bill['Bill']['client_address'] = str_replace("'", " ", $client['Client']['indirizzo']);
            $bill['Bill']['client_cap'] = $client['Client']['cap'];
            $bill['Bill']['client_city'] = str_replace("'", " ", $client['Client']['citta']);
            $bill['Bill']['client_province'] = $client['Client']['provincia'];
            $bill['Bill']['client_nation'] = $client['Client']['nation_id'];
        } else {
            $bill['Bill']['client_name'] = str_replace("'", " ", (string)$billxml->InvoiceTo->Name);
            $bill['Bill']['client_address'] = str_replace("'", " ", (string)$billxml->DeliveryAddress->attributes()->AddressLine1);
            $bill['Bill']['client_cap'] = (string)$billxml->DeliveryAddress->attributes()->PostalCode;
            $bill['Bill']['client_city'] = str_replace("'", " ", (string)$billxml->DeliveryAddress->attributes()->City);
            $bill['Bill']['client_province'] = (string)$billxml->DeliveryAddress->attributes()->StateCode;
            $bill['Bill']['client_nation'] = $this->Utilities->getNationIdFromShortCode((string)$billxml->DeliveryAddress->attributes()->CountryCode);
        }

        $bill['Bill']['client_nation'] = $this->Utilities->getNationIdFromShortCode((string)$billxml->DeliveryAddress->attributes()->CountryCode);
        $sectional = $this->Utilities->getDefaultBillSectional();
        $bill['Bill']['sectional_id'] = $sectional['Sectionals']['id'];
        $bill['Bill']['electronic_invoice'] = 1;
        $bill['Bill']['einvoicevatesigibility'] = 'I';
        $bill['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode((string)$billxml->ForeignAmount->Currency->attributes()->code);
        $invoiceLines = $billxml->InvoiceLine;
        $newBill = $this->Bill->save($bill['Bill']);
        $prova = false;
        foreach ($invoiceLines as $line) {
            $good1 = array();

            if(!$prova && $bill['Bill']['document_causal'] != '')
            {
                $good1['oggetto'] = (string)$billxml->YourRef;
                $good1['quantita'] = 0;
                $good1['prezzo'] = 0;
                $good1['iva_id'] = 99;
                $good1['bill_id'] = $newBill['Bill']['id'];
                $good1['codice'] = 0;
                $good1['company_id'] = MYCOMPANY;
                $good1['state'] = ATTIVO;
                $good1['line_number'] = 1;
                $prova = true;
                $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good1);
            }

            $good = array();
            $good['oggetto'] = (string)$line->Item->Description;
            $good['quantita'] = abs((int)$line->Quantity);
            $good['prezzo'] = (string)$line->ForeignAmount->Value / (string)$line->Quantity;
            $good['iva_id'] = (string)$line->UnitPrice->VAT->attributes()->code;
            $good['bill_id'] = $newBill['Bill']['id'];
            $storageId = $this->Utilities->getStorageByCode((string)$line->Item->attributes()->code);
            $good['storage_id'] = $storageId['Storages']['id'];

            $good['codice'] = (string)$line->Item->attributes()->code;
            $good['discount'] = (string)$billxml->EntryDiscount->Percentage;
            $good['unita'] = $this->Utilities->getUnitsIdFromName((string)$line->Unit->attributes()->code);
            $good['company_id'] = MYCOMPANY;
            $good['state'] = ATTIVO;
            $good['line_number'] = (string)$line->attributes()->line;

            $newBillGood = $this->Utilities->createBillGood($newBill['Bill']['id'], $good);
        }
    }

    public function ebillsend()
    {
        $this->loadModel('Utilities');

        $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        $this->set('clients', $this->Client->getAdvancedList());
    }

    public function sendmultibills()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Setting']);

        $dateFrom = date('Y-m-d', strtotime($_POST['dateFrom']));
        $dateTo = date('Y-m-d', strtotime($_POST['dateTo']));

        if ($_POST['clientId'] != '')
            $conditionsArray = ['date >=' => $dateFrom, 'date <=' => $dateTo, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia' => 1, 'OR' => ['id_ixfe IS NULL', 'id_ixfe' => ''], 'client_name' => $_POST['clientId']];
        else
            $conditionsArray = ['date >=' => $dateFrom, 'date <=' => $dateTo, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'tipologia' => 1, 'OR' => ['id_ixfe IS NULL', 'id_ixfe' => '']];

        $bills = $this->Bill->find('all', ['conditions' => $conditionsArray]);


        $message = "Le seguenti fatture sono state importate correttamente: <br>";

        foreach ($bills as $bill)
        {
            $return = $this->sendBillToIxfe($bill['Bill']['id'], false);

            if ($return == false)
                $message = "Le seguenti fatture non sono state importate" . "- " . $bill['Bill']['numero_fattura'] . " <br>";
        }

        echo $message;
    }

    public function  specialIndex(){

    }
}
