<?php
App::uses('AppController', 'Controller');
class WithholdingController extends AppController
{
    public function index()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Withholding', 'Csv']);

        $conditionsArray = ['Withholding.state' => ATTIVO];
        $filterableFields = ['description', null,null];
        $sortableFields = [['code', 'Codice cassa previdenziale'],['description', 'Cassa previdenziale'], ['#actions']];

        if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        $this->set('filterableFields', $filterableFields);
        $this->set('sortableFields', $sortableFields);

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Withholdings->find('all', ['conditions' => $conditionsArray, 'order' => ['Withholding.description' => 'asc']]);
            echo 'Cassa previdenziale' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                echo $xlsRow['Withholding']['description'] . ';' . "\r\n";
            }
        } else {
            $this->paginate = ['conditions' => $conditionsArray];
            $this->set('einvoice_welfare_box', $this->paginate());
        }
    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Withholding','Messages']);
        $messageArray = ["La", "Cassa previdenziale", "F"];
        if ($this->request->is('post')) {
            $this->Withholding->create();
            $this->request->data['Withholding']['state_id'] = ATTIVO;
            if ($this->Withholding->save($this->request->data)) {
                $this->Session->setFlash(__($this->Messages->successOfAdd($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__($this->Messages->failOfAdd($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
            }
        }
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Withholding','Messages']);
        $messageArray = ["La", "Cassa previdenziale", "F"];
        $this->Withholding->id = $id;
        if (!$this->Withholding->exists()) {
            throw new Exception($this->Messages->notFound($messageArray [0], $messageArray [1], $messageArray [2]));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Withholding->save($this->request->data)) {
                $this->Session->setFlash(__($this->Messages->successOfUpdate($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__($this->Messages->failOfUpdate($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
            }
        } else {
            $this->request->data = $this->Withholding->read(null, $id);
        }
    }


    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Messages', 'Withholding']);

        $messageArray = ["La", "Cassa previdenziale", "F"];
        if ($this->Withholding->isHidden($id))
            throw new Exception($this->Messages->notFound($messageArray [0], $messageArray [1], $messageArray [2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Withholding->find('first', ['conditions' => ['Withholding.id' => $id]]);
        if ($this->Withholding->hide($currentDeleted['Withholding']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($messageArray[0], $messageArray[1], $messageArray[2])), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }
}
