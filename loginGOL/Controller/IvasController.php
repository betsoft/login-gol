<?php
App::uses('AppController', 'Controller');

class IvasController extends AppController {


	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalog','Iva','Csv']);

		$conditionsArray =['Iva.company_id' => MYCOMPANY,'Iva.state'=>ATTIVO];
        $filterableFields = [null,null,'descrizione','codice',null];
        $sortableFields = [['null','Percentuale'],['nature','Natura'],['descrizione','Regime iva'],['code', 'Codice'],['#actions']];
		
		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
			
			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}
		
		$this->set('filterableFields',$filterableFields);
		$this->set('sortableFields',$sortableFields);

		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Iva->find('all',['conditions'=>$conditionsArray,'order' => ['Iva.descrizione' => 'asc']]); 			
			echo 'Percentuale;Regime iva;Codice;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				echo $xlsRow['Iva']['percentuale']. ';' .$xlsRow['Iva']['descrizione']. ';'.$xlsRow['Iva']['codice']. ';'."\r\n";
			}
		}
		else
		{
			$this->Iva->recursive = 0;
			$this->paginate = ['conditions' => $conditionsArray,'order'=>'percentuale asc'];
			$this->set('ivas', $this->paginate());
		}
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Iva->create();
				$this->request->data['Iva']['company_id']=MYCOMPANY;
			
			if ($this->Iva->save($this->request->data)) {

				$this->Session->setFlash(__('IVA salvata'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\'iva non è stata salvata, riprovare'), 'custom-danger');
			}
		}
		
		
		$this->loadModel('Utilities');
		$this->set('einvoicenatures',$this->Utilities->getEinvoicevatnature());
	}

	public function edit($id = null) {
		$this->Iva->id = $id;
		if (!$this->Iva->exists()) {
			throw new NotFoundException(__('IVA non valida'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Iva->save($this->request->data)) {
				$this->Session->setFlash(__('IVA salvata'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\'iva non è stata salvata, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Iva->read(null, $id);
		}
		
		$this->loadModel('Utilities');
		$this->set('einvoicenatures',$this->Utilities->getEinvoicevatnature());
	}
	

	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Iva','Messages']);

        $asg =  ["il","Regime iva","M"];
		if($this->Iva->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Iva->find('first',['conditions'=>['Iva.id'=>$id,'Iva.company_id'=>MYCOMPANY]]);
        if ($this->Iva->hide($currentDeleted['Iva']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}
}
