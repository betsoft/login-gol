<?php

class UsersController extends AppController {

	var $uses=array('User','Company');

    public function beforeFilter()
	{
		parent::beforeFilter();
	}

	public function login($createDemo = '')
	{
		$this->set('createDemo', $createDemo);

		if($createDemo == '')
		{
			$this->layout = 'loginLayout';
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this,['Groups']);
			$this->Utilities->loadModels($this,['Users']);
		}

		$this->Utilities->query('SET GLOBAL sql_mode = "" ');

		if($createDemo == 'demo')
		{
			$conditionsPresi = ['state' => 1];
			$utentiPresi = $this->User->find('all', ['conditions' => $conditionsPresi, 'fields' => 'username']);
			$this->set('usernames', $utentiPresi);
		}

		if ($this->request->is('post'))
		{
			if($createDemo == 'demo')
			{
				$conditions = ['User.username' => $this->data['User']['username']];

				if($this->User->hasAny($conditions))
				{
					$this->User->updateAll(['state' => 1, 'PayPal' => '\''.$this->data['User']['paypal'].'\''], ['username' => $this->data['User']['username']]);
					$this->Session->setFlash(__('Ambiente demo creato correttamente'), 'custom-flash');
					$this->redirect(['action'=>'login']);
				}
				else
				{
					$this->createNewDemoDb($this->data['User']);
				}
			}

			if ($this->Auth->login())
			{
                $user = $this->Users->find('first',['conditions' => ['id' => $_SESSION['Auth']['User']['id']]]);

                if(isset($user['Users']['PayPal']) && $user['Users']['PayPal'] != '')
                {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE); // da disattivare per live
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);// da disattivare per live

                    /*
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api-m.paypal.com/v1/oauth2/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Basic QWVkeGV0RG51WFNsc05vMVJ5TGVkdWx6X1NUZ2ZDRXcwTXkyRDRlUHIxLW5lUEcxWkhxUUNBQUZRQTRGWnNXRmZKWnE3NUVNWmFUaVUtbTI6RUZYOW5BUHpSTDFNXzNySUdsMVVFanRwQnF1WUhvbnRBVHJqR0VwaGcxZWcxYUtPUkxQaGg3SFpUcUZvalJtbTRSUUZHdURfdDJJQ1JpemE=',
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                    ));
                    */

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/oauth2/token',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Basic QVNQVkFORlRfemF1bU1fOURRMTAyY1c0RkVxVm1hckpTZ3MzYVl4YzlVY1ZRUkRpZlJIR25qRGhtT0p5dTlGWkw5MUhuaVh6TFYyNUNobkQ6RVBGVGhhR01rbTczOGV3Zm9tQkpQM3lCNEVHTnJJMVg5Wk5Pd1h0MTdLMV9wVFdSQXNGSXBNVVRHTVRPY3A2MUJlNGFsLTJFNkNPMFZsdU4=',
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);
                    $arrayToken = json_decode($response, true);
                    $tokenPayPal = $arrayToken['access_token'];

                    $paypalIdSub = $user['Users']['PayPal'];
                    $curl2 = curl_init();
                    curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, FALSE); // da disattivare per live
                    curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, FALSE);// da disattivare per live
                    curl_setopt_array($curl2, array(
                        CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/billing/subscriptions/'.$paypalIdSub.'',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Bearer '.$tokenPayPal.'',
                            'Content-Type: application/json'
                        ),
                    ));

                    /*
                    $curl2 = curl_init();
                    curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, FALSE); // da disattivare per live
                    curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, FALSE);// da disattivare per live
                    curl_setopt_array($curl2, array(
                        CURLOPT_URL => 'https://api-m.paypal.com/v1/billing/subscriptions/'.$paypalIdSub.'',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Bearer '.$tokenPayPal.'',
                            'Content-Type: application/json'
                        ),
                    ));
                    */

                    $response = curl_exec($curl2);
                    curl_close($curl2);
                    $arrayStato = json_decode($response, true);

                    if($arrayStato['status'] != "ACTIVE")
                    {
                        $user['Users']['state'] = 0;
                        $this->Users->save($user);
                    }
                }

                if(isset($user['Users']['state']) && $user['Users']['state'] == 0)
                {
                    $this->Session->setFlash(__('Hai messo in pausa la tua utenza'), 'custom-danger');
                }
                else
					{
						// Controllo utente loggato
						switch($_SESSION['Auth']['User']['group_id'])
						{
							case '20': // Login Tecnici
								$this->redirect(['controller'=>'maintenances','action'=>'technicianindex']);
							break;
							case '50': // Login Tecnici
								$this->redirect(['controller'=>'bills','action'=>'specialIndex']);
							break;
							default:
								$this->redirect($this->Auth->redirect());
							break;
						}
					}
			}
			else
			{
				$this->Session->setFlash(__('Username o password non validi, riprovare.'), 'custom-danger');
			}
		}
	}

	public function registration()
	{
		if ($this->request->is('post'))
		{
		}
	}

	public function logout()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Users']);

		session_destroy();
		$this->redirect($this->Auth->logout());
	}

    public function index()
    {
		$this->paginate = ['conditions' => [ 'User.company_id' => MYCOMPANY ],'order'=>['User.id'=>'desc'],'limit' => 100];
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
    }

	public function indexExtended()
    {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalog']);

		$conditionsArray =[ 'User.company_id' => MYCOMPANY ];
		$filterableFields = ['username',null,null];

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}

		$this->set('filterableFields',$filterableFields);
		$this->paginate = ['conditions' => $conditionsArray ,'order'=>['User.id'=>'desc'],'limit' => 100];
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
        $this->render('index_extended');
    }

    public function indexExtendedVfs()
    {
		$this->paginate = ['conditions' => [ 'User.company_id' => MYCOMPANY ],'order'=>['User.id'=>'desc'],'limit' => 100];
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
		$this->render('index_extended_vfs');
    }

    public function add()
	{
        if ($this->request->is('post'))
        {
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
            $this->User->create();
			$comp2=$this->Company->find('first',['fields'=>['Company.id']]);
			$this->request->data['User']['company_id']=$comp2['Company']['id'];

			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'index']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
    }

	public function addExtended()
	{
		if ($this->request->is('post'))
		{
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
			$this->User->create();

			$comp2 = $this->Company->find('first', array('fields' => array('Company.id')));
			$this->request->data['User']['company_id'] = MYCOMPANY;

			if ($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtended'));
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
		}

		$this->loadModel('Utilities');
		$this->set('groups', $this->Utilities->getCompanyUserGroup());
		$this->render('add_extended');
	}

	public function addExtendedVfs()
	{
        if ($this->request->is('post'))
        {
			$this->loadModel('Utilities');
			$this->Utilities->loadModels($this, ['Messages', 'Users']);
			$messageParameter = ["l\'", "utente", "M"];
			$this->User->create();
			$comp2 = $this->Company->find('first', array('fields' => array('Company.id')));
			$this->request->data['User']['company_id'] = MYCOMPANY;

			if ($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtendedVfs'));
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
		}

        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('add_extended_vfs');
    }

    public function edit($id = null)
    {
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
    	// Mi connetto al database globale
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Users']);

        $this->User->id = $id;
        if (!$this->User->exists())
        {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				$currentUser = $this->User->find('first',['conditions'=>['User.id'=>$id,'User.company_id'=>MYCOMPANY]]);

				// Prendo i dati dell'utente collegato
				$currentPassword = $currentUser['User']['password'];
				$currentCompanyId = $currentUser['User']['company_id'];
				$currentUsername = $currentUser['User']['username'];

				// Se il salvataggio funziona correttamente
				if ($currentUser = $this->User->save($this->request->data))
				{
					// Recupero la nuova password criptata
					$newPassword = $currentUser['User']['password'];

					// Connetto al database principale
					$this->Utilities->connectToMainDb();

					// Recupero l'utente dal database principale
					$mainUser = $this->Users->find('first',['conditions'=>['password'=>$currentPassword,'usercompany'=>$currentCompanyId, 'username'=>$currentUsername]]);

					// Aggiorno la password sull'utente principale
					$this->Users->updateAll(['password'=>'\''.$newPassword.'\''],['id'=>$mainUser['Users']['id']]);

					// Riconnetto al database utilizzo
					$this->Utilities->connectToDb($mainUser['Users']['dbname']);

					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'index']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }

    public function editExtended($id = null)
	{
        $this->User->id = $id;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
        if (!$this->User->exists())
        {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(['action' => 'indexExtended']);
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }

        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('edit_extended');
    }

	public function editExtendedVfs($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];
        if (!$this->User->exists())
        {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1],$messageParameter[2]));
        }
        if ($this->request->is('post') || $this->request->is('put'))
		{
			if($this->request->data['User']['password'] != $this->request->data['User']['password_confirm'])
			{
				$this->Session->setFlash(__('Le password non corrispondono'), 'custom-danger');
				$this->redirect($this->referer());
			}
			else
			{
				if ($this->User->save($this->request->data))
				{
					$this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
					$this->redirect(array('action' => 'indexExtendedVfs'));
				}
				else
				{
					$this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
				}
			}
        }
		else
		{
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }

        $this->loadModel('Utilities');
        $this->set('groups',$this->Utilities->getCompanyUserGroup());
        $this->render('edit_extended_vfs');
    }

    public function delete($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this, ['Messages', 'Users']);
		$messageParameter = ["l\'", "utente", "M"];

        if (!$this->request->is('post'))
        {
            throw new MethodNotAllowedException();
        }

        $this->User->id = $id;
        if (!$this->User->exists())
        {
			throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
        }

        if ($this->User->delete($id, false))
        {
			$this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            $this->redirect(['action' => 'index']);
        }

		$this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
        $this->redirect(['action' => 'index']);
    }

    public function createUserFromTechnician()
	{
		$this->autoRender = false;

		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Utilities','Technician','User']);

		$randomPassword = $this->Utilities->getRandomPassword();
		$currentTechnician = $this->Technician->find('first', ['conditions' => ['id' => $_POST['technicianId']]]);
		$dbName = $_SESSION['Auth']['User']['dbname'];
		$dbUser = $_SESSION['Auth']['User']['dbuser'];
		$dbPassword = $_SESSION['Auth']['User']['dbpassword'];
		$email = $currentTechnician['Technician']['email'];

		$this->Utilities->connectToDb(MAINDB);
		$newMainUser = $this->User->create();
		$newMainUser ['username'] = $email;
		$newMainUser ['password'] = $randomPassword;
		$newMainUser ['group_id'] = 20;
		$newMainUser ['dbname'] = $dbName;
		$newMainUser ['dbpassword'] = $dbPassword;
		$newMainUser ['dbuser'] = $dbUser;
		$newMainUser ['usercompany'] = MYCOMPANY;
		$newMainUser ['tare'] = 0;
		$this->User->Save($newMainUser);

		$this->Utilities->connectToDb($dbName);
		$newUser = $this->User->create();
		$newUser['username'] = $email;
		$newUser['password'] = $randomPassword;
		$newUser['group_id'] = 20;
		$newUser['company_id'] = MYCOMPANY;
		$this->User->Save($newUser);
	}

	public function enableUserFromTechnician()
	{
		$this->autoRender = false;
		$this->User->enableUserFromTechnician();
	}

	public function disableUserFromTechnician()
	{
		$this->autoRender = false;
		$this->User->disableUserFromTechnician();
	}

	public function createNewDemoDb($user)
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Utilities','Technician','User']);
		// Get next database name
		$newDemoDbName = $this->getNextDbName('login_GE');
		// Create new database demo
		$this->createDatabase('demo_demo', $newDemoDbName);

		$newMainUser = $this->createUserMainDb($user, $newDemoDbName);
		$user['password'] = $newMainUser['User']['password'];
		//$this->createUserDb($user, $newDemoDbName);

		$this->Session->setFlash(__('Ambiente demo creato correttamente'), 'custom-flash');
		$this->redirect(['action'=>'login']);
	}

	public function activateDemoDb($user)
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Utilities','Technician','User']);

		$oldDbName = $user['dbname'];
		// Get next database name
		$newDbName = $this->getNextDbName('login_GE');
		// Create new database
		$this->createDatabase($oldDbName, $newDbName);

		// Drop database demo
		$this->dropDatabase($oldDbName);

		// Update user fields
		$user['dbname'] = $newDbName;
		$user['isDemo'] = 0;
		$user['registrationDbDate'] = date('Y-m-d');
		$this->User->save($user);

		$this->Session->setFlash(__('Ambiente attivato correttamente'), 'custom-flash');
		$this->redirect(['action'=>'login']);
	}

	public function getNextDbName($prefix)
	{
		$existDatabase = true;
		$databaseNumber = 1;
		$databaseName = '';

		while($existDatabase)
		{
			$databaseName = $prefix.str_pad($databaseNumber, 4, "0", STR_PAD_LEFT);

			// Create connection
			$databaseConnection = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
			// Select database
			$databaseSelected = mysqli_select_db($databaseConnection, $databaseName);

			// Check connection
			if (!$databaseSelected)
			{
				$existDatabase = false;
			}

			$databaseNumber++;
		}

		return $databaseName;
	}

	public function createDatabase($startDbName, $newDbName)
	{
		// Create connection
		$link = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);

		// Check connection
		if($link !== false)
		{
			// Create new database
			$createNewDbSql = "CREATE DATABASE IF NOT EXISTS ".$newDbName;

			if(mysqli_query($link, $createNewDbSql))
			{
				// Close connection
				mysqli_close($link);

				// Connect start database
				$connectionDbDemo = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
				// Select start database
				mysqli_select_db($connectionDbDemo, $startDbName);

				// Connect new database
				$connectionNewDb = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
				// Select new database
				mysqli_select_db($connectionNewDb, $newDbName);

				$sqlQueryShowTables = "SHOW TABLES";
				$tables = mysqli_query($connectionDbDemo, $sqlQueryShowTables);

				while($table = mysqli_fetch_object($tables))
				{
					$arrayTableName = json_decode(json_encode($table), true);
					$tableName = $arrayTableName['Tables_in_'.$startDbName];

					// Create table on new database
					$sqlQueryCreateTable = "CREATE TABLE ".$newDbName.".".$tableName." LIKE ".$startDbName.".".$tableName;
					mysqli_query($connectionNewDb, $sqlQueryCreateTable);

					// Insert all rows from start table to new table
					$sqlQueryInsertRows = "INSERT INTO ".$newDbName.".".$tableName." SELECT * FROM ".$startDbName.".".$tableName;
					mysqli_query($connectionNewDb, $sqlQueryInsertRows);
				}

				// Close connections
				mysqli_close($connectionDbDemo);
				mysqli_close($connectionNewDb);
			}
		}
		else
		{
			// Close connection
			mysqli_close($link);
		}
	}

	public function createUserMainDb($user, $demoDbName)
	{
		// Create new user
		$result = $this->User->find('first', [ 'conditions' => [ 'username' => $user['username']]]);

		if(!empty($result) & $result['User']['state'] == 0){
			$this->Session->setFlash(__('Contattaci per ripristinare il tuo account'), 'custom-flash');
		}else if( !empty($result) && $result['User']['state'] == 1){
			$this->Session->setFlash(__('Esiste già un utente con questo username, per favore, scegline un altro'), 'custom-flash');
		}else{
		$newMainUser = $this->User->create();
		$newMainUser['User']['username'] = $user['username'];
		$newMainUser['User']['password'] = $user['password'];
		$newMainUser['User']['group_id'] = 6;
		$newMainUser['User']['dbname'] = $demoDbName;
		$newMainUser['User']['dbpassword'] = MAINPASSWORD;
		$newMainUser['User']['dbuser'] = MAINUSER;
		$newMainUser['User']['usercompany'] = 1;
		$newMainUser['User']['tare'] = 0;
		$newMainUser['User']['isDemo'] = 1;
		$newMainUser['User']['state'] = 1;
		$newMainUser['User']['registrationDbDate'] = date('Y-m-d');
		$newMainUser['User']['PayPal'] = $user['paypal'];
		$newMainUser = $this->User->Save($newMainUser);
		return $newMainUser;
		}
	}

	public function createUserDb($user, $demoDbName)
	{
		// Connect new database
		$newDatabaseConnection = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
		// Select new database
		mysqli_select_db($newDatabaseConnection, $demoDbName);

		// Insert new user
		$fields = "`company_id`, `username`, `password`, `group_id`";
		$values = "1, '".$user['username']."', '".$user['password']."', 6";
		$sqlQueryInsertUser = "INSERT INTO users (".$fields.") VALUES (".$values.")";
		mysqli_query($newDatabaseConnection, $sqlQueryInsertUser);

		// Close connection
		mysqli_close($newDatabaseConnection);
	}

	public function dropDatabase($dbName)
	{
		// Connect database
		$databaseConnection = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
		// Select database
		mysqli_select_db($databaseConnection, $dbName);

		// Drop database
		$sqlQueryDropDatabase = "DROP DATABASE ".$dbName;
		mysqli_query($databaseConnection, $sqlQueryDropDatabase);

		// Close connection
		mysqli_close($databaseConnection);
	}

	public function pauseUser()
	{
    	$this->autoRender = false;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Users']);

		/*
		$this->Utilities->loadModels($this,['Groups']);
		$userGroup = $this->Auth->user('group_id');
		$company = MYCOMPANY;
		$typeOfUser = $this->Groups->find('first',['conditions'=>['id'=>$userGroup,'company_id'=>$company]]);
		*/

		session_destroy();
		$this->Users->updateAll(['state' => 0], ['id' => $_SESSION['Auth']['User']['id']]);
		$this->redirect($this->Auth->logout());
	}
}
?>
