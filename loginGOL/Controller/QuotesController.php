<?php
App::uses('AppController', 'Controller');

class QuotesController extends AppController
{
    public $components = array('Mpdf');

    public function index()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Quote', 'Csv']);

        if (MODULO_CANTIERI)
        {
            $conditionsArray = ['Quote.state' => 1, 'Quote.company_id' => MYCOMPANY, 'quote_date >= ' => date("Y") . '-01-01', 'Quote.quote_date <= ' => date("Y") . '-12-31', 'Quote.has_child' => 0];
        }
        else
        {
            $conditionsArray = ['Quote.state' => 1, 'Quote.company_id' => MYCOMPANY, 'Quote.quote_date >= ' => date("Y") . '-01-01', 'Quote.quote_date <= ' => date("Y") . '-12-31'];
        }

        if (MODULO_CANTIERI)
        {
            $filterableFields = ['quote_number', '#htmlElements[0]', 'description', 'ragionesociale', 'name', null, null, null, null, null, null];
            $sortableFields = [['quote_number', 'N° preventivo'], ['date', 'Data preventivo'], ['description', 'Descrizione'], ['client_id', 'Cliente'], [null, 'Cantiere'], [null, 'N. Revisioni'],[null, 'N. Modifiche'], ['amount', 'Imponibile'], ['amount', 'Iva'], ['amount', 'Totale'], ['#actions']];
        }
        else
        {
            $filterableFields = ['quote_number', '#htmlElements[0]', 'ragionesociale', null, null, null, null];
            $sortableFields = [['quote_number', 'N° preventivo'], ['date', 'Data preventivo'], ['client_id', 'Cliente'], ['amount', 'Imponibile'], ['amount', 'Iva'], ['amount', 'Totale'], ['#actions']];
        }

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
        {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        }
        else
        {
            null;
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            if (MODULO_CANTIERI) {
                $conditionsArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.has_child' => 0];
            } else {
                $conditionsArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1];
            }

            if (isset($this->request->data['filters']['quote_number']) && $this->request->data['filters']['quote_number'] != '')
            {
                $conditionsArray['Quote.quote_number like'] = '%' . $this->request->data['filters']['quote_number'] . '%';
            }
            if (isset($this->request->data['filters']['ragionesociale']) && $this->request->data['filters']['ragionesociale'] != '')
            {
                $conditionsArray['Client.ragionesociale like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';
            }

            if (MODULO_CANTIERI)
            {
                if (isset($this->request->data['filters']['description']) && $this->request->data['filters']['description'] != '') {
                    $conditionsArray['Quote.description like'] = '%' . $this->request->data['filters']['description'] . '%';
                }
                if (isset($this->request->data['filters']['name']) && $this->request->data['filters']['name'] != '') {
                    $conditionsArray['Constructionsite.name like'] = '%' . $this->request->data['filters']['name'] . '%';
                }
            }

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['quote_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
            }

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['quote_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
            }

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Se attiva la personalizzazione delle stampe
        $this->paginate = ['contain' =>
            [
                'Client',
                'Quotegoodrow' => ['Iva'],
                'Constructionsite',
            ],
            'conditions' => $conditionsArray,
            'limit' => 25,
            'order' => ['quote_date' => 'desc', 'quote_number' => 'desc']
        ];

        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
        {
            $this->autoRender = false;
            $dataForXls = $this->Quote->find('all', ['conditions' => $conditionsArray, 'order' => ['Quote.quote_number' => 'asc', 'Quote.quote_date' => 'desc']]);
            echo 'N° Preventivo;Data Preventivo;Cliente;Imponibile;Iva;Totale' . "\r\n";

            foreach ($dataForXls as $xlsRow)
            {
                $quoteTaxableIncome = $this->Utilities->getQuoteTaxableIncome($xlsRow['Quote']['id']);
                $quoteVat = $this->Utilities->getQuoteTotalVat($xlsRow['Quote']['id']);
                $quoteTotal = $this->Utilities->getQuoteTotal($xlsRow['Quote']['id']);
                echo $xlsRow['Quote']['quote_number'] . ';' . $xlsRow['Quote']['quote_date'] . ';' . $xlsRow['Client']['ragionesociale'] . ';' . $quoteTaxableIncome . ';' . $quoteVat . ';' . $quoteTotal . ';' . "\r\n";
            }
        }
        else
        {
            $this->set('sortableFields', $sortableFields);
            $this->set('filterableFields', $filterableFields);
            $this->set('quotes', $this->paginate());
            $this->render('index');
        }
    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Clients', 'Quotedeadlines', 'Quotegoodrow', 'Ivas', 'Units','Payments','Quote']);

        $this->set('n_fatt', $this->Quote->getNextNumber(date("Y")));

        if ($this->request->is(['post', 'put']))
        {
            $client = $this->Clients->find('first', ['conditions' => ['Clients.ragionesociale' => $this->request->data['Quote']['client_id'], 'Clients.company_id' => MYCOMPANY], 'fields' => ['Clients.id']]);
            $this->request->data['Quote']['company_id'] = MYCOMPANY;

            $numberOfMovement = 0;

            // Se il cliente non esiste allora lo creo
            empty($client['Clients']['id']) ? $this->request->data['Quote']['client_id'] = $this->Utilities->createClient($this->request->data['Quote']['client_id']) : $this->request->data['Quote']['client_id'] = $client['Clients']['id'];;

            $this->request->data['Quote']['quote_date'] = date("Y-m-d", strtotime($this->request->data['Quote']['quote_date']));

            // Se è settato splitpayment

            $newQuote = $this->Quote->create();

            $this->request->data['state'] = 1;
            $this->request->data['Quote']['note'] = nl2br($this->request->data['Quote']['note']);

            if ($newQuote = $this->Quote->save($this->request->data['Quote']))
            {
                // Setto a zero i valori per i calcoli delle scadenze
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];

                $rowCounter = 0;

                foreach ($this->request->data['Good'] as $key => $good)
                {
                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                    $newQuoteGood = $this->Utilities->createQuoteGoodRow($newQuote['Quote']['id'], $good,$rowCounter++);

                    // Se l'articolo esiste già a magazzino
                    if ($good['storage_id'] != '')
                    {
                        $storageId = $good['storage_id'];
                    }
                    else // Se l'articolo non esiste
                    {
                        if (!$this->Utilities->isANote($good, 'quote') && $good['movable'] >= 0 )
                        {
                            if ($this->Utilities->notExistInStorage($good['storage_id']))
                            {
                                // Creo nuovo articolo magazzino
                                $newStorage = $this->Utilities->createNewStorageFromQuote($good, $this->request->data['Quote']['client_id']);

                                // Aggiorno lo storage_id sul good
                                $this->Quotegoodrow->updateAll(['Quotegoodrow.storage_id' => $newStorage['Storage']['id'], 'Quotegoodrow.company_id' => MYCOMPANY], ['Quotegoodrow.id' => $newQuoteGood['Quotegoodrow']['id']]);
                            }
                        }
                    }
                }

                $this->Session->setFlash(__('Il preventivo è stato salvato'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Session->setFlash(__('Non è stato possibile salvareil preventivo, riprovare'), 'custom-danger');
            }
        }

        $this->set('units', $this->Units->getList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('setting', $this->Setting->GetMySettings());

        $this->loadModel('Client');
        $this->set('clients', $this->Client->getAdvancedList());

        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        }
        else
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('nations', $this->Utilities->getNationsList());

        if (MODULO_CANTIERI)
        {
            $this->loadModel('Constructionsite');
            $this->set('constructionsites', $this->Constructionsite->getList());
        }

        // Per definire la tipologia all'interno della vista [ false = 1 fattura normale, true = 6 fattura accompagnatoria, utilizzato solo in add_extendedvfs]
        $this->Render('add');
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Storage', 'Quotegoodrow', 'Units']);

        !empty($this->params['pass'][0]) ? $this->Quote->id = $this->params['pass'][0] : $this->Quote->id = $id;

        if (!$this->Quote->exists())
        {
            throw new NotFoundException(__('Preventivo non valido'));
        }

        if ($this->request->is('post') || $this->request->is('put'))
        {
            // Recupero i vecchi articoli
            $oldGood = $this->Quotegoodrow->find('all', ['conditions' => ['Quotegoodrow.company_id' => MYCOMPANY, 'quote_id' => $id]]);

            $quoteGoodOldValues = $this->Quote->find('first', array('conditions' => array('Quote.id' => $id)));

            $this->request->data['Quote']['quote_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Quote']['quote_date'])));
            $this->request->data['Quote']['note'] = nl2br($this->request->data['Quote']['note']);

            $numberOfMovement = 0;
            foreach ($oldGood as $oldQuoteGoodGood) {
                $numberOfMovement++;

                // Riscarico tutto
                if ($oldQuoteGoodGood['Quotegoodrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                {
                }
                $this->Quotegoodrow->deleteAll(['Quotegoodrow.id' => $oldQuoteGoodGood['Quotegoodrow']['id']]);
            }

            $this->request->data['Quote']['state'] = 1;
            $rowCounter =0;
            if ($newQuotegood = $this->Quote->save($this->request->data['Quote'])) {

                foreach ($this->request->data['Quotegoodrow'] as $key => $good) {

                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino

                    $newQuotegoodGood = $this->Utilities->createQuoteGoodRow($newQuotegood['Quote']['id'], $good,$rowCounter++);

                    isset($good['storage_id']) ? null : $good['storage_id'] = '';
                    // Se l'articolo esiste già a magazzino
                    if ($good['storage_id'] != '') {
                        $storageId = $good['storage_id'];
                    } else {
                        if (!$this->Utilities->isANote($good, 'quote') &&  $good['movable'] >= 0 ) {
                            // Se l'articolo non esiste
                            if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                // Creo nuovo articolo magazzino
                                $newStorage = $this->Utilities->createNewStorageFromQuote($good, $quoteGoodOldValues['Quote']['client_id']);

                                // Aggiorno lo storage_id sul good
                                $this->Quotegoodrow->updateAll(['Quotegoodrow.storage_id' => $newStorage['Storage']['id'], 'Quotegoodrow.company_id' => MYCOMPANY], ['Quotegoodrow.id' => $newQuotegoodGood['Quotegoodrow']['id']]);
                            }
                        }
                    }
                }

                $this->Session->setFlash(__('Preventivo salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Il preventivo non è stato salvato correttamente.'), 'custom-danger');
            }
        }
        else
        {
            $this->request->data = $this->Quote->find('first', ['contain' => ['Client', 'Quotegoodrow' => ['Storage']], 'conditions' => ['Quote.id' => $id]]);
        }

        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        }
        else
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->set('units', $this->Units->getList());
        $this->set('clients', $this->Client->getList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('setting', $this->Setting->GetMySettings());

        if (MODULO_CANTIERI)
        {
            $this->loadModel('Constructionsite');
            $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Quote']['constructionsite_id'], $this->request->data['Quote']['client_id']));
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Quote', 'Messages']);
        $asg = ["il", "preventivo", "M"];
        if ($this->Quote->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Quote->find('first', ['conditions' => ['Quote.id' => $id, 'Quote.company_id' => MYCOMPANY]]);
        if ($this->Quote->hide($currentDeleted['Quote']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }

    public function quotespdf($ragioneSociale, $id)
    {
        $nomefile = str_replace(' ', '_', $ragioneSociale);

        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Setting']);

        $quote = $this->Quote->find('first', ['contain' => ['Quotegoodrow' => ['Iva', 'Units'], 'Client' => ['Clientdestination' => ['Nation'], 'Nation']], 'recursive' => 2, 'conditions' => ['Quote.id' => $id, 'Quote.company_id' => MYCOMPANY]]);
        $this->set('quote', $quote);

        $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);

        $this->set('pdfHeader', $this->Pdf->getQuoteHeader(MYCOMPANY));
        $this->set('pdfBody', $this->Pdf->getQuoteBody(MYCOMPANY));
        $this->set('pdfFooter', $this->Pdf->getQuoteFooter(MYCOMPANY));
        $this->set('settings', $this->Setting->GetMySettings(MYCOMPANY));

        $this->set('impostazioni', $this->Setting->find('first', array('conditions' => array('Setting.company_id' => MYCOMPANY))));
        $this->set('deadlines', $this->Utilities->getDeadlines($id));

        $quotePdfTitle = 'Preventivo n° ' . $quote['Quote']['quote_number'] . ' del ' . date('d/m/Y', strtotime($quote['Quote']['quote_date']));
        $this->Pdf->setMpdf($quotePdfTitle, $nomefile, $this->Mpdf);
    }

    public function createSellBillsFromQuote($quoteId)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Good']);

        $currentQuote = $this->Quote->find('first', ['conditions' => ['Quote.id' => $quoteId, 'Quote.company_id' => MYCOMPANY]]);

        // Creo la nuova fattura
        $newSellBill = $this->Utilities->createSellBillFromQuote($currentQuote['Quote']['client_id'], $quoteId);

        // Salvo le righe
        foreach ($currentQuote['Quotegoodrow'] as $quoterow) {
            if ($quoterow['type_of_line'] != 'S' && $quoterow['type_of_line'] != 'O') {
                $newGood = $this->Good->create();
                isset($quoterow['description']) ? $newGood['Good']['oggetto'] = $quoterow['description'] : $newGood['Good']['oggetto'] = '';
                isset($quoterow['customdescription']) ? $newGood['Good']['customdescription'] = $quoterow['customdescription'] : $newGood['Good']['customdescription'] = '';
                isset($quoterow['quantity']) ? $newGood['Good']['quantita'] = $quoterow['quantity'] : $newGood['Good']['quantita'] = null; // Messo null era 0 2018/11/29
                isset($quoterow['quote_good_row_price']) ? $newGood['Good']['prezzo'] = $quoterow['quote_good_row_price'] : $newGood['Good']['prezzo'] = null; // Messo null era 0 2018/11/29;
                isset($quoterow['quote_good_row_vat_id']) ? $newGood['Good']['iva_id'] = $quoterow['quote_good_row_vat_id'] : $newGood['Good']['iva_id'] = null; // Messo null era 0 2018/11/29;
                $newGood['Good']['bill_id'] = $newSellBill['Bill']['id'];
                isset($quoterow['storage_id']) ? $newGood['Good']['storage_id'] = $quoterow['storage_id'] : $newGood['Good']['storage_id'] = '';
                $newGood['Good']['transport_id'] = null;
                isset($quoterow['unit_of_measure_id']) ? $newGood['Good']['unita'] = $quoterow['unit_of_measure_id'] : $newGood['Good']['unita'] = '';
                $newGood['Good']['discount'] = 0;
                isset($quoterow['codice']) ? $newGood['Good']['codice'] = $quoterow['codice'] : $newGood['Good']['codice'] = '';
                $newGood['Good']['company_id'] = MYCOMPANY;
                $newGood['Good']['vat'] = 0;
                $newBillRow = $this->Good->save($newGood);
            }
        }

        // Setto l'entrata merce come registrata
        $this->Quote->updateAll(['bill_created' => 1], ['Quote.id' => $quoteId, 'Quote.company_id' => MYCOMPANY]);

        // Mi sposto sulla modifica delle fatture
        $this->redirect(['controller' => 'bills', 'action' => 'edit', $newSellBill['Bill']['id']]);
    }

    // Controllo duplicato fattura
    public function checkQuoteDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $A = $this->Utilities->checkQuoteDuplicate($_POST['quotenumber'], $_POST['date']);
        return json_encode($A);
    }

    public function quoteAdd($id = null)
    {
        if (MODULO_CANTIERI)
        {
            $newQuote = $this->quoteDuplicate('MO', $id);
            $this->redirect(['controller' => 'quotes', 'action' => 'editAdd', $newQuote['Quote']['id']]);
        }
    }

    public function quoteChange($id = null)
    {
        if (MODULO_CANTIERI) {
            $newQuote = $this->quoteDuplicate('VA', $id);
            $this->redirect(['controller' => 'quotes', 'action' => 'editChange', $newQuote['Quote']['id']]);
        }
    }

    public function duplicate($id = null)
    {
        $newQuote = $this->quoteDuplicate('DU', $id);
        $this->redirect(['controller' => 'quotes', 'action' => 'edit', $newQuote['Quote']['id']]);
    }

    // Finalizzazione modifica preventivo (non possono essere rimosse o variate righe precedenti)
    public function editAdd($id)
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Client', 'Storage', 'Quotegoodrow', 'Units']);

            $numberOfMovement = 0;

            if (!empty($this->params['pass'][0])) {
                $this->Quote->id = $this->params['pass'][0];
            } else {
                $this->Quote->id = $id;
            }

            if (!$this->Quote->exists()) {
                throw new NotFoundException(__('Preventivo non valido'));
            }

            if ($this->request->is('post') || $this->request->is('put')) {

                // Recupero i vecchi articoli
                $oldGood = $this->Quotegoodrow->find('all', ['conditions' => ['Quotegoodrow.company_id' => MYCOMPANY, 'quote_id' => $id]]);

                $quoteGoodOldValues = $this->Quote->find('first', ['conditions' => ['Quote.id' => $id]]);

                // Setto la creazione del figlio
                $this->Quote->setHasChild($quoteGoodOldValues['Quote']['parent_id'], true);

                $this->request->data['Quote']['quote_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Quote']['quote_date'])));
                $this->request->data['Quote']['note'] = nl2br($this->request->data['Quote']['note']);

                $numberOfMovement = 0;
                foreach ($oldGood as $oldQuoteGoodGood) {
                    $numberOfMovement++;

                    // Riscarico tutto
                    if ($oldQuoteGoodGood['Quotegoodrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                    {
                    }
                    $this->Quotegoodrow->deleteAll(['Quotegoodrow.id' => $oldQuoteGoodGood['Quotegoodrow']['id']]);
                }

                $this->request->data['Quote']['state'] = 1;

                if ($newQuotegood = $this->Quote->save($this->request->data['Quote'])) {
                    $rowCounter = 0;
                    foreach ($this->request->data['Quotegoodrow'] as $key => $good) {

                        $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino

                        $newQuotegoodGood = $this->Utilities->createQuoteGoodRow($newQuotegood['Quote']['id'], $good,$rowCounter++);

                        isset($good['storage_id']) ? null : $good['storage_id'] = '';
                        // Se l'articolo esiste già a magazzino
                        if ($good['storage_id'] != '') {
                            $storageId = $good['storage_id'];
                        } else {
                            if (!$this->Utilities->isANote($good, 'quote')) {
                                // Se l'articolo non esiste
                                if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                    // Creo nuovo articolo magazzino
                                    $newStorage = $this->Utilities->createNewStorageFromQuote($good, $quoteGoodOldValues['Quote']['client_id']);

                                    // Aggiorno lo storage_id sul good
                                    $this->Quotegoodrow->updateAll(['Quotegoodrow.storage_id' => $newStorage['Storage']['id'], 'Quotegoodrow.company_id' => MYCOMPANY], ['Quotegoodrow.id' => $newQuotegoodGood['Quotegoodrow']['id']]);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__('Preventivo salvato correttamente'), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__('Il preventivo non è stato salvato correttamente.'), 'custom-danger');
                }
            } else {
                $this->request->data = $this->Quote->find('first', ['contain' => ['Client', 'Quotegoodrow' => ['Storage']], 'recursive' => 2, 'conditions' => ['Quote.id' => $id]]);
            }

            $this->set('units', $this->Units->getList());
            $this->loadModel('Client');
            //$this->set('clients', $this->Client->getList());
            $this->set('clients', $this->Client->getAdvancedList());

            if (ADVANCED_STORAGE_ENABLED) {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            } else {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('setting', $this->Setting->GetMySettings());
            $this->set('vats', $this->Utilities->getVatsList());
            $this->set('nations', $this->Utilities->getNationsList());

            if (MODULO_CANTIERI) {
                $this->loadModel('Constructionsite');
                $this->set('constructionsites', $this->Constructionsite->getList());
            }
        }
    }

    // Finalizza variazione preventivo (può succedere di tutto ad un preventivo non accettato)
    public function editchange($id)
    {
        if (MODULO_CANTIERI) {

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Client', 'Storage', 'Quotegoodrow', 'Units']);

            $numberOfMovement = 0;

            if (!empty($this->params['pass'][0])) {
                $this->Quote->id = $this->params['pass'][0];
            } else {
                $this->Quote->id = $id;
            }

            if (!$this->Quote->exists()) {
                throw new NotFoundException(__('Preventivo non valido'));
            }

            if ($this->request->is('post') || $this->request->is('put')) {

                // Recupero i vecchi articoli
                $oldGood = $this->Quotegoodrow->find('all', ['conditions' => ['Quotegoodrow.company_id' => MYCOMPANY, 'quote_id' => $id]]);
                $quoteGoodOldValues = $this->Quote->find('first', array('conditions' => array('Quote.id' => $id)));

                // Setto la creazione del figlio
                $this->Quote->setHasChild($quoteGoodOldValues['Quote']['parent_id'], true);

                $this->request->data['Quote']['quote_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Quote']['quote_date'])));
                $this->request->data['Quote']['note'] = nl2br($this->request->data['Quote']['note']);

                $numberOfMovement = 0;
                foreach ($oldGood as $oldQuoteGoodGood) {
                    $numberOfMovement++;

                    // Riscarico tutto
                    if ($oldQuoteGoodGood['Quotegoodrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                    {
                    }
                    $this->Quotegoodrow->deleteAll(['Quotegoodrow.id' => $oldQuoteGoodGood['Quotegoodrow']['id']]);
                }

                $this->request->data['Quote']['state'] = 1;

                if ($newQuotegood = $this->Quote->save($this->request->data['Quote'])) {
                    $rowCounter = 0;
                    foreach ($this->request->data['Quotegoodrow'] as $key => $good) {

                        $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino

                        $newQuotegoodGood = $this->Utilities->createQuoteGoodRow($newQuotegood['Quote']['id'], $good, $rowCounter++);

                        isset($good['storage_id']) ? null : $good['storage_id'] = '';
                        // Se l'articolo esiste già a magazzino
                        if ($good['storage_id'] != '') {
                            $storageId = $good['storage_id'];
                        } else {
                            if (!$this->Utilities->isANote($good, 'quote')) {
                                // Se l'articolo non esiste
                                if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                    // Creo nuovo articolo magazzino
                                    $newStorage = $this->Utilities->createNewStorageFromQuote($good, $quoteGoodOldValues['Quote']['client_id']);

                                    // Aggiorno lo storage_id sul good
                                    $this->Quotegoodrow->updateAll(['Quotegoodrow.storage_id' => $newStorage['Storage']['id'], 'Quotegoodrow.company_id' => MYCOMPANY], ['Quotegoodrow.id' => $newQuotegoodGood['Quotegoodrow']['id']]);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__('Preventivo salvato correttamente'), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__('Il preventivo non è stato salvato correttamente.'), 'custom-danger');
                }
            } else {
                $this->request->data = $this->Quote->find('first', ['contain' => ['Client', 'Quotegoodrow' => ['Storage']], 'recursive' => 2, 'conditions' => ['Quote.id' => $id]]);
            }

            $this->set('units', $this->Units->getList());
            $this->loadModel('Client');
            //$this->set('clients', $this->Client->getList());
            $this->set('clients', $this->Client->getAdvancedList());

            if (ADVANCED_STORAGE_ENABLED) {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            } else {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('vats', $this->Utilities->getVatsList());
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('setting', $this->Setting->GetMySettings());

            if (MODULO_CANTIERI) {
                $this->loadModel('Constructionsite');
                $this->set('constructionsites', $this->Constructionsite->getList());
            }
        }
    }

    private function quoteDuplicate($typeofChange, $id)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Storage', 'Quotegoodrow', 'Units','Quote']);

        $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.id' => $id, 'Quote.state' => 1];

        // Aggiorno il vecchio preventivo indicando che ha un figlio
        $currentQuote = $this->Quote->find('first', ['conditions' => $conditionArray]);
        // $currentQuote['Quote']['has_child'] = 1;
        $this->Quote->save($currentQuote);

        // Creo il nuovo preventivo
        $newQuote = $this->Quote->find('first', ['conditions' => $conditionArray]);
        $newQuote['Quote']['id'] = null;
        $newQuote['Quote']['parent_id'] = $id;
        $newQuote['Quote']['type_of_variation'] = $typeofChange;

        if($typeofChange == 'MO')
        {
            $newQuote['Quote']['number_of_changes'] = $newQuote['Quote']['number_of_changes'] + 1;
        }

        if($typeofChange == 'VA')
        {
            $newQuote['Quote']['sent'] = 0;
            $newQuote['Quote']['number_of_variation'] = $newQuote['Quote']['number_of_variation'] + 1;
        }

        if($typeofChange == 'DU')
        {
            $newQuote['Quote']['quote_number'] = $this->Quote->getNextNumber(date('Y'));
        }

        $newQuote['Quote']['has_child'] = 0;
        //$newQuote['Quote']['state'] = 0;

        $newQuote = $this->Quote->save($newQuote);

        // Duplico le righe
        $conditionArray = ['Quotegoodrow.company_id' => MYCOMPANY, 'Quotegoodrow.quote_id' => $id, 'Quotegoodrow.state' => 1];
        $currentQuoterows = $this->Quotegoodrow->find('all', ['conditions' => $conditionArray, 'order' => 'line_number']);
        foreach ($currentQuoterows as $row)
        {
            $newQuoteGoodrow = $this->Quotegoodrow->create();
            $newQuoteGoodrow = $row;
            $newQuoteGoodrow  ['Quotegoodrow']['id'] = null;
            $newQuoteGoodrow  ['Quotegoodrow']['state'] = 0;
            $newQuoteGoodrow['Quotegoodrow']['quote_id'] = $newQuote['Quote']['id'];

            //$row['Quotegoodrow']['quote_id'] = $newQuote['Quote']['id'];
            $this->Quotegoodrow->save($newQuoteGoodrow);
        }

        return $newQuote;
    }

    public function quoteHistory($quoteId)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Quote']);

            $filterableFields = ['quote_number', '#htmlElements[0]', 'ragionesociale', null, null, null, null, null, null,];
            $sortableFields = [['quote_number', 'N° preventivo'], ['date', 'Data preventivo'], ['client_id', 'Cliente'], [null, 'Cantiere'], [null, 'Tipo di aggiornamento'], ['amount', 'Imponibile'], ['amount', 'Iva'], ['amount', 'Totale'], ['#actions']];

            /* if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
                 $conditionsArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1];
                 if (isset($this->request->data['filters']['quote_number'])) {
                     $conditionsArray['Quote.quote_number like'] = '%' . $this->request->data['filters']['quote_number'] . '%';
                 }
                 if (isset($this->request->data['filters']['ragionesociale']) && $this->request->data['filters']['ragionesociale'] != '') {
                     $conditionsArray['Client.ragionesociale like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';
                 }
                 if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '') {
                     $conditionsArray['quote_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
                 }
                 if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '') {
                     $conditionsArray['quote_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
                 }

                 $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
                 $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
                 $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
             } */


            $historyQuoteList = $this->Quote->getQuotesHistory($quoteId);

            $this->set('sortableFields', $sortableFields);
            $this->set('filterableFields', $filterableFields);
            $this->set('quotes', $historyQuoteList);
        }
    }

    public function quoteSetAccepted()
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Quote']);
            $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.id' => $_POST['quoteId'], 'Quote.state' => 1];
            $currentquote = $this->Quote->find('first', ['conditions' => $conditionArray]);
            $currentquote['Quote']['accepted'] = 1;
            $this->Quote->save($currentquote);
            return $_POST['quoteId'];
        }
    }

    public function quoteSetNotAccepted()
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Quote']);
            $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.id' => $_POST['quoteId'], 'Quote.state' => 1];
            $currentquote = $this->Quote->find('first', ['conditions' => $conditionArray]);
            $currentquote['Quote']['accepted'] = 0;
            $this->Quote->save($currentquote);
            return $_POST['quoteId'];
        }
    }

    public function quoteSetSent()
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Quote']);
            $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.id' => $_POST['quoteId'], 'Quote.state' => 1];
            $currentquote = $this->Quote->find('first', ['conditions' => $conditionArray]);
            $currentquote['Quote']['sent'] = 1;
            $this->Quote->save($currentquote);
            return $_POST['quoteId'];
        }
    }

    public function quoteSetNotSent()
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Quote']);
            $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.id' => $_POST['quoteId'], 'Quote.state' => 1];
            $currentquote = $this->Quote->find('first', ['conditions' => $conditionArray]);
            $currentquote['Quote']['sent'] = 0;
            $this->Quote->save($currentquote);
            return $_POST['quoteId'];
        }
    }

    public function closeQuote()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Quote']);
        $this->Quote->close($_POST['quoteId']);
        return $_POST['quoteId'] ;
    }

    public function openQuote()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Quote']);
        $this->Quote->open($_POST['quoteId']);
        return $_POST['quoteId'] ;
    }
}
