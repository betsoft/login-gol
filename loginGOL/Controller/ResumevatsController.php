<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'mpdf/mpdf');
App::import('Vendor', 'tpmacn/csvhelper');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('Routing','Router');
App::uses('Xml', 'Utility');
require 'PHPMailer/PHPMailerAutoload.php';

class ResumevatsController extends AppController
{
    public $components = array('Mpdf');

    /** Riepiloghi iva vendite*/
    public function ResumeVatSell() { }

    /** Riepiloghi iva acquisto */
    public function ResumeVatBuy() { }

    /** Fine riepiloghi iva */

    public function CreateResumeVatSell()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Bill', 'Pdf']);

        if ($_POST)
        {
            $conditionsArray = ['Bill.date >= ' => date('Y-m-d', strtotime($this->request->data['dateFrom'])), 'Bill.date <= ' => date('Y-m-d', strtotime($this->request->data['dateTo'])), 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia IN' => ['1', '3']];
            $bills = $this->Bill->find('all', ['contain' => ['Good' => ['Iva' => ['Einvoicevatnature'], 'Storage' => ['Units'], 'Units', 'Transports']], 'recursive' => 3, 'conditions' => $conditionsArray,'order'=>'date']);
            $this->set('bills', $bills);

            $setting = $this->Setting->GetMySettings();
            $this->set('setting', $setting);

            $this->Pdf->setMpdf("Registro iva vendite", "", $this->Mpdf);
            $this->set('dateFrom', $this->request->data['dateFrom']);
            $this->set('dateTo', $this->request->data['dateTo']);
            $this->set('utilities', $this->Utilities);
        }
    }

    public function CreateResumeVatBuy()
    {
        $this->layout = 'pdf';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Bill','Pdf']);

        if($_POST)
        {
            if($_POST['tipo'] == "datafatt")
                $conditionsArray = ['Bill.date >= ' => date('Y-m-d',strtotime($this->request->data['dateFrom'])), 'Bill.date <= ' => date('Y-m-d',strtotime($this->request->data['dateTo'])),'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia' => 2];
            else
                $conditionsArray = ['Bill.receive_date >= ' => date('Y-m-d',strtotime($this->request->data['dateFrom'])), 'Bill.receive_date <= ' => date('Y-m-d',strtotime($this->request->data['dateTo'])),'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.tipologia' => 2];

            $bills = $this->Bill->find('all', ['contain' => ['Good' => ['Iva','Storage'=>['Units'],'Units','Transports']], 'recursive' => 2, 'conditions' => $conditionsArray,'order'=>'date']);
            $this->set('bills',$bills);

            $setting = $this->Setting->GetMySettings();
            $this->set('setting',$setting);

            $this->Pdf->setMpdf("Registro iva acquisti","",$this->Mpdf);
            $this->set('dateFrom',$this->request->data['dateFrom']);
            $this->set('dateTo',$this->request->data['dateTo']);
            $this->set('utilities', $this->Utilities);
        }
    }
}