<?php
App::uses('AppController', 'Controller');

class ClientdestinationsController extends AppController {



	public function index($clientId)
	{
		$this->paginate = ['conditions' => ['Clientdestination.client_id' => $clientId, 'Clientdestination.company_id' => MYCOMPANY,'Clientdestination.state'=>ATTIVO],'order' => ['Clientdestination.name' => 'asc'], 'limit' => 100 ];
		$this->Clientdestination->recursive = 0;
		$this->set('Clientdestinations', $this->paginate());
		$this->set('clientId',$clientId);
	}


	/** Aggiunta cliente */
	public function add($clientId)
	{
		$this->loadModel('Utilities');
		if ($this->request->is('post'))
		{
			$this->Clientdestination->create();
			$this->request->data['Clientdestination']['company_id']=MYCOMPANY;
			$this->request->data['Clientdestination']['client_id']=$clientId;
			if ($this->Clientdestination->save($this->request->data)) {
				$this->Session->setFlash(__('destino cliente salvato'), 'custom-flash');
				$this->redirect(['action' => 'index',$clientId]);
			} else {
				$this->Session->setFlash(__('Il destino cliente non è stato salvato'), 'custom-danger');
			}
		}
		$this->set(compact('Clientdestinations'));
		$this->set('clientId',$clientId);
		$this->set('nations', $this->Utilities->getNationsList());
	}


	public function edit($id = null,$clientId = null)
	{
		$this->loadModel('Utilities');
		$this->Clientdestination->id = $id;
		if (!$this->Clientdestination->exists())
		{
			throw new NotFoundException(__('Destino cliente non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Clientdestination->save($this->request->data))
			{
				$this->Session->setFlash(__('Cliente salvato correttamente'), 'custom-flash');
				$this->redirect(['action' => 'index',$clientId]);
			}
			else
			{
				$this->Session->setFlash(__('Il destino cliente non é stato salvato, riprovare.'), 'custom-danger');
			}
		}
		else
		{
			$this->request->data = $this->Clientdestination->read(null, $id);
		}

		$this->set(compact('Clientdestinations'));
		$this->set('clientId',$clientId);
		$this->set('nations', $this->Utilities->getNationsList());
	}

	public function delete($id = null,$clientId)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Clientdestination','Messages']);
        $asg =  ["l'","articolo","M"];
		if($this->Clientdestination->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Clientdestination->find('first',['conditions'=>['Clientdestination.id'=>$id,'Clientdestination.company_id'=>MYCOMPANY]]);
        if ($this->Clientdestination->hide($currentDeleted['Clientdestination']['id']))
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		   $this->redirect(['action' => 'index',$clientId]);
	}

}
