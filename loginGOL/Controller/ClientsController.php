<?php
App::uses('AppController', 'Controller');

class ClientsController extends AppController
{

    public function index($filterableFields = [])
    {
        $this->loadModel('Payments');
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Csv']);

        if (MODULO_CANTIERI) {
            $conditionsArray = ['Client.company_id' => MYCOMPANY, 'Client.state >' => 0];
        } else {
            $conditionsArray = ['Client.company_id' => MYCOMPANY, 'Client.state' => ATTIVO];
        }

        $filterableFields = ['code', 'ragionesociale', 'indirizzo', 'cap', 'citta', 'provincia', 'telefono', 'mail', 'piva', 'cf', null];
        $filterableFields_fix = ['code', 'ragionesociale', 'indirizzo', 'cap', 'citta', 'provincia', 'telefono', 'mail', 'piva', 'cf', null];
        $sortableFields = [['code', 'codice'], ['ragionesociale', 'Ragione sociale'], ['indirizzo', 'Indirizzo'], ['cap', 'Cap'], ['citta', 'Città'], ['provincia', 'Prov.'], ['telefono', 'Telefono'], ['mail', 'Email'], ['piva', 'P.iva'], ['cf', 'Codice fiscale'], ['#actions']];
        $xlsTitle = 'Codice;Ragione sociale;Indirizzo;Cap;Città;Provincia;Telefono;Email;P.iva;Codice fiscale;Codice Univoco;Banca;Metodo di Pagamento;' . "\r\n";

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        } else {
            null;
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields_fix, $this->request->data['filters']);

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Client->find('all', ['conditions' => $conditionsArray, 'order' => ['Client.ragionesociale' => 'asc']]);
            echo $xlsTitle;
            foreach ($dataForXls as $xlsRow) {
                $xlsRow['Bank']['description'] != null ? $bank = $xlsRow['Bank']['description'] . ' ' . $xlsRow['Bank']['abi'] . ' ' . $xlsRow['Bank']['cab'] : $bank = '';

                $paymentMethod = $this->Payments->findById($xlsRow['Client']['payment_id']);
                $paymentMethod != null ? $payment = $paymentMethod['Payments']['metodo'] : $payment = '';

                echo $xlsRow['Client']['code'] . ';' . $xlsRow['Client']['ragionesociale'] . ';' . $xlsRow['Client']['indirizzo'] . ';' . $xlsRow['Client']['cap'] . ';' . $xlsRow['Client']['citta'] . ';' . $xlsRow['Client']['provincia'] . ';' . $xlsRow['Client']['telefono'] . ';' . $xlsRow['Client']['mail'] . ';' . $xlsRow['Client']['piva'] . ';' . $xlsRow['Client']['cf'] . ';' . $xlsRow['Nation']['name'] . ';' . $xlsRow['Client']['codiceDestinatario'] . ';' . $bank . ';' . $payment . ';' . "\r\n";
            }
        } else {
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0041")
            {
                $conditionsArray2 = $conditionsArray;
                $conditionsArray2['data_nascita = '] = date("Y-m-d");
                $clientBirthday = $this->Client->find('all', ['conditions' => $conditionsArray2, 'recursive' => -1]);
                if(sizeof($clientBirthday) > 0)
                {
                    $messageString = 'Questi clienti compiono gli anni oggi: ';
                    foreach($clientBirthday as $birthday){
                        $messageString = $messageString."-".$birthday['Client']['ragionesociale']." \n";
                    }
                    $this->Session->setFlash(__($messageString), 'custom-flash');
                }
            }
            $this->paginate = ['conditions' => $conditionsArray, 'order' => ['Client.ragionesociale' => 'asc'], 'limit' => 100];
            $this->Client->recursive = 0;
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('clients', $this->paginate());
        }
    }

    public function indexResellerClient($resellerId)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Clientreseller']);
        $currentClient = $this->Client->find('first', ['conditions' => ['Client.id' => $resellerId, 'Client.company_id' => MYCOMPANY]]);
        $resellerName = $currentClient['Client']['ragionesociale'];
        $clientReseller = $this->Clientreseller->find('all', ['contain' => ['Client' => ['Nation']], 'conditions' => ['Clientreseller.reseller_id' => $resellerId, 'Clientreseller.company_id' => MYCOMPANY, 'Clientreseller.state' => ATTIVO]]);

        $this->set('resellerId', $resellerId);
        $this->set('resellerName', $resellerName);
        $this->set('clients', $clientReseller);
    }

    public function importClientFromXml()
    {
        $this->loadModel('Currencies');
        set_time_limit(600);
        $this->loadModel('Utilities');
        if ($this->request->is('post')) {
            $file = file_get_contents($this->request->data['Clients']['file']['tmp_name']);
            $xmlElement = new SimpleXMLElement($file, LIBXML_NOCDATA);
            $clients = $xmlElement->Accounts->Account;
            $errori = array();

            foreach ($clients as $clientxml) {
                $flagPIVA = false;
                $flagPEC = false;
                $client = array();
                $id = (string)$clientxml->attributes()->code;
                $exists = $this->Client->find('first', ['conditions' => ['Client.id' => $id]]);
                if ($exists == null) {
                    $this->Client->create();
                    $client['Client']['id'] = (int)$id;
                    $client['Client']['company_id'] = 1;
                    $client['Client']['ragionesociale'] = (string)$clientxml->Name;
                    if (sizeof($clientxml->Address) > 1) {
                        $addresses = $clientxml->Address;
                        foreach ($addresses as $address) {
                            if ($address->attributes()->type == "VIS") {
                                $client['Client']['indirizzo'] = (string)$address->AddressLine1;
                                $client['Client']['citta'] = (string)$address->City;
                                $client['Client']['cap'] = str_pad($address->PostalCode, 5, "0", STR_PAD_LEFT);
                                if(strcmp(strtoupper($address->City),"ROMA")==0){
                                    $client['Client']['provincia'] = "RM";
                                }else{
                                    if(isset($address->State))
                                        $client['Client']['provincia'] = (string)$address->State->attributes()->code;
                                }
                                $client['Client']['cf'] = (string)$address->AddressLine2;
                                $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($address->Country->attributes()->code);
                                if (!empty($address->AddressLine3)) {
                                    if (strpos((string)$address->AddressLine3, '@') !== false) {
                                        $client['Client']['codiceDestinatario'] = '0000000';
                                        $client['Client']['pec'] = (string)$address->AddressLine3;
                                    } else {
                                        $client['Client']['codiceDestinatario'] = (string)$address->AddressLine3;
                                    }
                                } else {
                                    $client['Client']['codiceDestinatario'] = '0000000';
                                    $flagPEC = true;
                                }
                            }
                            else{
                                if(isset($address->State))
                                    $client['Client']['provincia'] = (string)$address->State->attributes()->code;
                            }
                        }
                    }
                    else {
                        $client['Client']['indirizzo'] = (string)$clientxml->Address->AddressLine1;
                        $client['Client']['citta'] = (string)$clientxml->Address->City;
                        $client['Client']['cap'] = str_pad($clientxml->Address->PostalCode, 5, "0", STR_PAD_LEFT);
                        if(strcmp(strtoupper($clientxml->Address->City),"ROMA")==0){
                            $client['Client']['provincia'] = "RM";
                        }else{
                            if(isset($clientxml->Address->State))
                                $client['Client']['provincia'] = (string)$clientxml->Address->State->attributes()->code;
                        }
                        $client['Client']['cf'] = (string)$clientxml->Address->AddressLine2;
                        $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($clientxml->Address->Country->attributes()->code);
                        if($client['Client']['nation_id'] != 106){
                            $client['Client']['codiceDestinatario'] = "XXXXXXX";
                        }else{
                            if (!empty($clientxml->Address->AddressLine3)) {
                                if (strpos((string)$clientxml->Address->AddressLine3, '@') !== false) {
                                    $client['Client']['codiceDestinatario'] = '0000000';
                                    $client['Client']['pec'] = (string)$clientxml->Address->AddressLine3;
                                } else {
                                    $client['Client']['codiceDestinatario'] = (string)$clientxml->Address->AddressLine3;
                                }
                            } else {
                                $client['Client']['codiceDestinatario'] = '0000000';
                            }
                        }
                    }

                    $client['Client']['telefono'] = (string)$clientxml->Phone;
                    $client['Client']['fax'] = (string)$clientxml->Fax;
                    $commaInEmail = strpos((string)$clientxml->Email, ",");
                    $client['Client']['mail'] = $commaInEmail === false ? (string)$clientxml->Email : substr((string)$clientxml->Email, 0, $commaInEmail);
                    $client['Client']['extra_mail'] = $commaInEmail === false ? null : substr((string)$clientxml->Email, $commaInEmail, strlen($commaInEmail));

                    if (!empty($clientxml->VATNumber)) {
                        if ((strlen((string)$clientxml->VATNumber) == 13) && substr((string)$clientxml->VATNumber, 0, 2) === 'IT'){
                            $client['Client']['piva'] = substr((string)$clientxml->VATNumber, 2);
                            $flagPIVA = true;
                        }
                        else{
                            $client['Client']['piva'] = (string)$clientxml->VATNumber;
                            $flagPIVA = true;
                        }
                    }


                    $client['Client']['payment_id'] = !empty($clientxml->SalesPaymentCondition) ? (string)$clientxml->SalesPaymentCondition->attributes()->code : null;
                    $client['Client']['state'] = 1;
                    $client['Client']['currency_id'] = (int)$this->Currencies->getCurrencyIdFromCode((string)$clientxml->PurchaseCurrency->attributes()->code);
                    if($flagPIVA && $flagPEC){
                        array_push($errori, (string)$clientxml->Name);
                    }
                    $newClient = $this->Client->save($client['Client']);

                }
                else {
                    $client['Client']['id'] = (int)$id;
                    $client['Client']['ragionesociale'] = (string)$clientxml->Name;

                    if (sizeof($clientxml->Address) > 1) {
                        $addresses = $clientxml->Address;
                        foreach ($addresses as $address) {
                            if ($address->attributes()->type == "VIS") {
                                $client['Client']['indirizzo'] = (string)$address->AddressLine1;
                                $client['Client']['citta'] = (string)$address->City;
                                $client['Client']['cap'] = str_pad($address->PostalCode, 5, "0", STR_PAD_LEFT);
                                if(strcmp(strtoupper($address->City), "ROMA") == 0){
                                    $client['Client']['provincia'] = "RM";
                                }else{
                                    if(isset($address->State))
                                        $client['Client']['provincia'] = (string)$address->State->attributes()->code;
                                }

                                $client['Client']['cf'] = (string)$address->AddressLine2;
                                $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($address->Country->attributes()->code);
                                if (!empty($address->AddressLine3)) {
                                    if (strpos((string)$address->AddressLine3, '@') !== false) {
                                        $client['Client']['codiceDestinatario'] = '0000000';
                                        $client['Client']['pec'] = (string)$address->AddressLine3;
                                    } else {
                                        $client['Client']['codiceDestinatario'] = (string)$address->AddressLine3;
                                    }
                                } else {
                                    $client['Client']['codiceDestinatario'] = '0000000';
                                    $flagPEC = true;
                                }
                            }
                        }
                    } else {
                        $client['Client']['indirizzo'] = (string)$clientxml->Address->AddressLine1;
                        $client['Client']['citta'] = (string)$clientxml->Address->City;
                        $client['Client']['cap'] = str_pad($clientxml->Address->PostalCode, 5, "0", STR_PAD_LEFT);
                        if(strcmp(strtoupper($clientxml->Address->City), "ROMA")== 0){
                            $client['Client']['provincia'] = "RM";
                        }else{
                            if(isset($clientxml->Address->State))
                                $client['Client']['provincia'] = (string)$clientxml->Address->State->attributes()->code;
                        }
                        $client['Client']['cf'] = (string)$clientxml->Address->AddressLine2;
                        $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($clientxml->Address->Country->attributes()->code);
                        if (!empty($clientxml->Address->AddressLine3)) {
                            if (strpos((string)$clientxml->Address->AddressLine3, '@') !== false) {
                                $client['Client']['codiceDestinatario'] = '0000000';
                                $client['Client']['pec'] = (string)$clientxml->Address->AddressLine3;
                            } else {
                                $client['Client']['codiceDestinatario'] = (string)$clientxml->Address->AddressLine3;
                            }
                        } else {
                            $client['Client']['codiceDestinatario'] = '0000000';
                            $flagPEC = true;
                        }
                    }

                    if (!empty($clientxml->VATNumber)) {
                        $flagPIVA = true;
                    }

                    $client['Client']['telefono'] = (string)$clientxml->Phone;
                    $client['Client']['fax'] = (string)$clientxml->Fax;
                    $commaInEmail = strpos((string)$clientxml->Email, ",");
                    $client['Client']['mail'] = $commaInEmail === false ? (string)$clientxml->Email : substr((string)$clientxml->Email, 0, $commaInEmail);
                    $client['Client']['extra_mail'] = $commaInEmail === false ? null : substr((string)$clientxml->Email, $commaInEmail, strlen($commaInEmail));
                    $client['Client']['payment_id'] = !empty($clientxml->SalesPaymentCondition) ? (string)$clientxml->SalesPaymentCondition->attributes()->code : null;
                    $client['Client']['currency_id'] = (int)$this->Currencies->getCurrencyIdFromCode((string)$clientxml->PurchaseCurrency->attributes()->code);
                    if($flagPIVA && $flagPEC)
                        array_push($errori, (string)$clientxml->Name);
                    $this->Client->save($client);
                }


            }

            $message = "I seguenti clienti non hanno nè codice destinatario nè PEC, ma hanno la partita IVA: \n";
            foreach($errori as $errore){
                $message = $message."-".$errore." \n";
            }
            $this->Session->setFlash(__('I clienti sono stati importati'), 'custom-flash');
            $this->Session->setFlash(__($message), 'custom-flash');
            $this->redirect(['action' => 'index']);

        }
    }

    public function getClientFromArxivar(){
        $this->autoRender = false;
        $mySettings = $this->Setting->GetMySettings();
        if($mySettings['Setting']['arxivarUsername'] != null && $mySettings['Setting']['arxivarPassword'] != null){
            if($mySettings['Setting']['arxivarToken'] != null){
                if($mySettings['Setting']['arxivarTokenExpires'] >= date('Y-m-d')) {
                    $token = $mySettings['Setting']['arxivarToken'];
                }else {
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => 'http://smart.betsoft-srl.it:45080/ARXivarNextWebApi/api/Authentication',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => '{
                        "username": "'.$mySettings['Setting']['arxivarUsername'].'",
                        "password": "'.$mySettings['Setting']['arxivarPassword'].'",
                        "clientId": "api",
                        "clientSecret": "95353353EB504BF08CE07A4DC5FB1CA5"
                    }',
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json'
                        ),
                    ));

                    $response = curl_exec($curl);
                    curl_close($curl);

                    $token = (string)json_decode($response, true)['accessToken'];
                    $expires = json_decode($response, true)['expires'];
                    $date = strtotime ( $expires );
                    $expiresFormatted = date('Y-m-d H:i:s', $date);

                    $this->Setting->updateAll(['arxivarToken'=> "'".$token."'", 'arxivarTokenExpires'=>"'".$expiresFormatted."'"], ['company_id'=>MYCOMPANY]);
                }
            }else {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://smart.betsoft-srl.it:45080/ARXivarNextWebApi/api/Authentication',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                        "username": "'.$mySettings['Setting']['arxivarUsername'].'",
                        "password": "'.$mySettings['Setting']['arxivarPassword'].'",
                        "clientId": "api",
                        "clientSecret": "95353353EB504BF08CE07A4DC5FB1CA5"
                    }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);

                $token = (string)json_decode($response, true)['accessToken'];
                $expires = json_decode($response, true)['expires'];
                $date = strtotime ( $expires );
                $expiresFormatted = date('Y-m-d H:i:s', $date);

                $this->Setting->updateAll(['arxivarToken'=> "'".$token."'", 'arxivarTokenExpires'=>"'".$expiresFormatted."'"], ['company_id'=>MYCOMPANY]);

            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://smart.betsoft-srl.it:45080/ARXivarNextWebApi/api/v3/Views',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "maxItems": 0,
                    "documentTypeDescription": "Anagrafica paziente",
                    "id": "f5db2ac591a84e5099764d875a1b4212",
                    "description": "PazientiApi",
                    "user": 2,
                    "userCompleteName": "Administrator",
                    "documentType": 24,
                    "type2": 25,
                    "type3": 0,
                    "selectFields": "DOCNUMBER,TESTO115_25,TESTO68_25,TESTO72_25,TESTO73_25,TESTO74_25,DATA69_25,TABLE129_25,TESTO71_25,TABLE130_25",
                    "editFields": "",
                    "lockFields": {
                    "description": null,
                    "daAAndOr": 1,
                    "fields": [
                            {
                                "operator": 3,
                                "valore1": {
                                    "documentType": 24,
                                    "type2": 25,
                                    "type3": 0
                                },
                                "valore2": {
                                    "documentType": 0,
                                    "type2": 0,
                                    "type3": 0
                                },
                                "groupId": 0,
                                "fieldType": 0,
                                "additionalFieldType": 0,
                                "defaultOperator": 3,
                                "tableName": null,
                                "binderFieldId": 0,
                                "multiple": null,
                                "name": "DocumentType",
                                "externalId": "",
                                "description": "Classe documento",
                                "order": 1,
                                "dataSource": "",
                                "required": false,
                                "formula": "",
                                "className": "FieldBaseForSearchDocumentTypeDto",
                                "locked": false,
                                "comboGruppiId": "",
                                "dependencyFields": [],
                                "associations": null,
                                "isAdditional": false,
                                "visible": true,
                                "predefinedProfileFormula": null
                            }
                        ]
                    },
                    "orderFields": "",
                    "showFields": false,
                    "formOpen": false,
                    "allowEmptyFilterMode": 0,
                    "showGroupsMode": 0,
                    "canExecute": true,
                    "canUpdate": true,
                    "canDelete": true,
                    "searchFilterDto": {
                        "description": null,
                        "daAAndOr": 0,
                        "fields": null
                        }
                }',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.$token.'',
                    'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response, true)['data'];
            foreach($data as $date){

                $cliente = $this->Client->find('first', ['conditions' => ['externalId' => $date[0]], 'recursive'=>-1]);
                if(sizeof($cliente) == 0) {
                    $this->Client->Create();
                    $cliente = array();

                    $cliente['Client']['externalId'] = $date[0];
                    $cliente['Client']['ragionesociale'] = $date[2] . " " . $date[3];
                    $cliente['Client']['cf'] = $date[4];
                    $cliente['Client']['mail'] = $date[6];
                    $cliente['Client']['indirizzo'] = $date[9];
                    $cliente['Client']['citta'] = $date[10];
                    $cliente['Client']['company_id'] = 1;

                    $this->Client->save($cliente['Client']);

                }else {
                    //$prova++;
                    $cliente['Client']['ragionesociale'] = $date[2] . " " . $date[3];
                    $cliente['Client']['cf'] = $date[4];
                    $cliente['Client']['mail'] = $date[6];
                    $cliente['Client']['indirizzo'] = $date[9];
                    $cliente['Client']['citta'] = $date[10];
                    $this->Client->save($cliente['Client']);
                }
            }

            $this->Session->setFlash(__('I Clienti sono stati importati correttamente'), 'custom-flash');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function createClientLineXML($clientxml)
    {
        set_time_limit(300);
        $this->Render(false);
        $this->loadModel('Utilites');
        $this->loadModel('Currencies');
        $client = array();
        $id = (string)$clientxml->attributes()->code;
        $exists = $this->Client->find('first', ['conditions' => ['Client.id' => $id]]);
        if (!isset($exists['Client'])) {
            $this->Client->create();
            $client['Client']['id'] = (int)$id;
            $client['Client']['company_id'] = 1;
            $client['Client']['ragionesociale'] = (string)$clientxml->Name;
            if (sizeof($clientxml->Address) > 1) {
                $addresses = $clientxml->Address;
                foreach ($addresses as $address) {
                    if ($address->attributes()->type == "VIS") {
                        $client['Client']['indirizzo'] = (string)$address->AddressLine1;
                        $client['Client']['citta'] = (string)$address->City;
                        $client['Client']['cap'] = str_pad($address->PostalCode, 5, "0", STR_PAD_LEFT);
                        if(strcmp(strtoupper($address->City),"ROMA")==0){
                            $client['Client']['provincia'] = "RM";
                        }else{
                            $client['Client']['provincia'] = (string)$address->State->attributes()->code;
                        }
                        $client['Client']['cf'] = (string)$address->AddressLine2;
                        $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($address->Country->attributes()->code);
                        if (!empty($address->AddressLine3)) {
                            if (strpos((string)$address->AddressLine3, '@') !== false) {
                                $client['Client']['codiceDestinatario'] = '0000000';
                                $client['Client']['pec'] = (string)$address->AddressLine3;
                            } else {
                                $client['Client']['codiceDestinatario'] = (string)$address->AddressLine3;
                            }
                        } else {
                            $client['Client']['codiceDestinatario'] = '0000000';
                        }
                    }
                }
            } else {
                $client['Client']['indirizzo'] = (string)$clientxml->Address->AddressLine1;
                $client['Client']['citta'] = (string)$clientxml->Address->City;
                $client['Client']['cap'] = str_pad($clientxml->Address->PostalCode, 5, "0", STR_PAD_LEFT);
                if(strcmp(strtoupper($clientxml->Address->City),"ROMA")==0){
                    $client['Client']['provincia'] = "RM";
                }else{
                    $client['Client']['provincia'] = (string)$clientxml->Address->State->attributes()->code;
                }
                $client['Client']['cf'] = (string)$clientxml->Address->AddressLine2;
                $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($clientxml->Address->Country->attributes()->code);
                if (!empty($clientxml->Address->AddressLine3)) {
                    if (strpos((string)$clientxml->Address->AddressLine3, '@') !== false) {
                        $client['Client']['codiceDestinatario'] = '0000000';
                        $client['Client']['pec'] = (string)$clientxml->Address->AddressLine3;
                    } else {
                        $client['Client']['codiceDestinatario'] = (string)$clientxml->Address->AddressLine3;
                    }
                } else {
                    $client['Client']['codiceDestinatario'] = '0000000';
                }
            }

            $client['Client']['telefono'] = (string)$clientxml->Phone;
            $client['Client']['fax'] = (string)$clientxml->Fax;
            $commaInEmail = strpos((string)$clientxml->Email, ",");
            $client['Client']['mail'] = $commaInEmail === false ? (string)$clientxml->Email : substr((string)$clientxml->Email, 0, $commaInEmail);
            $client['Client']['extra_mail'] = $commaInEmail === false ? null : substr((string)$clientxml->Email, $commaInEmail, strlen($commaInEmail));

            if (!empty($clientxml->VATNumber)) {
                if ((strlen((string)$clientxml->VATNumber) == 13) && substr((string)$clientxml->VATNumber, 0, 2) === 'IT')
                    $client['Client']['piva'] = substr((string)$clientxml->VATNumber, 2);
                else
                    $client['Client']['piva'] = (string)$clientxml->VATNumber;
            }


            $client['Client']['payment_id'] = !empty($clientxml->SalesPaymentCondition) ? (string)$clientxml->SalesPaymentCondition->attributes()->code : null;
            $client['Client']['state'] = 1;
            $client['Client']['currency_id'] = (int)$this->Currencies->getCurrencyIdFromCode((string)$clientxml->PurchaseCurrency->attributes()->code);

            $newClient = $this->Client->save($client['Client']);

        } else {
            $client['Client']['id'] = (int)$id;
            $client['Client']['ragionesociale'] = (string)$clientxml->Name;

            if (sizeof($clientxml->Address) > 1) {
                $addresses = $clientxml->Address;
                foreach ($addresses as $address) {
                    if ($address->attributes()->type == "VIS") {
                        $client['Client']['indirizzo'] = (string)$address->AddressLine1;
                        $client['Client']['citta'] = (string)$address->City;
                        $client['Client']['cap'] = str_pad($address->PostalCode, 5, "0", STR_PAD_LEFT);
                        if(strcmp(strtoupper($address->City), "ROMA") == 0){
                            $client['Client']['provincia'] = "RM";
                        }else{
                            $client['Client']['provincia'] = (string)$address->State->attributes()->code;
                        }

                        $client['Client']['cf'] = (string)$address->AddressLine2;
                        $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($address->Country->attributes()->code);
                        if (!empty($address->AddressLine3)) {
                            if (strpos((string)$address->AddressLine3, '@') !== false) {
                                $client['Client']['codiceDestinatario'] = '0000000';
                                $client['Client']['pec'] = (string)$address->AddressLine3;
                            } else {
                                $client['Client']['codiceDestinatario'] = (string)$address->AddressLine3;
                            }
                        } else {
                            $client['Client']['codiceDestinatario'] = '0000000';
                        }
                    }
                }
            } else {
                $client['Client']['indirizzo'] = (string)$clientxml->Address->AddressLine1;
                $client['Client']['citta'] = (string)$clientxml->Address->City;
                $client['Client']['cap'] = str_pad($clientxml->Address->PostalCode, 5, "0", STR_PAD_LEFT);
                if(strcmp(strtoupper($clientxml->Address->City), "ROMA")== 0){
                    $client['Client']['provincia'] = "RM";
                }else{
                    $client['Client']['provincia'] = (string)$clientxml->Address->State->attributes()->code;
                }
                $client['Client']['cf'] = (string)$clientxml->Address->AddressLine2;
                $client['Client']['nation_id'] = (int)$this->Utilities->getNationIdFromShortCode($clientxml->Address->Country->attributes()->code);
                if (!empty($clientxml->Address->AddressLine3)) {
                    if (strpos((string)$clientxml->Address->AddressLine3, '@') !== false) {
                        $client['Client']['codiceDestinatario'] = '0000000';
                        $client['Client']['pec'] = (string)$clientxml->Address->AddressLine3;
                    } else {
                        $client['Client']['codiceDestinatario'] = (string)$clientxml->Address->AddressLine3;
                    }
                } else {
                    $client['Client']['codiceDestinatario'] = '0000000';
                }
            }
            $client['Client']['telefono'] = (string)$clientxml->Phone;
            $client['Client']['fax'] = (string)$clientxml->Fax;
            $commaInEmail = strpos((string)$clientxml->Email, ",");
            $client['Client']['mail'] = $commaInEmail === false ? (string)$clientxml->Email : substr((string)$clientxml->Email, 0, $commaInEmail);
            $client['Client']['extra_mail'] = $commaInEmail === false ? null : substr((string)$clientxml->Email, $commaInEmail, strlen($commaInEmail));
            $client['Client']['payment_id'] = !empty($clientxml->SalesPaymentCondition) ? (string)$clientxml->SalesPaymentCondition->attributes()->code : null;
            $client['Client']['currency_id'] = (int)$this->Currencies->getCurrencyIdFromCode((string)$clientxml->PurchaseCurrency->attributes()->code);
            $this->Client->save($client);
        }
    }

    public function addNewResellerClient($resellerId)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Clientreseller', 'Client']);
        $currentClient = $this->Client->find('first', ['conditions' => ['Client.id' => $resellerId, 'Client.company_id' => MYCOMPANY]]);
        $resellerName = $currentClient['Client']['ragionesociale'];
        $clientReseller = $this->Clientreseller->find('all', ['conditions' => ['Clientreseller.company_id' => MYCOMPANY, 'Clientreseller.client_id <>' => $resellerId, 'Clientreseller.state' => ATTIVO]]);
        $clients = $this->Client->find('list', ['fields' => ['Client.id', 'Client.ragionesociale'], ['contain' => 'Clientreseller'], 'conditions' => ['Client.reseller' => 0, 'Client.company_id' => MYCOMPANY, 'Client.state' => ATTIVO]]);

        $checkedArray = null;

        // Da ottimizzare
        foreach ($clientReseller as $reselled) {
            foreach ($clients as $key => $client) {
                if ($reselled['Clientreseller']['client_id'] == $key) {
                    if ($reselled['Clientreseller']['reseller_id'] != $resellerId) {
                        unset($clients[$key]);
                    } else {
                        $checkedArray[] = $reselled['Clientreseller']['client_id'];
                    }
                }
            }
        }

        $this->set('checkedArray', $checkedArray);
        $this->set('resellerId', $resellerId);
        $this->set('resellerName', $resellerName);
        $this->set('clients', $clients);
    }

    public function saveClientsReferent()
    {
        $this->autoRender = false;
        $this->Utilities->loadModels($this, ['Clientreseller']);

        $reseller = $this->request->data['Client']['resellerId'];

        $this->Clientreseller->deleteAll(['Clientreseller.company_id' => MYCOMPANY, 'reseller_id' => $this->request->data(['Client'])['resellerId']]);

        foreach ($this->request->data['Client']['client_id'] as $client) {
            $newClientReseller = $this->Clientreseller->create();
            $newClientReseller['Clientreseller']['client_id'] = $client;
            $newClientReseller['Clientreseller']['reseller_id'] = $reseller;
            $newClientReseller['Clientreseller']['company_id'] = MYCOMPANY;

            $this->Clientreseller->save($newClientReseller);
        }

        $this->redirect(['action' => 'indexResellerClient', $reseller]);
    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->loadModel('Bank');
        $this->Utilities->loadModels($this, ['Myhelper']);

        if ($this->request->is('post')) {
            $this->request->data['Client']['company_id'] = MYCOMPANY;

            if($_SESSION['Auth']['User']['dbname'] == "login_GE0041")
            {
                $this->request->data['Client']['data_nascita'] = date("Y-m-d", strtotime($this->request->data['Client']['data_nascita']));
                if($this->request->data['Client']['data_nascita'] == '1970-01-01' )
                {
                    $this->request->data['Client']['data_nascita'] = '0000-00-00';
                }
            }


            if ($this->request->data['Client']['pec'] == '') {
                unset($this->request->data['Client']['pec']);
            }
            if ($this->request->data['Client']['codiceDestinatario'] == '') {
                unset($this->request->data['Client']['codiceDestinatario']);
            }
            if ($this->request->data['Client']['note'] == '') {
                unset($this->request->data['Client']['note']);
            }

            if ($newClient = $this->Client->save($this->request->data)) {
                $this->Session->setFlash(__('Cliente salvato'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Il cliente non è stato salvato'), 'custom-danger');
            }
        }

        $helperMessage['invoice'] = $this->Myhelper->getMessage('electronicInvoiceClientCode');
        $helperMessage['splitpayment'] = $this->Myhelper->getMessage('clientSplitPayment');
        $helperMessage['billsdelay'] = $this->Myhelper->getMessage('clientBillDelay');
        $this->set('helperMessage', $helperMessage);

        //$banks = $this->Utilities->getBanksList();
        $banks = $this->Bank->getBanksListWithAbiAndCab();
        $payments = $this->Utilities->getPaymentsList();
        $vats = $this->Utilities->getVatsList();
        $nations = $this->Utilities->getNationsList();
        $currencies = $this->Utilities->getCurrenciesList();

        $this->set(compact('ivas', 'banks', 'payments', 'vats', 'nations', 'currencies'));
    }

    public function add_fast($redirect = 'index')
    {
        $this->layout = 'voidMegaformLayout';
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        if ($this->request->is('post')) {

            $this->Client->create();
            $this->request->data['Client']['company_id'] = MYCOMPANY;
            $this->request->data['Client']['state'] = 2;

            if ($newClient = $this->Client->save($this->request->data)) {

            } else {
                $this->Session->setFlash(__('Il cliente non è stato salvato'), 'custom-danger');
            }

            if ($this->request->is('ajax')) {
                $this->layout = false;
                $dataArray = ['entityData' => $newClient['Client']];
                print(json_encode($dataArray));
                die;
            }
        }
        $this->set('nations', $this->Utilities->getNationsList());
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Bank');
        $this->Utilities->loadModels($this, ['Myhelper']);
        $this->Client->id = $id;
        $this->set('client_id', $id);
        if (!$this->Client->exists()) {
            throw new NotFoundException(__('Cliente non valido'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0041")
            {
                $this->request->data['Client']['data_nascita'] = date("Y-m-d", strtotime($this->request->data['Client']['data_nascita']));
            }

            if ($this->request->data['Client']['pec'] == '') {
                unset($this->request->data['Client']['pec']);
            }
            if ($this->request->data['Client']['codiceDestinatario'] == '') {
                unset($this->request->data['Client']['codiceDestinatario']);
            }

            if ($this->Client->save($this->request->data)) {
                $this->Session->setFlash(__('Cliente salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Il cliente non é stato salvato, riprovare.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Client->read(null, $id);
        }

        //$banks = $this->Utilities->getBanksList();
        $banks = $this->Bank->getBanksListWithAbiAndCab();
        $payments = $this->Utilities->getPaymentsList();
        $vats = $this->Utilities->getVatsList();
        $nations = $this->Utilities->getNationsList();
        $currencies = $this->Utilities->getCurrenciesList();

        $helperMessage['invoice'] = $this->Myhelper->getMessage('electronicInvoiceClientCode');
        $helperMessage['splitpayment'] = $this->Myhelper->getMessage('clientSplitPayment');
        $helperMessage['billsdelay'] = $this->Myhelper->getMessage('clientBillDelay');
        $this->set('helperMessage', $helperMessage);

        $this->set(compact('ivas', 'banks', 'payments', 'vats', 'nations', 'currencies'));
    }

    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Messages']);
        $asg = ["il", "cliente", "M"];
        if ($this->Client->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Client->find('first', ['conditions' => ['Client.id' => $id, 'Client.company_id' => MYCOMPANY]]);
        if ($this->Client->hide($currentDeleted['Client']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }

    public function getClientPaymentMethod()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $paymentMethod = $this->Utilities->getDefaultClientPaymentMethodId($_POST['clientId']) : $paymentMethod = $this->Utilities->getDefaultClientPaymentMethodId($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientPaymentMethodId($this->Utilities->getCustomerId($customerName));
        return $paymentMethod;
    }


    public function getDefaultClientPaymentMethodCollectionfees()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $collectionfees = $this->Utilities->getDefaultClientPaymentMethodCollectionfees($_POST['clientId']) : $collectionfees = $this->Utilities->getDefaultClientPaymentMethodCollectionfees($this->Utilities->getCustomerId($customerName));
        return $collectionfees;
    }

    public function getClientSplitPayment()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $splitPayment = $this->Utilities->getDefaultClientSplitPayment($_POST['clientId']) : $splitPayment = $this->Utilities->getDefaultClientSplitPayment($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientSplitPayment($this->Utilities->getCustomerId($customerName));
        return $splitPayment;
    }

    public function isClientPa()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $isPa = $this->Utilities->isClientPa($_POST['clientId']) : $isPa = $this->Utilities->isClientPa($this->Utilities->getCustomerId($customerName));
        // return $this->Utilities->isClientPa($this->Utilities->getCustomerId($customerName));
        return $isPa;
    }

    public function loadClientWithholdingTax()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $clientWithholdingtax = $this->Utilities->loadClientWithholdingTax($_POST['clientId']) : $clientWithholdingtax = $this->Utilities->loadClientWithholdingTax($this->Utilities->getCustomerId($customerName));
        return $clientWithholdingtax;
    }

    public function loadClientCurrency()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $currency = $this->Utilities->loadClientCurrency($_POST['clientId']) : $currency = $this->Utilities->loadClientCurrency($this->Utilities->getCustomerId($customerName));
        // return $this->Utilities->loadClientCurrency($this->Utilities->getCustomerId($customerName));
        return $currency;
    }


    public function getClientVat()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $customerName = $_POST['clientId'];
        is_numeric($_POST['clientId']) ? $vat = $this->Utilities->getDefaultClientVat($_POST['clientId']) : $vat = $this->Utilities->getDefaultClientVat($this->Utilities->getCustomerId($customerName));
        //return $this->Utilities->getDefaultClientVat($this->Utilities->getCustomerId($customerName));
        return $vat;
    }

    // Controllo duplicato fattura
    public function checkClientPivaDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        if(isset($_POST['client_id'])){
            $return = $this->Utilities->checkClientPivaDuplicate($_POST['piva'], $_POST['client_id']);
        }else{
            $return = $this->Utilities->checkClientPivaDuplicate($_POST['piva']);
        }

        return json_encode($return);
    }

    // Controllo duplicato fattura
    public function checkClientCfDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        if(isset($_POST['client_id'])){
            $return = $this->Utilities->checkClientCfDuplicate($_POST['cf'], $_POST['client_id']);
        }else{
            $return = $this->Utilities->checkClientCfDuplicate($_POST['cf']);
        }

        return json_encode($return);
    }

    public function getClientConstructionsites()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        return $this->Client->getConstructionsites($_POST['clientId']);
    }

    public function getClientQuotes()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);
        return $this->Client->getQuotes($_POST['clientId']);
    }

    public function enable()
    {
        $this->autoRender = false;
        $this->Client->enable();
    }

    public function getClientData()
    {
        $this->autoRender = false;
        $this->loadModel('Payment');

        $customer = $this->Client->find('first', ['conditions' => ['Client.id' => $_POST['clientId']]]);

        $payment = $this->Payment->find('first', ['recursive' => 0, 'conditions' => ['Payment.id' => $customer['Client']['payment_id']]]);
        $customer['Payment'] = $payment != null ? $payment['Payment'] : null;

        return json_encode($customer);
    }
}
