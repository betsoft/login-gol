<?php
App::uses('AppController', 'Controller');

class StoragesController extends AppController
{


    public function importStorages()
    {
        $this->loadModel('Utilities');
        $time_pre = microtime(true);

        if ($this->request->is('post')) {
            $filename = $this->data['Storages']['file']['name'];
            $tmpext = explode(".", $filename);
            $ext = end($tmpext);

            if ($ext == "zip") {
                $za = new ZipArchive();
                $za->open($this->request->data['Storages']['file']['tmp_name']);
                for ($i = 0; $i < $za->numFiles; $i++) {
                    $stat = $za->statIndex($i);
                    $basename = basename($stat['name']);
                    $file = file_get_contents('zip://' . $this->request->data['Storages']['file']['tmp_name'] . '#' . $basename . '');
                    $xmlelement = new SimpleXMLElement($file);
                    $storages = $xmlelement->Items->Item;

                    foreach ($storages as $storagexml)
                    {
                        $codice = (string)$storagexml->attributes()->code;

                        $storage = $this->Storage->find('first', ['recursive'=>-1,'conditions' => ['Storage.codice' => $codice]]);
                        if($storage == null){

                            $storage = array();

                            $this->Storage->create();

                            $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

                            $storage['Storage']['descrizione'] = (string)$storagexml->Description;

                            $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

                            $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

                            $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
                            $storage['Storage']['company_id'] = 1;

                            $categorie = $storagexml->ItemCategory;
                            if (!empty($categorie)) {
                                foreach ($categorie as $categoria) {
                                    if ($categoria->attributes()->number == 5) {
                                        $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                                    }
                                }
                            }
                            $this->Storage->save($storage);

                        }
                        else{
                            $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

                            $storage['Storage']['descrizione'] = (string)$storagexml->Description;

                            $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

                            $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

                            $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
                            $storage['Storage']['company_id'] = 1;

                            $categorie = $storagexml->ItemCategory;
                            if (!empty($categorie)) {
                                foreach ($categorie as $categoria) {
                                    if ($categoria->attributes()->number == 5) {
                                        $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                                    }
                                }
                            }

                            $this->Storage->save($storage);

                        }

                        //$this->createStorageFromXml($storage);
                    }
                }
            } else {
                $file = file_get_contents($this->request->data['Storages']['file']['tmp_name']);
                $xmlelement = new SimpleXMLElement($file);
                $storages = $xmlelement->Items->Item;
                foreach ($storages as $storagexml)
                {
                    $codice = (string)$storagexml->attributes()->code;

                    $storage = $this->Storage->find('first', ['recursive'=>-1,'conditions' => ['Storage.codice' => $codice]]);
                    if($storage == null){

                        $storage = array();

                        $this->Storage->create();

                        $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

                        $storage['Storage']['descrizione'] = (string)$storagexml->Description;

                        $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

                        $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

                        $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
                        $storage['Storage']['company_id'] = 1;

                        $categorie = $storagexml->ItemCategory;
                        if (!empty($categorie)) {
                            foreach ($categorie as $categoria) {
                                if ($categoria->attributes()->number == 5) {
                                    $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                                }
                            }
                        }
                        $this->Storage->save($storage);

                    }
                    else{
                        $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

                        $storage['Storage']['descrizione'] = (string)$storagexml->Description;

                        $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

                        $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

                        $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
                        $storage['Storage']['company_id'] = 1;

                        $categorie = $storagexml->ItemCategory;
                        if (!empty($categorie)) {
                            foreach ($categorie as $categoria) {
                                if ($categoria->attributes()->number == 5) {
                                    $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                                }
                            }
                        }

                        $this->Storage->save($storage);

                    }
                }
            }
            $this->Session->setFlash(__('Il Magazzino è stato importato'), 'custom-flash');
            $this->redirect(['action' => 'index']);

        }

    }

    public function createStorageFromXml($storagexml)
    {
        $this->Render(false);
        $this->loadModel('Utilites');

        $codice = (string)$storagexml->attributes()->code;

        $storage = $this->Storage->find('first', ['recursive'=>-1,'conditions' => ['Storage.codice' => $codice]]);
        if($storage == null){

            $storage = array();

            $this->Storage->create();

            $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

            $storage['Storage']['descrizione'] = (string)$storagexml->Description;

            $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

            $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

            $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
            $storage['Storage']['company_id'] = 1;

            $categorie = $storagexml->ItemCategory;
            if (!empty($categorie)) {
                foreach ($categorie as $categoria) {
                    if ($categoria->attributes()->number == 5) {
                        $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                    }
                }
            }
            $this->Storage->save($storage);

        }
        else{
            $storage['Storage']['codice'] = (string)$storagexml->attributes()->code;

            $storage['Storage']['descrizione'] = (string)$storagexml->Description;

            $storage['Storage']['unit_id'] = (int)$this->Utilities->getUnitsIdFromName((string)$storagexml->Sales->Unit->attributes()->code);

            $storage['Storage']['prezzo'] = (string)$storagexml->Sales->Price->Value;

            $storage['Storage']['barcode'] = (string)$storagexml->Barcode;
            $storage['Storage']['company_id'] = 1;

            $categorie = $storagexml->ItemCategory;
            if (!empty($categorie)) {
                foreach ($categorie as $categoria) {
                    if ($categoria->attributes()->number == 5) {
                        $storage['Storage']['category_id'] = $this->Utilities->getCategoryByName((string)$categoria->attributes()->code);
                    }
                }
            }

            $this->Storage->save($storage);

        }


    }

    public function index()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Csv']);
        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1];
        if ($_SESSION['Auth']['User']['dbname'] == 'login_GE0046') {
            $filterableFields = ['#htmlElements[0]', 'Storage__codice', 'Storage__descrizione', 'Supplier__name', null, null, null, 'barcode', null, null];
            $sortableFields = [[null, ''], ['codice', 'Codice'], ['descrizione', 'Articolo'], ['Supplier__name', 'Fornitore principale'], ['sell_price', 'Prezzo di vendita'], ['last_buy_price', 'Ultimo prezzo d\'acquisto'], ['Units__description', 'Unità di misura'], ['Barcode', 'Codice a barre'], [null, 'Quantità a magazzino'], ['#actions']];
        }
        else if($_SESSION['Auth']['User']['dbname'] == 'login_GE0047')
        {
            $filterableFields = ['#htmlElements[0]', 'Storage__codice', 'Storage__descrizione', 'Supplier__name', 'Category__description', null, null, null, null, null];
            $sortableFields = [[null, ''], ['codice', 'Codice'], ['descrizione', 'Articolo'], ['Supplier__name', 'Fornitore principale'], ['Category__description', 'Categoria'], ['sell_price', 'Prezzo di vendita'], ['Units__description', 'Unità di misura'], [null, 'Quantità a magazzino'], [null, 'Quantità nei furgoni'], ['#actions']];
        }
        else {
            $filterableFields = ['#htmlElements[0]', 'Storage__codice', 'Storage__descrizione', 'Supplier__name', 'Category__description', null, null, null, 'barcode', null, null];
            $sortableFields = [[null, ''], ['codice', 'Codice'], ['descrizione', 'Articolo'], ['Supplier__name', 'Fornitore principale'], ['Category__description', 'Categoria'], ['sell_price', 'Prezzo di vendita'], ['last_buy_price', 'Ultimo prezzo d\'acquisto'], ['Units__description', 'Unità di misura'], ['Barcode', 'Codice a barre'], [null, 'Quantità a magazzino'], ['#actions']];
        }

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        } else {
            null;
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && ($_POST['data']['createCsv'] == 'xls' || $_POST['data']['createCsv'] == 'xls2')) {
            $this->autoRender = false;
            if (isset($_POST['data']['arrayToPost']) && ($_POST['data']['arrayToPost'] != '')) {
                $arrayToPost = explode(",", $_POST['data']['arrayToPost']);
                $conditionsArray['Storage.id IN'] = $arrayToPost;
                $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
                //	echo 'Codice;Articolo;Fornitore;Prezzo di vendita;Ultimo prezzo d\'acquisto;Unità di misura;Codice a barre;Quantità a magazzino;'."\r\n";
            } else {
                if (isset($_POST['data']['toprint'])) {
                    $conditionsArray['Storage.id IN'] = $_POST['data']['toprint'];
                    $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
                    //	echo 'Codice;Articolo;Fornitore;Prezzo di vendita;Ultimo prezzo d\'acquisto;Unità di misura;Codice a barre;Quantità a magazzino;'."\r\n";
                } else {
                    $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
                    //	echo 'Codice;Articolo;Fornitore;Prezzo di vendita;Ultimo prezzo d\'acquisto;Unità di misura;Codice a barre;Quantità a magazzino;'."\r\n";
                }
            }

            if ($_POST['data']['createCsv'] == 'xls') {
                echo 'Codice;Articolo;Fornitore;Prezzo di vendita;Ultimo prezzo d\'acquisto;Unità di misura;Codice a barre;Quantità a magazzino;IVA;' . "\r\n";
            } else {
                echo 'Codice,Articolo,Fornitore,Prezzo di vendita,Ultimo prezzo d\'acquisto,Unità di misura,Codice a barre,Quantità a magazzino,IVA,' . "\r\n";
            }

            foreach ($dataForXls as $xlsRow) {
                $storageQuantity = $this->Utilities->getAvailableQuantity($xlsRow['Storage']['id']);

                $iva = '';

                if ($xlsRow['Ivas']['descrizione'] != null) {
                    $iva = $xlsRow['Ivas']['descrizione'] . ' - ' . $xlsRow['Ivas']['percentuale'];
                }

                // Se esiste la personalizzazione settings multicsv
                if ($_POST['data']['createCsv'] == 'xls') {
                    echo $xlsRow['Storage']['codice'] . SCSV . $xlsRow['Storage']['descrizione'] . SCSV . $xlsRow['Supplier']['name'] . SCSV . $xlsRow['Storage']['prezzo'] . SCSV . $xlsRow['Storage']['last_buy_price'] . SCSV . $xlsRow['Units']['description'] . SCSV . $xlsRow['Storage']['barcode'] . SCSV . $storageQuantity . SCSV . $iva . SCSV . "\r\n";
                } else {
                    echo $xlsRow['Storage']['codice'] . ',' . $xlsRow['Storage']['descrizione'] . ',' . $xlsRow['Supplier']['name'] . ',' . $xlsRow['Storage']['prezzo'] . ',' . $xlsRow['Storage']['last_buy_price'] . ',' . $xlsRow['Units']['description'] . ',' . $xlsRow['Storage']['barcode'] . ',' . $storageQuantity . ',' . $iva . ',' . "\r\n";
                }
            }
        } else {
            $this->Storage->recursive = 1;
            $this->set('filterableFields', $filterableFields);

            $this->paginate = ['contain' => ['Category', 'Supplier', 'Units'], 'conditions' => $conditionsArray, 'order' => ['Storage.codice' => 'asc'], 'limit' => 100];
            $this->set('storages', $this->paginate());

            $this->set('sortableFields', $sortableFields);

            $this->set('utilities', $this->Utilities);
            $this->set('myCompany', MYCOMPANY);
            $this->render('index');
        }
    }

    public function indexvariation($id = null)
    {

        $this->loadModel('Utilities');

        // $this->set('id',$id);
        if ($id != null) {
            $this->set('id', $id);
            $this->Session->write('variantId', $id);
        } else {
            $id = $this->Session->read('variantId');
            $this->set('id', $id);
        }

        $this->Utilities->loadmodels($this, ['Storage', 'Csv', 'Storage']);

        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.parent_id' => $id];
        // $filterableFields = ['Storage.code','Storage.description',null];
        $filterableFields = ['Storage__codice', 'Storage__descrizione', null];
        $sortableFields = [['code', 'Codice'], ['description', 'Descrizione'], ['#actions']];
        // if($this->request->is('ajax') && isset($this->request->data['filters']))
        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            echo 'Codice;Descrizione;' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                echo $xlsRow['Storage']['codice'] . ';' . $xlsRow['Storage']['descrizione'] . ';' . "\r\n";
            }
        } else {
            $storages = $this->Storage->find('all', ['contain' => ['Supplier', 'Units'], 'conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            $this->set('storages', $storages);
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('thisStorage', $this->Storage->find('first', ['conditions' => ['Storage.id' => $id]])['Storage']);
            $this->set('storageId', $id);
            $this->render('indexvariation');
        }
    }

    public function indexstoragevalue()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Csv']);

        $filterableFields = ['Storage__codice', 'Storage__descrizione', null, null, '#htmlElements[0]'];
        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 1];
        $sortableFields = [['codice', 'Codice'], ['descrizione', 'Articolo'], [null, 'Quantità a magazzino'], [null, 'U.m.'], [null, 'Valorizzazione']];

        $arrayDeposits = ['all' => 'Tutti i depositi'];

        $depositId = 'all'; // Di default tutti i depositi

        $arrayDeposits = $arrayDeposits + $this->Utilities->getDepositsList();
        $this->set('deposits', $arrayDeposits);

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        } else {
            null;
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $depositId = $this->request->data['filters']['deposit_id'];
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            $storageEvaluationMethod = $this->Utilities->getStorageEvaluationMethod();
            echo 'Codice;Articolo;Fornitore;Quantità a magazzino;Unità di misura;Valorizzazione;' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                $storageQuantity = $this->Utilities->getAvailableQuantity($xlsRow['Storage']['id'], $depositId);
                $storageQuantityValue = $this->Utilities->getAvailableQuantityValue($xlsRow['Storage']['id'], $storageEvaluationMethod, $depositId);
                echo $xlsRow['Storage']['codice'] . ';' . $xlsRow['Storage']['descrizione'] . ';' . $xlsRow['Supplier']['name'] . ';' . $storageQuantity . ';' . $xlsRow['Units']['description'] . ';' . $storageQuantityValue . ";" . "\r\n";
            }
        } else {
            //$storages =  $this->Storage->find('all',['conditions'=>$conditionsArray, 'contain'=>['Supplier','Units'],'order' => ['Storage.descrizione' => 'asc']]);
            $storages = $this->Storage->find('all', ['conditions' => $conditionsArray, 'contain' => ['Supplier', 'Units', 'Storagemovement'], 'order' => ['Storage.descrizione' => 'asc']]);

            $iteration = $storages;
            $arrayOfids = [];
            // START FIX
            foreach ($iteration as $key => $storagemovement) {
                $fixedStorageCount = 0;
                foreach ($storagemovement['Storagemovement'] as $counter) {
                    if (isset($this->request->data['filters']['deposit_id'])) {
                        if ($counter['deposit_id'] == $this->request->data['filters']['deposit_id'] || $this->request->data['filters']['deposit_id'] == 'all') {
                            $fixedStorageCount += $counter['quantity'];
                        }
                    } else {
                        $fixedStorageCount += $counter['quantity'];
                    }
                }
                $storages[$key]['Storage']['calculatedQuantity'] = $fixedStorageCount;
                if ($fixedStorageCount == 0) {
                    unset($storages[$key]);
                } else {
                    $arrayOfids[] = $storages[$key]['Storage']['id'];
                }
            }

            $this->paginate = ['limit' => 10];
            if ($arrayOfids == null) {
                $this->paginate = $this->Storage->find('all', ['conditions' => ['Storage.id  ' => '-1'], 'contain' => ['Supplier', 'Units', 'Storagemovement']]);
            } else {
                if (count($arrayOfids) > 1) {
                    // $this->paginate  = $this->Storage->find('all', ['conditions' => ['Storage.id IN ' => $arrayOfids], 'contain' => ['Supplier', 'Units', 'Storagemovement'], 'order' => ['Storage.descrizione' => 'asc']]);
                    $this->paginate = ['conditions' => ['Storage.id IN ' => $arrayOfids], 'contain' => ['Supplier', 'Units', 'Storagemovement'], 'order' => ['Storage.descrizione' => 'asc']];
                } else {
                    $this->paginate = $this->Storage->find('all', ['conditions' => ['Storage.id ' => $arrayOfids[0]], 'contain' => ['Supplier', 'Units', 'Storagemovement'], 'order' => ['Storage.descrizione' => 'asc']]);
                }
            }

            $this->set('storages', $this->paginate());

            // END FIX

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('getAviableQuantity', $this->Utilities);
            $this->set('depositId', $depositId);
            $this->set('myCompany', MYCOMPANY);
            $this->set('storageEvaluationMethod', $this->Utilities->getStorageEvaluationMethod());
            $this->render('indexstoragevalue');
        }
    }

    // Trasferimento merce tra depositi
    public function indexdepositmovement()
    {
        $this->loadModel('Utilities');

        $depositList = $this->Utilities->getDepositsList();
        $defaultDeposit = $this->Utilities->getDefaultDeposits()['Deposits']['id'];
        $this->set('deposits', $depositList);
        $this->set('defaultDeposit', $defaultDeposit);

        // Definisco articoli per autocomplete
        $this->set('magazzini', $this->Utilities->getStoragesWithVariations($defaultDeposit, 'true', false, null, true));

        try {
            if ($this->request->is('post')) {
                $numeroMevimento = 1;
                foreach ($this->request->data['Good'] as $storage) {
                    $this->Utilities->createDepositStorageTransfer($storage['storage_id'], $storage['quantita'], $this->request->data['Storage']['startDeposit'], $this->request->data['Storage']['endDeposit'], $numeroMevimento, $storage['variation_id']);
                    $numeroMevimento = $numeroMevimento + 1;
                }
                $this->Session->setFlash(__('Trasferimento avvenuto correttamente'), 'custom-flash');
            }
        } catch (exception $ecc) {
            $this->Session->setFlash(__('Errore durante il trasferimento di magazzino'), 'custom-danger');
        }
    }

    // Utilizzata unicamente nel trasferimento tra depositi
    public function getDepositArticles()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        print(json_encode($this->Utilities->getStoragesWithVariations($_POST['deposit'], 'true', 'false', null, true)));
    }

    public function getAvailableQuantity()
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $quantity = $this->Utilities->getAvailableQuantity($_POST['storageId'], $_POST['deposit']);
        return $quantity;
    }

    public function indexnotmovable()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Csv', 'Storage']);
        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.movable' => 0];
        $filterableFields = ['Storage__codice', 'Storage__descrizione', null, 'iva', null];
        $sortableFields = [['Storage__codice', 'Codice'], ['descrizione', 'Descrizione'], ['sell_price', 'Prezzo di vendita'], [null, 'Iva'], ['#actions']];

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
        } else {
            null;
        }

        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $dataForXls = $this->Storage->find('all', ['conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);
            echo 'Codice;Descrizione;Prezzo di vendita;Iva;' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                echo $xlsRow['Storage']['codice'] . ';' . $xlsRow['Storage']['descrizione'] . ';' . $xlsRow['Storage']['prezzo'] . ';' . $xlsRow['Ivas']['descrizione'] . ';' . "\r\n";
            }
        } else {
            $this->Storage->recursive = 1;
            $this->paginate = ['contain' => ['Supplier', 'Units', 'Ivas'], 'conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']];
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('storages', $this->paginate());
            $this->set('getAviableQuantity', $this->Utilities);
            $this->set('myCompany', MYCOMPANY);
            $this->render('indexnotmovable');
        }
    }


    public function depositdetails($storageId)
    {
        $this->loadModel('Utilities');
        $conditionsArray = ['Storage.company_id' => MYCOMPANY, 'Storage.id' => $storageId];
        $filterableFields = [null, null];
        //$sortableFields = [[null,'Deposito'],[null,'Quantità disponibile'],['#actions']];
        $sortableFields = [[null, 'Deposito'], [null, 'Quantità disponibile']];

        if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        $this->Storage->recursive = 1;

        $storages = $this->Storage->find('first', ['contain' => ['Supplier', 'Units'], 'conditions' => $conditionsArray, 'order' => ['Storage.descrizione' => 'asc']]);

        $deposits = $this->Utilities->getDepositsList();
        $arrayDeposit = [];
        foreach ($deposits as $key => $deposit) {
            $arrayDeposit[$key]['description'] = $deposit;
            $arrayDeposit[$key]['id'] = $key;
            $arrayDeposit[$key]['quantity'] = $this->Utilities->getAvailableQuantity($storageId, $key);
        }

        $this->set('sortableFields', $sortableFields);
        $this->set('filterableFields', $filterableFields);
        $this->set('storages', $storages);
        $this->set('deposits', $arrayDeposit);

        $this->set('myCompany', MYCOMPANY);
        $this->render('depositdetails');
    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier', 'Units','Category']);

        $this->set('suppliers', $this->Supplier->getList());

        if ($this->request->is('post')) {
            /* Prendo l'ID del fornitore per il salvataggio dei dati, se il fornitore esiste ovviamente */
            $fornitore = $this->Supplier->find('first', ['conditions' => ['Supplier.name' => $this->request->data['Storage']['codice_fornitore']], 'fields' => ['Supplier.id']]);

            // Se il fornitore esiste
            if (!empty($fornitore['Supplier']['id'])) {
                // Definisco il client id
                $supplierId = $fornitore['Supplier']['id'];
            } else {
                $newSupplierId = $this->Utilities->createSupplier($this->request->data['Storage']['codice_fornitore']);
                // Definisco il client id
                $supplierId = $newSupplierId; //$newSupplier['Supplier']['id'];
            }

            $this->request->data['Storage']['company_id'] = MYCOMPANY;
            $this->request->data['Storage']['supplier_id'] = $supplierId;
            $this->Storage->create();

            if (isset($this->request->data['Storage']['certified'])) {
                $fileUplaoded = $this->Utilities->storeUploadedDocument($this->request->data['Storage']['certified'], 'documents/storageCertification/' . MYCOMPANY)['file_name'];
                $this->request->data['Storage']['certified'] = $fileUplaoded;
            }

            if ($this->Storage->save($this->request->data)) {
                $this->Session->setFlash(__('Articolo salvato'), 'custom-flash');

                if ($this->request->is('ajax')) {
                    $this->set('data', $this->request->data);
                    $this->autoRender = false;
                    $this->render('/Ajax/flash');
                    die;
                }

                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Questo articolo non è stato salvato'), 'custom-danger');
            }
        }


        $this->set('units', $this->Units->getList());
        $this->set('suppliers', $this->Supplier->getList());
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047" ||$_SESSION['Auth']['User']['dbname'] == "login_GE0048" || $_SESSION['Auth']['User']['dbname'] == "login_GE0049" )
            $this->set('categories', $this->Category->getList());
        $this->set('vats', $this->Utilities->getVatsList());

        $this->render('add');
    }


    public function addvariation($storageId, $id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Variation', 'Storage', 'Myhelper', 'Units']);

        // Se storage id ha un segno negativo vuol dire che il padre è una variante !
        $redirect = ['action' => 'indexvariation'];

        if ($storageId > 0) {

            $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId]]);
            $this->set('storage', $storage);

            $parentCode = $storage['Storage']['codice'];
            $parentDescription = $storage['Storage']['descrizione'];
            $this->set('parentCode', $parentCode);
            $this->set('storageId', $storageId);
            $this->set('parentDescription', $parentDescription);
            $storageIdToSave = $storageId;
            $redirect = ['action' => 'indexvariation', $storageIdToSave];
        }

        if ($this->request->is('post')) {
            $this->request->data['Storage']['company_id'] = MYCOMPANY;
            $this->request->data['Storage']['state'] = 1;
            $this->request->data['Storage']['parent_id'] = $storageId; // ??? Controllare questo
            //$this->request->data['Variation']['code']= $this->request->data['Variation']['parentcode'] . $this->request->data['Variation']['code'];
            $this->request->data['Storage']['codice'] = $this->request->data['Storage']['parentcode'] . $this->request->data['Storage']['codice'];


            $this->request->data['Storage']['descrizione'] = $this->request->data['Storage']['parentdescription'] . ' ' . $this->request->data['Storage']['descrizione'];
            if ($newVariation = $this->Storage->save($this->request->data)) {
                if ($this->request->data['Storage']['sn'] == 1) {
                    // Carico la quantità che è 1
                    $this->Utilities->storageLoad($newVariation['Storage']['id'], 1, 'Carico automatico variante univoca di magazzino', 0, $this->Utilities->getDefaultDepositId(), 1, 'NEW_VAR_AUT_LO', $newVariation['Storage']['id']);
                }
                $this->Session->setFlash(__('Variante articolo salvata'), 'custom-flash');

                if ($this->request->is('ajax')) {
                    $this->set('data', $this->request->data);
                    $this->autoRender = false;
                    $this->render('/Ajax/flash');
                    die;
                }
                $this->redirect($redirect);
            } else {
                $this->Session->setFlash(__('Questo variante articolo non è stata salvata'), 'custom-danger');
            }
        }

        $this->loadModel('Utilities');
        $this->set('units', $this->Units->getList());
        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());
        $this->set('vats', $this->Utilities->getVatsList());

        $helperMessage = $this->Myhelper->getMessage('variationUnique');
        $this->set('helperMessage', $helperMessage);

        $this->render('addvariation');
    }


    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier', 'Units', 'Category']);
        $this->Storage->id = $id;

        if (!$this->Storage->exists()) {
            throw new NotFoundException(__('Articolo non valido'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $fornitore = $this->Supplier->find('first', ['conditions' => ['Supplier.name' => $this->request->data['Storage']['codice_fornitore']], 'fields' => ['Supplier.id']]);
            if (isset($fornitore['Supplier']['id'])) {
                $this->request->data['Storage']['supplier_id'] = $fornitore['Supplier']['id'];
            }

            $currentStorage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $id]]);

            if (isset($currentStorage['Storage']['certified']['name'])) {
                if ($this->request->data['Storage']['certified']['name'] == '') {
                    $this->request->data['Storage']['certified'] = $currentStorage['Storage']['certified'];

                } else {
                    $fileUplaoded = $this->Utilities->storeUploadedDocument($this->request->data['Storage']['certified'], 'documents/storageCertification/' . MYCOMPANY)['file_name'];
                    $this->request->data['Storage']['certified'] = $fileUplaoded;
                }
            }

            if ($this->Storage->save($this->request->data)) {
                $this->Session->setFlash(__('Articolo salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Questo articolo non é stato salvato, riprovare.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Storage->read(null, $id);
        }

        $this->loadModel('Utilities');
        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());
        $this->set('units', $this->Units->getList());
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047" || $_SESSION['Auth']['User']['dbname'] == "login_GE0048" || $_SESSION['Auth']['User']['dbname'] == "login_GE0049"){
            $this->set('categories', $this->Category->getList());
        }
        $this->set('vats', $this->Utilities->getVatsList());
        $this->render('edit');
    }

    public function editvariation($storageId)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Units');
        $this->Storage->id = $storageId;

        if (!$this->Storage->exists()) {
            throw new NotFoundException(__('Variante articolo non trovata'));
        }

        $redirect = ['action' => 'indexvariation'];
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Storage']['codice'] = $this->request->data['Storage']['parentcode'] . $this->request->data['Storage']['codice'];
            $this->request->data['Storage']['descrizione'] = $this->request->data['Storage']['parentdescription'] . ' ' . $this->request->data['Storage']['descrizione'];
            if ($this->Storage->save($this->request->data)) {
                $this->Session->setFlash(__('Variante articolo salvata correttamente'), 'custom-flash');
                $redirect = ['action' => 'indexvariation', $this->request->data['redirect']];
                $this->redirect($redirect);
            } else {
                $this->Session->setFlash(__('Questa variante non è stata modificata, riprovare.'), 'custom-danger');
                $redirect = ['action' => 'indexvariation', $this->request->data['redirect']];
                $this->redirect($redirect);
            }
        } else {
            $this->request->data = $this->Storage->read(null, $storageId);
        }

        $this->set('parentId', $this->Utilities->getStorageParentId($storageId));
        $this->set('parentCode', $this->Utilities->getStorageParentCodeFromCurrentStorage($storageId));
        $this->set('parentDescription', $this->Utilities->getStorageParentDescriptionFromCurrentStorage($storageId));

        $this->set('storageId', $storageId);
        $this->set('vats', $this->Utilities->getVatsList());
        // $this->set('units',$this->Utilities->getUnitsList());
        $this->set('units', $this->Units->getList());
        $this->render('editvariation');
    }

    public function addnotmovable()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier']);

        $this->set('suppliers', $this->Supplier->getList());

        if ($this->request->is('post')) {
            $this->request->data['Storage']['company_id'] = MYCOMPANY;
            $this->Storage->create();

            if ($this->request->data['Storage']['codice'] == null) {
                unset($this->Storage->validate['codice']);
            }

            if ($this->Storage->save($this->request->data)) {
                $this->Session->setFlash(__('Articolo salvato'), 'custom-flash');

                if ($this->request->is('ajax')) {
                    $this->set('data', $this->request->data);
                    $this->autoRender = false;
                    $this->render('/Ajax/flash');
                    die;
                }

                $this->redirect(['action' => 'indexnotmovable']);
            } else {
                $this->Session->setFlash(__('Questo articolo non è stato salvato'), 'custom-danger');
            }
        }
        $this->loadModel('Utilities');
        $this->set('vats', $this->Utilities->getVatsList());

        $this->render('addnotmovable');
    }

    public function editnotmovable($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier']);

        $this->Storage->id = $id;

        if (!$this->Storage->exists()) {
            throw new NotFoundException(__('Articolo non valido'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $currentStorage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $id]]);

            if ($this->request->data['Storage']['codice'] == null) {
                unset($this->Storage->validate['codice']);
            }

            if ($this->Storage->save($this->request->data)) {
                $this->Session->setFlash(__('Articolo salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => 'indexnotmovable']);
            } else {
                $this->Session->setFlash(__('Questo articolo non é stato salvato, riprovare.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Storage->read(null, $id);
        }

        $this->loadModel('Utilities');
        $this->set('vats', $this->Utilities->getVatsList());
        $this->render('editnotmovable');
    }

    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages', 'Messages']);

        $asg = ["l'", "articolo di magazzino", "M"];
        if ($this->Storage->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Storage->find('first', ['conditions' => ['Storage.id' => $id, 'Storage.company_id' => MYCOMPANY]]);
        if ($this->Storage->hide($currentDeleted['Storage']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }

    /*	public function deletevariation($id = null,$storageId)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this,['Variation','Messages']);
            $asg =  ["la","variante ","F"];
            if($this->Variation->isHidden($id))
                throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

            $this->request->allowMethod(['post', 'delete']);
            $currentDeleted = $this->Variation->find('first',['conditions'=>['Variation.id'=>$id,'Variation.company_id'=>MYCOMPANY]]);
            if ($this->Variation->hide($currentDeleted['Variation']['id']))
                  $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
            else
               $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
            return $this->redirect(['action' => 'indexvariation',$storageId]);
        }*/


    public function deletenotmovable($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages', 'Messages']);
        $asg = ["la", "Descrizione voce fattura", "F"];
        if ($this->Storage->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Storage->find('first', ['conditions' => ['Storage.id' => $id, 'Storage.company_id' => MYCOMPANY]]);
        if ($this->Storage->hide($currentDeleted['Storage']['id']))
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
        else
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
        return $this->redirect(['action' => 'indexnotmovable']);
    }


    public function fileDownload($fileName, $fileType, $companyId)
    {
        $this->loadModel('Utilities');
        $this->autoRender = false;
        $path = $this->Utilities->DownloadFile($companyId, $fileType, $fileName);
        $this->response->file($path, ['download' => true, 'name' => $fileName]);
    }


    public function load($storageId)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages']);

        //$this->set('storages',$this->Utilities->getStoragesLeaf($storageId));

        if ($this->request->is('post') || $this->request->is('put')) {
            try {
                // Se abilitata la gestione avanzata di magazzino
                if (ADVANCED_STORAGE_ENABLED) {
                    isset($this->request->data['Storage']['variante']) && $this->request->data['Storage']['variante'] != '' ? $variationId = $this->request->data['Storage']['variante'] : $variationId = null;
                    $this->Utilities->storageLoad($storageId, $this->request->data['Storage']['quantity'], $this->request->data['Storage']['movementDescription'], $this->request->data['Storage']['last_buy_price'], $this->request->data['Storage']['deposit_id'], 1, 'MAN_LO', $variationId);
                } else {
                    $this->Utilities->storageLoad($storageId, $this->request->data['Storage']['quantity'], $this->request->data['Storage']['movementDescription']);
                }
                $this->redirect('index');
            } catch (Exception $ecc) {
                $this->Session->setFlash($ecc->getMessage(), 'custom-danger');
            }
        }

        if (ADVANCED_STORAGE_ENABLED) // Se abilitata gestione avanzata di magazzino
        {
            $this->set('depositArray', $this->Utilities->getDepositsList());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        }

        $this->set('selectedStorage', $this->Storages->find('first', ['conditions' => ['id' => $storageId, 'company_id' => MYCOMPANY]]));
    }

    public function unload($storageId)
    {

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages']);

        if ($this->request->is('post') || $this->request->is('put')) {
            try {
                if (ADVANCED_STORAGE_ENABLED) {
                    isset($this->request->data['Storage']['variante']) && $this->request->data['Storage']['variante'] != '' ? $variationId = $this->request->data['Storage']['variante'] : $variationId = null;
                    $this->Utilities->storageUnload($storageId, $this->request->data['Storage']['quantity'], $this->request->data['Storage']['movementDescription'], 0, $this->request->data['Storage']['deposit_id'], 1, 'MAN_UN', $variationId);
                } else {
                    $this->Utilities->storageUnload($storageId, $this->request->data['Storage']['quantity'], $this->request->data['Storage']['movementDescription']);
                }
                $this->redirect('index');
            } catch (Exception $ecc) {
                $this->Session->setFlash($ecc->getMessage(), 'custom-danger');
            }
        }

        if (ADVANCED_STORAGE_ENABLED) // Se abilitata gestione avanzata di magazzino
        {
            $this->set('depositArray', $this->Utilities->getDepositsList());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
            $this->set('maxStorageDefault', $this->Utilities->getAvailableQuantity($storageId, $this->Utilities->getDefaultDeposits()['Deposits']['id']));
        }

        $this->set('storageId', $storageId);

        $this->set('selectedStorage', $this->Storages->find('first', ['conditions' => ['id' => $storageId, 'company_id' => MYCOMPANY]]));
    }


    // Controlla che gli articoli di un array esistono
    public function checkIfStorageArticleExist()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $articleArray = [];
        foreach ($_POST['articles'] as $article) {
            if ($this->Utilities->checkIfStorageArticleExist($article, $_POST['clientId']) == false) {
                array_push($articleArray, $article);
            }
        }
        print(json_encode($articleArray));
        die;
    }


    public function getMaxDepositAvailability()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        // Nel caso non siano definite le varianti
        print(json_encode($this->Utilities->getAvailableQuantity($_POST['storageId'], $_POST['deposit'])));
    }

    public function  getStorageElements()
    {
        $this->loadModel('Utilities');

        $storage_id = $_POST['storage_id'];
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047")
            isset($_POST['client_id']) ? $client_id = $_POST['client_id'] : $client_id = null;
        $this->autoRender = false;
        $storage = $this->Storage->find('first', ['recursive'=>-1, 'conditions'=>['state'=>ATTIVO, 'id'=>$storage_id], 'fields'=>['descrizione', 'codice', 'prezzo', 'movable', 'unit_id', 'vat_id'], ]);

        if ($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            if ($client_id != null) {
                if(!is_numeric($client_id)){
                    $client_id = $this->Utilities->getClientIdFromClientName($client_id);
                }
                $queryTakePrice = "SELECT prezzo FROM order_rows INNER JOIN orders ON order_rows.order_id = orders.id WHERE order_rows.storage_id = ".$storage_id." AND orders.client_id = ".$client_id." ORDER BY orders.date DESC, orders.id DESC LIMIT 1";
                $result = $this->Storage->query($queryTakePrice);
                if(sizeof($result) > 0)
                    $storage['Storage']['prezzo'] = $result[0]['order_rows']['prezzo'];
            }
        }
        return json_encode($storage);
    }

    public function fixQuantity(){
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages']);
        $storages = $this->Storage->find('all');

        foreach($storages as $storage)
        {
            debug($storage['Storage']['id']);
            $this->Utilities->storageLoad($storage['Storage']['id'], $storage['Storage']['qta'], '');
        }
    }
}
