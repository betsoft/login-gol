<?php

App::uses('AppController', 'Controller');

class Constructionsitescontroller extends AppController
{
    public function index()
    {
        $this->loadModel('Utilities');

        if (MODULO_CANTIERI)
        {
            $this->Utilities->loadModels($this, ['Constructionsite']);
            $conditionsArray = ['Constructionsite.company_id' => MYCOMPANY, 'Constructionsite.state >' => 0];
            $filterableFields = ['Client__ragionesociale','name','description','address','city','Constructionsite__cap','province','#htmlElements[0]','#actions'];
            $sortableFields = [['Client.ragionesociale','Cliente'],['name','Cantiere'],['description','Descrizione'],['address','Indirizzo'],['city','Città'],['cap', 'Cap'],['province','Provincia'],['Constructionsite.state','Stato'],['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters']))
            {
                if(isset($this->request->data['filters']['state']) && $this->request->data['filters']['state'] != '')
                {
                    switch($this->request->data['filters']['state'])
                    {
                        case '1':
                            $conditionsArray['Constructionsite.state'] = 1;
                        break;
                        case '2':
                            $conditionsArray['Constructionsite.state'] = 2;
                        break;
                        case '3':
                            $conditionsArray['Constructionsite.state'] = 3;
                        break;
                    }
                }

                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            // Generazione XLS
            if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
            {
                $this->autoRender = false;
                $dataForXls = $this->Constructionsite->find('all', ['conditions' => $conditionsArray, 'order' => ['Client.ragionesociale', 'name', 'description']]);
                echo 'Cliente;Cantiere;Descrizione;Indirizzo;Città;Cap;Provincia;Stato;' . "\r\n";

                foreach ($dataForXls as $xlsRow)
                {
                    switch ($xlsRow['Constructionsite']['state'])
                    {
                        case '1':
                            $state = 'APERTO';
                        break;
                        case '2':
                            $state = 'CHIUSO';
                        break;
                        case '3':
                            $state = 'SOSPESO';
                        break;
                        default:
                            $state = '';
                        break;
                    }

                    echo $xlsRow['Client']['ragionesociale'] . ';' . $xlsRow['Constructionsite']['name'] . ';' . $xlsRow['Constructionsite']['description'] . ';' . $xlsRow['Constructionsite']['address'] . ';' . $xlsRow['Constructionsite']['city'] . ';' . $xlsRow['Constructionsite']['cap'] . ';' . $xlsRow['Constructionsite']['province'] . ';' . $state . ';' . "\r\n";
                }
            }
            else
            {
                $this->paginate = [
                    'contain' => ['Client'],
                    'recursive' => 2,
                    'conditions' => $conditionsArray,
                    'order' => ['Client.ragionesociale', 'name', 'description']
                ];

                $constructionsitesWithMaintenance = [];

                foreach($this->paginate() as $constructionsite)
                {
                    $maintenances = $this->Constructionsite->getConstructionsiteMaintenances($constructionsite['Constructionsite']['id']);

                    if(count($maintenances) > 0)
                        array_push($constructionsitesWithMaintenance, $constructionsite['Constructionsite']['id']);
                }

                $this->set('state',['1'=>'Aperto', '2'=>'Chiuso','3'=>'Sospeso']);
                $this->set('constructionsitesWithMaintenance', $constructionsitesWithMaintenance);
                $this->set('constructionsites', $this->paginate());
            }
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Constructionsites', 'Messages', 'Nation', 'Client']);

            $messageParameter = ["il", "cantiere", "M"];

            if ($this->request->is('post'))
            {
                $this->Constructionsites->create();
                $this->request->data['Constructionsites']['company_id'] = MYCOMPANY;

                if ($this->Constructionsites->save($this->request->data))
                {
                    $this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                }
                else
                {
                    $this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                }
            }

            $this->set('customers', $this->Client->getCompleteList());
            $this->set('nations', $this->Nation->getList());
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add_fast($redirect = 'index')
    {
        if (MODULO_CANTIERI)
        {
            $this->layout = 'voidMegaformLayout';
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Constructionsites', 'Messages', 'Nation', 'Client']);

            $messageParameter = ["il", "cantiere", "M"];

            if ($this->request->is('post'))
            {
                $this->Constructionsites->create();
                $this->request->data['Constructionsites']['company_id'] = MYCOMPANY;
                $this->request->data['Constructionsites']['state'] = 3;
                $this->request->data['Constructionsites']['client_id'] = $this->request->data['Constructionsites']['client_megaform_id2'];

                $newConstructionSite = $this->Constructionsites->save($this->request->data);

                if (!$newConstructionSite)
                    $this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');

                if($this->request->is('ajax'))
                {
                    $this->layout = false;
                    $dataArray = ['entityData' => $newConstructionSite['Constructionsites']];
                    print(json_encode($dataArray));
                    die;
                }
            }

            $this->set('customers', $this->Client->getCompleteList());
            $this->set('nations', $this->Nation->getList());
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Constructionsites', 'Messages', 'Nation', 'Client']);
            $messageParameter = ["il", "cantiere", "M"];
            $this->Constructionsites->id = $id;

            if (!$this->Constructionsites->exists())
                throw new NotFoundException(__('cantiere non valido'));

            if ($this->request->is('post') || $this->request->is('put'))
            {
                if ($this->Constructionsites->save($this->request->data))
                {
                    $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                }
                else
                {
                    $this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                }
            }
            else
            {
                $this->request->data = $this->Constructionsites->read(null, $id);
            }

            $this->set('customers', $this->Client->getCompleteList());
            $this->set('nations', $this->Nation->getList());
            $this->set('constructionsiteName', $this->Constructionsite->getName($id));
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function delete($id = null)
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Constructionsite']);

            $messageParameter = ["il", "Cantiere", "M"];

            if ($this->Constructionsite->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Constructionsite->find('first', ['conditions' => ['Constructionsite.id' => $id, 'Constructionsite.company_id' => MYCOMPANY]]);

            if ($this->Constructionsite->hide($currentDeleted['Constructionsite']['id']))
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');

            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function getAddress()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Constructionsite']);
        return json_encode($this->Constructionsite->getAddress($_POST['constructionsiteId']));
    }

    public function closeConstructionsite()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Constructionsite']);
        $this->Constructionsite->close($_POST['constructionsiteId']);
        return $_POST['constructionsiteId'];
    }

    public function openConstructionsite()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Constructionsite']);
        $this->Constructionsite->open($_POST['constructionsiteId']);
        return $_POST['constructionsiteId'];
    }

    public function constructionSiteDetail($id)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Units');
        $this->Utilities->loadModels($this, ['Constructionsite']);
        $this->set('constructionSiteMaintenance', $this->Constructionsite->getConstructionsiteMaintenances($id));
        $this->set('constructionSiteQuote', $this->Constructionsite->getConstructionsiteQuotes($id));
        $this->set('constructionSiteName',$this->Constructionsite->getName($id));
		$this->set('utilities', $this->Utilities);
        $this->set('units', $this->Units);
    }

    public function enable()
    {
        $this->autoRender = false;
        $this->Constructionsite->enable();
    }

	public function getClientId()
    {
        $this->autoRender = false;
        $this->loadModel('Constructionsite');
        return $this->Constructionsite->getClientId($_POST['constructionsiteId']);
    }
}
