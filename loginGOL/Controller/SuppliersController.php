<?php
App::uses('AppController', 'Controller');

class SuppliersController extends AppController {


	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Supplier','Csv']);
				
		$conditionsArray = [ 'Supplier.company_id' => MYCOMPANY ,'Supplier.state'=>ATTIVO];
		$filterableFields = ['Code','Supplier__name','indirizzo', 'cap', 'city', 'province','telefono','e-mail','piva','cf',null];
		$sortableFields = [['code','Codice'],['name','Ragione sociale'],['indirizzo','Indirizzo'],['cap', 'Cap'],['city', 'Città'],['province','Prov.'],['telefono','Telefono'],['e-mail','Email'],['piva', 'P.iva'],['cf','Codice fiscale'],['#actions']];								 
		
		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
			
			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession); 
		}
		
		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Supplier->find('all',['conditions'=>$conditionsArray,'order' => ['Supplier.name' => 'asc']]); 			
			echo 'Codice;Ragione sociale;Indirizzo;Cap;Città;Provincia;Telefono;Email;P.iva;Codice fiscale;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				echo $xlsRow['Supplier']['code']. ';'.$xlsRow['Supplier']['name']. ';' .$xlsRow['Supplier']['indirizzo']. ';'.$xlsRow['Supplier']['cap']. ';'.$xlsRow['Supplier']['city']. ';'.$xlsRow['Supplier']['province'].';'.$xlsRow['Supplier']['telefono'].';'.$xlsRow['Supplier']['e-mail'].';'.$xlsRow['Supplier']['piva'].';'.$xlsRow['Supplier']['cf'].';'.$xlsRow['Nation']['name'].';'."\r\n";
			}
		} // Fine Xls
		else
		{
			$this->Supplier->recursive = 0;
			$this->paginate = ['conditions' => $conditionsArray,'order'=>'Supplier.name asc', 'limit' => 100 ];
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->set('suppliers', $this->paginate());
		}
	}
	
	public function add() 
	{
		$this->loadModel('Utilities');
		if ($this->request->is('post')) {
			$this->Supplier->create();
			$this->request->data['Supplier']['company_id']=MYCOMPANY;
			
			// Fix per il model
			if($this->request->data['Supplier']['pec'] == '') { unset($this->request->data['Supplier']['pec']); }
			if($this->request->data['Supplier']['interchange_code'] == ''){ unset($this->request->data['Supplier']['interchange_code']); }


			if ($this->Supplier->save($this->request->data)) {
				$this->Session->setFlash(__('Fornitore salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('Il fornitore non è stato salvato'), 'custom-danger');
			}
		}
		
		$this->set('nations',$this->Utilities->getNationsList());
		$this->set('currencies', $this->Utilities->getCurrenciesList());
	}
	
	
	public function edit($id = null)
	{
		$this->Supplier->id = $id;
		$this->loadModel('Utilities');
		if (!$this->Supplier->exists())
		{
			throw new NotFoundException(__('Fornitore non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			
			// Fix per il model
			if($this->request->data['Supplier']['pec'] == '') { unset($this->request->data['Supplier']['pec']); }
			if($this->request->data['Supplier']['interchange_code'] == ''){ unset($this->request->data['Supplier']['interchange_code']); }
			
			if ($this->Supplier->save($this->request->data))
			{
				$this->Session->setFlash(__('Fornitore salvato correttamente'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Session->setFlash(__('Il fornitore non é stato salvato, riprovare.'), 'custom-danger');
			}
		}
		else
		{
			$this->request->data = $this->Supplier->read(null, $id);
		}
		
		$this->set('nations',$this->Utilities->getNationsList());
		$this->set('currencies', $this->Utilities->getCurrenciesList());
	}


	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Supplier','Messages']);
        $asg =  ["il","fornitore","M"];
		if($this->Supplier->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Supplier->find('first',['conditions'=>['Supplier.id'=>$id,'Supplier.company_id'=>MYCOMPANY]]);
        if ($this->Supplier->hide($currentDeleted['Supplier']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}

	// Controllo duplicato fattura
	public function checkSupplierPivaDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkSupplierPivaDuplicate($_POST['piva']);
		return json_encode($return);
	}
	
	// Controllo duplicato fattura
	public function checkSupplierCfDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkSupplierCfDuplicate($_POST['cf']);
		return json_encode($return);
	}

}
