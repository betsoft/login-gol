<?php

App::uses('AppController', 'Controller');

class SettingsController extends AppController
{
   public function index()
   {
        $username = $_SESSION['Auth']['User']['username'];
        $paypalid = $this->payPal($username);
        if($paypalid != null) {
            $this->set('idPayPal', $paypalid);
            $userTokenPaypal = base64_encode("ASPVANFT_zaumM_9DQ102cW4FEqVmarJSgs3aYxc9UcVQRDifRHGnjDhmOJyu9FZL91HniXzLV25ChnD"."EPFThaGMkm738ewfomBJP3yB4EGNrI1X9ZNOwXt17K1_pTWRAsFIpMUTGMTOcp61Be4al-2E6CO0VluN");
        }
        $this->loadModel('Utilities');
    	$this->Utilities->loadModels($this,['Setting','Einvoicewithholdingtaxcausal']);
    	$this->Utilities->loadModels($this,['Setting','Einvoicewelfarebox']);

	    $settings = $this->Setting->getMySettings();

	    if(isset($settings['Setting']['withholding_tax_causal_id']))
	    {
	    	$invoiceWithholdiengTax =  $this->Einvoicewithholdingtaxcausal->find('first',['conditions'=>['id' => $settings['Setting']['withholding_tax_causal_id']]])['Einvoicewithholdingtaxcausal'];
	    	$this->set('invoiceWithholdingTax', $invoiceWithholdiengTax['code']. ' - '. $invoiceWithholdiengTax['description']);
	    }
	    else
	    {
	    	$this->set('invoiceWithholdingTax', '');
	    }

		// Se è settata una cassa previdenziale
		if(isset($settings['Setting']['welfare_box_percentage']))
		{
			$welfareBox =  $this->Einvoicewelfarebox->find('first',['conditions'=>['code'=>$settings['Setting']['welfare_box_code']]])['Einvoicewelfarebox'];
			$this->set('welfareBox', $welfareBox['code']. ' - '. $welfareBox['description']);
		}
		else
		{
			$this->set('welfareBox', '');
		}

		// Casse di previdenza
		if($settings['Setting']['welfare_box_vat_id'] > 0) // Se è settata una cassa di previdenza
		{
			$this->set('welfareBoxVatPercentage', $this->Utilities->getVatPercentageFromId($settings['Setting']['welfare_box_vat_id']));
		}

       /*if($settings['Setting']['tokenIXFE'] != -1 && $settings['Setting']['tokenIXFE'] != null)
       {
           $this->set('token', 1);
       }
       else{
           $this->set('token', 1);
       }*/

    	$this->set('settings',  $settings );
   }

    public function payPal($username)
    {
        $connectionDB = mysqli_connect('localhost', MAINUSER, MAINPASSWORD);
        mysqli_select_db($connectionDB, MAINDB);
        $queryPayPal = "SELECT PayPal FROM users WHERE username = '$username'";
        $paypalID = mysqli_query($connectionDB, $queryPayPal);
        $paypal = mysqli_fetch_object($paypalID);
        $paypalArray = json_decode(json_encode($paypal), true);
        $paypalID = $paypalArray['PayPal'];
        return $paypalID;
    }

    public function edit($id = null)
    {
        $oldSettings = $this->Setting->find('first', array('conditions' => array('Setting.id' => $id)));

        $this->loadModel('Utilities');
        $this->set('einvoiceFiscalRegimes', $this->Utilities->getEinvoiceFiscalRegimesList());
        $this->set('einvoiceWitholdingTaxCausals', $this->Utilities->getEinvoiceWithholdingTaxCausals());
        $this->set('einvoiceTypeOfPerson', $this->Utilities->getEinvoiceTypeOfPerson());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('welfareBoxes', $this->Utilities->getEinvoiceWelfareBox());
        $this->set('shortCodeList', $this->Utilities->getNationShortcodeList());

        $this->set('setting', $this->Setting->read(null, $id));
        $this->Setting->id = $id;

        if (!$this->Setting->exists())
            throw new NotFoundException(__('Impostazione non valida'));

        if ($this->request->is('post') || $this->request->is('put'))
        {
            if (!empty($this->request->data['Setting']['password'])) // Controllo che sia settato password
            {
                if ($this->request->data['Setting']['password'] != '') // Controllo che sia stata inserita una password
                {
                    if ($this->request->data['Setting']['password'] != $oldSettings['Setting']['password']) // Controllo che la password non sia identica a quella precedente (carico il valore cryptato)
                    {
                        $this->request->data['Setting']['password'] = $this->Utilities->mycrypt($this->request->data['Setting']['password'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');
                    }
                }
            }

            if (isset($this->request->data['Setting']['start_nighttime']))
                $this->request->data['Setting']['maintenance_hour_from'] = date("H:i:s", strtotime($this->request->data['Setting']['start_nighttime']));

            if (isset($this->request->data['Setting']['start_nighttime']))
                $this->request->data['Setting']['maintenance_hour_to'] = date("H:i:s", strtotime($this->request->data['Setting']['end_nighttime']));

            // Se modificata la password
            if (isset($this->request->data['Setting']['passwordIXFE']) && $this->request->data['Setting']['passwordIXFE'] != '' && $this->request->data['Setting']['passwordIXFE'] != $oldSettings['Setting']['passwordIXFE'])
                $this->request->data['Setting']['passwordIXFE'] = $this->Utilities->mycrypt($this->request->data['Setting']['passwordIXFE'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');
            else
                $this->request->data['Setting']['passwordIXFE'] = $oldSettings['Setting']['passwordIXFE'];

            // Se modificata la password
            $PaypalKey = '$PPAL@cryptVladh$$2018';
            $PaypalIv = 'live12388@22@32!';

            if (isset($this->request->data['Setting']['paypal_account']) && $this->request->data['Setting']['paypal_account'] != '' && $this->request->data['Setting']['paypal_account'] != $oldSettings['Setting']['paypal_account'])
                $this->request->data['Setting']['paypal_account'] = $this->Utilities->mycrypt($this->request->data['Setting']['paypal_account'], 'aes-256-cbc', $PaypalKey, 0, $PaypalIv);
            else
                $this->request->data['Setting']['paypal_account'] = $oldSettings['Setting']['paypal_account'];

            if (isset($this->request->data['Setting']['paypal_password']) && $this->request->data['Setting']['paypal_password'] != '' && $this->request->data['Setting']['paypal_account'] != $oldSettings['Setting']['paypal_password'])
                $this->request->data['Setting']['paypal_password'] = $this->Utilities->mycrypt($this->request->data['Setting']['paypal_password'], 'aes-256-cbc', $PaypalKey, 0, $PaypalIv);
            else
                $this->request->data['Setting']['paypal_password'] = $oldSettings['Setting']['paypal_password'];

            if (isset($this->request->data['Setting']['paypal_user_id']) && $this->request->data['Setting']['paypal_user_id'] != '' && $this->request->data['Setting']['paypal_account'] != $oldSettings['Setting']['paypal_user_id'])
                $this->request->data['Setting']['paypal_user_id'] = $this->Utilities->mycrypt($this->request->data['Setting']['paypal_user_id'], 'aes-256-cbc', $PaypalKey, 0, $PaypalIv);
            else
                $this->request->data['Setting']['paypal_user_id'] = $oldSettings['Setting']['paypal_user_id'];

            if (isset($this->request->data['Setting']['paypal_user_password']) && $this->request->data['Setting']['paypal_user_password'] != '' && $this->request->data['Setting']['paypal_account'] != $oldSettings['Setting']['paypal_user_password'])
                $this->request->data['Setting']['paypal_user_password'] = $this->Utilities->mycrypt($this->request->data['Setting']['paypal_user_password'], 'aes-256-cbc', $PaypalKey, 0, $PaypalIv);
            else
                $this->request->data['Setting']['paypal_user_password'] = $oldSettings['Setting']['paypal_user_password'];

            if (isset($this->request->data['Setting']['paypal_user_signature']) && $this->request->data['Setting']['paypal_user_signature'] != '' && $this->request->data['Setting']['paypal_account'] != $oldSettings['Setting']['paypal_user_signature'])
                $this->request->data['Setting']['paypal_user_signature'] = $this->Utilities->mycrypt($this->request->data['Setting']['paypal_user_signature'], 'aes-256-cbc', $PaypalKey, 0, $PaypalIv);
            else
                $this->request->data['Setting']['paypal_user_signature'] = $oldSettings['Setting']['paypal_user_signature'];

            if (!empty($this->request->data['Setting']['header']['name']))
            {
                $date = new DateTime();
                $date = $date->getTimestamp();
                $image = $date . '_' . $this->request->data['Setting']['header']['name'];
                $final_path = APP . DS . 'webroot' . DS . 'img' . DS . 'societa' . DS . $image;
                move_uploaded_file($this->request->data['Setting']['header']['tmp_name'], $final_path);
                $this->request->data['Setting']['header'] = $image;
            }
            else
            {
                $this->request->data['Setting']['header'] = $oldSettings['Setting']['header'];
            }

            if (!empty($this->request->data['Setting']['bill_subscript']['name']))
            {
                $dateS = new DateTime();
                $dateS = $dateS->getTimestamp();
                $imageS = $dateS . '_' . $this->request->data['Setting']['bill_subscript']['name'];

                $final_pathS = APP . DS . 'webroot' . DS . 'img' . DS . 'societa' . DS . $imageS;
                move_uploaded_file($this->request->data['Setting']['bill_subscript']['tmp_name'], $final_pathS);
                $this->request->data['Setting']['bill_subscript'] = $imageS;
            }
            else
            {
                $this->request->data['Setting']['bill_subscript'] = $oldSettings['Setting']['bill_subscript'];
            }

            if ($this->Setting->save($this->request->data))
            {
                $this->Session->setFlash(__('Impostazioni salvate'), 'custom-flash');
                $this->redirect(['controller' => 'settings', 'action' => 'index']);
            }
            else
            {
                $this->Session->setFlash(__('Impostazioni non salvate'), 'custom-danger');
            }
        }
        else
        {
            $this->request->data = $this->Setting->read(null, $id);
        }
    }

	public function pdfsettings()
	{
		$this->loadModel('Utilities');
		$this->loadModel('Setting');
		$this->set('Setting',  $this->Setting->GetMySettings());
		$this->Render('pdfsettings');
	}

	public function removeCompanySettingLogo()
	{
		$this->autoRender = false;
		$this->Setting->updateAll(['header'=>"''"],['company_id'=>MYCOMPANY]);
	}

	public function removeCompanyBillSubscript()
	{
		$this->autoRender = false;
		$this->Setting->updateAll(['bill_subscript'=>"''"],['company_id'=>MYCOMPANY]);
	}

	public function changeCurrentCompany()
	{

		$this->loadModel('Utilities');
		$this->loadModel('Setting');
		if($this->request->query != null)
		{
			$this->autoRender = false;
			$_SESSION['MYCOMPANY'] = $this->request->query[0]; // Chiama company_id
			$currentSetting = $this->Setting->find('first',['conditions'=>['company_id'=>	$_SESSION['MYCOMPANY']]]);
			$_SESSION['companyName'] = $currentSetting['Setting']['name'];
			$this->request->query[1] == 'users' ? $controller = 'settings' : $controller = $this->request->query[1];
			$this->redirect(['controller'=>$controller,'action'=>$this->request->query[2]]);
		}
	}
}
