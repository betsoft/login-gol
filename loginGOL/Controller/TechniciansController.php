<?php
App::uses('AppController', 'Controller');

class TechniciansController extends AppController
{
    public function index()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->loadModel('Technician');

            $conditionsArray = ['Technician.company_id' => MYCOMPANY, 'state' => ATTIVO];
            $filterableFields = ['name', 'surname', 'email', null];
            $sortableFields = [['name', 'Nome'], ['surname', 'Cognome'], ['email', 'E-mail'], ['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            // Generazione XLS
            if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
                $this->autoRender = false;
                $dataForXls = $this->Technician->find('all', ['conditions' => $conditionsArray, 'order' => ['Technician.name' => 'asc']]);
                echo 'Tecnici' . "\r\n";
                foreach ($dataForXls as $xlsRow) {
                    echo $xlsRow['Technician']['name'] . ';' . $xlsRow['Technician']['surname'] . ';' . "\r\n";
                }
            } else {
                $this->paginate = ['conditions' => $conditionsArray];
                $this->set('technicians', $this->paginate());

            }
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technicians', 'Messages']);
            $messageParameter = ["il", "tecnico", "M"];
            if ($this->request->is('post')) {
              
                /** Salvo firma tecnico */
                if($this->request->data['Technicians']['sign']['tmp_name'])
                {
                    $path = $this->request->data['Technicians']['sign']['tmp_name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);
                    $this->request->data['Technicians']['sign'] =   base64_encode($data);
                }
                /** fine salvataggio firma tecnico */

                $this->Technicians->create();
                $this->request->data['Technicians']['company_id'] = MYCOMPANY;
                if($this->request->data['Technicians']['sign']['tmp_name'] == ''){ $this->request->data['Technicians']['sign'] = null;}
                if ($this->Technicians->save($this->request->data)) {
                    //$this->Session->setFlash(__('Tecnico salvato'), 'custom-flash');
                    $this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    //$this->Session->setFlash(__('Il tecnico è stato salvato'), 'custom-danger');
                    $this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                    //                    $this->Message->successOfAdd($messageParameter))
                }
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {
        IF (MODULO_CANTIERI) {

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technicians', 'Messages']);
            $messageParameter = ["il", "tecnico", "M"];
            $this->Technicians->id = $id;

            $oldTechnician = $this->Technicians->find('first', ['conditions' => ['Technicians.id' => $id]]);

            if (!$this->Technicians->exists()) {
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
            }

            if ($this->request->is('post') || $this->request->is('put')) {
                /** Salvo firma tecnico */
                if (!empty($this->request->data['Technicians']['sign']['tmp_name']) && $this->request->data['Technicians']['sign']['tmp_name'] != '') {
                        $path = $this->request->data['Technicians']['sign']['tmp_name'];
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($path);
                        $this->request->data['Technicians']['sign'] = base64_encode($data);

                    }
                else {
                    $this->request->data['Technicians']['sign'] = $oldTechnician['Technicians']['sign'];
                }
                /** fine salvataggio firma tecnico */



                if ($this->Technicians->save($this->request->data)) {
                    $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                }
            } else {
                $this->request->data = $this->Technicians->read(null, $id);
                $this->set('technician',$this->request->data);
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function delete($id = null)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Technician']);

            $messageParameter = ["il", "tecnico", "M"];
            if ($this->Technician->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Technician->find('first', ['conditions' => ['Technician.id' => $id, 'Technician.company_id' => MYCOMPANY]]);
            if ($this->Technician->hide($currentDeleted['Technician']['id']))
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
            return $this->redirect(['action' => 'index']);
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function techniciandetails($id = null)
    {
        if (MODULO_CANTIERI)
        {
            if($id != null)
            {
                $_SESSION['TechnicianId'] = $id;
            }

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technician']);

            $conditionsArray = ['Maintenance.state' => ATTIVO, 'Maintenance.maintenance_date >=' => date("Y-m") . '-01', 'Maintenance.maintenance_date <=' => date("Y") . '-12-31'];

            $filterableFields = ['maintenance_number', '#htmlElements[0]', 'intervention_description', null, null, null, null, null, null];
            $sortableFields = [[null, 'Numero scheda'], [null, 'Data scheda'], [null, 'Descrizione intervento'], [null, 'Ora inizio'], [null, 'Ora fine'], [null, 'Totale'], [null, 'Weekend'], [null, 'Notturno'], [null, 'Cantiere']];

            // Filtri automatici
            $automaticFilter = $this->Session->read('arrayOfFilters');
            if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
            {
                $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
                $startDate = $this->request->data['filters']['date1'];
                $endDate = $this->request->data['filters']['date2'];
            }
            else
            {
                //null;
                $startDate = '01'.date("-m-Y");
                $endDate = '31-12-'.date("Y");
            }

            $this->set('startDate', $startDate);
            $this->set('endDate', $endDate);

            if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
            {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                    $conditionsArray['Maintenance.maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                    $conditionsArray['Maintenance.maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

                $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
                $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
                $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
            }

            $this->set('TechnicianName', $this->Technician->getSurnameAndName($_SESSION['TechnicianId']));
            $this->set('technicianHours', $this->Technician->getWorkedHour($_SESSION['TechnicianId'], $conditionsArray));
            $this->set('technicinaId', $_SESSION['TechnicianId']);
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);
            $this->set('utilities', $this->Utilities);
        }
    }
}