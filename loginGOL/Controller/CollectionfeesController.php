<?php
App::uses('AppController', 'Controller');


class  CollectionfeesController extends AppController {

	public function index() 
	{
		$this->paginate = ['conditions' => [ ' Collectionfee.company_id' => MYCOMPANY ]];
		$this->set('collectionfee', $this->paginate());
	}

	
	public function checkInterval($start,$end,$id = -1)
	{
		
		if($end < $start)
		{
			$this->Session->setFlash(__('L\' importo finale dell\'intervallo deve essere maggiore dell\' importo iniziale'), 'custom-danger');
			return false;
		}
		
		$totale = $totaleGlobale = 0;

		$conditionArray =  ['OR' => ['AND'=> ['from_amount <=' => $start, 'to_amount >='=>$end,'id <>'=>$id],'AND'=>['from_amount <=' => $end,'to_amount >='=>$start,'id <>'=>$id]]];
		$totale = $this->Collectionfee->find('count', ['conditions'=> $conditionArray]);

		if($totale == 0)		
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function add() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Collectionfee']);

		if ($this->request->is('post')) 
		{
			$this-> Collectionfee->create();
				$this->request->data['Collectionfee']['company_id']= MYCOMPANY;
			
				// Controllo che l'importo a sia maggiore dell'importo da
				$correctInterval = $this->checkInterval($this->request->data['Collectionfee']['from_amount'],$this->request->data['Collectionfee']['to_amount'],MYCOMPANY);

				if($correctInterval)
				{
					if ($this->Collectionfee->save($this->request->data)) 
					{
						$this->Session->setFlash(__('Intervallo spese d\'incasso salvato'), 'custom-flash');
						$this->redirect(['action' => 'index']);
					}
					else 
					{
						$this->Session->setFlash(__('L\' Intervallo spese d\'incasso non è stato salvato'), 'custom-danger');
					}
				}
				else // errore intervallo
				{
						$this->Session->setFlash(__('Attenzione ! L\'intervallo inserito non è corretto. L\' intervallo non deve essere contenuto all\'interno degli altri intervalli' ), 'custom-danger');
				}
		}
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Collectionfee']);
		$this->Collectionfee->id = $id;
			if (!$this->Collectionfee->exists()) {
				throw new NotFoundException(__('Intervallo spese d\'incasso non valido'));
			}
			if ($this->request->is('post') || $this->request->is('put')) 
			{
				$correctInterval = $this->checkInterval($this->request->data['Collectionfee']['from_amount'],$this->request->data['Collectionfee']['to_amount'],MYCOMPANY,$id);

				if($correctInterval)
				{
					if ($this->Collectionfee->save($this->request->data)) 
					{
						$this->Session->setFlash(__('Intervallo spese d\'incasso'), 'custom-flash');
						$this->redirect(['action' => 'index']);
					} else 
					{
						$this->Session->setFlash(__('Intervallo spese d\'incasso non salvato, riprovare'), 'custom-danger');
					}
				}
			}
			else {
			$this->request->data = $this->Collectionfee->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Collectionfee']);
		if (!$this->request->is('post')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->Collectionfee->id = $id;
		if (!$this->Collectionfee->exists()) {
			throw new NotFoundException(__('Intervallo spese d\'incasso non valido'));
		}
		if ($this->Collectionfee->delete($id, false)) {
			$this->Session->setFlash(__('Intervallo spese d\'incasso eliminato'), 'custom-flash');
			$this->redirect(['action' => 'index']);
		}
		$this->Session->setFlash(__('L\'intervallo delle spese d\'incasso non è stato eliminato'), 'custom-danger');
		$this->redirect(['action' => 'index']);
	}
}
