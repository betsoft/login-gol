<?php
App::uses('AppController', 'Controller');

class ExchangesController extends AppController {

	public function index($id) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Exchange','Currencies','Csv']);
		
		// $this->set('currencyName',$this->Currencies->GetCurrencyName($id));
		$this->set('currencyId',$id);
		
		$conditionsArray =['Exchange.company_id' => MYCOMPANY, 'currency_id'=>$id ];
		$filterableFields = [];
		$sortableFields = [['start_date','Data'],[null,'1€ = '.$this->Currencies->GetCurrencyName($id)],['#actions']];								 

		if($this->request->is('ajax') && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		}
		
			// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			/* $this->autoRender = false;
			$dataForXls = $this->Exchange->find('all',['conditions'=>$conditionsArray,'order' => ['Exchange.description' => 'asc']]); 			
			echo 'Valuta;Simbolo;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				echo $xlsRow['Exchange']['description']. ';' .$xlsRow['Exchange']['symbol']. ';'."\r\n";
			}*/
		}
		else
		{
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
		}

		
		$this->paginate = ['conditions' => $conditionsArray];
		$this->set('exchanges', $this->paginate());
	}

	public function add($id) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Exchanges','Currencies']);
		$datasource = $this->Exchanges->getDataSource();
		try 
		{
			$datasource->begin();
			if ($this->request->is('post')) 
			{
				$this->Exchanges->create();
				$this->request->data['Exchanges']['company_id']=MYCOMPANY;

				if (!$this->Exchanges->save($this->request->data)) 
				{
					throw new Exception('Errore durante la creazione della nuova valuta.');
				}
				else
				{
					$this->Session->setFlash(__('Valuta salvata'), 'custom-flash');
					$datasource->commit();
					$this->redirect(['controller'=>'currencies','action' => 'index']);
				}
			}
		} 
		catch(Exception $e) 
		{
    		$datasource->rollback();
    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
		}
		
		$this->set('currencyName',$this->Currencies->getCurrencyName($id));
		$this->set('currencyId',$id);
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Exchanges','Currencies']);

		$this->Exchanges->id = $id;
		if (!$this->Exchanges->exists()) 
		{
			throw new NotFoundException(__('Cambio non valido'));
		}
		
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			$datasource = $this->Exchanges->getDataSource();
			try 
			{
				$datasource->begin();
				if (!$this->Exchanges->save($this->request->data)) 
				{
						throw new Exception('Errore durante la modifica del cambio.');
				}
				else
				{
					$this->Session->setFlash(__('Cambio salvato'), 'custom-flash');
					$datasource->commit();
					$this->redirect(['controller'=>'currencies','action' => 'index']);
				}
			} 
			catch(Exception $e) 
			{
	    		$datasource->rollback();
	    		$this->Session->setFlash(__($e->getMessage()), 'custom-danger');
			}
		}
		else 
		{
			$this->request->data = $this->Exchanges->read(null, $id);
		}
		
		$this->set('currencyName',$this->Currencies->getCurrencyName($this->request->data['Exchanges']['currency_id']));
	}

	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Exchange','Messages']);
        $asg =  ["il","cambio","M"];
		if($this->Exchange->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Exchange->find('first',['conditions'=>['Exchange.id'=>$id,'Exchange.company_id'=>MYCOMPANY]]);
        if ($this->Exchange->hide($currentDeleted['Exchange']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}
}
