<?php
App::uses('AppController', 'Controller');

class ConfigurationsController extends AppController
{
   public function index()
   {
   		$this->loadModel('Utilities');
   		$this->Utilities->loadModels($this,['Configurations']);
   		if(ADMIN_CONFIGURATION)
   		{
			$myConfigurationArray = ['company_id'=>MYCOMPANY,'used'=>1];
	        $configurations = $this->Configurations->find('all',['conditions'=>$myConfigurationArray]);
		    $this->set('configurations',$configurations);
		    $this->render('index');
   		}
   		else
   		{
   			throw new NotFoundException(__('Pagina inesistente'));
   		}
   }


	public function changestate($id)
	{
   		$this->loadModel('Utilities');
   		$this->Utilities->loadModels($this,['Configurations']);

		if(ADMIN_CONFIGURATION)
		{
			$configuration = $this->Configurations->find('first',['conditions'=>['company_id'=>MYCOMPANY,'id'=>$id]]);
			
			if($configuration['Configurations']['settingvalue'] == 0)
			{ 
			
				// Recupero tutte le voci di menu e le abilito
				if($configuration['Configurations']['menuvoices'] != '')
				{
					$menuvoices = explode("|",$configuration['Configurations']['menuvoices']);
					foreach($menuvoices as $menuvoice)
					{
						$this->Utilities->enableMenuVoice($menuvoice);
						$this->Utilities->enablePermissions($menuvoice);
					}
				}
				
				$configuration['Configurations']['settingvalue'] = 1;
			} 
			else
			{
				
				// $configuration['Configurations']['menuvoices'] != '' ?  $this->Utilities->disableMenuVoice($configuration['Configurations']['menuvoices']) : '';
				if($configuration['Configurations']['menuvoices'] != '')
				{
					$menuvoices = explode("|",$configuration['Configurations']['menuvoices']);
					foreach($menuvoices as $menuvoice)
					{
						$this->Utilities->disableMenuVoice($menuvoice);
						$this->Utilities->disablePermissions($menuvoice);					
					}
				}
				
				$configuration['Configurations']['settingvalue'] = 0;
			}
		
			$this->Configurations->save($configuration);
			$this->redirect(['action' => 'index']);
		}
		else
		{
			throw new NotFoundException(__('Pagina inesistente'));
		}
	}

}
