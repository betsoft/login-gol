<?php
App::uses('AppController', 'Controller');
class FidelitiesController extends AppController
{
    public function index(){
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities','Fidelitypoints']);

        $conditionsArray = ['Fidelity.state' => ATTIVO];
        $filterableFields = ['fidelity_id', 'client_name', 'note','punti',null];
        $sortableFields = [['fidelity_id','Codice Fidelities'],['client_name','Cliente'],['note','Note'],['punti','Totale punti'],['#actions']];

        if($this->request->is('ajax') && isset ($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        $this->set('filterableFields', $filterableFields);
        $this->set('sortableFields', $sortableFields);

        $this->set('utilities', $this->Utilities);
        $this->paginate = ['conditions' => $conditionsArray];
        $this->set('fidelity', $this->paginate());



    }

    public function add()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities','Messages','Client']);
        $messageArray = ["Fidelities Card"];
        if ($this->request->is('post')) {
            $fidelity = $this->Fidelity->create();
            $fidelity['Fidelity']['state_id'] = ATTIVO;
            $cliente = $this->Client->find('first', ['conditions' => ['ragionesociale' => $this->request->data['Fidelity']['client_id'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['id', 'ragionesociale']]);
            $fidelity['Fidelity']['client_name'] = $cliente['Client']['ragionesociale'];
            $fidelity['Fidelity']['client_id'] = $cliente['Client']['id']; //$this->request->data['Fidelity']['client_id']
            $fidelity['Fidelity']['note'] = $this->request->data['Fidelity']['note'];

            if ($this->Fidelity->save($fidelity)) {
                $this->Session->setFlash(__($this->Messages->successOfAdd($messageArray[0])), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__($this->Messages->failOfAdd($messageArray[0])), 'custom-flash');
            }
        }

        $this->set('clients', $this->Client->getAdvancedList());

    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities', 'Messages', 'Client']);
        $messageArray = ["Fidelity Card"];
        $this->Fidelity->id = $id;
        if (!$this->Fidelity->exists()) {
            throw new Exception($this->Messages->notFound($messageArray[0]));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Fidelity']['client_name']]]);


            if (isset($cliente['Client']['id'])) {
                $this->request->data['Fidelity']['client_id'] = $cliente['Client']['id'];
                $this->request->data['Fidelity']['client_name'] = $cliente['Client']['ragionesociale'];
            }

            if($this->Fidelity->save($this->request->data)) {
                $this->Session->setFlash(__('Fidelity salvata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('Questa Fidelity non è stata salvata'),'custom-flash');
            }
        } else {
            $this->request->data = $this->Fidelity->read(null, $id);
        }

        $this->loadModel('Client');
        $this->set('clients', $this->Client->getAdvancedList());
    }

    public function delete($id = null) {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities','Messages' ]);

        $messageArray = ["Fidelities Card"];
        if($this->Fidelity->isHidden($id))
            throw new Exception($this->Messages->notFound($messageArray[0]));

        $this->request->allowMethod(['post','delete']);

        $currentDeleted = $this->Fidelity->find('first', ['conditions' => ['Fidelity.id' => $id]]);
        if ($this->Fidelity->hide($currentDeleted['Fidelity']['id']))
            $this->Session->setFlash(__('Fidelity eliminata correttamente'), 'custom-flash');
        else
            $this->Session->setFlash(__('Errore nell eliminazione della fidelity' ), 'custom-danger');
        return $this->redirect(['action' => 'index']);
    }

    public function load($id){
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities', 'Settings', 'Client', 'FidelityPoints']);
        $conditionsArray = ['Fidelity.state' => ATTIVO];
        //$conditionsArray = $this->Utilities->buildConditions($conditionsArray);
        $mySettings = $this->Setting->GetMySettings();
        $this->set('euro_punti', $mySettings['Setting']['euro_punti']);


        $fidelityPoints = $this->FidelityPoints->find('first', ['conditions' => ['fidelity_id' => $id, 'decal' => 1], 'order' => ['date' => 'asc']]);

        if($fidelityPoints != null){
            $date1 = date_create(date('Y-m-d'));
            $date2 = date_create($fidelityPoints['FidelityPoints']['date']);
            $diff=date_diff($date1,$date2);
            if($diff->days >= 90)
            {
                $this->Session->setFlash("L'ultima decalcificazione è stata fatta più di tre mesi fa", 'custom-danger');
            }
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            try{
                $this->FidelityPoints = ClassRegistry::init('FidelityPoints');
                $fidelityPoints = $this->FidelityPoints->Create();
                $fidelityPoints['FidelityPoints']['punti'] = $this->request->data['FidelityPoints']['punti'];
                $fidelityPoints['FidelityPoints']['importo'] = $this->request->data['FidelityPoints']['importo'];
                $fidelityPoints['FidelityPoints']['decal'] = $this->request->data['FidelityPoints']['decal'];
                $fidelityPoints['FidelityPoints']['note'] = $this->request->data['FidelityPoints']['note'];
                $fidelityPoints['FidelityPoints']['date'] = date("Y/m/d");
                $fidelityPoints['FidelityPoints']['movement_type'] = 0;
                $fidelityPoints['FidelityPoints']['fidelity_id'] = $id;
                $this->FidelityPoints->save($fidelityPoints);
                $this->redirect('index');

            } catch (Exception $ecc) {
                $this->Session->setFlash($ecc->getMessage(), 'custom-danger');
            }
        }



    }

    public function unload($id){
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities', 'FidelityPoints']);
        $mySettings = $this->Setting->GetMySettings();
        $this->set('euro_punti', $mySettings['Setting']['euro_punti']);

        $this->set('utilities', $this->Utilities);

        $fidelity = $this->Fidelity->find('first', ['conditions' => ['Fidelity.id' => $id]]);
        $this->set('fidelity', $fidelity);

        $fidelityPoints = $this->FidelityPoints->find('first', ['conditions' => ['fidelity_id' => $id, 'decal' => 1], 'order' => ['date' => 'asc']]);

        if($fidelityPoints != null){
            $date1 = date_create(date('Y-m-d'));
            $date2 = date_create($fidelityPoints['FidelityPoints']['date']);
            $diff=date_diff($date1,$date2);
            if($diff->days >= 90)
            {
                $this->Session->setFlash("L'ultima decalcificazione è stata fatta più di tre mesi fa", 'custom-danger');
            }
        }

        if($this->request->is('post') || $this->request->is('put')) {
            try {
                $this->FidelityPoints = ClassRegistry::init('FidelityPoints');
                $fidelityPoints = $this->FidelityPoints->Create();
                $fidelityPoints['FidelityPoints']['punti'] = $this->request->data['FidelityPoints']['punti'] * (-1);
                $fidelityPoints['FidelityPoints']['decal'] = $this->request->data['FidelityPoints']['decal'];
                $fidelityPoints['FidelityPoints']['note'] = $this->request->data['FidelityPoints']['note'];
                $fidelityPoints['FidelityPoints']['date'] = date("Y/m/d");
                $fidelityPoints['FidelityPoints']['movement_type'] = 1;
                $fidelityPoints['FidelityPoints']['fidelity_id'] = $id;
                $this->FidelityPoints->save($fidelityPoints);
                $this->redirect('index');
            } catch (Exception $ecc) {
                $this->Session->setFlash($ecc->getMessage(),'custom-danger');
            }
        }
    }


    public function qrCode($id, $save = false) {
        $this->autoRender = false;
        $url = "https://login.gestionale-online.net/fidelities/qrCodeRedirect/".$id;
        $filename = 'qr_fidelity'.$id.'.png';
        $size = 500;
        $url = preg_match("#^https?\:\/\/#", $url) ? $url : "http://{$url}";
        $googleChartAPI = 'https://chart.apis.google.com/chart';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $googleChartAPI,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => "chs={$size}x{$size}&cht=qr&chl=". urlencode($url),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));
        $img = curl_exec($curl);

        curl_close($curl);


        if($img) {
            if($filename){
                if($save){
                    if(!preg_match("#\.png$#i", $filename)) {
                        $filename .= ".png";
                    }

                    file_put_contents(APP . 'tmpQRCode/'.$filename, $img);
                    return $filename;
                }
                if(!preg_match("#\.png$#i", $filename)) {
                    $filename .= ".png";
                }

                file_put_contents($filename, $img);
                $this->stmpimg($id);
            } else {
                print $img;
            }
        }
    }

    public function stmpimg($id) {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities']);
        $fidelity = $this->Fidelity->create();
        $fidelity['Fidelity']['state_id'] = ATTIVO;
        $fidelity['Fidelity']['client_name'] = $id;
        $file = 'qr_fidelity'.$id.'.png';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        //ob_clean();
        //flush();
        readfile($file);
    }

    public function qrCodeRedirect($id){
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Fidelities']);

        $this->autoRender = false;

        try {
            $fidelity = $this->Fidelity->find('first',['conditions' => ['Fidelity.id' => $id]]);
            if($fidelity['Fidelity']['client_id'] == null){
                $this->redirect(['action'=>'edit', $id]);
            } else {
                $this->redirect(['action'=>'chooseLoadUnload', $id]);
            }
        } catch (Exception $ecc) {
            $this->Session->setFlash($ecc->getMessage(),'custom-danger');
        }
    }

    public function chooseLoadUnload($id){
        $this->loadModel('Utilities');
        $this->set('id', $id);
    }

    public function generateFidelityEmpty() {

        if($this->request->is('post')) {
            $arrayOfFiles = array();
            $zipname = APP . 'tmpQRCode/QRFidelity.zip';
            for($i = 0; $i < $_POST['num']; $i++){

                $this->Fidelity->create();
                $fidelity = $this->Fidelity->save();
                array_push($arrayOfFiles, $this->qrCode($fidelity['Fidelity']['id'], $save = true));
            }

            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);

            foreach ($arrayOfFiles as $file) {
                $zip->addFile(APP . 'tmpQRCode/' . $file, $file);
            }

            $zip->close();

            if ($i > 0) {
                foreach ($arrayOfFiles as $file) {
                    try {
                        unlink(APP . 'tmpQRCode/' . $file);
                    } catch (Exception $exception) {
                    }
                }

                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename=QrCOdes.zip');

                readfile($zipname);
                unlink($zipname);
            }
        }
    }

}
