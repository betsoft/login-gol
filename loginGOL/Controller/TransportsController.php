<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'mpdf/mpdf');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

class TransportsController extends AppController
{
    public $components = array('Mpdf');
    var $uses = array('Transport','Quote','Bill','Client','Transportgood','Iva','Setting', 'Storage','User');

    public function index($filterableFields = [])
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Transport','Csv']);

        $conditionsArray = ['Transport.company_id' => MYCOMPANY,'Transport.state <>' => 0, 'Transport.date >= ' => date("Y-m") . '-01', 'Transport.date <= ' => date("Y") . '-12-31'];
        $conditionsArray2 = ['1 = 1'];
        $conditionsArray3 = ['1 = 1'];

        $filterableFields = ['transport_number','#htmlElements[0]',null,'ragionesociale','descrizioni',null,null,null,null];
        $sortableFields = [['transport_number','N° bolla'],['Transport.date','Data'],[null,'Causale'],['ragionesociale','Cliente'],[null,'Descrizione articoli'],[null,'Quantità'],[null, 'Prezzo u'],[null,'Fattura'],['#actions']];

        $automaticFilter = $this->Session->read('arrayOfFilters');
        if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
        {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
            $startDate = $this->request->data['filters']['date1'];
            $endDate = $this->request->data['filters']['date2'];
        }
        else
        {
            $startDate = '01'.date("-m-Y");
            $endDate = '31-12-'.date("Y");
        }

        $this->set('startDate', $startDate);
        $this->set('endDate', $endDate);

        if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            $conditionsArray = ['Transport.company_id' => MYCOMPANY,'Transport.state <>' => 0];

            if(isset($this->request->data['filters']['transport_number']) && ( $this->request->data['filters']['transport_number'] != '' ))
                $conditionsArray['Transport.transport_number like'] = '%'. $this->request->data['filters']['transport_number']. '%';

            if(isset($this->request->data['filters']['descrizioni']) && $this->request->data['filters']['descrizioni'] != '')
            {
                $conditionsArray2 = [
                    'OR' => [
                        'Transportgood.oggetto like' => '%'. $this->request->data['filters']['descrizioni']. '%' ,
                        'Transportgood.customdescription like' => '%'. $this->request->data['filters']['descrizioni']. '%' ]
                ];
            }

            if(isset($this->request->data['filters']['ragionesociale']) && ($this->request->data['filters']['ragionesociale'] != ''))
            {
                $conditionsArray3 = [
                    'OR' => [
                        'Transport.client_name like' => '%'. $this->request->data['filters']['ragionesociale']. '%',
                        'Transport.supplier_name like' => '%'. $this->request->data['filters']['ragionesociale']. '%'
                    ]
                ];
            }

            if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['Transport.date >='] = date('Y-m-d',strtotime($this->request->data['filters']['date1']));

            if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['Transport.date <='] =  date('Y-m-d',strtotime($this->request->data['filters']['date2']));

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters',$arrayFilterableForSession);
        }

        $conditionsArray = array_merge($conditionsArray, $conditionsArray3);

        // Se attiva la personalizzazione delle stampe
        $Transport = $this->Transport->find('all',[
            'contain' => [
                'Client',
                'Quote',
                'Transportgood' => [
                    'conditions' => $conditionsArray2,
                    'fields' => ['Transportgood.id','Transportgood.quantita','Transportgood.prezzo','Transportgood.iva_id','Transportgood.oggetto','Transportgood.customdescription'],
                    'Iva'
                ],
                'Bill'
            ],
            'conditions' =>	$conditionsArray ,
            'order' => ['Transport.date' => 'desc','Transport.id' => 'desc'],
        ]);

        $arrayTransport = [];
        foreach($Transport as $key =>  $B)
        {
            count($B['Transportgood']) > 0 ? $arrayTransport[] = $B['Transport']['id'] : null;
        }

        if($arrayTransport != null)
            $conditionsArray['Transport.id IN'] = $arrayTransport;
        else
            $conditionsArray['Transport.id'] = -1;

        // Generazione XLS
        if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
        {
            $this->autoRender = false;
            $dataForXls = $this->Transport->find('all',['conditions' => $conditionsArray,'order' => ['Transport.date' => 'desc','Transport.transport_number' => 'desc']]);

            echo 'N° Bolla;Data;Cliente;Fattura(Numero/Anno)'."\r\n";

            foreach ($dataForXls as $xlsRow)
            {
                isset($xlsRow['Bill']['numero_fattura']) ? $dataFattura = $xlsRow['Bill']['numero_fattura'] . ' / '.date('Y',strtotime($xlsRow['Bill']['date'])) : $dataFattura = '';
                $xlsRow['Transport']['causal_id'] != 3 && $xlsRow['Transport']['causal_id'] != 7 ? $holderName = $xlsRow['Transport']['client_name'] : $holderName = $xlsRow['Transport']['supplier_name'];

                echo $xlsRow['Transport']['transport_number'] . SCSV . date('d-m-Y', strtotime($xlsRow['Transport']['date'])). SCSV . $holderName . SCSV . $dataFattura . SCSV . "\r\n";
            }
        }
        else
        {
            $this->paginate = [
                'contain' => ['Client', 'Quote', 'Transportgood' => ['fields' => ['Transportgood.id','Transportgood.quantita','Transportgood.prezzo','Transportgood.iva_id','Transportgood.oggetto','Transportgood.customdescription'], 'Iva' ], 'Bill'],
                'conditions' =>	$conditionsArray ,
                'order' => ['Transport.date' => 'desc','Transport.id' => 'desc'],
                'limit' => 20,
            ];

            $this->set('filterableFields',$filterableFields);
            $this->set('sortableFields',$sortableFields);

            $this->Transport->recursive = 0;
            $this->set('transports', $this->paginate());

            $this->render('index');
        }
    }

    // DDT ESTESA
    public function add()
    {
        $this->loadModel('Client');
        $this->loadModel('Supplier');
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Storage','Clientdestination','Supplier','Supplierdestination','Currencies','Causal','Units']);

        $defaultSectional = $this->Utilities->getDefaultTransportSectional();
        $this->set('defaultSectional',$defaultSectional);

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $this->set('transportNumber', $nextSectionalNumber);
        $this->set('sectionals', $this->Utilities->getTransportSectionalList());

        if ($this->request->is('post'))
        {
            // Definisco causale
            if($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F') // Se non è una bolla di reso
            {
                /* Prendo l'ID del cliente per il salvataggio dei dati, se il cliente esiste ovviamente */
                $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Transport']['client_id'], 'Client.state'=>ATTIVO, 'Client.company_id'=> MYCOMPANY], 'fields' => ['Client.id']]);

                // Se il cliente non esiste
                if(empty($cliente['Client']['id']))
                {
                    $clientAddress = $this->request->data['Transport']['client_address'];
                    $clientCap = $this->request->data['Transport']['client_cap'];
                    $clientCity = $this->request->data['Transport']['client_city'];
                    $clientProvince = $this->request->data['Transport']['client_province'];
                    isset($this->request->data['Transport']['client_nation']) ? $clientNation = $this->request->data['Transport']['client_nation'] :  $clientNation = null;
                    // Setto il nome utente
                    $this->request->data['Transport']['client_name'] = $this->request->data['Transport']['client_id']; // In realta client_id è la ragsociale
                    // Poi client id diventa id
                    $this->request->data['Transport']['client_id'] = $this->Utilities->createClient($this->request->data['Transport']['client_id'],$clientAddress,$clientCap,$clientCity,$clientProvince,$clientNation);
                    // La definizione della currencies (default EUR)
                    $this->request->data['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');
                    $this->request->data['Bill']['changevalue'] = 1;
                }
                else
                {
                    // Setto il nome utente
                    $this->request->data['Transport']['client_name'] = $this->request->data['Transport']['client_id']; // In realta client_id è la ragsociale
                    // Definisco il client id
                    $this->request->data['Transport']['client_id'] = $cliente['Client']['id'];
                }
            }
            else
            {
                /* Prendo l'ID del fornitore per il salvataggio dei dati, se il fornitore esiste ovviamente */
                $fornitore = $this->Supplier->find('first', ['conditions' => ['Supplier.name' => $this->request->data['Transport']['supplier_id'], 'Supplier.state' => ATTIVO], 'fields' => ['Supplier.id']]);

                // Se il fornitore non esiste
                if(empty($fornitore['Supplier']['id']))
                {
                    $supplierAddress = $this->request->data['Transport']['supplier_address'];
                    $supplierCap = $this->request->data['Transport']['supplier_cap'];
                    $supplierCity = $this->request->data['Transport']['supplier_city'];
                    $supplierProvince = $this->request->data['Transport']['supplier_province'];
                    isset($this->request->data['Transport']['supplier_nation']) ? $csupplierNation = $this->request->data['Transport']['supplier_nation'] :  $supplierNation = null;
                    // Setto il nome utente
                    $this->request->data['Transport']['supplier_name'] = $this->request->data['Transport']['supplier_id']; // In realta supplier_id è la ragsociale
                    // Poi supplier_id diventa id
                    $this->request->data['Transport']['supplier_id'] = $this->Utilities->createClient($this->request->data['Transport']['supplier_id'],$supplierAddress,$supplierCap,$supplierCity,$supplierProvince,$supplierNation);
                }
                else
                {
                    // Setto il nome utente
                    $this->request->data['Transport']['supplier_name'] = $this->request->data['Transport']['supplier_id']; // In realta client_id è la ragsociale
                    // Definisco il supplier_id
                    $this->request->data['Transport']['supplier_id'] = $fornitore['Supplier']['id'];
                }
            }

            $this->request->data['Transport']['company_id'] = MYCOMPANY;

            $this->Transport->create();
            $time = strtotime($this->request->data['Transport']['date']);
            $this->request->data['Transport']['date'] = date("Y-m-d",$time);

            if(isset($this->request->data['Transport']['transport_date']))
            {
                if($this->request->data['Transport']['transport_date'] != null)
                {
                    $transportTime=strtotime($this->request->data['Transport']['transport_date']);
                    $this->request->data['Transport']['transport_date']=date("Y-m-d",$transportTime);
                }
            }

            // Se è una bolla verso cliente
            if($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F')
            {
                // Se è selezionato alternative address lo salvo come destino
                if(isset($this->request->data['Transport']['alternativeaddress_id']))
                {
                    $shippingAddress = $this->Clientdestination->find('first',['conditions'=>['Clientdestination.id' => $this->request->data['Transport']['alternativeaddress_id']]]);

                    if(count($shippingAddress)>0)
                    {
                        $this->request->data['Transport']['client_shipping_name'] = $shippingAddress['Clientdestination']['name'];
                        $this->request->data['Transport']['client_shipping_address'] = $shippingAddress['Clientdestination']['address'];
                        $this->request->data['Transport']['client_shipping_cap'] = $shippingAddress['Clientdestination']['cap'];
                        $this->request->data['Transport']['client_shipping_city'] = $shippingAddress['Clientdestination']['city'];
                        $this->request->data['Transport']['client_shipping_province'] = $shippingAddress['Clientdestination']['province'];

                        if($shippingAddress['Clientdestination']['nation_id'] != null)
                            $this->request->data['Transport']['client_shipping_nation'] = $this->Utilities->getNationName($shippingAddress['Clientdestination']['nation_id'])['Nation']['name'];
                    }
                }
            }
            else
            {
                if(isset($this->request->data['Transport']['supplier_alternativeaddress_id']))
                {
                    $supplierAddress = $this->Supplierdestination->find('first',['conditions'=>['Supplierdestination.id' => $this->request->data['Transport']['supplier_alternativeaddress_id']]]);

                    if(count($supplierAddress)>0)
                    {
                        $this->request->data['Transport']['supplier_shipping_name'] = $supplierAddress['Supplierdestination']['name'];
                        $this->request->data['Transport']['supplier_shipping_address'] = $supplierAddress['Supplierdestination']['address'];
                        $this->request->data['Transport']['supplier_shipping_cap'] = $supplierAddress['Supplierdestination']['cap'];
                        $this->request->data['Transport']['supplier_shipping_city'] = $supplierAddress['Supplierdestination']['city'];
                        $this->request->data['Transport']['supplier_shipping_province'] = $supplierAddress['Supplierdestination']['province'];

                        if($supplierAddress['Supplierdestination']['nation_id'] != null)
                            $this->request->data['Transport']['supplier_shipping_nation'] = $this->Utilities->getNationName($supplierAddress['Supplierdestination']['nation_id'])['Nation']['name'];
                    }
                }
            }

            $numberOfMovement = 0;

            if ($newTransport =$this->Transport->save($this->request->data))
            {
                foreach($this->request->data['Transportgood'] as $key => $transportgood)
                {
                    $store = $this->Storage->find('first', ['conditions' => [ 'Storage.id' => $transportgood['storage_id']]]);

                    $newGood = $this->Transportgood->Create();
                    $transportgood['Transportgood']['transport_id'] = $newTransport['Transport']['id'];
                    $transportgood['Transportgood']['codice'] = $transportgood['codice'];
                    $transportgood['Transportgood']['oggetto'] = $transportgood['oggetto'];
                    $transportgood['Transportgood']['prezzo'] =  $transportgood['prezzo'];
                    $transportgood['Transportgood']['quantita'] = $transportgood['quantita'];
                    $transportgood['Transportgood']['iva_id'] =  $transportgood['iva_id'];
                    $transportgood['Transportgood']['discount'] =  $transportgood['discount'];
                    isset($transportgood['customdescription']) ?  $transportgood['Transportgood']['customdescription'] =  $transportgood['customdescription'] :  $transportgood['Transportgood']['customdescription'] = '';
                    isset($transportgood['unita']) ?  $transportgood['Transportgood']['unita'] = $transportgood['unita'] :  $transportgood['Transportgood']['unita'] = '';
                    $transportgood['Transportgood']['movable'] = $transportgood['movable'];
                    $transportgood['Transportgood']['company_id'] = MYCOMPANY;
                    $newBillGood = $this->Transportgood->save($transportgood['Transportgood']);

                    // Se l'articolo esiste già a magazzino
                    if($transportgood['storage_id'] != '')
                    {
                        $numberOfMovement++;

                        $storageId = $transportgood['storage_id'];

                        // Se gestione avanzata di magazzino
                        if(ADVANCED_STORAGE_ENABLED)
                        {
                            $this->Utilities->storageUnload($storageId,$transportgood['quantita'],'Scarico bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),$this->Utilities->getLastBuyPrice($storageId),$newTransport['Transport']['deposit_id'],$numberOfMovement, 'TR_AD');
                        }
                        else
                        {
                            $this->Utilities->storageUnload($storageId,$transportgood['quantita'],'Scarico bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),0,-1,$numberOfMovement, 'TR_AD');
                        }

                        // Aggiorno lo storage_id sul good appena creato
                        $this->Transportgood->updateAll(['Transportgood.storage_id'=> $transportgood['storage_id'],'Transportgood.company_id'  => MYCOMPANY],['Transportgood.id'=>$newBillGood['Transportgood']['id']]);
                    }
                    else
                    {
                        // Se è una nota non controllo l'esistenza dell'articolo in magazzino
                        if(!$this->Utilities->isANote($transportgood['Transportgood'],'transport'))
                        {
                            // Controllo che esista a magazzino l'articolo
                            if($this->Utilities->notExistInStorage($transportgood['storage_id']))
                            {
                                // Fattura a clienti sono in questo caso lo creo
                                if($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F')  // Se non è  una bolla di reso
                                {
                                    $newStorage = $this->Utilities->createNewStorageFromGood($transportgood,$newTransport['Transport']['client_id']);


                                    // Aggiorno lo storage_id sul good
                                    $transportgood['Transportgood']['storage_id'] = $newStorage['Storage']['id'];
                                    $this->Transportgood->updateAll(['Transportgood.storage_id'=>$newStorage['Storage']['id']],['Transportgood.id'=>$newBillGood['Transportgood']['id']]);

                                    // Faccio carico e scarivo virtuale di magazzino
                                    if(ADVANCED_STORAGE_ENABLED)
                                    {
                                        if(isset($newStorage['Storage']['id']))
                                        {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId,$transportgood['quantita'],'Carico (oggetto nuovo di magazzino) bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),$this->Utilities->getLastBuyPrice($theStorageId),$newTransport['Transport']['deposit_id'],$numberOfMovement, 'TR_AD');
                                            $this->Utilities->storageUnload($theStorageId,$transportgood['quantita'],'Scarico (oggetto nuovo di magazzino) bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),$this->Utilities->getLastBuyPrice($theStorageId),$newTransport['Transport']['deposit_id'],$numberOfMovement, 'TR_AD');
                                        }
                                    }
                                    else
                                    {
                                        if(isset($newStorage['Storage']['id']))
                                        {
                                            $theStorageId = $newStorage['Storage']['id'];
                                            $this->Utilities->storageLoad($theStorageId,$transportgood['quantita'],'Carico (oggetto nuovo di magazzino) bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),0,-1,$numberOfMovement, 'TR_AD');
                                            $this->Utilities->storageUnload($theStorageId,$transportgood['quantita'],'Scarico (oggetto nuovo di magazzino) bolla n.'.$newTransport['Transport']['transport_number'] . ' del '. date('d-m-Y',strtotime($newTransport['Transport']['date'])),0,-1,$numberOfMovement, 'TR_AD');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $this->Session->setFlash(__('La bolla è stata salvata'), 'custom-flash');
                $this->Utilities->updateTransportSectional($newTransport['Transport']['id']);
                $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Session->setFlash(__('Non è stato possibile salvare la bolla, riprovare'), 'custom-danger');
            }
        }

        $this->set('clients',$this->Client->getAdvancedList());
        $this->set('suppliers',$this->Supplier->getList());
        $this->set('causali',$this->Utilities->getCausals());
        $this->set('units',$this->Units->getList());
        $this->set('carriers',$this->Utilities->getCarriersList());
        $this->set('nations',$this->Utilities->getNationsList());
        $this->set('vats',$this->Utilities->getVatsList());

        if(ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini',$this->Utilities->getStoragesWithVariations());
            $this->set('deposits', $this->Utilities->getDepositsList());
            $this->set('mainDeposit',$this->Utilities->getDefaultDeposits());
        }
        else
        {
            $this->set('magazzini',$this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->render('add');
    }

    public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storages','Transportgood','Clientdestination','Supplier','Supplierdestination','Currencies','Causal','Units', 'Sectional']);

        !empty($this->params['pass'][0]) ? $this->Transport->id = $this->params['pass'][0] : $this->Transport->id = $id;

        $defaultSectional = $this->Utilities->getDefaultTransportSectional();
        $this->set('defaultSectional',$defaultSectional);

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $this->set('transportNumber', $nextSectionalNumber);
        $this->set('sectionals', $this->Utilities->getTransportSectionalList());
        if (!$this->Transport->exists())
            throw new NotFoundException(__('Bolla non valida'));

        if ($this->request->is('post') || $this->request->is('put'))
        {
            $time = strtotime($this->request->data['Transport']['date']);
            $this->request->data['Transport']['date'] = date("Y-m-d", $time);

            if (isset($this->request->data['Transport']['transport_date']))
            {
                if ($this->request->data['Transport']['transport_date'] != null)
                {
                    $transportTime = strtotime($this->request->data['Transport']['transport_date']);
                    $this->request->data['Transport']['transport_date'] = date("Y-m-d", $transportTime);
                }
            }

            // Recupero i vecchi articoli
            $oldGood = $this->Transportgood->find('all', ['conditions' => ['Transportgood.company_id' => MYCOMPANY, 'Transportgood.transport_id' => $id]]);

            if ($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F')
            {
                // Se è selezionato alternative address lo salvo come destino
                if (isset($this->request->data['Transport']['alternativeaddress_id']))
                {
                    $shippingAddress = $this->Clientdestination->find('first', ['conditions' => ['Clientdestination.id' => $this->request->data['Transport']['alternativeaddress_id']]]);

                    if (count($shippingAddress) > 0)
                    {
                        $this->request->data['Transport']['client_shipping_name'] = $shippingAddress['Clientdestination']['name'];
                        $this->request->data['Transport']['client_shipping_address'] = $shippingAddress['Clientdestination']['address'];
                        $this->request->data['Transport']['client_shipping_cap'] = $shippingAddress['Clientdestination']['cap'];
                        $this->request->data['Transport']['client_shipping_city'] = $shippingAddress['Clientdestination']['city'];
                        $this->request->data['Transport']['client_shipping_province'] = $shippingAddress['Clientdestination']['province'];

                        if($shippingAddress['Clientdestination']['nation_id'] != null)
                        {
                            $this->request->data['Transport']['client_shipping_nation'] = $this->Utilities->getNationName($shippingAddress['Clientdestination']['nation_id'])['Nation']['name'];
                        }
                    }
                    else
                    {
                        $this->request->data['Transport']['client_shipping_name'] = null;
                        $this->request->data['Transport']['client_shipping_address'] = null;
                        $this->request->data['Transport']['client_shipping_cap'] = null;
                        $this->request->data['Transport']['client_shipping_city'] = null;
                        $this->request->data['Transport']['client_shipping_province'] = null;
                        $this->request->data['Transport']['client_shipping_nation'] = null;
                        $this->request->data['Transport']['alternativeaddress_id'] = null;
                    }
                }
                else
                {
                    $this->request->data['Transport']['client_shipping_name'] = null;
                    $this->request->data['Transport']['client_shipping_address'] = null;
                    $this->request->data['Transport']['client_shipping_cap'] = null;
                    $this->request->data['Transport']['client_shipping_city'] = null;
                    $this->request->data['Transport']['client_shipping_province'] = null;
                    $this->request->data['Transport']['client_shipping_nation'] = null;
                    $this->request->data['Transport']['alternativeaddress_id'] = null;
                }
            }

            // Se non è  una bolla verso fornitore
            if ($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) == 'F')
            {
                // Se è selezionato alternative address lo salvo come destino
                if (isset($this->request->data['Transport']['supplier_alternativeaddress_id']))
                {
                    $suppliershippingAddress = $this->Supplierdestination->find('first', ['conditions' => ['Supplierdestination.id' => $this->request->data['Transport']['supplier_alternativeaddress_id']]]);

                    if (count($suppliershippingAddress) > 0)
                    {
                        $this->request->data['Transport']['supplier_shipping_name'] = $suppliershippingAddress['Supplierdestination']['name'];
                        $this->request->data['Transport']['supplier_shipping_address'] = $suppliershippingAddress['Supplierdestination']['address'];
                        $this->request->data['Transport']['supplier_shipping_cap'] = $suppliershippingAddress['Supplierdestination']['cap'];
                        $this->request->data['Transport']['supplier_shipping_city'] = $suppliershippingAddress['Supplierdestination']['city'];
                        $this->request->data['Transport']['supplier_shipping_province'] = $suppliershippingAddress['Supplierdestination']['province'];

                        if($suppliershippingAddress['Supplierdestination']['nation_id'] != null)
                        {
                            $this->request->data['Transport']['supplier_shipping_nation'] = $this->Utilities->getNationName($suppliershippingAddress['Supplierdestination']['nation_id'])['Nation']['name'];
                        }
                    }
                    else
                    {
                        $this->request->data['Transport']['supplier_shipping_name'] = null;
                        $this->request->data['Transport']['supplier_shipping_address'] = null;
                        $this->request->data['Transport']['supplier_shipping_cap'] = null;
                        $this->request->data['Transport']['supplier_shipping_city'] = null;
                        $this->request->data['Transport']['supplier_shipping_province'] = null;
                        $this->request->data['Transport']['supplier_shipping_nation'] = null;
                        $this->request->data['Transport']['supplier_alternativeaddress_id'] = null;
                    }
                }
                else
                {
                    $this->request->data['Transport']['supplier_shipping_name'] = null;
                    $this->request->data['Transport']['supplier_shipping_address'] = null;
                    $this->request->data['Transport']['supplier_shipping_cap'] = null;
                    $this->request->data['Transport']['supplier_shipping_city'] = null;
                    $this->request->data['Transport']['supplier_shipping_province'] = null;
                    $this->request->data['Transport']['supplier_shipping_nation'] = null;
                    $this->request->data['Transport']['supplier_alternativeaddress_id'] = null;
                }
            }

            /** CAMBIAMENTO CLIENTE **/

            if ($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F')
            {
                /* Prendo l'ID del cliente per il salvataggio dei dati, se il cliente esiste ovviamente */
                $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Transport']['client_name'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id']]);

                // Se il cliente non esiste
                if (empty($cliente['Client']['id']))
                {
                    $clientAddress = $this->request->data['Transport']['client_address'];
                    $clientCap = $this->request->data['Transport']['client_cap'];
                    $clientCity = $this->request->data['Transport']['client_city'];
                    $clientProvince = $this->request->data['Transport']['client_province'];
                    isset($this->request->data['Transport']['client_nation']) ? $clientNation = $this->request->data['Transport']['client_nation'] : $clientNation = null;

                    // Poi client id diventa id
                    $this->request->data['Transport']['client_id'] = $this->Utilities->createClient($this->request->data['Transport']['client_name'], $clientAddress, $clientCap, $clientCity, $clientProvince, $clientNation);

                    // La definizione della currencies (default EUR)
                    $this->request->data['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');
                    $this->request->data['Bill']['changevalue'] = 1;
                }
                else
                {
                    // Definisco il client id
                    $this->request->data['Transport']['client_id'] = $cliente['Client']['id'];
                }
            }
            else
            {
                /* Prendo l'ID del fornitore per il salvataggio dei dati, se il fornitore esiste ovviamente */
                $fornitore = $this->Supplier->find('first', ['conditions' => ['Supplier.name' => $this->request->data['Transport']['supplier_name'], 'Supplier.state' => ATTIVO], 'fields' => ['Supplier.id']]);

                // Se il fornitore non esiste
                if(empty($fornitore['Supplier']['id']))
                {
                    $supplierAddress = $this->request->data['Transport']['supplier_address'];
                    $supplierCap = $this->request->data['Transport']['supplier_cap'];
                    $supplierCity = $this->request->data['Transport']['supplier_city'];
                    $supplierProvince = $this->request->data['Transport']['supplier_province'];
                    isset($this->request->data['Transport']['supplier_nation']) ? $csupplierNation = $this->request->data['Transport']['supplier_nation'] :  $supplierNation = null;

                    // Poi supplier_id diventa id
                    $this->request->data['Transport']['supplier_id'] = $this->Utilities->createClient($this->request->data['Transport']['supplier_name'], $supplierAddress, $supplierCap, $supplierCity, $supplierProvince, $supplierNation);
                }
                else
                {
                    // Definisco il supplier_id
                    $this->request->data['Transport']['supplier_id'] = $fornitore['Supplier']['id'];
                }
            }

            $oldTransport = $this->Transport->find('first', ['conditions' => ['Transport.id' => $id]]);

            // Se la bolla viene salvata
            if ($newTransport = $this->Transport->save($this->request->data))
            {
                $defaultSectional = $this->Utilities->getDefaultTransportSectional();

                if($oldTransport['Transport']['transport_number'] == $defaultSectional['Sectionals']['last_number'] && $oldTransport['Transport']['sectional_id'] == $defaultSectional['Sectionals']['id'])
                    $this->Sectional->updateAll(['last_number' => $newTransport['Transport']['transport_number']], ['company_id' => MYCOMPANY, 'default_transport' => 1]);

                // Per ogni articolo vecchio scarico da magazzino
                $numberOfMovement = 0;

                foreach ($oldGood as $oldTransportGood)
                {
                    $numberOfMovement++;
                    if ($oldTransportGood['Transportgood']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                    {
                        if (ADVANCED_STORAGE_ENABLED)
                        {
                            $this->Utilities->storageLoad($oldTransportGood['Transportgood']['storage_id'], $oldTransportGood['Transportgood']['quantita'], 'Carico bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($oldTransportGood['Transportgood']['storage_id']), $this->request->data['Transport']['deposit_id'], $numberOfMovement, 'TR_ED');
                        }
                        else
                        {
                            $this->Utilities->storageLoad($oldTransportGood['Transportgood']['storage_id'], $oldTransportGood['Transportgood']['quantita'], 'Carico bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])) . ' per modifica ', 0, -1, $numberOfMovement, 'TR_ED');
                        }
                    }

                    $this->Transportgood->deleteAll(['Transportgood.id' => $oldTransportGood['Transportgood']['id']]);
                }

                foreach ($this->request->data['Transportgood'] as $key => $transportgood)
                {
                    $numberOfMovement++;

                    // Salvo il nuovo articolo collegandolo alla fattura
                    $store = $this->Storage->find('first', ['conditions' => ['Storage.id' => $transportgood['storage_id']]]);
                    $newGood = $this->Transportgood->Create();
                    $transportgood['Transportgood']['transport_id'] = $this->request->data['Transport']['id'];
                    $transportgood['Transportgood']['codice'] = $transportgood['codice'];
                    $transportgood['Transportgood']['oggetto'] = $transportgood['oggetto'];
                    $transportgood['Transportgood']['prezzo'] = $transportgood['prezzo'];
                    $transportgood['Transportgood']['quantita'] = $transportgood['quantita'];
                    $transportgood['Transportgood']['iva_id'] = $transportgood['iva_id'];
                    $transportgood['Transportgood']['discount'] = $transportgood['discount'];
                    isset($transportgood['customdescription']) ? $transportgood['Transportgood']['customdescription'] = $transportgood['customdescription'] : $transportgood['Transportgood']['customdescription'] = '';
                    isset($transportgood['unita']) ? $transportgood['Transportgood']['unita'] = $transportgood['unita'] : $transportgood['Transportgood']['unita'] = '';  // Perché questo non è nullable a db
                    $transportgood['Transportgood']['company_id'] = MYCOMPANY;
                    $newBillGood = $this->Transportgood->save($transportgood['Transportgood']);

                    // Se l'articolo esiste già a magazzino
                    if ($transportgood['storage_id'] != '')
                    {
                        $storageId = $transportgood['storage_id'];
                        // Scarico quantità a magazzino
                        try
                        {
                            if (ADVANCED_STORAGE_ENABLED)
                            {
                                $this->Utilities->storageUnload($storageId, $transportgood['quantita'], 'Scarico bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])) . ' per modifica.', $this->Utilities->getLastBuyPrice($storageId), $this->request->data['Transport']['deposit_id'], $numberOfMovement, 'TR_ED');
                            }
                            else
                            {
                                $this->Utilities->storageUnload($storageId, $transportgood['quantita'], 'Scarico bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])) . ' per modifica.', 0, -1, $numberOfMovement, 'TR_ED');
                            }
                        }
                        catch (Exception $ecc)
                        {
                            $this->Session->setFlash($ecc->getMessage(), 'custom-danger');
                        }

                        // Aggiorno lo storage_id sul good appena creato
                        $this->Transportgood->updateAll(['Transportgood.storage_id' => $transportgood['storage_id']], ['Transportgood.id' => $newBillGood['Transportgood']['id']]);
                    }
                    else
                    {
                        // Se è una nota non controllo l'esistenza dell'articolo in magazzino
                        if (!$this->Utilities->isANote($transportgood, 'transport'))
                        {
                            // Faccio carico e scarico viruale
                            if ($this->Utilities->notExistInStorage($transportgood['storage_id']))
                            {
                                if(!isset($newTransport['Transport']['client_id']))
                                    $newTransport['Transport']['client_id'] = null;

                                if(!isset($newTransport['Transport']['supplier_id']))
                                    $newTransport['Transport']['supplier_id'] = null;

                                // Se è settato il salvataggio automatico degli articoli di magazzino
                                $newStorage = $this->Utilities->createNewStorageFromGood($transportgood, $newTransport['Transport']['client_id'], $newTransport['Transport']['supplier_id']);
                                // Aggiorno lo storage_id sul good
                                $transportgood['Transportgood']['storage_id'] = $newStorage['Storage']['id'];
                                $this->Transportgood->updateAll(['Transportgood.storage_id' => $newStorage['Storage']['id']], ['Transportgood.id' => $newBillGood['Transportgood']['id']]);

                                // Faccio carico e scarivo virtuale di magazzino
                                try
                                {
                                    if (ADVANCED_STORAGE_ENABLED)
                                    {
                                        $this->Utilities->storageLoad($newStorage['Storage']['id'], $transportgood['quantita'], 'Carico (oggetto nuovo di magazzino) bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])), $this->Utilities->getLastBuyPrice($newStorage['Storage']['id']), $this->request->data['Transport']['deposit_id'], $numberOfMovement, 'TR_ED');
                                        $this->Utilities->storageUnload($newStorage['Storage']['id'], $transportgood['quantita'], 'Scarico (oggetto nuovo di magazzino) bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])), $this->Utilities->getLastBuyPrice($newStorage['Storage']['id']), $this->request->data['Transport']['deposit_id'], $numberOfMovement, 'TR_ED');
                                    }
                                    else
                                    {
                                        $this->Utilities->storageLoad($newStorage['Storage']['id'], $transportgood['quantita'], 'Carico (oggetto nuovo di magazzino) bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])), 0, -1, $numberOfMovement, 'TR_ED');
                                        $this->Utilities->storageUnload($newStorage['Storage']['id'], $transportgood['quantita'], 'Scarico (oggetto nuovo di magazzino) bolla n.' . $this->request->data['Transport']['transport_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Transport']['date'])), 0, -1, $numberOfMovement, 'TR_ED');
                                    }
                                }
                                catch (Exception $ecc)
                                {
                                    $this->Session->setFlash($ecc->getMessage(), 'custom-danger');
                                }
                            }
                        }
                    }
                }

                $this->Session->setFlash(__('Bolla salvata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            }

            $this->Session->setFlash(__('La bolla non è stato salvato correttamente.'), 'custom-danger');
        }
        else
        {
            $this->request->data = $this->Transport->find('first', ['contain' => ['Client', 'Transportgood' => ['Iva'], 'Deposit'], 'conditions' => ['Transport.id' => $id]]);
        }

        $this->loadModel('Client');
        $this->loadModel('Supplier');

        $this->set('clients',$this->Client->getAdvancedList());
        $this->set('suppliers',$this->Supplier->getList());

        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        }
        else
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $transport = $this->Transport->find('first', ['contain' => ['Client', 'Transportgood' => 'Storage'], 'conditions' => ['Transport.id' => $id, 'Transport.company_id' => MYCOMPANY]]);
        $this->set('transport', $transport);

        $arrayOfAlternativeAddress = [];

        if ($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) != 'F')
        {
            $alternativeAddresses = $this->Utilities->getCustomerDifferentAddress($transport['Transport']['client_id']);

            foreach ($alternativeAddresses as $alternativeAddress)
            {
                isset($alternativeAddress['Clientdestination']['nation_id']) ? $nationName = $this->Utilities->getNationName($alternativeAddress['Clientdestination']['nation_id'])['Nation']['name'] : $nationName = '';
                $arrayOfAlternativeAddress[$alternativeAddress['Clientdestination']['id']] = $alternativeAddress['Clientdestination']['name'] . ' - ' . $alternativeAddress['Clientdestination']['address'] . ' - ' . $alternativeAddress['Clientdestination']['cap'] . ' - ' . $alternativeAddress['Clientdestination']['city'] . ' - ' . $alternativeAddress['Clientdestination']['province'] . ' - ' . $nationName;
            }

            $this->set('alternativeAddress', $arrayOfAlternativeAddress);
        }
        else
        {
            $supplierAlternativeAddresses = $this->Utilities->getSupplierDifferentAddress($transport['Transport']['supplier_id']);

            foreach ($supplierAlternativeAddresses as $supplierAlternativeAddress)
            {
                isset($supplierAlternativeAddress['Supplierdestination']['nation_id']) ? $nationName = $this->Utilities->getNationName($supplierAlternativeAddress['Supplierdestination']['nation_id'])['Nation']['name'] : $nationName = '';
                $arrayOfAlternativeAddress[$supplierAlternativeAddress['Supplierdestination']['id']] = $supplierAlternativeAddress['Supplierdestination']['name'] . ' - ' . $supplierAlternativeAddress['Supplierdestination']['address'] . ' - ' . $supplierAlternativeAddress['Supplierdestination']['cap'] . ' - ' . $supplierAlternativeAddress['Supplierdestination']['city'] . ' - ' . $supplierAlternativeAddress['Supplierdestination']['province'] . ' - ' . $nationName;
            }

            $this->set('supplierAlternativeAddress', $arrayOfAlternativeAddress);
        }

        $causals = $this->Utilities->getCausals();

        if ($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) == 'F')
            $causals = $this->Utilities->getCausals('fornitori');

        if($this->Causal->getCausalType($this->request->data['Transport']['causal_id']) == 'C')
            $causals = $this->Utilities->getCausals('clienti');

        $this->set('causali', $causals);
        $this->set('getAviableQuantity', $this->Utilities);
        $this->set('units', $this->Units->getList());
        $this->set('carriers', $this->Utilities->getCarriersList());
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('vats', $this->Utilities->getVatsList());


        if(ADVANCED_STORAGE_ENABLED)
        {
            $this->set('deposits', $this->Utilities->getDepositsList());
            $this->set('mainDeposit',$this->Utilities->getDefaultDeposits());
        }

        $this->render('edit');
    }

    public function delete($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Sectional', 'Transports', 'Messages']);
        $asg =  ["la", "Bolla", "F"];

        if (!$this->request->is('post'))
            throw new MethodNotAllowedException();

        $this->Transport->id = $id;
        $oldTransport = $this->Transports->find('first',['conditions'=>['Transports.id' => $id]]);

        /*Recupero i beni da rimettere nel magazzino*/
        $transportgoods = $this->Transportgood->find('all', ['conditions' => ['Transportgood.transport_id' => $id]]);

        $numberOfMovement = 0;

        foreach($transportgoods as $transportgood)
        {
            if($transportgood['Transportgood']['storage_id'] != '' && $transportgood['Transportgood']['storage_id'] != null)
            {
                $numberOfMovement++;

                if(ADVANCED_STORAGE_ENABLED)
                    $this->Utilities->storageLoad($transportgood['Transportgood']['storage_id'],$transportgood['Transportgood']['quantita'],'Eliminazione  bolla. n.'.$oldTransport['Transports']['transport_number'] . ' del '. date('d-m-Y',strtotime($oldTransport['Transports']['date'])),$this->Utilities->getLastBuyPrice($transportgood['Transportgood']['storage_id']),$oldTransport['Transports']['deposit_id'],$numberOfMovement, 'TR_DE'.'_'.$oldTransport['Transports']['id']);
                else
                    $this->Utilities->storageLoad($transportgood['Transportgood']['storage_id'],$transportgood['Transportgood']['quantita'],'Eliminazione  bolla. n.'.$oldTransport['Transports']['transport_number'] . ' del '. date('d-m-Y',strtotime($oldTransport['Transports']['date'])),0,-1,$numberOfMovement, 'TR_DE'.'_'.$oldTransport['Transports']['id']);
            }
        }

        if($this->Transport->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Transport->find('first',['conditions'=>['Transport.id' => $id, 'Transport.company_id' => MYCOMPANY, 'Transport.state' => ATTIVO]]);

        if ($this->Transport->hide($currentDeleted['Transport']['id']))
        {
            $this->Sectional->reduceSectionalValue($currentDeleted['Transport']['sectional_id'], $currentDeleted['Transport']['transport_number']);

            $this->Transportgood->deleteAll(['Transportgood.transport_id' => $currentDeleted['Transport']['id']]);
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        }
        else
        {
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
        }

        return $this->redirect(['action' => 'index']);
    }

    // Controllo duplicato bolla
    public function checkTransportDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $A =  $this->Utilities->checkTransportDuplicate($_POST['transportnumber'],$_POST['date']);
        return json_encode($A);
    }

    public function pdfddt()
    {
        $this->loadModel('Utilities');
        $this->loadModel('Setting');
        $this->Utilities->loadModels($this,['Pdf']);
        $nomefile = str_replace(' ', '_', $this->params['pass'][0]);
        $id = $this->params['pass'][1];
        $this->layout = 'pdf';
        $transport = $this->Transport->find('first', ['contain' => ['Deposit','Carrier','Supplier','Client'=>['Catalog'=>['CatalogStorage']],'Transportgood' => ['Iva','Storage'=> ['Units'],'Units'],'Payment','Causal'], 'conditions' => ['Transport.id' => $id]]);
        $this->set('transport', $transport);
        $numero = $this->Transport->find('first', ['conditions' => ['Transport.id' => $id]]);
        $this->set('impostazioni', $this->Setting->find('first',['conditions'=>['Setting.company_id'=>MYCOMPANY]]));
        $logo = $this->Setting->find('first');

        $this->set('pdfHeader',$this->Pdf->getTransportHeader());
        $this->set('pdfBody',$this->Pdf->getTransportBody());
        $this->set('pdfFooter',$this->Pdf->getTransportFooter());
        $this->set('settings',$this->Setting->GetMySettings());

        $this->set('transportCausal', $this->Utilities->getTransportCausalNameFromId($transport['Transport']['causal_id']));
        $this->Set('companyLogo',$this->Setting->getCompanyLogo()['Setting']['header']);

        $clientNation = $this->Utilities->getNationName($transport['Transport']['client_nation']);
        $supplierNation = $this->Utilities->getNationName($transport['Transport']['supplier_nation']);
        $clientShipping = $transport['Transport']['client_shipping_nation'];
        $supplierShipping = $transport['Transport']['supplier_shipping_nation'];

        isset($clientNation['Nation']['name']) ?  $clientNation = $clientNation['Nation']['name'] : $clientNation = '';
        isset($supplierNation['Nation']['name']) ?  $supplierNation = $supplierNation['Nation']['name'] : $supplierNation = '';
        isset($clientShipping) ?  $clientShipping  : $clientShipping = '';
        isset($supplierShipping) ? $supplierShipping : $supplierShipping = '';

        $this->Set('clientNation',$clientNation);
        $this->Set('supplierNation', $supplierNation );
        $this->Set('clientShippingNation',$clientShipping);
        $this->Set('supplierShippingNation',$supplierShipping);


        foreach ($transport['Transportgood'] as $transportgood)
        {
            if(isset($transportgood['Storage']['certified']))
            {
                $transportgood['Storage']['certified'] != null ? $arrayCerified[] = $this->Utilities->getFullPathFile('certificazione',$transportgood['Storage']['certified']) : null;
            }
        }

        $transportPdfTitle = 'Bolla n° '. $transport['Transport']['transport_number'] .' del '.date('d/m/Y',strtotime($transport['Transport']['date']));
        return $this->Pdf->setmpdf($transportPdfTitle,$nomefile,$this->Mpdf);	$this->set('certifications',$arrayCerified);
    }

    public function fatturaExtendedClient($clientId)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Tranports']);
        $clientTransports = $this->Transport->find('all',['conditions'=>['Transport.client_id' => $clientId, 'bill_id' => null]]);
        foreach($clientTransports as $transport)
        {
            $arrayOfTransport[] = $transport['Transport']['id'];
        }

        $newBillId = $this->Utilities->buildInvoiceFromTransports($arrayOfTransport);
        $this->redirect(['controller' => 'bills', 'action' => 'editExtendedvfs',	$newBillId]);
    }

    public function fatturaExtended($transportId,$detailedbill,$splitPayment,$electronicInvoice)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $arrayOfTransport[0] = $transportId;
        $newBillId = $this->Utilities->buildInvoiceFromTransports($transportId,$detailedbill,$splitPayment,$electronicInvoice);
        return $newBillId;
    }

    public function transportSummary($filterableFields = [])
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Transport','Myhelper']);
        $conditionsArray = ['Transport.company_id' => MYCOMPANY, 'Transport.bill_id'=>null,'Transport.causal_id NOT IN '=>[3,7],'Transport.state' => ATTIVO];
        $filterableFields = ['ragionesociale',null];

        if($this->request->is('ajax') && isset($this->request->data['filters']))
        {
            if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['Transport.date >='] = date('Y-m-d' , strtotime($this->request->data['filters']['date1'])) ;
            }
            if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['Transport.date <='] = date('Y-m-d' , strtotime($this->request->data['filters']['date2']));
            }

            // Filtro sul multiselect dei clienti
            if(isset($this->request->data['filters']['clients']) && count($this->request->data['filters']['clients']) > 0)
            {
                foreach($this->request->data['filters']['clients'] as $client)
                {
                    $arrayCl[] = $client;
                }

                $conditionsArray['Client.id IN'] = $arrayCl;
            }
        }

        $this->paginate = [
            'contain' => ['Client', 'Quote', 'Transportgood' => ['fields' => ['Transportgood.id','Transportgood.quantita','Transportgood.prezzo','Transportgood.iva_id','Transportgood.oggetto'],'Iva'], 'Bill'],
            'conditions' => $conditionsArray,
            'order' => [ 'Client.ragionesociale' => 'asc', 'Transport.date' => 'desc'],
            'limit' => 100
        ];

        $filteredClients = $this->Transport->find('all', [
            'contain' => ['Client'=>['fields' => ['Client.id, ragionesociale']]],
            'conditions' => $conditionsArray,
            'order' => [ 'Client.ragionesociale' => 'asc', 'Transport.date' => 'desc'],
        ]);

        $customerArray = [];

        foreach ($filteredClients as $filteredClient)
        {
            // Questo fixa un eventuale eliminazione errata (manca il link col cliente) di un cliente
            if($filteredClient['Client']['id'] != null)
            {
                $customerArray[$filteredClient['Client']['id']] =	$filteredClient['Client']['ragionesociale'];
            }
        }

        // Definisco i messaggi degli helepr nella vista
        $this->set('helperMessage',$this->Myhelper->getMessage('selectMultipleClientInBill'));

        $this->set('clients',$customerArray);
        $this->set('filterableFields',$filterableFields);
        $this->Transport->recursive = 0;
        $this->set('transports', $this->paginate());
    }

    public function createBillForTransportSummary()
    {
        $this->loadModel('Utilities');
        $conditionsArray = ['Transport.company_id' => MYCOMPANY, 'Transport.bill_id'=>null, 'Transport.state' => ATTIVO];

        if($this->request->is(['post','ajax']))
        {
            if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['Transport.date >='] = date('Y-m-d' , strtotime($this->request->data['filters']['date1'])) ;
            }
            if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['Transport.date <='] = date('Y-m-d' , strtotime($this->request->data['filters']['date2']));
            }

            if(isset($this->request->data['date3']) && $this->request->data['date3'] != '')
            {
                $customBillDateForTransport = $this->request->data['date3'];
            }
            else
            {
                $customBillDateForTransport = null;
            }

            // Filtro sul multiselect dei clienti
            if(isset($this->request->data['filters']['clients']) && $this->request->data['filters']['clients'] != '')
            {
                foreach($this->request->data['filters']['clients'] as $client)
                {
                    $arrayCl[] = $client;
                }

                $conditionsArray['Client.id IN'] = $arrayCl;
            }
            else // Nessun cliente selezionato
            {
                $conditionsArray['Client.id IN'] = ['-1'];
            }
        }

        // Chiamo la query
        $filteredTransports = $this->Transport->find('all', [
            'contain' => ['Client', 'Quote', 'Transportgood' => ['fields' => ['Transportgood.id','Transportgood.quantita','Transportgood.prezzo','Transportgood.iva_id','Transportgood.oggetto'],'Iva'], 'Bill'],
            'conditions' => $conditionsArray,
            'order' => [ 'Client.ragionesociale' => 'asc', 'Transport.date' => 'asc'],
        ]);

        // Per ogni cliente
        $currentCustomer = null;

        // Per tutti i clienti -- Per tutte le bolle
        $this->request->data['Transport']['split_payment'] == 1 ? $splitPayment = 'true' : $splitPayment = 'false';
        $this->request->data['Transport']['electronic_invoice'] == 1 ? $electronicInvoice = 'true' : $electronicInvoice = 'false';
        $this->request->data['Transport']['detailed'] == 1 ? $detailed = 'true' : $detailed = 'false';

        $tranportToBill = [];
        foreach($filteredTransports as $transport)
        {
            if($currentCustomer != null && $transport['Transport']['client_id'] != $currentCustomer)
            {
                $this->Utilities->buildInvoiceFromTransports($tranportToBill,$detailed,$splitPayment,$electronicInvoice,$customBillDateForTransport);
                $tranportToBill = null;
            }

            $currentCustomer = $transport['Transport']['client_id'];
            $tranportToBill[] =  $transport['Transport']['id'];
        }

        // Ultimo cliente del ciclo
        if(isset($tranportToBill))
        {
            $this->Utilities->buildInvoiceFromTransports($tranportToBill,$detailed,$splitPayment,$electronicInvoice,$customBillDateForTransport);
        }

        // Creo fattura
        $this->Session->setFlash(__('Fatture correttamente create'), 'custom-flash');
        $this->redirect(['controller'=>'transports' , 'action'=>'transportSummary']);
    }

    public function closeTransport($id)
    {
        $this->autoRender = false;
        $this->Transport->updateAll(['Transport.state' => 2], ['Transport.id' => $id]);
        return $this->redirect(['action' => 'index']);
    }
}
