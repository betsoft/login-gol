<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'tpmacn/csvhelper');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('Routing','Router');
App::uses('Xml', 'Utility');
require 'PHPMailer/PHPMailerAutoload.php';

class LoadgoodsController extends AppController
{
	public $components = ['Mpdf'];
	var $uses = ['Loadgood'];

	public function beforeFilter()
	{
		parent::beforeFilter();
		$anno = date('Y');
		$this->Auth->allow('mailpdf','mailpdfcreditnote');
	}


	public function index()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Loadgood','Csv']);
		$conditionsArray = ['Loadgood.company_id' => MYCOMPANY,'load_good_date >= ' => date("Y").'-01-01','load_good_date <= ' => date("Y").'-12-31'];

		$filterableFields = ['load_good_number','#htmlElements[0]','Supplier__name',null, null,null,null,'load_good_note',null,null];
		$sortableFields = [['name','N° registrazione em'],['load_good_date','Data e.m.'],['Supplier__name','Fornitore'],[null,'Imponibile'],[null,'Iva'],[null,'Totale'],[null,'Note'],[null,'Numero ddt'],[null,'Data ddt'] ,['#actions']];

		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }


		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			if(isset($this->request->data['filters']['Supplier__name']) && $this->request->data['filters']['Supplier__name'] != '') { $conditionsArray['Supplier.name like' ] = '%'. $this->request->data['filters']['Supplier__name']. '%'; }
			if(isset($this->request->data['filters']['load_good_number']) && $this->request->data['filters']['load_good_number'] != '') { $conditionsArray['load_good_number like' ] = '%'. $this->request->data['filters']['load_good_number']. '%'; }
			if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != ''){$conditionsArray['load_good_date >= '] = date('Y-m-d',strtotime($this->request->data['filters']['date1'])); }
			if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != ''){ $conditionsArray['load_good_date <= '] =  date('Y-m-d',strtotime($this->request->data['filters']['date2'])) ; }

			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}



		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Loadgood->find('all',['conditions'=>$conditionsArray,'order' => ['load_good_date' => 'desc']]);
			echo 'N° Registrazione e.m.;Data e.m.;Fornitore;Imponibile;Iva;Totale;Note;Numero ddt;Data ddt;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				$loadgoodId = $xlsRow['Loadgood']['id'];
				$taxableIncome = $this->Utilities->getLoadgoodTotalTaxableIncome($loadgoodId);
				$vat = $this->Utilities->getLoadgoodTotalVat($loadgoodId);
				$total  = $this->Utilities->getLoadgoodTotal($loadgoodId);
				echo $xlsRow['Loadgood']['load_good_number']. '/' . substr($xlsRow['Loadgood']['load_good_date'],0,4) . ';'. date("d-m-Y",strtotime($xlsRow['Loadgood']['load_good_date'])).';'.$xlsRow['Supplier']['name'].';'.$taxableIncome .';'.$vat.';'.$total. ';'. "\r\n";
			}
		}
		else
		{
			$this->paginate = ['contain' => [ 'Supplier','Loadgoodrow'=>['Iva'],],'conditions' => $conditionsArray, 'limit' => 100,'order' => ['load_good_date' => 'desc']];
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->set('loadgoods', $this->paginate());
			$this->set('Utilities', $this->Utilities);
			$this->render('index');
		}
	}

	public function add($redirect = 'index')
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Suppliers', 'Loadgooddeadlines', 'Loadgoodrow', 'Ivas', 'Units','Quote','Constructionsite']);

        $this->set('n_fatt', $this->Utilities->getNextLoadgoodNumber(date("Y"))); // Le prende tutte

        if ($this->request->is(['post', 'put'])) {
            $supplier = $this->Suppliers->find('first', ['conditions' => ['Suppliers.name' => $this->request->data['Loadgood']['supplier_id'], 'Suppliers.company_id' => MYCOMPANY], 'fields' => ['Suppliers.id']]);
            $this->request->data['Loadgood']['company_id'] = MYCOMPANY;

            $numberOfMovement = 0;

            // Se il cliente non esiste allora lo creo
            empty($supplier['Suppliers']['id']) ? $this->request->data['Loadgood']['supplier_id'] = $this->Utilities->createSupplier($this->request->data['Loadgood']['supplier_id']) : $this->request->data['Loadgood']['supplier_id'] = $supplier['Suppliers']['id'];

            $this->request->data['Loadgood']['load_good_date'] = date("Y-m-d", strtotime($this->request->data['Loadgood']['date']));

            isset($this->request->data['Loadgood']['ddt_date']) ? $this->request->data['Loadgood']['ddt_date'] = date("Y-m-d", strtotime($this->request->data['Loadgood']['ddt_date'])) : null;

            // Se è settato splitpayment
            $newLoadgood = $this->Loadgood->create();

            $this->request->data['Loadgood']['state'] = 1;

            if ($newLoadgood = $this->Loadgood->save($this->request->data['Loadgood'])) {
                // Setto a zero i valori per i calcoli delle scadenze
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];

                foreach ($this->request->data['Good'] as $key => $good) {
                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                    $newLoadgoodGood = $this->Utilities->createLoadGoodRow($newLoadgood['Loadgood']['id'], $good);


                    // Se l'articolo esiste già a magazzino
                    if (($good['storage_id']) != '') {
                        $storageId = $good['storage_id'];
                        $this->Utilities->storageLoad($storageId, $good['quantity'], 'Carico entrata merce. n.' . $newLoadgood['Loadgood']['load_good_number'] . ' del ' . date('d-m-Y', strtotime($newLoadgood['Loadgood']['load_good_date'])), $good['load_good_row_price'], $newLoadgood['Loadgood']['deposit_id'], $numberOfMovement, 'LG_AD');
                        if (isset($good['suggestedsellprice']) && $good['suggestedsellprice'] != '') {
                            $this->Utilities->updateStoragePrice($storageId, $good['suggestedsellprice']);
                        }
                    } else // Se l'articolo non esiste
                    {
                        // Entrata merce non ha note quindi non controllo che ci siano
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            // Creo nuovo articolo magazzino
                            $newStorage = $this->Utilities->createNewStorageFromLoad($good, $this->request->data['Loadgood']['supplier_id']);
                            if (isset($good['suggestedsellprice']) && $good['suggestedsellprice'] != '') {
                                $this->Utilities->updateStoragePrice($newStorage['Storage']['id'], $good['suggestedsellprice']);
                            }
                            $this->Loadgoodrow->updateAll(['Loadgoodrow.storage_id' => $newStorage['Storage']['id'], 'Loadgoodrow.company_id' => MYCOMPANY], ['Loadgoodrow.id' => $newLoadgoodGood['Loadgoodrow']['id']]);
                            $this->Utilities->storageLoad($newStorage['Storage']['id'], $good['quantity'], 'Carico (oggetto nuovo di magazzino) entrata merce n.' . $newLoadgood['Loadgood']['load_good_number'] . ' del ' . date('d-m-Y', strtotime($newLoadgood['Loadgood']['load_good_date'])), $good['load_good_row_price'], $newLoadgood['Loadgood']['deposit_id'], $numberOfMovement, 'LG_AD');
                        }
                    }
                }

                $this->Session->setFlash(__('L\'entrata merce a è stata salvata'), 'custom-flash');
                $this->redirect(['action' => $redirect]);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare l\'entrata merce, riprovare'), 'custom-danger');
            }
        }

        // $this->set('units',$this->Utilities->getUnitsList());
        $this->set('units', $this->Units->getList());

        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());
        $magazzino = $this->Utilities->getStoragesWithVariations();
        $this->set('magazzini', $this->Utilities->getStoragesWithVariationsNotUnique());
        $this->set('vats', $this->Utilities->getVatsList());
        // Per definire la tipologia all'interno della vista [ false = 1 fattura normale, true = 6 fattura accompagnatoria, utilizzato solo in add_extendedvfs]

        // Se avanza
        $this->set('deposits', $this->Utilities->getDepositsList());
        $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());

        if(MODULO_CANTIERI)
        {
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->set('quotes', $this->Quote->getList());
        }


        $this->Render('add');
    }


	public function edit($id = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier', 'Storage', 'Loadgoodrow', 'Units','Quote','Constructionsite']);

        $numberOfMovement = 0;

        if (!empty($this->params['pass'][0])) {
            $this->Loadgood->id = $this->params['pass'][0];
        } else {
            $this->Loadgood->id = $id;
        }

        if (!$this->Loadgood->exists()) {
            throw new NotFoundException(__('Entrata merce non valida'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            // Recupero i vecchi articoli
            $oldGood = $this->Loadgoodrow->find('all', ['conditions' => ['Loadgoodrow.company_id' => MYCOMPANY, 'load_good_id' => $id]]);

            $loadGoodOldValues = $this->Loadgood->find('first', ['conditions' => ['Loadgood.id' => $id]]);
            if ($this->request->data['Loadgood']['load_good_date'] == '' || $this->request->data['Loadgood']['load_good_date'] == '01-01-1970') {
                $this->request->data['Loadgood']['load_good_date'] = $loadGoodOldValues['Loadgood']['load_good_date'];
            } else {
                $this->request->data['Loadgood']['load_good_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Loadgood']['load_good_date'])));
            }


            if ($this->request->data['Loadgood']['ddt_date'] == '' || $this->request->data['Loadgood']['ddt_date'] == '01-01-1970') {
                $this->request->data['Loadgood']['ddt_date'] = $loadGoodOldValues['Loadgood']['ddt_date'];
            } else {
                $this->request->data['Loadgood']['ddt_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Loadgood']['ddt_date'])));
            }


            // Scarico entrata merce
            $numberOfMovement = 0;
            foreach ($oldGood as $oldLoadGoodGood) {
                $numberOfMovement++;

                // Riscarico tutto
                if ($oldLoadGoodGood['Loadgoodrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                {
                    // E' di default advanced_storage_enbaled
                    $this->Utilities->storageUnload($oldLoadGoodGood['Loadgoodrow']['storage_id'], $oldLoadGoodGood['Loadgoodrow']['quantity'], 'Scarico entrata merce n.' . $this->request->data['Loadgood']['load_good_number'] . ' del ' . date('d-m-Y', strtotime($this->request->data['Loadgood']['load_good_date'])) . ' per modifica ', $oldLoadGoodGood['Loadgoodrow']['load_good_row_price'], $this->request->data['Loadgood']['deposit_id'], $numberOfMovement, 'LG_ED');
                }
                $this->Loadgoodrow->deleteAll(['Loadgoodrow.id' => $oldLoadGoodGood['Loadgoodrow']['id']]);
            }

            $this->request->data['state'] = 1;

            if ($newLoadgood = $this->Loadgood->save($this->request->data['Loadgood'])) {
                foreach ($this->request->data['Loadgoodrow'] as $key => $good)
                {
                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                    $newLoadgoodGood = $this->Utilities->createLoadGoodRow($newLoadgood['Loadgood']['id'], $good);

                    // Se l'articolo esiste già a magazzino
                    if ($good['storage_id'] != '') {
                        $storageId = $good['storage_id'];
                        $newStorage = $this->Utilities->createNewStorageFromLoad($good, $loadGoodOldValues['Loadgood']['supplier_id']);
                        if (isset($good['suggestedsellprice']) && $good['suggestedsellprice'] != '') {
                            $this->Utilities->updateStoragePrice($newStorage['Storage']['id'], $good['suggestedsellprice']);
                        }
                        // carico  quantità a magazzino
                        $this->Utilities->storageLoad($storageId, $newLoadgoodGood['Loadgoodrow']['quantity'], 'Carico entrata merce. n.' . $newLoadgood['Loadgood']['load_good_number'] . ' del ' . date('d-m-Y', strtotime($newLoadgood['Loadgood']['load_good_date'])), $newLoadgoodGood['Loadgoodrow']['load_good_row_price'], $newLoadgood['Loadgood']['deposit_id'], $numberOfMovement, 'LG_ED');
                    } else {
                        // Se l'articolo non esiste
                        if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                            // Creo nuovo articolo magazzino
                            $good['movable'] = 1; // Sono di sicuro sempre articoli di magazizno
                            $newStorage = $this->Utilities->createNewStorageFromLoad($good, $loadGoodOldValues['Loadgood']['supplier_id']);
                            if (isset($good['suggestedsellprice']) && $good['suggestedsellprice'] != '') {
                                $this->Utilities->updateStoragePrice($newStorage['Storage']['id'], $good['suggestedsellprice']);
                            }
                            // Aggiorno lo storage_id sul good
                            $this->Loadgoodrow->updateAll(['Loadgoodrow.storage_id' => $newStorage['Storage']['id'], 'Loadgoodrow.company_id' => MYCOMPANY], ['Loadgoodrow.id' => $newLoadgoodGood['Loadgoodrow']['id']]);

                            // Carico scarico virtuale
                            $this->Utilities->storageLoad($newStorage['Storage']['id'], $good['quantity'], 'Carico (oggetto nuovo di magazzino) modifica  entrata merce n.' . $newLoadgood['Loadgood']['load_good_number'] . ' del ' . date('d-m-Y', strtotime($newLoadgood['Loadgood']['load_good_date'])), $good['load_good_row_price'], $good['load_good_row_discount'], $newLoadgood['Loadgood']['deposit_id'], $numberOfMovement, 'LG_ED');
                        }
                    }
                }

                $this->Session->setFlash(__('Entrata merce salvata correttamente'), 'custom-flash');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Session->setFlash(__('L\'entrata merce non è stata salvata correttamente.'), 'custom-danger');
            }
        } else {
            $this->request->data = $this->Loadgood->find('first', ['contain' => ['Supplier', 'Loadgoodrow' => ['Storage']], 'recursive' => 2, 'conditions' => ['Loadgood.id' => $id, 'Loadgood.company_id' => MYCOMPANY]]);
        }

        $this->set('deposits', $this->Utilities->getDepositsList());
        $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());

        $this->set('units', $this->Units->getList());
        $this->loadModel('Supplier');
        $this->set('suppliers', $this->Supplier->getList());

        $this->set('magazzini', $this->Utilities->getStoragesWithVariationsNotUnique());
        $this->set('vats', $this->Utilities->getVatsList());

        if(MODULO_CANTIERI)
        {
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->set('quotes', $this->Quote->getList());
        }
    }


	public function delete($id = null)
	{

		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Loadgood','Loadgoodrow','Storage']);

		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}

		$this->Loadgood->id = $id;
		$Loadgoodrow = $this->Loadgoodrow->find('all', ['conditions' => ['Loadgoodrow.load_good_id' => $id]]);
		$oldLoadGood = $this->Loadgood->find('first',['conditions'=>['Loadgood.id' => $id]]);
		$numberOfMovement = 0;

		foreach($Loadgoodrow as $good)
		{
			$numberOfMovement++;
			$storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $good['Loadgoodrow']['storage_id']]]);
			// Se è presente l'id dell'articolo a magazzino
			if(isset($storage['Storage']['id']))
			{
				$this->Utilities->storageUnload($storage['Storage']['id'],$good['Loadgoodrow']['quantity'],'Carico per eliminazione entrata merce. n.'.$oldLoadGood['Loadgood']['load_good_number'] . ' del '. date('d-m-Y',strtotime($oldLoadGood['Loadgood']['load_good_date'])),$this->Utilities->getLastBuyPrice($storage['Storage']['id']),$oldLoadGood['Loadgood']['deposit_id'],$numberOfMovement, 'LG_DE');
			}
	    }


        /*
	        $asg =  ["l'","entrata merce","F"];
			if($this->Loadgood->isHidden($id))
				throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

			$this->request->allowMethod(['post', 'delete']);

	        $currentDeleted = $this->StorageLoadgood->find('first',['conditions'=>['Loadgood.id'=>$id,'Loadgood.company_id'=>MYCOMPANY]]);
	        if ($this->Loadgood->hide($currentDeleted['Loadgood']['id']))
		      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
	        else
	           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
			return $this->redirect(['action' => 'index']);
		*/

		if ($this->Loadgood->delete($id))
		{

		   // Se esiste una corrispondente bolla slego l'id della fatutura alla bolla
			$this->Loadgoodrow->deleteAll(['load_good_id'=>$id]);
			$this->Session->setFlash(__('Entrata merce eliminata correttamente'), 'custom-flash');
			$this->redirect(['action' => 'index']);
		}

		$this->Session->setFlash(__('Entrata merce non eliminata'), 'custom-danger');
		$this->redirect(['action' => 'index'  ]);
	}


	public function createBuyBillsFromLoadGoods($loadGoodId)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Bill','Good']);

		$currentLoadGood = $this->Loadgood->find('first',['conditions'=>['Loadgood.id'=>$loadGoodId,'Loadgood.company_id'=>MYCOMPANY]]);
		// Creao la nuova fattura
		$newBuyBill = $this->Utilities->createBuyBillForSupplier($currentLoadGood['Loadgood']['supplier_id']);

		// Salvo le righe
		foreach($currentLoadGood['Loadgoodrow'] as $loadGoodRow)
		{
			$newGood = $this->Good->create();
			isset($loadGoodRow['description']) ?  $newGood['Good']['oggetto'] = $loadGoodRow['description'] :   $newGood['Good']['oggetto'] = '' ;
			isset($loadGoodRow['quantity']) ?  $newGood['Good']['quantita'] = $loadGoodRow['quantity'] :  $newGood['Good']['quantita'] = 0;
			isset($loadGoodRow['load_good_row_price']) ? $newGood['Good']['prezzo'] =  $loadGoodRow['load_good_row_price'] : $newGood['Good']['prezzo'] = 0;
			isset($loadGoodRow['load_good_row_vat_id']) ? $newGood['Good']['iva_id'] = $loadGoodRow['load_good_row_vat_id'] : $newGood['Good']['iva_id'] = '';
			$newGood['Good']['bill_id'] = $newBuyBill['Bill']['id'];
			isset($loadGoodRow['storage_id']) ? $newGood['Good']['storage_id'] = $loadGoodRow['storage_id'] : $newGood['Good']['storage_id'] = '';
			$newGood['Good']['transport_id'] = null;
			isset($loadGoodRow['unit_of_measure_id']) ? $newGood['Good']['unita'] =$loadGoodRow['unit_of_measure_id'] : $newGood['Good']['unita'] = '';
			$newGood['Good']['discount'] = 0;
			isset($loadGoodRow['codice']) ? $newGood['Good']['codice'] = $loadGoodRow['codice'] : $newGood['Good']['codice'] = '';
			$newGood['Good']['company_id'] = MYCOMPANY;
			$newGood['Good']['vat'] = 0;
			$newBillRow = $this->Good->save($newGood);
		}

		// Setto l'entrata merce come registrata
		$this->Loadgood->updateAll(['buy_bill_created'=>1],['Loadgood.id'=>$loadGoodId,'Loadgood.company_id'=>MYCOMPANY]);
		// Mi sposto sulla modifica delle fatture
		$this->redirect(['controller'=>'bills' ,'action'=>'editExtendedBuy',$newBuyBill['Bill']['id']]);

	}

	// Controllo duplicato entrata merce
	public function checkLoadGoodDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$A =  $this->Utilities->checkLoadGoodDuplicate($_POST['loadgoodnumber'],$_POST['date']);
		return json_encode($A);
	}

    public function createLoadgoodFromPurchaseOrder($purchaseOrderId)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Loadgood');
            $newLoadgoodId = $this->Loadgood->createLoadGoodFromPurchaseOrder($purchaseOrderId);
            $this->redirect(['controller' => 'loadgoods', 'action' => 'edit', $newLoadgoodId]);
        }
    }


    public function  multiLoadgoodsBillCreate(){

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Supplier']);
        $this->set('suppliers', $this->Supplier->getList());

    }

    public function multiLoadgoodBill(){
        $startDate = date('Y-m-d', strtotime($_POST['dateFrom'])); $endDate = date('Y-m-d', strtotime($_POST['dateTo'])); $supplier_id = $_POST['supplierId'];
        $this->autoRender = false;
        $this->loadModel('Loadgood');

        $LoadgoodId = $this->Loadgood->find('all', ['recursive'=>-1, 'conditions'=>["load_good_date >="=>$startDate, "load_good_date <="=>$endDate, "Supplierdestination.supplier_id"=>$supplier_id, "Loadgood.completed"=>1, "Loadgood.bill_id" =>null], 'fields' => ['id']]);
        $newBillId = $this->Utilities->buildInvoiceFromLoadgoods($LoadgoodId,false,false,true);
        return $newBillId;
    }
}
