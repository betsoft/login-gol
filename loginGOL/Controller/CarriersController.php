<?php
App::uses('AppController', 'Controller');


class  CarriersController extends AppController {

	public function index() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Carrier','Csv']);
		$conditionsArray = [ 'Carrier.company_id' => MYCOMPANY ,'Carrier.state' => ATTIVO ];
		$filterableFields = ['Carrier__name','piva','cf',null];
		$sortableFields = [['name','Ragione sociale'],['piva','P.iva'],['cf','Codice fiscale'],null];
		
		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		
		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
		
			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession); 
		}
		
		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Carrier->find('all',['conditions'=>$conditionsArray,'order' => ['Carrier.name' => 'asc']]); 			
			echo 'Ragione sociale;Codice fiscale;P.iva;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				echo $xlsRow['Carrier']['name']. ';' .$xlsRow['Carrier']['piva']. ';'.$xlsRow['Carrier']['cf']. ';'."\r\n";
			}
		}
		else
		{
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->paginate = ['conditions' => $conditionsArray];
			$this->set('carrier', $this->paginate());
		}
	}

	public function add() 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Carrier']);
		$nations = $this->Utilities->getNationsList();
		$this->set('nations',$nations);
		if ($this->request->is('post')) 
		{
			$this-> Carrier->create();
				$this->request->data['Carrier']['company_id']=MYCOMPANY;
				if ($this->Carrier->save($this->request->data)) 
				{
					$this->Session->setFlash(__('Vettore salvato'), 'custom-flash');
					$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('Il vettore non è stato salvato'), 'custom-danger');
			}
		}
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Carrier']);
		$this->Carrier->id = $id;
		$nations = $this->Utilities->getNationsList();
		$this->set('nations',$nations);
		if (!$this->Carrier->exists()) {
			throw new NotFoundException(__('Vettore non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Carrier->save($this->request->data)) {
				$this->Session->setFlash(__('Vettore salvato correttamente'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('Vettore non salvato, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Carrier->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Carrier','Messages']);
        $asg =  ["il","vettore","M"];
		if($this->Carrier->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Carrier->find('first',['conditions'=>['Carrier.id'=>$id,'Carrier.company_id'=>MYCOMPANY]]);
        if ($this->Carrier->hide($currentDeleted['Carrier']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}

	// Controllo duplicato fattura
	public function checkCarrierPivaDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkCarrierPivaDuplicate($_POST['piva']);
		return json_encode($return);
	}
	
	// Controllo duplicato fattura
	public function checkCarrierCfDuplicate()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$return =  $this->Utilities->checkCarrierCfDuplicate($_POST['cf']);
		return json_encode($return);
	}

}
