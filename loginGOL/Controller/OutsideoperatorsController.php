<?php
App::uses('AppController', 'Controller');

class OutsideoperatorsController extends AppController
{
    public function index()
    {

        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->loadModel('Outsideoperator');

            $conditionsArray = ['state' => ATTIVO,'company_id'=>MYCOMPANY];
            $filterableFields = ['surname', 'name','labour_cost','labour_cost_night','labour_cost_weekend', null];
            $sortableFields = [['surname', 'Cognome'], ['name', 'Nome'],['labour_cost','Costo orario'],['labour_cost_night','Costo notturno'],['labour_cost_weekend','Costo weekend'], ['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            // Generazione XLS
            if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
                $this->autoRender = false;
                $dataForXls = $this->Outsideoperator->find('all', ['conditions' => $conditionsArray, 'order' => ['Outsideoperator.name' => 'asc']]);
                echo 'Operatori esterni' . "\r\n";
                foreach ($dataForXls as $xlsRow) {
                    echo $xlsRow['Outsideoperator']['name'] . ';' . $xlsRow['Outsideoperator']['surname'] . ';' . "\r\n";
                }
            } else {
                $this->paginate = ['conditions' => $conditionsArray,'order' => ['Outsideoperator.surname' => 'asc', 'Outsideoperator.name' => 'asc'],];
                $this->set('outsideoperators', $this->paginate());
            }

        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Outsideoperators', 'Messages']);
            $messageParameter = ["l'", "operatore esterno", "M"];
            if ($this->request->is('post')) {
                $this->Outsideoperators->create();

                if ($this->Outsideoperators->save($this->request->data)) {
                    $this->Session->setFlash(__($this->Messages->successOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__($this->Messages->filedOfAdd($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                }
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {
        IF (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Outsideoperators', 'Messages']);
            $messageParameter = ["l'", "operatore esterno", "M"];
            $this->Outsideoperators->id = $id;
            if (!$this->Outsideoperators->exists()) {
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));
            }
            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->Outsideoperators->save($this->request->data)) {
                    $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__($this->Messages->failedOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                }
            } else {
                $this->request->data = $this->Outsideoperators->read(null, $id);
            }
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }


    public function delete($id = null)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Outsideoperator']);

            $messageParameter = ["l'", "operatore esterno", "M"];
            if ($this->Outsideoperator->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Outsideoperator->find('first', ['conditions' => ['Outsideoperator.id' => $id]]);
            if ($this->Outsideoperator->hide($currentDeleted['Outsideoperator']['id']))
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
            return $this->redirect(['action' => 'index']);
        } else {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function operatordetails($id)
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Technician']);

            $conditionsArray = ['Outsideoperator.company_id' => MYCOMPANY, 'Outsideoperator.state' => ATTIVO,'Outsideoperator.operator_id' => $id];
            $filterableFields = [null,null,'Outsideoperator.description', null,null,null,null,null,null];
            $sortableFields = [[null, 'Numero scheda'], [null,'Data scheda'],['description','Descrizione intervento'],[null,'Ora inizio'],[null,'Ora fine'],[null,'Totale'],[null,'Weekend'],[null,'Notturno'],[null,'Cantiere']];

            if ($this->request->is('ajax') && isset($this->request->data['filters']))
            {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
            }

            $this->set('OperatorName', $this->Outsideoperator->getSurnameAndName($id));
            $this->set('OperatorHours', $this->Outsideoperator->getWorkedHour($id));

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            $this->set('utilities', $this->Utilities);
        }
    }

}
