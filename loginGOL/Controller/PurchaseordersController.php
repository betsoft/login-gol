<?php
App::uses('AppController', 'Controller');

class PurchaseordersController extends AppController
{
    public $components = array('Mpdf');

    public function index()
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Purchaseorder', 'Csv']);

            $conditionsArray = ['Purchaseorder.state' => 1, 'Purchaseorder.company_id' => MYCOMPANY, 'date >= ' => date("Y") . '-01-01', 'date <= ' => date("Y") . '-12-31'];
            // $filterableFields = ['number', '#htmlElements[0]', 'ragionesociale', null, null];
            $filterableFields = ['number', '#htmlElements[0]', 'name', null];
            //$sortableFields = [['number', 'N° ordine'], ['date', 'Data ordine'], ['supplier_id', 'Fornitore'], [null, 'Cantiere'], ['#actions']];
            $sortableFields = [['number', 'N° ordine'], ['date', 'Data ordine'], ['supplier_id', 'Fornitore'], ['#actions']];

            $automaticFilter = $this->Session->read('arrayOfFilters');

            if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) {
                $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
            } else {
                null;
            }

            if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters'])) {

                $conditionsArray = ['Purchaseorder.company_id' => MYCOMPANY, 'Purchaseorder.state' => 1];

                if (isset($this->request->data['filters']['Purchaseorder.number'])) {
                    $conditionsArray['Purchaseorder.number like'] = '%' . $this->request->data['filters']['Purchaseorder.number'] . '%';
                }
                if (isset($this->request->data['filters']['name']) && $this->request->data['filters']['name'] != '') {
                    $conditionsArray['Supplier.name like'] = '%' . $this->request->data['filters']['name'] . '%';
                }
                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '') {
                    $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));
                }
                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '') {
                    $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
                }

                $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
                $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
                $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
            }

            // Se attiva la personalizzazione delle stampe
            $this->paginate = ['contain' =>
                ['Supplier',
                    'Purchaseorderrow',
                    'Constructionsite',
                ],
                'conditions' => $conditionsArray,
                'limit' => 100,
                'order' => ['date' => 'desc', 'number' => 'desc']
            ];

            if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
                $this->autoRender = false;
                $dataForXls = $this->Purchaseorder->find('all', ['conditions' => $conditionsArray, 'order' => ['Purchaseorder.number' => 'asc', 'Purchaseorder.date' => 'desc']]);
                echo 'N° Preventivo;Data Preventivo;Cliente;Imponibile;Iva;Totale' . "\r\n";
                foreach ($dataForXls as $xlsRow) {
                    $purchaseorderTaxableIncome = $this->Utilities->getPurchaseordereTaxableIncome($xlsRow['Purchaseorder']['id']);
                    $purchaseorderVat = $this->Utilities->getPurchaseorderTotalVat($xlsRow['Purchaseorder']['id']);
                    $purchaseorderTotal = $this->Utilities->getPurchaseorderTotal($xlsRow['Purchaseorder']['id']);
                    echo $xlsRow['Purchaseorder']['number'] . ';' . $xlsRow['Purchaseorder']['date'] . $xlsRow['Supplier']['name'] . ';' . $purchaseorderTaxableIncome . ';' . $purchaseorderVat . ';' . $purchaseorderTotal . ';' . "\r\n";
                }
            } else {
                $this->set('sortableFields', $sortableFields);
                $this->set('filterableFields', $filterableFields);
                $this->set('purchaseorders', $this->paginate());
                $this->render('index');
            }
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Supplier');
            $this->loadModel('Constructionsite');
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Storage', 'Suppliers', 'Purchaseorderrow', 'Ivas', 'Units', 'Purchaseorder']);

            $this->set('purchaseOrderNumber', $this->Purchaseorder->getNextPurchaseorderNumber(date("Y")));

            if ($this->request->is(['post', 'put']))
            {
                $supplier = $this->Suppliers->find('first', ['conditions' => ['Suppliers.name' => $this->request->data['Purchaseorder']['supplier_id'], 'Suppliers.company_id' => MYCOMPANY], 'fields' => ['Suppliers.id']]);
                $this->request->data['Purchaseorder']['company_id'] = MYCOMPANY;

                $numberOfMovement = 0;

                // Se il fornitore non esiste allora lo creo
                $this->request->data['Purchaseorder']['date'] = date("Y-m-d", strtotime($this->request->data['Purchaseorder']['date']));

                // Se è settato splitpayment
                $newPurchaseorder = $this->Purchaseorder->create();

                $this->request->data['state'] = 1;
                $this->request->data['Purchaseorder']['note'] = nl2br($this->request->data['Purchaseorder']['note']);
                if ($newPurchaseorder = $this->Purchaseorder->save($this->request->data['Purchaseorder'])) {
                    // Setto a zero i valori per i calcoli delle scadenze
                    $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = 0;  // Aggiunto ritenuta acconto
                    $arrayIva = $arrayImponibili = [];

                    foreach ($this->request->data['Good'] as $key => $good) {
                        $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino

                        $newPurchaseOrderRow = $this->Purchaseorder->createPurchaseOrderRow($newPurchaseorder['Purchaseorder']['id'], $good);


                        // Se l'articolo esiste già a magazzino
                        if ($good['storage_id'] != '') {
                            $storageId = $good['storage_id'];
                        } else // Se l'articolo non esiste
                        {
                            if (!$this->Utilities->isANote($good, 'Purchaseorder')) {
                                if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                    // Creo nuovo articolo magazzino
                                    $newStorage = $this->Purchaseorder->createNewStorageFromPurchaseorder($good, $this->request->data['Purchaseorder']['supplier_id']); // ??
                                    // Aggiorno lo storage_id sul good
                                    $this->Purchaseorderrow->updateAll(['Purchaseorderrow.storage_id' => $newStorage['Storage']['id'], 'Purchaseorderrow.company_id' => MYCOMPANY], ['Purchaseorderrow.id' => $newPurchaseOrderRow['Purchaseorderrow']['id']]);
                                }
                            }
                        }
                    }
                    $this->Session->setFlash(__('Il preventivo è stato salvato'), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__('Non è stato possibile salvareil preventivo, riprovare'), 'custom-danger');
                }
            }

            if (ADVANCED_STORAGE_ENABLED)
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'true')); // Solo articoli di magazzino
            else
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

            $this->set('units', $this->Units->getList());
            $this->set('suppliers', $this->Supplier->getList());
            $this->set('vats', $this->Utilities->getVatsList());
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->Render('add');
        }
    }

    public function edit($id = null)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Client', 'Storage', 'Purchaseorderrow', 'Units']);

            $numberOfMovement = 0;

            if (!empty($this->params['pass'][0])) {
                $this->Purchaseorder->id = $this->params['pass'][0];
            } else {
                $this->Purchaseorder->id = $id;
            }

            if (!$this->Purchaseorder->exists()) {
                throw new NotFoundException(__('Preventivo non valido'));
            }

            if ($this->request->is('post') || $this->request->is('put')) {

                // Recupero i vecchi articoli
                $oldGood = $this->Purchaseorderrow->find('all', ['conditions' => ['Purchaseorderrow.company_id' => MYCOMPANY, 'purchaseorder_id' => $id]]);
                $purchaseorderGoodOldValues = $this->Purchaseorder->find('first', array('conditions' => array('Purchaseorder.id' => $id)));

                $this->request->data['Purchaseorder']['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Purchaseorder']['date'])));
                isset($this->request->data['Purchaseorder']['note']) ? $this->request->data['Purchaseorder']['note'] = nl2br($this->request->data['Purchaseorder']['note']) : $this->request->data['Purchaseorder']['note'] = '';
                $numberOfMovement = 0;
                foreach ($oldGood as $oldPurchaseorderGoodGood) {
                    $numberOfMovement++;

                    // Riscarico tutto
                    if ($oldPurchaseorderGoodGood['Purchaseorderrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                    {
                    }
                    $this->Purchaseorderrow->deleteAll(['Purchaseorderrow.id' => $oldPurchaseorderGoodGood['Purchaseorderrow']['id']]);
                }

                $this->request->data['Purchaseorder']['state'] = 1;
                if ($newPurchaseOrder = $this->Purchaseorder->save($this->request->data['Purchaseorder'])) {
                    foreach ($this->request->data['Purchaseorderrow'] as $key => $good) {
                        $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                        $newPurchaseOrderRow = $this->Purchaseorder->createPurchaseOrderRow($newPurchaseOrder['Purchaseorder']['id'], $good);
                        isset($good['storage_id']) ? null : $good['storage_id'] = '';
                        // Se l'articolo esiste già a magazzino
                        if ($good['storage_id'] != '') {
                            $storageId = $good['storage_id'];
                        } else {
                            if (!$this->Utilities->isANote($good, 'purchaseorder')) {
                                // Se l'articolo non esiste
                                if ($this->Utilities->notExistInStorage($good['storage_id'])) {
                                    // Creo nuovo articolo magazzino
                                    $newStorage = $this->Purchaseorder->createNewStorageFromPurchaseorder($good, $purchaseorderGoodOldValues['Purchaseorder']['supplier_id']);
                                    // Aggiorno lo storage_id sul good
                                    $this->Purchaseorderrow->updateAll(['Purchaseorderrow.storage_id' => $newStorage['Storage']['id'], 'Purchaseorderrow.company_id' => MYCOMPANY], ['Purchaseorderrow.id' => $newPurchaseOrderRow['Purchaseorderrow']['id']]);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__('Preventivo salvato correttamente'), 'custom-flash');
                    $this->redirect(['action' => 'index']);
                } else {
                    $this->Session->setFlash(__('Il preventivo non è stato salvato correttamente.'), 'custom-danger');
                }
            } else {
                $this->request->data = $this->Purchaseorder->find('first', ['contain' => ['Supplier', 'Purchaseorderrow' => ['Storage']], 'recursive' => 2, 'conditions' => ['Purchaseorder.id' => $id]]);
            }

            if (ADVANCED_STORAGE_ENABLED) {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'true')); // Solo articoli di magazzino
            } else {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('units', $this->Units->getList());
            $this->loadModel('Supplier');
            $this->set('suppliers', $this->Supplier->getList());
            $this->set('vats', $this->Utilities->getVatsList());
            $this->set('nations', $this->Utilities->getNationsList());
            $this->loadModel('Constructionsite');
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->Render('edit');
        }
    }

    public function delete($id = null)
    {
        if (MODULO_CANTIERI) {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Purchaseorder', 'Messages']);
            $asg = ["l'", "ordine d'acquisto", "M"];
            if ($this->Purchaseorder->isHidden($id))
                throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Purchaseorder->find('first', ['conditions' => ['Purchaseorder.id' => $id, 'Purchaseorder.company_id' => MYCOMPANY]]);
            if ($this->Purchaseorder->hide($currentDeleted['Purchaseorder']['id']))
                $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
            return $this->redirect(['action' => 'index']);
        }
    }

    public function checkPurchaseorderDuplicate()
    {
        if (MODULO_CANTIERI) {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $A = $this->Purchaseorder->checkPurchaseOrderDuplicate($_POST['purchaseordernumber'], $_POST['date']);
            return json_encode($A);
        }
    }
}