<?php
App::uses('AppController', 'Controller');
require 'PHPMailer/PHPMailerAutoload.php';

class MaintenancesController extends AppController
{
    public $components = ['Mpdf'];

    public function index()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician']);

            $startDate = date('Y-m').'-01';
            $endDate = date('Y').'-12-31';

            $conditionsArray = ['Maintenance.company_id' => MYCOMPANY, 'maintenance_date >=' => $startDate, 'maintenance_date <=' => $endDate, 'Maintenance.state' => ATTIVO];
            $filterableFields = ['Maintenance_number', 'Technician__name',  '#htmlElements[0]', 'Client__ragionesociale', 'Constructionsite__name', 'Constructionsite__description', 'Maintenance__intervention_description', null];
            $sortableFields = [['maintenance_number', 'Numero'], ['Technician.name', 'Tecnico'], [null, 'Data'], ['Client.ragionesociale', 'Cliente'], ['Constructionsite.name', 'Cantiere'], ['Constructionsite.description', 'Descrizione cantiere'], ['intervention_description', 'Descrizione intervento'], ['#actions']];

            $technicianId = 0;
            $technicianFilter = false;

            if ($this->request->is('ajax') && isset($this->request->data['filters']))
            {
                if (isset($this->request->data['filters']['Technician__name']) && $this->request->data['filters']['Technician__name'] != '')
                {
                    $conditionsTechinician = [];
                    $arrayTechnician = explode(" ", $this->request->data['filters']['Technician__name']);

                    foreach($arrayTechnician as $technician)
                    {
                        $condition = [
                            'OR' => [
                                'name like' => '%'.$technician.'%',
                                'surname like' => '%'.$technician.'%',
                            ],
                        ];

                        array_push($conditionsTechinician, $condition);
                    }

                    $techinician = $this->Technician->find('first', ['conditions' => $conditionsTechinician]);

                    if($techinician != null)
                        $technicianId = $techinician['Technician']['id'];

                    $technicianFilter = true;

                    unset($this->request->data['filters']['Technician__name']);
                }

                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                    $conditionsArray['maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                    $conditionsArray['maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            if($technicianFilter && $technicianId == 0)
                $conditionsArray = ['Maintenance.id' => -1];

            if($technicianFilter && $technicianId != 0)
            {
                $maintenances = $this->Maintenance->find('all', [
                    'contain' => [
                        'Client',
                        'Constructionsite',
                        'Maintenancehour'=>['Maintenancehourstechnician' => ['Technician']]
                    ],
                    'conditions' => $conditionsArray,
                    'order' => ['Maintenance.maintenance_number' => 'desc', 'Maintenance.maintenance_date' => 'desc'],
                ]);

                $arrayMaintenanceIds = [];

                foreach($maintenances as $key => $maintenance)
                {
                    $exist = false;

                    foreach($maintenance['Maintenancehour'] as $maintenanceHour)
                    {
                        $conditionsMaintenancehourstechnician = ['Maintenancehourstechnician.maintenance_hour_id' => $maintenanceHour['id'], 'Maintenancehourstechnician.technician_id' => $technicianId, 'Maintenancehourstechnician.state >' => 0];
                        $maintenanceHoursTechnician = $this->Maintenancehourstechnician->find('all', ['conditions' => $conditionsMaintenancehourstechnician]);

                        if(count($maintenanceHoursTechnician) > 0)
                            $exist = true;
                    }

                    if($exist)
                        array_push($arrayMaintenanceIds, $maintenance['Maintenance']['id']);
                }

                if(count($arrayMaintenanceIds) > 0)
                    $conditionsArray = ['Maintenance.id IN' => $arrayMaintenanceIds];
                else
                    $conditionsArray = ['Maintenance.id' => -1];
            }

            $this->paginate = ['contain' => [
                'Client',
                'Constructionsite',
                'Maintenancehour'=>['Maintenancehourstechnician' => ['Technician']]
            ],
                'conditions' => $conditionsArray,
                'order' => ['Maintenance.maintenance_number' => 'desc', 'Maintenance.maintenance_date' => 'desc'],
                //'limit' => 20
            ];

            $this->set('maintenances', $this->paginate());
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function add()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Constructionsite', 'Quote', 'Units', 'Maintenancerow', 'Client', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician', 'Maintenancehoursoutsideoperator', 'Outsideoperator']);

            if ($this->request->is('post'))
            {
                $this->request->data['Maintenances']['company_id'] = MYCOMPANY;
                $this->request->data['Maintenances']['state'] = ATTIVO;
                $this->request->data['Maintenances']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenances']['maintenance_date']));

                // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                if (isset($this->request->data['Maintenances']['operator_id']))
                {
                    $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                    unset($this->request->data['Maintenances']['operator_id']);
                }

                if (isset($this->request->data['Maintenances']['technician_id']))
                {
                    $this->request->data['Hours'][0]['technician_id_multiple_select_'] = $this->request->data['Maintenances']['technician_id'];
                    unset($this->request->data['Maintenances']['technician_id']);
                }

                // Manage clonable/ddt/data
                if(isset($this->request->data['Ddt_0_ddtdate']))
                {
                    $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'];
                    unset($this->request->data['Ddt_0_ddtdate']);
                }

                if(isset($this->request->data['Maintenances']['rowsupplier_id']))
                {
                    $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'];
                    unset($this->request->data['Maintenances']['rowsupplier_id']);
                }

                foreach ($this->request->data['Ddt'] as $key => $ddt)
                {
                    if ($ddt['ddtdate'] != '')
                    {
                        $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                    }
                }

                $error = false;
                foreach ($this->request->data['Ddt'] as $key => $ddt)
                {
                    if($ddt['ddtnumber'] != '')
                    {
                        $conditions = [
                            'Maintenanceddt.number' => $ddt['ddtnumber'],
                            'YEAR(Maintenanceddt.date)' => date("Y",strtotime($ddt['ddtdate'])),
                            'Maintenanceddt.state >' => 0,
                            'Maintenanceddt.company_id' => MYCOMPANY
                        ];

                        $result = $this->Maintenanceddt->find('all', ['conditions' => $conditions]);

                        if(count($result) > 0)
                        {
                            $error = true;
                        }
                    }
                }

                if($error)
                {
                    $this->Session->setFlash(__("Esiste già un DDT di rifermiento con il numero inserito"), 'custom-danger');
                }
                else
                {
                    if ($newMaintenance = $this->Maintenance->save($this->request->data['Maintenances']))
                    {
                        foreach ($this->request->data['Good'] as $key => $good)
                        {
                            $newMaintenanceRow = $this->Maintenancerow->create();
                            $newMaintenanceRow['Maintenancerow']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                            $newMaintenanceRow['Maintenancerow']['codice'] = $good['codice'];
                            $newMaintenanceRow['Maintenancerow']['description'] = $good['description'];
                            $newMaintenanceRow['Maintenancerow']['customdescription'] = $good['customdescription'];
                            $newMaintenanceRow['Maintenancerow']['quantity'] = $good['quantity'];
                            $newMaintenanceRow['Maintenancerow']['unit_of_measure_id'] = $good['unit_of_measure_id'];
                            $newMaintenanceRow['Maintenancerow']['storage_id'] = $good['storage_id'];
                            $newMaintenanceRow['Maintenancerow']['company_id'] = MYCOMPANY;
                            $newMaintenanceRow['Maintenancerow']['state'] = ATTIVO;
                            $this->Maintenancerow->save($newMaintenanceRow);
                        }

                        foreach ($this->request->data['Ddt'] as $key => $ddt)
                        {
                            if($ddt['ddtnumber'] != '')
                            {
                                $newMaintenanceDdt = $this->Maintenanceddt->create();
                                $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                                $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                                $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                                $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                                $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                                $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                                $this->Maintenanceddt->save($newMaintenanceDdt);
                            }
                        }

                        /* Salvataggio ore */
                        if (isset($this->request->data['Hours']))
                        {
                            foreach ($this->request->data['Hours'] as $key => $hour)
                            {
                                $newMaintenanceHour = $this->Maintenancehour->create();

                                $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                                $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                                $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                                $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];

                                $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                                if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour))
                                {
                                    if($currentmaintenancehour['Maintenancehour']['partecipating'] == 1)
                                    {
                                        if (isset($hour['technician_id_multiple_select_']) && $hour['technician_id_multiple_select_'] != '')
                                        {
                                            foreach ($hour['technician_id_multiple_select_'] as $key => $technicianhour)
                                            {
                                                $newMaintenanceHourTechnician = $this->Maintenancehourstechnician->create();
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['maintenance_hour_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['technician_id'] = $technicianhour;
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['company_id'] = 1;
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['state'] = ATTIVO;
                                                $this->Maintenancehourstechnician->save($newMaintenanceHourTechnician);
                                            }
                                        }
                                    }

                                    if (isset($hour['operator_id_multiple_select_']) && $hour['operator_id_multiple_select_'] != '')
                                    {
                                        foreach ($hour['operator_id_multiple_select_'] as $key => $outsideoperatorhour)
                                        {
                                            $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = ATTIVO;
                                            $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                        }
                                    }
                                }

                            }
                        }

                        $this->Session->setFlash(__("Scheda d'intervento salvata"), 'custom-flash');
                        $this->redirect(['action' => 'index']);
                    }
                    else
                    {
                        $this->Session->setFlash(__("La scheda d'intervento non è stata salvata"), 'custom-danger');
                    }
                }
            }

            /* Passo list */
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('technicians', $this->Technician->getList());
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->set('quotes', $this->Quote->getList());
            $this->set('units', $this->Units->getList());
            $this->set('outsideoperators', $this->Outsideoperator->getList());

            // Carico i magazzini
            if (ADVANCED_STORAGE_ENABLED)
            {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            }
            else
            {
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
            }

            $this->set('clients', $this->Client->getCompleteList());
            $this->set('suppliers', $this->Supplier->getList());

            $this->set('maintenance_next_number', $this->Maintenance->getNextMaintenanceNumber(date("Y")));
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function edit($id = null)
    {
        if (MODULO_CANTIERI)
        {
            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY];
            $testMaintenance = $this->Maintenance->find('first',['conditions' => $conditionArray]);

            if ($testMaintenance['Maintenance']['sent'] == 0)
            {
                $this->loadModel('Utilities');
                $this->Utilities->loadModels($this, ['Maintenance', 'Constructionsite', 'Quote', 'Units', 'Client', 'Messages', 'Maintenancerow', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician', 'Maintenancehoursoutsideoperator', 'Outsideoperator']);
                $messageParameter = ["la", "scheda d'intervento", "F"];
                $this->Maintenance->id = $id;

                if (!$this->Maintenance->exists())
                    throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

                if ($this->request->is('post') || $this->request->is('put'))
                {
                    $this->request->data['Maintenance']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenance']['maintenance_date']));

                    foreach ($this->request->data['Hours'] as $key => $hour)
                    {
                        if (isset($hour['technician_id_multiple_select_']))
                        {
                            $this->request->data['Hours'][$key]['technician_id_multiple_select'] = $hour['technician_id_multiple_select_'];
                            unset($this->request->data['Hours'][$key]['technician_id_multiple_select_']);
                        }

                        if (isset($hour['operator_id_multiple_select_']))
                        {
                            $this->request->data['Hours'][$key]['operator_id_multiple_select'] = $hour['operator_id_multiple_select_'];
                            unset($this->request->data['Hours'][$key]['operator_id_multiple_select_']);
                        }
                    }

                    // Manage clonable/ddt/data
                    isset($this->request->data['Ddt_0_ddtdate']) ? $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'] : null;
                    isset($this->request->data['Maintenances']['rowsupplier_id']) ? $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'] : null;

                    $error = false;
                    if (isset($this->request->data['Ddt']))
                    {
                        foreach ($this->request->data['Ddt'] as $key => $ddt)
                        {
                            if ($ddt['ddtdate'] != '')
                            {
                                $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                            }
                        }

                        foreach ($this->request->data['Ddt'] as $key => $ddt)
                        {
                            if($ddt['ddtnumber'] != '')
                            {
                                $conditions = [
                                    'Maintenanceddt.number' => $ddt['ddtnumber'],
                                    'YEAR(Maintenanceddt.date)' => date("Y",strtotime($ddt['ddtdate'])),
                                    'Maintenanceddt.state >' => 0,
                                    'Maintenanceddt.company_id' => MYCOMPANY
                                ];

                                $result = $this->Maintenanceddt->find('all', ['conditions' => $conditions]);

                                if(count($result) > 0)
                                {
                                    //$error = true;
                                }
                            }
                        }
                    }

                    if($error)
                    {
                        $this->Session->setFlash(__("Esiste già un DDT di rifermiento con il numero inserito"), 'custom-danger');
                    }
                    else
                    {
                        if ($newMaintenance = $this->Maintenance->save($this->request->data))
                        {
                            //Storages
                            if(isset($this->request->data['Maintenancerow']))
                            {
                                $this->hideStorages($id);
                                foreach ($this->request->data['Maintenancerow'] as $Maintenancerow)
                                {
                                    $newMaintenanceRow = $this->Maintenancerow->create();
                                    $newMaintenanceRow = $Maintenancerow;
                                    $newMaintenanceRow['maintenance_id'] = $id;
                                    $newMaintenanceRow['state'] = ATTIVO;
                                    $newMaintenanceRow['company_id'] = MYCOMPANY;
                                    $this->Maintenancerow->save($newMaintenanceRow);
                                }
                            }

                            //Ddts
                            if (isset($this->request->data['Ddt']))
                            {
                                $this->hideDdts($id);
                                foreach ($this->request->data['Ddt'] as $key => $ddt)
                                {
                                    if($ddt['ddtnumber'] != '')
                                    {
                                        $newMaintenanceDdt = $this->Maintenanceddt->create();
                                        $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $id;
                                        $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                                        $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                                        $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                                        isset( $ddt['supplier_id_multiple_select']) ? $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'] : $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = null;
                                        $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                                        $this->Maintenanceddt->save($newMaintenanceDdt);
                                    }
                                }
                            }

                            // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                            if (isset($this->request->data['Maintenances']['operator_id']))
                            {
                                $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                            }

                            //Hours
                            if (isset($this->request->data['Hours']))
                            {
                                $this->hideHours($id);

                                foreach ($this->request->data['Hours'] as $key => $hour)
                                {
                                    $newMaintenanceHour = $this->Maintenancehour->create();

                                    $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $id;
                                    $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                                    $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                                    $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];
                                    $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                                    if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour))
                                    {
                                        if($currentmaintenancehour['Maintenancehour']['partecipating'] == 1)
                                        {
                                            if (isset($hour['technician_id_multiple_select']) && $hour['technician_id_multiple_select'] != '')
                                            {
                                                foreach ($hour['technician_id_multiple_select'] as $key => $technicianhour)
                                                {
                                                    $newMaintenanceHourTechnician = $this->Maintenancehourstechnician->create();
                                                    $newMaintenanceHourTechnician['Maintenancehourstechnician']['maintenance_hour_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                                    $newMaintenanceHourTechnician['Maintenancehourstechnician']['technician_id'] = $technicianhour;
                                                    $newMaintenanceHourTechnician['Maintenancehourstechnician']['company_id'] = 1;
                                                    $newMaintenanceHourTechnician['Maintenancehourstechnician']['state'] = ATTIVO;
                                                    $this->Maintenancehourstechnician->save($newMaintenanceHourTechnician);
                                                }
                                            }
                                        }

                                        if (isset($hour['operator_id_multiple_select']) && $hour['operator_id_multiple_select'] != '')
                                        {
                                            foreach ($hour['operator_id_multiple_select'] as $key => $outsideoperatorhour)
                                            {
                                                $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                                $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                                $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                                $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                                $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = ATTIVO;
                                                $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                            }
                                        }
                                    }

                                }
                            }

                            $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                            $this->redirect(['action' => 'index']);
                        }
                        else
                        {
                            $this->Session->setFlash(__($this->Messages->failOfupdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                        }
                    }
                }
                else
                {
                    $this->request->data = $this->Maintenance->find('first', ['contain' => ['Client', 'Maintenancerow' => ['Storage'], 'Maintenanceddt', 'Maintenancehour' => ['Maintenancehourstechnician', 'Maintenancehoursoutsideoperator']], 'recursive' => 2, 'conditions' => ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY]]);
                }

                // Carico i magazzini
                if (ADVANCED_STORAGE_ENABLED)
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
                else
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

                $this->set('outsideoperators', $this->Outsideoperator->getList());
                $this->set('suppliers', $this->Supplier->getList());
                $this->set('clients', $this->Client->getListAndSelected($this->request->data['Maintenance']['client_id']));
                $this->set('nations', $this->Utilities->getNationsList());
                $this->set('technicians', $this->Technician->getList());
                $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Maintenance']['constructionsite_id'], $this->Constructionsite->getClient($this->request->data['Maintenance']['constructionsite_id'])));
                $this->set('quotes', $this->Quote->getList());
                $this->set('units',  $this->Units->getList());
            }
            else
            {
                throw new MethodNotAllowedException(__('Sezione non abilitata'));
            }
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function delete($id = null)
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Messages', 'Maintenance', 'Maintenancehour', 'Maintenanceddt', 'Maintenancerow']);

            $messageParameter = ["la", "scheda d'intervento", "F"];

            if ($this->Maintenance->isHidden($id))
                throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

            $this->request->allowMethod(['post', 'delete']);

            $currentDeleted = $this->Maintenance->find('first', ['conditions' => ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY]]);

            if ($this->Maintenance->hide($currentDeleted['Maintenance']['id']))
            {
                $this->Session->setFlash(__($this->Messages->successOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');

                $this->hideHours($id);
                $this->hideDdts($id);
                $this->hideStorages($id);
            }
            else
                $this->Session->setFlash(__($this->Messages->failOfDelete($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');

            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function hideDdts($oldMaintenanceId)
    {
        $this->loadModel('Maintenanceddt');

        $newMaintenanceDdt = $this->Maintenanceddt->find('all', ['conditions' => ['Maintenanceddt.maintenance_id' => $oldMaintenanceId, 'Maintenanceddt.state >' => 0, 'Maintenanceddt.company_id' => MYCOMPANY]]);

        foreach ($newMaintenanceDdt as $maintenanceddt)
        {
            $this->Maintenanceddt->hide($maintenanceddt['Maintenanceddt']['id']);
        }
    }

    public function hideHours($oldMaintenanceId)
    {
        $this->loadModel('Maintenancehour');

        $maintenancehours = $this->Maintenancehour->find('all', ['conditions' => ['Maintenancehour.maintenance_id' => $oldMaintenanceId, 'Maintenancehour.state >' => 0, 'Maintenancehour.company_id' => MYCOMPANY]]);

        foreach ($maintenancehours as $maintenancehour)
        {
            $this->Maintenancehour->hide($maintenancehour['Maintenancehour']['id']);
            $this->hideHourOutsideoperators($maintenancehour['Maintenancehour']['id']);
            $this->hideHoursTechnician($maintenancehour['Maintenancehour']['id']);
        }
    }

    public function hideStorages($oldMaintenanceId)
    {
        $this->loadModel('Maintenancerows');

        $maintenancerows = $this->Maintenancerow->find('all', ['conditions' => ['Maintenancerow.maintenance_id' => $oldMaintenanceId, 'Maintenancerow.state >' => 0, 'Maintenancerow.company_id' => MYCOMPANY]]);

        foreach ($maintenancerows as $maintenancerow)
        {
            $this->Maintenancerow->hide($maintenancerow['Maintenancerow']['id']);
        }
    }

    public function hideHourOutsideoperators($oldMaintenanceHourId)
    {
        $this->loadModel('Maintenancehoursoutsideoperator');

        $maintenancehouroutsideoperators = $this->Maintenancehoursoutsideoperator->find('all', ['conditions' => ['Maintenancehoursoutsideoperator.maintenance_hours_id' => $oldMaintenanceHourId, 'Maintenancehoursoutsideoperator.state >' => 0, 'Maintenancehoursoutsideoperator.company_id' => MYCOMPANY]]);

        foreach ($maintenancehouroutsideoperators as $maintenancehouroutsideoperator)
        {
            $this->Maintenancehoursoutsideoperator->hide($maintenancehouroutsideoperator['Maintenancehoursoutsideoperator']['id']);
        }
    }

    public function hideHoursTechnician($oldMaintenanceHourId)
    {
        $this->loadModel('Maintenancehourstechnician');

        $maintenanceHoursTechnician = $this->Maintenancehourstechnician->find('all', ['conditions' => ['Maintenancehourstechnician.maintenance_hour_id' => $oldMaintenanceHourId, 'Maintenancehourstechnician.state >' => 0, 'Maintenancehourstechnician.company_id' => MYCOMPANY]]);

        foreach ($maintenanceHoursTechnician as $maintenanceHourTechnician)
        {
            $this->Maintenancehourstechnician->hide($maintenanceHourTechnician['Maintenancehourstechnician']['id']);
        }
    }

    public function checkDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Maintenance');
        $return = $this->Maintenance->checkDuplicate($_POST['maintenanceNumber'], $_POST['maintenanceDate']);
        return json_encode($return);
    }

    public function maintenancepdf($id)
    {
        if(MODULO_CANTIERI)
        {
            $this->layout = 'pdf';
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Setting', 'Currencies']);

            $settings = $this->Setting->GetMySettings(MYCOMPANY);
            $this->set('settings', $settings);



            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state >' => 0];
            $maintencance = $this->Maintenance->find('first',['conditions' => $conditionArray, 'recursive' => '3']);

            $this->set('maintenance',$maintencance);
            $numero = $maintencance['Maintenance']['maintenance_number'];
            $title = "Scheda d intervento n. $numero";
            $filename = "scheda_intervento_$numero";
            $this->Pdf->setMpdf($title, $filename, $this->Mpdf);
            $this->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
            $this->set('pdfHeader', $this->Pdf->getMaintenanceHeader());
            $this->set('pdfBody', $this->Pdf->getMaintenanceBody());
            $this->set('pdfFooter', $this->Pdf->getMaintenanceFooter());
        }
    }

    public function sendMaintenanceMail($id)
    {
        if (MODULO_CANTIERI)
        {
            $this->autoRender = false;
            $this->loadModel('Utilities');
            $id = $this->params['pass'][0];

            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state >' => 0];
            $maintenance = $this->Maintenance->find('first', ['conditions' => $conditionArray, 'recursive' => '2']);
            $settings = $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY]]);

            $currentDbName = ConnectionManager::getDataSource('default')->config['database'];

            $message = "<h3>Cordiale " . $maintenance['Maintenance']['handling'] . "</h3><br/> In allegato potete trovare la scheda d'intervento. <br/><br/>Cordiali Saluti";
            $okMessage = 'La scheda d\'intervento è stata spedita correttamente via mail al cliente';

            try
            {
                $mail = new PHPMailer;
                $mail->isSMTP();
                $mail->Host = $settings['Setting']['host'];
                $mail->SMTPAuth = true;
                $mail->Username = $settings['Setting']['username']; // SMTP username
                $mail->Password = $this->Utilities->mydecrypt($settings['Setting']['password'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');
                $mail->SMTPSecure = $settings['Setting']['smtpSecure']; // Enable TLS encryption, `ssl` also accepted
                $mail->Port = $settings['Setting']['port']; // TCP port to connect to

                if (isset($settings['Setting']['email']) && $settings['Setting']['email'] != '')
                    $mail->SetFrom($settings['Setting']['email'], $settings['Setting']['name']);
                else
                    $mail->SetFrom($settings['Setting']['username'], $settings['Setting']['name']);

                $mail->Timeout = 5;
                $mail->addAddress($maintenance['Maintenance']['email']); // Add a recipient
                $mail->isHTML(true); // Set email format to HTML
                $mail->Subject = $settings['Setting']['name'] . ' Invio scheda d\'intervento a: ' . $maintenance['Maintenance']['handling'];
                $mail->Body = $message;

                $createdFileName = $this->createMailMaintenancePdf($id);

                $mail->addAttachment($createdFileName);

                if ($mail->send() == 1)
                {
                    $this->Maintenance->setSent($maintenance['Maintenance']['id']);
                    $this->Session->setFlash(($okMessage), 'custom-flash');
                }
                else
                {
                    $this->Session->setFlash(("Errore durante l'invio della scheda d'intevento, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
                }
            }
            catch (Exception $ecc)
            {
                $this->Session->setFlash(($ecc), 'custom-danger');
                $this->Session->setFlash(("Errore durante l'invio della scheda d'intervento, controllare i dati di configurazione dell'smtp nella sezione impostazioni"), 'custom-danger');
            }

            unlink($createdFileName);
            $this->redirect($this->referer());
        }
    }

    public function createMailMaintenancePdf($id)
    {
        if(MODULO_CANTIERI)
        {
            $view = new View(null, false);

            /* Questo non ha crypt e decrypt e restituisce un file da scaricare */
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
            error_reporting(0);

            $nomefile = str_replace(' ', '_', $id);

            $this->layout = 'pdf';
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Configuration', 'Pdf', 'Client', 'Transport', 'Setting', 'Currencies']);

            $configuration = array('win-1252', 'A4', 0, 0, 0, 0, 0, 0, 0, 0);
            $this->Mpdf->init($configuration);
            $this->Mpdf->showImageErrors = true;
            $this->Mpdf->setFilename($nomefile . '.pdf');
            $this->Mpdf->SetTitle("Fattura");
            $this->Mpdf->SetAuthor("");
            $this->Mpdf->SetDisplayMode('fullpage');

            $maintenanceFileName = APP . 'tmp' . DS . 'scheda_intervento_' . time() . '_' . $id . '.pdf';

            $viewPath = 'Maintenances';
            $view->viewPath = $viewPath;  //folder to look in in Views directory
            $settings = $this->Setting->GetMySettings(MYCOMPANY);

            $view->Set('companyLogo', $this->Setting->getCompanyLogo()['Setting']['header']);
            $view->set('settings', $settings);

            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state >' => 0];
            $maintencance = $this->Maintenance->find('first', ['conditions' => $conditionArray, 'recursive' => '2']);

            $view->set('maintenance', $maintencance);
            $view->set('pdfHeader', $this->Pdf->getMaintenanceHeader());
            $view->set('pdfBody', $this->Pdf->getMaintenanceBody());
            $view->set('pdfFooter', $this->Pdf->getMaintenanceFooter());

            $output = $view->render('maintenancepdf', 'pdf');

            $this->Mpdf->WriteHTML($output);
            $this->Mpdf->Output($maintenanceFileName, 'F');
            return $maintenanceFileName;
        }
    }

    /** Viste per i tecnici */

    public function technicianIndex()
    {
        if (MODULO_CANTIERI)
        {
            $this->loadModel('User');
            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician']);

            $currentUser = $this->User->find('first', ['conditions' => ['User.company_id' => $_SESSION['MYCOMPANY'], 'username' => $_SESSION['Auth']['User']['username']]]);
            isset($this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id']) ? $currentTechinicianId = $this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id'] : null;

            $startDate = date('Y-m').'-01';
            $endDate = date('Y').'-12-31';

            $conditionsArray = ['Maintenance.company_id' => MYCOMPANY, 'Maintenance.state >' => 0, 'maintenance_date >=' => $startDate, 'maintenance_date <=' => $endDate];

            $filterableFields = ['maintenance_number', '#htmlElements[0]', 'Client__ragionesociale', 'Constructionsite__name', 'Constructionsite__description', 'Maintenance__intervention_description', null];
            $sortableFields = [['maintenance_number', 'Numero'], [null, 'Data'], ['Client.ragionesociale', 'Cliente'], ['Constructionsite.name', 'Cantiere'], ['Constructionsites.description', 'Descrizione cantiere'], ['Maintenance.intervention_description', 'Descrizione intervento'], ['#actions']];

            if ($this->request->is('ajax') && isset($this->request->data['filters']))
            {
                $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

                if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                    $conditionsArray['maintenance_date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

                if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                    $conditionsArray['maintenance_date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));
            }

            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            $maintenances = $this->Maintenance->find('all', ['conditions' => $conditionsArray, 'order' => ['Maintenance.maintenance_date' => 'desc', 'Maintenance.maintenance_number' => 'desc']]);

            $arrayMaintenanceIds = [];

            foreach($maintenances as $maintenance)
            {
                $exist = false;

                $conditionsMaintenanceHours = ['Maintenancehour.maintenance_id' => $maintenance['Maintenance']['id'], 'Maintenancehour.state >' => 0];
                $maintenanceHours = $this->Maintenancehour->find('all', ['conditions' => $conditionsMaintenanceHours]);

                foreach($maintenanceHours as $maintenanceHour)
                {
                    $conditionsMaintenancehourstechnician = ['Maintenancehourstechnician.maintenance_hour_id' => $maintenanceHour['Maintenancehour']['id'], 'Maintenancehourstechnician.technician_id' => $currentTechinicianId, 'Maintenancehourstechnician.state >' => 0];
                    $maintenanceHoursTechnician = $this->Maintenancehourstechnician->find('all', ['conditions' => $conditionsMaintenancehourstechnician]);

                    if(count($maintenanceHoursTechnician) > 0)
                        $exist = true;
                }

                if($exist)
                    array_push($arrayMaintenanceIds, $maintenance['Maintenance']['id']);

            }

            if(count($arrayMaintenanceIds) > 0)
                $this->paginate = ['conditions' => ['Maintenance.id IN' => $arrayMaintenanceIds], 'order' => 'maintenance_date desc, maintenance_number desc'];
            else
                $this->paginate = ['conditions' => ['Maintenance.id' => -1], 'order' => 'maintenance_date desc, maintenance_number desc'];

            $this->set('maintenances', $this->paginate());
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function technicianAdd()
    {
        if (MODULO_CANTIERI)
        {
            $currentUser = $this->User->find('first', ['conditions' => ['User.company_id' => $_SESSION['MYCOMPANY'], 'username' => $_SESSION['Auth']['User']['username']]]);
            isset($this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id']) ? $currentTechinicianId = $this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id'] : null;

            $this->loadModel('Utilities');
            $this->Utilities->loadModels($this, ['Maintenance', 'Constructionsite', 'Quote', 'Units', 'Maintenancerow', 'Client', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician', 'Maintenancehoursoutsideoperator', 'Outsideoperator']);

            if ($this->request->is('post'))
            {
                $this->request->data['Maintenances']['company_id'] = MYCOMPANY;
                $this->request->data['Maintenances']['state'] = ATTIVO;
                $this->request->data['Maintenances']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenances']['maintenance_date']));

                // Manage clonable/ddt/data

                if(isset($this->request->data['Ddt_0_ddtdate']))
                {
                    $this->request->data['Ddt'][0]['ddtdate'] = $this->request->data['Ddt_0_ddtdate'];
                    unset($this->request->data['Ddt_0_ddtdate']);
                }

                if(isset($this->request->data['Maintenances']['rowsupplier_id']))
                {
                    $this->request->data['Ddt'][0]['supplier_id_multiple_select'] = $this->request->data['Maintenances']['rowsupplier_id'];
                    unset($this->request->data['Maintenances']['rowsupplier_id']);
                }

                foreach ($this->request->data['Ddt'] as $key => $ddt)
                {
                    if ($ddt['ddtdate'] != '')
                        $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                }

                if (isset($this->request->data['Maintenances']['maintenance_hour_from']))
                    $this->request->data['Maintenances']['maintenance_hour_from'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_from']));

                if (isset($this->request->data['Maintenances']['maintenance_hour_to']))
                    $this->request->data['Maintenances']['maintenance_hour_to'] = date("H:i:s", strtotime($this->request->data['Maintenances']['maintenance_hour_to']));

                // Fix Muliselect nelle ore (andrebbe modificato nella vista ma ci metto troppo tempo e ho scadenza
                if (isset($this->request->data['Maintenances']['operator_id']))
                {
                    $this->request->data['Hours'][0]['operator_id_multiple_select_'] = $this->request->data['Maintenances']['operator_id'];
                    unset($this->request->data['Maintenances']['operator_id']);
                }

                if (isset($this->request->data['Maintenances']['technician_id']))
                {
                    $this->request->data['Hours'][0]['technician_id_multiple_select_'] = $this->request->data['Maintenances']['technician_id'];
                    unset($this->request->data['Maintenances']['technician_id']);
                    //$this->request->data['Maintenances']['technician_id'] = $this->request->data['Maintenances']['technician_id'][0];
                }

                if ($newMaintenance = $this->Maintenance->save($this->request->data['Maintenances']))
                {
                    /* Salvataggio articoli */
                    foreach ($this->request->data['Good'] as $key => $good)
                    {
                        if($good['codice'] != '' || $good['description'] != '')
                        {
                            $newMaintenanceRow = $this->Maintenancerow->create();
                            $newMaintenanceRow['Maintenancerow']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                            $newMaintenanceRow['Maintenancerow']['codice'] = $good['codice'];
                            $newMaintenanceRow['Maintenancerow']['description'] = $good['description'];
                            $newMaintenanceRow['Maintenancerow']['customdescription'] = $good['customdescription'];
                            $newMaintenanceRow['Maintenancerow']['quantity'] = $good['quantity'];
                            $newMaintenanceRow['Maintenancerow']['unit_of_measure_id'] = $good['unit_of_measure_id'];
                            $newMaintenanceRow['Maintenancerow']['storage_id'] = $good['storage_id'];
                            $newMaintenanceRow['Maintenancerow']['company_id'] = MYCOMPANY;
                            $newMaintenanceRow['Maintenancerow']['state'] = ATTIVO;
                            $this->Maintenancerow->save($newMaintenanceRow);
                        }
                    }

                    /* Salvataggio bolle */
                    foreach ($this->request->data['Ddt'] as $key => $ddt)
                    {
                        if(isset($ddt['supplier_id_multiple_select']) && $ddt['supplier_id_multiple_select'] != '')
                        {
                            $newMaintenanceDdt = $this->Maintenanceddt->create();
                            $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                            $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'];
                            $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                            $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                            $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                            $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                            $this->Maintenanceddt->save($newMaintenanceDdt);
                        }
                    }

                    /* Salvataggio ore */
                    foreach ($this->request->data['Hours'] as $key => $hour)
                    {
                        $newMaintenanceHour = $this->Maintenancehour->create();
                        $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $newMaintenance['Maintenance']['id'];
                        $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                        $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                        $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating']; // Presenza del tecnico
                        $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                        if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour))
                        {
                            if($currentmaintenancehour['Maintenancehour']['partecipating'] == 1)
                            {
                                if (isset($hour['technician_id_multiple_select_']) && $hour['technician_id_multiple_select_'] != '')
                                {
                                    foreach ($hour['technician_id_multiple_select_'] as $key => $technicianhour)
                                    {
                                        $newMaintenanceHourTechnician = $this->Maintenancehourstechnician->create();
                                        $newMaintenanceHourTechnician['Maintenancehourstechnician']['maintenance_hour_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                        $newMaintenanceHourTechnician['Maintenancehourstechnician']['technician_id'] = $technicianhour;
                                        $newMaintenanceHourTechnician['Maintenancehourstechnician']['company_id'] = 1;
                                        $newMaintenanceHourTechnician['Maintenancehourstechnician']['state'] = ATTIVO;
                                        $this->Maintenancehourstechnician->save($newMaintenanceHourTechnician);
                                    }
                                }
                            }

                            if (isset($hour['operator_id_multiple_select_']) && $hour['operator_id_multiple_select_'] != '')
                            {
                                foreach ($hour['operator_id_multiple_select_'] as $key => $outsideoperatorhour)
                                {
                                    $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                    $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = ATTIVO;
                                    $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                }
                            }
                        }
                    }

                    $this->Session->setFlash(__("Scheda d'intervento salvata"), 'custom-flash');
                    $this->redirect(['action' => 'technicianindex']);
                }
                else
                {
                    $this->Session->setFlash(__("La scheda d'intervento non è stata salvata"), 'custom-danger');
                }
            }

            /* Passo list */
            $this->set('nations', $this->Utilities->getNationsList());
            $this->set('technicians', $this->Technician->getList());
            $this->set('outsideoperators', $this->Outsideoperator->getList());

            $this->set('quotes', $this->Quote->getList());
            $this->set('units', $this->Units->getList());
            $this->set('suppliers', $this->Supplier->getList());

            $this->set('currentTechinicianId', $currentTechinicianId);
            $this->set('technician', $this->Technician->getSurnameAndName($currentTechinicianId));

            // Carico i magazzini
            if (ADVANCED_STORAGE_ENABLED)
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            else
                $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

            $this->set('clients', $this->Client->getCompleteList());
            $this->set('constructionsites', $this->Constructionsite->getList());
            $this->set('maintenance_next_number', $this->Maintenance->getNextMaintenanceNumber(date("Y")));
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }

    public function technicianEdit($id)
    {
        if (MODULO_CANTIERI)
        {
            $conditionArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY];
            $testMaintenance = $this->Maintenance->find('first', ['conditions'=>$conditionArray]);

            if ($testMaintenance['Maintenance']['sent'] == 0 && date_diff(date_create($testMaintenance['Maintenance']['maintenance_date']), date_create(date('Y-m-d')))->format('%d') < 8)
            {
                $currentUser = $this->User->find('first', ['conditions' => ['User.company_id' => $_SESSION['MYCOMPANY'], 'username' => $_SESSION['Auth']['User']['username']]]);
                isset($this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id']) ? $currentTechinicianId = $this->Technician->find('first', ['conditions' => ['user_id' => $currentUser['User']['id']]])['Technician']['id'] : null;

                $this->loadModel('Utilities');
                $this->Utilities->loadModels($this, ['Maintenance', 'Constructionsite', 'Quote', 'Units', 'Client', 'Messages', 'Maintenancerow', 'Supplier', 'Maintenanceddt', 'Maintenancehour', 'Maintenancehourstechnician', 'Technician', 'Maintenancehoursoutsideoperator', 'Outsideoperator']);
                $messageParameter = ["la", "scheda d'intervento", "F"];
                $this->Maintenance->id = $id;

                if (!$this->Maintenance->exists())
                    throw new Exception($this->Messages->notFound($messageParameter[0], $messageParameter[1], $messageParameter[2]));

                if ($this->request->is('post') || $this->request->is('put'))
                {
                    $this->request->data['Maintenance']['maintenance_date'] = date("Y-m-d", strtotime($this->request->data['Maintenance']['maintenance_date']));

                    // Manage clonable/ddt/data
                    if (isset($this->request->data['Ddt']))
                    {
                        foreach ($this->request->data['Ddt'] as $key => $ddt)
                        {
                            if ($ddt['ddtdate'] != '')
                            {
                                $this->request->data['Ddt'][$key]['ddtdate'] = date('Y-m-d', strtotime($ddt['ddtdate']));
                            }
                        }
                    }

                    foreach ($this->request->data['Hours'] as $key => $hour)
                    {
                        if (isset($hour['technician_id_multiple_select_']))
                        {
                            $this->request->data['Hours'][$key]['technician_id_multiple_select'] = $hour['technician_id_multiple_select_'];
                            unset($this->request->data['Hours'][$key]['technician_id_multiple_select_']);
                        }

                        if (isset($hour['operator_id_multiple_select_']))
                        {
                            $this->request->data['Hours'][$key]['operator_id_multiple_select'] = $hour['operator_id_multiple_select_'];
                            unset($this->request->data['Hours'][$key]['operator_id_multiple_select_']);
                        }
                    }

                    if ($newMaintenance = $this->Maintenance->save($this->request->data))
                    {
                        if (isset($this->request->data['Maintenancerow']))
                        {
                            $this->hideStorages($id);
                            foreach ($this->request->data['Maintenancerow'] as $Maintenancerow)
                            {
                                $newMaintenanceRow = $this->Maintenancerow->create();
                                $newMaintenanceRow = $Maintenancerow;
                                $newMaintenanceRow['maintenance_id'] = $id;
                                $newMaintenanceRow['state'] = ATTIVO;
                                $newMaintenanceRow['company_id'] = MYCOMPANY;
                                $this->Maintenancerow->save($newMaintenanceRow);
                            }
                        }

                        if (isset($this->request->data['Ddt']))
                        {
                            $this->hideDdts($id);
                            foreach ($this->request->data['Ddt'] as $key => $ddt)
                            {
                                if(isset($ddt['supplier_id_multiple_select']) && $ddt['supplier_id_multiple_select'] != '')
                                {
                                    $newMaintenanceDdt = $this->Maintenanceddt->create();
                                    $newMaintenanceDdt['Maintenanceddt']['maintenance_id'] = $id;
                                    isset($ddt['ddtnumber']) ? $newMaintenanceDdt['Maintenanceddt']['number'] = $ddt['ddtnumber'] : $newMaintenanceDdt['Maintenanceddt']['number'] = '';
                                    $newMaintenanceDdt['Maintenanceddt']['value'] = $ddt['ddtvalue'];
                                    $newMaintenanceDdt['Maintenanceddt']['date'] = $ddt['ddtdate'];
                                    $newMaintenanceDdt['Maintenanceddt']['supplier_id'] = $ddt['supplier_id_multiple_select'];
                                    $newMaintenanceDdt['Maintenanceddt']['company_id'] = MYCOMPANY;
                                    $newMaintenanceDdt['Maintenanceddt']['state'] = ATTIVO;
                                    $this->Maintenanceddt->save($newMaintenanceDdt);
                                }
                            }
                        }

                        if (isset($this->request->data['Hours']))
                        {
                            $this->hideHours($id);
                            foreach ($this->request->data['Hours'] as $key => $hour)
                            {
                                $newMaintenanceHour = $this->Maintenancehour->create();
                                $newMaintenanceHour['Maintenancehour']['maintenance_id'] = $id;
                                $newMaintenanceHour['Maintenancehour']['hour_from'] = $hour['maintenance_hour_from'];
                                $newMaintenanceHour['Maintenancehour']['hour_to'] = $hour['maintenance_hour_to'];
                                $newMaintenanceHour['Maintenancehour']['partecipating'] = $hour['maintenance_partecipating'];
                                $newMaintenanceHour['Maintenancehour']['company_id'] = MYCOMPANY;

                                if ($currentmaintenancehour = $this->Maintenancehour->save($newMaintenanceHour))
                                {
                                    if($currentmaintenancehour['Maintenancehour']['partecipating'] == 1)
                                    {
                                        if (isset($hour['technician_id_multiple_select']) && $hour['technician_id_multiple_select'] != '')
                                        {
                                            foreach ($hour['technician_id_multiple_select'] as $key => $technicianhour)
                                            {
                                                $newMaintenanceHourTechnician = $this->Maintenancehourstechnician->create();
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['maintenance_hour_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['technician_id'] = $technicianhour;
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['company_id'] = 1;
                                                $newMaintenanceHourTechnician['Maintenancehourstechnician']['state'] = ATTIVO;
                                                $this->Maintenancehourstechnician->save($newMaintenanceHourTechnician);
                                            }
                                        }
                                    }

                                    if (isset($hour['operator_id_multiple_select']) && $hour['operator_id_multiple_select'] != '')
                                    {
                                        foreach ($hour['operator_id_multiple_select'] as $key => $outsideoperatorhour)
                                        {
                                            $newMaintenanceHourOutsideOperator = $this->Maintenancehoursoutsideoperator->create();
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['operator_id'] = $outsideoperatorhour;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['maintenance_hours_id'] = $currentmaintenancehour['Maintenancehour']['id'];
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['company_id'] = 1;
                                            $newMaintenanceHourOutsideOperator['Maintenancehoursoutsideoperator']['state'] = ATTIVO;
                                            $this->Maintenancehoursoutsideoperator->save($newMaintenanceHourOutsideOperator);
                                        }
                                    }
                                }
                            }
                        }

                        $this->Session->setFlash(__($this->Messages->successOfUpdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-flash');
                        $this->redirect(['action' => 'technicianindex']);
                    }
                    else
                    {
                        $this->Session->setFlash(__($this->Messages->failOfupdate($messageParameter[0], $messageParameter[1], $messageParameter[2])), 'custom-danger');
                    }
                }
                else
                {
                    $conditionsArray = ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY];
                    $this->request->data = $this->Maintenance->find('first', ['contain' => ['Client', 'Maintenancerow' => ['Storage'], 'Maintenanceddt', 'Maintenancehour' => ['Maintenancehourstechnician', 'Maintenancehoursoutsideoperator']], 'recursive' => 2, 'conditions' => $conditionsArray]);
                }

                // Carico i magazzini
                if (ADVANCED_STORAGE_ENABLED)
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
                else
                    $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

                $this->set('clients', $this->Client->getListAndSelected($this->request->data['Maintenance']['client_id']));
                $this->set('constructionsites', $this->Constructionsite->getListAndSelected($this->request->data['Maintenance']['constructionsite_id'], $this->Constructionsite->getClient($this->request->data['Maintenance']['constructionsite_id'])));
                $this->set('outsideoperators', $this->Outsideoperator->getList());
                $this->set('nations', $this->Utilities->getNationsList());
                $this->set('technicians', $this->Technician->getList());
                $this->set('technician', $this->Technician->getSurnameAndName($currentTechinicianId));
                $this->set('quotes', $this->Quote->getList());
                $this->set('units', $this->Units->getList());
                $this->set('suppliers', $this->Supplier->getList());
            }
        }
        else
        {
            throw new MethodNotAllowedException(__('Sezione non abilitata'));
        }
    }
}
