<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'mpdf/mpdf');
App::import('Vendor', 'tpmacn/csvhelper');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('Routing','Router');
App::uses('Xml', 'Utility');

require 'PHPMailer/PHPMailerAutoload.php';

class OrdersController extends AppController
{
    public $components = ['Mpdf'];
    var $uses = ['Order', 'Client', 'Good', 'Iva', 'Setting', 'Storage', 'User', 'Company', 'Payment'];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Order']);

        // Recupero l'array degli stati per IXFE

        $conditionsArray = ['Order.company_id' => MYCOMPANY, 'Order.state <> ' => 0, 'date >= ' => date("Y-m") . '-01', 'date <= ' => date("Y") . '-12-31'];
        $conditionsArray2 = ['Orderrow.state' => ATTIVO, 'Orderrow.company_id'=>MYCOMPANY, '1 = 1'];

        $filterableFields = ['numero_ordine', '#htmlElements[0]', 'ragionesociale', 'descrizioni', null, null, '#htmlElements[1]','#htmlElements[2]',null,'#htmlElements[3]','#htmlElements[4]'];

        $sortableFields = [['numero_ordine', 'N° Ordine'], ['date', 'Data'], ['Client.ragionesociale', 'Cliente'], [null, 'Descrizione articoli'], [null, 'Imponibile'], [null, 'Tot. Ivato'], [null, 'Tipo Ordine'],[null, 'Metodo di pagamento'], ['#actions'], [null, 'Agente'],['Order.bill_id', 'Fatturato']];

        // Filtri automatici
        $automaticFilter = $this->Session->read('arrayOfFilters');
        if (isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false)
        {
            $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action];
            $startDate = $this->request->data['filters']['date1'];
            $endDate = $this->request->data['filters']['date2'];
        }
        else
        {
            $startDate = '01'.date("-m-Y");
            $endDate = '31-12-'.date("Y");
        }

        $this->set('startDate', $startDate);
        $this->set('endDate', $endDate);

        $stati = [0=>"TUTTI", 1=>"PAGATO",2=>"DA PAGARE"];
        $this->set("stati", $stati);
        if (($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
        {
            $conditionsArray = ['Order.company_id' => MYCOMPANY, 'Order.state <> ' => 0];
            $conditionsArray['Order.numero_ordine like'] = $this->request->data['filters']['numero_ordine'] . '%';
            if($this->request->data['filters']['tipologia'] != 0)
                $conditionsArray['Order.tipologia'] = $this->request->data['filters']['tipologia'];

            if($this->request->data['filters']['agenti'] != 0)
                $conditionsArray['Order.agent_id'] = $this->request->data['filters']['agenti'];

            if($this->request->data['filters']['payment'] == 2)
                $conditionsArray['Order.payment_id <>'] = 36;
            else if($this->request->data['filters']['payment']){
                $conditionsArray['Order.payment_id'] = 36;

            }

            if (isset($this->request->data['filters']['bill_id']) && $this->request->data['filters']['bill_id'] != '')
            {
                switch ($this->request->data['filters']['bill_id'])
                {
                    case '1':
                        $conditionsArray['Order.bill_id'] = null;
                    break;
                    case '2':
                        $conditionsArray['Order.bill_id <>'] = 0;
                    break;
                }
            }

            if (isset($this->request->data['filters']['descrizioni']) && $this->request->data['filters']['descrizioni'] != '')
                $conditionsArray2 = [
                    'OR' => [
                        'Orderrow.oggetto like' => '%' . $this->request->data['filters']['descrizioni'] . '%',]
                ];

            if (isset($this->request->data['filters']['ragionesociale']) && $this->request->data['filters']['ragionesociale'] != '')
                $conditionsArray['ragionesociale like'] = '%' . $this->request->data['filters']['ragionesociale'] . '%';

            if (isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
                $conditionsArray['date >='] = date('Y-m-d', strtotime($this->request->data['filters']['date1']));

            if (isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
                $conditionsArray['date <='] = date('Y-m-d', strtotime($this->request->data['filters']['date2']));

            $arrayFilterableForSession = $this->Session->read('arrayOfFilters');
            $arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
            $this->Session->write('arrayOfFilters', $arrayFilterableForSession);
        }

        // Seleziono tutto
        $orders = $this->Order->find('all', ['contain' => [
            'Client' => 'Bank',
            'Orderrow' => ['conditions' => $conditionsArray2, 'fields' => ['Orderrow.id', 'Orderrow.quantita', 'Orderrow.prezzo', 'Orderrow.iva_id', 'Orderrow.discount', 'Orderrow.Oggetto', 'Orderrow.complimentaryQuantity']],
            'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
        ],
            'conditions' => $conditionsArray,
            'order' => ['Order.date' => 'desc', 'Order.id' => 'desc'],
        ]);

        // Aggiorno il token prima di ciclare le fatture
        if (MODULE_IXFE)
        {
            $this->Utilities->resetSettingsIXFE();
            $arrayForIXFEBill = [];
        }

        // Rielaboro per filtrare
        $arrayOrders = [];


        foreach ($orders as $key => $B)
        {
            if(count($B['Orderrow']) != 0 || !isset($this->request->data['filters']['descrizioni']) || $this->request->data['filters']['descrizioni'] == '')
            {
                $arrayOrders[] = $B['Order']['id'];
            }

        }


        if ($arrayOrders != null)
            $conditionsArray['Order.id IN'] = $arrayOrders;
        else
            $conditionsArray['Order.id'] = -1;
        // E Poi pagino
        $this->paginate = ['contain' => [
            'Client',
            'Orderrow' => ['fields' => ['Orderrow.id', 'Orderrow.quantita', 'Orderrow.prezzo', 'Orderrow.iva_id', 'Orderrow.discount', 'Orderrow.Oggetto', 'Orderrow.complimentaryQuantity']],
            'Payment' => ['fields' => ['Payment.id', 'Payment.metodo', 'Payment.banca', 'Payment.iban']],
        ],
            'conditions' => $conditionsArray,
            'order' => ['Order.date' => 'desc', 'Order.id' => 'desc'],
            'limit' => 20
        ];

        $this->set('tipologia',['0'=>'Tutti','1'=>'Ordine', '2'=>'Tentata Vendita']);
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047")
        {
            $this->set('agenti', ['0'=>'Tutti','1'=>'Simone Adoni','2'=>'Fabio Ronconi','3'=>'Lorenzo Gianola']);
        }
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0041")
        {
            $this->set('agenti',['0'=>'Tutti','4'=>'Fabio', '5'=>'Ronconi']);
        }

        $fatturato = [0=>"Tutti", 1=>"Non Ancora fatturato", 2=>"Gia fatturato"];
        $this->set('fatturato', $fatturato); //bill_id

        $this->set('filterableFields', $filterableFields);
        $this->set('sortableFields', $sortableFields);
        $this->set('orders', $this->paginate());
        $this->set('Utilities', $this->Utilities);
        $this->set('sectionals', $this->Utilities->getSectionalsArray());
        $this->set('agents', $this->Utilities->getAgentsArray());
        $this->render('index');
    }

    public function edit($id = null, $tipologia = 1)
    {
        ini_set('max_execution_time', 600);
        set_time_limit(600);

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client', 'Storage', 'Orderrow', 'Ivas', 'Units', 'Clientdestination', 'Setting', 'Currencies']);

        $this->Utilities->setMenuAsActive("Ordini di vendita"); // Fix per il menu

        $this->Order->id = $id;

        if (!$this->Order->exists())
            throw new NotFoundException(__('Ordine non valido'));

        if ($tipologia == 1)
        {
            $redirect = 'index';
            $this->set('sectionals', $this->Utilities->getOrderSectionalList());
        }



        if ($this->request->is('post') || $this->request->is('put') || $this->request->is('patch'))
        {
            $old_date = $this->Order->find('first', ['conditions' => ['Order.id' => $id, 'Order.company_id' => MYCOMPANY]]);

            if ($_POST['data']['Order']['date'] == '' || $_POST['data']['Order']['date'] == '01-01-1970')
                $this->request->data['Order']['date'] = $old_date['Order']['date'];
            else
                $this->request->data['Order']['date'] = date("Y-m-d", strtotime(str_replace('/', '-', $this->request->data['Order']['date'])));

            /** INIZIA MODIFICA CLIENTE */

            // Controllo ragione sociale se viene cambiato cliente
            if ($_POST['data']['Order']['client_id'] != $old_date['Order']['client_id'])
                $this->request->data['Order']['client_name'] = $this->Utilities->getCustomerName($_POST['data']['Order']['client_id']);


            // Recupero i vecchi articoli della bolla
            $oldRow = $this->Orderrow->find('all', ['conditions' => ['order_id' => $id, 'Orderrow.company_id'=>MYCOMPANY, 'Orderrow.state'=>ATTIVO]]);
            // Se è selezionato alternative address lo salvo come destino


            $settings = $this->Setting->GetMySettings();

            // Aggiorno lo stato ad 1 lo utilizzo per il duplica che di default è 0
            $this->request->data['Order']['state'] = 1;

            if ($newOrder = $this->Order->save($this->request->data['Order']))
            {
                $newOrder = $this->Order->find('first', ['conditions' => ['Order.id' => $newOrder['Order']['id']]]);

                // Per ogni articolo vecchio scarico da magazzino
                $numberOfMovement = 0;

                foreach ($oldRow as $oldOrderRow)
                {
                    $numberOfMovement++;

                    // Se è una fattura ed è accompagnatoria
                    if ($newOrder['Order']['tipologia'] == 1 )
                    {
                        if ($oldOrderRow['Orderrow']['storage_id'] != null) // Altrimenti caricherebbe anche gli articoli non a magazzino
                        {
                            if (ADVANCED_STORAGE_ENABLED)
                            {
                                if (isset($oldOrderRow['Orderrow']['storage_id']))
                                {
                                    $theStorageId = $oldOrderRow['Orderrow']['storage_id'];
                                    $this->Utilities->storageLoad($theStorageId, $oldOrderRow['Orderrow']['quantita'], '[BC1598] Carico Ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])) . ' per modifica ', $this->Utilities->getLastBuyPrice($theStorageId), $newOrder['Order']['deposit_id'], $numberOfMovement, 'BI_ED');
                                }
                            }
                            else
                            {
                                if (isset($oldOrderRow['Orderrow']['storage_id']))
                                {
                                    $theStorageId = $oldOrderRow['Orderrow']['storage_id'];
                                    $this->Utilities->storageLoad($theStorageId, $oldOrderRow['Orderrow']['quantita'], '[BC1602] Carico Ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])) . ' per modifica ', 0, -1, $numberOfMovement, 'BI_ED');
                                }
                            }
                        }
                    }

                    // Aggiorno i vecchi valore
                    $this->Orderrow->delete($oldOrderRow['Orderrow']['id']);
                }

                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];

                foreach ($this->request->data['Orderrow'] as $key => $row)
                {
                    $numberOfMovement++;
                    $newOrderRow = $this->Utilities->createOrderRow($newOrder['Order']['id'], $row);

                    $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $row['iva_id']]]);

                    if (!isset($row['discount']))
                        $row['discount'] = 0;


                    $prezzo_riga = $this->Utilities->calculateOrderRowWithDiscount($row['quantita'], $row['prezzo'], $row['discount']);

                    if (isset($newVat['Iva']['percentuale'])) {
                        $importo_iva += $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        if($row['complimentaryQuantity'] > 0){
                            $importo_iva += (($row['complimentaryQuantity']*$row['prezzo'])/100) * $newVat['Iva']['percentuale'];
                        }
                    }
                    $imponibile_iva += $prezzo_riga;

                    if (isset($newVat['Iva']['percentuale']))
                    {
                        $calcoloIva = $this->Utilities->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                        if($row['complimentaryQuantity'] > 0){
                            $calcoloIva += (($row['complimentaryQuantity']*$row['prezzo'])/100) * $newVat['Iva']['percentuale'];
                        }
                        isset($arrayIva[$row['iva_id']]['iva']) ? $arrayIva[$row['iva_id']]['iva'] += $calcoloIva : $arrayIva[$row['iva_id']]['iva'] = $calcoloIva;
                    }

                    isset($arrayImponibili[$row['iva_id']]['imponibile']) ? $arrayImponibili[$row['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$row['iva_id']]['imponibile'] = $prezzo_riga;

                    // Il carico e scarico lo faccio solo se è presente nel magazzino

                    if ($row['storage_id'] != '')
                    {
                        // Se esiste l'oggetto (potrei aver cambiato la descrizione quindi è un controllo aggiuntivo)
                        if ($this->Storage->find('count', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.id' => $row['storage_id']]]) > 0) {
                            $theStorageId = $row['storage_id'];
                            $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico fattura ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), 0, $newOrder['Order']['deposit_id'], $numberOfMovement, 'BI_ED');

                        }
                    }
                    else
                    {
                        if (!$this->Utilities->isANote($row, 'order')) {
                            if ($this->Utilities->notExistInStorage($row['storage_id'])) {
                                $newStorage = $this->Utilities->createNewStorageFromrow($row, $newOrder['Order']['client_id'], null);
                                $this->Orderrow->updateAll(['Orderrow.storage_id' => $newStorage['Storage']['id'], 'Orderrow.company_id' => MYCOMPANY], ['Orderrow.id' => $newBillGood['Orderrow']['id']]);
                                if (ADVANCED_STORAGE_ENABLED) {
                                    if (isset($newStorage['Storage']['id'])) {
                                        $theStorageId = $newStorage['Storage']['id'];
                                        $this->Utilities->storageLoad($theStorageId, $row['quantita'], 'Carico (oggetto nuovo di magazzino) ordine. n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newOrder['Order']['deposit_id'], $numberOfMovement, 'BI_ED');
                                        $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico (oggetto nuovo di magazzino) ordine. n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newOrder['Order']['deposit_id'], $numberOfMovement, 'BE_AD');
                                    }
                                } else {
                                    if (isset($newStorage['Storage']['id'])) {
                                        $theStorageId = $newStorage['Storage']['id'];
                                        $this->Utilities->storageLoad($theStorageId, $row['quantita'], 'Carico (oggetto nuovo di magazzino) ordine. n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                        $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico (oggetto nuovo di magazzino) ordine. n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                    }
                                }
                            } else  // altrimenti scarico
                            {
                                $currentRow = $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'descrizione' => $row['oggetto']]]);
                                $this->Orderrow->updateAll(['Orderrow.storage_id' => $currentRow['Storage']['id'], 'Orderrow.company_id' => MYCOMPANY], ['Orderrow.id' => $row['id']]);
                                if ($newOrder['Order']['tipologia'] == 1 && $newOrder['Order']['accompagnatoria'] == 1)  // fattura accompagnatoria
                                {
                                    $theStorageId = $currentRow['Storage']['id'];
                                    if (ADVANCED_STORAGE_ENABLED) {
                                        $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico Ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newOrder['Order']['deposit_id'], $numberOfMovement, 'BI_ED');
                                    } else {
                                        $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico Ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                                    }
                                }
                            }
                        }
                    }

                    isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;


                }

                $this->redirect(['action' => $redirect]);
            }
            else
            {
                $this->Session->setFlash(__("L'ordine non è stato salvata correttamente."), 'custom-danger');
            }
        }
        else
        {
            $this->request->data = $this->Order->find('first', ['contain' => ['Client' => ['Bank'], 'Orderrow' => ['conditions'=>['Orderrow.state'=>ATTIVO, 'Orderrow.company_id'=>MYCOMPANY],'Iva'], 'Deposit'], 'recursive' => 2, 'conditions' => ['Order.id' => $id, 'Order.company_id'=>MYCOMPANY, 'Order.state'=>ATTIVO]]);
        }

        $orders = $this->Order->find('first', ['contain' => ['Client' => ['Bank'], 'Orderrow' => [ 'conditions'=>['Orderrow.state'=>ATTIVO, 'Orderrow.company_id'=>MYCOMPANY],'Iva', 'Storage'], 'Deposit'], 'recursive' => 3, 'conditions' => ['Order.id' => $id, 'Order.company_id' => MYCOMPANY, 'Order.state'=>ATTIVO]]);

        $this->set('orders', $orders);
        $this->set('clients', $this->Client->getList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('units', $this->Units->getList());
        $this->set('myCompany', MYCOMPANY);
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('getAviableQuantity', $this->Utilities);
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('settings', $this->Setting->GetMySettings());
        $this->set('tipologia', $tipologia);
        $this->set('redirect', $redirect);

        if (ADVANCED_STORAGE_ENABLED)
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
        else
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));

        // Se è selezionato alternative address lo salvo come destino
        $arrayOfAlternativeAddress = [];
        $alternativeAddresses = $this->Utilities->getCustomerDifferentAddress($orders['Order']['client_id']);
        foreach ($alternativeAddresses as $alternativeAddress)
        {
            isset($alternativeAddress['Clientdestination']['nation_id']) ? $nationName = ' - ' . $this->Utilities->getNationName($alternativeAddress['Clientdestination']['nation_id'])['Nation']['name'] : $nationName = '';
            $arrayOfAlternativeAddress[$alternativeAddress['Clientdestination']['id']] = $alternativeAddress['Clientdestination']['name'] . ' - ' . $alternativeAddress['Clientdestination']['address'] . ' - ' . $alternativeAddress['Clientdestination']['cap'] . ' - ' . $alternativeAddress['Clientdestination']['city'] . ' - ' . $alternativeAddress['Clientdestination']['province'] . $nationName;
        }

        $this->set('alternativeAddress', $arrayOfAlternativeAddress);

        // START CONTROLLARE POTREBBE ESERE CODICE OBSOLETO
        $arrayOfReferredAddress = [];
        $referredClientAddresses = $this->Utilities->getResellerClient($orders['Order']['client_id']);

        foreach ($referredClientAddresses as $referredClientAddress)
            $arrayOfReferredAddress[$referredClientAddress['Client']['id']] = $referredClientAddress['Client']['ragionesociale'] . ' - ' . $referredClientAddress['Client']['indirizzo'] . ' - ' . $referredClientAddress['Client']['cap'] . ' - ' . $referredClientAddress['Client']['citta'] . ' - ' . $referredClientAddress['Client']['provincia'];

        $this->set('referredClientAddress', $arrayOfReferredAddress);
        // FINE

        $causals = $this->Utilities->getCausals();
        unset($causals[3]);
        $this->set('causali', $causals);

        // Se avanza
        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('deposits', $this->Utilities->getDepositsList());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        }

        $this->Render('edit');
    }

    // Aggiunta estese
    public function add($tipologia = 1)
    {
        ini_set('memory_limit', '-1');


        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Storage', 'Orderrow', 'Deadline', 'Ivas', 'Units', 'Clientdestination', 'Setting', 'Currencies','Client', 'Bank']);

        $this->request->data['Order']['tipologia'] = $tipologia;


        $redirect = 'index';

        $defaultSectional = $this->Utilities->getDefaultOrderSectional();
        $this->set('defaultSectional', $defaultSectional);

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $this->set('nextOrderNumber', $nextSectionalNumber);
        $this->set('sectionals', $this->Utilities->getOrderSectionalList());



        if ($this->request->is(['post', 'put']))
        {

            $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $this->request->data['Order']['client_id'], 'Client.state' => ATTIVO, 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id', 'Client.currency_id']]);
            $this->request->data['Order']['company_id'] = MYCOMPANY;

            // Salva se è una fattura accompagnatoria o no [ in edit non lo faccio tanto è già definita ] - ridondanza inutile perché adesso tipologia = 6 è accompagnatoria
            $numberOfMovement = 0;

            // Se il cliente non esiste allora lo creo
            if (empty($cliente['Client']['id'])) {
                $clientAddress = $this->request->data['Order']['client_address'];
                $clientCap = $this->request->data['Order']['client_cap'];
                $clientCity = $this->request->data['Order']['client_city'];
                $clientProvince = $this->request->data['Order']['client_province'];
                isset($this->request->data['Order']['client_nation']) ? $clientNation = $this->request->data['Order']['client_nation'] : $clientNation = null;
                // Setto il nome utente
                $this->request->data['Order']['client_name'] = $this->request->data['Order']['client_id']; // In realta client_id è la ragsociale
                // Poi client id diventa id
                $this->request->data['Order']['client_id'] = $this->Utilities->createClient($this->request->data['Order']['client_id'], $clientAddress, $clientCap, $clientCity, $clientProvince, $clientNation);
            } else {
                // Setto il nome utente
                $this->request->data['Order']['client_name'] = $this->request->data['Order']['client_id']; // In realta client_id è la ragsociale
                // Poi client id diventa id
                $this->request->data['Order']['client_id'] = $cliente['Client']['id'];
                // Salvo valuta sulla fattura
            }

            $this->request->data['Order']['date'] = date("Y-m-d", strtotime($this->request->data['Order']['date']));
            $this->request->data['Order']['delivery_date'] = date("Y-m-d", strtotime($this->request->data['Order']['delivery_date']));


            if ($newOrder = $this->Order->save($this->request->data['Order'])) {
                // Setto a zero i valori per i calcoli delle scadenze
                $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = $calcoloIva = $taxableIncomeForDeadline = $vatForDeadline = 0;  // Aggiunto ritenuta acconto
                $arrayIva = $arrayImponibili = [];
                foreach ($this->request->data['Orderrow'] as $key => $row)
                {
                    $numberOfMovement++; // Per registrazione movimenti in carico scarico di magazzino
                    //$newOrderRow = $this->Utilities->createOrderRow($newOrder['Order']['id'], $row);
                    $this->Utilities->createOrderRow($newOrder['Order']['id'], $row);

                    if($this->request->data['Order']['completed'] == 1){
                        if ($row['storage_id'] != '') {
                            $theStorageId = $row['storage_id'];
                            $this->Utilities->storageUnload($theStorageId, $row['quantita'], 'Scarico ordine n.' . $newOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($newOrder['Order']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newOrder['Order']['deposit_id'], $numberOfMovement, 'BI_AD');
                        }
                    }
                    // Se esiste lo storage id


                    isset($row['iva_id']) ? $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $row['iva_id']]]) : null;


                    $prezzo_riga = $this->Utilities->calculateBillRowWithDiscount($row['quantita'], $row['prezzo'], $row['discount']);


                    isset($newVat['Iva']['percentuale']) ? $importo_iva += ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;
                    if(isset($newVat['Iva']['percentuale'])){
                        $row['complimentaryQuantity'] > 0 ? $importo_iva+= (($row['complimentaryQuantity'] * $row['prezzo'])/100) * $newVat['Iva']['percentuale'] : null;
                    }
                    $imponibile_iva += $prezzo_riga;

                    // Ho messo nullable il calcolo iva
                    isset($newVat['Iva']['percentuale']) ? $calcoloIva = ($prezzo_riga / 100) * $newVat['Iva']['percentuale'] : null;

                    if(isset($newVat['Iva']['percentuale'])){
                        $row['complimentaryQuantity'] > 0 ? $calcoloIva+= (($row['complimentaryQuantity'] * $row['prezzo'])/100) * $newVat['Iva']['percentuale'] : null;
                    }
                    if (isset($row['iva_id'])) {
                        isset($arrayIva[$row['iva_id']]['iva']) ? $arrayIva[$row['iva_id']]['iva'] += $calcoloIva : $arrayIva[$row['iva_id']]['iva'] = $calcoloIva;
                        isset($arrayImponibili[$row['iva_id']]['imponibile']) ? $arrayImponibili[$row['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$row['iva_id']]['imponibile'] = $prezzo_riga;
                    }
                }
                $this->Utilities->updateOrderSectional($newOrder['Order']['id']);

                $this->Session->setFlash(__('Ordine di vendita salvato correttamente'), 'custom-flash');
                $this->redirect(['action' => $redirect]);
            } else {
                $this->Session->setFlash(__('Non è stato possibile salvare l\'ordine di vendita, riprovare.'), 'custom-danger');
            }
        }

        if (ADVANCED_STORAGE_ENABLED)
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations());
            $this->set('mainDeposit', $this->Utilities->getDefaultDeposits());
        }
        else
        {
            $this->set('magazzini', $this->Utilities->getStoragesWithVariations(null, 'false'));
        }

        $this->Setting->getCompanyWithholdingTax() > 0 ? $withholdingtax = $this->Setting->getCompanyWithholdingTax() : $withholdingtax = '';

        $this->set('deposits', $this->Utilities->getDepositsList());
        $this->set('units', $this->Units->getList());
        $this->set('clients', $this->Client->getAdvancedList());
        $this->set('payments', $this->Utilities->getPaymentsList());
        $this->set('vats', $this->Utilities->getVatsList());
        $this->set('settings', $this->Setting->GetMySettings());
        $this->set('tipologia', $tipologia);
        $this->set('nations', $this->Utilities->getNationsList());
        $this->set('banks', $this->Bank->getBanksListWithAbiAndCab());
        $this->set('redirect', $redirect);

        $this->Render('add');
    }

    public function orderDuplicate($id, $type)
    {
        $this->loadModel('Utilities');
        $this->loadModel('Orderrow');
        $conditionArray = ['Order.company_id' => MYCOMPANY, 'Order.id' => $id, 'Order.state' => 1];
        $currentOrder = $this->Order->find('first', ['conditions' => $conditionArray]);
        $currentOrder['Order']['id'] = null;

        $defaultSectional = $this->Utilities->getDefaultOrderSectional();

        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $currentOrder['Order']['numero_ordine'] = $nextSectionalNumber;

        $currentOrder['Order']['sectional_id'] = $defaultSectional['Sectionals']['id'];
        $currentOrder['Order']['tipologia'] = $type;
        $currentOrder['Order']['state'] = 1;

        $currentOrder['Order']['date'] = date("Y-m-d");

        $newOrder = $this->Order->Save($currentOrder);

        // Duplico le righe
        $conditionGoodArray = ['Orderrow.company_id' => MYCOMPANY, 'Orderrow.order_id' => $id, 'Orderrow.state' => 1];
        $currentOrderRows = $this->Orderrow->find('all', ['conditions' => $conditionGoodArray]);

        foreach ($currentOrderRows as $row)
        {
            $newOrderRow = $this->Orderrow->create();
            $newOrderRow = $row;
            $newOrderRow['Orderrow']['id'] = null;
            $newOrderRow['Orderrow']['order_id'] = $newOrder['Order']['id'];
            $newOrderRow['Orderrow']['state'] = 1;
            $this->Orderrow->save($newOrderRow);
        }

        $this->Utilities->updateOrderSectional($newOrder['Order']['id']);
        $this->redirect(['controller'=>'orders', 'action' => 'edit', $newOrder['Order']['id']]);
    }

    public function checkOrderDuplicate()
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        isset($_POST['sectional']) ? $sectional = $_POST['sectional'] : $sectional = null;
        $return = $this->Utilities->checkOrderDuplicate($_POST['orderNumber'], $_POST['date'], $_POST['type'], $sectional);
        return json_encode($return);
    }

    public function delete($id = null, $redirect = 'index')
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Transports', 'Bills', 'Orderrow', 'Messages', 'Transportgood']);

        if (!$this->request->is('post'))
            throw new MethodNotAllowedException();

        $this->Order->id = $id;
        $orderrows = $this->Orderrow->find('all', ['conditions' => ['Orderrow.order_id' => $id]]);
        $oldOrder = $this->Order->find('first', ['conditions' => ['Order.id' => $id]]);
        $numberOfMovement = 0;

        foreach ($orderrows as $row)
        {
            $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $row['Orderrow']['storage_id']]]);

            if ($oldOrder['Order']['completed'] == 1 && isset($storage['Storage']['id'])) {
                $numberOfMovement++;
                if (ADVANCED_STORAGE_ENABLED)
                {
                    if (isset($storage['Storage']['id']))
                    {
                        $theStorageId = $storage['Storage']['id'];
                        $this->Utilities->storageLoad($theStorageId, $row['Orderrow']['quantita'], 'Carico per eliminazione ordine n.' . $oldOrder['Order']['numero_ordine'] . ' del ' . date('d-m-Y', strtotime($oldOrder['Order']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $oldOrder['Order']['deposit_id'], $numberOfMovement, 'BI_DE' . '_' . $oldOrder['Order']['id']);
                    }
                }
                else
                {
                    if (isset($storage['Storage']['id']))
                    {
                        $theStorageId = $storage['Storage']['id'];
                        $this->Utilities->storageLoad($theStorageId, $row['Good']['quantita'], 'Carico per eliminazione ordine n.' . $oldOrder['Order']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($oldOrder['Order']['date'])), 0, -1, $numberOfMovement, 'BI_DE' . '_' . $oldOrder['Order']['id']);
                    }
                }
            }
        }

        $gender = 'F';
        $article = 'la';
        $billname  = 'Ordine';

        $asg = [$article, $billname, $gender];

        if ($this->Order->isHidden($id))
            throw new Exception($this->Messages->notFound($asg[0], $asg[1], $asg[2]));

        $this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Order->find('first', ['conditions' => ['Order.id' => $id, 'Order.company_id' => MYCOMPANY, 'Order.state' => ATTIVO]]);

        if ($this->Order->hide($currentDeleted['Order']['id']))
        {
            $this->loadModel('Sectional');
            $this->Sectional->reduceSectionalValue($currentDeleted['Order']['sectional_id'], $currentDeleted['Order']['numero_ordine']);

            /* $this->Transports->updateAll(['order_id' => null], ['company_id' => MYCOMPANY, 'order_id' => $id]);

             $this->Transportgood->updateAll(['Transportgood.order_id' => null], ['Transportgood.order_id' => $id]);*/
            $this->Orderrow->deleteAll(['Orderrow.order_id' => $id]);
            $this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1], $asg[2])), 'custom-flash');
            $this->redirect(['action' => $redirect]);
        }
        else
        {
            $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1], $asg[2])), 'custom-danger');
            $this->redirect(['action' => $redirect]);
        }
    }

    public function createBillForOrderSummary()
    {
        $this->loadModel('Utilities');
        $conditionsArray = ['Order.company_id' => MYCOMPANY, 'Order.bill_id'=>null, 'Order.state' => 1];

        if($this->request->is(['post','ajax']))
        {
            if(isset($this->request->data['filters']['date1']) && $this->request->data['filters']['date1'] != '')
            {
                $conditionsArray['Order.date >='] = date('Y-m-d' , strtotime($this->request->data['filters']['date1'])) ;
            }
            if(isset($this->request->data['filters']['date2']) && $this->request->data['filters']['date2'] != '')
            {
                $conditionsArray['Order.date <='] = date('Y-m-d' , strtotime($this->request->data['filters']['date2']));
            }

            if(isset($this->request->data['date3']) && $this->request->data['date3'] != '')
            {
                $customBillDateForOrder = $this->request->data['date3'];
            }
            else
            {
                $customBillDateForOrder = null;
            }

            // Filtro sul multiselect dei clienti
            if(isset($this->request->data['filters']['clients']) && $this->request->data['filters']['clients'] != '')
            {
                foreach($this->request->data['filters']['clients'] as $client)
                {
                    $arrayCl[] = $client;
                }

                $conditionsArray['Client.id IN'] = $arrayCl;
            }
            else // Nessun cliente selezionato
            {
                $conditionsArray['Client.id IN'] = ['-1'];
            }
        }

        // Chiamo la query
        $filteredOrders = $this->Order->find('all', [
            'contain' => ['Client', 'Bill'],
            'conditions' => $conditionsArray,
            'order' => [ 'Client.ragionesociale' => 'asc', 'Order.date' => 'asc'],
        ]);

        // Per ogni cliente
        $currentCustomer = null;


        $this->request->data['Transport']['splitPayment'] == 1 ? $splitPayment = 'true' : $splitPayment = 'false';
        $this->request->data['Transport']['electronicInvoice'] == 1 ? $electronicInvoice = 'true' : $electronicInvoice = 'false';
        $this->request->data['Transport']['detailed'] == 1 ? $detailed = 'true' : $detailed = 'false';

        $orderToBill = [];
        foreach($filteredOrders as $order)
        {
            if($currentCustomer != null && $order['Order']['client_id'] != $currentCustomer)
            {
                $this->Utilities->buildInvoiceFromOrders($orderToBill,$detailed,$splitPayment,$electronicInvoice,$customBillDateForOrder);
                $orderToBill = null;
            }

            $currentCustomer = $order['Order']['client_id'];
            $orderToBill[] =  $order['Order']['id'];
        }

        // Ultimo cliente del ciclo
        if(isset($orderToBill))
        {
            $this->Utilities->buildInvoiceFromOrders($orderToBill,$detailed,$splitPayment,$electronicInvoice,$customBillDateForOrder);
        }

        // Creo fattura
        $this->Session->setFlash(__('Fatture correttamente create'), 'custom-flash');
        $this->redirect(['controller'=>'orders' , 'action'=>'index']);
    }

    public function fatturaExtended($orderId,$detailedbill,$splitPayment,$electronicInvoice)
    {
        $this->autoRender = false;
        $this->loadModel('Utilities');
        //$arrayOfOrder[] = $orderId;
        $newBillId = $this->Utilities->buildInvoiceFromOrders($orderId,$detailedbill,$splitPayment,$electronicInvoice);
        return $newBillId;
    }

    public function  multiOrdersBillCreate(){

        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Client']);

        $this->set('clients', $this->Client->getAdvancedList());

    }

    public function multiOrderBill(){
        $this->autoRender = false;
        $this->loadModel('Utilities');
        $startDate = date('Y-m-d', strtotime($_POST['dateFrom'])); $endDate = date('Y-m-d', strtotime($_POST['dateTo']));
        $client_id = $_POST['clientId'];
        if(isset($_POST['destinationId']) && $_POST['destinationId'] > 0){
            $destination_id = $_POST['destinationId'];
            $ordersId = $this->Order->find('all', ['recursive'=>-1, 'conditions'=>["Order.state" => 1,"Order.delivery_date >="=>$startDate, "Order.delivery_date <="=>$endDate, "Order.client_id"=>$client_id, "Order.destination_id"=>$destination_id, "Order.completed"=>1, "Order.bill_id" =>null], 'fields' => ['id']]);
            $newBillId = $this->Utilities->buildInvoiceFromOrders($ordersId,false,false,true);
            return $newBillId;
        }else{
            $ordersId = $this->Order->find('all', ['recursive'=>-1, 'conditions'=>["Order.state" => 1,"Order.delivery_date >="=>$startDate, "Order.delivery_date <="=>$endDate, "Order.client_id"=>$client_id, "Order.completed"=>1, "Order.bill_id" =>null], 'fields' => ['id']]);
            $newBillId = $this->Utilities->buildInvoiceFromOrders($ordersId,false,false,true);
            return $newBillId;
        }

    }

    public function ordinePdfExtendedvfs($ragioneSociale, $id, $companyId = null, $realCompanyId = null, $token = null)
    {
        $numeroArr = $this->Order->find('first', ['conditions'=>['Order.company_id'=>MYCOMPANY, 'Order.state'=>ATTIVO, 'Order.id'=>$id], 'fields'=>['numero_ordine']])['Order']['numero_ordine'];
        $curl = curl_init();
        $db = $_SESSION['Auth']['User']['dbname'];

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://api.gestionale-online.net/createPDFOrder',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "db": "'.$db.'",
                "order" : "'.$id.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/pdf',
                'Content-Disposition:attachment; filename="pdfOrder.pdf"'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $destination = dirname(__FILE__) . '/file.pdf';
        $file = fopen($destination, "w+");
        fputs($file, $response);
        fclose($file);
        $filename = 'ordine_n_'.$numeroArr.'.pdf';

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        readfile($destination);


    }

}
