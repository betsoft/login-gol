<?php
App::uses('AppController', 'Controller');
App::uses('ConnectionManager', 'Model');


class PaymentsController extends AppController {

	public function index()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Catalog','Payment','Csv']);

		$conditionsArray = [ 'Payment.company_id' => MYCOMPANY,'Payment.state'=>ATTIVO];

		if(BILLS_RECEIPT)
		{
			$filterableFields = ['metodo',null,null,null,null,'Bank__description','Bank__description',null];
			$sortableFields = [['metodo','Metodo di pagamento'],[null,'Fine mese'],[ null,'Riba'],[null,'Predefinito scontrini'],[ null,'Scadenze'],[null,'Banca d\'appoggio'],[null,'Banca d\'appoggio alternativa'],['#actions']];
		}
		else
		{
			$filterableFields = ['metodo',null,null,null,'Bank__description','Bank__description',null];
			$sortableFields = [['metodo','Metodo di pagamento'],[null,'Fine mese'],[ null,'Riba'],[ null,'Scadenze'],[null,'Banca d\'appoggio'],[null,'Banca d\'appoggio alternativa'],['#actions']];

		}

		$automaticFilter = $this->Session->read('arrayOfFilters') ;
		if(isset($automaticFilter[$this->params['controller']][$this->action]) && $this->request->is('ajax') == false) { $this->request->data['filters'] = $automaticFilter[$this->params['controller']][$this->action]; } else { null; }

		if(($this->request->is('ajax') || isset($automaticFilter)) && isset($this->request->data['filters']))
		{
			$conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);

			$arrayFilterableForSession = $this->Session->read('arrayOfFilters');
			$arrayFilterableForSession[$this->params['controller']][$this->action] = $this->request->data['filters'];
			$this->Session->write('arrayOfFilters',$arrayFilterableForSession);
		}

		// Generazione XLS
		if(isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls')
		{
			$this->autoRender = false;
			$dataForXls = $this->Payment->find('all',['conditions'=>$conditionsArray,'order' => ['Payment.metodo' => 'asc']]);

			echo 'Metodo di pagamento;Fine mese;Riba;Scadenze;Banca d\'appoggio;'."\r\n";
			foreach ($dataForXls as $xlsRow)
			{
				$stringVar = ''; $i = 0;
				foreach($xlsRow['Paymentdeadlines'] as $deadline)
				{
					if($deadline['deadlineday'] == 0){ $deadline['deadlineday'] = 'immediata'; }
					if($i == 0)
					{
						$stringVar .= $deadline['deadlineday'];
					}
					else
					{
						$stringVar .= '/' . $deadline['deadlineday'];
					}
					$i++;
				}

				$xlsRow['Payment']['endofmonth'] == 0 ? $endOfmonth = 'NO' : $endOfmonth = 'SI';
				$xlsRow['Payment']['riba'] == 0 ? $riba = 'NO' : $riba = 'SI';

				echo $xlsRow['Payment']['metodo']. ';' .$endOfmonth. ';'.$riba. ';'.$stringVar. ';'.$xlsRow['Bank']['description'].';'."\r\n";
			}
		}
		else
		{
			$this->set('filterableFields',$filterableFields);
			$this->set('sortableFields',$sortableFields);
			$this->paginate = ['contain'=>['AlternativeBank', 'Bank','Paymentdeadlines'],'conditions' => $conditionsArray,'order'=>'metodo'];
			$this->set('payments', $this->paginate());
			$this->render('index');
		}
	}

	public function add()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Myhelper','Payments','Paymentdeadlines']);

		if ($this->request->is('post'))
		{
			$this->db = ConnectionManager::getDataSource('default');
			$this->db->begin();

			$this->Payments->create();
			$this->request->data['Payment']['company_id']=MYCOMPANY;
			if ($newPaymentMethod = $this->Payment->save($this->request->data))
			{
				// Salvo le scadenze del metodo di pagamento)
				foreach($this->request->data['Deadline'] as $deadline)
				{
					if($deadline['Day'] != '') // Se è stato definita una scadenza.
					{
						$newDeadline = $this->Paymentdeadlines->create();
						$newDeadline['payment_id'] = $newPaymentMethod['Payment']['id'];
						$newDeadline['deadlineday'] = $deadline['Day'];
						$this->Paymentdeadlines->save($newDeadline);
					}
				}

				$this->Payments->commit();

				$this->Session->setFlash(__('Metodo di pagamento salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			}
			else
			{
				$this->Session->setFlash(__('Metodo di pagamento non salvato'), 'custom-danger');
			}
		}
			$banks = $this->Utilities->getBanksList();
			$this->set('banks',$banks);
			$this->set('einvoicePaymentType',$this->Utilities->getEinvoicePaymentType());
			$this->set('einvoicePaymentMethod',$this->Utilities->getEinvoicePaymentMethod());

			// Definisco i messaggi degli helepr nella vista
			$this->set('helperMessage',$this->Myhelper->getMessage('generateRiba'));
			$this->Render('add');
	}

	public function edit($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Myhelper','Paymentdeadlines']);


		$this->Payment->id = $id;
		if (!$this->Payment->exists()) {
			throw new NotFoundException(__('Metodo di pagamento non valido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if ($this->Payment->save($this->request->data))
			{
				$this->Paymentdeadlines->deleteAll(['payment_id'=>$id]);
				foreach($this->request->data['Deadline'] as $deadline)
				{
					$newPaymentDeadline = $this->Paymentdeadlines->create();
					$newPaymentDeadline['payment_id'] = $id;
					$newPaymentDeadline['deadlineday'] = $deadline['Day'];
					$newPaymentDeadline['company_id'] = MYCOMPANY;
					$this->Paymentdeadlines->save($newPaymentDeadline);
				}

				$this->Session->setFlash(__('Metodo di pagamento salvato'), 'custom-flash');
				$this->redirect(['action' => 'index']);

			}
			else
			{
				$this->Session->setFlash(__('Metodo di pagamento non salvato, riprovare'), 'custom-danger');
				$banks = $this->Utilities->getBanksList();
				$this->set('banks',$banks);
				$conditionsArray = ['payment_id'=>$id];
				$deadlines = $this->Paymentdeadlines->find('all',['conditions' =>$conditionsArray]);
				$this->set('deadlines',$deadlines);
			}
		} else
		{

			$this->request->data = $this->Payment->read(null, $id);
			$conditionsArray = ['payment_id'=>$id];
			$deadlines = $this->Paymentdeadlines->find('all',['conditions' =>$conditionsArray]);
			$banks = $this->Utilities->getBanksList();
			$this->set('banks',$banks);
			$this->set('deadlines',$deadlines);

		}

		$this->set('einvoicePaymentType',$this->Utilities->getEinvoicePaymentType());
		$this->set('einvoicePaymentMethod',$this->Utilities->getEinvoicePaymentMethod());

		// Definisco i messaggi degli helepr nella vista
		$this->set('helperMessage',$this->Myhelper->getMessage('generateRiba'));
		$this->Render('edit');
	}

	// Elimino la scadenza selezionata
	public function elimina_scadenza_metodo_di_pagamento()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Paymentdeadlines','Payments']);
		$id = $_GET['id'];
		$paymentMethodDeadline = $this->Paymentdeadlines->find('first',['conditions' => ['id' => $id]]);
		$payment = $this->Payments->find('first', ['conditions'=> [ 'id' => $paymentMethodDeadline['Paymentdeadlines']['payment_id']]]);
		if($payment['Payments']['company_id'] ==  MYCOMPANY)
		{
			$this->Paymentdeadlines->delete($paymentMethodDeadline['Paymentdeadlines']['id'],false);
		}
		else
		{
			throw new NotFoundException('Scadenza non trovata');
		}
		echo $id;
		die();
	}

	public function delete($id = null)
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Payment','Messages']);

        $asg =  ["il","metodo di pagamento","M"];
		if($this->Payment->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);

        $currentDeleted = $this->Payment->find('first',['conditions'=>['Payment.id'=>$id,'Payment.company_id'=>MYCOMPANY]]);
        if ($this->Payment->hide($currentDeleted['Payment']['id']))
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}

	public function getPaymentCollectionFees()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Payment']);
		$this->autoRender = false;
		$currentPayment = $this->Payment->find('first',['conditions'=>['Payment.id'=>$_POST['PaymentId'],'Payment.company_id'=>MYCOMPANY]]);
		return $currentPayment['Payment']['paymentFixedCost'];
	}

	public function getPaymentName()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Payment']);
        $this->autoRender = false;
        $currentPayment = $this->Payment->find('first',['conditions'=>['Payment.id'=>$_POST['paymentId'],'Payment.company_id'=>MYCOMPANY]]);
        return $currentPayment['Payment']['metodo'];
    }

	public function setDefaultReceiptPayment()
	{
		$this->loadModel('Utilities');
		$this->autoRender = false;
		$receiptPayment = $_POST['id'];
		$this->Utilities->setDefaultReceiptPayment($receiptPayment);
	}

    public function getPaymentBanks()
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this,['Payment']);
        $this->autoRender = false;
        $currentPayment = $this->Payment->find('first',['conditions'=>['Payment.id'=>$_POST['PaymentId'],'Payment.company_id'=>MYCOMPANY]]);

        $arrayBanks = ['bank_id' => $currentPayment['Payment']['bank_id'], 'alternative_bank_id' => $currentPayment['Payment']['alternative_bank_id']];
        return json_encode($arrayBanks);
    }
}
