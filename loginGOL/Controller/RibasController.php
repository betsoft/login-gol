<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'mpdf/mpdf');
App::import('Vendor', 'tpmacn/csvhelper');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');
App::uses('Routing','Router');
App::uses('Xml', 'Utility');
require 'PHPMailer/PHPMailerAutoload.php';

class RibasController extends AppController
{
	public $components = array('Mpdf');

    public function createRibaCsv()
	{
		$this->autoRender = false;
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Bills','Banks','Setting']);

		$MyCompanyData = $this->Setting->GetMySettings($this->Auth->User("company_id"));
		
		$ribaNumber = $this->Utilities->getNextRibaNumber($this->Auth->User('company_id'),date("Y"));
		
		// Recupero le riba da effettuare
		$ribaBills = $this->Utilities->getRibaBills($this->Auth->User("company_id"));
		
		if(empty($fileName))
		    $fileName = "export_".date("Y-m-d").".csv";

		$newFile = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');
		
		$enclosure = $fileName = '';
		
		$bankId = $_POST['bank'];

		$bank = $this->Banks->find('first',['conditions'=>['id'=>$bankId]]);					

		$missingData = 0;
		if($MyCompanyData['Setting']['name'] == ''){ $missingData = 1;}
		if($MyCompanyData['Setting']['indirizzo'] == ''){ $missingData = 2;}
		if($MyCompanyData['Setting']['citta'] == ''){ $missingData = 3;}
		if($MyCompanyData['Setting']['prov'] == ''){ $missingData = 4;}
		if(!isset($MyCompanyData['Setting']['siacode']) || $MyCompanyData['Setting']['siacode'] == ''){ $missingData = 5;}
		if(!isset($MyCompanyData['Setting']['siasocname']) || $MyCompanyData['Setting']['siasocname'] == ''){ $missingData = 6;}
		if($bank['Banks']['abi'] == ''){ $missingData = 7;}
		if($bank['Banks']['cab'] == ''){ $missingData = 8;}
	
		if($missingData > 0)
		    return $missingData;

		// Controllo esattezza dati riba
		foreach($ribaBills as $bill)
		{
			if(in_array($bill['Bill']['id'],$_POST['arrayOfBill']))
			{
				// Recupero le scadenze della riba	
				$deadlines = $this->Utilities->getDeadlines($bill['Bill']['id']);
				if(count($deadlines) == 0)
				{
					return 9;
				}

				if(isset($bill['Client']['Bank']['description']) && isset($bill['Client']['Bank']['abi']) && isset($bill['Client']['Bank']['cab']) && $bill['Client']['Bank']['abi'] != '' && $bill['Client']['Bank']['cab'] != '' )
				{
				}
				else
				{
					return 10;
				}
			}
		}

		$csvFile = fopen('php://output', 'w');
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');

		// Struttura del record di testa - codice fisso IB
		$recordDiTesta  = ' ';  									// 1 - filler : Blank
		$recordDiTesta .= 'IB'; 									// 2-3 - tipo record : "IB"
		// !!! Parametrizzare
		$recordDiTesta .= $MyCompanyData['Setting']['siacode'];		// 4-8 Mittente: codice assegnato dalla "Sia" all'azienda mittente (è censita in una directory)
		$recordDiTesta .= str_pad($bank['Banks']['abi'],5,'0',STR_PAD_LEFT);	// 9-13  ricevente: codice ABI della banca assuntrice cui devono essere inviate le disposizioni (è censita in una directory);
		$recordDiTesta .= date("dmy");							// 14-19 data creazione : data di creazione del flusso da parte dell'azienda mittente nel formato GGMMAA
		// !!! Parametrizzare il numero in base ai flussi creati giornalmente
		$recordDiTesta .= str_pad('000001',20,' ',STR_PAD_RIGHT);	// 20-39 Nome supporto : Campo di libera composizione da parte dell'azienda Mittente; dev'essere univoco nell'ambito della data di creazione a parità di mittente e ricevente					
		$recordDiTesta .= str_repeat(' ',6); 						// 40-45 campo a disposizione :  campo a disposizione dell'azienda mittente
 		$recordDiTesta .= str_repeat(' ',59);						// 46-104 filler : blank
 		$recordDiTesta .= str_repeat(' ',7);						// 105-111 quantificatore fluss : Campo facoltativo salvo regole invio differentei [3 sottocampi 105 [flusso] 106 [qualificatore flusso] 107-11 [soggetto veicoloatore]]
		$recordDiTesta .= str_repeat(' ',2);						// 112-113 filler : blank;
		$recordDiTesta .= 'E';										// 114 codice divisa : assumere il valore "E" ( Euro ). Fisso
		$recordDiTesta .=  str_repeat(' ',1);						// 115 filler : blank;
		$recordDiTesta .= str_repeat(' ',5); // 116-120 campo non disponibile 

		// Inserisco il record di testata nel file csv
		fwrite($csvFile, $recordDiTesta."\r\n");

		$i = 0;
		$numerorecord = 2;
		$totaleRiba = 0;
		// Per ogni fattura non pagata
 
		foreach($ribaBills as $bill)
		{
			if(in_array($bill['Bill']['id'],$_POST['arrayOfBill']))
			{
                // Recupero le scadenze della riba
                $deadlines = $this->Utilities->getDeadlines($bill['Bill']['id']);
                if(isset($bill['Client']['Bank']['description']) && isset($bill['Client']['Bank']['abi']) && isset($bill['Client']['Bank']['cab']) )
                {
				    foreach($deadlines as $deadline)
				    {
                        $i++;

                        $totaleRiba = $totaleRiba + $deadline['Deadlines']['amount'];

                        // Struttura del record - codice fisso "14";
                        $recordCodiceFisso14 = ' '; 																	// 1 filler : blank
                        $recordCodiceFisso14 .= '14';																	// 2-3 tipo record: codice fisso "14"
                        $recordCodiceFisso14 .= str_pad($i,7,'0',STR_PAD_LEFT);	 										// 4-10 numero progressivo : numero progessivo della disposizione all'interno del flusso. Inizia con 1 ed è progressivo di 1. Il numero deve essere uguale per tutti i record della ricevuta
                        $recordCodiceFisso14 .= str_repeat(' ',12);		 												// 11-22 filler: blank
                        $recordCodiceFisso14 .= date('dmy',strtotime($deadline['Deadlines']['deadline']));				// 23-28 data pagamento : data di scadenza nel formato GGMMAA della ricevuta
                        $recordCodiceFisso14 .= '30000';																// 29-33 causale : Assume il valore fisso "300000"
                        $recordCodiceFisso14 .= str_pad(str_replace('.','',substr($deadline['Deadlines']['amount'],0,12)),13,'0',STR_PAD_LEFT);	// 34-46 importo: importo della ricevuta in centesimi di euro
                        $recordCodiceFisso14 .= '-';																	// 47 segno : valore fisso "-"
                        $recordCodiceFisso14 .= str_pad($bank['Banks']['abi'],5,'0',STR_PAD_LEFT);						// 48-52 codice ABI assuntrice : codice ABI della banca assuntrice delle ricevute deve corrispondere a quello presente sul record di testa
                        $recordCodiceFisso14 .= str_pad($bank['Banks']['cab'],5,'0',STR_PAD_LEFT);						// 52-53 codice CAB assuntrice : codice CAB dello sportello della banca
                        $recordCodiceFisso14 .=  str_pad($bank['Banks']['account'],12,'0',STR_PAD_LEFT);		  	    // 58-69 conto : conto corrente che il cliente chiede di accreditare
                        $recordCodiceFisso14 .=  str_pad($bill['Client']['Bank']['abi'],5,'0',STR_PAD_LEFT);			// 70-74 Codice ABI domiciliataria : codice ABI della banca domiciliataria
                        $recordCodiceFisso14 .=  str_pad($bill['Client']['Bank']['cab'],5,'0',STR_PAD_LEFT);			// 75-79 Codice CAB domiciliataria : codice CAB della banca domiciliataria
                        $recordCodiceFisso14 .=  str_repeat(' ',12);													// 80-91 filler : blank;
                        // Devo parametrizzarlo
                        $recordCodiceFisso14 .=  str_pad($bank['Banks']['clientCode'],5,' ',STR_PAD_RIGHT);				// 92-96 codice azienda : codice sia del cliente ordinante, tale codice se presente, deve essere valorizzato su tutte le singole disposizione può differire dal sia delle banche date
                        $recordCodiceFisso14 .=  '4';																	// 97 tipo codice : deve assumere il valore fisso "4"
                        $recordCodiceFisso14 .=  str_pad(substr($bill['Client']['code'],0,16),16,' ',STR_PAD_LEFT);		// 98-113 codice cliente debitore : codice con il quale il debitore è conosciuto dal creditore
                        // Devo !!! parametrizzarlo
                        $recordCodiceFisso14 .= ' ';																	// 114 Nel caso in cui il debitore sia una banca deve assumere il valore "B"
                        $recordCodiceFisso14 .=  str_repeat(' ',5);														// 115-119 filler : blank;
                        $recordCodiceFisso14 .= 'E';																	// 120 codice divisa: deve coincidere con l'omonimo record di testa

                        // Struttura del record - codice fisso "20";
                        $recordCodiceFisso20 = ' '; 																// 1 filler : blank
                        $recordCodiceFisso20 .= '20';																// 2-3 tipo record: codice fisso "20"
                        $recordCodiceFisso20 .= str_pad($i,7,'0',STR_PAD_LEFT);										// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        // Descrizione del creditore è suddivisa in 4 segmenti alfanumerici di 24 caratteri ciasucuno

                        if(strlen($MyCompanyData['Setting']['name']) > 24)
                        {
                            $ragSoc1 = substr($MyCompanyData['Setting']['name'],0,24);
                            $ragSoc2 = substr($MyCompanyData['Setting']['name'],24,24);
                        }
                        else
                        {
                            $ragSoc1 = substr($MyCompanyData['Setting']['name'],0,24);
                            $ragSoc2 = '';
                        }

                        $recordCodiceFisso20 .= str_pad($ragSoc1,24,' ',STR_PAD_RIGHT);							    // 11-34 1° segmento
                        $recordCodiceFisso20 .= str_pad($ragSoc2,24,' ',STR_PAD_RIGHT);							    // 35-58 2° segmento
                        $recordCodiceFisso20 .= str_pad(substr($MyCompanyData['Setting']['indirizzo'],0,24),24,' ',STR_PAD_RIGHT); // 59-82 3° segmento
                        // Paese
                        $countryAndProvince = $MyCompanyData['Setting']['citta'] . ' ('. $MyCompanyData['Setting']['prov'].')';
                        $recordCodiceFisso20 .= str_pad(substr($countryAndProvince,0,24),24,' ',STR_PAD_RIGHT);		// 83-106 4° segmento
                        $recordCodiceFisso20 .=  str_repeat(' ',14);												// 107-120 filler : blank;

                        // Struttura del record - codice fisso "30";
                        $recordCodiceFisso30 = ' '; 																// 1 filler : blank
                        $recordCodiceFisso30 .= '30';																// 2-3 tipo record: codice fisso "30"
                        $recordCodiceFisso30 .= str_pad($i,7,'0',STR_PAD_LEFT);										// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        // Descrizione del debitore (suddivisa in 2 segmenti alfanumerici di 30 caratteri)
                        $lunghezzaRagioneSociale = strlen($bill['Client']['ragionesociale']);
                        if($lunghezzaRagioneSociale > 30)
                        {
                            $lunghezzaRagioneSociale >60 ? $lunghezza = 30 : $lunghezza = $lunghezzaRagioneSociale - 30; // calcolo lunghezza seconda parte
                            $recordCodiceFisso30 .= str_pad(substr($bill['Client']['ragionesociale'],0,30),30,' ',STR_PAD_RIGHT);			// 11-40 Indirizzo - 1° segmento
                            $recordCodiceFisso30 .= str_pad(substr($bill['Client']['ragionesociale'],30,30),30,' ',STR_PAD_RIGHT);	// 41-70 Indirizzo - 2° segmento
                        }
                        else
                        {
                            $recordCodiceFisso30 .= str_pad($bill['Client']['ragionesociale'],30,' ',STR_PAD_RIGHT);	// 11-40 Indirizzo - 1° segmento
                            $recordCodiceFisso30 .= str_repeat(' ',30);	// 41-70 Indirizzo - 2° segmento
                        }
					
                        ($bill['Client']['cf'] != null  && $bill['Client']['cf'] != '') ? $clientCf =  $bill['Client']['cf'] : $clientCf = $bill['Client']['piva'];
                        $recordCodiceFisso30 .= str_pad($clientCf,16,' ',STR_PAD_RIGHT);													// 71-86 Codifica fiscale : Codice fiscale
                        $recordCodiceFisso30 .=  str_repeat(' ',30);																		// 87-120 filler : blank

                        // Struttura del record - codice fisso "40";
                        $recordCodiceFisso40 = ' '; 																						// 1 filler : blank
                        $recordCodiceFisso40 .= '40';																						// 2-3 tipo record: codice fisso "40"
                        $recordCodiceFisso40 .= str_pad($i,7,'0',STR_PAD_LEFT);																// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        $recordCodiceFisso40 .= str_pad(substr($bill['Client']['indirizzo'],0,30),30,' ',STR_PAD_RIGHT);					// 11-40 Via, numero civico e/o nome della frazione
                        $recordCodiceFisso40 .= str_pad(substr($bill['Client']['cap'],0,5),5,' ',STR_PAD_RIGHT); 							// 41-45 Cap : codice di avviamento postale
                        $recordCodiceFisso40 .= str_pad(substr($bill['Client']['citta'].' '.$bill['Client']['provincia'],0,25),25,' ',STR_PAD_RIGHT);	// 46-70 comune e sigla prov. - comune e sigla della provincia
                        $recordCodiceFisso40 .= str_pad(substr($bill['Client']['Bank']['description'],0,50),50,' ',STR_PAD_RIGHT); 		// 71-120 Banca/sportello domiciliaria - eventuale denominazione in chiaro della banca/sportello domiciliatgaria/o

                        // Struttura del record - codice fisso "50";
                        $recordCodiceFisso50 = ' '; 																						// 1 filler : blank
                        $recordCodiceFisso50 .= '50';																						// 2-3 tipo record: codice fisso "40"
                        $recordCodiceFisso50 .= str_pad($i,7,'0',STR_PAD_LEFT);																// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        // Riferimenti al debito ( è suddiviso in 2 segmenti di 40 caratteri ciascuno)
                        $recordCodiceFisso50 .= str_pad('PER LA FATTURA' .' '.str_pad($bill['Bill']['numero_fattura'],6,'0',STR_PAD_LEFT).' '.'DEL'.' '.date('d/m/y',strtotime($bill['Bill']['date'])),'40',' ',STR_PAD_LEFT); // 11-50 - 1° segmento
                        $recordCodiceFisso50 .= str_repeat(' ',40);																			// 51-90 - 2° segmento (lo lascio vuoto salvo richieste)
                        $recordCodiceFisso50 .= str_repeat(' ',10);																			// 91-100 - filler : blank
					
                        $MyCompanyData['Setting']['cf'] != '' ? $cfPivaCompany = $MyCompanyData['Setting']['cf'] : $cfPivaCompany = $MyCompanyData['Setting']['piva'];

                        $recordCodiceFisso50 .= str_pad($cfPivaCompany,16,' ',STR_PAD_RIGHT);			// 101-116 codifica creditore : Codice fiscale/piva del creditore.
                        $recordCodiceFisso50 .= str_repeat(' ',4);										// 117-120 - filler : blank

                        // Struttura del record - codice fisso "50";
                        $recordCodiceFisso51 = ' '; 													// 1 filler : blank
                        $recordCodiceFisso51 .= '51';													// 2-3 tipo record: codice fisso "40"
                        $recordCodiceFisso51 .= str_pad($i,7,'0',STR_PAD_LEFT);							// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        $recordCodiceFisso51 .= str_pad($ribaNumber,10,'0',STR_PAD_LEFT);				// 11-20 numero ricevuta : numero ricevuta attribuito dal creditore

                        $recordCodiceFisso51 .= str_pad($MyCompanyData['Setting']['siasocname'],20,' ',STR_PAD_RIGHT);		// 21-40 denominazione creditore : denominazione sociale del creditore in forma abbreviata
                        $recordCodiceFisso51 .= str_repeat(' ',15);										// 41-55 provincia : provincia dell'intendenza di finanza che ha autorizzato il pagamento delbollo in modo virtuale
                        $recordCodiceFisso51 .= str_repeat(' ',10);										// 56-65 numero autorizzazione : numero dell'autroizzazione concessa dall'Intendenza di Finanza
                        $recordCodiceFisso51 .= str_repeat(' ',6);										// 66-71 data autorizzazione : data (nel formato GGMMAA) di concessione dell'autorizzazione da parte dell'Intendneza di Finanza
                        $recordCodiceFisso51 .= str_repeat(' ',49); 									// 72-120 filler : blank
					
                        $ribaNumber++;

                        // Struttura del record - codice fisso "50";
                        $recordCodiceFisso70 = ' '; 													// 1 filler : blank
                        $recordCodiceFisso70 .= '70';													// 2-3 tipo record: codice fisso "40"
                        $recordCodiceFisso70 .= str_pad($i,7,'0',STR_PAD_LEFT);							// 4-10 numero progressivo: stesso numero del recrod 14 della disposizione
                        $recordCodiceFisso70 .= str_repeat(' ',78);										// 11-88 filler : blank
                        $recordCodiceFisso70 .= str_repeat(' ',12); 									// 89-100 indicatori di circuito : campo a disposizione per altri circuiti
                        $recordCodiceFisso70 .= str_repeat(' ',3);	 									// 101-103 indicatore richiesta incasso : indicatori richiesta incasso
                        $recordCodiceFisso70 .= str_repeat(' ',17);	 									// 104-120 chiavi di controllo  :

                        $numerorecord = $numerorecord +7;
					
                        // Scrivo i record 14 / 20
                        fwrite($csvFile, $recordCodiceFisso14."\r\n");
                        fwrite($csvFile, $recordCodiceFisso20."\r\n");
                        fwrite($csvFile, $recordCodiceFisso30."\r\n");
                        fwrite($csvFile, $recordCodiceFisso40."\r\n");
                        fwrite($csvFile, $recordCodiceFisso50."\r\n");
                        fwrite($csvFile, $recordCodiceFisso51."\r\n");
                        fwrite($csvFile, $recordCodiceFisso70."\r\n");
					}
			    }

                $this->Utilities->SetRibaEmitted($bill['Bill']['id']);
			}
		}

        $totaleRiba = number_format($totaleRiba,2,'.','');

        // Struttura del record di coda - codice fisso IB
        $recordDiCoda  = ' '  ;																// 1 - filler : Blank
        $recordDiCoda .= 'EF' ; 							    							// 2-3 - tipo record : "EF"
        $recordDiCoda .= $MyCompanyData['Setting']['siacode'];								// 4-8 Mittente: stessi dati presenti sul record di testata
        // !!! Parametrizzare
        $recordDiCoda .= str_pad($bank['Banks']['abi'],5,'0',STR_PAD_LEFT);					// 9-13  ricevente: stessi dati presenti sul record di testata
        $recordDiCoda .= date("dmy");														// 14-19 data creazione : stessi dati presenti sul record di testata
        // !!! Parametrizzare il numero in base ai flussi creati giornalmente
        $recordDiCoda .= str_pad('000001',20,' ',STR_PAD_RIGHT);							// 20-39 Nome supporto : stessi dati presenti sul record di testata
        $recordDiCoda .= str_repeat(' ',6); 												// 40-45 campo a disposizione : campo a disposizone dell'azienda mittente
        $recordDiCoda .= str_pad($i,7,'0',STR_PAD_LEFT);									// 46-52 Numero di dispozioni : numero delle ricevute riba contentute nel flusso
        $recordDiCoda .= str_pad(str_replace('.','',$totaleRiba),15,'0',STR_PAD_LEFT); 		// 53-67 tot. importi negativi : importo totale in centesimi di euro delle disposizioni contenute nel flusso
        $recordDiCoda .= str_repeat('0',15);										        // 68-82 tot. importi positivi : valorizzato con "0"
        $recordDiCoda .= str_pad($numerorecord, 7,'0',STR_PAD_LEFT);						// 83-89 numero record: numero dei record che compongolo il flusso (comprensivo di testa e di coda)
        $recordDiCoda .= str_repeat(' ',24);												// 90-113 filler : blank
        $recordDiCoda .= 'E';																// 114 codice divisa : deve assumere lo stesso valore del campo omonimo presente sul record di testa
        $recordDiCoda .= str_repeat('0',6); 												// 115-120 campo non disponibile: campo non utilizzabile per l'inserimento di informazioni

        fwrite($csvFile, $recordDiCoda."\r\n");
        fclose($csvFile);
	}
}
?>