<?php
App::uses('AppController', 'Controller');

class StoragemovementsController extends AppController
{

	public function index($storageId = null)
    {
        $this->loadModel('Utilities');
        $this->Utilities->loadModels($this, ['Catalog', 'Storagemovement', 'Storages']);

        $conditionsArray = ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.storage_id'=>$storageId];
        //$filterableFields = ['description', null, null,null];
		$filterableFields = [null, null, null,null];
        $sortableFields = [['description', 'Movimento'], ['movement_time','Ora Movimento'],['deposit_name','Deposito'], ['quantity','Quantità movimentata']];

        if ($this->request->is('ajax') && isset($this->request->data['filters'])) {
            $conditionsArray = $this->Utilities->buildConditions($conditionsArray, $filterableFields, $this->request->data['filters']);
        }

        // Generazione XLS
        if (isset($_POST['data']['createCsv']) && $_POST['data']['createCsv'] == 'xls') {
            $this->autoRender = false;
            $conditionsArray['Storagemovement.storage_id'] = explode(",", $_POST['data']['arrayToPost']);
            $dataForXls = $this->Storagemovement->find('all', ['conditions' => $conditionsArray, 'order' => ['Storagemovement.description' => 'asc']]);
            echo 'Movimento;Ora Movimento;Deposito;Quantita\' movimentata;' . "\r\n";
            foreach ($dataForXls as $xlsRow) {
                echo $xlsRow['Storagemovement']['description'] . ';' . $xlsRow['Storagemovement']['movement_time'] . ';'. 'Principale' . ';' . $xlsRow['Storagemovement']['quantity'] . ';' . "\r\n";
            }
        }
        else {
            $this->set('filterableFields', $filterableFields);
            $this->set('sortableFields', $sortableFields);

            $this->paginate = ['conditions' => $conditionsArray,'order'=>'movement_time desc'];
            $this->set('storagemovements', $this->paginate());
            $this->set('storageId', $storageId);
        }
    }

/*	public function add()
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Units']);

		if ($this->request->is('post')) 
		{
			$this->Units->create();
				$this->request->data['Units']['company_id']=MYCOMPANY;
				if ($this->Units->save($this->request->data)) 
				{
					$this->Session->setFlash(__('Unità di misura salvata'), 'custom-flash');
					$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\'unità di misura non è stata salvata'), 'custom-danger');
			}
		}
	}

	public function edit($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Units']);
		
		$this->Units->id = $id;
		if (!$this->Units->exists()) {
			throw new NotFoundException(__('Unità di misura non valida'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Units->save($this->request->data)) {
				$this->Session->setFlash(__('Unità di misura salvata'), 'custom-flash');
				$this->redirect(['action' => 'index']);
			} else {
				$this->Session->setFlash(__('L\' unità di misura non salvata, riprovare'), 'custom-danger');
			}
		} else {
			$this->request->data = $this->Units->read(null, $id);
		}
	}


	public function delete($id = null) 
	{
		$this->loadModel('Utilities');
		$this->Utilities->loadModels($this,['Messages','Units']);

        $asg =  ["l'","Unità di misura","F"];
		if($this->Units->isHidden($id))
			throw new Exception($this->Messages->notFound($asg[0], $asg[1],$asg[2]));

		$this->request->allowMethod(['post', 'delete']);
		
        $currentDeleted = $this->Units->find('first',['conditions'=>['Units.id'=>$id,'Units.company_id'=>MYCOMPANY]]);
        if ($this->Units->hide($currentDeleted['Units']['id'])) 
	      	$this->Session->setFlash(__($this->Messages->successOfDelete($asg[0], $asg[1],$asg[2])), 'custom-flash');
        else
           $this->Session->setFlash(__($this->Messages->failOfDelete($asg[0], $asg[1],$asg[2])), 'custom-danger');
		return $this->redirect(['action' => 'index']);
	}*/
	

}
