<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Entrata merce','indexelements' => ['add'=>'Nuovo entrata merce']]); ?>

	<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
		</thead>
		<?php
			if (count($loadgoods) > 0) {
			$currentYear = date("Y");
			?>
			<tr>
			<?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
			'htmlElements' => [
				'<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
				'<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>'
				]]);
			?></tr>
		<?php }
			else
			{
				?><tr><td colspan="10" style="text-align: center">nessun entrata merce trovata</td></tr><?php
			}
		?>
		<tbody class="ajax-filter-table-content">
		<?php
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
		foreach ($loadgoods as $loadgood)
		{ ?>
			<tr>
				<td><?= $loadgood['Loadgood']['load_good_number']; ?> / <?= substr($loadgood['Loadgood']['load_good_date'],0,4); ?></td>
				<td><?= $this->Time->format('d-m-Y', $loadgood['Loadgood']['load_good_date']); ?></td>
				<td class="table-max-width uk-text-truncate">
					<?php echo $loadgood['Supplier']['name']; ?>
				</td>
			<td style="text-align:right;">
				<?= number_format($Utilities->getLoadgoodTotalTaxableIncome($loadgood['Loadgood']['id']),2,',','.'); ?>
			</td>
			<td style="text-align:right;">
			<?= number_format($Utilities->getLoadgoodTotalVat($loadgood['Loadgood']['id']),2,',','.'); ?>
		</td>
		<td style="text-align:right;">
			<?= number_format($Utilities->getLoadgoodTotal(($loadgood['Loadgood']['id']),2,',','.')); ?>
		</td>
		<td class="table-max-width uk-text-truncate"><?= $loadgood['Loadgood']['load_good_note']; ?></td>
		<td class="table-max-width uk-text-truncate"><?= $loadgood['Loadgood']['ddt_number']; ?></td>
		<td class="table-max-width uk-text-truncate"><?= ($loadgood['Loadgood']['ddt_date'] != '0000-00-00' &&  $loadgood['Loadgood']['ddt_date'] != '1970-01-01' &&   $loadgood['Loadgood']['ddt_date'] != null ) ? date("d-m-Y",strtotime($loadgood['Loadgood']['ddt_date'])) : null; ?></td>
		<td class="actions" style="text-align:left;">
			<?php
				if($loadgood['Loadgood']['buy_bill_created'] == 0)
				{
					echo $this->Html->link($iconaModifica, ['action' => 'edit', $loadgood['Loadgood']['id']], ['title'=>__('Modifica entrata merce'),'escape' => false]);
					echo $this->Form->postLink($iconaGeneraFatturaAcquisto, ['action' => 'createBuyBillsFromLoadGoods', $loadgood['Loadgood']['id']], ['title'=>__('Genera fattura acquisto'),'escape' => false], __('Generare la fattura d\'acquisto per l\'entrata merce selezionata ? ', $loadgood['Loadgood']['id']));
					echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $loadgood['Loadgood']['id']], ['title'=>__('Elimina entrata merce'),'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $loadgood['Loadgood']['id']));
				}
				else
				{
					echo 'Fattura acquisto generata';
				}
			?>
		</td>
	</tr>
<?php } ?>

</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
	<div id="element"></div>

<?=  $this->element('Js/datepickercode'); ?>
