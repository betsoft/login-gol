<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>

<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<script>
    $(document).ready
    (
        function()
        {
         	// Nascondo a priori
         	$("#differentAddress").hide();
         	$("#differentAddressreseller").hide();
            $("#Good0Description").prop('required',true);

            var fornitori = setSuppliers();
            var articoli = setArticles();
            console.log(articoli);
            var codici = setCodes();

            // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori,"#LoadgoodSupplierId");

            loadSupplierCatalog(articoli,"Good","0","entratamerce",codici);

            $('#BillImporto').hide();

            // Abilita il clonable [è giusto billclientiid perché se undefined prende il supplier]
            enableCloning($("#BillClientId").val(),"#LoadgoodSupplierId");

            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );
</script>
    <?= $this->Form->create('Loadgood', ['class' => 'uk-form uk-form-horizontal' ]); ?>
      <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Nuova entrata merce</div>
      <div class="col-md-12"><hr></div>
      <?php $attributes = ['legend' => false]; ?>

     <div class="form-group col-md-12">
	   <div class="col-md-4" style="float:left;" >
	        <label class="form-label"><strong>Numero entrata merce</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <?php
                echo $this->Form->input('load_good_number', ['label' => false,'value' => $n_fatt,'class' => 'form-control','required'=>true]);
          ?>
       </div>
     </div>
    <div class="col-md-4" >
        <label class="form-label form-margin-top"><strong>Data entrata merce</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
              <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Loadgood][date]" value="<?= date("d-m-Y"); ?>"  required />
        </div>
      </div>
    <div class="col-md-4" >
        <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
         <?php
            echo $this->Form->input('supplier_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','required'=>true,'maxlength'=>false]);
        ?>
       </div>
      </div>

     </div>

    <div class="form-group col-md-12">
        <div class="col-md-4">
            <label class="form-label"><strong>Deposito</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->select('deposit_id', $deposits, ['value'=>$mainDeposit['Deposits']['id'],'class' => 'form-control','required'=>true,'empty'=>false]); ?>
            </div>
        </div>
        <?php
            if(MODULO_CANTIERI)
            {
               ?>
                <div class="col-md-3" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top">
                        <strong>Cantiere</strong>
                    </label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'constructionsite_id',
                            "aggregator" => '',
                            "prefix" => "constructionsite_quote",
                            "list" => $constructionsites,
                            "options" => [ 'multiple' => false,'required'=> false],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-3" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'quote_id',
                            "aggregator" => '',
                            "prefix" => "quote_id",
                            "list" => $quotes,
                            "options" => [ 'multiple' => false,'required'=> false],
                        ]);
                        ?>
                    </div>
                </div>
               <?php
            }
        ?>
    </div>

      <div class="form-group col-md-12">
          <div class="col-md-4" >
            <label class="form-label form-margin-top"><strong>Numero bolla</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('ddt_number', ['div' => false,'label' => false,'class' => 'form-control']);?>
            </div>
          </div>
          <div class="col-md-4" >
            <label class="form-label form-margin-top"><strong>Data bolla</strong></label>
            <div class="form-controls">
                  <input  type="datetime" id="datepicker2" class="datepicker segnalazioni-input form-control" name="data[Loadgood][ddt_date]"  />
            </div>
          </div>
          <div class="col-md-4" >
            <label class="form-label form-margin-top"><strong>Nota</strong></label>
            <div class="form-controls">
              <?= $this->Form->input('load_good_note', ['div' => false,'label' => false,'class' => 'form-control']);?>
              </div>
          </div>
      </div>

      <div class="col-md-12"><hr></div>
      <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe entrata merce</div>
         <div class="col-md-12"><hr></div>
        <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
            <fieldset id="fatture"  class="col-md-12">
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden></span>
          <div class="col-md-12">
          <!-- Non esiste versione semplificata -->
           <div class="col-md-2">
             <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo']]); ?>
           </div>

        <div class="col-md-2" >
             <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'required'=>true]); ?>
           </div>

           <div class="col-md-4">
             <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.description', ['div' => false,'label' => false,'class' => 'form-control goodDescription jsDescription']); ?>
             <?= $this->Form->hidden('Good.0.storage_id'); ?>
             <?= $this->Form->hidden('Good.0.movable',['class'=>'jsMovable','value'=>1]); ?>
             <?= $this->Form->hidden('Good.0.variation_id',['type'=>'text']); ?>

           </div>
            <div class="col-md-2">
              <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
              <?php echo $this->Form->input('Good.0.quantity', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity','type'=>"number",'step'=>"0.001",'required'=>true, 'min' => '0']);?>
           </div>

             <div class="col-md-2">
             <label class="form-margin-top"><strong>Unità di misura</strong></label>
                     <?=  $this->Form->input('Good.0.unit_of_measure_id', ['label' => false,'type'=>'select','class' => 'form-control', 'options' => $units, 'empty'=>true]); ?>
           </div>

           <div class="col-md-2">
             <label class="form-margin-top "><strong>Costo</strong><?php if($_SESSION['Auth']['User']['dbname'] != "login_GE0047"){?><i class="fa fa-asterisk"></i><?php } ?></label>
             <?php //  $this->Form->input('Good.0.load_good_row_price', ['label' => false,'class' => 'form-control jsPrice','type'=>'number','min'=>'0','step'=>'0.01','required'=>true]); ?>
             <?=  $_SESSION['Auth']['User']['dbname'] == "login_GE0047" ? $this->Form->input('Good.0.load_good_row_price', ['label' => false,'class' => 'form-control jsPrice','min'=>'0','step'=>'0.0001','type'=>'number', 'required'=>false]) : $this->Form->input('Good.0.load_good_row_price', ['label' => false,'class' => 'form-control jsPrice','min'=>'0','step'=>'0.01','type'=>'number', 'required'=>true]) ?>
           </div>

           <div class="col-md-2">
             <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
             <?= $this->Form->input('Good.0.load_good_row_vat_id', ['label' => false,'class' => 'form-control','type'=>'select','options'=>$vats,'empty'=>true,'required'=>true]);?>
           </div>
           <div class="col-md-2">
           <label class="form-margin-top form-label"><strong>Importo</strong></label>
               <?=
               $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true, 'value' => number_format('Good.0.load_good_row_price' * 'Good.0.quantity', 2, ',', '')]); ?>
               <!--$this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]);-->
           </div>
           <div class="col-md-2">
             <label class="form-label"><strong>Codice a barre</strong></label>
             <?= $this->Form->input('Good.0.barcode', ['div' => false,'label' => false,'class' => 'form-control goodBarcode']); ?>
           </div>
           <div class="col-md-2">
             <label class="form-label"><strong>Prezzo di vendita</strong><i class="fa fa-question-circle jsLoadGoodPrice" style="color:#589ab8;cursor:pointer;"></i></label>
             <?= $this->Form->input('Good.0.suggestedsellprice', ['div' => false,'label' => false,'class' => 'form-control']); ?>
           </div>
          </div>

          <div class="col-md-12"><hr></div>
        </div>
        </fieldset>
        <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
      </div>
        	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
    <?=  $this->Form->end(); ?>

<?= $this->element('Js/datepickercode'); ?>

<script>

        // Inizio codice per gestione prezzo/quantity/tipo
        $(".jsPrice").change(function(){setImporto(this); });
        $(".jsQuantity").change(function(){setImporto(this);  });

        function setImporto(row)
        {
            var quantity = $(row).parents(".clonableRow").find(".jsQuantity").val();
            var prezzo = $(row).parents(".clonableRow").find(".jsPrice").val();
            var importo = quantity * prezzo;
            $(row).parents(".clonableRow").find(".jsImporto").val(importo.toFixed(2));
        }

        function setCodeRequired(row)
        {
            var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
            if(tipo == 1)
            {
                $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
                $(row).parents(".clonableRow").find(".jsMovable").val(1);
            }
            else
            {
                $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
                $(row).parents(".clonableRow").find(".jsMovable").val(0);
            }
        }
        // Fine codice per gestione prezzo/quantity/tipo

     $("#LoadgoodAddForm").on('submit.default',function(ev)
     {
     });

     $("#LoadgoodAddForm").on('submit.validation',function(ev)
     {
        ev.preventDefault(); // to stop the form from submitting
        checkLoadGoodDuplicate();
     });
</script>
 <script>

    function checkLoadGoodDuplicate()
    {
        $.ajax
        ({
		    method: "POST",
			url: "<?= $this->Html->url(["controller" => "loadgoods","action" => "checkLoadGoodDuplicate"]) ?>",
    		data:
    		{
    		    loadgoodnumber : $("#LoadgoodLoadGoodNumber").val(),
    			date : $("#datepicker").val(),
    		},
    		success: function(data)
    		{
    		    errore = 0;
                if(data > 0)
                {
                    errore = 4;
                }
                switch (errore)
                {
                   case 0:
                       $("#LoadgoodAddForm").trigger('submit.default');
                       break;
                   case 4:
                        $.alert
                        ({
        					icon: 'fa fa-warning',
	        			    title: '',
    	    			    content: 'Attenzione esiste già una entrata merce con lo stesso numero nell\'anno di competenza.',
    		    		    type: 'orange',
				        });
                     return false;
                  break;
                }
			},
        	error: function(data)
    		{
    		}
    	});


    }

    $(".jsLoadGoodPrice").click(function() { showHelpMessage('loadgoodprice'); });
    	function showHelpMessage(message)
	{
		switch(message)
		{
			case 'loadgoodprice':
			    $.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "Questo campo serve <b><span style=\"color:#589ab8\">unicamente</span></b> per sovrascrivere il prezzo di vendita suggerito per l'articolo in questione. Questo dato non verrà salvato nell'entrata merce e quindi non sara visible nella schermata di modifica.",
        			type: 'blue',
				});
			break;
		}
	}
    </script>
