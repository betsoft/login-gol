<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<?= $this->Form->create('Loadgood', ['class' => 'uk-form uk-form-horizontal']); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= 'Crea fattura da più entrate merci'; ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data entrata merce dal</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateFrom" class="datepicker segnalazioni-input form-control" name="data[Loadgood][load_good_date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data entrata merce al </strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateTo" class="datepicker segnalazioni-input form-control" name="data[Loadgood][lood_good_date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'client_id',
                "aggregator" => '',
                "prefix" => "supplier_id",
                "list" => $suppliers,
                "options" => ['multiple' => false, 'required' => true],
            ]);
            ?>
        </div>
    </div>
    <div class="row col-md-4">
        <label class="form-label form-margin-top"></label>
        <div class="form-controls">
            <div class=" col-md-1 blue-button ebill sendbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Crea Fattura</div>
        </div>
    </div>
</div>

<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>



<script>
    $(".sendbillixfe").click(
        function()
        {
            addWaitPointer();
            var datastart = $("#DateFrom").val();
            var dataend = $("#DateTo").val();
            var supplierId = $("#supplier_id_multiple_select").val();
            var diffDays = getDateDiff(dataend,datastart);

            if(diffDays < 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: 'Creazione Fattura',
                    content: 'Attenzione la data di fine non può precedere la data di inizio.',
                    type: 'orange',
                });

                removeWaitPointer();
            }
            else
            {

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "loadgoods","action" => "multiLoadgoodBill"]) ?>",
                    async : true,
                    data:
                        {
                            dateFrom: datastart,
                            dateTo: dataend,
                            supplierId: supplierId,
                        },
                    success: function(data)
                    {
                        window.location.assign("<?= $this->Html->url(["controller" => "bills", "action" => "edit"]) ?>" + "/" + data);
                        removeWaitPointer();
                    },
                    error: function(data)
                    {
                        removeWaitPointer();
                    }
                })
            }
        }
    );
</script>
