<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->element('Form/Components/FilterableSelect/loader') ?>

<script>
    $(document).ready
    (
        function () {
            var fornitori = setSuppliers();
            var articoli = setArticles();
            var codici = setCodes();

            // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori, "#LoadgoodSupplierId");

            //loadSupplierCatalog(articoli,"Loadgoodrow","0","Description","entratamerce",codici);
            loadSupplierCatalog(articoli, "Loadgoodrow", "0", "entratamerce", codici);

            enableCloningedit(null, "#LoadgoodSupplierId");

            addcrossremoving();
        }
    );
</script>

<?php $prezzo_beni_riga = $totale_imponibile = $totale_imposta = 0 ?>

<?= $this->Form->create('Loadgood', ['class' => 'uk-form uk-form-horizontal']); ?>
<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Modifica Entrata merce</div>

<div class="col-md-12"><hr></div>
<?= $this->Form->hidden('id'); ?>

<?= $this->Form->hidden('Supplier.name', ['value' => $this->request->data['Supplier']['name']]); ?>
<div class="form-group col-md-12">
    <div class="col-md-2">
        <label class="form-label form-margin-top"><strong>Numero entrata merce</strong><i
                    class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?php // $this->Form->input('load_good_number', ['div' => false,'label' => false,'class' => 'form-control', 'required'=>true]); ?>
            <?= $this->Form->input('load_good_number', ['label' => false, 'class' => 'form-control', 'required' => true]); ?>
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Data entrata merce</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control"
                   name="data[Loadgood][load_good_date]"
                   value="<?= $this->Time->format('d-m-Y', $this->data['Loadgood']['load_good_date']); ?>">
        </div>
    </div>
    <div class="form-group col-md-4">
        <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'supplier_id',
                "aggregator" => '',
                "prefix" => "supplier_id",
                "list" => $suppliers,
                "options" => ['multiple' => false, 'required' => false],
            ]); ?>
        </div>
    </div>
</div>

<?= $this->Form->hidden('deposit_id', ['value' => $this->request->data['Loadgood']['deposit_id'], 'class' => 'form-control']); ?>

<div class="form-group col-md-12">
    <div class="col-md-4">
        <label class="form-label"><strong>Deposito</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->select('deposit_id', $deposits, ['value' => $mainDeposit['Deposits']['id'], 'class' => 'form-control', 'required' => true, 'empty' => false]); ?>
        </div>
    </div>
    <?php
    if (MODULO_CANTIERI) {
        ?>
        <div class="col-md-3" style="float:left;">
            <label class="form-label form-margin-top">
                <strong>Cantiere</strong>
            </label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'constructionsite_id',
                    "aggregator" => '',
                    "prefix" => "constructionsite_quote",
                    "list" => $constructionsites,
                    "options" => ['multiple' => false, 'required' => false],
                ]);
                ?>
            </div>
        </div>
        <div class="col-md-3" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'quote_id',
                    "aggregator" => '',
                    "prefix" => "quote_id",
                    "list" => $quotes,
                    "options" => ['multiple' => false, 'required' => false],
                ]);
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="form-group col-md-12">
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Numero Bolla</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('ddt_number', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Data bolla</strong></label>
        <div class="form-controls">
            <?php ($this->data['Loadgood']['ddt_date'] != '0000-00-00' && $this->data['Loadgood']['ddt_date'] != '1970-01-01' && $this->data['Loadgood']['ddt_date'] != null) ? $dataValue = $this->Time->format('d-m-Y', $this->data['Loadgood']['ddt_date']) : $dataValue = ''; ?>
            <input type="datetime" id="datepicker2" class="datepicker segnalazioni-input form-control"
                   name="data[Loadgood][ddt_date]" value=<?= $dataValue; ?>>
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Nota</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('load_good_note', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
</div>

<div class="col-md-12">
    <hr>
</div>
<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 "
         style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe entrata merce
    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>

    <fieldset id="fatture" class="col-md-12">

        <?php
        $conto = count($this->request->data['Loadgoodrow']) - 1;
        //  $this->data = $Loadgoods;
        $i = -1;
        ?>
        <script>
            var articoli = setArticles();
            var codici = setCodes();
        </script>
        <?php
        foreach ($this->request->data['Loadgoodrow'] as $chiave => $oggetto) {
            $i++;
            $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
            $chiave == 0 ? $classe1 = "originale" : $classe1 = '';
            ?>
            <!-- Lunghezza serve per l'editclonable non cancellare -->
            <!--div class="principale<?= $oggetto['id'] ?> lunghezza clonableRow contacts_row   <?php // $classe1
            ?>  <?php // $classe
            ?>" id="' <?php // $chiave
            ?> '"-->
            <div class="principale lunghezza clonableRow contacts_row   <?= $classe1 ?>  <?= $classe ?>"
                 id="' <?= $chiave ?> '">
                <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove"
                      title="Rimuovi riga"></span>
                <?= $this->Form->input("Loadgoodrow.$chiave.id"); ?>
                <div class="col-md-12">
                    <!-- non esiste versione semplificata -->
                    <div class="col-md-2">
                        <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                        <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '0'; ?>
                        <?= $this->Form->input("Loadgoodrow.$chiave.tipo", ['required' => true, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo'], 'value' => $movableValue, 'disabled' => true]); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Loadgoodrow.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'readonly' => true, 'style' => 'background-color:#fafafa;', 'required' => true]); ?>
                    </div>
                    <div class="col-md-4">
                        <label class="form-margin-top"><strong>Descrizione</strong><i
                                    class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Loadgoodrow.$chiave.description", ['div' => false, 'label' => false, 'class' => 'form-control  jsDescription', 'readonly' => true, 'style' => 'background-color:#fafafa;']); ?>
                        <?= $this->Form->hidden("Loadgoodrow.$chiave.storage_id"); ?>
                        <?= $this->Form->hidden("Loadgoodrow.$chiave.movable", ['class' => 'jsMovable', 'value' => $movableValue]); ?>
                        <?= $this->Form->hidden("Loadgoodrow.$chiave.variation_id"); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?php
                        echo $this->Form->input("Loadgoodrow.$chiave.quantity", ['label' => false, 'class' => 'form-control jsQuantity', 'type' => 'number', 'step' => '0.001', 'min' => '0']); ?>
                    </div>

                    <div class="col-md-2">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input("Loadgoodrow.$chiave.unit_of_measure_id", ['label' => false, 'class' => 'form-control', 'div' => false, 'options' => $units, 'empty' => true]); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-margin-top "><strong>Costo</strong><i class="fa fa-asterisk"></i></label>
                        <?php //  $this->Form->input("Loadgoodrow.$chiave.load_good_row_price", ['label' => false,'class' => 'form-control jsPrice','type'=>'number']);
                        ?>
                        <?= $this->Form->input("Loadgoodrow.$chiave.load_good_row_price", ['label' => false, 'class' => 'form-control jsPrice', 'type' => 'number', 'min' => '0', 'step' => '0.01']); ?>
                    </div>


                    <div class="col-md-2">
                        <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Loadgoodrow.$chiave.load_good_row_vat_id", ['label' => false, 'class' => 'form-control', 'type' => 'number', 'type' => 'select', 'options' => $vats, 'empty' => true]); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-margin-top form-label"><strong>Importo</strong></label>
                        <?= $this->Form->input("Good.$chiave.importo", ['label' => false, 'class' => 'form-control jsImporto', 'disabled' => true, 'value' => number_format($oggetto['quantity'] * $oggetto['load_good_row_price'], 2, ',', '')]); ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-label"><strong>Codice a barre</strong></label>
                        <?php
                        echo $this->Form->input("Loadgoodrow.$chiave.barcode", ['div' => false, 'label' => false, 'class' => 'form-control goodBarcode']);
                        ?>
                    </div>
                    <div class="col-md-2">
                        <label class="form-label"><strong>Prezzo di vendita</strong></label>
                        <?= $this->Form->input("Loadgoodrow.$chiave.suggestedsellprice", ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
                    </div>
                    <div class="row" style="padding-bottom:10px;">
                        <hr>
                    </div>
                </div>
            </div>

            <script>
                loadSupplierCatalog(articoli, "Loadgoodrow", "<?php echo $chiave; ?>", "entratamerce", codici);
            </script>
        <?php } ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>
    <center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
    <?php echo $this->Form->end(); ?>

    <?= $this->element('Js/datepickercode'); ?>

    <script>

        // Inizio codice per gestione prezzo/quantity/tipo
        $(".jsPrice").change(function () {
            setImporto(this);
        });
        $(".jsQuantity").change(function () {
            setImporto(this);
        });
        $(".jsTipo").change(function () {
            setCodeRequired(this);
        });

        /*function setImporto(row)
        {
            var quantity = $(row).parents(".clonableRow").find(".jsQuantity").val();
            var prezzo = $(row).parents(".clonableRow").find(".jsPrice").val();
            var importo = quantity * prezzo;
            $(row).parents(".clonableRow").find(".jsImporto").val(importo.toFixed(2));
        }*/

        /*function setCodeRequired(row)
        {
            var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
            if(tipo == 1)
            {
                $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
                $(row).parents(".clonableRow").find(".jsMovable").val(1);
            }
            else
            {
                $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
                $(row).parents(".clonableRow").find(".jsMovable").val(0);
            }
        }*/

    </script>

    <script>

        $("#LoadgoodEditForm").on('submit.default', function (ev) {
        });

        $("#LoadgoodEditForm").on('submit.validation', function (ev) {
            ev.preventDefault(); // to stop the form from submitting
            checkLoadgoodDuplicate()
        });
    </script>

    <script>

        function checkLoadgoodDuplicate() {
            var errore = 0;

            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "loadgoods", "action" => "checkLoadgoodDuplicate"]) ?>",
                data:
                    {
                        loadgoodnumber: $("#LoadgoodLoadGoodNumber").val(),
                        date: $("#datepicker").val(),
                    },
                success: function (data) {
                    console.log(data);
                    if (data > 0 && ($("#LoadgoodLoadGoodNumber").val() != '<?= $this->data['Loadgood']['load_good_number']; ?>')) {
                        errore = 4;
                    }
                    console.log(errore);
                    switch (errore) {
                        case 0:
                            $("#LoadgoodEditForm").trigger('submit.default');
                            break;
                        case 4:
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                                title: '',
                                content: 'Attenzione esiste già un entrata merce con lo stesso numero nell\'anno corrente.',
                                type: 'orange',
                            });
                            return false;
                            break;
                    }
                },
                error: function (data) {
                }
            });
        }
    </script>
