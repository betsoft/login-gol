<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Intervalli spese d\'incasso'); ?></span>
	</div>
	<div class=" -badge tools">
		<?php echo $this->Html->link('<i class="uk-icon-plus-circle icon"></i>', ['action' => 'add'],array('title'=>__('Nuovo vettore'),'escape'=>false)); ?>
	</div>
</div>
<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?php echo $this->Paginator->sort('Importo da'); ?></th>
					<th><?php echo $this->Paginator->sort('Importo a'); ?></th>
					<th><?php echo $this->Paginator->sort('Importo spese di incasso'); ?></th>
					<th class="actions"><?php echo __('Azioni'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($collectionfee as $fee) 
				{
				?>
					<tr>
						<td><?php echo h($fee['Collectionfee']['from_amount']); ?></td>
						<td><?php echo h($fee['Collectionfee']['to_amount']); ?></td>
						<td><?php echo h($fee['Collectionfee']['fee']); ?></td>
						<td class="actions">
							<?=  $this->element('Form/Simplify/Actions/edit',['id' => $fee['Collectionfee']['id']]); ?>
							<?php
								echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $fee['Collectionfee']['id']], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler cancellare il vettore ?', $fee['Collectionfee']['id']));
							?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<p class="uk-text-center">
			<?php echo $this->Paginator->counter(array('format' => __('Pagina {:page} di {:pages}'))); ?>
		</p>
		<div class="paging uk-text-center">
			<?php
				echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
				echo $this->Paginator->numbers(array('separator' => ' - '));
				echo $this->Paginator->next('&nbsp; <i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
			?>
		</div>
	</div>
</div>
