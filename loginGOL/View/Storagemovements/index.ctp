<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'movimenti di magazzino','indexelements' => ['add'=>'Nuova unità di misura']]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($storagemovements) > 0) { ?>
				<?php }
				else
				{
					?><tr><td colspan="2"><center>nessun movimento trovato</center></td></tr><?php				
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($storagemovements as $storagemovement)
				{
					$storagemovement['Storagemovement']['description'] = str_replace('[TR_DEP_SC]','',$storagemovement['Storagemovement']['description']);
                    $storagemovement['Storagemovement']['description'] = str_replace('[TR_DEP_CA]','',$storagemovement['Storagemovement']['description']);
				?>
					<tr>
						<td><?= h($storagemovement['Storagemovement']['description']); ?></td>
                        <td style="text-align:center;"><?= h(date("d-m-Y H:i:s",strtotime($storagemovement['Storagemovement']['movement_time']))); ?></td>
						<td><?= h($storagemovement['Deposit']['deposit_name']); ?></td>
						<td class="right"><?= h(number_format($storagemovement['Storagemovement']['quantity'],2,',','')); ?></td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>

<script>
    var arrayToPost = "<?= $storageId; ?>";
</script>
