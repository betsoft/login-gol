<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Importa Clienti']); ?>
<div><br/></div>
<?= $this->Form->create('Clients', ['url' => ['action' => 'importClientFromXml', 'controller' => 'clients'], 'type' => 'file', 'class' => 'uk-form uk-form-horizontal']); ?>
<div class="row">
    <div class="col-md-4" style="float:left;margin-left:10px;">
        <?= $this->Form->input('file', ['type' => 'file', 'class' => 'form-control', 'required'=>true, 'accept'=>'.xml']); ?>
        <?= $this->Form->end(' Importa ', array('label' => false, 'type' => 'submit', 'class' => 'btn btn-primary', 'id'=>'clickSubmit')); ?>

    </div>
</div>
<div><br/></div>
<?= $this->Form->end(); ?>
<style>
    .submit > input{
        -webkit-text-size-adjust: 100%;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        line-height: 1.42857143;
        direction: ltr;
        box-sizing: border-box;
        text-decoration: none;
        border-radius: 0 !important;
        text-shadow: none;
        color: #fff !important;
        width: 15%;
        font-size: 15px;
        font-weight: 500 !important;
        border: 1px solid #ffffff;
        margin-left: 5px;
        padding: 10px !important;
        margin-right: 20px;
        font-family: 'Barlow Semi Condensed' !important;
        text-transform: none;
        background-color: #ea5d0b;

    }
    .submit > input:hover{
        background-color: #589AB8 !important;
    }
</style>
