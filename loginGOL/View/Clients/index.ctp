<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?php
if($_SESSION['Auth']['User']['dbname'] == 'login_GE0046')
    echo $this->element('Form/formelements/indextitle',['indextitle'=>'Clienti','indexelements' => ['importClientFromXml' => 'Importa clienti', 'add'=>'Nuovo cliente']]);
else if($_SESSION['Auth']['User']['dbname'] == 'login_GE0051' || $_SESSION['Auth']['User']['dbname'] == 'login_GE0052')
    echo $this->element('Form/formelements/indextitle', ['indextitle'=>'Clienti', 'indexelements'=>['getClientFromArxivar'=>'Importa Clienti da Arxivar', 'add'=>'Nuovo Cliente']]);
else
    echo $this->element('Form/formelements/indextitle',['indextitle'=>'Clienti','indexelements' => ['add'=>'Nuovo cliente']]);
?>
<div class="clients index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
		</thead>
		<tbody class="ajax-filter-table-content">
        <?php
			if (count($clients) == 0)
			{
				?><tr><td colspan="11"><center>Nessun cliente trovato</center></td></tr><?php
			}
			foreach ($clients as $client)
			{ ?>
				<tr>
					<td><?= h($client['Client']['code']); ?></td>
					<td class="table-max-width uk-text-truncate"><?php echo h($client['Client']['ragionesociale']); ?></td>
					<td><?= h($client['Client']['indirizzo']); ?></td>
					<td><?= h($client['Client']['cap']); ?></td>
					<td><?= h($client['Client']['citta']); ?></td>
					<td><?= h($client['Client']['provincia']); ?></td>
					<td><?= h($client['Client']['telefono']); ?></td>
					<td><?= h($client['Client']['mail']); ?></td>
					<td><?= h($client['Client']['piva']); ?></td>
					<td><?= h($client['Client']['cf']); ?></td>
					<td class="actions">
						<?= $this->element('Form/Simplify/Actions/edit',['id' => $client['Client']['id']]); ?>
						<?php
							echo $this->Html->link($iconaDestini, array('controller'=>'Clientdestinations','action' => 'index', $client['Client']['id']),array('title'=>__('Gestione destini diversi'),'escape'=>false));
							if(ADVANCED_STORAGE_ENABLED)
							{
								echo $this->Html->link($iconaLisitini, array('controller' => 'catalogs' , 'action' => 'index', $client['Client']['id'], 'cliente'),array('title'=>__('Listino'),'escape'=>false));
							}
						    if(MODULO_CANTIERI)
                            {
                                if($client['Client']["state"] == 2)
                                {
                                    ?>
                                    <i class="fa fa-user jsEnableClient" style="font-size:20px;vertical-align: middle;color:#ea5d0b;cursor:pointer;" title="Abilita il cliente" clientid="<?= $client['Client']['id'] ?>"></i>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <i class="fa fa-user" style="font-size:20px;color:#23a24d;vertical-align: middle;" clientid="<?= $client['Client']['id'] ?>"></i>
                                    <?php
                                }
                            }
						?>
						<?= $this->element('Form/Simplify/Actions/delete',['id' => $client['Client']['id'],'message' => 'Sei sicuro di voler eliminare il cliente ?']); ?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?= $this->element('Form/Components/Paginator/component'); ?>
</div>

<script>

    $(".jsEnableClient").click(function()
    {
        var client =  $(this);
        $.confirm({
            title: 'Abilitazione clienti.',
            content: 'Abilitare il cliente selezionato ?',
            type: 'blue',
            buttons: {
                Ok: function () {
                    action:
                    {
                        $.ajax
                        ({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "clients", "action" => "enable"]) ?>",
                            data:
                                {
                                    clientId: $(client).attr("clientid"),
                                },
                            success: function (data)
                            {
                                $(client).css("color", "#23a24d");
                            }
                        });
                    }
                },
                annulla: function () {
                    action:
                    {
                        // Nothing
                    }
                },
            }
        });
});

</script>


