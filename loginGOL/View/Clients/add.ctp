<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?= $this->Form->create('Client'); ?>
<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?=  $this->element('Form/Components/MegaForm/loader'); ?>
<?php // $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>


<?= $this->element('Form/formelements/add_and_edit_title',['addedittitle'=>'Nuovo cliente']); ?>

<div class ="form-group col-md-12">
	<div class="col-md-2" >
		<label class="form-label form-margin-top" "required" ><strong>Codice cliente</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('code',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	<div class="col-md-6" >
		<label class="form-label form-margin-top"><strong>Nome cognome / Ragione sociale<i class="fa fa-asterisk"></i></strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('ragionesociale',['label' => false, 'div' =>false, 'class'=>'form-control','required'=>true,'pattern'=>PATTERNBASICLATIN]); ?>
		</div>
	</div>
    <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0041") { ?>
    <div class="col-md-2">
        <label class="form-label form-margin-top"><strong>Data di Nascita</strong></label>
        <div class="form-controls">
            <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Client][data_nascita]"/>
        </div>
    </div>
    <?php } ?>
</div>
<?= $this->element('Js/datepickercode'); ?>
<div class ="form-group col-md-12">
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Indirizzo</strong></label>
		<div class="form-controls">
			<?= $this->Form->input('indirizzo',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>CAP</strong></label>
   		<div class="form-controls">
			<?=  $this->Form->input('cap',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>4 ]); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Città</strong></label>
   		<div class="form-controls">
		<?= $this->Form->input('citta',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Provincia</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('provincia',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Nazione</strong></label>
   		<div class="form-controls">
			<?= $this->element('Form/Components/FilterableSelect/component', [
			    "name" => 'nation_id',
                "aggregator" => '',
                "prefix" => "nation_id",
                "list" => $nations,
                "options" => [ 'multiple' => false,'required'=> false,
                    'default'=>'106']
            ]); ?>
   		</div>
   </div>
</div>

<div class ="form-group col-md-12">
	<div class="col-md-4" >
		<label class="form-label form-margin-top"><strong>Partita IVA</strong></label>
	 	<div class="form-controls">
			<?=  $this->Form->input('piva',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
		</div>
	</div>
	<div class="col-md-4" >
		<label class="form-label form-margin-top"><strong>Codice fiscale</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('cf',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
		</div>
	</div>
	<?php if(MULTICURRENCY)	{ ?>
		<div class="col-md-2" >
			<label class="form-label form-margin-top"><strong>Valuta</strong></label>
			<div class="form-controls">
				<?= $this->Form->input('currency_id',['label' => false, 'div' =>false, 'class'=>'form-control','options'=>$currencies,'empty'=>false,'required'=>true]); ?>
			</div>
		</div>
	<?php } ?>
    <div class="col-md-2" >
        <label class="form-label form-margin-top"><strong>Sconto</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('discount',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo numeri' , 'min' => '0', 'max'=>'100', 'step'=>'0.01', 'type'=>'number']); ?>
        </div>
    </div>
</div>

<div class ="form-group col-md-12">
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Telefono</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('telefono',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Fax</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('fax',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
		</div>
	</div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Mail</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('mail',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'email']); ?>
		</div>
	</div>
    <div class="col-md-2" >
        <label class="form-label form-margin-top"><strong>Seconda mail</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('extra_mail',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'email']); ?>
        </div>
    </div>
	<div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Pec</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('pec',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'email','required'=>false]); ?>
		</div>
	</div>
</div>

<div class="col-md-12">
	<label class="form-label form-margin-top"><strong>Note</strong></label>
    <div class="col-md-12">
		<?= $this->Form->input('note',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'text','required'=>false]); ?>
    </div>
</div>
 <div class="col-md-12"><hr></div>

	<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per fatturazione</div>

	<div class ="form-group col-md-12">
   		<div class="col-md-2" >
			<label class="form-label form-margin-top"><strong>Metodo di pagamento predefinito</strong></label>
	   		<div class="form-controls">
				<?= $this->Form->input('payment_id',['label' => false, 'div' =>false, 'class'=>'form-control','empty'=>true]); ?>
			</div>
		</div>
		<div class="col-md-2">
			<label class="form-label form-margin-top"><strong>Aliquota iva predefinita</strong></label>
	   		<div class="form-controls">
				<?= $this->Form->input('vat_id',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'select','options'=>$vats,'empty'=>true]); ?>
			</div>
		</div>
		<div class="col-md-2" >
			<label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
	   		<div class="form-controls">
				<?= $this->Form->input('withholding_tax',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'number','step'=>'0.01']); ?>
			</div>
		</div>
		<div class="col-md-2" >
			<label class="form-label form-margin-top"><strong>Split payment</strong><i class="fa fa-question-circle jsSplitpayment" style="color:#589ab8;cursor:pointer;"></i></label>
	   		<div class="form-controls">
				<?= $this->Form->input('splitpayment',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'select','options'=>['0'=>'NO','1'=>'SI']]); ?>
			</div>
		</div>
		<div class="col-md-2" >
			<label class="form-label form-margin-top"><strong>Posticipa scadenze 31/08 e 31/12</strong><i class="fa fa-question-circle jsBillsdelay" style="color:#589ab8;cursor:pointer;"></i></label>
   			<div class="form-controls">
				<?= $this->Form->input('billdeadlineaugustanddecemeber',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'select','options'=>['0'=>'NO','1'=>'SI']]); ?>
			</div>
		</div>
	</div>

 <div class="col-md-12"><hr></div>
	<?php if(RIBA_FLOW) { ?>
		<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per riba</div><br/>
	<div class="col-md-12" >
		<div class="col-md-4" >
			<label class="form-label form-margin-top"><strong>Banca - filiale</strong><i class="fa fa-question-circle jsBankDescription" style="color:#589ab8;cursor:pointer;"></i></label>
   			<div class="form-controls">
				<?=
					$this->element('Form/Components/FilterableSelect/component',
						[
							"name" => 'bank_id',
							"aggregator" => '',
							"prefix" => "bank_id",
							"list" => $banks,
							"options" =>  [  'multiple' => false ,'required'=>false ],
							"actions" =>
							[
								$this->element('Form/Components/MegaForm/component', [ 'megaFormIdSuffix' => "add_bank", 'linkClass' => 'fa fa-plus', 'buttonOnly' => true, 'buttonTitle'=>'Aggiungi una nuova banca' ])
							]
						]);

				?>
   			</div>
		</div>
	<br/><br/><br/>
	 <div class="col-md-12"><hr></div>

	<?php } ?>

		<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per Fattura elettronica</div>

		<div class="form-group col-md-12">
			<div class="col-md-4" >
				<label class="form-label form-margin-top"><strong>Codice cliente</strong><i class="fa fa-question-circle jsInvoice" style="color:#589ab8;cursor:pointer;"></i></label>
   				<div class="form-controls">
					<?= $this->Form->input('codiceDestinatario',['label' => false, 'div' =>false, 'class'=>'form-control','required'=>false]); ?>
				</div>
			</div>
			<div class="col-md-4" >
				<label class="form-label form-margin-top"><strong>Tipologia di cliente</strong></label>
	   			<div class="form-controls">
					<?= $this->Form->input('pa',['label' => false, 'div' =>false, 'class'=>'form-control','type'=>'select', 'options'=>['0'=>'Privato','1'=>'Pubblica amministrazione']]); ?>
				</div>
			</div>
		</div>


		<div class="form-group col-md-12">
			<div class="col-md-4" >
				<label class=" form-margin-top"><strong>Riferimento amministrazione "Cedente prestatore" [ Rif. 1.2.6 ]</strong></label>
   				<div class="form-controls">
					<?= $this->Form->input('ref_adm_company',['label' => false, 'div' =>false, 'class'=>'form-control','required'=>false]); ?>
				</div>
			</div>
			<div class="col-md-4" >
				<label class=" form-margin-top"><strong>Riferimento amministrazione "Dettaglio linee" [ Rif. 2.2.1.15 ]</strong></label>
   				<div class="form-controls">
					<?= $this->Form->input('ref_adm_detailrow',['label' => false, 'div' =>false, 'class'=>'form-control','required'=>false]); ?>
				</div>
			</div>
			<div class="col-md-4" >
				<label class="form-margin-top"><strong>Riferimento amministrazione "Cassa previdenziale" [ Rif. 2.1.1.7.8 ]</strong></label>
   				<div class="form-controls">
					<?= $this->Form->input('ref_adm_welfarebox',['label' => false, 'div' =>false, 'class'=>'form-control','required'=>false]); ?>
				</div>
			</div>
		</div>

	 <div class="col-md-12"><hr></div>
	<!--center><?php // $this->Form->submit(__('Salva',true), ['class'=>'btn blue-button new-bill']); ?></center-->
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
    <?= $this->Form->end(); ?>

<script>

	$(".jsBankDescription").click(function()	{ showHelpMessage('bankdescription'); });
	$(".jsInvoice").click(function()	{ showHelpMessage('invoice'); });
	$(".jsSplitpayment").click(function()	{ showHelpMessage('splitpayment'); });
	$(".jsBillsdelay").click(function()	{ showHelpMessage('billsdelay'); });

	function showHelpMessage(message)
	{
		switch(message)
		{
			case 'bankdescription':
				$.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "Nella descrizione della banca vengono indicati anche ABI e CAB per semplificare la ricerca",
        			type: 'blue'
				});
			break;
			case 'invoice':
				$.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "<?= addslashes($helperMessage['invoice']); ?>",
        			type: 'blue',
				});
			break;
			case 'splitpayment':
				$.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "<?= addslashes($helperMessage['splitpayment']); ?>",
        			type: 'blue',
				});
			break;
			case 'billsdelay':
				$.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "<?= addslashes($helperMessage['billsdelay']); ?>",
        			type: 'blue',
				});
			break;
		}
	}

</script>

<script>
     $("#ClientAddForm").on('submit.default',function(ev)
     {
     });

     $("#ClientAddForm").on('submit.validation',function(ev)
     {
        ev.preventDefault(); // to stop the form from submitting
        checkClientPivaAndCfDuplicate()
     });
</script>


<script>

function checkClientPivaAndCfDuplicate()
{
	var errore = 0;

    $.ajax
    ({
    	method: "POST",
    	url: "<?= $this->Html->url(["controller" => "clients","action" => "checkClientPivaDuplicate"]) ?>",
    	data:
    	{
    		piva : $("#ClientPiva").val(),
    	},
    	success: function(data)
    	{

    		// Controllo codice cliente e tipo cliente per fatturazione elettronica
    		var typeOfClient = $("#ClientPa").val();
    		var clientCode = $("#ClientCodiceDestinatario").val();
    		if(clientCode != '')
    		{
    			if(typeOfClient == 0 && clientCode.length != 7) { errore = 3; }
    			if(typeOfClient == 1 && clientCode.length != 6) { errore = 4; }
    		}

    		if(data > 0){ errore = 1; }
        	$.ajax
    		({
    			method: "POST",
    			url: "<?= $this->Html->url(["controller" => "clients","action" => "checkClientCfDuplicate"]) ?>",
    			data:
    			{
    				cf : $("#ClientCf").val(),
    			},
    			success: function(data)
    			{
    				if(data > 0){ errore = 2; }
		            switch (errore)
        		    {
		            	case 0:
                			$("#ClientAddForm").trigger('submit.default');
    		            break;
	                    case 1:
							 $.confirm({
    			            	title: 'Nuovo cliente.',
        		                content: 'Attenzione esiste già un cliente con la stessa partita iva, continuare comunque?',
        		                type: 'orange',
        		                buttons: {
            		            Ok: function ()
            		            {
            		            	$("#ClientAddForm").trigger('submit.default');
            		            },
            		            annulla: function ()
                                {
                                	action:
                                	{

                                	}
	                            }}
							 });
                            $("#ClientPiva").css("border-color",'1px solid red');
                    		return false;
                        case 2:
                        	 $.confirm({
    			            	title: 'Nuovo cliente.',
        		                content: 'Attenzione esiste già un cliente con lo stesso codice fiscale, continuare comunque?',
        		                type: 'orange',
        		                buttons: {
            		            Ok: function ()
            		            {
            		            	$("#ClientAddForm").trigger('submit.default');
            		            },
            		            annulla: function ()
                                {
                                	action:
                                	{

                                	}
	                            }}
							 });
                            $("#ClientCf").css("border-color",'1px solid red');
                       		return false;
                    	break;
                    	case 3:
                    		 $.alert({
    			            	title: 'Nuovo cliente.',
        		                content: 'Attenzione se il cliente è privato il "codice cliente" per la fatturazione elettronica deve essere di 7 cifre !',
        		                type: 'orange',
							 });
							 return false;
                    		break;
                    	case 4:
                    		 $.alert({
    			            	title: 'Nuovo cliente.',
        		                content: 'Attenzione se il cliente è una pubblica amministrazione il "codice cliente" per la fatturazione elettronica deve essere di 6 cifre !',
        		                type: 'orange',
							 });
                    		return false;
                    		break;
					}
   				},
           		error: function(data)
       			{
       				// Nothing
       			}
    		});
    	}
    })
}


  // Chiamata quando viene aperta dal megarform

	function addClientPageCustomLoaders(htmlContent, megaFormButton, megaForm) {
	 //	initializeFieldValueFormat(); // Posso lasciarlo vuoto
	}

	// Quando premo salva aggiorno il valore sull celalchiamata
	function addClientMegaFormsAfterSaveCallback(entityData, saveButton, megaFormButton, megaForm) {
		var changedSelect = megaFormButton.parents('.multiple-select-container').find('[id$="_multiple_select"]').first();
        var val = $.isNumeric(changedSelect.val()) || !$.isArray(changedSelect.val()) ? [changedSelect.val] : changedSelect.val();
        changedSelect.append($('<option>', {
            value: entityData.id,
            text: entityData.fullName ? entityData.fullName : entityData.description
        }))
        changedSelect.val($.merge(val, [""+entityData.id])).multiselect('rebuild');
	}

</script>

<?=
	$this->element('Form/Components/MegaForm/component',[
		'megaFormIdSuffix' => "add_bank",
		'url' => $this->Html->Url(['controller'=>'Banks', 'action'=>'add']),
		'clonableExtension' => 0,
		'linkClass' => 'fa fa-plus',
		'afterAjaxCallback' => 'loadJsForMessageFix',
		'beforePerformShowOrHideCallback' => 'addBankPageCustomLoaders', // Eventuale funzione js che carica le funzioni js apprartenenti alla pagina che viene richiamata
		'afterSaveCallback' => 'addBankMegaFormsAfterSaveCallback', // Tutto quello che deve essere eseguito dopo aver premuto il pulsante salva sulla schermata richiamata dal megaform
		'formOnly' => true
    ])
?>
