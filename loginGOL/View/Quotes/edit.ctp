<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?php $prezzo_beni_riga = $totale_imponibile = $totale_imposta = 0 ?>

<?= $this->Form->create('Quote', ['class' => 'uk-form uk-form-horizontal']); ?>
<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Modifica preventivo</div>
<div class="col-md-12"><hr></div>
<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Numero preventivo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('quote_number', ['value' => $this->request->data['Quote']['quote_number'], 'class' => 'form-control', 'label' => false]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data preventivo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="datapreventivo" class="datepicker segnalazioni-input form-control"
                   name="data[Quote][quote_date]"
                   value="<?= $this->Time->format('d-m-Y', $this->data['Quote']['quote_date']); ?>"/>
        </div>
    </div>
    <div class="col-md-5" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cliente</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'client_id',
                "aggregator" => '',
                "prefix" => "client_id",
                "list" => $clients,
                "options" => ['multiple' => false, 'required' => false, $this->data['Quote']['client_id']],
            ]);
            ?>
        </div>
    </div>
</div>

<?php if (MODULO_CANTIERI) {
    echo $this->element('Form/constructionsites');
} ?>

<?= $this->Form->hidden('id', ['value' => $this->request->data['Quote']['id'], 'class' => 'form-control']); ?>
<?= $this->element('Form/client'); ?>

<div class="form-group col-md-12">
    <div class="col-md-3">
        <label class="form-label form-margin-top"><strong>Note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => str_replace("<br />", "", $this->request->data['Quote']['note'])]); ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-label form-margin-top"><strong>Esclusioni e ulteriori note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('elimination', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'textarea', 'value' => $this->request->data['Quote']['elimination']]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Validità</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('validity', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'maxlength' => false]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Pagamento</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('payment', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'maxlength' => false]); ?>
        </div>
    </div>
</div>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe preventivo</div>
    <div class="col-md-12"><hr></div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture" class="col-md-12">
    <?php
        $conto = count($this->request->data['Quotegoodrow']) - 1;
        //  $this->data = $Quotegoods;
        $i = -1;

        foreach ($this->request->data['Quotegoodrow'] as $chiave => $oggetto)
        {
            $i++;
            $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
            $chiave == 0 ? $classe1 = "originale" : $classe1 = '';
            if (($oggetto['quantity'] == null) && $oggetto['quote_good_row_price'] == null && $oggetto['quote_good_row_vat_id'] == null)
            {
                $isNote = 'display:none';
                $isDescriptionNote = ' col-md-12';
                $noteRequired = false;
            }
            else
            {
                $isNote = 'display:block';
                $isDescriptionNote = 'col-md-3';
                $noteRequired = true;
            }
            ?>
            <div class="principale lunghezza clonableRow contacts_row <?= $classe1 ?> <?= $classe ?>" id="' <?= $chiave ?> '">
                <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                <?= $this->Form->input("Quotegoodrow.$chiave.id"); ?>
                <div class="col-md-12">
                    <?php
                    isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = null; // : $movableValue = '0';
                    if (ADVANCED_STORAGE_ENABLED) {
                        ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Quotegoodrow.$chiave.tipo", ['required' => $noteRequired, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo', '0' => 'Voce descrittiva'], 'empty' => true, 'value' => $movableValue, 'disabled' => true]); ?>
                        </div>

                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Quotegoodrow.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice ']); ?>
                        </div>
                        <?php
                    } else {
                        echo $this->Form->hidden("Quotegoodrow.$chiave.tipo", ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]);
                        ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong></label>
                            <?= $this->Form->input("Quotegoodrow.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice']); ?>
                        </div>
                        <?php
                    }

                    $descriptionName = 'Descrizione';
                    $styleColor = "#6a6a6a";

                    if ($oggetto['type_of_line'] == 'S')
                    {
                        $descriptionName = "SUBTOTALE";
                        $styleColor = "#ea5d0b";
                    }

                    if ($oggetto['type_of_line'] == 'O')
                    {
                        $descriptionName = "SEZIONE OPZIONALE";
                        $styleColor = "#589AB8";
                    }

                    ?>
                    <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                        <label class="form-margin-top"><strong class="jsStrongDesc" style="color:<?= $styleColor ?>"><?= $descriptionName ?></strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Quotegoodrow.$chiave.description", ['div' => false, 'label' => false, 'class' => 'form-control jsDescription']); ?>
                        <?= $this->Form->hidden("Quotegoodrow.$chiave.storage_id"); ?>
                        <?= $this->Form->hidden("Quotegoodrow.$chiave.variation_id"); ?>
                        <?= $this->Form->hidden("Quotegoodrow.$chiave.type_of_line"); ?>
                        <?= $this->Form->hidden("Quotegoodrow.$chiave.movable", ['class' => 'jsMovable', 'value' => $movableValue]); ?>
                    </div>
                    <div class="col-md-3 jsRowField" style="<?= $isNote ?>">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input("Quotegoodrow.$chiave.customdescription", ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px', 'pattern' => PATTERNBASICLATIN]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?php
                        echo $this->Form->input("Quotegoodrow.$chiave.quantity", ['label' => false, 'class' => 'form-control jsQuantity', 'type' => "number", 'step' => "0.001", 'min' => "0",]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input("Quotegoodrow.$chiave.unit_of_measure_id", ['label' => false, 'class' => 'form-control', 'div' => false, 'options' => $units, 'empty' => true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top "><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Quotegoodrow.$chiave.quote_good_row_price", ['label' => false, 'class' => 'form-control jsPrice']); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top form-label"><strong>Importo</strong></label>
                        <?= $this->Form->input("Quotegoodrow.$chiave.importo", ['label' => false, 'class' => 'jsImporto', 'class' => 'form-control jsImporto', 'disabled' => true, 'value' => number_format($oggetto['quantity'] * $oggetto['quote_good_row_price'], 2, ',', '')]); ?>
                    </div>
                    <?php
                    if (isset($setting['Setting']['quoteVatVisible']) && $setting['Setting']['quoteVatVisible'] == 1) { ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Quotegoodrow.$chiave.quote_good_row_vat_id", ['label' => false, 'class' => 'form-control jsVat', 'type' => 'select', 'options' => $vats, 'empty' => true]); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="row" style="padding-bottom:10px;"><hr></div>
            </div>
            <script>
                loadClientCatalog(<?= $i ?>, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#QuoteClientId", "Quotegoodrow");
            </script>
        <?php } ?>
    </fieldset>
</div>
<?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
</br>
<div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
<?php echo $this->Form->end(); ?>

<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function ()
        {
            var clienti = setClients();
            // var articoli = setArticles();

            // Carico il listino
            loadClientCatalog(0, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#QuoteClientId", "Quotegoodrow");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "QuoteClientId");

            // Clonable
            enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#QuoteClientId");

            addcrossremoving();
        }
    );
</script>

<script>

    // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
    // Controllo che non siano ste inserite più di 5 aliquote iva

    function getVat(row) {
        return $(row).find(".jsVat").val();
    }

    function getPrice(row) {
        return $(row).find(".jsPrice").val();
    }

    function getQuantity(row) {
        return $(row).find(".jsQuantity").val();
    }


    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function () {
        setImporto(this);
    });

    $(".jsQuantity").change(function () {
        setImporto(this);
    });

    $(".jsTipo").change(function () {
        setCodeRequired(this);
    });

    // Fine codice per gestione prezzo/quantity/tipo

    $("#QuoteEditForm").on('submit.default', function (ev) {});

    $("#QuoteEditForm").on('submit.validation', function (ev) {

        ev.preventDefault(); // to stop the form from submitting
        /* Validations go here */

        var arrayOfVat = [];
        var errore = 0;
        $(".lunghezza").each( // clonablero

            function () {
                var iva = getVat(this);
                var prezzo = getPrice(this);
                var quantita = getQuantity(this);

                if (arrayOfVat.indexOf(iva) === -1) {
                    arrayOfVat.push(iva);
                }
                if (quantita != '' && quantita !== undefined) {
                    quantita = true;
                } else {
                    quantita = false;
                }
                if (prezzo != '' && prezzo !== undefined) {
                    prezzo = true;
                } else {
                    prezzo = false;
                }
                if (iva !== undefined && iva != '') {
                    iva = true;
                } else {
                    iva = false;
                }

                if ((quantita && prezzo && iva) || (!quantita && !prezzo && !iva)) {
                    /* nothing, is correct */
                    //  errore= 0;
                } else if (iva && (quantita == false || prezzo == false)) {
                    errore = 1;
                } else if (iva == false && (quantita == true || prezzo == true)) {
                    errore = 2;
                } else {
                    /* Not Possible */
                }

                if (arrayOfVat.length > 5) {
                    errore = 3;
                }
            });

        checkQuoteDuplicate(errore)
        //  return false;

    });

    function checkQuoteDuplicate(errore) {
        var errore = errore;

        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "checkQuoteDuplicate"]) ?>",
            data:
                {
                    quotenumber: $("#QuoteQuoteNumber").val(),
                    date: $("#datapreventivo").val(),
                },
            success: function (data) {
                if (data > 0 && ($("#QuoteQuoteNumber").val() != '<?= $this->data['Quote']['quote_number']; ?>')) {
                    errore = 4;
                }

                switch (errore) {
                    case 0:
                        $("#QuoteEditForm").trigger('submit.default');
                        break;
                    case 1:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 2:
                    <?php if($setting['Setting']['quoteVatVisible'] == 1) { ?>
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                    <?php }
                    else {
                        return true;
                    }?>
                        break;
                    case 3:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                    break;
                    case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione esiste già una fattura con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });
                        return false;
                    break;
                }
            },
            error: function (data)
            {
                // Nothing
            }
        });
    }

</script>
