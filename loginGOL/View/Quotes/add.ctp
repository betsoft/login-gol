
<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<script>
$(document).ready
    (
        function()
        {
         	// Nascondo a priori
         	$("#differentAddress").hide();
         	$("#differentAddressreseller").hide();

            var clienti = setClients();

             // Carico il listino passando chiave e id cliente
            loadClientCatalog(0,$("#QuoteClientId").val(),'#QuoteClientId',"Good");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti,"#QuoteClientId");

            $('#BillImporto').hide();

            // Abilita il clonable
            enableCloning($("#QuoteClientId").val(),"#QuoteClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );
</script>
    <?= $this->Form->create('Quote', ['class' => 'uk-form uk-form-horizontal']); ?>

      <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo preventivo') ?></span>
      <div class="col-md-12"><hr></div>
      <?php
        $attributes = ['legend' => false];
     ?>
       <div class="form-group col-md-12">
            <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero preventivo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <?= $this->Form->input('quote_number', ['label' => false,'value' => $n_fatt,'class' => 'form-control','required'=>true]); ?>
       </div>
       </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data preventivo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
              <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Quote][quote_date]" value="<?= date("d-m-Y"); ?>"  required />
        </div>
      </div>
     <div class="col-md-5" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
         <?php
            echo $this->Form->input('client_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','required'=>true,'maxlength'=>false]);
        ?>
       </div>
      </div>
       </div>
       <?php
       if(MODULO_CANTIERI)
            { ?>

                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top">
                        <strong>Cantiere</strong>
                    </label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'constructionsite_id',
                            "aggregator" => '',
                            "prefix" => "constructionsite_quote",
                            "list" => $constructionsites,
                            "options" => [ 'multiple' => false,'required'=> false],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="col-md-2" style="float:left;">
                        <label class="form-label form-margin-top"><strong>Descrizione</strong></label>
                        <div class="form-controls">
                            <?= $this->Form->input('description', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                        </div>
                    </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>CIG</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('cig', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                    </div>
                </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>CUP</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('cup', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                    </div>
                </div>
           </div>
          <?php } ?>


  <?= $this->element('Form/client'); ?>

  <div class="form-group col-md-12">
       <div class="col-md-3">
        <label class="form-label form-margin-top"><strong>Note</strong></label>
        <div class="form-controls">
          <?= $this->Form->input('note', ['div' => false,'label' => false,'class' => 'form-control','type'=>'textarea']);?>
          </div>
      </div>
      <div class="col-md-3">
          <label class="form-label form-margin-top"><strong>Esclusioni e ulteriori note</strong></label>
          <div class="form-controls">
              <?= $this->Form->input('elimination', ['div' => false,'label' => false,'class' => 'form-control','type'=>'textarea','value'=>$setting['Setting']['elimination']]);?>
          </div>
      </div>
          <div class="col-md-2" style="float:left;margin-left:10px;">
              <label class="form-label form-margin-top"><strong>Validità</strong></label>
              <div class="form-controls">
                  <?= $this->Form->input('validity', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false,'value'=>$setting['Setting']['validity']]); ?>
              </div>
          </div>
      <div class="col-md-2" style="float:left;margin-left:10px;">
          <label class="form-label form-margin-top"><strong>Pagamento</strong></label>
          <div class="form-controls">
              <?= $this->Form->input('payment', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
          </div>
      </div>
          <!--div class="col-md-2" style="float:left;margin-left:10px;">
              <label class="form-label form-margin-top"><strong>Pagamento</strong></label>
              <div class="form-controls">
                  <?php //  $this->Form->input('payment', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false]); ?>
                  <div class="form-controls">
                      <?php
                      /* $this->element('Form/Components/FilterableSelect/component', [
                          "name" => 'payment',
                          "aggregator" => '',
                          "prefix" => "payment",
                          "list" => $payments,
                          "options" => [ 'multiple' => false,'required'=> false],
                      ]);*/
                      ?>
                  </div>
              </div>
          </div-->
     </div>


     <div class="col-md-12"><hr></div>
      <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe preventivo</div>
         <div class="col-md-12"><hr></div>
         <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
            <fieldset id="fatture"  class="col-md-12">
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <!--span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" style="right:0;position:absolute;" hidden></span-->
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden></span>
          <div class="col-md-12">

          <?php
            if(ADVANCED_STORAGE_ENABLED)
            {
                ?>
            <div class="col-md-2 jsRowField">
             <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true]); ?>
           </div>
        <div class="col-md-2 jsRowField">
             <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control' ]); ?>
           </div>
         <?php
            }
            else
            {
                // Tolgo il jsTipo
                echo $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]);
                ?>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Codice</strong></label>
                    <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'maxlenght'=>11]); ?>
                </div>
                <?php
            }
          ?>
           <div class="col-md-3 jsRowFieldDescription">
             <label class="form-label"><strong class="jsStrongDesc">Descrizione</strong><i class="fa fa-asterisk"></i></label>
             <?php
                echo $this->Form->input('Good.0.description', ['div' => false,'label' => false,'class' => 'form-control goodDescription jsDescription']);
                echo $this->Form->hidden('Good.0.storage_id');
                echo $this->Form->hidden('Good.0.movable',['class'=>'jsMovable']);
            ?>
           </div>
              <div class="col-md-3 jsRowField">
                  <label class="form-label "><strong>Descrizione aggiuntiva</strong></label>
                  <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control  ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
              </div>
            <div class="col-md-2 jsRowField">
              <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
              <?php
                echo $this->Form->input('Good.0.quantity', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity', 'type'=>"number",'step'=>"0.001", 'min'=>"0",]);?>
            </div>

            <div class="col-md-2 jsRowField">
             <label class="form-margin-top"><strong>Unità di misura</strong></label>
                     <?=  $this->Form->input('Good.0.unit_of_measure_id', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units, 'empty'=>true]); ?>
           </div>
           <div class="col-md-2 jsRowField">
             <label class="form-margin-top "><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.quote_good_row_price', ['label' => false,'class' => 'form-control jsPrice']); ?>
           </div>
           <div class="col-md-2 jsRowField">
           <label class="form-margin-top form-label"><strong>Importo</strong></label>
             <?=  $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
           </div>

              <?php
              if(isset($setting['Setting']['quoteVatVisible']) && $setting['Setting']['quoteVatVisible'] == 1)
              { ?>
              <div class="col-md-2 jsRowField">
                  <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                  <?= $this->Form->input('Good.0.quote_good_row_vat_id', ['label' => false, 'class' => 'form-control jsVat', 'type' => 'number', 'type' => 'select', 'options' => $vats, 'empty' => true]); ?>
              </div>
          </div>
                <?php
                }
                ?>
           <div class="row" style="padding-bottom:10px;"><hr></div>
        </div>
        </fieldset>
        <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
      </div>
        	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
    <?=  $this->Form->end(); ?>

<?= $this->element('Js/datepickercode'); ?>


<script>
            function getVat(row){return $(row).find(".jsVat").val();}
            function getPrice(row){return $(row).find(".jsPrice").val();}
            function getQuantity(row){ return $(row).find(".jsQuantity").val();}

           // Inizio codice per gestione prezzo/quantity/tipo
            $(".jsPrice").change(function(){setImporto(this); });
            $(".jsQuantity").change(function(){setImporto(this);  });
            $(".jsTipo").change(function(){setCodeRequired(this);});

            // Fine codice per gestione prezzo/quantity/tipo

            $("#QuoteAddForm").on('submit.default',function(ev)
            {
            });

            $("#QuoteAddForm").on('submit.validation',function(ev)
            {
                ev.preventDefault(); // to stop the form from submitting
                /* Validations go here */

                var arrayOfVat = [];
                var errore = 0;
                $(".clonableRow").each(

                function()
                {
                    var iva = getVat(this);
                    var prezzo = getPrice(this);
                    var quantita = getQuantity(this);

                     if(arrayOfVat.indexOf(iva) === -1)
                     {
                        arrayOfVat.push(iva);
                     }

                    if(quantita != '' && quantita !== undefined) { quantita = true; } else { quantita = false; }
                    if(prezzo != '' && prezzo !== undefined) { prezzo = true; } else { prezzo = false; }
                    if(iva !== undefined && iva != '') {iva = true; } else { iva = false; }

                    if((quantita && prezzo && iva) || (!quantita && !prezzo && !iva))
                    {
                         /* nothing, is correct */
                    }
                    else if(iva && (quantita == false || prezzo == false))
                    {
                        errore = 1;
                    }
                    else if(iva == false && (quantita == true || prezzo == true))
                    {
                        errore = 2;
                    }
                    else
                    {
                      /* Not Possible */
                    }

                    if(arrayOfVat.length > 4)
                    {
                        errore = 3;
                    }
                });


                checkBillDuplicate(errore)
            });


        function checkBillDuplicate(errore)
        {

            var errore = errore;
            var formName = "#QuoteAddForm";

                $.ajax
                ({
    			    method: "POST",
    				url: "<?= $this->Html->url(["controller" => "quotes","action" => "checkQuoteDuplicate"]) ?>",
    				data:
    				{
    				    quotenumber : $("#QuoteQuoteNumber").val(),
    				    date : $("#datepicker").val(),
    				},
    				success: function(data)
    				{
                        if(data > 0)
                        {
                            errore = 4;
                        }

                        switch (errore)
                        {
                           case 0:
                                 $(formName).trigger('submit.default');
                           break;
                           case 1:
                               $.alert
                                ({
                                    icon: 'fa fa-warning',
                                	title: '',
                                    content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                                    type: 'orange',
                                });
                                return false;
                           break;
                           case 2:
                               <?php if($setting['Setting']['quoteVatVisible'] == 1) { ?>
                                   $.alert
                                   ({
                                       icon: 'fa fa-warning',
                                       title: '',
                                       content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                                       type: 'orange',
                                   });
                                   return false;
                               <?php }
                               else
                                   {
                                       return true;
                                   }?>
                           break;
                           case 3:
                                $.alert
                                ({
                                    icon: 'fa fa-warning',
                                	title: '',
                                    content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                                    type: 'orange',
                                });
                                return false;
                           break;
                           case 4:
                               $.alert
                                ({
                                    icon: 'fa fa-warning',
                                	title: '',
                                    content: 'Attenzione esiste già una fattura con lo stesso numero nell\'anno di competenza.',
                                    type: 'orange',
                                });
                                return false;
                           break;
                        }

        			},
        			error: function(data)
    				{
    				    // Nothing
    				}
    			});
        }

        function setCodeRequired(row)
        {
            // Rimossa perché in bolle sono tutti movable
            var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
            if(tipo == 1 )
            {
                $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
                $(row).parents(".clonableRow").find(".jsMovable").val(1);
            }
            else
            {
                $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
                $(row).parents(".clonableRow").find(".jsMovable").val(0);
            }
        }
</script>
