<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Preventivi','indexelements' => ['add'=>'Nuovo preventivo']]); ?>

<?php $currentYear = date("Y"); ?>
<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
		<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
        <tr>
            <?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
                'htmlElements' => [
                    '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
                    '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>'
                ]]);
            ?>
        </tr>
    </thead>
    <tbody class="ajax-filter-table-content">
    <?php if(count($quotes) == 0): ?>
        <?php if(MODULO_CANTIERI): ?>
            <tr><td colspan="11"><center>nessun preventivo trovato</center></td></tr>
        <?php else: ?>
            <tr><td colspan="7"><center>nessun preventivo trovato</center></td></tr>
        <?php endif; ?>
    <?php endif; ?>
    <?php
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
		foreach ($quotes as $quote)
		{
    ?>
			<tr>
				<td><?= $quote['Quote']['quote_number']; ?> / <?= substr($quote['Quote']['quote_date'],0,4); ?></td>
				<td style="text-align:center;"><?= $this->Time->format('d-m-Y', $quote['Quote']['quote_date']); ?></td>
                <?php
                if (MODULO_CANTIERI)
                {
                ?>
                    <td><?= $quote['Quote']['description']; ?></td>
                <?php } ?>
				<td class="table-max-width uk-text-truncate">
					<?php echo $quote['Client']['ragionesociale']; ?>
				</td>
                <?php
                if (MODULO_CANTIERI)
                {
                    ?>
                    <td ><?= $quote['Constructionsite']['name']; ?></td>
                    <td class="right"><?= $quote['Quote']['number_of_variation'] == null ? '' : $quote['Quote']['number_of_variation']; ?></td>
                    <td class="right"><?= $quote['Quote']['number_of_changes'] == null ? '' : $quote['Quote']['number_of_changes']; ?></td>
                    <?php
                }
                ?>
			<!-- importo -->
			<td style="text-align:right;">

			<?php
			if(isset($quote['Quotegoodrow']))
			{
				// Importo
				$articolo = '';
				$prezzo_articoli = $prezzo_articoli_ivato = $prezzo_pagato = $prezzo_da_pagare = $prezzo_non_pagato = $prezzo_scontato = $ritenutaAcconto = $importo = 0;
				
				// Per ogni articolo
				
				foreach($quote['Quotegoodrow'] as $articolo) {

                    isset($articolo['Iva']['percentuale']) ? $definedPercentage = (1 + $articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;

                    $totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
                    $totaleRigaScontato = $totaleRiga;
                    $prezzo_articoli += $totaleRigaScontato * $definedPercentage;
                    $prezzo_scontato += $totaleRigaScontato;
                }
			
				$totale_fatturato += $prezzo_articoli ;
				$totale_fatturato_pagato += $prezzo_pagato ;
				$totale_fatturato_da_pagare += $prezzo_da_pagare ;
				$totale_fatturato_pagato_parzialmente += $prezzo_pagato_parziale ;
				$totale_fatturato_non_pagato += $prezzo_non_pagato ;
				
				$importo += $prezzo_scontato;
				
				if($prezzo_articoli == ''){} else{echo number_format($prezzo_scontato, 2, ',', '.');}
				echo'<br/>';
			}
			?>
		</td>
		<td style="text-align:right;">
			<?php
				if(isset($quote['Quotegoodrow'])) {
                    $articolo = $prezzo_articoli_ivato = '';
                    foreach ($quote['Quotegoodrow'] as $articolo) {
                        isset($articolo['Iva']['percentuale']) ? $definedPercentage = ($articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;
                        $totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
                        $totaleRigaScontato = $totaleRiga;
                        $prezzo_articoli += $totaleRigaScontato * $definedPercentage;
                        $prezzo_scontato += $totaleRigaScontato;

                        $prezzo_articoli_ivato += $totaleRigaScontato * $definedPercentage;

                    }

                    if (isset($prezzo_articoli_ivato)) {
                        if ($prezzo_articoli_ivato != '') {
                            echo number_format($prezzo_articoli_ivato, 2, ',', '.');
                        } else {
                            // Nothing
                        }
                    } else {
                        // Nothing
                    }
                }
			?>
		</td>
		<td style="text-align:right;"> 
			<?php 
					$articolo = $prezzo_articoli_ivato = '';
			if(isset($quote['Quotegoodrow']))
			{
				foreach($quote['Quotegoodrow'] as $articolo) 
				{
						isset($articolo['Iva']['percentuale']) ? $definedPercentage = (1 + $articolo['Iva']['percentuale'] / 100) : $definedPercentage = 1;
						$totaleRiga = $articolo['quote_good_row_price'] * $articolo['quantity'];
						$totaleRigaScontato = $totaleRiga ;
						$prezzo_articoli += $totaleRigaScontato * $definedPercentage ;
						$prezzo_scontato += $totaleRigaScontato;

					    $prezzo_articoli_ivato += $totaleRigaScontato * $definedPercentage;
	
				}

				if(isset($prezzo_articoli_ivato))
				{
					if($prezzo_articoli_ivato != '')
					{
						echo number_format($prezzo_articoli_ivato, 2, ',', '.');
					}
					else
					{
						// Nothing
					}
				} else 
				{
					// Nothing
				} 
			}
			?>
		</td>
		<td class="actions" style="text-align:left;">
			<?php
				if($quote['Quote']['bill_created'] == 0) {
				$false = 'none';
                $true = 'inline';
                if(MODULO_CANTIERI)
                {
                    //   if ($quote['Quote']['accepted'] == 0){ $hidden3 = 'inline'; $hidden4 = 'none'; } else { $hidden3 = 'none'; $hidden4 = 'inline'; }

                    if($quote['Quote']['state'] > 1)
                    {
                        $displayClose = 'none';
                        $displayOpen = 'inline';
                    }
                    else
                    {
                        $displayClose = 'inline';
                        $displayOpen = 'none';
                    }

                    switch ($quote['Quote']['sent']) {
                        case 0:

                            switch ($quote['Quote']['accepted']) {
                                case 0:
                                    $displayNotSent = $true;
                                    $displaySent = $false;
                                    $hidden3 = $false;
                                    $hidden4 = $false;
                                    $displayModify = $false;
                                    $displayNotModify = $true;
                                    $displayNotVariation = $true;
                                    $displayNotAccepted = $false;
                                    $displayAccepted = $false;
                                    $displayAcceptedGray = $false;
                                    $displayNotAcceptedGray = $true;
                                    $displayVariation = $false;
                                    $editable = $true;
                                    $notEditable = $false;
                                    $displaySentGrey = $false;
                                    break;
                                case 1:
                                    /*$displayNotSent = $true;
                                    $displaySent  = $false;
                                    $displayNotModify = $false;
                                    $displayModify = $true;
                                    $displayNotVariation   = $false;
                                    $displayVariation = $true;
                                    $editable = $false;
                                    $notEditable=  $true;*/
                                    break;
                            }
                            break;
                        case 1:
                            switch ($quote['Quote']['accepted']) {
                                case 0:
                                    $displayNotSent = $false;
                                    $displaySent = $true;
                                    $displayModify = $true;
                                    $displayNotModify = $false;
                                    $displayNotVariation = $true;
                                    $displayVariation = $false;
                                    $displayNotAccepted = $true;
                                    $displayAccepted = $false;
                                    $displayAcceptedGray = $false;
                                    $displayNotAcceptedGray = $false;
                                    $editable = $false;
                                    $notEditable = $true;
                                    $displaySentGrey = $false;
                                    break;
                                case 1:
                                    $displayNotSent = $false;
                                    $displaySent = $false;
                                    $displaySentGrey = $true;
                                    $displayModify = $false;
                                    $displayNotModify = $true;
                                    $displayNotVariation = $false;
                                    $displayVariation = $true;
                                    if($quote['Quote']['number_of_changes'] > 0)
                                    {
                                        $displayAccepted = $false;
                                        $displayNotAccepted = $false;
                                        $displayNotAcceptedGray = $false;
                                        $displayAcceptedGray = $true;
                                    }
                                    else
                                        {
                                            $displayNotAccepted = $false;
                                            $displayAccepted = $true;
                                            $displayNotAcceptedGray = $false;
                                            $displayAcceptedGray = $false;
                                        }
                                    $editable = $false;
                                    $notEditable = $true;

                                    break;
                            }
                            break;
                    }
                }
                else {
                    $editable = $true;
                    $notEditable = $false;
                }

                    echo $this->Html->link($iconaModifica, ['action' => 'edit', $quote['Quote']['id']], ['class' => 'jsModify  iconamodifica'.$quote["Quote"]["id"],'style'=>['display:'.$editable ], 'title' => __('Modifica preventivo'), 'escape' => false]);
                
                    if(MODULO_CANTIERI)
                    {
                       ?>
                        <img src="<?= $this->webroot; ?>img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica"  class="golIcon iconamodificaoff<?= $quote['Quote']['id'] ?>"  title = "Preventivo non modificabile" style="display:<?= $notEditable ?>">
                        <?php
                    }

                    echo $this->Form->postLink($iconaGeneraFattura, ['action' => 'createSellBillsFromQuote', $quote['Quote']['id']], ['title' => __('Genera fattura'), 'escape' => false], __('Generare la fattura di vendita per il preventivo selezionato ? ', $quote['Quote']['id']));
                    echo $this->Html->link($iconaPdf, ['action' => 'quotespdf', $quote['Client']['ragionesociale'] . '-' . str_replace("/", "", $quote['Quote']['quote_number']) . '_' . $this->Time->format('d_m_Y', $quote['Quote']['quote_date']), $quote['Quote']['id']], ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);

                    if (MODULO_CANTIERI)
                    {
                        echo $this->Html->link('<i class="fa fa-plus-square icon jsQuoteRevision" id="quoteRevision'.$quote["Quote"]['id'] . '"  quoteId = '.$quote["Quote"]['id'] . ' style="display:'.$displayModify.';font-size:20px;vertical-align: middle;color:#589AB8;" title="Revisione preventivo"></i>', ['action' => 'quotechange', $quote['Quote']['id']], ['title' => __('Modifica preventivo'), 'escape' => false]);
                        ?><i class="fa fa-plus-square icon jsQuoteRevisionBlocked" id="quoteRevisionBlocked<?= $quote["Quote"]['id'] ?>"  quoteId = "<?=  $quote["Quote"]['id'] ; ?>" style="display:<?= $displayNotModify ?>;font-size:20px;vertical-align: middle;color:#DADFE0;" title="Revisione preventivo"></i><?php
                        echo $this->Html->link('<i class="fa fa-random jsQuoteChange" id="quoteChange'.$quote["Quote"]['id'] . '" quoteId =  '.$quote["Quote"]['id'] . ' style="display:'.$displayVariation .';font-size:20px;vertical-align: middle;color:#589AB8;" title="Aggiunta a preventivo accettato"></i>', ['action' => 'quoteadd', $quote['Quote']['id']], ['title' => __('Aggiunta preventivo'), 'escape' => false]);
                        ?><i class="fa fa-random jsQuoteChangeBlocked" id="quoteChangeBlocked<?= $quote["Quote"]['id'] ?>" quoteId = "<?=  $quote["Quote"]['id'] ; ?>" style="display:<?= $displayNotVariation  ?>;font-size:20px;vertical-align: middle;color:#DADFE0;" title="Aggiunta a preventivo accettato"></i>

                        <i class="fa fa-send jsQuoteNotSent icon " id="quoteNotSent<?=  $quote["Quote"]['id'] ; ?>" quoteId = "<?=  $quote["Quote"]['id'] ; ?>"
                           style="display:<?= $displayNotSent ?>;font-size:20px;vertical-align: middle;color:#EA5D0B;cursor:pointer"
                           title="Segna preventivo come inviato"  quoteId="<?= $quote['Quote']['id'] ?>"></i>

                        <i class="fa fa-send jsQuoteSentGrey icon"  id="quoteSentGrey<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displaySentGrey ?>;font-size:20px;vertical-align: middle;color:#DADFE0;cursor:pointer"
                           title="Segna preventivo come non inviato" ></i>

                        <i class="fa fa-send jsQuoteSent icon" id="quoteSent<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displaySent ?>;font-size:20px;vertical-align: middle;color:#577400;cursor:pointer"
                           title="Segna preventivo come non inviato"  ></i>

                        <i class="fa fa-square-o jsQuoteNotAccepted icon"  id="quoteNotAccepted<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displayNotAccepted ?>;font-size:20px;vertical-align: middle;color:#EA5D0B;cursor:pointer"
                           title="Segna preventivo come accettato" quoteId="<?= $quote['Quote']['id'] ?>"></i>

                        <i class="fa fa-square-o jsQuoteNotAcceptedGray icon"  id="quoteNotAcceptedGrey<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displayNotAcceptedGray ?>;font-size:20px;vertical-align: middle;color:#DADFE0;"
                           title="" ></i>

                        <i class="fa  fa-check-square-o  jsQuoteAcceptedGray icon"  id="quoteAcceptedGrey<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displayAcceptedGray ?>;font-size:20px;vertical-align: middle;color:#DADFE0;"
                           title="" ></i>

                        <i class="fa fa-check-square-o jsQuoteAccepted icon" id="quoteAccepted<?=  $quote["Quote"]['id'] ; ?>" quoteId="<?= $quote['Quote']['id'] ?>"
                           style="display:<?= $displayAccepted ?>;font-size:20px;vertical-align: middle;color:#577400;cursor:pointer"
                           title="Segna preventivo come non accettato" ></i>

                        <?php
                        if($quote['Quote']['parent_id'] > 0) {
                            echo $this->Html->link('<i class="fa fa-history icon" style="font-size:20px;vertical-align: middle;color:#EA5D0B;" title="Storia preventivo"></i>', ['action' => 'quotehistory', $quote['Quote']['id']], ['title' => __('Storia preventivo'), 'escape' => false]);
                        }
                        else
                            {
                                echo '<i class="fa fa-history icon" style="font-size:20px;vertical-align: middle;color:#DADFE0;" title="Storia preventivo"></i>';
                            }
                        ?>
                           <i class="fa fa-unlock jsQuoteClose" id="jsOpen<?=  $quote['Quote']['id'] ; ?>" quoteId = "<?=   $quote['Quote']['id'];  ?>" style="display:<?= $displayClose  ?>;font-size:20px;vertical-align: middle;color:#577400;cursor:pointer;" title="Chiudi preventivo"></i>
                        <i class="fa fa-lock jsQuoteOpen" id="jsClose<?=  $quote['Quote']['id'] ; ?>" quoteId = "<?=   $quote['Quote']['id']  ; ?>" style="display:<?= $displayOpen  ?>;font-size:20px;vertical-align: middle;color:#EA5D0B;cursor:pointer;" title="Riapri preventivo"></i>

                    <?php

                    }
                    echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica preventivo"></i>', ['action' => 'duplicate', $quote['Quote']['id']], ['title' => __('Duplica preventivo'), 'escape' => false]);
                    echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $quote['Quote']['id']], ['title' => __('Elimina preventivo'), 'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $quote['Quote']['id']));
                }
				else {
                    echo $this->Html->link($iconaPdf, array('action' => 'quotespdf', $quote['Client']['ragionesociale'] . '-' . str_replace("/", "", $quote['Quote']['quote_number']) . '_' . $this->Time->format('d_m_Y', $quote['Quote']['quote_date']), $quote['Quote']['id']), ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);
                    echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica preventivo"></i>', ['action' => 'duplicate', $quote['Quote']['id']], ['title' => __('Duplica preventivo'), 'escape' => false]);

                }
			?>
		</td>
	</tr>
<?php } ?>

</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
	<div id="element"></div>

<?=  $this->element('Js/datepickercode'); ?>

<script>

    $(".jsQuoteRevision").click(function(){
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteChange"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (quoteId) {
            }
        });
    });


    $(".jsQuoteChange").click(function(){
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteadd"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (quoteId) {

            }
        });
    });

    // Visualizza storico
    $(".jsQuoteStory").click(function(){
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quotestory"]) ?>",
            data:
                {
                    quoteid: $(this).attr("quoteid"),
                },
            success: function (quoteId) {
                $(this).attr("color", "red");
            }
        });
    });

    // IMPOSTA COME PREVENTIVO INVIATO
    $(".jsQuoteNotSent").click(
        function() {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteSetSent"]) ?>",
                data:
                    {
                        quoteId: $(this).attr("quoteid"),
                    },
                success: function (quoteid)
                {

                    setSent(quoteid);
                    // Se non è stato accettato
                    if ($(".jsQuoteNotAccepted").is(':visible') || $(".jsQuoteAcceptedGray").is(':visible'))
                    {
                        enableRevision(quoteid);
                        unsetAccepted(quoteid);
                        disableModify(quoteid);
                    }
                }
            });
        })

    // IMPOSTA COME PREVENTIVO NON INVIATO
    $(".jsQuoteSent").click(function() {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteSetNotSent"]) ?>",
            data:
                {
                    quoteId: $(this).attr("quoteid"),
                },
            success: function (quoteid)
            {
                setUnsent(quoteid);

                // Se il preventivo è stato inviato, ma non è stato accettato posso fare delle variazioni.
                if ($(".jsQuoteNotAccepted").is(':visible'))
                {
                    disableResvision(quoteid);
                    disableAccepted(quoteid);
                    enableModify(quoteid);
                }
            }
        });
    })


    // Preventivo non accettato
    $(".jsQuoteAccepted").click(function() {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteSetNotAccepted"]) ?>",
            data:
                {
                    quoteId: $(this).attr("quoteid"),
                },
            success: function (quoteId)
            {
                unsetAccepted(quoteId);
                setSent(quoteId);
                enableRevision(quoteId);
                disableChangeQuoteAccepted(quoteId);
            }
        });
    })

    // Accettazione preventivo
    $(".jsQuoteNotAccepted").click(
        function() {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "quotes", "action" => "quoteSetAccepted"]) ?>",
                data:
                    {
                        quoteId: $(this).attr("quoteid"),
                    },
                success: function (quoteId) {
                    setAccepted(quoteId);
                    disableSent(quoteId);
                    disableResvision(quoteId);
                    enableChangeQuoteAccepted(quoteId);
                }
            });
        })


    $(".jsQuoteClose").click(function()
    {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "openQuote"]) ?>",
            data:
                {
                    quoteId: $(this).attr("quoteid"),
                },
            success: function (data)
            {
                $("#jsOpen"+data).hide();
                $("#jsClose"+data).show();
            }
        });
    });
    $(".jsQuoteOpen").click(function()
    {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "quotes", "action" => "closeQuote"]) ?>",
            data:
                {
                    quoteId: $(this).attr("quoteid"),
                },
            success: function (quoteId)
            {
                $("#jsOpen"+quoteId).show();
                $("#jsClose"+quoteId).hide();
            }
        });
    });

    function enableChangeQuoteAccepted(quoteId)
    {
        $("#quoteChange"+quoteId).show();
        $("#quoteChangeBlocked"+quoteId).hide();
    }

    function disableChangeQuoteAccepted(quoteId)
    {
        $("#quoteChange"+quoteId).hide();
        $("#quoteChangeBlocked"+quoteId).show();
    }

    function enableRevision(quoteId)
    {
        $("#quoteRevision"+quoteId).show();
        $("#quoteRevision"+quoteId).show();
        $("#quoteRevisionBlocked"+quoteId).hide();
    }

    function disableResvision(quoteId)
    {
        $("#quoteRevision"+quoteId).hide();
        $("#quoteRevisionBlocked"+quoteId).show();
    }

    function enableModify(quoteId)
    {
        $(".iconamodifica"+quoteId).show();
        $(".iconamodificaoff"+quoteId).hide();
    }

    function disableModify(quoteId)
    {
        $(".iconamodifica"+quoteId).hide();
        $(".iconamodificaoff"+quoteId).show();
    }

    function setSent(quoteId)
    {
        $("#quoteNotSent"+quoteId).hide();
        $("#quoteSent"+quoteId).show();
        $("#quoteSentGrey"+quoteId).hide();
    }

    function setUnsent(quoteId)
    {
        $("#quoteNotSent"+quoteId).show();
        $("#quoteSent"+quoteId).hide();
        $("#quoteSentGrey"+quoteId).hide();
    }

    function disableSent(quoteId)
    {
        $("#quoteNotSent"+quoteId).hide();
        $("#quoteSent"+quoteId).hide();
        $("#quoteSentGrey"+quoteId).show();
    }

    function disableAccepted(quoteId)
    {
        $("#quoteNotAcceptedGrey"+quoteId).show();
        $("#quoteNotAccepted"+quoteId).hide();
        $("#quoteAccepted"+quoteId).hide();
    }

    function setAccepted(quoteId)
    {
        $("#quoteNotAcceptedGrey"+quoteId).hide();
        $("#quoteNotAccepted"+quoteId).hide();
        $("#quoteAccepted"+quoteId).show();
    }

    function unsetAccepted(quoteId)
    {
        $("#quoteNotAcceptedGrey"+quoteId).hide();
        $("#quoteNotAccepted"+quoteId).show();
        $("#quoteAccepted"+quoteId).hide();
    }
</script>
