
<?=  $this->Form->create(); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Banca'); ?></span>
	 <div class="col-md-12"><hr></div>
	    <?= $this->Form->input('id',array('class'=>'form-control')); ?>
  	
  <div class="col-md-12">
       <div class="col-md-2" >
        <label class="form-margin-top form-label"><strong>Codice</label><i class="fa fa-asterisk"></i></strong>
         <div class="form-controls">
	           <?= $this->Form->input('code', array('div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>5)); ?>
         </div>
    </div>
    <div class="col-md-4" >
        <label class="form-margin-top form-label"><strong>Descrizione</label><i class="fa fa-asterisk"></i></strong>
         <div class="form-controls">
	           <?= $this->Form->input('deposit_name', array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
         </div>
    </div>
    </div>
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>