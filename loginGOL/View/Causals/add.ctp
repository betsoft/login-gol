
  <?= $this->Form->create(); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova causale') ?></span>
  	 <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
     <div class="col-md-6">
        <label class="form-margin-top form-label"><strong>Causale</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
	           <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
      </div>
      <div class="col-md-2">
        <label class="form-margin-top form-label"><strong>Tipologia Intestatario</strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('clfo', ['div' => false, 'label' => false, 'class'=>'form-control','type'=>'select','options'=>['C'=>'Cliente','F'=>'Fornitore']]); ?>
         </div>
      </div>
    </div>
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>
	
	
	