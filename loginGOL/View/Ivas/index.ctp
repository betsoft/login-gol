<?= $this->element('Form/Components/Paginator/loader'); ?><?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Regimi Iva','indexelements' => ['add'=>'Nuovo regime iva']]); ?>

<div class="clients index">
	<div class="ivas index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($ivas) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php }
				else
				{
					?><tr><td colspan="5"><center>nessun regime iva trovato</center></td></tr><?php
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($ivas as $iva){ ?>
					<tr>
						<td style="text-align:right;"><?= h($iva['Iva']['percentuale']). ' %'; ?></td>
                        <td><?= h($iva['Einvoicevatnature']['code']); ?>&nbsp;</td>
                        <td><?= h($iva['Iva']['descrizione']); ?></td>
						<td><?= h($iva['Iva']['codice']); ?>&nbsp;</td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, array('action' => 'edit', $iva['Iva']['id']),array('title'=>__('View'),'escape'=>false));
								echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $iva['Iva']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare il regime  ?', $iva['Iva']['id']));
							?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
