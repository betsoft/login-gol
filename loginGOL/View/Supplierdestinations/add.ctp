   <?= $this->element('Form/Components/FilterableSelect/loader'); ?>
    <?= $this->Form->create('Supplierdestination'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo destino') ?></span>
  	 <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <label class="form-margin-top form-label"><strong>Ragione sociale<i class="fa fa-asterisk"></i></strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
    
    <div class ="form-group col-md-12">
        <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Indirizzo<i class="fa fa-asterisk"></i></strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('address', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
     <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Cap<i class="fa fa-asterisk"></i></strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('cap', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Città<i class="fa fa-asterisk"></i></strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('city', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Provincia<i class="fa fa-asterisk"></i></strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('province', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>

	<div class="col-md-2" style="float:left;margin-left:10px;">
	<label class="form-label form-margin-top"><strong>Nazione</strong></label> 
   <div class="form-controls">
		<?= $this->element('Form/Components/FilterableSelect/component', [
			"name" => 'nation_id',
			"aggregator" => '',
			"prefix" => "nation_id",
			"list" => $nations,
			"options" => [ 'multiple' => false,'required'=> false],
			]); 
	?>
   </div>
   </div>   
   </div>
    
    <div class="col-md-12"><hr></div>

	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index','passedValue'=>$supplierId]); ?></center>
	<?= $this->Form->end(); ?>
