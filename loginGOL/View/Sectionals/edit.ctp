<?= $this->Form->create('Sectional'); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica sezionale') ?></span>
 <div class="col-md-12"><hr></div>
<div class="form-group col-md-12">
   <div class="col-md-2" style="float:left;">
      <label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
         <?= $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control', 'required'=>true]); ?>
      </div>
   </div>
   <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Contatore</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
         <?= $this->Form->input('last_number', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>0, 'required'=>true]); ?>
      </div>
   </div>
   <!--div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Prefisso</strong></label>
      <div class="form-controls">
         <?php // $this->Form->input('prefix', ['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div-->
   <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Suffisso</strong></label>
      <div class="form-controls">
         <?= $this->Form->input('suffix', ['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div>
     <?php if(MODULE_IXFE &&  ($currentSectional == 'creditnote_sectional' || $currentSectional == 'bill_sectional')) { ?>
    <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Sezionale ixfe</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
         <?= $this->Form->input('ixfe_sectional', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,"pattern"=>"[a-zA-Z0-9\-]+"]); ?>
      </div>
   </div>
   <?php }  ?>
    <div class="col-md-2" style="float:left;margin-left:10px;">
      <label class="form-margin-top form-label"><strong>Tipo contatore</strong></label>
      <div class="form-controls">
         <?= $this->Form->input('sectional_type', ['div' => false, 'label' => false, 'class'=>'form-control','option'=>$sectionalTypes,'type'=>'select','value'=>$currentSectional,'disabled'=>false]); ?>
      </div>
   </div>
</div>
 <div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>
