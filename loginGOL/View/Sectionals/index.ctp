<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Sezionali','indexelements' => ['add'=>'Nuovo Sezionale']]); ?>

<div class="index">
    <table class="table table-bordered table-striped table-condensed flip-content">
        <thead class ="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
        </thead>
        <tbody class="ajax-filter-table-content">
        <?php if(count($sectionals) == 0): ?>
            <tr><td colspan="10"><center>Nessun sezionale trovato</center></td></tr>
        <?php else: ?>
            <?php foreach ($sectionals as $secional): ?>
                <tr>
                    <td><?= $secional['Sectional']['description']; ?></td>
                    <td><?= $secional['Sectional']['last_number']; ?></td>
                    <td><?= $secional['Sectional']['suffix']; ?></td>
                    <?php if(MODULE_IXFE): ?>
                        <td><?= $secional['Sectional']['ixfe_sectional']; ?></td>
                    <?php endif; ?>
                    <td style="text-align:center;"><?= $secional['Sectional']['default_bill'] == 0 ? '<i  class="fa fa-star-o  jsStarBill" starid="'.$secional['Sectional']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
                    <td style="text-align:center;"><?= $secional['Sectional']['default_creditnote'] == 0 ? '<i  class="fa fa-star-o jsStarCreditnote" starid="'.$secional['Sectional']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
                    <td style="text-align:center;"><?= $secional['Sectional']['default_proforma'] == 0 ? '<i  class="fa fa-star-o jsStarProforma" starid="'.$secional['Sectional']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
                    <td style="text-align:center;"><?= $secional['Sectional']['default_transport'] == 0 ? '<i  class="fa fa-star-o jsStarTransport" starid="'.$secional['Sectional']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
                    <?php if($_SESSION['Auth']['User']['dbname'] == 'login_GE0047' || $_SESSION['Auth']['User']['dbname'] == 'login_GE0041') { ?>
                        <td style="text-align:center;"><?= $secional['Sectional']['default_order'] == 0 ? '<i  class="fa fa-star-o jsStarOrder" starid="'.$secional['Sectional']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
                    <?php }else{?>
                        <td></td>
                    <?php } ?>
                    <td>
                        <?php
                        $sectionalType ='';
                        if($secional['Sectional']['default_bill'] == 1 ){ $sectionalType = 'Fatture'; }
                        if($secional['Sectional']['default_creditnote'] == 1 ){ $sectionalType = 'Note di credito'; }
                        if($secional['Sectional']['default_transport'] == 1 ){ $sectionalType = 'Bolle'; }
                        if($secional['Sectional']['default_proforma'] == 1 ){ $sectionalType = 'Pro forma'; }
                        if(isset($secional['Sectional']['default_order']) && $secional['Sectional']['default_order'] == 1 ){ $sectionalType = 'Ordini'; }

                        if($secional['Sectional']['bill_sectional'] == 1 ){ $sectionalType = 'Fatture'; }
                        if($secional['Sectional']['creditnote_sectional'] == 1 ){ $sectionalType = 'Note di credito'; }
                        if(isset($secional['Sectional']['order_sectional']) && $secional['Sectional']['order_sectional'] == 1 ){ $sectionalType = 'Ordini'; }
                        if($secional['Sectional']['transport_sectional'] == 1 ){ $sectionalType = 'Bolle'; }
                        if($secional['Sectional']['proforma_sectional'] == 1 ){ $sectionalType = 'Pro forma'; }
                        echo $sectionalType;
                        ?>
                    </td>
                    <td class="actions">
                        <?=	$this->element('Form/Simplify/Actions/edit',['id' => $secional['Sectional']['id']]); ?>
                        <?php
                        if($utilities->isSectionalUsed($secional['Sectional']['id']) == 0)
                            echo $this->element('Form/Simplify/Actions/delete',['id' => $secional['Sectional']['id'],'message' => 'Sei sicuro di voler eliminare il sezionale ?']);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <?= $this->element('Form/Components/Paginator/component'); ?>
</div>

<script>
	$(".jsStarBill").click(
		function()
		{
			var id = $(this).attr('starid');

			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "sectionals","action" => "setDefaultBillSectional"]) ?>",
				data:
				{
					id:id,
				},
				success: function(data)
				{
                    location.reload();
				},
				error: function(response)
				{
					$.alert({
    					icon: 'fa fa-warning',
	    			    title: '',
    				    content: 'Un sezionale può essere utilizzato per un unica tipologia di documento.',
    					type: 'orange',
                    });
				}
			});
		}
    );

	$(".jsStarCreditnote").click(
		function()
		{
			var id = $(this).attr('starid');

			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "sectionals","action" => "setDefaultCreditnoteSectional"]) ?>",
				data:
				{
					id:id,
				},
				success: function(data)
				{
                    location.reload();
				},
				error: function(response)
				{
					$.alert({
    					icon: 'fa fa-warning',
	    			    title: '',
    				    content: 'Un sezionale può essere utilizzato per un unica tipologia di documento.',
    					type: 'orange',
                    });
				}
			});
		}
    );

	$(".jsStarProforma").click(
		function()
		{
			var id = $(this).attr('starid');

			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "sectionals","action" => "setDefaultProformaSectional"]) ?>",
				data:
                {
                    id:id,
                },
                success: function(data)
                {
                    location.reload();
                },
				error: function(response)
				{
                    $.alert({
                        icon: 'fa fa-warning',
                        title: '',
                        content: 'Un sezionale può essere utilizzato per un unica tipologia di documento.',
                        type: 'orange',
                    });
				}
			});
		}
    );

	$(".jsStarTransport").click(
		function()
		{
			var id = $(this).attr('starid');

			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "sectionals","action" => "setDefaultTransportSectional"]) ?>",
				data:
				{
					id:id,
				},
				success: function(data)
				{
                    location.reload();
				},
				error: function(response)
				{
                    $.alert({
                        icon: 'fa fa-warning',
                        title: '',
                        content: 'Un sezionale può essere utilizzato per un unica tipologia di documento.',
                        type: 'orange',
                    });
				}
			});
		}
    );

    $(".jsStarOrder").click(
            function()
            {
                var id = $(this).attr('starid');

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "sectionals","action" => "setDefaultOrderSectional"]) ?>",
                    data:
                        {
                            id:id,
                        },
                    success: function(data)
                    {
                        location.reload();
                    },
                    error: function(response)
                    {
                        $.alert({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Un sezionale può essere utilizzato per un unica tipologia di documento.',
                            type: 'orange',
                        });
                    }
                });
            }
        );

</script>
