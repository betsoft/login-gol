<?php
	echo $this->Html->css (
		array (
			'plugins/font-awesome/css/font-awesome.min',
			'plugins/simple-line-icons/simple-line-icons.min',
			'plugins/bootstrap/css/bootstrap',
			'plugins/uniform/css/uniform.default',
			'plugins/jqvmap/jqvmap/jqvmap',
			'plugins/morris/morris',
			'pages/css/tasks',
			'components',
			'plugins',
			'layout',
			'default',
			'custom',
			'nyroModal'
		)
	);
?>

<?php
	echo $this->Html->script (
		array ( 
		  'plugins/jquery.min.js',
		  'plugins/jquery-migrate.min.js',
		  'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js',
		  'plugins/bootstrap/js/bootstrap.js',
		  'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		  'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		  'plugins/jquery.blockui.min.js',
		  'plugins/jquery.cokie.min.js',
		  'plugins/uniform/jquery.uniform.min.js',
		  'plugins/jqvmap/jqvmap/jquery.vmap.js',
		  'plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js',
		  'plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js',
		  'plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js',
		  'plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js',
		  'plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js',
		  'plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js',
		  'plugins/morris/morris.min.js',
		  'plugins/morris/raphael-min.js',
		  'plugins/jquery.sparkline.min.js',
		  'scripts/metronic.js',
		  'jquery-ui-1.9.2.custom.min.js',
		  'jquery.nyroModal.custom.js',
		  'jquery.nyroModal.custom.min.js',
		  'jquery.nyroModal.filters.dom.js',
		  'jquery.nyroModal-ie6.js',
		  'jquery.nyroModal-ie6.min.js',
		  'jquery-1.8.3.js'	
		)
	)
?>

<div class="col-md-6" id="test" >
	<?php echo $this->Form->create('User', array('url' => 'login'));?>
	<?php echo $this->Session->flash('auth'); ?>
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-cogs font-green-sharp"></i>
				<span class="caption-subject font-green-sharp bold uppercase">
					<?php echo __('Si prega di inserire username e password'); ?></span>
				</div>
							<div class="tools">
								<?php// echo $this->Html->link('<i class="fa fa-times"></i>', array('action' => 'index'),array('title'=>__('List Fatture'),'escape'=>false)); ?>							
								
							</div>
						</div>
						
						<div class="portlet-body form">
							<form role="form" class="nyroModal">
								<div class="form-body">
									<div class="form-group">
										<label></label>
										<div class="input-group">
											
											
												
										<?php
											echo $this->Form->input('username',array('class'=>'form-control'));
											echo $this->Form->input('password',array('class'=>'form-control'));
										?>
										</div>
									</div>
									<div class="form-actions">
								<?php echo $this->Form->submit(__('Login',true), array('class'=>'btn blue')); 
    									echo $this->Form->end(); ?>
    									 <p><a href="#"><?php echo __("I forgot my password"); ?></a></p>
									
								</div>
	

</div>
</div>
</div>
<div id="element"></div>
</div>



