<?php if(!$createDemo): ?>
    <link href='https://fonts.googleapis.com/css?family=Barlow Semi Condensed' rel='stylesheet'>
    <div class="contenitore" style="width:100%" >
            <div style="width:100%">
            <center>
                <?= $this->Form->create('User');?>
                <?= $this->Session->flash('auth'); ?>
                <div style="color: #589AB8;font-family: 'Barlow Semi Condensed';font-size: 16pt;">Inserisci username e password per accedere</div>
                <div class="portlet-body form col-md-12">
                    <div class="form-body col-md-12">
                        <div class="row col-md-12">
                            <div class="col-md-offset-5 col-md-2">
                                <input type="text" placeholder="Username" name="data[User][username]"  style="font-family: 'Barlow Semi Condensed';font-size: 12pt; width:100%" autofocus>
                            </div>
                        </div>
                        <div class="row col-md-12">
                            <div class="col-md-offset-5 col-md-2">
                                <input type="password" placeholder="Password" name="data[User][password]" style="font-family: 'Barlow Semi Condensed';font-size: 12pt;width:100%">
                            </div>
                        </div>
                        <div class="flash form-margin-top col-md-offset-4 col-md-4" >
                            <?= $this->Session->flash() ?>
                        </div>
                        <div class="row col-md-12">
                            <div class="form-actions col-md-offset-4 col-md-12" >
                                <?php
                                    echo $this->Form->submit(__('Login',true), ['class'=>'btn blue-button','style'=>"font-family: 'Barlow Semi Condensed' !important;font-size: 10pt;"]);
                                    echo $this->Form->end();
                                ?>
                                <br/>
                            </div>
                            <br><br><br>
                            <!--<div style="color: red;font-family: 'Barlow Semi Condensed';font-size: 16pt;">Il gestionale è momentaneamente non disponibile causa manutenzione del server per la giornata odierna.<br> Il servizio verrà ripristinato nella giornata di domani </div>
-->
                            <!--<div class="form-actions col-md-offset-4 col-md-12" >
                                <?= $this->Html->link(__('Crea ambiente demo'), array('controller' => 'users', 'action' => 'login', 'demo'), array('escape' => false)); ?>
                                <br/>
                            </div>-->
                        </div>
                    </div>
                </div>
            </center>
        </div>
    </div>
<?php else: ?>
    <?= $this->Form->create('User');?>
    <?= $this->Session->flash('auth'); ?>
    <?= $this->element('Form/formelements/add_and_edit_title',['addedittitle'=>'Nuovo ambiente']); ?>
    <script src="https://www.paypal.com/sdk/js?client-id=ASPVANFT_zaumM_9DQ102cW4FEqVmarJSgs3aYxc9UcVQRDifRHGnjDhmOJyu9FZL91HniXzLV25ChnD&vault=true&intent=subscription&currency=EUR"></script>

    <div class ="form-group col-md-12">
        <div class="col-md-2">
            <label class="form-label form-margin-top"><strong>Email</strong></label>
            <div class="form-controls">
                <input id="mail1" type="email" placeholder="Email" name="data[User][username]"  style="font-family: 'Barlow Semi Condensed';font-size: 12pt; width:100%" autofocus>
            </div>
        </div>
        <div class="col-md-2">
            <label class="form-label form-margin-top"><strong>Password</strong></label>
            <div class="form-controls">
                <input id="pw" type="password" placeholder="Password" name="data[User][password]" style="font-family: 'Barlow Semi Condensed';font-size: 12pt;width:100%">
            </div>
        </div>

        <div class="col-md-2">
            <input type="hidden" name="data[User][paypal]" id="paypal-id" style="font-family: 'Barlow Semi Condensed';font-size: 12pt;width:100%">
            <div id="paypal-button-container-P-103681078T738951BMDEMTWY" style="display: none"></div>
            <script src="https://www.paypal.com/sdk/js?client-id=AQxo5u-x8fufbimUy8vXfo_rav_8hlvroYTpA7tlQnA03qjgOgtlecsUVbhklgwdGsGZZivjnvLBKodA&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script>
            <script>
                paypal.Buttons({
                    style:{
                        shape: 'rect',
                        color: 'blue',
                        layout: 'horizontal',
                        label: 'paypal'
                    },
                    createSubscription: function(data, actions) {
                        return actions.subscription.create({
                            /* Creates the subscription */
                            plan_id: 'P-103681078T738951BMDEMTWY'
                        });
                    },
                    onApprove: function(data, actions) {
                        // You can add optional success message for the subscriber here
                        document.getElementById('mostra').style.display = 'block';
                        document.getElementById('paypal-id').value = data.subscriptionID;
                        document.getElementById('paypal-button-container-P-103681078T738951BMDEMTWY').style.display = 'none';
                    }
                }).render('#paypal-button-container-P-103681078T738951BMDEMTWY'); // Renders the PayPal button
            </script>

            <!--<div id="paypal-button-container-P-9VP96001TF309342LMDELXQI" style="display: none"></div>
                    <script src="https://www.paypal.com/sdk/js?client-id=AXsIyk57YAJxZbHvC4wNRi2eYt9ZKhPDcSh81qcdQAT2UwU1rlYQf_KfbQvI3PxVqBxb3JBQHyLAFZx2&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script>
            <script>
                paypal.Buttons({
                    style: {
                        shape: 'rect',
                        color: 'blue',
                        layout: 'horizontal',
                        label: 'paypal'
                    },
                    createSubscription: function(data, actions) {
                        return actions.subscription.create({
                            /* Creates the subscription */
                            plan_id: 'P-9VP96001TF309342LMDELXQI'
                        });
                    },
                    onApprove: function(data, actions) {
                        alert(data.subscriptionID);
                        document.getElementById('mostra').style.display = block;// You can add optional success message for the subscriber here
                        document.getElementById('paypal-id').value = data.subscriptionID;
                        document.getElementById('paypal-button-container-P-9VP96001TF309342LMDELXQI').style.display = 'none';
                    }
                }).render('#paypal-button-container-P-9VP96001TF309342LMDELXQI'); // Renders the PayPal button
            </script>-->
            <div>
                <button id="procedi" type="button" class="btn blue-button" style="display: inline-block;margin-bottom: 0;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;touch-action: manipulation;cursor: pointer;user-select: none;padding: 7px 14px;outline: none !important;background-image: none !important;filter: none;box-shadow: none;text-shadow: none;background-color: #589AB8;border: 1px solid #589AB8;color: #fff !important;width: 15%;font-size: 15px;font-family: 'Montserrat', sans-serif !important;font-weight: 500 !important;text-transform: uppercase;font-family: 'Barlow Semi Condensed' !important;font-size: 10pt;width:100%; margin-top: 25px"> Procedi </button>
            </div>
        </div>

        <div id="mostra" style="display: none">
        <?php
            echo $this->Form->submit(__('Login',true), ['id' => 'login', 'class'=>'btn blue-button', 'disabled'=>'true', 'style'=>"font-family: 'Barlow Semi Condensed' !important;font-size: 10pt;"]);
            echo $this->Form->end();
        ?>
        </div>

        <div style="display: none">
            <select id="mailss">
            <?php $i = 0; ?>
            <?php foreach ($usernames as $nome): ?>
                <?php $i++; ?>
                <option value="<?= $nome['User']['username'] ?>"><?= $nome['User']['username']?></option>
            <?php endforeach; ?>
            </select>

            <script>
                var arr = new Array();

                $("#procedi").click(
                    function ()
                    {
                        var mail1 = $('#mail1').val();
                        var pw = $('#pw').val();
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

                        if(mail1 == '')
                        {
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Inserisci una mail valida',
                                content: 'Attenzione, la mail inserita non è valida',
                                type: 'orange',
                            });
                        }
                        else if(pw.length === 0)
                        {
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Inserisci una password valida',
                                content: 'Attenzione, la password inserita non è valida',
                                type: 'orange',
                            });
                        }
                        else if(!emailReg.test(mail1))
                        {
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Inserisci una mail valida',
                                content: 'Attenzione, la mail inserita non è valida',
                                type: 'orange',
                            });
                        }
                        else
                        {
                            arr = $('#mailss option').map(function(){return this.value;}).get();

                            if($.inArray(mail1, arr) != -1)
                            {
                                $.alert({
                                    icon: 'fa fa-warning',
                                    title: 'Nome utente già scelto',
                                    content: 'Attenzione, il nome utente selezionato non può essere utilizzato perchè già scelto',
                                    type: 'orange',
                                });
                            }
                            else
                            {
                                //document.getElementById("paypal-button-container-P-9VP96001TF309342LMDELXQI").style.display = "block";
                                document.getElementById("paypal-button-container-P-103681078T738951BMDEMTWY").style.display = "block";
                                document.getElementById("procedi").style.display = "none";
                                document.getElementById("login").disabled = false;
                            }
                        }
                    }
                );
            </script>
         </div>
    </div>
<?php endif; ?>
