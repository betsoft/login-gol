<?php echo $this->Form->create('User', array('type' => 'file', 'class' => 'uk-form uk-form-horizontal')); ?>
<h3 class="h1"><?php echo __('Registra la tua azienda'); ?></h3>
<div class=" -badge tools">	
    <?= $this->Html->link('<i class="uk-icon-sign-in"></i>&nbsp;Accedi', array('controller' => 'users', 'action' => 'login'), array('class'=>'btn blue-button bad','escape'=>false)); ?>
    </div>
     <center>
     <div class="panel-body">
          <div class="form-group col-md-12">
          <span style="text-align:left;margin-bottom:5px;"><strong>Email</strong></span>
          <div class="form-controls" style="width:25%">
            <?= $this->Form->input('email',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
          </div>
          </div>
      </div>
    </center>

<?php
	echo "<hr>";
	echo "<center>".$this->Form->submit(__('Registrati',true), array('class'=>'btn blue-button'))."</center>";
    echo $this->Form->end();
?>

<script>
   $(document).ready(function(){
      $(".provincia").attr("maxlength", 2) 
      
   });
   
 function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex =/^[0-9._]+|[\b]+$/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
</script>