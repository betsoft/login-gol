		<?= $this->Form->create('User'); ?>
	    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo utente') ?></span>
	     <div class="col-md-12"><hr></div>
	    <label><strong>Username</strong></label>
	    <?= $this->Form->input('username', array('label' => false, 'div' => false, 'class'=>'form-control')); ?>
	    <label class="form-margin-top"><strong>Password</strong></label>
	    <?= $this->Form->input('password', array('label' => false, 'div' => false, 'class'=>'form-control')); ?>
	    <label class="form-margin-top"><strong>Conferma password</strong></label>
	    <?= $this->Form->input('password_confirm', array('label' => false, 'div' => false, 'type' => 'password','class'=>'form-control')); ?>
	    <label class="form-margin-top"><strong>Gruppo</strong></label>
	    <?= $this->Form->input('group_id', array('label' => false, 'div' => false, 'type' => 'password','class'=>'form-control', 'options'=>$groups , 'type'=>'select')); ?>
	    <label id="UserTareLabel"><strong>Tara del camion</strong></label>
		<?= $this->Form->input('tare',array('div' => false, 'label' => false, 'class'=>'form-control')); ?>
	    <div class="col-md-12"><hr></div>
	    <center><?= $this->Form->submit(__('Salva',true), array('label' => false, 'div' => false, 'class'=>'btn blue-button new-bill')) ?></center>
    	<?= $this->Form->end(); ?>
	

		<script>
			$("#UserTare").hide();
			$("#UserTareLabel").hide();
			$("#UserGroupId").change(function()
			{
				if($(this).val() == '4')
				{
					$("#UserTare").show();
					$("#UserTareLabel").show();
				}
				else
				{
					$("#UserTare").hide();
					$("#UserTareLabel").hide();
				}
			})
		</script>