<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Utenti'); ?></span>
	</div>
	<div class=" -badge tools">
	</div>
</div>
<table class="table table-bordered table-striped table-condensed flip-content">
	<thead class ="flip-content">
		<tr>
			<th><?php echo $this->Paginator->sort('Nome utente'); ?></th>
			<th class="actions" style="width:10%;"><?php echo __('Azione'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td class="actions">
					<?php
						echo $this->Html->link($iconaModifica, array('action' => 'edit', $user['User']['id']),array('title'=>__('Edit'),'escape'=>false));
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<p class="uk-text-center">
	<?php
		echo $this->Paginator->counter(array('format' => __('Pagina {:page} di {:pages}')));
	?>
</p>
<div class="paging uk-text-center">
	<?php
		echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' - '));
		echo $this->Paginator->next('&nbsp;<i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
	?>
</div>
