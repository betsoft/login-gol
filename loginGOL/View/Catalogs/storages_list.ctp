<?= $this->element('Form/Components/Paginator/loader'); ?><?= $this->element('Form/Components/AjaxFilter/loader') ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Prodotti del listino'); ?></span>
	</div>
		<div class=" -badge tools">
			<?= $this->Html->link('< Lisino', array('controller'=>'catalogs','action' => 'index', $id,'Cliente'), array('title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;')); ?>		
		</div>
</div>
<div class="clients index">
	<div class="ivas index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?= $this->Paginator->sort('Articolo'); ?></th>
					<th><?= $this->Paginator->sort('Prezzo standard'); ?></th>
					<th><?= $this->Paginator->sort('Prezzo Listino'); ?></th>
					<!--th><?php // $this->Paginator->sort('Descrizione'); ?></th-->
					<th class="actions"><?= __('Azioni'); ?></th>
				</tr>
				<?php if (count($storageList) > 0) { ?>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php } ?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php
					foreach ($storageList as $storage)
					{
						?>
						<tr>
							<td><?= h($storage['Storages']['descrizione']); ?></td>
							<td style="text-align:right;"><?= h($storage['Storages']['prezzo']) ?></td>
							<td style="text-align:right;"><?= h($storage['Catalogstorages']['catalog_price']) ?></td>
							<!--td><?php // h($storage['Catalogstorages']['description'])  ?></td-->
							<td class="actions">
								<?= $this->Html->link($iconaModifica, array('action' => 'editCatalogStorage',$storage['Catalogstorages']['id'],$storage['Storages']['id'],$idCatalog),array('title'=>__('Modifica il prodotto'),'escape'=>false));
								?>
							</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
			<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
