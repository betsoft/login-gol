<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<!-- Icone -->
<?php
	$iconaModifica = $this->Html->image('gestionaleonlineicon/gestionale-online.net-modifica.svg', [ 'class'=>'golIcon', 'title'=>'Modifica il destino diverso']);
	$iconaElimina = $this->Html->image('gestionaleonlineicon/gestionale-online.net-elimina.svg', [ 'class'=>'golIcon', 'title'=>'Elimina il destino diverso']);
?>


<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Destini'); ?></span>
	<!--/div>
	<div class=" -badge tools"-->
		<?php // $this->Html->link('< elenco clienti', ['controller'=>'clients','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;']); ?>
		<?php // $this->Html->link('Nuovo destino', ['action' => 'add',$clientId], ['title'=>__('Nuovo destino'),'escape' => false,'class' => "blue-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;color:#555555 !imporant;margin-right:20px;']); ?>
        <div class="-badge tools"  style="float:right;">
            <?= $this->Html->link('elenco clienti', ['controller'=>'clients','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:30%;text-align:center;']); ?>
        	<?= $this->Html->link('Nuovo destino', ['action' => 'add',$clientId], ['title'=>__('Nuovo destino'),'escape' => false,'class' => "blue-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;color:#555555 !imporant;margin-right:20px;float:right;width:30%;text-align:center;']); ?>
	    </div>
</div>
	<div class="banks index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?php echo $this->Paginator->sort('Nome'); ?></th>
					<th><?php echo $this->Paginator->sort('Indirizzo'); ?></th>
					<th><?php echo $this->Paginator->sort('Cap'); ?></th>
					<th><?php echo $this->Paginator->sort('Città'); ?></th>
					<th><?php echo $this->Paginator->sort('Provincia'); ?></th>
					<th><?php echo $this->Paginator->sort('Nazione'); ?></th>
					<th class="actions"><?php echo __('Azioni'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Clientdestinations as $Clientdestination)
				{
				?>
					<tr>
						<td><?php echo h($Clientdestination['Clientdestination']['name']); ?></td>
						<td><?php echo h($Clientdestination['Clientdestination']['address']); ?></td>
						<td><?php echo h($Clientdestination['Clientdestination']['cap']); ?></td>
						<td><?php echo h($Clientdestination['Clientdestination']['city']); ?></td>
						<td><?php echo h($Clientdestination['Clientdestination']['province']); ?></td>
						<td><?php echo h($Clientdestination['Nation']['name']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, ['action' => 'edit', $Clientdestination['Clientdestination']['id'],$clientId],['title'=>__('Modifica'),'escape'=>false]);
								echo $this->Form->postLink($iconaElimina, ['action' => 'delete',$Clientdestination['Clientdestination']['id'],$clientId], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare il destino diverso ?', $Clientdestination['Clientdestination']['name']));
							?>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>
