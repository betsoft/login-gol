<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Tecnici','indexelements' => ['add'=>'Nuovo tecnico']]); ?>


<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
                <?php
                if (count($technicians) < 1)
				{
					?><tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">nessun tecnico trovato</td></tr><?php
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php foreach ($technicians as $technician)
				{
				?>
					<tr>
                        <td><?= h($technician['Technician']['name']); ?></td>
                        <td><?= h($technician['Technician']['surname']); ?></td>
                        <td><?= h($technician['Technician']['email']); ?></td>
						<td class="actions">
                            <?php
                                echo $this->Html->link($iconaModifica, ['action' => 'edit', $technician['Technician']['id']],['title'=>__('Modifica'),'escape'=>false]);
                                echo $this->Html->link('<i class="fa fa-list  verde" style="vertical-align:middle;font-size:20px;margin-right:5px;" technicianId = "'. $technician['Technician']['id'].'" ></i>', ['action' => 'techniciandetails','controller'=>'technicians', $technician['Technician']['id']],['title'=>__('Visualizza riepilogativo ore tecnico'),'escape'=>false]);
                                echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $technician['Technician']['id']]   , ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare il tecnico ?'  , $technician['Technician']['id']));
							?>
                            <!--i class="fa fa-user jsCreateUser" style="font-size:20px;vertical-align: middle;" technicianId="<?php // $technician['Technician']['id'] ?>" ></i-->
                            <?php
                               /* switch ($technician['Technician']['accountstate']) {
                                   case 0:
                                       ?><i class="fa fa-user jsCreateUser" style="font-size:20px;vertical-align: middle;"
                                            technicianId="<?= $technician['Technician']['id'] ?>"
                                            title="Crea utenza per il tecnico."></i><?php
                                       break;
                                   case 1:
                                       ?><i class="fa fa-user jsEnableUser" style="font-size:20px;vertical-align: middle;"
                                            technicianId="<?= $technician['Technician']['id'] ?>"
                                            title="Abilita utenza per il tecnico"></i><?php
                                       break;
                                   case 2:
                                       ?><i class="fa fa-user jsDisableUser" style="font-size:20px;vertical-align: middle;"
                                            technicianId="<?= $technician['Technician']['id'] ?>"
                                            title="Disabilita utenza per il tecnico."></i><?php
                                       break;
                               } */
                               ?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
<script>
    // Crea utenza gestionale
    $(".jsCreateUser").click(function() {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "users", "action" => "createUserFromTechnician"]) ?>",
            data:
                {
                    technicianId: $(this).attr("technicianId"),
                },
            success: function (data) {
                $(this).attr("color", "green");
            }
        });
    });

    $(".jsEnableUser").click(function() {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "users", "action" => "enableUserFromTechnician"]) ?>",
            data:
                {
                    technicianId: $(this).attr("technicianId"),
                },
            success: function (data) {
                $(this).attr("color", "green");
            }
        });
    });


    $(".jsDisableUser").click(function() {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "users", "action" => "disableUserFromTechnician"]) ?>",
            data:
                {
                    technicianId: $(this).attr("technicianId"),
                },
            success: function (data) {
                $(this).attr("color", "red");
            }
        });
    });

</script>