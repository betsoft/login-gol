<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?= $this->Html->link('indietro', ['controller'=>'technicians','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']); ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Schede d\'intervento tecnico' . ' - ' . $TechnicianName]); ?>

<?php $totalWork = $totalNightwork = 0; ?>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class ="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <tr>
                <?=  $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
                    'htmlElements' => [
                        '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input f datepicker" name="data[filters][date1]" value="'.$startDate.'"  bind-filter-event="change"/>'.''.
                        '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input  datepicker" name="data[filters][date2]"  value="'.$endDate.'" bind-filter-event="change"/></center>'
                    ]]);
                ?></tr>
            </thead>
            <tbody class="ajax-filter-table-content">
                <?php if(count($technicianHours) < 1): ?>
                    <tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">nessuna scheda d'intervento trovata</td></tr>
                <?php else: ?>
                    <?php foreach ($technicianHours as $hour): ?>
                        <?php foreach ($hour['Maintenancehour'] as $row): ?>
                            <?php $exist = false; ?>
                            <?php foreach($row['Maintenancehourstechnician'] as $maintenanceHourTechnician): ?>
                                <?php if($maintenanceHourTechnician['technician_id'] == $technicinaId): ?>
                                    <?php $exist = true; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if($row['partecipating'] == 1 && $exist): ?>
                                <tr>
                                    <td style="text-align: center;"><?= $hour['Maintenance']['maintenance_number']; ?></td>
                                    <td style="text-align: center;"><?= date("d-m-Y", strtotime($hour['Maintenance']['maintenance_date'])); ?></td>
                                    <td><?= $hour['Maintenance']['intervention_description']; ?></td>
                                    <td style="text-align: center;"><?= $row['hour_from']; ?></td>
                                    <td style="text-align: center;"><?= $row['hour_to']; ?></td>
                                    <td class="right">
                                        <?php
                                            echo $utilities->differencesInMinutes(date("H:i", strtotime( $row['hour_from'])), date("H:i", strtotime($row['hour_to'])));
                                            $totalWork += $utilities->differencesInMinutes(date("H:i", strtotime($row['hour_from'])), date("H:i", strtotime($row['hour_to'])), false);
                                        ?>
                                    </td>
                                    <td style="text-align: center;"><?= $utilities->isWeekend($hour['Maintenance']['maintenance_date']) ? '' : 'NO'; ?></td>
                                    <td class="right">
                                        <?php
                                            echo $utilities->formatTimeInHourAndMinutes($utilities->getNightTimeHourAndMinutes($row['hour_from'], $row['hour_to']));
                                            $totalNightwork += $utilities->getNightTimeHourAndMinutes($row['hour_from'], $row['hour_to']);
                                        ?>
                                    </td>
                                    <td><?= $hour['Constructionsite']['name'] . ' - ' . $hour['Constructionsite']['description']; ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    <tr><td colspan="5"></td><td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalWork) ?></b></td><td></td><td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalNightwork) ?></b></td><td></td></tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?=  $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>
<?=  $this->element('Js/datepickercode'); ?>