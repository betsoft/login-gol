<?= $this->Form->create('Technicians', ['type' => 'file']); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo tecnico') ?></span>

<div class="col-md-12"><hr></div>


<div class="form-group col-md-12">
    <!--div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Codice</strong></label>
        <div class="form-controls">
            <?php // $this->Form->input('code', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>10]); ?>
        </div>
    </div-->

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Nome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
    <label class="form-margin-top form-label"><strong>Cognome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
	           <?= $this->Form->input('surname', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
         </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Email</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('email', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255,'type'=>'email']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Orario</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Notturno</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_night', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Weekend</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_weekend', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>

</div>
<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left">
        <label form-label"><strong>Firma tecnico</strong></label>
        <?=  $this->Form->input('sign',['div' => false, 'label' => false, 'type'=>'file','class'=>'form-control']); ?>
        </br>
    </div>
</div>
<div class="col-md-12"><hr></div>



<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

