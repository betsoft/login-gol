<?=  $this->Form->create('Technicians', ['type' => 'file']); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica tecnici'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',['class'=>'form-control']); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Nome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Cognome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('surname', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Email</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('email', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255,'type'=>'email']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Orario</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Notturno</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_night', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Weekend</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_weekend', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
</div>
<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Firma</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('sign', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255]); ?>
            <?php
                if(isset($technician['Technicians']['sign']) && $technician['Technicians']['sign'] != '')
                {
                    ?>
                    <label class="form-margin-top form-label attualLogo"><strong>Firma attuale</strong></label>
                    <div class="form-controls ">
                         <img src="data:image/jpg;base64,<?= $technician['Technicians']['sign'] ?>" />
                    </div>
                    <?php
                 }
            ?>
        </div>
    </div>
</div>
	 <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>

