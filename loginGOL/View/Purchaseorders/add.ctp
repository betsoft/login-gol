<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>

<!--<script>
    $(document).ready(
        function()
        {
            // Nascondo a priori
            $("#differentAddress").hide();
            $("#differentAddressreseller").hide();
            $("#Good0Description").prop('required',true);

            var fornitori = setSuppliers();
            var articoli = setArticles();
            var codici = setCodes();

            setSupplierAutocomplete(fornitori,"#PurchaseorderSupplierId");

            loadSupplierCatalog(articoli,"Good","0","ordineacquisto",codici);
            // Definisce quel che succede all'autocomplete del fornitore

            $('#BillImporto').hide();
            // Abilita il clonable [è giusto billclientiid perché se undefined prende il supplier]
            enableCloning($("#BillClientId").val(),"#PurchaseorderSupplierId");
            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );
</script>-->

<?= $this->Form->create('Purchaseorder', ['class' => 'uk-form uk-form-horizontal' ]); ?>

<div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Nuovo ordine d'acquisto</div>
<div class="col-md-12"><hr></div>

<?php $attributes = ['legend' => false]; ?>

<div class="form-group col-md-12">
    <div class="col-md-4" style="float:left;" >
        <label class="form-label"><strong>Numero ordine d'acquisto</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('number', ['label' => false, 'class' => 'form-control', 'required' => true, 'value' => $purchaseOrderNumber]); ?>
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Data entrata merce</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Purchaseorder][date]" value="<?= date("d-m-Y"); ?>"  required="true" />
        </div>
    </div>
    <div class="col-md-4">
        <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'supplier_id',
                "aggregator" => '',
                "prefix" => "supplier_id",
                "list" => $suppliers,
                "options" => ['multiple' => false,'required' => true],
            ]);
            ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-label form-margin-top"><strong>Nota</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['div' => false,'label' => false,'class' => 'form-control']);?>
        </div>
    </div>
</div>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe ordine d'acquisto</div>
    <div class="col-md-12"><hr></div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture"  class="col-md-12">
        <div class="principale contacts_row clonableRow originale ultima_riga">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
            <div class="col-md-12">
                <?php $this->Form->input('Good.0.hidden', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo']]); ?>
                <div class="col-md-2" >
                    <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                    <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'required'=>true]); ?>
                </div>
                <div class="col-md-4">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.description', ['div' => false,'label' => false,'class' => 'form-control goodDescription jsDescription']); ?>
                    <?= $this->Form->hidden('Good.0.storage_id'); ?>
                    <?= $this->Form->hidden('Good.0.movable',['class'=>'jsMovable','value'=>1]); ?>
                    <?= $this->Form->hidden('Good.0.variation_id',['type'=>'text']); ?>
                </div>
                <div class="col-md-2">
                    <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                    <?php echo $this->Form->input('Good.0.quantity', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity','type'=>"number",'step'=>"0.001", 'min'=>"0",'required'=>true]);?>
                </div>
                <div class="col-md-2">
                    <label class="form-margin-top"><strong>Unità di misura</strong></label>
                    <?=  $this->Form->input('Good.0.unit_of_measure_id', ['label' => false,'type'=>'select','class' => 'form-control', 'options' => $units, 'empty'=>true]); ?>
                </div>
                <div class="col-md-2">
                    <label class="form-margin-top "><strong>Costo</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.price', ['label' => false,'class' => 'form-control jsPrice','min'=>'0','step'=>'0.01','type'=>'number', 'required'=>true]); ?>
                </div>
                <div class="col-md-2">
                    <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.vat_id', ['label' => false,'class' => 'form-control','type'=>'select','options'=>$vats,'empty'=>true,'required'=>true]);?>
                </div>
                <div class="col-md-2">
                    <label class="form-margin-top form-label"><strong>Importo</strong></label>
                    <?=  $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
                </div>
            </div>
            <div class="col-md-12"><hr></div>
        </div>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
</div>

<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function()
        {
            // Nascondo a priori
            $("#differentAddress").hide();
            $("#differentAddressreseller").hide();
            $("#Good0Description").prop('required',true);

            var fornitori = setSuppliers();
            var articoli = setArticles();
            var codici = setCodes();

            setSupplierAutocomplete(fornitori,"#PurchaseorderSupplierId");

            loadSupplierCatalog(articoli,"Good","0","ordineacquisto",codici);
            // Definisce quel che succede all'autocomplete del fornitore

            $('#BillImporto').hide();
            // Abilita il clonable [è giusto billclientiid perché se undefined prende il supplier]
            enableCloning($("#BillClientId").val(),"#PurchaseorderSupplierId");
            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );
</script>

<script>
    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function(){setImporto(this); });
    $(".jsQuantity").change(function(){setImporto(this);  });

    function setImporto(row)
    {
        var quantity = $(row).parents(".clonableRow").find(".jsQuantity").val();
        var prezzo = $(row).parents(".clonableRow").find(".jsPrice").val();
        var importo = quantity * prezzo;
        $(row).parents(".clonableRow").find(".jsImporto").val(importo.toFixed(2));
    }

    function setCodeRequired(row)
    {
        var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();

        if (tipo == 1)
        {
            $(row).parents(".clonableRow").find(".jsCodice").attr('required', true);
            $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
            $(row).parents(".clonableRow").find(".jsMovable").val(1);
        }
        else
        {
            $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
            $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
            $(row).parents(".clonableRow").find(".jsMovable").val(0);
        }
    }
    // Fine codice per gestione prezzo/quantity/tipo

    $("#PurchaseorderAddForm").on('submit.default',function(ev) { });

    $("#PurchaseorderAddForm").on('submit.validation',function(ev)
    {
        ev.preventDefault(); // to stop the form from submitting
        checkPurchaseorderDuplicate();
    });
</script>

<script>
    function checkPurchaseorderDuplicate()
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "purchaseorders", "action" => "checkPurchaseorderDuplicate"]) ?>",
            data:
            {
                purchaseordernumber: $("#PurchaseorderNumber").val(),
                date: $("#datepicker").val(),
            },
            success: function (data)
            {
                errore = 0;

                if (data > 0)
                {
                    errore = 4;
                }

                switch (errore)
                {
                    case 0:
                        $("#PurchaseorderAddForm").trigger('submit.default');
                    break;
                    case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione esiste già un ordine d\' acquisto con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });
                        return false;
                    break;
                }
            },
            error: function (data)
            {
            }
        });
    }
</script>
