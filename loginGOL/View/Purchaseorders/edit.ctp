<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<script>
    $(document).ready
    (
        function() {
            var fornitori = setSuppliers();
            var articoli = setArticles();
            var codici = setCodes();

            // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori, "#PurchaseorderSupplierId");

         //   loadSupplierCatalog(articoli, "Purchaseordergoodrow", "0", "ordineacquisto", codici);
            loadSupplierCatalog(articoli, "Purchaseorderrow", "0", "ordineacquisto", codici);

            enableCloningedit(null, "#PurchaseorderSupplierId");

            addcrossremoving();
        }
    );
</script>

    <?= $this->Form->create('Purchaseorder', ['class' => 'uk-form uk-form-horizontal']); ?>

           <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Modifica ordine d'acquisto</div>
             <div class="col-md-12"><hr></div>

    <div class="form-group col-md-12">
        <div class="col-md-2"  >
            <label class="form-label form-margin-top"><strong>Numero ordine d'acquisto</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?=  $this->Form->input('number', ['label' => false,'class' => 'form-control', 'required'=>true ]); ?>
                <?=  $this->Form->hidden('id'); ?>
            </div>
        </div>
	    <div class="col-md-2" >
            <label class="form-label form-margin-top"><strong>Data ordine d'acquisto</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Purchaseorder][date]" value=<?= $this->Time->format('d-m-Y', $this->data['Purchaseorder']['date']);  ?> />
            </div>
        </div>
        <div class="col-md-4" >
        <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'supplier_id',
                "aggregator" => '',
                "prefix" => "supplier_id",
                "list" => $suppliers,
                "options" => [ 'multiple' => false,'required'=> true],
            ]);
            ?>
    </div>
    </div>


     <div class="col-md-12"><hr></div>

        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe ordine d'acquisto</div>
         <div class="col-md-12"><hr></div>
       <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>

        <fieldset id="fatture"  class="col-md-12">

        <?php
            $conto = count($this->request->data['Purchaseorderrow']) - 1;
            $i = -1;
            ?>
            <script>
                var articoli = setArticles();
                var codici = setCodes();
           </script>
           <?php

            foreach ($this->request->data['Purchaseorderrow'] as $chiave => $oggetto)
            {
                $i++;
                $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
                $chiave == 0 ? $classe1 = "originale" : $classe1 = '';
            ?>

                <!--div class="principale<?= $oggetto['id'] ?> lunghezza clonableRow contacts_row   <?php // $classe1 ?>  <?php // $classe ?>" id="' <?php // $chiave ?> '"-->
                <div class="principale lunghezza clonableRow contacts_row   <?= $classe1 ?>  <?= $classe ?>" id="' <?= $chiave ?> '">
                    <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                        <?= $this->Form->input("Good.$chiave.id"); ?>
                    <div class="col-md-12">
                        <!--div class="col-md-2">
                         <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                        <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '0';    ?>

                        <?php  // $this->Form->input("Purchaseorderrow.$chiave.tipo", ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo'],'value'=>$movableValue ,'disabled'=>true]); ?>
                       <?=  $this->Form->hidden("Purchaseorderrow.$chiave.tipo", ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo'],'value'=>$movableValue ,'disabled'=>true]); ?>
                       </div-->
                        <div class="col-md-2">
                             <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?=  $this->Form->input("Purchaseorderrow.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control jsCodice','required'=>true]); ?>
                        </div>
                        <div class="col-md-4">
                            <label class="form-margin-top"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Purchaseorderrow.$chiave.description", [ 'div' => false, 'label' => false, 'class' => 'form-control  jsDescription']); ?>
                            <?= $this->Form->hidden("Purchaseorderrow.$chiave.storage_id"); ?>
                             <?= $this->Form->hidden("Purchaseorderrow.$chiave.movable",['class'=>'jsMovable','value'=>$movableValue ]); ?>
                             <?= $this->Form->hidden("Purchaseorderrow.$chiave.variation_id"); ?>
                        </div>
                    <div class="col-md-2">
              <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
              <?php
                echo $this->Form->input("Purchaseorderrow.$chiave.quantity", ['label' => false,'class' => 'form-control jsQuantity','type'=>'number', 'min'=>"0", 'step'=>"0.001"]);?>
           </div>
                        <div class="col-md-2">
             <label class="form-margin-top"><strong>Unità di misura</strong></label>
                     <?=  $this->Form->input("Purchaseorderrow.$chiave.unit_of_measure_id", ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units, 'empty'=>true]); ?>
           </div>
           <div class="col-md-2">
             <label class="form-margin-top "><strong>Costo</strong><i class="fa fa-asterisk"></i></label>
             <?php //  $this->Form->input("Purchaseorderrow.$chiave.load_good_row_price", ['label' => false,'class' => 'form-control jsPrice','type'=>'number']); ?>
             <?=  $this->Form->input("Purchaseorderrow.$chiave.price", ['label' => false,'class' => 'form-control jsPrice','type'=>'number', 'min'=>'0','step'=>'0.01']); ?>
           </div>
           <div class="col-md-2">
             <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                     <?= $this->Form->input("Purchaseorderrow.$chiave.vat_id", ['label' => false,'class' => 'form-control','type'=>'number','type'=>'select','options'=>$vats,'empty'=>true]);?>
              </div>
            <div class="col-md-2">
                           <label class="form-margin-top form-label"><strong>Importo</strong></label>
                             <?=  $this->Form->input("Purchaseorderrow.$chiave.importo", ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true, 'value'=> number_format($oggetto['quantity'] * $oggetto['price'],2,',','')]); ?>
                           </div>
          <!--div class="col-md-2">
             <label class="form-label"><strong>Codice a barre</strong></label>
             <?php
             //   echo $this->Form->input("Purchaseorderrow.$chiave.barcode", ['div' => false,'label' => false,'class' => 'form-control goodBarcode']);
            ?>
           </div-->
           <div class="row" style="padding-bottom:10px;"><hr></div>
                    </div>
                </div>

                <script>
                    //loadSupplierCatalog(articoli,"Good","<?= $chiave; ?>","ordineacquisto",codici);
                    loadSupplierCatalog(articoli,"Purchaseorderrow","<?= $chiave; ?>","ordineacquisto",codici);
                </script>
        <?php } ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
    <?php echo $this->Form->end(); ?>

    <?= $this->element('Js/datepickercode'); ?>

    <script>
         // Inizio codice per gestione prezzo/quantity/tipo
         $(".jsPrice").change(function(){setImporto(this); });
         $(".jsQuantity").change(function(){setImporto(this);});
         $(".jsTipo").change(function(){setCodeRequired(this);});
    </script>

    <script>
        $("#PurchaseorderEditForm").on('submit.default',function(ev)
        {
        });

        $("#PurchaseorderEditForm").on('submit.validation',function(ev)
        {
            ev.preventDefault(); // to stop the form from submitting
            checkPurchaseorderDuplicate();
        });
    </script>

 <script>
	 function checkPurchaseorderDuplicate() {
         var errore = 0;

         $.ajax
         ({
             method: "POST",
             url: "<?= $this->Html->url(["controller" => "purchaseorders", "action" => "checkPurchaseorderDuplicate"]) ?>",
             data:
                 {
                     purchaseordernumber: $("#PurchaseorderNumber").val(),
                     date: $("#datepicker").val(),
           //          supplier_id: $("#supplier_id_multiple_select").val()
                 },
             success: function (data)
             {
                 if (data > 0 && ($("#PurchaseorderNumber").val() != '<?= $this->data['Purchaseorder']['number']; ?>')) {
                     errore = 4;
                 }
                 switch (errore) {
                     case 0:
                         $("#PurchaseorderEditForm").trigger('submit.default');
                         break;
                     case 4:
                         $.alert
                         ({
                             icon: 'fa fa-warning',
                             title: '',
                             content: 'Attenzione esiste già un ordine \'acquisto con lo stesso numero nell\'anno corrente.',
                             type: 'orange',
                         });
                         return false;
                         break;
                 }
             },
             error: function (data) {
             }
         });
     }
    </script>
