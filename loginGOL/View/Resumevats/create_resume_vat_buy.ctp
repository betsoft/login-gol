<table style="font-size:12px;">
    <tr><td><?= $setting['Setting']['name']; ?></td></tr>
    <tr><td><?= $setting['Setting']['indirizzo']; ?></td></tr>
    <tr><td><?= $setting['Setting']['cap'] . ' ' . $setting['Setting']['citta'] . ' ('. $setting['Setting']['prov'] . ') '; ?></td></tr>
    <tr><td><?= "C.F./P.Iva" . (isset($setting['Setting']['piva']) && $setting['Setting']['piva'] != '') ? $setting['Setting']['piva'] : $setting['Setting']['cf']; ?></td></tr>
</table>

<div style="margin-top:20px;"><b>Registro Iva acquisti dal <?= $dateFrom ?> al <?= $dateTo ?></b></div>

<table width="100%" style="font-size:10px;margin-top:20px;">
    <thead>
        <tr>
            <th width="5%" style="text-align: center">N°</th>
            <th width="35%" style="text-align: left;" >Documento</th>
            <th width="30%" style="text-align: left;" >Fornitore</th>
            <th width="10%" style="text-align: center;">Codice IVA</th>
            <th width="10%" style="text-align: center;">Imponibile</th>
            <th width="10%" style="text-align: center;">Imposta</th>
        </tr>
    </thead>
    <tbody>
    <?php $imponibileTotale = 0; $impostaTotale = 0; ?>
    <?php foreach($bills as $bill): ?>
        <?php
            unset($arrayIva);

            $coefficiente = 1;

            switch($bill['Bill']['typeofdocument'])
            {
                case "TD01":
                    $documentType = "Fattura";
                    break;
                case "TD02":
                    $documentType = "Acconto/Anticipo su fattura";
                    break;
                case "TD03":
                    $documentType = "Acconto/Anticipo su parcella";
                    break;
                case "TD04":
                    $documentType  = "Nota di Credito";
                    $coefficiente = -1;
                    break;
                case "TD05":
                    $documentType  = "Nota di Debito";
                    break;
                case "TD06":
                    $documentType =  "Parcella";
                    break;
                case "TD07":
                    $documentType =  "Fattura semplificata";
                    break;
                case "TD08":
                    $documentType =  "Nota di Credito semplificata";
                    $coefficiente = -1;
                    break;
                default:
                    $documentType = "Fattura";
                    break;
                }

            $impostaparziale = 0; $imponibileParziale = 0;

            foreach($bill['Good'] as $good) {
                $good['quantita'] == null ? $good['quantita'] = 1 : null;
                if($_SESSION['Auth']['User']['dbname'] == 'login_GE0007') {
                    if ($good['category_id'] !== '33') {
                        if ($good['imported_total_row'] != null) {
                            $arrayIva[$good['Iva']['id']]['percentuale'] == null ? $arrayIva[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                            $arrayIva[$good['Iva']['id']]['codice'] == null ? $arrayIva[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                            $arrayIva[$good['Iva']['id']]['natura'] == null ? $arrayIva[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                            isset($arrayIva[$good['Iva']['id']]['imponibile']) ? $arrayIva[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['imported_total_row'] : $arrayIva[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['imported_total_row'];
                            isset($arrayIva[$good['Iva']['id']]['imposta']) ? $arrayIva[$good['Iva']['id']]['imposta'] += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $arrayIva[$good['Iva']['id']]['imposta'] = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;

                            isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $good['imported_total_row'] : $imponibileParziale = $coefficiente * $good['imported_total_row'];
                            isset($impostaparziale) ? $impostaparziale += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $impostaparziale = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;

                            $arrayTotale[$good['Iva']['id']]['percentuale'] == null ? $arrayTotale[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                            $arrayTotale[$good['Iva']['id']]['codice'] == null ? $arrayTotale[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                            $arrayTotale[$good['Iva']['id']]['natura'] == null ? $arrayTotale[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                            isset($arrayTotale[$good['Iva']['id']]['imponibile']) ? $arrayTotale[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['imported_total_row'] : $arrayTotale[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['imported_total_row'];
                            isset($arrayTotale[$good['Iva']['id']]['imposta']) ? $arrayTotale[$good['Iva']['id']]['imposta'] += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $arrayTotale[$good['Iva']['id']]['imposta'] = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;
                        } else {
                            $arrayIva[$good['Iva']['id']]['percentuale'] == null ? $arrayIva[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                            $arrayIva[$good['Iva']['id']]['codice'] == null ? $arrayIva[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                            $arrayIva[$good['Iva']['id']]['natura'] == null ? $arrayIva[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                            isset($arrayIva[$good['Iva']['id']]['imponibile']) ? $arrayIva[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['prezzo'] * $good['quantita'] : $arrayIva[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['prezzo'] * $good['quantita'];
                            isset($arrayIva[$good['Iva']['id']]['imposta']) ? $arrayIva[$good['Iva']['id']]['imposta'] += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $arrayIva[$good['Iva']['id']]['imposta'] = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;

                            isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $good['prezzo'] * $good['quantita'] : $imponibileParziale = $coefficiente * $good['prezzo'] * $good['quantita'];
                            isset($impostaparziale) ? $impostaparziale += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $impostaparziale = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;

                            $arrayTotale[$good['Iva']['id']]['percentuale'] == null ? $arrayTotale[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                            $arrayTotale[$good['Iva']['id']]['codice'] == null ? $arrayTotale[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                            $arrayTotale[$good['Iva']['id']]['natura'] == null ? $arrayTotale[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                            isset($arrayTotale[$good['Iva']['id']]['imponibile']) ? $arrayTotale[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['prezzo'] * $good['quantita'] : $arrayTotale[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['prezzo'] * $good['quantita'];
                            isset($arrayTotale[$good['Iva']['id']]['imposta']) ? $arrayTotale[$good['Iva']['id']]['imposta'] += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $arrayTotale[$good['Iva']['id']]['imposta'] = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;
                        }


                    }
                }
                else{
                    if ($good['imported_total_row'] != null) {
                        $arrayIva[$good['Iva']['id']]['percentuale'] == null ? $arrayIva[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                        $arrayIva[$good['Iva']['id']]['codice'] == null ? $arrayIva[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                        $arrayIva[$good['Iva']['id']]['natura'] == null ? $arrayIva[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                        isset($arrayIva[$good['Iva']['id']]['imponibile']) ? $arrayIva[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['imported_total_row'] : $arrayIva[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['imported_total_row'];
                        isset($arrayIva[$good['Iva']['id']]['imposta']) ? $arrayIva[$good['Iva']['id']]['imposta'] += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $arrayIva[$good['Iva']['id']]['imposta'] = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;

                        isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $good['imported_total_row'] : $imponibileParziale = $coefficiente * $good['imported_total_row'];
                        isset($impostaparziale) ? $impostaparziale += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $impostaparziale = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;

                        $arrayTotale[$good['Iva']['id']]['percentuale'] == null ? $arrayTotale[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                        $arrayTotale[$good['Iva']['id']]['codice'] == null ? $arrayTotale[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                        $arrayTotale[$good['Iva']['id']]['natura'] == null ? $arrayTotale[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                        isset($arrayTotale[$good['Iva']['id']]['imponibile']) ? $arrayTotale[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['imported_total_row'] : $arrayTotale[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['imported_total_row'];
                        isset($arrayTotale[$good['Iva']['id']]['imposta']) ? $arrayTotale[$good['Iva']['id']]['imposta'] += $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100 : $arrayTotale[$good['Iva']['id']]['imposta'] = $coefficiente * $good['imported_total_row'] * $good['Iva']['percentuale'] / 100;
                    } else {
                        $arrayIva[$good['Iva']['id']]['percentuale'] == null ? $arrayIva[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                        $arrayIva[$good['Iva']['id']]['codice'] == null ? $arrayIva[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                        $arrayIva[$good['Iva']['id']]['natura'] == null ? $arrayIva[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                        isset($arrayIva[$good['Iva']['id']]['imponibile']) ? $arrayIva[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['prezzo'] * $good['quantita'] : $arrayIva[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['prezzo'] * $good['quantita'];
                        isset($arrayIva[$good['Iva']['id']]['imposta']) ? $arrayIva[$good['Iva']['id']]['imposta'] += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $arrayIva[$good['Iva']['id']]['imposta'] = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;

                        isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $good['prezzo'] * $good['quantita'] : $imponibileParziale = $coefficiente * $good['prezzo'] * $good['quantita'];
                        isset($impostaparziale) ? $impostaparziale += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $impostaparziale = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;

                        $arrayTotale[$good['Iva']['id']]['percentuale'] == null ? $arrayTotale[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
                        $arrayTotale[$good['Iva']['id']]['codice'] == null ? $arrayTotale[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
                        $arrayTotale[$good['Iva']['id']]['natura'] == null ? $arrayTotale[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
                        isset($arrayTotale[$good['Iva']['id']]['imponibile']) ? $arrayTotale[$good['Iva']['id']]['imponibile'] += $coefficiente * $good['prezzo'] * $good['quantita'] : $arrayTotale[$good['Iva']['id']]['imponibile'] = $coefficiente * $good['prezzo'] * $good['quantita'];
                        isset($arrayTotale[$good['Iva']['id']]['imposta']) ? $arrayTotale[$good['Iva']['id']]['imposta'] += $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100 : $arrayTotale[$good['Iva']['id']]['imposta'] = $coefficiente * $good['prezzo'] * $good['quantita'] * $good['Iva']['percentuale'] / 100;
                    }
                }
                if ($bill['Bill']['collectionfees'] > 0) {
                    $arrayIva[$bill['Bill']['collectionfeesvatid']]['percentuale'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $ivas['Ivas']['percentuale'] : null;
                    $arrayIva[$bill['Bill']['collectionfeesvatid']]['codice'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['codice'] = $ivas['Ivas']['codice'] : null;
                    $arrayIva[$bill['Bill']['collectionfeesvatid']]['natura'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['natura'] = $ivasCode : null;
                    isset($arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile'] += $coefficiente * $bill['Bill']['collectionfees'] : $arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile'] = $coefficiente * $bill['Bill']['collectionfees'];
                    isset($arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta']) ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta'] += $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100 : $arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta'] = $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100;

                    $arrayTotale[$bill['Bill']['collectionfeesvatid']]['percentuale'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $ivas['Ivas']['percentuale'] : null;
                    $arrayTotale[$bill['Bill']['collectionfeesvatid']]['codice'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['codice'] = $ivas['Ivas']['codice'] : null;
                    $arrayTotale[$bill['Bill']['collectionfeesvatid']]['natura'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['natura'] = $ivasCode : null;
                    isset($arrayTotale[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imponibile'] += $coefficiente * $bill['Bill']['collectionfees'] : $arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile'] = $coefficiente * $bill['Bill']['collectionfees'];
                    isset($arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta']) ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta'] += $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100 : $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta'] = $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100;

                    isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $bill['Bill']['collectionfees'] : $imponibileParziale = $coefficiente * $bill['Bill']['collectionfees'];
                    isset($impostaparziale) ? $impostaparziale += $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100 : $impostaparziale = $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100;
                }
            }
        ?>
        <tr>
            <td><?= $bill['Bill']['id']?></td>
            <td style="text-align: left;"><?= $documentType.' n° '.$bill['Bill']['numero_fattura'].' del '.date('d-m-Y', strtotime($bill['Bill']['date'])); ?></td>
            <td><?= $bill['Bill']['supplier_name'] ?></td>
            <td style="text-align: center;">
                <?php foreach($arrayIva as $iva ): ?>
                    <?php if ($iva['percentuale'] != null): ?>
                        <?= $iva['codice'] . '<br/>'; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </td>
            <td style="text-align: right;">
                <?php foreach($arrayIva as $iva ): ?>
                    <?php if ($iva['percentuale'] != null): ?>
                        <?= number_format($iva['imponibile'], 2, ',', '\'') . ' €' . '<br/>'; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </td>
            <td style="text-align: right;">
                <?php foreach($arrayIva as $iva): ?>
                    <?php if ($iva['percentuale'] != null): ?>
                        <?= number_format($iva['imposta'], 2, ',', '\'') . ' €' . '<br/>'; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: right;"><b>Totale registrazioni:</b></td>
            <td></td>
            <td style="text-align: right;"><b><?= number_format($imponibileParziale,2,',','\''). ' €' ; ?></b></td>
            <td style="text-align: right;"><b><?= number_format($impostaparziale,2,',','\''). ' €' ; ?></b></td>
        </tr>
        <tr><td colspan="5"></td></tr>
        <tr><td colspan="5"></td></tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div style="text-align: center; margin-top:20px; margin-bottom:20px;"><b>Riepilogo complessivo</b></div>

<table width="100%" style="font-size:10px;">
    <thead>
        <tr>
            <th style="text-align: left;">Tipo IVA</th>
            <th style="text-align: right;">Imponibile</th>
            <th style="text-align: right;">Imposta</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($arrayTotale as $riepilogo): ?>
    <?php if($riepilogo['percentuale'] != null): ?>
        <tr>
            <?php isset($riepilogo['natura']) ? $natura = " - " . $riepilogo['natura']: $natura =  ""; ?>
            <td style="text-align:left;"><?=  str_pad($riepilogo['percentuale'],5,"0" ,STR_PAD_LEFT). "% - ". $riepilogo['codice'].$natura  ; ?></td>
            <td style="text-align:right;"><?=  number_format($riepilogo['imponibile'],2,',','\''). ' €' ?></td>
            <td style="text-align:right;"><?=  number_format($riepilogo['imposta'],2,',','\'') . ' €' ?></td>
        </tr>
        <?php
            $impostaTotale += number_format($riepilogo['imposta'],2,'.','');
            $imponibileTotale += number_format($riepilogo['imponibile'],2,'.','');
        ?>
    <?php endif; ?>
<?php endforeach; ?>
        <tr>
            <td></td>
            <td style="text-align:right"><b> <?= number_format($imponibileTotale,2,',','\'') . ' €' ?></b></td>
            <td style="text-align:right"><b><?= number_format($impostaTotale ,2,',','\'') . ' €' ?></b></td>
        </tr>
    </tbody>
</table>
