<table style="font-size:12px;margin-bottom:20px;">
    <tr><td><?= $setting['Setting']['name']; ?></td>
    <tr><td><?= $setting['Setting']['indirizzo']; ?></td>
    <tr><td><?= $setting['Setting']['cap'] . ' ' . $setting['Setting']['citta'] . ' ('. $setting['Setting']['prov'] . ') '; ?></td>
    <tr><td><?= "C.F./P.Iva" . (isset($setting['Setting']['piva']) && $setting['Setting']['piva'] != '') ? $setting['Setting']['piva'] : $setting['Setting']['cf']; ?></td>
</table>

<div>Registro Iva vendite dal <?= $dateFrom ?> al <?= $dateTo ?></div>

<table style="font-size:10px;margin-top:30px;">
    <thead>
    <tr>
        <th width="5%" style="text-align: left;" >Nr.</th>
        <th width="5%" style="text-align: left;" >Cliente</th>
        <th width="10%" style="text-align: left;" >Data documento</th>
        <th width="10%" style="text-align: left;" >Documento</th>
        <th width="20%" style="text-align: center;">Codice iva</th>
        <th width="20%" style="text-align: center;">Imponibile</th>
        <th width="20%" style="text-align: center;">Imposta</th>
    </tr>
    </thead>
    <tbody>
    <tr><td colspan="6"></td></tr><tr><td colspan="6"></td></tr>
    <?php

    $importoTotalte = $imponibileTotale  =0;

    foreach($bills as $bill)
    {
        unset($arrayIva);
        $bill['Bill']['tipologia'] == 1 ? $coefficiente = 1 : $coefficiente = -1;
        $impostaparziale =$imponibileParziale = 0;

        foreach($bill['Good'] as $good)
        {

            $arrayIva[$good['Iva']['id']]['percentuale'] == null ? $arrayIva[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
            $arrayIva[$good['Iva']['id']]['codice'] == null ? $arrayIva[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
            $arrayIva[$good['Iva']['id']]['natura'] == null ? $arrayIva[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
            isset($arrayIva[$good['Iva']['id']]['imponibile']) ? $arrayIva[$good['Iva']['id']]['imponibile']+= $coefficiente  * $good['prezzo'] * $good['quantita'] * (1 - $good['discount'] / 100) : $arrayIva[$good['Iva']['id']]['imponibile']  = $coefficiente  *  $good['prezzo'] * $good['quantita'] * (1 - $good['discount'] / 100);
            isset($arrayIva[$good['Iva']['id']]['imposta']) ? $arrayIva[$good['Iva']['id']]['imposta'] +=$coefficiente  * $good['prezzo'] * $good['quantita']* $good['Iva']['percentuale'] /100 : $arrayIva[$good['Iva']['id']]['imposta'] = $coefficiente  *  $good['prezzo']* $good['quantita']* $good['Iva']['percentuale'] /100;

            isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $good['prezzo']* $good['quantita'] * (1 - $good['discount'] / 100) :  $imponibileParziale = $coefficiente  * $good['prezzo']* $good['quantita'] * (1 - $good['discount'] / 100);
            isset($impostaparziale) ? $impostaparziale += $coefficiente *$good['prezzo'] * $good['quantita']* $good['Iva']['percentuale']/100: $impostaparziale = $coefficiente  * $good['prezzo']* $good['quantita'] *  $good['Iva']['percentuale']/100;

            $arrayTotale[$good['Iva']['id']]['percentuale'] == null ? $arrayTotale[$good['Iva']['id']]['percentuale'] = $good['Iva']['percentuale'] : null;
            $arrayTotale[$good['Iva']['id']]['codice'] == null ? $arrayTotale[$good['Iva']['id']]['codice'] = $good['Iva']['codice'] : null;
            $arrayTotale[$good['Iva']['id']]['natura'] == null ? $arrayTotale[$good['Iva']['id']]['natura'] = $good['Iva']['Einvoicevatnature']['code'] : null;
            isset($arrayTotale[$good['Iva']['id']]['imponibile']) ? $arrayTotale[$good['Iva']['id']]['imponibile']+= $coefficiente  *  $good['prezzo'] * $good['quantita'] * (1 - $good['discount'] / 100): $arrayTotale[$good['Iva']['id']]['imponibile']  = $coefficiente  *  $good['prezzo'] * $good['quantita'] * (1 - $good['discount'] / 100);
            isset($arrayTotale[$good['Iva']['id']]['imposta']) ? $arrayTotale[$good['Iva']['id']]['imposta'] += number_format($coefficiente  * $good['prezzo'] * $good['quantita']* $good['Iva']['percentuale'] /100,2,'.','') : $arrayTotale[$good['Iva']['id']]['imposta'] =  number_format($coefficiente  * $good['prezzo']* $good['quantita']* $good['Iva']['percentuale'] /100,2,'.','');

        }



        if($bill['Bill']['collectionfees'] >0 )
        {
            $ivas = $utilities->getVatFromId($bill['Bill']['collectionfeesvatid']);
            $ivasCode  = $utilities->getEinvoiceVatNatureCodeFromId($ivas['Ivas']['einvoicenature_id']);


            $arrayIva[$bill['Bill']['collectionfeesvatid']]['percentuale'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $ivas['Ivas']['percentuale']: null;
            $arrayIva[$bill['Bill']['collectionfeesvatid']]['codice'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['codice'] =$ivas['Ivas']['codice'] : null;
            $arrayIva[$bill['Bill']['collectionfeesvatid']]['natura'] == null ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['natura'] = $ivasCode : null;
            isset($arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile']+= $coefficiente  * $bill['Bill']['collectionfees']: $arrayIva[$bill['Bill']['collectionfeesvatid']]['imponibile']  = $coefficiente  *  $bill['Bill']['collectionfees'];
            isset($arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta']) ? $arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta'] += $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100 : $arrayIva[$bill['Bill']['collectionfeesvatid']]['imposta'] = $coefficiente  * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100;

            $arrayTotale[$bill['Bill']['collectionfeesvatid']]['percentuale'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $ivas['Ivas']['percentuale'] : null;
            $arrayTotale[$bill['Bill']['collectionfeesvatid']]['codice'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['codice'] =$ivas['Ivas']['codice'] : null;
            $arrayTotale[$bill['Bill']['collectionfeesvatid']]['natura'] == null ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['natura'] = $ivasCode : null;
            isset($arrayTotale[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imponibile']+= $coefficiente  * $bill['Bill']['collectionfees']: $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imponibile']  = $coefficiente  *  $bill['Bill']['collectionfees'];
            isset($arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta']) ? $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta'] += $coefficiente * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100 : $arrayTotale[$bill['Bill']['collectionfeesvatid']]['imposta'] = $coefficiente  * $bill['Bill']['collectionfees'] * $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100;

            isset($imponibileParziale) ? $imponibileParziale += $coefficiente * $bill['Bill']['collectionfees'] :  $imponibileParziale = $coefficiente  * $bill['Bill']['collectionfees'];
            isset($impostaparziale) ? $impostaparziale += $coefficiente *$bill['Bill']['collectionfees']*  $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100: $impostaparziale = $coefficiente  *$bill['Bill']['collectionfees'] *   $utilities->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) /100;

        }

        ?>
        <tr>
            <td width="5%"><?= $bill['Bill']['numero_fattura'] ?></td>
            <td width="5%"><?= $bill['Bill']['client_name'] ?></td>
            <td width="10%" style="text-align: center;"><?= date('d-m-Y', strtotime($bill['Bill']['date'])); ?></td>
            <td width="20%" style="text-align: left;">
                <?php
                switch ($bill['Bill']['tipologia'])
                {
                    case '1':
                        echo  'Fattura';
                        break;
                    case '3':
                        echo   'Nota di Credito';
                        break;
                }

                ?>
            </td>
            <td  width="20%" style="text-align: center;">
                <?php
                foreach($arrayIva as $iva )
                {
                    if($iva['percentuale'] != null ) {
                        echo $iva['codice'] . '<br/>';
                    }
                }
                ?>
            </td>
            <td  width="20%" style="text-align: right;">
                <?php
                foreach($arrayIva as $iva ) {
                    if ($iva['percentuale'] != null) {
                        echo number_format($iva['imponibile'], 2, ',', '\'') . ' €' . '<br/>';
                    }
                }
                ?>
            </td>
            <td  width="20%"  style="text-align: right;">
                <?php
                foreach($arrayIva as $iva) {
                    if ($iva['percentuale'] != null) {
                        echo number_format($iva['imposta'], 2, ',', '\'') . ' €' . '<br/>';
                    }
                }
                ?>
            </td>
        </tr>
        <tr>
            <td width="10%"></td>
            <td width="10%"></td>
            <td width="20%"><b>Totale registrazioni:</b></td>
            <td width="20%"></td>
            <td width="20%" style="text-align: right;"><b><?= number_format($imponibileParziale,2,',','\''). ' €' ; ?></b></td>
            <td width="20%" style="text-align: right;"><b><?= number_format($impostaparziale,2,',','\''). ' €' ; ?></b></td>
        </tr>
    <?php }

    ?>
    </tbody>
</table>

<div style="text-align: center; margin-top:40px; margin-bottom:40px;" >Riepilogo complessivo</div>
<table width="100%" style="font-size:10px;">
    <thead>

    <tr >

        <th width="50%" style="text-align: left;">Tipo IVA</th>
        <th width="10%" ></th>
        <th width="10%">Imponibile</th>
        <th width="30%" style="text-align:right;">Imposta</th>
    </tr>
    </thead>
    <tdoby>
        <?php

        foreach($arrayTotale as $riepilogo)
        {
            if($riepilogo['percentuale'] != null) {
                ?>
                <tr>
                    <?php isset($riepilogo['natura']) ? $natura = " - " . $riepilogo['natura'] : $natura = ""; ?>
                    <td width="50%" style="text-align:left;">
                        <?= str_pad($riepilogo['percentuale'], 5, "0", STR_PAD_LEFT) . "% - " . $riepilogo['codice'] . $natura; ?>
                    </td>
                    <td width="10%"></td>
                    <td width="10%"
                        style="text-align:right;"><?= number_format($riepilogo['imponibile'], 2, ',', '\'') . ' €' ?>
                    </td>
                    <td width="30%"
                        style="text-align:right;"><?= number_format($riepilogo['imposta'], 2, ',', '\'') . ' €' ?>
                    </td>
                </tr>
                <?php
                $impostaTotale += number_format($riepilogo['imposta'], 2, '.', '');
                $imponibileTotale += number_format($riepilogo['imponibile'], 2, '.', '');
            }
        }
        ?>
        <tr>
            <td colspan="3" style="text-align:right" >
                <b> <?= number_format($imponibileTotale,2,',','\'') . ' €' ?></b>
            </td>
            <td  style="text-align:right" >
                <b><?= number_format($impostaTotale ,2,',','\'') . ' €' ?></b>
            </td>
        </tr>
    </tdoby>
</table>
