
<?= $this->Html->script
	([ 
	    // Se tolgo questo non va più l'accordion
	    'plugins/bootstrap/js/bootstrap.js',  
	])
?>

<?php echo $this->Form->create('Setting', ['type' => 'file']); ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Impostazioni']); ?>
<br/>
<div class="panel-group" id="accordion"  style="font-family: 'Barlow Semi Condensed' !important;">
  <div class="panel panel-default">
    <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
      <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Aspetto stampe</a>
      </span>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
       <div class="panel-body">
          <div class="form-group col-md-12">
            <label class="form-margin-top form-label"><strong>Logo</strong></label><i class="fa fa-trash jsRemove" style="font-size:16pt;color:#ea5d0b;" title="Rimuovi il logo attuale"></i>
            <div class="form-controls"> 
              <?=  $this->Form->input('header',['div' => false, 'label' => false, 'type'=>'file','class'=>'form-control','class'=>'logo']); ?>
              </br>
             <?php
               if(isset($setting['Setting']['header']) && $setting['Setting']['header'] != '')
               { ?>
                 <label class="form-margin-top form-label attualLogo"><strong>Logo attuale</strong></label>
                 <div class="form-controls attualLogo">
                    <?= $this->Html->image('societa/'.$setting['Setting']['header'].'', ['style'=>'height:150px; width:auto;max-width:600px;']); ?>
                </div>
              <?php 
                }
              ?>
            </div>
     	   </div>
         <div class="form-group col-md-12">
         <div class=" col-md-3" style="float:left">
             <label class="form-controls "><strong>Allineamento intestazione (in assenza di logo)</strong></label>
             <div class="form-controls">
               <?=  $this->Form->input('logoalign',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['0'=>'Sinistra','1'=>'Centrato','2'=>'Destra']]); ?>
             </div>
         </div>
         <div class=" col-md-3" style="float:left;margin-left:10px;">
             <label class="form-controls "><strong>Visualizzazione dati azienda</strong></label>
             <div class="form-controls">
               <?=  $this->Form->input('bill_print_society_data_visible',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['1'=>'Sì','0'=>'No']]); ?>
             </div>
          </div>
          <div class=" col-md-3" style="float:left;margin-left:10px;">
             <label class="form-controls "><strong>Allineamento logo (in assenza di dati azienda)</strong></label>
             <div class="form-controls">
               <?php $setting['Setting']['bill_print_society_data_visible'] == 1 ? $disabled = true : $disabled = false; ?>
               <?=  $this->Form->input('bill_print_logo_align',['div' => false, 'label' => false, 'disabled'=>$disabled, 'class'=>'form-control','options'=>['0'=>'Sinistra','1'=>'Centrato','2'=>'Destra']]); ?>
             </div>
         </div>
        </div>
        
        <div class="form-group col-md-12">
          <div class=" col-md-3" style="float:left">
               <label class="form-controls "><strong>Nascondi colonna quantità dalla stampa fattura</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_print_remove_quantity',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['1'=>'Sì','0'=>'No']]); ?>
               </div>
           </div>
           <div class=" col-md-3" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Nascondi colonna sconto dalla stampa fattura</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_print_remove_discount',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['1'=>'Sì','0'=>'No']]); ?>
               </div>
            </div>
         <div class=" col-md-3" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Mostra deposito di prelievo nelle bolle</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('ddtdepositenabled',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['1'=>'Sì','0'=>'No']]); ?>
               </div>
         </div>
        </div>
        
         <div class="form-group col-md-12">
          <div class=" col-md-2" style="float:left">
               <label class="form-controls "><strong>Descrizione per "totale imponibile"</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_footer_total_taxableincome_desc',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
           </div>
           <div class=" col-md-2" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Descrizione per "totale iva"</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_footer_total_vat_desc',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
            </div>
         <div class=" col-md-2" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Descrizione per "totale fattura"</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_footer_total_desc',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
         </div>
         <div class=" col-md-2" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Descrizione per "Bolli"</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_footer_seal_desc',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
         </div>
         <div class=" col-md-2" style="float:left;margin-left:10px;">
               <label class="form-controls "><strong>Descrizione per "Netto a pagare"</strong></label>
               <div class="form-controls">
                 <?=  $this->Form->input('bill_footer_topay_desc',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
         </div>
        </div>
        
        <div class="form-group col-md-12">
         <div class=" col-md-4" style="float:left;">
             <label class="form-controls "><strong>Nota fine corpo fattura</strong></label>
             <div class="form-controls">
               <?=  $this->Form->input('bill_subscript_note',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'textarea','style'=>'resize:none;']); ?>
             </div>
          </div>
        </div>

        <div class="form-group col-md-12">
          <label class="form-margin-top form-label"><strong>Immagine a pedice</strong></label><i class="fa fa-trash jsRemove2" style="font-size:16pt;color:#ea5d0b;" title="Rimuovi l\'immagine a pedice"></i>
          <div class="form-controls"> 
          <?=  $this->Form->input('bill_subscript',['div' => false, 'label' => false, 'type'=>'file','class'=>'form-control','class'=>'subscript']); ?>
          </br>
          <?php
          if(isset($setting['Setting']['bill_subscript']) && $setting['Setting']['bill_subscript'] != '' && $setting['Setting']['bill_subscript'] != null)
               { ?>
                 <label class="form-margin-top form-label attualLogo"><strong>Immagine a pedice corrente</strong></label>
                  <div class="form-controls attualLogo">
                    <?= $this->Html->image('societa/'.$setting['Setting']['bill_subscript'].'', ['style'=>'height:150px; width:auto;max-width:800px;']); ?>
                </div>
              <?php 
                }
              ?>
            </div>
     	   </div>
     	    <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Note a fine intestazione</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('note',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
        </div>

           <h4 style="font-size:14px;">PREVENTIVI</h4>
           <div class="form-group col-md-12">
           <div class="form-group col-md-6">
               <label class="form-label form-margin-top"><strong>Intestazione blocco note a fine preventivo</strong></label>
               <div class="form-controls">
                   <?= $this->Form->input('eliminationdescription',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
           </div>
           <div class="form-group col-md-6">
               <label class="form-label form-margin-top"><strong>Note a fine preventivo</strong></label>
               <div class="form-controls">
                   <?= $this->Form->input('elimination',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'textarea']); ?>
               </div>
           </div>
           </div>
           <div class="form-group col-md-12">
           <div class="form-group col-md-4">
               <label class="form-label form-margin-top"><strong>Validità Preventivo</strong></label>
               <div class="form-controls">
                   <?= $this->Form->input('validity',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
           </div>
           <div class="form-group col-md-4">
               <label class="form-label form-margin-top"><strong>Descrizione per totale preventivo</strong></label>
               <div class="form-controls">
                   <?= $this->Form->input('quoteTotalDescription',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
               </div>
           </div>
           <div class="form-group col-md-4">
              <label class="form-label form-margin-top"><strong>Visualizzare Iva</strong></label>
                <div class="form-controls">
                    <?=  $this->Form->input('quoteVatVisible',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true,'options'=>['1'=>'Sì','0'=>'No']]); ?>
                </div>
              </div>
           </div>
       </div>
    </div>
  </div>
  
  
  <!-- Dati ditta -->
  <div class="panel panel-default">
     
    <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
      <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Dati Anagrafici</a>
  
    <!--div class="panel-heading">
      <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color:#ffffff !important;">Dati anagrafici</a-->

      </span>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        
          <div class="form-group col-md-12">
          <label class="form-label"><strong>Nome / Ragione sociale</strong></label>
          <div class="form-controls">
          <?= $this->Form->input('name',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
          </div>

   
        <div class="form-group col-md-12">
          <label class="form-label"><strong>P.iva</strong></label>
          <div class="form-controls">
          <?= $this->Form->input('piva',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group col-md-12">
          <label class="form-label"><strong>CF</strong></label>
          <div class="form-controls">
          <?= $this->Form->input('cf',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
        </div>

          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Indirizzo</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('indirizzo',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
          </div>

          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Città</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('citta',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
          </div>

          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Provincia</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('prov',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
            </div>
          </div>

          <div class="form-group col-md-12">
             <label class="form-label form-margin-top"><strong>CAP</strong></label>
                <div class="form-controls">
              <?=  $this->Form->input('cap',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
          </div>

          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Telefono</strong></label>
            <div class="form-controls">
              <?= $this->Form->input('tel',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
          </div>

          <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0041") { ?>
              <div class="form-group col-md-12">
                  <label class="form-label form-margin-top"><strong>Rapporto Fidelity</strong></label>
                  <div class="form-controls">
                      <?=  $this->Form->input('euro_punti',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
                  </div>
              </div>
          <?php } ?>
          
          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Fax</strong></label>
            <div class="form-controls">
              <?= $this->Form->input('fax',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
          </div>

          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Mail</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('email',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'email']); ?>
            </div>
          </div>
          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Pec</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('pec',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'email']); ?>
            </div>
          </div>
      </div>
    </div>
  </div>

  <!-- DATI EMAIL -->
  <div class="panel panel-default">
    <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
      <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Email</a>
      </span>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body  ">
      	<div class=" -badge tools">
			</div>
          <div class="form-group col-md-12">
            <label class="form-label"><strong>Host SMTP</strong></label>
              <div class="form-controls">
              <?=  $this->Form->input('host', ['div' => false, 'label' => false, 'class' => 'form-control remove-space', 'id' => 'lunghezza']); ?>
              </div>
					</div>
          
          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Porta SMTP</strong></label>
            <div class="form-controls">
              <?= $this->Form->input('port',['div' => false, 'label' => false, 'class'=>'form-control remove-space']); ?>
            </div>
					</div>
          
          <div class="form-group col-md-12">
            <label class="form-label form-margin-top"><strong>Mail</strong></label>
            <div class="form-controls">
              <?= $this->Form->input('username',['div' => false, 'label' => false, 'class'=>'form-control remove-space']); ?>
            </div>
					</div>
          
					<div class="form-group col-md-12">
					  <label class="form-label form-margin-top"><strong>Password</strong></label>
					  <div class="form-controls">
					  <?=  $this->Form->input('password',['div' => false, 'label' => false, 'class'=>'form-control remove-space']); ?>
					  </div>
					</div>
          <?php
				  	  $smtpSec=["ssl"=>"ssl","tls"=>"tls"];
					?>
					<div class="form-group col-md-12">
					  <label class="form-label form-margin-top"><strong>Protocollo di Sicurezza</strong></label>
					  <div class="form-controls">
					  <?= $this->Form->input('smtpSecure',['div' => false, 'label' => false, 'class'=>'form-control remove-space','type'=>"select",'options'=>$smtpSec]); ?>
					  </div>
					</div>
      </div>
    </div>
  </div>
  <!-- Fine email -->
  
  <!-- Magazzino -->  
  	<?php if(ADVANCED_STORAGE_ENABLED) { ?>
  	<div class="panel panel-default">
       <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseStorage" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Magazzino</a>
			</span>
		</div>
		<div id="collapseStorage" class="panel-collapse collapse">
			<div class="panel-body  ">
				<div class=" -badge tools">
			</div>
        <div class="form-group col-md-12">
					<label ><strong>Metodo di valutazione scorte di magazzino</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('storage_evaluation_method', ['div' => false, 'label' => false,'type'=>'select','options'=>['CMP'=> 'Costo medio ponderato','FIFO'=>'FIFO','LIFO'=>'LIFO'],  'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
<!-- Fine Riba -->    

  
  <!-- Dati di fatturazione -->
  <div class="panel panel-default jsFatturazione">
    <!--div class="panel-heading">
      <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnePointFive" style="color:#ffffff !important;">Fatturazione</a-->
        
       <div class="panel-heading " style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnePointFive" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Fatturazione</a>
  
      </span>
    </div>
    <div id="collapseOnePointFive" class="panel-collapse collapse">
      <div class="panel-body">
      
      <h4 style="font-size:14px;">Ritenuta d'acconto</h4>
        <div class="form-group" >
           <div class="col-md-4" >
              <label class="form-label form-margin-top"><strong>Tipologia ritenuta</strong></label>
              <div class="form-controls">
                <?= $this->Form->input('type_of_person',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'select','options'=> $einvoiceTypeOfPerson,'empty'=>true]); ?>
            </div>
          </div>
          <div class=" col-md-4" >
            <label class="form-label"><strong>Ritenuta d'acconto (%)</strong></label>
            <div class="form-controls" >
              <?= $this->Form->input('withholding_tax',['div' => false, 'label' => false, 'class'=>'form-control','min'=>0,'max'=>100]); ?>
            </div>
          </div>
          <div class="col-md-4" >
            <label class=""><strong>Causale del pagamento (come da mod. 770)</strong></label>
            <div class="form-controls">
            <?= $this->Form->input('withholding_tax_causal_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $einvoiceWitholdingTaxCausals ]); ?>
            </div>
        </div>
       
      </div>
     <h4 style="font-size:14px;">Cassa previdenziale</h4>
       <div class="form-group col-md-12">
         <div class=" col-md-4" >
           <label class="form-label"><strong>Tipologia cassa</strong></label>
          <div class="form-controls" >
                <?= $this->Form->input('welfare_box_code',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $welfareBoxes ]); ?>            
          </div>
          </div>
        <div class="col-md-4" >
          <label class="form-label"><strong>Percentuale</strong></label>
         <div class="form-controls">
              <?= $this->Form->input('welfare_box_percentage',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control' ]); ?>            
          </div>
        </div>
        <div class="col-md-4" >
          <label class="form-label"><strong>Iva Applicata</strong></label>
         <div class="form-controls">
              <?= $this->Form->input('welfare_box_vat_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $vats ]); ?>            
          </div>
        </div>
        <div class="col-md-4" >
          <label class="form-label"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
         <div class="form-controls">
              <?= $this->Form->input('welfare_box_withholding_appliance',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => ['0'=>'No','1'=>'Sì']]); ?>            
          </div>
        </div>

        </div>
      <?php
      // Costi fissi in % vengono visualizzati solo se abilitati nei configurations 
      if(FIXED_COST_PERCENTAGE) { ?>
      <h4 style="font-size:14px;">Costi fissi in %</h4>
        <div class="form-group col-md-12">
         
          <div class="col-md-4" >
            <label><strong>Funzione costi fissi in percentuale</strong></label>
            <div class="form-controls" >
              <?= $this->Form->input('fixed_cost_percentage_enabled',['div' => false, 'label' => false, 'class'=>'form-control','type'=>'select','options'=>['0'=>'Disabilitata','1'=>'Abilitata']]); ?>
            </div>
          </div>
         
          <div class=" col-md-4" >
            <label ><strong>Percentuale costi fissi (%)</strong></label>
            <div class="form-controls" >
              <?= $this->Form->input('fixed_cost_percentage_value',['div' => false, 'label' => false, 'class'=>'form-control','min'=>0,'max'=>100,'type'=>'number']); ?>
            </div>
          </div>
         
          <div class="col-md-4" >
            <label><strong>Descrizione predefinita costi fissi</strong></label>
            <div class="form-controls">
            <?= $this->Form->input('fixed_cost_percentage_description',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
          </div>
        </div>
      </div>
      <?php } ?>
      <script>
      
        if($("#SettingFixedCostPercentageEnabled").val() == 0)
        {
            //$("#SettingFixedCostPerncentageValue").val('');
            $("#SettingFixedCostPerncentageValue").attr('disabled',true);
            //$("#SettingFixedCostPerncentageDescription").val('');
            $("#SettingFixedCostPerncentageDescription").attr('disabled',true);
        }
      
        $("#SettingFixedCostPercentageEnabled").click(
          function()
          {
            if($(this).val() == 0)
            {
              $("#SettingFixedCostPerncentageValue").val('');
              $("#SettingFixedCostPerncentageValue").prop('disabled',true);
              $("#SettingFixedCostPerncentageDescription").val('');
              $("#SettingFixedCostPerncentageDescription").prop('disabled',true);
            }
            else
            {
              $("#SettingFixedCostPerncentageValue").prop('disabled',false);
              $("#SettingFixedCostPerncentageDescription").prop('disabled',false);
            }
          }
        )
      </script>
      </div>
    </div>
    
    
     <div id="collapseOnePointFive" class="panel-collapse collapse">
      <div class="panel-body">

        
      </div>
    </div>
    
  </div>
 
 
     <!-- Fattruazione elettronica  --> 
 
		
  		<div class="panel panel-default">
  		  <!--div class="panel-heading">
			<span class="panel-title ">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" style="color:#ffffff !important;">Fatturazione elettronica</a-->
				
				   
       <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Fatturazione elettronica</a>
  
				
			</span>
		</div>
		<div id="collapseFive" class="panel-collapse collapse">
			<div class="panel-body  ">
				<div class=" -badge tools">
			</div>
				<div class="form-group col-md-12">
					<label ><strong>Codice paese trasmittente</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('transmitter_nationality_code', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'select', 'options'=>$shortCodeList]); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Identificazione fiscale trasmittente</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('transmitter_fiscal_identification', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Nostro codice di interscambio (importazione fatture)</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('recipient_code', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        		<div class="form-group col-md-12">
					<label ><strong>Codice paese cedente/prestatore</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('provider_nationality_code', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'select', 'options'=>$shortCodeList]); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Identificazione fiscale cedente/prestatore</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('provider_fiscal_identification', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Regime fiscale</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('fiscal_regime', ['div' => false, 'label' => false, 'class' => 'form-control remove-space', 'options'=>$einvoiceFiscalRegimes,'empty'=>true]); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Sigla provincia del registro imprese presso il quale l'azienda è registrata</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('rea_province', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Iscritto nel registro imprese</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('rea_registered', ['div' => false, 'label' => false, 'class' => 'form-control remove-space', 'options'=>[ '1'=>'SI', '0'=> 'NO']]); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>NumeroRea</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('rea_number', ['div' => false, 'label' => false, 'class' => 'form-control remove-space',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Capitale sociale</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('share_capital', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Socio Unico</strong></label>
					<div class="form-controls">
					<?php $arrayParterUnique = [ 'SU' => 'SU - La società è a socio unico' , 'SM' => 'SM - La società NON è a socio unico'] ; ?>
					<?= $this->Form->input('partner_unique', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'select','options'=>$arrayParterUnique,'empty'=>true]); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Stato liquidazione</strong></label>
					<div class="form-controls">
					  <?php $arrayClosureState = [ 'LS' => 'LS - La società è in stato di liquidazione' , 'LN' => 'LN - La società NON è in stato di liquidazione']; ?>
					<?= $this->Form->input('closure_state', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'select','options'=>$arrayClosureState,'empty'=>true]); ?>
					 </div>
        </div>
      </div>
      </div>  
	
</div>


<!-- Fine fatturazione elettronica -->

<!-- RIBA -->  
  	
  	<div class="panel panel-default">
       <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Presentazione riba</a>
			</span>
		</div>
		<div id="collapseFour" class="panel-collapse collapse">
			<div class="panel-body  ">
				<div class=" -badge tools">
			</div>
				<div class="form-group col-md-12">
					<label class="form-label"><strong>Codice Sia</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('siacode', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Descrizione abbreviata società</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('siasocname', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
      </div>
    </div>
    </div>
    
<!-- Fine Riba -->

    <!-- MODULO CANTIERI-->
    <?php if(MODULO_CANTIERI){ ?>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseConstructionsite" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Modulo Cantieri</a>
			</span>
            </div>
            <div id="collapseConstructionsite" class="panel-collapse collapse">
                <div class="panel-body  ">
                    <div class=" -badge tools"></div>
                    <div class="col-md-2" style="float:left;">
                        <label class="form-label form-margin-top"><strong>Inizio orario notturno</strong></label>
                        <div class="form-controls">
                            <input  type="time"  class="segnalazioni-input form-control" name="data[Setting][start_nighttime]"   value="<?= $this->request->data['Setting']['start_nighttime'];  ?>" />
                        </div>
                    </div>
                    <div class="col-md-2" style="float:left;margin-left:10px;">
                        <label class="form-label form-margin-top"><strong>Fine orario notturno</strong></label>
                        <div class="form-controls">
                            <input  type="time"  class=" segnalazioni-input form-control" name="data[Setting][end_nighttime]" value="<?= $this->request->data['Setting']['end_nighttime'];  ?>"   />
                        </div>
                    </div>

                </div>
           </div>
        </div>
    <?php } ?>

    <!-- FINE MODULO CANTIERI  -->

    <!-- IXFE -->
  	<?php if(MODULE_IXFE)
  	{ ?>
  	<div class="panel panel-default">
       <div class="panel-heading" style="background-color:#ea5d0b;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseIXFE" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Modulo IXFE</a>
			</span>
		</div>
		<div id="collapseIXFE" class="panel-collapse collapse">
			<div class="panel-body  ">
				<div class=" -badge tools">
			</div>
				<div class="form-group col-md-12">
					<label class="form-label"><strong>Utente</strong></label>
					<div class="form-controls">
					<?= $this->Form->input('userIXFE', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
					 </div>
        </div>
        <div class="form-group col-md-12">
					<label ><strong>Password</strong></label>
					<div class="form-controls">
					  	<?php $setting['Setting']['passwordIXFE'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
					  <?= $this->Form->input('passwordIXFE', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value'=>'','placeholder'=>"$placeholder"]); ?>
					</div>
        </div>
      </div>
    </div>
    </div>
    <?php } ?>
    
<!-- IXFE  -->    

<!-- START PAYPAL -->  
  	<?php if(PAYPAL){
  	?>
  	<div class="panel panel-default">
       <div class="panel-heading" style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:100%;padding:0px;padding-left:5px;">
        <span class="panel-title ">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapsePaypal" style="color:#ffffff !important;font-weight:normal;padding: 0;margin-left:2px;text-transform:capitalize;font-family: 'Barlow Semi Condensed' !important;" >Modulo Paypal</a>
			</span>
		</div>
		<div id="collapsePaypal" class="panel-collapse collapse">
			<div class="panel-body  ">
				<div class=" -badge tools"></div>
				  <div class="form-group col-md-12">
				    <div class="col-md-4" style="float:left">
    					<label class="form-label"><strong>Account Paypal</strong></label>
	    				<div class="form-controls">
		      			<?= $this->Form->input('paypal_mail', ['div' => false, 'label' => false, 'class' => 'form-control remove-space']); ?>
			  	    </div>
            </div>
				    <div class="col-md-4" style="float:left">
    					<label class="form-label"><strong>APP Key</strong></label>
	    				<div class="form-controls">
	    				  <?php $setting['Setting']['paypal_account'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
		      			<?= $this->Form->input('paypal_account', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value' => '', 'placeholder'=>"$placeholder"]); ?>
			  	    </div>
            </div>
            <div class="col-md-4" style="float:left">
	  				  <label ><strong>APP Signature</strong></label>
		  			  <div class="form-controls">
		  			    <?php $setting['Setting']['paypal_password'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
			  		    <?= $this->Form->input('paypal_password', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value' => '','placeholder'=>$placeholder]); ?>
				  	  </div>
            </div>
          </div>
      </div>
      <div class="panel-body  ">
				<div class=" -badge tools"></div>
				  <div class="form-group col-md-12">
				    <div class="col-md-4" style="float:left">
    					<label class="form-label"><strong>Paypal user Id</strong></label>
	    				<div class="form-controls">
	    				  <?php $setting['Setting']['paypal_user_id'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
		      			<?= $this->Form->input('paypal_user_id', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value' => '','placeholder'=>$placeholder]); ?>
			  	    </div>
            </div>
            <div class="col-md-4" style="float:left">
	  				  <label ><strong>Paypal user password</strong></label>
		  			  <div class="form-controls">
		  			    <?php $setting['Setting']['paypal_user_password'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
			  		    <?= $this->Form->input('paypal_user_password', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value' => '','placeholder'=>$placeholder]); ?>
				  	  </div>
            </div>
            <div class="col-md-4" style="float:left">
	  				  <label ><strong>Paypal user signature</strong></label>
		  			  <div class="form-controls">
		  			    <?php $setting['Setting']['paypal_user_signature'] != '' ? $placeholder = 'Lasciare vuoto per mantenere invariato' : $placeholder = ''; ?>
			  		    <?= $this->Form->input('paypal_user_signature', ['div' => false, 'label' => false, 'class' => 'form-control remove-space','type'=>'password','value' => '','placeholder'=>$placeholder]); ?>
				  	  </div>
            </div>
          </div>
          
      </div>
    </div>
    </div>
    <?php } ?>
    
<!-- END PAYPAL  -->    



<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?php    echo $this->Form->end(); ?>
<script>
        $('.color-picker-input').minicolors({theme: 'bootstrap'});
</script>






<!-- Rimozione logo -->
<script>
  $(".jsRemove").click(
    function()
   {
    var result = confirm("Il logo salvato verrà eliminato definitamente, continuare ?");
    if (result)
    {
        $.ajax
		    ({
		        method: "POST",
			      url: "<?= $this->Html->url(["controller" => "settings","action" => "removeCompanySettingLogo"]) ?>",
            data:
				    {
				      
 				    },
				    success: function(data)
				    {
				        $(".attualLogo").remove();
				    }})
    }
    else
    {
      /* nothing */
    }
  });
  
   $(".jsRemove2").click(
    function()
   {
    var result = confirm("L\'immagine di pedice verrà rimossa, continuare ?");
    if (result)
    {
        $.ajax
		    ({
		        method: "POST",
			      url: "<?= $this->Html->url(["controller" => "settings","action" => "removeCompanyBillSubscript"]) ?>",
            data:
				    {
				      
 				    },
				    success: function(data)
				    {
				        $(".attualLogo").remove();
				    }})
    }
    else
    {
      /* nothing */
    }
  });
  
  $("#SettingBillPrintSocietyDataVisible").change(
    function()
    {
      // Se diventa visualizza
      if($(this).val() == 1)
      {
        // Disabilito e setto valore allineamento a sinistra
        $("#SettingBillPrintLogoAlign").attr('disabled','false');
        $("#SettingBillPrintLogoAlign").val(0);
      }
      else
      {
        $("#SettingBillPrintLogoAlign").removeAttr('disabled','true');
      }
    });
     
    $("#SettingEditForm").on('submit.default',function(ev) 
    {
    });
            
    $("#SettingEditForm").on('submit.validation',function(ev) 
    {              
        ev.preventDefault(); // to stop the form from submitting
      
        $(document).ready
        (
          function()
          {
              var codice = $("#SettingWelfareBoxCode").val();
              var percentuale = $("#SettingWelfareBoxPercentage").val();
              var iva = $("#SettingWelfareBoxVatId").val();
              var ritenuta =$("#SettingWelfareBoxWithholdingAppliance").val();
                
              if((codice == '' && iva == ''  && percentuale == '' && ritenuta == '') || (codice != '' && iva != ''  && percentuale != '' && ritenuta != ''))
              {
                  $("#SettingEditForm").trigger('submit.default');
              }
              else
              {
                  $.alert({
                    icon: 'fa fa-warning',
                  	title: 'Creazione fattura',
                    content: 'Attenzione sono stati compilati solo parzialmente i campi della cassa previdenziale. Se si vuole compilare la cassa previdenziale, tutti i campi devono essere compilati. In caso contrario devono essere tutti lasciati vuoti.',
                    type: 'orange',
                  });
                  
              }
            }
          )
      })         
</script>



       
  
   


