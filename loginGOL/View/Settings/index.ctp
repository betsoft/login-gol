<div class="portlet-title" style="font-family: 'Barlow Semi Condensed' !important;font-size: 9pt;">
	<?php //	$this->element('Form/formelements/indextitle',['indextitle'=>'Impostazioni','indexelements' => ['edit'=>'Modifica dati', $settings['Settings']['id']],['title'=>__('Edit'),'escape'=>false]]); ?>
	<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Impostazioni','indexelements' => ['editsettings'=>'Modifica dati'],'title'=>'Modifica dati azienda']); ?>
	<?php
	if(empty($settings))
	{
	?>
		<!--div class=" -badge tools">
			<?php //echo $this->Html->link('<i class="uk-icon-plus-circle icon"></i>', ['action' => 'add'], ['title'=>__('Settaggi azienda'),'escape' => false]); ?>
		</div-->
		<?php //	$this->element('Form/formelements/indextitle',['indextitle'=>'Impostazioni','indexelements' => ['add'=>'Settaggi azienda']]); ?>
	<?php
	}

		if($settings == null)
		{
			?>
				<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
					<tr><th></th></tr>
				</thead>
				<tbody>
					<td>Non è stata definita nessuna impostazione, creare le impostazioni per configuare i valori </td>
				</tbody>
				</table>
			<?php
		}
		else
		{
	?>

		<div class="clients index">
			<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Aspetto stampe</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
			</thead>
			<tbody>

			<tr><td>Logo documenti</td>
				<td>
					<?php
					if(isset($settings['Setting']['header']) && $settings['Setting']['header'] != "")
					{
						echo $this->Html->image('societa/'.$settings['Setting']['header'].'', ['style'=>'height:150px; width:auto;max-width:600px;']);
					}
					else
					{
						echo "Nessun logo inserito";
					}
					?></td>
				</tr>

			<tr><td>Allineamento intestazione (in assenza di logo) </td>
			<td>
				<?php
					switch($settings['Setting']['logoalign'])
					{
						case 0: echo 'A Sinistra'; break;
						case 1: echo 'Centrato'; break;
						case 2: echo 'A Destra'; break;
					}
				?>
			</td></tr>
			<tr><td>Visualizzazione dati azienda </td>
			<td>
			<?php
				switch($settings['Setting']['bill_print_remove_quantity'])
				{
					case 1: echo 'Sì'; break;
					case 0: echo 'No'; break;
				}
			?>
			</td></tr>
			<tr><td>Allineamento logo (in assenza di intestazione) </td>
			<td>
			<?php
				switch($settings['Setting']['bill_print_society_data_visible'])
				{
					case 0: echo 'A Sinistra'; break;
					case 1: echo 'Centrato'; break;
					case 2: echo 'A Destra'; break;
				}
			?>
			</td></tr>
			<tr><td>Nascondi colonna quantità dalla stampa fattura</td>
			<td>
			<?php
				switch($settings['Setting']['bill_print_remove_quantity'])
				{
					case 1: echo 'Sì'; break;
					case 0: echo 'No'; break;
				}
			?>
			</td></tr>
			<tr><td>Nascondi colonna sconto dalla stampa fattura</td>
			<td>
			<?php
				switch($settings['Setting']['bill_print_remove_discount'])
				{
					case 1: echo 'Sì'; break;
					case 0: echo 'No'; break;
				}
			?>
			</td></tr>
			<tr><td>Mostra deposito di prelievo nelle bolle</td>
			<td>
			<?php
				switch($settings['Setting']['ddtdepositenabled'])
				{
					case 1: echo 'Sì'; break;
					case 0: echo 'No'; break;
				}
			?>
			</td></tr>
			<tr>
				<td>Note a fine corpo fattura</td>
				<td>
					<?php
						if(isset($settings['Setting']['bill_subscript_note']))
						{
							echo $settings['Setting']['bill_subscript_note'];
						}
					?>
				</td>
			</tr>
			<tr>
				<td>Immagine a pedice</td>
				<td>
					<?php
					if(isset($settings['Setting']['bill_subscript']) && $settings['Setting']['bill_subscript'] != "")
					{
						echo $this->Html->image('societa/'.$settings['Setting']['bill_subscript'].'', ['style'=>'height:100px; width:auto;max-width:800px;']);
					}
					else
					{
						echo "Nessun immagine di pedice inserita";
					}
					?>
				</td>
			</tr>
			<tr><td>Note a fine intestazione:</td><td><?php echo h($settings['Setting']['note']); ?></td></tr>
            <tr><td>Preventivi - Intestazione blocco note a fine preventivo</td><td><?= isset($settings['Setting']['eliminationdescription']) ? $settings['Setting']['eliminationdescription']: ''; ?></td></tr>
            <tr><td>Preventivi - Note a fine preventivo:</td><td><?= isset($settings['Setting']['elimination']) ? nl2br($settings['Setting']['elimination']): ''; ?></td></tr>
            <tr><td>Preventivi - Validità preventivo:</td><td><?= isset($settings['Setting']['validity']) ? $settings['Setting']['validity']: ''; ?></td></tr>
            <tr><td>Preventivi - Visualizzazione colonna iva</td><td>
            <?php
            switch($settings['Setting']['quoteVatVisible'])
            {
                case 1: echo 'Sì'; break;
                case 0: echo 'No'; break;
            }
            ?>
            </td></tr>
			</table>



			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Dati anagrafici</th>
					<th style="width:80%;background-color:#589ab8;"></th>
					<!--th width="25%" ></th><?php // 'Campi' ?></th>
					<th  width="75%"><?php // 'Valori ' ?><?php // echo $this->Html->link('<i class="uk-icon-pencil icon"', ['action' => 'edit', $settings['Settings']['id']],['title'=>__('Edit'),'escape'=>false]); ?></th-->
				</tr>
			</thead>
			<tr><td>Ragione sociale:</td><td><?= h($settings['Setting']['name']); ?></td></tr>
			<tr><td>P.Iva</td><td><?= h($settings['Setting']['piva']); ?></td></tr>
			<tr><td>C.F.</td><td><?= h($settings['Setting']['cf']); ?></td></tr>
			<tr><td>Indirizzo</td><td><?= h($settings['Setting']['indirizzo']); ?></td></tr>
			<tr><td>Città</td><td><?php echo h($settings['Setting']['citta']); ?></td></tr>
			<tr><td>Provincia</td><td><?php echo h($settings['Setting']['prov']); ?></td></tr>
			<tr><td>CAP</td><td><?php echo h($settings['Setting']['cap']); ?></td></tr>
			<tr><td>Telefono</td><td><?php echo h($settings['Setting']['tel']); ?></td></tr>
			<tr><td>Fax</td><td><?php echo h($settings['Setting']['fax']); ?></td></tr>
			<tr><td>Mail</td><td><?php echo h($settings['Setting']['email']); ?></td></tr>
			<tr><td>Pec</td><td><?php echo h($settings['Setting']['pec']); ?></td></tr>
			</table>

			<!-- Email -->
			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Email</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
			</thead>
			<tr><td>Host:</td><td><?php echo h($settings['Setting']['host']); ?></td></tr>
			<tr><td>Porta:</td><td><?php echo h($settings['Setting']['port']); ?></td></tr>
			<tr><td>Username:</td><td><?php echo h($settings['Setting']['username']); ?></td></tr>
			</tr>
			</tbody>
			</table>
			<!-- Fine email -->

			<!-- MAGAZZINO -->
			<?php if(ADVANCED_STORAGE_ENABLED) { ?>
			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Magazzino</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
				</thead>
				<tr><td>Metodo valutazione rimanenze di magazzino</td><td>
					<?php
					switch($settings['Setting']['storage_evaluation_method'])
					{
						case 'CMP':
						   echo 'Costo medio ponderato';
						break;
						case 'LIFO':
						   echo 'LIFO';
						break;
						case 'FIFO':
						   echo 'FIFO';
						break;
					}
					?>
					</td></tr>
			</table>
			<?php } ?>

			<!-- Fatturazione -->
			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Fatturazione</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
			</thead>
				<tr>
					<td>Tipologia ritenuta</td>
					<td>
					<?php
					//if(isset($settings['Setting']['type_of_person']))
                    if(isset($settings['Einvoicetypeofperson']))
					{
                        echo $settings['Einvoicetypeofperson']['description'];
						/*switch($settings['Setting']['type_of_person'])
						{
							case 'RT02':
								echo 'Rit. Persone giuridiche';
							break;
							case 'RT01':
								echo 'Rit Persone fisiche';
							break;
							default:
								echo '';
							break;
						}*/
					}
					?>
					</td>
				</tr> <!-- Giuridica 0 Fisica 1 -->
				<tr><td>Ritenuta d'acconto</td><td><?= $settings['Setting']['withholding_tax'] != 0 ? h($settings['Setting']['withholding_tax']) : ''; ?></td></tr>
				<tr><td>Causale del pagamento</td><td><?= $invoiceWithholdingTax; ?></td></tr>
				<?php
					if(FIXED_COST_PERCENTAGE)
					{
				?>
						<tr><td>Costi fissi in %</td><td><?= $settings['Setting']['fixed_cost_percentage_enabled'] == 0 ? 'DISATTIVATO' : 'ATTIVATO'; ?></td></tr>
				<?php
					}
				?>
				<tr><td>Cassa di previdenza</td><td><?= $welfareBox; ?></td></tr>
				<tr><td>Percentuale</td><td><?= $settings['Setting']['welfare_box_percentage'] != null ? h($settings['Setting']['welfare_box_percentage'] . ' %') : ''; ?></td></tr>
				<tr><td>Iva Applicata</td><td><?= (!isset($welfareBoxVatPercentage) || $welfareBoxVatPercentage == '') ? '': $welfareBoxVatPercentage. ' %' ; ?></td></tr>
			 	<tr><td>Applicata ritenuta acconto</td><td><?= $settings['Setting']['welfare_box_withholding_appliance'] == 1 ? 'Sì' : 'No'; ?></td></tr>
			</table>

			<?php
		// Se abilitata fatturazione elettronica
		if(RIBA_FLOW) { ?>
		<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Configurazione per riba</th>
					<th style="width:80%;background-color:#589ab8;"></th>

				</tr>
			</thead>
			<tr><td>Codice Sia:</td><td><?= h($settings['Setting']['siacode']); ?></td></tr>
			<tr><td>Descrizione abbreviata società</td><td><?= h($settings['Setting']['siasocname']); ?></td></tr>
		</tr>
		</tbody>
		</table>
		<?php } ?>

			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Configurazione fatturazione elettronica</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
			</thead>
			<tr><td>Codice paese trasmittente</td><td><?php echo h($settings['Setting']['transmitter_nationality_code']); ?></td></tr>
			<tr><td>Identificazione fiscale trasmittente</td><td><?php echo h($settings['Setting']['transmitter_fiscal_identification']); ?></td></tr>
			<tr><td>Nostro codice di interscambio</td><td><?php echo h($settings['Setting']['recipient_code']); ?></td></tr>
			<tr><td>Codice paese cedente/prestatore</td><td><?php echo h($settings['Setting']['provider_nationality_code']); ?></td></tr>
			<tr><td>Identificazione fiscale cedente/prestatore</td><td><?php echo h($settings['Setting']['provider_fiscal_identification']); ?></td></tr>
			<tr><td>Regime fiscale</td><td><?php echo h($settings['Setting']['fiscal_regime']); ?></td></tr>
			<tr><td>Sigla provincia ufficio registre imprese</td><td><?php echo h($settings['Setting']['rea_province']); ?></td></tr>
			<tr><td>Numero REA</td><td><?php echo h($settings['Setting']['rea_number']); ?></td></tr>
			<tr><td>Capitale sociale</td><td><?php echo h($settings['Setting']['share_capital']); ?></td></tr>
			<tr><td>Socio unico</td><td><?php echo h($settings['Setting']['partner_unique']); ?></td></tr>
			<tr><td>Stato liquidazione</td><td><?php echo h($settings['Setting']['closure_state']); ?></td></tr>

		</tr>
		</tbody>
		</table>


            <?php if(MODULO_CANTIERI) { ?>
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class ="flip-content">
                    <tr>
                        <th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Configurazione modulo CANTIERI</th>
                        <th style="width:80%;background-color:#589ab8;"></th>
                    </tr>
                    </thead>
                    <tr><td>Inizio orario notturno:</td><td><?=  $settings['Setting']['start_nighttime'] ?></td></tr>
                    <tr><td>Fine orario notturno:</td><td><?=  $settings['Setting']['end_nighttime'] ?></td></tr>
                    </tbody>
                </table>
            <?php } ?>

		<?php if(MODULE_IXFE) { ?>
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class ="flip-content">
                <tr>
                    <th style="background-color:#ea5d0b;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Configurazione modulo IXFE</th>
                    <th style="width:100%;background-color:#ea5d0b;"><td style="background-color:#ea5d0b;width: 100%; background-color:#ea5d0b;color: white; font-size: 100%">STATO</td></th>
                </tr>
                </thead>
                <tr><td>Utenza IXFE:</td><td><?= h($settings['Setting']['userIXFE']); ?></td></tr>
                <tr><td>Password IXFE:</td><td><?=  $settings['Setting']['passwordIXFE'] != '' ? 'Password configurata' : ' Non configurata' ; ?></td></tr>


            </table>
		<?php }  ?>

			<?php if(PAYPAL) { ?>
			<table class="table table-bordered table-striped table-condensed flip-content">
				<thead class ="flip-content">
				<tr>
					<th style="background-color:#589ab8;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Configurazione modulo PAYPAL</th>
					<th style="width:80%;background-color:#589ab8;"></th>
				</tr>
			</thead>
			<tr><td>Account paypal:</td><td><?=  $settings['Setting']['paypal_mail'] ?></td></tr>
			<tr><td>Chiave App Paypal:</td><td><?=  $settings['Setting']['paypal_account'] != '' ? 'Configurata' : ' Non configurata' ; ?></td></tr>
			<tr><td>Password Paypal:</td><td><?=  $settings['Setting']['paypal_password'] != '' ? 'Configurata' : ' Non configurata' ; ?></td></tr>
			<tr><td>Paypal user Id:</td><td><?=  $settings['Setting']['paypal_user_id'] != '' ? 'Configurato' : ' Non configurato' ; ?></td></tr>
			<tr><td>Paypal user password:</td><td><?=  $settings['Setting']['paypal_password'] != '' ? 'Configurata' : ' Non configurata' ; ?></td></tr>
			<tr><td>Paypal user signature:</td><td><?=  $settings['Setting']['paypal_password'] != '' ? 'Configurato' : ' Non configurato' ; ?></td></tr>
		</tbody>
		</table>
		<?php } ?>





		<?php
			}
		?>
            <?php
            if(isset($idPayPal)){
            ?>
                <script>
                    function cancella()
                    {
                        var xhr = new XMLHttpRequest();
                        xhr.withCredentials = true;

                        xhr.open("POST", "https://api-m.sandbox.paypal.com/v1/billing/subscriptions/<?= $idPayPal ?>/cancel");
                        xhr.setRequestHeader("Authorization", "Basic QVNQVkFORlRfemF1bU1fOURRMTAyY1c0RkVxVm1hckpTZ3MzYVl4YzlVY1ZRUkRpZlJIR25qRGhtT0p5dTlGWkw5MUhuaVh6TFYyNUNobkQ6RVBGVGhhR01rbTczOGV3Zm9tQkpQM3lCNEVHTnJJMVg5Wk5Pd1h0MTdLMV9wVFdSQXNGSXBNVVRHTVRPY3A2MUJlNGFsLTJFNkNPMFZsdU4=");
                        xhr.setRequestHeader("Content-Type", "application/json");

                        xhr.send();

                        window.location.href = "/gol/login-gol/users/pauseUser";
                    }
                </script>
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class ="flip-content">

                    <tr>
                        <th style="background-color:#ea5d0b;color:#ffffff;font-size:9pt;font-weight:normal;width:20%;" >Disdici l'abbonamento al gestionale</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr><td><button style="border: 1px solid #ffffff;color: #fff !important;width: 15%;font-size: 15px;margin-left: 5px;padding: 10px !important;color: #555555 !imporant;margin-right: 20px;font-weight: normal;font-family: 'Barlow Semi Condensed' !important;text-transform: none;background-color: #ea5d0b;" onclick="cancella()">Disdici</button></td></tr>

                    </tbody>
                </table>

                <div class="col-md-2">
            </div>
            <?php }
            ?>
	<div class="paging uk-text-center">
	</div>
</div>

