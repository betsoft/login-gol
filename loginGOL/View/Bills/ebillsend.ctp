<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>

<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= 'Invio fatture a IXFE'; ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura dal</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateFrom" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura al </strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateTo" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Cliente</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('client_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','required'=>'required','maxlength'=>false,'pattern'=>PATTERNBASICLATIN]); ?>
        </div>
    </div>
    <div class="row col-md-4">
        <label class="form-label form-margin-top"></label>
        <div class="form-controls">
            <div class=" col-md-1 blue-button ebill sendbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Invia fatture a IXFE filtrate per data</div>
        </div>
    </div>
</div>

<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function()
        {
            var clienti = setClients();

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti,"#BillClientId");
        }
    );
</script>

<script>

    $(".sendbillixfe").click(
        function()
        {
            addWaitPointer();
            var datastart = $("#DateFrom").val();
            var dataend = $("#DateTo").val();
            var clientId = $("#BillClientId").val();
            var diffDays = getDateDiff(dataend,datastart);

            if(diffDays < 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: 'Invio fatture IXFE',
                    content: 'Attenzione la data di fine non può precedere la data di inizio.',
                    type: 'orange',
                });

                removeWaitPointer();
            }
            else
            {
                if(diffDays > 31)
                {
                    $.alert({
                        icon: 'fa fa-warning',
                        title: 'Invio fatture IXFE',
                        content: 'L\'intervallo massimo selezionabile è pari a 31 giorni.',
                        type: 'orange',
                    });

                    removeWaitPointer();
                }
                else
                {
                    $.ajax({
                        method: "POST",
                        url: "<?= $this->Html->url(["controller" => "bills","action" => "sendmultibills"]) ?>",
                        async : true,
                        data:
                        {
                            dateFrom: $("#DateFrom").val(),
                            dateTo: $("#DateTo").val(),
                            clientId: $("#BillClientId").val(),
                        },
                        success: function(data)
                        {
                            $.alert({
                                icon: 'fa fa-ok',
                                title: 'Invio fatture IXFE',
                                content: data,
                                type: 'blue',
                            });
                            removeWaitPointer();
                        },
                        error: function(data)
                        {
                            removeWaitPointer();
                        }
                    })
                }
            }
        }
    );
</script>
