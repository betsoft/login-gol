<div>Riepilogo registrazioni acquisti dal <?= $dateFrom ?> al <?= $dateTo ?></div>

<table style="font-size:10px;margin-top:30px;">
    <thead>
        <tr>
            <th width="10%" style="text-align: center">N°</th>
            <th width="40%" style="text-align: left;">Documento</th>
            <th width="40%" style="text-align: center;">Conto</th>
            <th width="10%" style="text-align: center;">Imponibile</th>
        </tr>
    </thead>
    <tbody>
        <tr><td colspan="3"></td></tr>
        <?php $arrayConti = []; ?>
        <?php foreach($bills as $bill): ?>
            <?php if (count($bill['Good']) > 0): ?>
                <?php
                    $coefficiente = 1;

                    switch($bill['Bill']['typeofdocument'])
                    {
                        case "TD01":
                            $type = "Fattura";
                        break;
                        case "TD02":
                            $type = "Acconto/Anticipo su fattura";
                        break;
                        case "TD03":
                            $type = "Acconto/Anticipo su parcella";
                        break;
                        case "TD04":
                            $type  = "Nota di Credito";
                            $coefficiente = -1;
                        break;
                        case "TD05":
                            $type  = "Nota di Debito";
                        break;
                        case "TD06":
                            $type =  "Parcella";
                        break;
                        case "TD07":
                            $type = "Fattura semplificata";
                        break;
                        case "TD08":
                            $type =  "Nota di Credito semplificata";
                            $coefficiente = -1;
                        break;
                        default:
                            $type = "Fattura";
                        break;
                    }

                    $clientOrSupplierData = 'Fornitore: ' . $bill['Bill']['supplier_name'];
                ?>
                <tr style="width:100%;">
                <?php
                    $i = 0;

                    for($k = 0; $k < count($bill['Good']); $k++)
                    {
                        for($j = 0; $j < count($bill['Good']) - $i - 1; $j++)
                        {
                            if ($bill['Good'][$j]['category_id'] < $bill['Good'][$j+1]['category_id'])
                            {
                                $t = $bill['Good'][$j];
                                $bill['Good'][$j]= $bill['Good'][$j+1];
                                $bill['Good'][$j+1] = $t;
                            }
                        }
                    }

                    $categoria = [];

                    foreach ($bill['Good'] as $good)
                    {
                        $good['category_id'] == null ? $cat = "(categoria non inserita)" : $cat = $good['Category']['description'];
                        $bill['Bill']['imported_bill'] == 1 ? $price = $good['imported_total_row'] : $price = $good['prezzo'] * $good['quantita'] * (1 - $good['discount'] / 100);

                        $price = $coefficiente * $price;

                        if(isset($categoria[$cat]))
                            $categoria[$cat] += $price;
                        else
                            $categoria[$cat] = $price;

                        if(isset($arrayConti[$cat]))
                            $arrayConti[$cat] += $price;
                        else
                            $arrayConti[$cat] = $price;
                    }
                ?>

                    <td style="text-align: center"><?=$bill['Bill']['id']?></td>
                <?php if(count($categoria) > 0): ?>
                    <td width="100%"><?= $type . ' numero ' . $bill['Bill']['numero_fattura'] . ' del ' . date("d-m-Y", strtotime($bill['Bill']['date'])) . '<br/>' . $clientOrSupplierData; ?></td>
                    <?php $descriptions = ''; $totals = ''; ?>
                    <?php foreach ($categoria as $descrizione => $totale): ?>
                        <?php if($totale != 0): ?>
                            <?php
                                $descriptions .= $descrizione.'<br>';
                                $totals .= number_format($totale, 2, ',', '').' €'.'<br>';
                            ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td style="text-align: right;"><?= $descriptions ?></td>
                    <td style="text-align: right;"><?= $totals ?></td>
                <?php endif; ?>
                </tr>
                <tr><td colspan="3"><hr></td></tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>

<div style="text-align: center; margin-top:40px; margin-bottom:40px;" >Riepilogo complessivo per conto</div>

<table width="100%" style="font-size:10px;">
    <thead>
        <tr>
            <th width="60%" style="text-align: left;">Conto</th>
            <th width="40%">Imponibile</th>
        </tr>
    </thead>
    <tbody>
        <?php $totale = 0; ?>
        <?php foreach($arrayConti as $key => $totaleConto): ?>
            <?php if($totaleConto != 0): ?>
                <?php $totale += $totaleConto; ?>
                <tr width="100%">
                    <td width="60%"><?= $key; ?> </td>
                    <td width="40%" style="text-align:right;"><?= number_format($totaleConto, 2, ',', '.'). ' €'?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>

<br>Totale: <?= number_format($totale,2,',','.'). ' €'?>