<?php if($bill['Client']['divisa'] == 0) { $divisa = 'EURO'; } else { $divisa = 'CHF'; } ?>
<html>
<head>
<style>
body {font-family: sans-serif;
    font-size: 10pt;
}
p {    margin: 0pt;
}
td { vertical-align: top; }
.items td {
    border-left: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
    text-align: center;
    border: 0.1mm solid #000000;
}
.items td.blanktotal {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-top: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
.items td.totals {
    text-align: right;
    border: 0.1mm solid #000000;
}

.items td.white {
   
    border: 0.0mm solid #ffffff;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>
<td width="50%" style="color:#000;"><span style="font-weight: bold; font-size: 14pt;"><?php echo $impostazioni['Setting']['header']; ?></span><br /><br /><?php echo $impostazioni['Setting']['indirizzo']; ?></td>
<td width="50%" style="text-align: right;">DDT n.<br /><span style="font-weight: bold; font-size: 12pt;"><?php echo $bill['Bill']['riferimentoalternativo']; ?></span></td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
DDT n. <?php echo $bill['Bill']['riferimentoalternativo']; ?> del <?php echo $this->Time->format('d/m/y', $bill['Bill']['date']); ?>. Pagina {PAGENO} di {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<div style="text-align: right">Data documento: <?php echo $this->Time->format('d/m/y', $bill['Bill']['date']); ?></div>
<br />
<table width="100%" style="font-family: serif;" cellpadding="10">
<tr>
<td width="45%" style="border: 0mm solid #000;">&nbsp;</td>
<td width="10%">&nbsp;</td>
<td width="45%" style="border: 0.1mm solid #888888;"><span style="font-size: 7pt; color: #555555; font-family: sans;">Destinatario:</span><br /><br /><?php echo $bill['Client']['ragionesociale']; ?><br /><?php echo $bill['Client']['indirizzo']; ?><br /><?php echo $bill['Client']['cap'] . ' ' . $bill['Client']['citta'] . ' ' . $bill['Client']['provincia']; ?><br /><?php echo 'N.IVA: ' . $bill['Client']['piva']; ?></td>
</tr>
</table>
<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="6">
<thead>
<tr>
<td width="45%">DESCRIZIONE</td>
<td width="10%">QUANTITA'</td>

</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->
<?php
	foreach($bill['Good'] as $numero => $oggetto)
	{
		$prezzo_riga = $oggetto['quantita'] * $oggetto['prezzo'];
		$classe='';
		if ($numero % 2 != 0) 
		{
			$classe = 'style="background-color: #eee;"';
		}
		?>
		<tr <?php echo $classe; ?>>
			<td><?php echo $oggetto['oggetto']; ?></td>
			<td align="center"><?php echo $oggetto['quantita']; ?></td>
			
		</tr>
	<?php
		
	}

?>
<!-- END ITEMS HERE -->
<tr>
<td class="blanktotal" colspan="2" rowspan="6"></td>

</tr>
</tbody>
</table>
<br /><br />
<div style="text-align: left; font-style: italic;">Impresa incaricata del trasporto </strong><br/><br/>_______________________________________</div>
<div style="text-align: right; font-style: italic;">Data di inizio trasporto<br/><br/> __________________________________</div>
<!--<div style="text-align: center; font-style: italic;">Note: </strong><?php echo $bill['Bill']['note']; ?></div>
<div style="text-align: center; font-style: italic;">Ns. Banca <?php echo $impostazioni['Setting']['banca']; ?></div>
<div style="text-align: center; font-style: italic;">IBAN <?php echo $impostazioni['Setting']['iban']; ?></div>-->
</body>
</html>