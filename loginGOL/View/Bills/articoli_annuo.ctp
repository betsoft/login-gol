
<div class="portlet-title">
    <div class="caption">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Scaricamento fatture elettroniche inviate'); ?></span>
    </div>
</div>

<div><br/></div>

<form action="<?= $this->Html->url(["controller" => "bills","action" => "downloadArticoli"]) ?>" method="post" id="downloadBillIxfeForm">
    <div class="row">
        <div class="form-group col-md-4">
            <div class="col-md-12">
                <label class="form-label form-margin-top"><strong>Anno</strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('Anno', ['label'=>false,'type'=>'date', 'dateFormat'=>'Y', 'minYear'=>1999, 'maxYear'=>date('Y'), 'class'=>'form-control']);?>
                </div>
            </div>
        </div>
        <div class="row col-md-6">
            <label class="form-label form-margin-top"></label>
            <div class="form-controls">
                <div class=" col-md-1 blue-button ebill downloadbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Scarica Fatturato per articoli per l'anno selezionato</div>
            </div>
        </div>
    </div>
</form>
<br/>
<script>

    // Importazione fattura manuale
    $(".importbillxml").click(
        function()
        {
            loadFileAsText();
        });

    /* SCARICAMENTO FILE PER DATA*/
    $(".downloadbillixfe").click(
        function() {
            $("#downloadBillIxfeForm").submit();
                removeWaitPointer();
        });
</script>

<?= $this->element('Js/datepickercode'); ?>
<script>
    removeWaitPointer();
</script>
