<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Importazione transazioni paypal'); ?></span>
	</div>
</div>
<div>
	<br/>
</div>

<div class="ajax-filters-container">
 
<table style="width:100%">
		<tr style="width:100%">
			<td style="width:10%;vertical-align:middle  !important;padding:5px;"><label><b>Transazioni dal</b></label></td>
			<td style="width:10%;"><input  type="datetime" id="datepicker" class="form-control ajax-filter-input datepicker paypalFrom" name="data[filters][date1]"  bind-filter-event="change"/></td>
			<td style="width:5%;vertical-align:middle !important;padding-left:1%;"><label><b>al</b></label></td>
			<td style="width:10%;"><input type="datetime" id="datepicker2" class="form-control ajax-filter-input datepicker paypalTo" name="data[filters][date2]"  bind-filter-event="change" /></td>
			<td style="width:44%"><div class="blue-button ebill paypalImport" style="width:20%;margin-left:10px;padding:3px;" ><center>Importa transazioni</center></div></td>
		</tr>
</table>
<!--div class="col-md-6 blue-button ebill paypalImport" style="float:left;" >Paypal Import</div-->
<?= $this->element('Js/datepickercode'); ?>

<script>
    	$(".paypalImport").click(
		function()
		{
			addWaitPointer()
			importPaypal();
		});
		
		function importPaypal()
		{
		    var paypalFrom = $(".paypalFrom").val();
		    var paypalTo = $(".paypalTo").val();

		    errore = 0;
		    if(paypalFrom == ''){ errore = 1}
		    if(paypalTo == ''){ errore = 2}
		    
		    if(errore == 0)
		    {
		        var diffDays = getDateDiff(paypalTo,paypalFrom);
		        if(diffDays > 30)
		        {
		            errore = 3;
		        }
		        if(diffDays < 0)
		        {
		            errore = 4;
		        }
		        
		    }

		    switch (errore)
		    {
		        case 0:
        			$.ajax
        			({
        				method: "POST",
        		    	url: "<?= $this->Html->url(["controller" => "paypals","action" => "recoverPaypalTransaction"]) ?>",
        	    		data: 
        	    		{
        	    			dateFrom: $(".paypalFrom").val(),
        	    			dateTo: $(".paypalTo").val(),
        		    	},
        		    	success: function(data)
        				{
            			    $.alert({
    				            icon: '',
	    			            title: 'Importazione transazioni paypal',
    				            content: 'importazione effettuata con successo',
    				            type: 'blue',
				            });
				            removeWaitPointer();
        		    	},
        		    	error: function(data){}
        			});	
        		break;
        		case 1:
        		    $.alert({
    				    icon: 'fa fa-warning',
	    			    title: 'Importazione transazioni paypal',
    				    content: 'Inserire una data di partenza e una di arrivo',
    				    type: 'orange',
				    });
				break;
        		case 2:
        		    $.alert({
    				    icon: 'fa fa-warning',
	    			    title: 'Importazione transazioni paypal',
    				    content: 'Inserire una data di partenza e una di arrivo',
    				    type: 'orange',
				    });
				    removeWaitPointer();
				break;
        	    case 3:
        	        $.alert({
    				    icon: 'fa fa-warning',
	    			    title: 'Importazione transazioni paypal',
    				    content: 'L\'intervallo può essere al massimo di trenta giorni',
    				    type: 'orange',
				    });
				    removeWaitPointer();
				break;
        	    case 4:
        	        $.alert({
    				    icon: 'fa fa-warning',
	    			    title: 'Importazione transazioni paypal',
    				    content: 'Date inserite non corrette',
    				    type: 'orange',
				    });
				    removeWaitPointer();
        	    break;
    		}
		}
		
</script>