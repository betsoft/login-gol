<?= $this->element('Form/Components/MegaForm/loader'); ?>
<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>

    <?= $this->Form->hidden('tipologia',  ['value' => 7,'class' => 'form-control']); ?>


    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo scontrino'); ?></span>
    <div class="col-md-12"><hr></div>
    <?php $attributes = ['legend' => false]; ?>

     <div class="form-group col-md-12">
            <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Scontrino</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <?= $this->Form->input('numero_fattura', ['label' => false,'value' => $n_fatt,'class' => 'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici']);?>
       </div>
     </div>
     <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data scontrino</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
              <input  type="datetime" id="datepicker" class=" datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>" required />
        </div>
      </div>
     <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
         <?= $this->Form->input('client_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','required'=>'required','maxlength'=>false,'id'=>'ReceiptClientId','pattern'=>PATTERNBASICLATIN]); ?>
       </div>
      </div>
     <div class="col-md-2" style="float:left;margin-left:10px;">
             <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
               <div class="form-controls">
             <?= $this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','required' => 'true', 'empty'=>true,'value'=>$defaultReceiptPaymentMethod]); ?>
       </div>
     </div>
      <?php
        // Se modulo di magazzino attivato
        if(ADVANCED_STORAGE_ENABLED)
		{
		 ?>
	    	<div class="col-md-2" style="float:left;margin-left:10px;">
            	<label class="form-label"><strong>Deposito di prelievo</strong></label>
        	    <div class="form-controls">
              	    <?= $this->Form->select('deposit_id', $deposits, ['value'=>$mainDeposit['Deposits']['id'],'class' => 'form-control','required'=>true,'empty'=>false]); ?>
                </div>
      	      </div>
		<?php
    	}
    	else
    	{
    	    echo $this->Form->hidden('deposit_id',['value'=>-1]);
    	}
		?>
      </div>

     <div class="col-md-12"><hr></div>
      <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe scontrino</div>
        <div class="col-md-12"><hr></div>
           <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
            <fieldset id="fatture"  class="col-md-12">
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden></span>
          <div class="col-md-12">

        <?php
        if(ADVANCED_STORAGE_ENABLED)
        {
            ?>
           <div class="col-md-2 jsRowField">
             <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>['1'=>'Articolo']]); ?>
           </div>
          <div class="col-md-2 jsRowField">
             <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ]); ?>
           </div>
        <?php
        }
        else
        {
            // Tolgo il jsTipo
            echo $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]);
        ?>
            <div class="col-md-2 jsRowField">
                <label class="form-label"><strong>Codice</strong></label>
                <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'maxlenght'=>11]); ?>
            </div>
        <?php
            }
        ?>


           <div class="col-md-3 jsRowFieldDescription">
             <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
               <?= $this->Form->input('Good.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control goodDescription jsDescription','pattern'=>PATTERNBASICLATIN]); ?>
               <?= $this->Form->hidden('Good.0.storage_id'); ?>
               <?= $this->Form->hidden('Good.0.movable',['class'=>'jsMovable','value'=>1]);  ?>
           </div>
         <div class="col-md-3 jsRowField">
  				<label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
	            <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
				</div>
           <div class="col-md-2 jsRowField">
             <label class="form-margin-top form-label"><strong>Importo ivato</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.prezzo', ['label' => false,'class' => 'form-control']); ?>
           </div>

            <div class="col-md-2 jsRowField">
              <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
              <?php
                echo $this->Form->input('Good.0.quantita', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity']);?>
           </div>
              <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") : ?>
                  <div class="col-md-2 jsRowField">
                      <label class="form-margin-top form-label"><strong>Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                      <?= $this->Form->input('Good.0.complimentaryQuantity', ['label' => false,'class' => 'form-control ', 'step'=>'0.0001', 'value'=>'0','disabled'=>false]); ?>
                  </div>
              <?php endif; ?>
           <div class="col-md-2 jsRowField">
             <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                     <?=  $this->Form->input('Good.0.iva_id', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $vats, 'empty'=>true,'required'=>true]); ?>
              </div>
          </div>
          <div class="col-md-12"><hr></div>
        </div>
        </fieldset>
        <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
      </div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexReceipt']); ?></center>
    <?= $this->Form->end(); ?>
    <?= $this->element('Js/datepickercode'); ?>

<script>
$(document).ready
    (
        function()
        {
            // Nascondo a priori
            $("#differentAddress").hide();

             $("#Good0Oggetto").prop('required',true);

             var clienti = setClients();

            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0,$("#ReceiptClientId").val(),"#ReceiptClientId","Good");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti,"#ReceiptClientId");

            // Abilita il clonable
            enableCloning($("#ReceiptClientId").val(),"#ReceiptClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );

</script>

<script>
        $(".jsTipo").change(function(){setCodeRequired(this);});

        function setCodeRequired(row)
        {
            console.log(row);

            var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
            if(tipo == 1)
            {
                $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
                $(row).parents(".clonableRow").find(".jsMovable").val(1);
            }
            else
            {
                $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
                $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
                $(row).parents(".clonableRow").find(".jsMovable").val(0);
            }
        }


     $("#BillAddReceiptForm").on('submit.default',function(ev)
     {
     });

     $("#BillAddReceiptForm").on('submit.validation',function(ev)
     {
        ev.preventDefault(); // to stop the form from submitting
        checkReceiptDuplicate()
     });
</script>


  <script>

    function checkReceiptDuplicate()
    {
        var errore = 0;
        var erroreQuantity = 0;
           // Gestione dei maxquantity dinamici da magazzino
            $(".jsQuantity").each(
                function()
                {
                    if($(this).val() > parseFloat($(this).attr("maxquantity")))
                    {
                       $(this).css('border-color',"red");
                       $(this).parent().find(".fastErroreMessage").remove();
                       var maxAttr = $(this).attr("maxquantity");
                       if (maxAttr < 0){ maxAttr = 0; }
                       $(this).parent().append('<span class="fastErroreMessage" style="color:red">Quantità disponibile : '+ maxAttr +'</span>');
                       erroreQuantity = 11;
                    }
                    else
                    {

                    }
                })
                console.log(erroreQuantity);

                $.ajax
                ({
    			    method: "POST",
    				url: "<?= $this->Html->url(["controller" => "bills","action" => "checkReceiptDuplicate"]) ?>",
    				data:
    				{
    				    receiptnumber : $("#BillNumeroFattura").val(),
    				    date : $("#datepicker").val(),
    				},
    				success: function(data)
    				{
                        if(data > 0)
                        {
                            errore = 4;
                        }

                        switch (errore)
                        {
                           case 0:
                                 if(erroreQuantity == 11)
                                {
                                    $.confirm({
    			                        title: 'Scontrino.',
        		                        content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
        		                        type: 'orange',
        		                        buttons: {
            		                    Ok: function ()
            		                    {
                                			action:
                                			{
                                                $("#BillAddReceiptForm").trigger('submit.default');
                                			}
                                		},
                                		annulla: function ()
                                		{
                                			action:
                                			{
                                			  // Nothing
                                			  $(".enhanced-dialog-container").show();
                                			}
                                		},
                            			}
                        			});
                                }
                                else
                                {
                                    $("#BillAddReceiptForm").trigger('submit.default');
                                }
                            break;
                            case 4:
                                 $.alert
                                ({
    				                icon: 'fa fa-warning',
	    			                title: 'Creazione scontrino',
    				                content: 'Attenzione esiste già uno scontrino con lo stesso numero in data odierna.',
    				                type: 'orange',
				                });
                                return false;
                            break;
                        }

    				},
            		error: function(data)
        			{
        			}

    			});
    }
    </script>
  <?= $this->element('Js/checkifarenewarticle'); ?>


