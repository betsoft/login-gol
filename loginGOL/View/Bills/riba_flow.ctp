<?php
    $articolo = $prezzo_articoli = $prezzo_articoli_ivato = $prezzo_pagato = $prezzo_da_pagare = $prezzo_non_pagato = '';
    $totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto  = 0;
?>

<div class="portlet-title">
    <div class="caption">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Elenco fatture per flusso riba'); ?></span>
    </div>
    <div class="tools">
        <div style='float:right;'>
            <?= $this->Form->input('selectBank', ['label' => false,'class' => 'form-control','type' =>'number','type'=>'select','options'=>$banks,'empty'=>true]); ?>
        </div>
        <div id='createRibaCsv' class = "blue-button btn-button btn-outline dropdown-toggle" title="Genera flusso riba della banca visualizzata" style='text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;min-width:150px;'>Genera flusso riba</div>
        <?= $this->Html->link(' < Elenco fatture', ['action' => 'index'], ['title'=>__('Genera Flusso Riba'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'min-width:140px;float:right;text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;']); ?>
    </div>
</div>
<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
		<tr>
			<th></th>
			<th><?= $this->Paginator->sort('numero_fattura', 'N° Fattura'); ?></th>
			<th><?= $this->Paginator->sort('date', 'Data'); ?></th>
			<th><?= $this->Paginator->sort('client_id','Cliente'); ?></th>
			<th><?= $this->Paginator->sort('banca','Banca'); ?></th>
			<th>Metodo pagamento</th>
			<th>Scadenze</th>
			<th>Abi</th>
			<th>Cab</th>
			<th>Imponibile</th>
			<th>Totale ivato</th>
			<th>Rit. Acc.</th>
			<th>A pagare</th>
		</tr>
    </thead>
    <?php foreach($bills as $bill): ?>
    <?php
        $totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = $totAPagare= 0;
        $apagare = $totImponibile = $totIvato = $totWithholdingtax = 0;

        $bill['Bill']['changevalue'] != '' && $bill['Bill']['changevalue'] != null ? $billChange = $bill['Bill']['changevalue'] : null;
        isset($bill['Bill']['collectionfees']) ? $collectionFees = $bill['Bill']['collectionfees'] : $collectionFees = 0;
        isset($bill['Bill']['seal']) ? $seal = $bill['Bill']['seal'] : $seal = 0;
    ?>
		<tr>
			<td><?= $this->Form->checkbox($bill['Bill']['id'], array('hiddenField' => false, 'style'=>'transform:scale(1.5)','class'=>'checkbill')); ?></td>
			<td><?= $bill['Bill']['numero_fattura']; ?></td>
			<td><?= $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
			<td class="table-max-width uk-text-truncate"><?= $bill['Client']['ragionesociale']; ?></td>
            <td class="table-max-width uk-text-truncate"><?= isset($bill['Client']['Bank']['description']) ? $bill['Client']['Bank']['description'] : ''; ?></td>
            <td><?= $bill['Payment']['metodo']; ?></td>
            <td>
                <?php
                    $counter = 0;
                    foreach($bill['Deadline'] as $deadline)
                    {
                        if($counter > 0)
                            echo '</br>';

                        echo date("d-m-Y",strtotime($deadline['deadline'])). ' - ' . $deadline['amount'];
                        $counter++;
                    }
                ?>
		    </td>
            <td><?= isset($bill['Client']['Bank']['abi']) && $bill['Client']['Bank']['abi'] != '' ? $bill['Client']['Bank']['abi'] : ''; ?></td>
            <td><?= isset($bill['Client']['Bank']['cab']) && $bill['Client']['Bank']['cab'] != '' ? $bill['Client']['Bank']['cab'] : ''; ?></td>
            <td>
                <?php
                    $totalWelfareBox = $Utilities->getWelfareBoxTotal($bill['Bill']['id']) - $Utilities->stornoImportoWelfareBox($bill['Bill']['id']);
                    $taxableIncome = $Utilities->getBillTaxable($bill['Bill']['id']) ;
                    $taxableIncome += $totalWelfareBox;
                    $taxableIncome = $taxableIncome / $billChange;
                    $totImponibile += $taxableIncome ;
                    echo number_format($taxableIncome  / $billChange,2,',','.');
                ?>
            </td>
            <td>
                <?php
                    $ivato = $Utilities->getBillTotal($bill['Bill']['id']);
                    $ivato += $totalWelfareBox;
                    $bill['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal =  $Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox  / 100  : $welfarVatTotal = 0;
                    $ivato  += $welfarVatTotal ;
                    $ivato = $ivato / $billChange;
                    $totIvato += $ivato;
                    echo number_format($ivato ,2,',','.');
                ?>
            </td>
			<td style="text-align:right;">
				<?php
					$withHoldingTax = $Utilities->getWitholdingTaxTotal($bill['Bill']['id']) /  $billChange;
					$totWithholdingtax += $withHoldingTax;
					$withHoldingTax = $withHoldingTax - $Utilities->stornoImportoWitholdingTax($bill['Bill']['id']) / $billChange;
					echo number_format($withHoldingTax,2,',','.');
				?>
            </td>
            <td style="text-align:right;">
                <?php
                    $daPagareFattura = $Utilities->getBillTotalToPay($bill['Bill']['id']);
                    $daPagareFattura += $welfarVatTotal + $totalWelfareBox;
                    $apagare += $daPagareFattura ;
                    $apagare = $apagare / $billChange;
                    $totAPagare += $apagare ;
                    echo number_format($daPagareFattura + ($Utilities->stornoImportoWitholdingTax($bill['Bill']['id']) / $billChange) ,2,',','.');
                ?>
            </td>
	    </tr>
    <?php endforeach; ?>
</table>

<script>
	$('#createRibaCsv').click(
		function() 
		{
			var arrayOfBill = [];

			if($("#selectBank").val() == '')
			{
				$.alert({
                	icon: 'fa fa-warning',
                    title: '',
                    content: 'Nessuna banca selezionata, selezionare la banca per cui fare il flusso riba.',
                    type: 'orange',
                });
			}
			else
			{
				$(".checkbill").each(function() {
                    if($(this).prop( "checked" ))
                    {
                        arrayOfBill.push($(this).prop("id"));
                    }
                });
				
				if(arrayOfBill.length == 0)
				{
					 $.alert({
    					icon: 'fa fa-warning',
	    			    title: 'Generazione flusso riba',
    				    content: 'Attenzione, non è stata selezionata alcuna fattura.',
    				    type: 'orange',
				     });
				}
				else
				{
					$.ajax({
						method: "POST",
						url: "<?= $this->Html->url(["controller" => "ribas","action" => "createRibaCsv"]) ?>",
						data: 
						{
							bank : $("#selectBank").val(),
							arrayOfBill: arrayOfBill, 
						},
						success: function(data)
						{
							switch(data)
							{
								case '1':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Ragione sociale non correttamente compilata nei dati anagrafici della società nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '2':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Indirizzo non correttamente compilato nei dati anagrafici della società nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '3':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Città non correttamente compilata nei dati anagrafici della società nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '4':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Provincia non correttamente compilata nei dati anagrafici della società nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '5':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Codice Sia non correttamente compilato nella nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '6':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Descrizione abbreviata società non correttamente compilata nella nella sezione impostazioni.',
    					    			type: 'orange',
				    				});
									break;
								case '7':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Abi della banca selezionata per la presentazione delle riba mancante.',
    					    			type: 'orange',
				    				});
									break;
								case '8':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Cab della banca selezionata per la presentazione delle riba mancante.',
    					    			type: 'orange',
				    				});
									break;
								case '9':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Attenzione, non può essere generato il flusso riba! Alcune fatture selezionate risultano prive di scadenze.',
    					    			type: 'orange',
				    				});
									break;
								case '10':
									$.alert({
    									icon: 'fa fa-warning',
	    				    			title: 'Generazione flusso riba',
    					    			content: 'Attenzione, non può essere generato il flusso riba! Ad uno o più clienti delle fatture  non è stata associata alcuna una banca o non sono stati correttamente compilati abi e cab della banca.',
    					    			type: 'orange',
				    				});
									break;
								default:
									$.each(arrayOfBill,function(index,value)
									{
										 if(value != -1)	
										 {
										 	$("#"+value).parent().parent().hide();
										 }
									})
									
									csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
									var a = document.createElement('A');
									a.href = csvData;
									a.download = 'flusso_riba.txt';
									document.body.appendChild(a);
									a.click();
									document.body.removeChild(a);
								break;
							}
						},
						error: function(data)
						{
							$.alert({
    							icon: 'fa fa-error',
	    				    	title: 'Generazione flusso riba',
    					    	content: 'Attenzione, errore durante la creazione del flusso riba !',
    					    	type: 'red',
				    		});
						}
					})
				}
			}
		}
    );
</script>