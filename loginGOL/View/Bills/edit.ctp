<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/advancedclientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/checkmaxstoragequantityerror'); ?>

<?php $prezzo_beni_riga = $totale_imponibile = $totale_imposta = 0 ?>
<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= $tipologia == 1 ? 'Modifica fattura di vendita' : 'Modifica fattura di vendita'; ?></span>
<div class="col-md-12"><hr></div>

<?= $this->Form->input('id'); ?>
<div class="form-group col-md-12">
    <div class="col-md-4" style="float:left;">
        <div class="form-controls">
            <?= $this->Form->hidden('Bill.client_name', ['value' => $this->request->data['Client']['ragionesociale']]); ?>
            <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'client_id',
                    "aggregator" => '',
                    "prefix" => "client_id",
                    "list" => $clients,
                    "options" => ['multiple' => false, 'required' => true],
                ]);
                ?>
            </div>
            <?php if($bills['Bill']['client_name'] != $this->request->data['Client']['ragionesociale']): ?>
                <?= '</br><span style="color:red;">La ragione sociale memorizzata sulla fattura "' . $bills['Bill']['client_name'] . '" è differente da quella mostrata per aggiornarla cliccare su salva</span>.' ?>
            <?php endif; ?>
        </div>
    </div>
    <?php if($tipologia != 8): ?>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Fattura accompagnatoria</strong></label>
            <div class="form-controls" style="margin-top:8px;">
                <?= $this->Form->hidden('Bill.accompagnatoria', ['label' => false, 'class' => 'form-control', 'type' => 'checkbox', 'style' => 'margin-top:10px;']); ?>
                <?= $bills['Bill']['accompagnatoria'] == 1 ? '<span style="color:#577400"><b>SI</b></span>' : '<span style="color:#ea5d0b"><b>NO</b></span>'; ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
        <div class="form-controls" style="margin-top:8px;">
            <?= $this->Form->input('Bill.split_payment', ['label' => false, 'type' => 'checkbox', 'style' => 'width:25px;height:25px;margin-top:-9px;margin-left:3px;']); ?>
        </div>
    </div>
    <?php if ($tipologia != 8): ?>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Fattura Elettronica</strong></label>
            <div class="form-controls" style="margin-top:8px;">
                <?= $this->Form->input('Bill.electronic_invoice', ['label' => false, 'type' => 'checkbox', 'style' => 'width:25px;height:25px;margin-top:-9px;margin-left:3px;']); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-1" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Nota di debito</strong></label>
        <div class="form-controls " style="margin-top:8px;">
            <?= $this->Form->input('Bill.debitnote', ['label' => false, 'type' => 'checkbox', 'style' => 'width:25px;height:25px;margin-top:-9px;margin-left:3px;']); ?>
        </div>
    </div>
</div>

<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('numero_fattura', ['label' => false, 'class' => 'form-control', 'required' => true, "pattern" => "[0-9a-zA-Z]+", 'title' => 'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('sectional_id', ['label' => false, 'options' => $sectionals, 'class' => 'form-control jsSectional']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" class="datepicker segnalazioni-input form-control" id="datafattura" readonly
                   name="data[Bill][date]"
                   value=<?= $this->Time->format('d-m-Y', $this->data['Bill']['date']); ?> />
        </div>
    </div>
</div>

<?= $this->element('Form/client'); ?>

<?php if ($alternativeAddress != null): ?>
    <div class="form-group col-md-12" id="differentAddress">
        <div class="col-md-12">
            <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('alternativeaddress_id', ['div' => false, 'label' => false, 'class' => 'form-control', 'empty' => true, 'options' => $alternativeAddress]); ?>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.payment_id', ['label' => false, 'class' => 'form-control', 'empty' => true, 'required' => true]); ?>
        </div>
    </div>
    <div class="col-md-5" style="float:left;">
        <label class="form-label form-margin-top"><strong>Banca di destinazione del pagamento</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component',
                [
                    "name" => 'bank_id',
                    "aggregator" => '',
                    "prefix" => "bank_id",
                    "list" => $banks,
                    "options" =>  [  'multiple' => false ,'required'=>false ],
                    "actions" =>
                        [
                            $this->element('Form/Components/MegaForm/component', [ 'megaFormIdSuffix' => "add_bank", 'linkClass' => 'fa fa-plus', 'buttonOnly' => true, 'buttonTitle'=>'Aggiungi una nuova banca' ])
                        ]
                ]);

            ?>
        </div>
    </div>
    <div class="col-md-5" style="float:left;">
        <label class="form-label form-margin-top"><strong>Banca alternativa del pagamento</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component',
                [
                    "name" => 'alternative_bank_id',
                    "aggregator" => '',
                    "prefix" => "alternative_bank_id",
                    "list" => $banks,
                    "options" =>  [  'multiple' => false ,'required'=>false ],
                    "actions" =>
                        [
                            $this->element('Form/Components/MegaForm/component', ['megaFormIdSuffix' => "add_bank", 'linkClass' => 'fa fa-plus', 'buttonOnly' => true, 'buttonTitle'=>'Aggiungi una nuova banca' ])
                        ]
                ]);

            ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.withholding_tax', ['label' => false, 'class' => 'form-control jsWithholdingTax', 'min' => 0, 'max' => 100]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Spese incasso</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.collectionfees', ['label' => false, 'class' => 'form-control', 'min' => 0]); ?>
        </div>
    </div>
    <div class="col-md-2 jsAliquota" style="float:left;margin-left:10px;" hidden>
        <label class="form-label form-margin-top"><strong>Aliquota IVA spese incasso</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.collectionfeesvatid', ['label' => false, 'class' => 'form-control', 'min' => 0, 'options' => $vats, 'empty' => true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Bollo a tuo carico</strong><i class="fa fa-question-circle jsSeal" style="color:#ea5d0b;cursor:pointer;"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.seal', ['label' => false, 'class' => 'form-control', 'min' => 0]); ?>
        </div>
    </div>
    <?php
    if($_SESSION['Auth']['User']['dbname'] == "login_GE0046"){
        ?>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Sconto (%)</strong><i class="fa fa-question-circle jsSeal" style="color:#ea5d0b;cursor:pointer;"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.discount', ['label' => false, 'class' => 'form-control', 'min' => 0]); ?>
            </div>
        </div>
        <?php
    }

    ?>
    <?php if (MULTICURRENCY && $currencyName != 'EUR'): ?>
        <div class="col-md-2 jsMulticurrency" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Cambio (EUR / <?= '<span class="jsValueCode">' . $currencyName . '</span>' ?>)</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.changevalue', ['label' => false, 'class' => 'form-control jsChangeValue', 'min' => 0]); ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if ($settings['Setting']['welfare_box_vat_id'] != '' || $settings['Setting']['welfare_box_percentage'] > 0 || $settings['Setting']['welfare_box_withholding_appliance'] != '' || $settings['Setting']['welfare_box_code']): ?>
    <div class="form-group col-md-12">
        <div class=" col-md-4">
            <label class="form-label"><strong>Tipologia cassa previdenziale</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_code', ['type' => 'select', 'empty' => true, 'div' => false, 'label' => false, 'class' => 'form-control', 'options' => $welfareBoxes]); ?>
            </div>
        </div>
        <div class="col-md-4">
            <label class="form-label"><strong>Percentuale</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_percentage', ['empty' => true, 'div' => false, 'label' => false, 'class' => 'form-control jsBillWelfareBox']); ?>
            </div>
        </div>
        <div class="col-md-4">
            <label class="form-label"><strong>Iva Applicata</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_vat_id', ['type' => 'select', 'empty' => true, 'div' => false, 'label' => false, 'class' => 'form-control', 'options' => $vats]); ?>
            </div>
        </div>
        <div class="col-md-4">
            <label class="" style="margin-top:5px"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_withholding_appliance', ['type' => 'select', 'empty' => true, 'div' => false, 'label' => false, 'class' => 'form-control', 'options' => ['0' => 'No', '1' => 'Sì']]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="form-group col-md-12">
    <div class="col-md-2">
        <label class="form-label form-margin-top "><strong>Arrotondamento</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('rounding', ['div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'select', 'empty' => true, 'options' => ['0.01' => '0.01', '-0.01' => '-0.01']]); ?>
        </div>
    </div>
    <div class="col-md-10">
        <label class="form-label"><strong>Note</strong><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['label' => false, 'class' => 'form-control', 'id' => 'lunghezza2', 'div' => false]); ?>
        </div>
    </div>
</div>

<!-- Per accompagnatoria -->
<?php if ($bills['Bill']['accompagnatoria'] == 1): ?>
    <span id="billTransport"><?= $this->element('Form/bills/billtransportedit'); ?></span>
    <script>$("#BillDepositId").attr("required", "true");</script>
<?php endif; ?>

<!-- Per fatturazione elettronica  -->
<div id="billPublic"><?= $this->element('Form/bills/billpa'); ?></div>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe fattura</div>
    <div class="col-md-12"><hr></div>
    <?php
    if (FIXED_COST_PERCENTAGE)
    {
        if ($settings['Setting']['fixed_cost_percentage_enabled'])
            echo $this->element('Form/Simplify/action_fixed_cost_percentage_button');
    }
    ?>

    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture" class="col-md-12">
        <?php
        $conto = count($this->data['Good']) - 1;
        $this->data = $bills;
        $i = -1;

        if(count($this->data['Good']) != 0)
        {
            $settatoOriginale = false;
        foreach ($this->data['Good'] as $chiave => $oggetto) {
            $i++;
            $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
            if(!$settatoOriginale && $oggetto['quantita'] != 0){
                $classe1 = "originale";
                $settatoOriginale = true;
            }else{
                $classe1 = "";
            }
            //$chiave == 0 ? $classe1 = "originale" : $classe1 = '';

            // Metto il redonly per correzione errore bolle
            if ($oggetto['transport_id'] != null) {
                $readOnly = true;
                $backgroundColorReadonly = '#fafafa';
            } else {
                $readOnly = false;
                $backgroundColorReadonly = '#ffffff';
            }

            if (($oggetto['quantita'] == null || $oggetto['quantita'] == 0) && ($oggetto['prezzo'] == null || $oggetto['prezzo'] == 0) && $oggetto['iva_id'] == null) {
                $isNote = 'display:none';
                $isDescriptionNote = ' col-md-12';
            } else {
                $isNote = 'display:block';
                $isDescriptionNote = 'col-md-3';
            }
            ?>
            <div class="principale lunghezza clonableRow contacts_row <?= $classe1 ?> <?= $classe ?>" id="'<?= $chiave ?>'">
                <?php if ($oggetto['transport_id'] != null): ?>
                    <span class="remove rimuoviRigaIcon icon fa fa-remove" style="color:#dedede;" title="Articoli provenienti da bolla, rimozione riga disabilitata"></span>
                <?php else: ?>
                    <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                <?php endif; ?>
                <?= $this->Form->input("Good.$chiave.id"); ?>
                <div class="col-md-12">
                    <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = null; ?>
                    <?php if (ADVANCED_STORAGE_ENABLED): ?>
                        <div class="col-md-2 jsRowField">
                            <label class="form-label "><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.tipo", ['required' => true, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo', '0' => 'Voce descrittiva'], 'empty' => true, 'value' => $movableValue, 'disabled' => true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.codice", ['div' => false, 'label' => false, 'readonly' => $readOnly, 'class' => 'form-control  jsCodice', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                        </div>
                    <?php else: ?>
                        <?= $this->Form->hidden("Good.$chiave.tipo", ['div' => false, 'label' => false, 'class' => 'form-control', 'value' => 0]); ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong></label>
                            <?= $this->Form->input("Good.$chiave.codice", ['div' => false, 'label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsCodice', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                        </div>
                    <?php endif; ?>
                    <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                        <label class="form-margin-top"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;"></i></label>
                        <?= $this->Form->input("Good.$chiave.oggetto", ['div' => false, 'readonly' => $readOnly, 'label' => false, 'class' => 'form-control jsDescription ', 'pattern' => PATTERNBASICLATIN, 'required' => 'required', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                        <?= $this->Form->hidden("Good.$chiave.storage_id", ['class' => 'jsStorage', 'type' => 'text']); ?>
                        <?= $this->Form->hidden("Good.$chiave.transport_id", ['type' => 'text']); ?>
                        <?= $this->Form->hidden("Good.$chiave.movable", ['class' => 'jsMovable', 'value' => $movableValue]); ?>
                    </div>
                    <div class="col-md-3 jsRowField" style="<?= $isNote ?>">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input("Good.$chiave.customdescription", ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px', 'pattern' => PATTERNBASICLATIN]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?php if (ADVANCED_STORAGE_ENABLED)
                            echo $this->Form->input("Good.$chiave.quantita", ['label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsQuantity', 'prodotto' => $oggetto['storage_id'], 'div' => false, 'maxquantity' => $getAviableQuantity->getAvailableQuantity($oggetto['storage_id'], $bills['Bill']['deposit_id']) + $oggetto['quantita'], 'step' => '0.001', 'min' => '0']);
                        else
                            echo $this->Form->input("Good.$chiave.quantita", ['label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsQuantity', 'prodotto' => $oggetto['storage_id'], 'div' => false, 'step' => 1]);
                        ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input("Good.$chiave.unita", ['div' => false, 'label' => false, 'class' => 'form-control', 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                    </div>
                    <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                        <label class="form-label form-margin-top"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Good.$chiave.prezzo", ['label' => false, 'class' => 'jsPrice form-control  ' . $oggetto['storage_id']]); ?>
                    </div>
                    <?php if (MULTICURRENCY && $currencyName != 'EUR'): ?>
                        <div class="col-md-2 jsMulticurrency" style="<?= $isNote ?>">
                            <label class="form-margin-top form-label"><strong>Prezzo(<?= '<span class="jsValueCode">' . $currencyName . '</span>' ?>)</strong></label>
                            <?= $this->Form->input("Good.$chiave.currencyprice", ['label' => false, 'class' => 'form-control jsChange', 'value' => $oggetto['currencyprice']]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top form-label"><strong>Importo</strong></label>
                        <?= $this->Form->input("Good.$chiave.importo", ['label' => false, 'class' => 'jsImporto', 'class' => 'form-control jsImporto', 'disabled' => true, 'value' => number_format($oggetto['quantita'] * $oggetto['prezzo'], 2, ',', '')]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                        <?= $this->Form->input("Good.$chiave.discount", ['label' => false, 'class' => 'form-control', 'div' => false, 'max' => 100]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>IVA</strong><i class="fa fa-asterisk"></i></label>
                        <?php isset($oggetto['Iva']['id']) ? $iva = $oggetto['Iva']['id'] : $iva = ''; ?>
                        <?= $this->Form->input("Good.$chiave.iva_id", ['label' => false, 'class' => 'form-control jsVat', 'div' => false, 'options' => $vats, 'empty' => true, 'value' => $iva]); ?>
                    </div>
                    <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0041") : ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Totale Ivato</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("ivato", ['label' => false, 'class' => 'form-control jsImporto', 'div' => false, 'value' => number_format($oggetto['quantita'] * $oggetto['prezzo'], 2, ',', '') + number_format($oggetto['quantita'] * $oggetto['prezzo'], 2, ',', '') * 22/100]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-2 jsWithholdingAppliance jsRowField" hidden>
                        <label class="form-margin-top form-label"><strong>Applica ritenute/contributi</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Good.$chiave.withholdingapplied", ['label' => false, 'class' => 'form-control jsPrice', 'options' => ['1' => 'Sì', '0' => 'No']]); ?>
                    </div>
                    <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") : ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.complimentaryQuantity", ['label' => false, 'class' => 'form-control', 'div' => false, 'step'=>'0.0001']); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row" style="padding-bottom:10px;"><hr></div>
            </div>
        <?php
        $prezzo_beni_riga = '';
        $prezzo_beni_riga = $oggetto['prezzo'] * $oggetto['quantita'] * (1 - $oggetto['discount'] / 100);
        $totale_imponibile += $prezzo_beni_riga;

        if (isset($oggetto['Iva']['percentuale']))
            $totale_imposta += ($prezzo_beni_riga / 100) * $oggetto['Iva']['percentuale'];
        ?>
            <script>loadClientCatalog(<?= $i ?>, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#BillClientId", "Good");</script>
        <?php
        }
        }
        else
        {
        ?>
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"></span>
                <div class="col-md-12">
                    <?php if(ADVANCED_STORAGE_ENABLED): ?>
                        <div class="col-md-2 jsRowField" >
                            <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField">
                            <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'maxlenght'=>11]); ?>
                        </div>
                    <?php else: ?>
                        <?= $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]); ?>
                        <div class="col-md-2 jsRowField">
                            <label class="form-label"><strong>Codice</strong></label>
                            <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ,'maxlenght'=>11]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-3 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;" ></i></label>
                        <?php
                        echo $this->Form->input('Good.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control jsDescription goodDescription','required'=>'required','pattern'=>PATTERNBASICLATIN]);
                        echo $this->Form->hidden('Good.0.storage_id',['class'=>'jsStorage']);
                        echo $this->Form->hidden('Good.0.movable',['class'=>'jsMovable']);
                        ?>
                    </div>
                    <div class="col-md-3 jsRowField">
                        <label class="form-label "><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control  ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.quantita', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity', 'step'=> '0.001', 'min' => '0']);?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input('Good.0.unita', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units,'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.prezzo', ['label' => false,'class' => 'form-control jsPrice' , 'step'=>'0.01']); ?> <!-- Fissato altrimenti me ne da 5 -->
                    </div>
                    <?php if(MULTICURRENCY): ?>
                        <div class="col-md-2 jsMulticurrency " style="display:none;">
                            <label class="form-margin-top form-label"><strong>Prezzo (<?= '<span class="jsValueCode"></span>' ?>)</strong></label>
                            <?= $this->Form->input('Good.0.currencyprice', ['label' => false,'class' => 'form-control jsChange' ]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label "><strong>Importo</strong></label>
                        <?= $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                        <?= $this->Form->input('Good.0.discount', ['label' => false,'class' => 'form-control','div'=> false,'max'=>100]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>IVA</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.iva_id', ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats,'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsWithholdingAppliance jsRowField" hidden>
                        <label class="form-margin-top form-label"><strong>Applica ritenute/contributi</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.withholdingapplied', ['label' => false,'class' => 'form-control ' , 'options'=>['1'=>'Sì','0'=>'No']]); ?> <!-- Fissato altrimenti me ne da 5 -->
                    </div>
                    <div class="row" style="padding-bottom:10px;"><hr></div>
                </div>
            </div>
            <?php
        }
        ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>
    <?php $totale_complessivo = $totale_imponibile + $totale_imposta; ?>
</div>
<center><?= $this->element('Form/Components/Actions/component', ['redirect' => $redirect]); ?></center>

<?php echo $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function ()
        {
            var clienti = setClients();

            $("#BillPaymentId").change(function () {
                $.ajax
                ({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "payments", "action" => "getPaymentCollectionFees"]) ?>",
                    data:
                        {
                            PaymentId: $("#BillPaymentId").val(),
                        },
                    success: function (data)
                    {
                        $("#BillCollectionfees").val(data);
                        showCollectionfeesVat(data);

                        $.ajax({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "payments","action" => "getPaymentBanks"]) ?>",
                            data:
                                {
                                    PaymentId : $("#BillPaymentId").val(),
                                },
                            success: function(data)
                            {
                                var arrayBanks = JSON.parse(data);
                                var bankId = arrayBanks['bank_id'];
                                var alternativeBankId = arrayBanks['alternative_bank_id'];

                                $("#bank_id_multiple_select").val(bankId);
                                $("#bank_id_multiple_select").multiselect('rebuild');

                                $("#alternative_bank_id_multiple_select").val(alternativeBankId);
                                $("#alternative_bank_id_multiple_select").multiselect('rebuild');
                            }
                        });
                    }
                });
            });

            $("#client_id_multiple_select").change(
                function()
                {
                    refreshData()

                    var clientId = $("#client_id_multiple_select").val();

                    if(clientId != '')
                    {
                        $.ajax({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "clients","action" => "getClientData"]) ?>",
                            data:
                                {
                                    clientId : clientId,
                                },
                            success: function(data)
                            {
                                data = JSON.parse(data);

                                $('#BillClientAddress').val(data['Client']['indirizzo']);
                                $('#BillClientCap').val(data['Client']['cap']);
                                $('#BillClientCity').val(data['Client']['citta']);
                                $('#BillClientProvince').val(data['Client']['provincia']);

                                $('#client_nation_multiple_select').val(data['Client']['nation_id']);
                                $("#client_nation_multiple_select").multiselect('rebuild');

                                $('#bank_id_multiple_select').val(data['Client']['bank_id']);
                                $("#bank_id_multiple_select").multiselect('rebuild');

                                $('#BillPaymentId').val(data['Client']['payment_id']);

                                if(data['Payment'] != null && data['Payment']['paymentFixedCost'] > 0)
                                {
                                    $("#BillCollectionfees").val(data['Payment']['paymentFixedCost']);
                                    $(".jsAliquota").show();
                                    $('#BillCollectionfeesvatid').attr('required',true);
                                }

                                if(data['Client']['splitpayment'] == '1' && $("#BillSplitPayment").prop("checked") == false)
                                    $("#BillSplitPayment").click();

                                if(data['Client']['splitpayment'] == '0' && $("#BillSplitPayment").prop("checked") == true)
                                    $("#BillSplitPayment").click();

                                if (data['Client']['withholding_tax'] > 0)
                                {
                                    var withholdingtax = data['Client']['withholding_tax'].toFixed(2);
                                    $("#BillWithholdingTax").val(withholdingtax);
                                }

                                if(data['Clientdestination'].length > 0)
                                {
                                    $("#differentAddress").show();
                                    $('#BillAlternativeaddressId').append($("<option></option>").attr("value", 'empty').text(""));

                                    $.each(data['Clientdestination'], function (key, element) {
                                        if (element.nation != null)
                                            var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province + '  ' + element.nation;
                                        else
                                            var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province;

                                        $('#BillAlternativeaddressId').append($("<option></option>").attr("value", element.id).text(indirizzo));
                                    })
                                }

                                $(".contacts_row.clonableRow").each(
                                    function(index)
                                    {
                                        var discountField = $(this).find($("[name*='discount']"));
                                        var discountValue = discountField.val();

                                        if(discountValue == '')
                                            discountField.val(data['Client']['discount']);

                                        var vatField = $(this).find($("[name*='iva_id']"));
                                        var vatValue = vatField.val();

                                        if(vatValue == '' || vatValue == null)
                                            vatField.val(data['Client']['vat_id']);
                                    }
                                );
                            }
                        });
                    }
                }
            );

            // Carico il listino
            loadClientCatalog(0, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#BillClientId", "Good");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#BillClientId");

            if ($("#BillSplitPayment").prop("cecked") == true && $("#BillElectronicInvoice").prop("checked") == true) {
                $("#BillEinvoicevatesigibility").val("S");
            }

            /** Se splitpayment .. */
            $("#BillSplitPayment").click(function () {
                if ($("#BillElectronicInvoice").prop("checked") == true) {
                    $("#BillEinvoicevatesigibility").val("S");
                }
            });

            $('#BillImporto').hide();

            enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#BillClientId");

            $("#BillReferredclientId").change(function () {
                if ($(this).val() != 'empty') {
                    $("#BillAlternativeaddressId").val('');
                }
            });

            $("#BillAlternativeaddressId").change(function () {
                if ($(this).val() != 'empty') {
                    $("#BillReferredclientId").val('');
                }
            });

            <?php
            if($bills['Bill']['accompagnatoria'] == 1 && $tipologia == 1 )
            {
            ?>
            $(".jsQuantity").each(function () {
                var storageId = $(this).parents(".uk-grid.uk-grid-divider").first().find(".jsStorage").attr("value");
                var quantityId = "#" + this.id;

                var currentBillStoragequantity = 0;
                $.ajax
                ({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "bills", "action" => "getCurrentBillStorageQuantity"]) ?>",
                    data:
                        {
                            billId: '<?= $bills['Bill']['id']; ?>',
                            storageId: storageId,
                        },
                    success: function (data) {
                        currentBillStoragequantity = data;
                        setMaxStorageQuantity($("#BillDepositId").val(), storageId, quantityId, currentBillStoragequantity);
                    }
                });


            })
            <?php
            } ?>

            addcrossremoving();

            // Se spese incasso maggiroe di zero mostro il campo
            showCollectionfeesVat($("#BillCollectionfees").val());
        }
    );
</script>

<script>
    function refreshData()
    {
        $('#BillClientAddress').val("");
        $('#BillClientCap').val("");
        $('#BillClientCity').val("");
        $('#BillClientProvince').val("");

        $('#client_nation_multiple_select').val("");
        $("#client_nation_multiple_select").multiselect('rebuild');

        $('#BillPaymentId').val("");

        $("#BillCollectionfees").val("");
        $(".jsAliquota").hide();
        $('#BillCollectionfeesvatid').attr('required', false);

        if($("#BillSplitPayment").prop("checked") == true)
            $("#BillSplitPayment").click();

        $("#BillWithholdingTax").val("");

        $('#BillAlternativeaddressId').find('option').remove().end();
        $('#BillAlternativeaddressId').val("");
        $("#differentAddress").hide();

        $('#bank_id_multiple_select').val("");
        $("#bank_id_multiple_select").multiselect('rebuild');
    }
</script>

<script>$('.segnalazioni-input').css('cursor', 'pointer'); </script>

<script>
    // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
    // Controllo che non siano ste inserite più di 5 aliquote iva

    function getVat(row)
    {
        return $(row).find(".jsVat").val();
    }

    function getPrice(row)
    {
        return $(row).find(".jsPrice").val();
    }

    function getQuantity(row)
    {
        return $(row).find(".jsQuantity").val();
    }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function () {
        setImporto(this);
        setIvato(this);
        <?php if(MULTICURRENCY && $currencyName != 'EUR'){ ?>setCurrencyChange(this, 'toCurrency');  <?php } ?>
    });
    $(".jsChange").change(function () {
        <?php if(MULTICURRENCY && $currencyName != 'EUR'){ ?>setCurrencyChange(this, 'toEuro');  <?php } ?>
        setImporto(this);
        setIvato(this);
    });
    $(".jsQuantity").change(function () {
        setImporto(this);
        setIvato(this);
    });

    $(".jsTipo").change(function () {
        setCodeRequired(this);
    });

    // Fine codice per gestione prezzo/quantity/tipo

    <?php if($tipologia == 1): ?>
    var formName = "#BillEditForm";
    <?php else: ?>
    var formName = "#BillEditproformaForm";
    <?php endif; ?>

    $(formName).on('submit.default', function (ev) { });

    $(formName).on('submit.validation', function (ev) {
        ev.preventDefault(); // to stop the form from submitting

        /* Validations go here */
        var arrayOfVat = [];
        var errore = 0;
        $(".lunghezza").each( // clonablero

            function () {
                var iva = getVat(this);
                var prezzo = getPrice(this);
                var quantita = getQuantity(this);

                if (arrayOfVat.indexOf(iva) === -1) {
                    arrayOfVat.push(iva);
                }

                if (quantita != '' && quantita !== undefined) {
                    quantita = true;
                } else {
                    quantita = false;
                }
                if (prezzo != '' && prezzo !== undefined) {
                    prezzo = true;
                } else {
                    prezzo = false;
                }
                if (iva !== undefined && iva != '') {
                    iva = true;
                } else {
                    iva = false;
                }

                if ((quantita && prezzo && iva) || (!quantita && !prezzo && !iva)) {
                    /* nothing, is correct */
                    //  errore= 0;
                } else if (iva && (quantita == false || prezzo == false)) {
                    errore = 1;
                } else if (iva == false && (quantita == true || prezzo == true)) {
                    errore = 2;
                } else {
                    /* Not Possible */
                }

                if (arrayOfVat.length > 5) {
                    errore = 3;
                }
            });

        check = 0;
        if ($("#datepickercigdate").val() != '' && $("#datepickercigdate").val() != undefined) {
            check += 1;
        }
        if ($("#BillCigNumItem").val() != '' && $("#BillCigNumItem").val() != undefined) {
            check += 1;
        }
        if ($("#BillCigCodiceCommessa").val() != '' && $("#BillCigCodiceCommessa").val() != undefined) {
            check += 1;
        }
        if ($("#BillCig").val() != '' && $("#BillCig").val() != undefined) {
            check += 1;
        }
        if ($("#BillCup").val() != '' && $("#BillCup").val() != undefined) {
            check += 1;
        }

        var typeEmpty = 0;
        if ($("#BillCigDocumentid").val() != '' && $("#BillCigDocumentid").val() != undefined) {
            typeEmpty = 1;
        }

        var enDoc = 0;
        if ($("#BillCigType").val() != '' && $("#BillCigType").val() != undefined) {
            enDoc = 1;
        }

        if (check > 0 && (typeEmpty == 0 || enDoc == 0)) {
            errore = 77;
        }

        <?php if($bills['Bill']['split_payment'] == 1)
        { ?>
        if ($("#BillEinvoicevatesigibility").is(':visible')) {
            if ($("#BillEinvoicevatesigibility").val() != 'S') {
                errore = 100;
            }
        }
        <?php } ?>

        <?php

        if( $bills['Bill']['accompagnatoria'] == 1)
        {
        ?>
        var pesolordo = $("#BillWeight").val();

        var pesonetto = $("#BillBillNetWeight").val();
        if (+pesonetto > +pesolordo) {
            errore = 111;
        }

        <?php } ?>

        // Errore per cassa previdenziale
        if ($("#BillWelfareBoxCode").val() == '' && ($("#BillWelfareBoxPercentage").val() == 0 || $("#BillWelfareBoxPercentage").val() == '') && $("#BillWelfareBoxVatId").val() == '' && $("#BillWelfareBoxWithholdingAppliance").val() == '') {
        } else {
            if ($("#BillWelfareBoxCode").val() != '' && $("#BillWelfareBoxPercentage").val() != 0 && $("#BillWelfareBoxPercentage").val() != '' && $("#BillWelfareBoxVatId").val() != '' && $("#BillWelfareBoxWithholdingAppliance").val() != '') {
            } else {
                errore = 112;
            }
        }

        if (($("#BillCigDocumentid").val() != '' || $("#datepickercigdate").val() != '' || $("#BillCigNumItem").val() != '' || $("#BillCigCodiceCommessa").val() != '' || $("#BillCig").val() || $("#BillCup").val() != '') && $("#BillCigType").val() == '') {
            errore = 747;
        } else {
            if ($("#BillCigDocumentid").val() == '' && $("#BillCigType").val() != '') {
                errore = 748;
            } else {
            }
        }

        checkBillDuplicate(errore)
    });

    function checkBillDuplicate(errore) {
        // var errore = errore;
        var erroreQuantity = 0;

        <?php
        if( $bills['Bill']['accompagnatoria'] == 1)
        {
        ?>
        // Gestione dei maxquantity dinamici da magazzino solo se è accompagnatoria
        erroreQuantity = checkMaxStorageQuantityError(".jsQuantity", errore);
        <?php } ?>

        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "checkBillDuplicate"]) ?>",
            data:
                {
                    billnumber: $("#BillNumeroFattura").val(),
                    date: $("#datafattura").val(),
                    type: <?= $tipologia ?>,
                    sectional: $(".jsSectional").val(),
                },
            success: function (data) {
                if (data > 0 && ($("#BillNumeroFattura").val() != '<?= $this->data['Bill']['numero_fattura']; ?>')) {
                    errore = 4;
                }

                switch (errore) {
                    case 112:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione compilare tutti i campi della cassa previdenziale (se utilizzata) oppure svuotarli tutti.',
                            type: 'orange',
                        });
                        break;
                        return false;
                    case 111:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione il peso netto non può essere superiore al peso lordo',
                            type: 'orange',
                        });
                        break;
                        return false;
                    case 100:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione è stato selezionato split-payment, ma nel campo "esigibilità iva" non è stato selezionato correttamente "split payment"',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 77:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione sono stati compilati dati nella sezione "riferimento documento", ma non sono stati selezionati n. documento e tipologia di documento.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 0:
                    <?php
                    if($tipologia == 1)
                    {
                    ?>var formName = "#BillEditForm";<?php
                    }
                    else
                    {
                    ?>var formName = "#BillEditproformaForm";<?php
                    }
                    ?>
                        if (erroreQuantity == 11) {
                            $.confirm({
                                title: 'Fattura accompagnatoria.',
                                content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
                                type: 'orange',
                                buttons: {
                                    Ok: function () {
                                        action:
                                        {
                                            <?php
                                            if($tipologia == 1)
                                            {
                                            ?>var formName = "#BillEditForm";<?php
                                            }
                                            else
                                            {
                                            ?>var formName = "#BillEditproformaForm";<?php
                                            }
                                            ?>

                                            $(formName).trigger('submit.default');
                                        }
                                    },
                                    annulla: function () {
                                        action:
                                        {
                                            // Nothing
                                            $(".enhanced-dialog-container").show();
                                        }
                                    },
                                }
                            });
                        } else {
                            $(formName).trigger('submit.default');
                        }
                        break;
                    case 1:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 2:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 3:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 4:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura',
                            content: 'Attenzione esiste già un preventivo con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case  747:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione è stato compilato almeno un campo nella sezione "Riferimento ordine documento", ma non è stata selezionata la tipolgia di documento.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 748:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione è stata selezionata la tipologia di documento ma non è stato correttamente inserito il numero di documento.',
                            type: 'orange',
                        });
                        return false;
                        break;
                }
            },
            error: function (data) {
            }
        });
    }

</script>

<?= $this->element('Js/checkifarenewarticle'); ?>

<script>
    if ($('#BillCollectionfees').val() > 0)
        showCollectionfeesVat($('#BillCollectionfees').val());

    $('#BillCollectionfees').change(function () {
        showCollectionfeesVat($(this).val());
    })

    function showCollectionfeesVat(valueOfCollectionFee)
    {
        if (valueOfCollectionFee > 0)
        {
            $('.jsAliquota').show();
            $('#BillCollectionfeesvatid').attr('required', true);
        }
        else
        {
            $('.jsAliquota').hide();
            $('#BillCollectionfeesvatid').attr('required', false);
        }
    }
</script>

<script>
    $(".jsSeal").click(function () {
        $.alert({
            icon: 'fa fa-question-circle',
            title: '',
            content: "In questa cella va indicato il bollo a proprio carico. Se il bollo deve anche essere sommato al totale del documento (bollo a carico del cliente), si deve aggiungere una voce al dettaglio, per esempio \"Rimborso spese bollo\", con quantità 1, importo del bollo e IVA esente articolo 15.",
            type: 'orange',
        });
    });
    $(".jsDescriptionHelper").click(function () {
        $.alert({
            icon: 'fa fa-question-circle',
            title: '',
            content: "Attenzione i campi note non verranno salvati nella fattura elettronica.",
            type: 'blue',
        });
    });
</script>

<script>
    manageWithholdingApplianceVisibility();

    $(".jsWithholdingTax").change(function () {
        manageWithholdingApplianceVisibility();
    });

    $(".jsBillWelfareBox").change(function () {
        manageWithholdingApplianceVisibility();
    });

    function manageWithholdingApplianceVisibility()
    {
        if ($(".jsWithholdingTax").val() > 0 || $(".jsBillWelfareBox").val())
            $(".jsWithholdingAppliance").show();
        else
            $(".jsWithholdingAppliance").hide();
    }
</script>
