
<div class="portlet-title">
    <div class="caption">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Scaricamento fatturato per cliente'); ?></span>
    </div>
</div>

<div><br/></div>

<form action="<?= $this->Html->url(["controller" => "bills","action" => "downloadVeroFatturato"]) ?>" method="post" id="downloadBillIxfeForm">
    <div class="row">
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data fatturato dal</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="datetime" id="ImportBilldateFromDownload" class="datepicker segnalazioni-input form-control" name="dateFrom" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data fatturato al </strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="datetime" id="ImportBilldateToDownload" class="datepicker segnalazioni-input form-control" name="dateTo" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>

        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
                <input  type="hidden" name="download" value="1"  required />
            </div>
        </div>

        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
                <input  type="hidden" name="filterreceived" value="0"  required />
            </div>
        </div>
        <div class="row col-md-6">
            <label class="form-label form-margin-top"></label>
            <div class="form-controls">
                <div class=" col-md-1 blue-button ebill downloadbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Scarica PDF Fatturato</div>
            </div>
        </div>
    </div>
</form>
<br/>
<script>

    // Importazione fattura manuale
    $(".importbillxml").click(
        function()
        {
            loadFileAsText();
        });

    /* SCARICAMENTO FILE PER DATA*/
    $(".downloadbillixfe").click(
        function() {
            var datastart = $("#ImportBilldateFromDownload").val();
            var dataend = $("#ImportBilldateToDownload").val();
            var diffDays = getDateDiff(dataend, datastart);
            addWaitPointer();
            if (diffDays < 0) {
                $.alert({
                    icon: 'fa fa-warning',
                    title: 'Scaricamento fatture da ixfe',
                    content: 'Attenzione la data di fine non può precedere la data di inizio.',
                    type: 'orange',
                });
                removeWaitPointer();
            } else {
                    $("#downloadBillIxfeForm").submit();
                    removeWaitPointer();
                }
            }
        );
</script>

<?= $this->element('Js/datepickercode'); ?>
<script>
    removeWaitPointer();
</script>
