BETON DUCA SNC di Duca Ermete & C.<br>
Strada Comunale di Campagna, 38 - 23017 MORBEGNO SO<br>
C.F. 00642560148
<br>
<br>
<p align="center"><h1>Statistica Fatturato per articolo</h1> <p><h2><?= $anno ?></h2></p></p>
<table style="width:200mm;border: 2px solid; border-collapse: collapse" >
    <tr style="border: 2px solid">
        <th style="padding: 10px;border-right: 2px solid; ">Articolo</th>
        <th style="padding: 10px;border-right: 2px solid; ">Gennaio</th>
        <th style="padding: 10px;border-right: 2px solid; ">Febbraio</th>
        <th style="padding: 10px;border-right: 2px solid; ">Marzo</th>
        <th style="padding: 10px;border-right: 2px solid; ">Aprile</th>
        <th style="padding: 10px;border-right: 2px solid; ">Maggio</th>
        <th style="padding: 10px;border-right: 2px solid; ">Giugno</th>
        <th style="padding: 10px;border-right: 2px solid; ">Luglio</th>
        <th style="padding: 10px;border-right: 2px solid; ">Agosto</th>
        <th style="padding: 10px;border-right: 2px solid; ">Settembre</th>
        <th style="padding: 10px;border-right: 2px solid; ">Ottobre</th>
        <th style="padding: 10px;border-right: 2px solid; ">Novembre</th>
        <th style="padding: 10px;border-right: 2px solid; ">Dicembre</th>
        <th style="padding: 10px;border-right: 2px solid; ">Totale Anno</th>
        <th style="padding: 10px;border-right: 2px solid; ">Prz. Medio</th>
    </tr>
    <?php
        $totaleFatturato = 0;
        $totaleQuantità = 0;

        $listaMesi = [
            0 => ['fatturato' => 0, 'quantità' => 0],
            1 => ['fatturato' => 0, 'quantità' => 0],
            2 => ['fatturato' => 0, 'quantità' => 0],
            3 => ['fatturato' => 0, 'quantità' => 0],
            4 => ['fatturato' => 0, 'quantità' => 0],
            5 => ['fatturato' => 0, 'quantità' => 0],
            6 => ['fatturato' => 0, 'quantità' => 0],
            7 => ['fatturato' => 0, 'quantità' => 0],
            8 => ['fatturato' => 0, 'quantità' => 0],
            9 => ['fatturato' => 0, 'quantità' => 0],
            10 => ['fatturato' => 0, 'quantità' => 0],
            11 => ['fatturato' => 0, 'quantità' => 0],
        ];
    ?>

    <?php foreach($definitivo as $riga) : ?>
        <tr style="border: 2px solid">
            <td style="padding: 10px;border-right: 2px solid; "><?= $riga['nome'] ?><br><br><p style="text-align: left"><?= $riga['id']?></p><?= $riga['unita']?></td>
            <?php $somma = 0; $qtaTot = 0 ?>
            <?php for($i = 1; $i <= 12; $i++): ?>
                <td style="padding: 10px;border-right: 2px solid; text-align: right">
                    <?php foreach($riga['calcoli'] as $valoriMese): ?>
                        <?php $totali = $valoriMese[0]; ?>
                        <?php if($totali['mese'] == $i): ?>
                            <?php $somma += $totali['totale']; $qtaTot += $totali['qta']; ?>
                            <?= $totali['totale'] != 0 ? number_format($totali['totale'], 2, ',', '.').'<br>'.$totali['qta'] : '' ?>
                            <?php $listaMesi[$i - 1]['quantità'] += $totali['qta'];?>
                            <?php $listaMesi[$i - 1]['fatturato'] += $totali['totale'];?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </td>
            <?php endfor; ?>
            <?php $totaleFatturato += $somma; $totaleQuantità += $qtaTot; ?>
            <td style="padding: 10px;border-right: 2px solid; text-align: right"><?= $somma ?><br><?= $qtaTot ?></td>
            <td style="padding: 10px;border-right: 2px solid; text-align: right"><?= $somma / $qtaTot ?></td>
        </tr>
    <?php endforeach; ?>
    <tr style="border: 2px solid">
        <td style="padding: 5px;border-right: 2px solid">Totali Generali</td>
        <?php for($i = 0; $i < 12; $i++): ?>
            <td style="padding: 5px;border-right: 2px solid; text-align: right"><?= number_format($listaMesi[$i]['fatturato'], 2, ',', '.').'<br>'.$listaMesi[$i]['quantità'] ?></td>
        <?php endfor; ?>
        <td style="padding: 5px;border-right: 2px solid; text-align: right"><?= number_format($totaleFatturato, 2, ',', '.') ?><br><?= $totaleQuantità ?></td>
        <td style="padding: 5px;border-right: 2px solid"></td>
    </tr>
</table>
