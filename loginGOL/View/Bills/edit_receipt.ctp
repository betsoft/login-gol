<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/checkmaxstoragequantityerror'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?php $prezzo_beni_riga  = $totale_imponibile = $totale_imposta   = 0 ?>


    <?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>
           <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;">Modifica scontrino numero <?= $this->data['Bill']['numero_fattura']; ?> del <?= $this->Time->format('d-m-Y', $this->data['Bill']['date']);  ?> </span>
             <div class="col-md-12"><hr></div>
            <?=  $this->Form->input('id'); ?>
    <div class="form-group col-md-12">
        <label class="form-label"><strong>Cliente</strong></label>
        <div class="form-controls">
            <label><?=  $this->request->data['Client']['ragionesociale'] ?></label>
        </div>
    </div>
    <?=  $this->Form->hidden('client_id', ['value' => $this->request->data['Client']['id'],'class' => 'form-control']); ?>

  <div class="form-group col-md-12">
      <div class="col-md-2" style="float:left;">
          <label class="form-label form-margin-top"><strong>Numero scontrino</strong><i class="fa fa-asterisk"></i></label>
          <div class="form-controls">
              <input  class="segnalazioni-input form-control" id="numeroscontrino" name="data[Bill][numero_fattura]"   value="<?= $this->data['Bill']['numero_fattura']; ?>"  pattern="[0-9]+", title='Il campo può contenere solo caratteri alfanumerici' />
           </div>
      </div>

      <div class="col-md-2" style="float:left;margin-left:10px;">
          <label class="form-label form-margin-top"><strong>Data scontrino</strong><i class="fa fa-asterisk"></i></label>
          <div class="form-controls">
              <input  type="datetime" id="datascontrino" class="segnalazioni-input form-control  datepicker" name="data[Bill][date]" value=<?= $this->Time->format('d-m-Y', $this->data['Bill']['date']);  ?> readonly />
          </div>
      </div>
      <div class="col-md-2" style="float:left;margin-left:10px;">
          <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
          <div class="form-controls">
              <?= $this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','required' => 'true', 'empty'=>true]); ?>
          </div>
      </div>
       <?php
        // Se modulo di magazzino attivato
        if(ADVANCED_STORAGE_ENABLED)
		{
		 ?>
	    	<div class="col-md-2" style="float:left;margin-left:10px;">
            	<label class="form-label"><strong>Deposito di prelievo</strong></label>
        	    <div class="form-controls">
              	    <?= $this->Form->select('deposit_id', $deposits, ['class' => 'form-control','required'=>true,'empty'=>false]); ?>
                </div>
      	      </div>
		<?php
    	}
    	else
    	{
    	    echo $this->Form->hidden('deposit_id',['value'=>-1]);
    	}
		?>
    </div>

    <div class="col-md-12"><hr></div>
      <div class="">
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe scontrino</span>
        </div>
      </div>
        <div class="col-md-12"><hr></div>
         <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture"  class="col-md-12">
        <?php
            $conto = count($this->data['Good']) - 1;
            $this->data = $bills;
           $i = -1;
            foreach ($this->data['Good'] as $chiave => $oggetto)
            {
                $i++;
                $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
                $chiave == 0 ? $classe1 = "originale" : $classe1 = '';

                if($oggetto['quantita'] == null && $oggetto['prezzo'] == null && $oggetto['iva_id'] == null)
                {
                    $isNote = 'display:none' ;
                    $isDescriptionNote = ' col-md-12';
                }
                else
                {
                    $isNote = 'display:block';
                    $isDescriptionNote = 'col-md-3';
                }
            ?>
                <div class="principale clonableRow lunghezza contacts_row <?= $classe1 ?> <?= $classe ?>" id="'<?= $chiave ?>'">
                    <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                        <?= $this->Form->input("Good.$chiave.id"); ?>
                   <div class="col-md-12">

                    <?php
                        isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '0';
                        if(ADVANCED_STORAGE_ENABLED)
                        {
                    ?>
                            <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                                 <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                                 <?=  $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>['1'=>'Articolo'],'value'=>isset($oggetto['Storage']['movable']) ? $oggetto['Storage']['movable'] : 0 ]); ?>
                            </div>
                            <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                                 <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                                <?=  $this->Form->input("Good.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control jsCodice']); ?>
                            </div>
                         <?php
                        }
                        else
                        {
                          echo  $this->Form->hidden("Good.$chiave.tipo", ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]);
                    ?>
                         <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                             <label class="form-margin-top"><strong>Codice</strong></label>
                             <?=  $this->Form->input("Good.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice']); ?>
                         </div>
                    <?php
                        }
                    ?>
                        <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                            <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.oggetto", [ 'div' => false, 'label' => false, 'class' => 'form-control jsDescription','pattern'=>PATTERNBASICLATIN]); ?>
                            <?= $this->Form->hidden("Good.$chiave.storage_id"); ?>
                            <?= $this->Form->hidden("Good.$chiave.movable",['class'=>'jsMovable','value'=>$movableValue]);  ?>
                        </div>
                         <div class="col-md-3  jsRowField" style="<?= $isNote ?>">
  						<label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
	            	<?= $this->Form->input("Good.$chiave.customdescription", ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
							</div>
                           <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                            <label class="form-label form-margin-top"><strong>Importo ivato</strong><i class="fa fa-asterisk"></i></label>
                                <?= $this->Form->input("Good.$chiave.prezzo", ['value'=>  $oggetto['prezzo'] + $oggetto['vat'],'label' => false,'class' => 'jsPrice form-control ' . $oggetto['storage_id']]); ?>
                            </div>

                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.quantita", ['label' => false,'class' => 'form-control jsQuantity','prodotto' => $oggetto['storage_id'],'div' => false,'maxquantity'=> $getAviableQuantity->getAvailableQuantity($oggetto['storage_id'],$bills['Bill']['deposit_id']) + $oggetto['quantita']]); ?>
                        </div>
                       <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") : ?>
                           <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                               <label class="form-margin-top"><strong>Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                               <?= $this->Form->input("Good.$chiave.complimentaryQuantity", ['label' => false, 'class' => 'form-control', 'div' => false, 'step'=>'0.0001','disabled'=>false]); ?>
                           </div>
                       <?php endif; ?>
                         <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                         <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                                    <?php isset($oggetto['Iva']['id']) ? $iva = $oggetto['Iva']['id'] : $iva =  ''; ?>
                                 <?=  $this->Form->input("Good.$chiave.iva_id", ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats, 'empty'=>true,  'value' => $iva,'required'=>true]); ?>
                          </div>
                          <div class="row" style="padding-bottom:10px;"><hr></div>
                    </div>
                </div>
        <?php
            $prezzo_beni_riga = '';
            $prezzo_beni_riga = $oggetto['prezzo'] *  $oggetto['quantita'] * (1-$oggetto['discount']/100);
            $totale_imponibile += $prezzo_beni_riga;

           if(isset($oggetto['Iva']['percentuale'])){ $totale_imposta += ($prezzo_beni_riga / 100) *  $oggetto['Iva']['percentuale']; }
        ?>
            <script>
                loadClientCatalog(<?= $i ?>,'<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId","Good");
            </script>
        <?php } ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>

  	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexReceipt']); ?></center>
    <?php echo  $this->Form->end();  ?>

    <script>
    $(document).ready
    (
        function()
        {
           var clienti = setClients();
           //RIMOSSOARTICOLI var articoli = setArticles();

           // Carico il listino
           loadClientCatalog(0,'<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId","Good");

           // Definisce quel che succede all'autocomplete del cliente
           setClientAutocomplete(clienti,"#BillClientId");

           $('#BillImporto').hide();

           enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId");

           addcrossremoving();
        }
    );

</script>
	<?=  $this->element('Js/datepickercode'); ?>

	 <script>


    function getVat(row){return $(row).find(".jsVat").val();}

     function getPrice(row){ return $(row).find(".jsPrice").val(); }

     function getQuantity(row){ return $(row).find(".jsQuantity").val(); }

     // Inizio codice per gestione prezzo/quantity/tipo
     $(".jsTipo").change(function(){setCodeRequired(this);});

    function setCodeRequired(row)
    {
        var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
        if(tipo == 1)
        {
            $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
            $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
            $(row).parents(".clonableRow").find(".jsMovable").val(1);
        }
        else
        {
            $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
            $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
            $(row).parents(".clonableRow").find(".jsMovable").val(0);
        }
    }

</script>

	 <script>
	  $("#BillEditReceiptForm").on('submit.default',function(ev)
     {
     });

     $("#BillEditReceiptForm").on('submit.validation',function(ev)
     {
        ev.preventDefault(); // to stop the form from submitting
        checkReceiptDuplicate()
     });

	 function checkReceiptDuplicate()
       {
           var errore = 0;
           var erroreQuantity = 0;

           erroreQuantity = checkMaxStorageQuantityError(".jsQuantity",errore);

           $.ajax
           ({
    	            method: "POST",
    				url: "<?= $this->Html->url(["controller" => "bills","action" => "checkReceiptDuplicate"]) ?>",
    				data:
    				{
    				    receiptnumber : $("#numeroscontrino").val(),
    				    date : $("#datascontrino").val(),
    				},
    				success: function(data)
    				{
                        if(data > 0 && ( $("#numeroscontrino").val() != '<?= $this->data['Bill']['numero_fattura']; ?>'))
                        {
                            errore = 4;
                        }

                        switch (errore)
                        {
                           case 0:
                                if(erroreQuantity == 11)
                                {
                                    $.confirm({
    			                        title: 'Modifica scontrino.',
        		                        content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
        		                        type: 'orange',
        		                        buttons: {
            		                    Ok: function ()
            		                    {
                                			action:
                                			{
                                                $("#BillEditReceiptForm").trigger('submit.default');
                                			}
                                		},
                                		annulla: function ()
                                		{
                                			action:
                                			{
                                			  $(".enhanced-dialog-container").show();
                                			}
                                		},
                            			}
                    			    });
                                }
                                else
                                {
                                    $("#BillEditReceiptForm").trigger('submit.default');
                                }
                           break;
                           case 4:
                                $.alert
                                ({
    				                icon: 'fa fa-warning',
	    			                title: 'Modifica scontrino',
    				                content: 'Attenzione esiste già uno scontrino con lo stesso numero in data odierna.',
    				                type: 'orange',
				                });
                                return false;
                           break;
                        }
    				},
            		error: function(data)
        			{
        			}

    			});
            }
    </script>

	<?= $this->element('Js/checkifarenewarticle'); ?>
