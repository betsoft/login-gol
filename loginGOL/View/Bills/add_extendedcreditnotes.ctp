<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>
<?= $this->Form->hidden('tipologia',  ['value' => 3,'class' => 'form-control']); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova nota di credito') ?></span>
    <div class="col-md-12"><hr></div>
    <?php $attributes = ['legend' => false]; ?>
    <div class="form-group col-md-12">
        <div class="col-md-4" style="float:left;">
            <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('client_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','required'=>'required','maxlength'=>false,'pattern'=>PATTERNBASICLATIN]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Fattura Elettronica</strong></label>
            <div class="form-controls" style="margin-top:8px;">
                <?= $this->Form->input('Bill.electronic_invoice', ['label' => false,'class' => 'form-control','type'=>'checkbox','checked'=>true]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
            <div class="form-controls" style="margin-top:8px;">
                <?= $this->Form->input('Bill.split_payment', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
            </div>
        </div>
    </div>
    <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label"><strong>Numero nota di credito</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('numero_fattura', ['label' => false,'value' => $nextCreditNoteNumber,'class' => 'form-control','required'=>true,"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']);?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('sectional_id', ['label' => false, 'value'=> $defaultSectional['Sectionals']['id'], 'options'=>$sectionals, 'class' => 'form-control jsSectional']);?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data nota di credito</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
    </div>
    <?= $this->element('Form/client'); ?>
    <div class="form-group col-md-12" id="differentAddress">
        <div class="col-md-12">
            <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('alternativeaddress_id', ['div' => false,'type' => 'select','label' => false,'class' => 'form-control','empty'=>true]); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','empty'=>true,'required'=>true]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.withholding_tax', ['label' => false, 'class' => 'form-control jsWithholdingTax', 'min' => 0, 'max' => 100]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Spese incasso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.collectionfees', ['label' => false, 'class' => 'form-control', 'min' => 0]); ?>
            </div>
        </div>
        <div class="col-md-2 jsAliquota" style="float:left;margin-left:10px;" hidden>
            <label class="form-label form-margin-top"><strong>Aliquota IVA spese incasso</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.collectionfeesvatid', ['label' => false, 'class' => 'form-control', 'min' => 0, 'options' => $vats, 'empty' => true]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Bollo a tuo carico</strong><i class="fa fa-question-circle jsSeal" style="color:#ea5d0b;cursor:pointer;"></i></label>
            <div class="form-controls" >
                <?= $this->Form->input('Bill.seal', ['label' => false,'class' => 'form-control','min'=>0]); ?>
            </div>
        </div>
    <?php if(MULTICURRENCY): ?>
        <div class="col-md-2 jsMulticurrency" style="float:left;margin-left:10px;display:none" >
            <label class="form-label form-margin-top"><strong>Cambio (EUR / <?= '<span class="jsValueCode"></span>' ?>)</strong></label>
            <div class="form-controls" >
                <?= $this->Form->input('Bill.changevalue', ['label' => false,'class' => 'form-control jsChangeValue','min'=>0]); ?>
            </div>
        </div>
    <?php endif; ?>
    </div>
<?php if($settings['Setting']['welfare_box_vat_id'] != '' || $settings['Setting']['welfare_box_percentage'] > 0 ||  $settings['Setting']['welfare_box_withholding_appliance'] != '' || $settings['Setting']['welfare_box_code']): ?>
    <div class="form-group col-md-12">
        <div class=" col-md-3">
            <label class="form-label"><strong>Tipologia cassa previdenziale</strong></label>
            <div class="form-controls" >
                <?= $this->Form->input('welfare_box_code',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $welfareBoxes , 'value'=>$settings['Setting']['welfare_box_code']]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="form-label"><strong>Percentuale</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_percentage',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control jsBillWelfareBox', 'value'=>$settings['Setting']['welfare_box_percentage'] ]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="form-label"><strong>Iva Applicata</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_vat_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $vats, 'value'=>$settings['Setting']['welfare_box_vat_id'] ]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="" style="margin-top:5px"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_withholding_appliance',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => ['0'=>'No','1'=>'Sì'], 'value'=>$settings['Setting']['welfare_box_withholding_appliance']]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="form-group col-md-12">
        <div class="col-md-2">
            <label class="form-label form-margin-top "><strong>Arrotondamento</strong><!--i class="fa fa-question-circle jsDescriptionRounding" style="color:#589ab8;cursor:pointer;" ></i--></label>
            <div class="form-controls">
                <?= $this->Form->input('rounding',['div' => false,'label' => false,'class' => 'form-control','type'=>'select','empty'=>true,'options'=>['0.01'=>'0.01','-0.01'=>'-0.01']]); ?>
            </div>
        </div>
        <div class="col-md-10">
            <label class="form-label form-margin-top"><strong>Note</strong><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;" ></i></label>
            <div class="form-controls">
                <?= $this->Form->input('note', ['div' => false,'label' => false,'class' => 'form-control']); ?>
            </div>
        </div>
    </div>
    <!-- Per fatturazione elettronica -->
    <span id="billPublic" ><?= $this->element('Form/bills/billpa'); ?></span>
    <div class="col-md-12"><hr></div>
    <div class="form-group">
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe nota di credito</div>
        <div class="col-md-12"><hr></div>
        <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
        <fieldset id="fatture"  class="col-md-12">
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden></span>
                <div class="col-md-12">
                <?php if(ADVANCED_STORAGE_ENABLED): ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                            <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ]); ?>
                        </div>
                <?php else: ?>
                    <?= $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'value'=>'0']); ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label"><strong>Codice</strong></label>
                        <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ]); ?>
                    </div>
                <?php endif; ?>
                    <div class="col-md-3 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;" ></i></label>
                    <?php
                        echo $this->Form->input('Good.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control goodDescription jsDescription','pattern'=>PATTERNBASICLATIN]);
                        echo $this->Form->hidden('Good.0.storage_id',['class'=>'jsStorage']);
                        echo $this->Form->hidden('Good.0.movable',['class'=>'jsMovable']);
                    ?>
                    </div>
                    <div class="col-md-3 jsRowField">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.quantita', ['label' => false,'default' => 1, 'step'=>"0.001", 'min'=>"0", 'class' => 'form-control jsQuantity']);?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input('Good.0.unita', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units, 'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.prezzo', ['label' => false,'class' => 'jsPrice form-control']); ?>
                    </div>
                <?php if(MULTICURRENCY): ?>
                    <div class="col-md-2 jsMulticurrency" style="display:none;">
                        <label class="form-margin-top form-label"><strong>Prezzo (<?= '<span class="jsValueCode"></span>' ?>)</strong></label>
                        <?= $this->Form->input('Good.0.currencyprice', ['label' => false,'class' => 'form-control jsChange' ]); ?>
                    </div>
                <?php endif; ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label"><strong>Importo</strong></label>
                        <?= $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                        <?= $this->Form->input('Good.0.discount', ['label' => false,'class' => 'form-control','div'=> false,'max'=>100]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.iva_id', ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats, 'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsWithholdingAppliance jsRowField" hidden>
                        <label class="form-margin-top form-label"><strong>Applica ritenute/contributi</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.withholdingapplied', ['label' => false,'class' => 'form-control jsPrice' , 'options'=>['1'=>'Sì','0'=>'No']]); ?> <!-- Fissato altrimenti me ne da 5 -->
                    </div>
                </div>
                <div class="col-md-12"><hr></div>
            </div>
        </fieldset>
        <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexExtendedcreditnotes']); ?></center>
<?= $this->Form->end(); ?>

<script>
    $(document).ready(
        function()
        {
            // Nascondo a priori
            $("#differentAddress").hide();

            $("#Good0Oggetto").prop('required',true);

            var clienti = setClients();

            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0,$("#BillClientId").val(),"#BillClientId","Good");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti,"#BillClientId");

            // Mostra o nasconde la sezione della fattura elettronica
            showHideElectronicInvoice();

            $('#BillImporto').hide();

            // Abilita il clonable
            enableCloning($("#BillClientId").val(),"#BillClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();

            showCollectionfeesVat($("#BillCollectionfees").val());
        }
    );
</script>

<?= $this->element('Js/datepickercode'); ?>

<script>
    function getVat(row){ return $(row).find(".jsVat").val(); }
    function getPrice(row){ return $(row).find(".jsPrice").val(); }
    function getQuantity(row){ return $(row).find(".jsQuantity").val(); }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function(){
        setImporto(this);
        <?php if(MULTICURRENCY){ ?>setCurrencyChange(this,'toCurrency');  <?php } ?>
    });

    $(".jsChange").change(function()
    {
        <?php if(MULTICURRENCY){ ?>setCurrencyChange(this,'toEuro');  <?php } ?>
        setImporto(this);
    });

    $(".jsQuantity").change(function(){
        setImporto(this);
    });

    $(".jsTipo").change( function() { setCodeRequired(this); });

    $("#BillAddExtendedcreditnotesForm").on('submit.default',function(ev) { });

    $("#BillAddExtendedcreditnotesForm").on('submit.validation',function(ev)
    {
        ev.preventDefault(); // to stop the form from submitting
        /* Validations go here */

        var arrayOfVat = [];
        var errore = 0;
        $(".clonableRow").each(
            function()
            {
                var iva = getVat(this);
                var prezzo = getPrice(this);
                var quantita = getQuantity(this);

                if(arrayOfVat.indexOf(iva) === -1)
                {
                    arrayOfVat.push(iva);
                }

                if(quantita != '' && quantita !== undefined) { quantita = true; } else { quantita = false; }
                if(prezzo != '' && prezzo !== undefined) { prezzo = true; } else { prezzo = false; }
                if(iva !== undefined && iva != '') {iva = true; } else { iva = false; }

                if((quantita && prezzo && iva) || (!quantita && !prezzo && !iva))
                {
                     /* nothing, is correct */
                }
                else if(iva && (quantita == false || prezzo == false))
                {
                    errore = 1;
                }
                else if(iva == false && (quantita == true || prezzo == true))
                {
                    errore = 2;
                }
                else
                {
                  /* Not Possible */
                }

                if(arrayOfVat.length > 4)
                {
                    errore = 3;
                }
            }
        )

        var datanotadicredito = $("#datepicker").val();
        var datafatturanotadicredito = $("#datepickerreferreddate").val();
        var diffDays = getDateDiff(datafatturanotadicredito,datanotadicredito);
        if(diffDays > 0)
        {
            errore = 111;
        }

        checkBillDuplicate(errore)
    });

    function checkBillDuplicate(errore)
    {
        var errore = errore;
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "checkBillDuplicate"]) ?>",
            data:
            {
                billnumber : $("#BillNumeroFattura").val(),
                date : $("#datepicker").val(),
                type : 3,
                sectional : $(".jsSectional").val(),
            },
            success: function(data)
            {
                if(data > 0)
                {
                    errore = 4;
                }

                switch (errore)
                {
                    case 0:
                         $("#BillAddExtendedcreditnotesForm").trigger('submit.default');
                    break;
                    case 1:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione Nota di credito',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                    break;
                    case 2:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione Nota di credito',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                    break;
                    case 3:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione Nota di creditoo',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                   break;
                    case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione Nota di credito',
                            content: 'Attenzione esiste già una nota di credito con lo stesso numero nell\'anno di competenza per il sezionale selezionato.',
                            type: 'orange',
                        });
                        return false;
                   break;
                    case 111:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione la data della fattura collegata non può essere sucessiva la data della nota di credito',
                            type: 'orange',
                        });
                        return false;
                    break;
                }
            },
            error: function(data)
            {
            }
        });
    }
</script>

<script>
    $('.jsSectional').change(function()
    {
        $.ajax({
    	    method: "POST",
    		url: "<?= $this->Html->url(["controller" => "sectionals","action" => "getSectionalNextNumber"]) ?>",
    		data:
    		{
    		    sectionalId : $(this).val(),
    		},
    		success: function(data)
    		{
                $("#BillNumeroFattura").val(+data +1 );
        	}
    	});
    })
</script>

<?= $this->element('Js/checkifarenewarticle'); ?>

<script>
    $(".jsDescriptionHelper").click(function()	{ showHelpMessage('note'); });
    $(".jsSeal").click(function()	{ showHelpMessage('seal'); });
    function showHelpMessage(message)
	{
		switch(message)
		{
			case 'seal':
		        $.alert({
    			    icon: 'fa fa-question-circle',
	    		    title: '',
    			    content: "In questa cella va indicato il bollo a proprio carico. Se il bollo deve essere sommato al totale del documento (bollo a carico del cliente), si deve anche aggiungere una voce al dettaglio, per esempio \"Rimborso spese bollo\", con quantità 1, importo del bollo e IVA esente articolo 15.",
        		    type: 'orange',
			    });
			break;
			case 'note':
		       	$.alert({
        	        icon: 'fa fa-question-circle',
        		    title: '',
    			    content: "Attenzione i campi nota non verranno salvati nella fattura elettronica.",
        		    type: 'blue',
    		    });
 		    break;
		}
	}
</script>

<script>
    manageWithholdingApplianceVisibility();

    $(".jsWithholdingTax").change(function () {
        manageWithholdingApplianceVisibility();
    });

    $(".jsBillWelfareBox").change(function () {
        manageWithholdingApplianceVisibility();
    });

    function manageWithholdingApplianceVisibility()
    {
        if ($(".jsWithholdingTax").val() > 0 || $(".jsBillWelfareBox").val())
            $(".jsWithholdingAppliance").show();
        else
            $(".jsWithholdingAppliance").hide();
    }
</script>

<script>
    if ($('#BillCollectionfees').val() > 0)
    {
        showCollectionfeesVat($('#BillCollectionfees').val());
    }

    $('#BillCollectionfees').change(function () {
        showCollectionfeesVat($(this).val());
    })

    function showCollectionfeesVat(valueOfCollectionFee)
    {
        if (valueOfCollectionFee > 0)
        {
            $('.jsAliquota').show();
            $('#BillCollectionfeesvatid').attr('required', true);
        }
        else
        {
            $('.jsAliquota').hide();
            $('#BillCollectionfeesvatid').attr('required', false);
        }
    }
</script>