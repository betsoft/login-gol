<?php 
	ini_set('memory_limit', '512M');
	set_time_limit(200); 
?>


<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
		<tr>
			<th><?php echo $this->Paginator->sort('numero_fattura', 'Numero Fattura'); ?></th>
			<th><?php echo $this->Paginator->sort('date', 'Data'); ?></th>
			<th><?php echo $this->Paginator->sort('client_id','Cliente'); ?></th>
			<th><?php echo $this->Paginator->sort('pagato','Tipologia'); ?></th>
			<th>Importo</th>
			<th>Importo IVA compr.</th>
			
			</thead>
		</tr>
		<?php
		foreach ($bills as $bill): ?>
		<tr>
			<th><?php echo $bill['Bill']['numero_fattura']; ?></th>
			<td><?php echo $this->Time->format('d.m.Y', $bill['Bill']['date']); ?></td>
			<td class="table-max-width uk-text-truncate">
			<?php echo $this->Html->link($bill['Client']['ragionesociale'], array('controller' => 'clients', 'action' => 'edit', $bill['Client']['id'])); ?>
		</td>
		<?php if($bill['Bill']['tipologia'] == 1): ?>
		<td class="pagamento_fattura<?php echo $bill['Bill']['id']; ?>">
			Vendita:&nbsp;<?php if($bill['Bill']['pagato']==1){echo 'Pagata';} if($bill['Bill']['pagato']==0){echo 'Non Pagata';} if($bill['Bill']['pagato']==2){echo 'Pag. parziale';} if($bill['Bill']['pagato']==3){echo 'Pag. contanti';}?>
		</td>
		<?php else: ?>
		<td>Fatt. acquisto</td>
		<?php endif; ?>
		
		<td>
			<?php
			if($bill['Bill']['tipologia'] == 1):
				$articolo = '';
				$prezzo_articoli = '';
				$prezzo_articoli_ivato = '';
				$prezzo_pagato = '';
				$prezzo_da_pagare = '';
				$prezzo_non_pagato = '';
					
				foreach($bill['Good'] as $articolo) {
				/*	$prezzo_articoli += $articolo['prezzo'] * $articolo['quantita']; 
					$prezzo_articoli_ivato += ($articolo['prezzo'] * $articolo['quantita']) * $articolo['Iva']['percentuale'] / 100; */
				
				   $prezzo_articoli += $articolo['prezzo']; 
					$prezzo_articoli_ivato += ($articolo['prezzo']) * $articolo['Iva']['percentuale'] / 100;
				
					if($bill['Bill']['pagato'] == 1)
					{
						//$prezzo_pagato += $articolo['prezzo'] * $articolo['quantita'];
						$prezzo_pagato += $articolo['prezzo'];
					}	
					if($bill['Bill']['pagato'] == 0)
					{
						//$prezzo_non_pagato += $articolo['prezzo'] * $articolo['quantita'];
						$prezzo_non_pagato += $articolo['prezzo'];
					}
						
					if($bill['Bill']['pagato'] == 2)
					{
						/*$prezzo_da_pagare += $articolo['prezzo'] * $articolo['quantita'];
						$prezzo_pagato_parziale = $bill['Bill']['pagamento_parziale'];*/
						$prezzo_da_pagare += $articolo['prezzo'];
						$prezzo_pagato_parziale = $bill['Bill']['pagamento_parziale'];
					}
				}
				$totale_fatturato += $prezzo_articoli;
				$totale_fatturato_pagato += $prezzo_pagato;
				$totale_fatturato_da_pagare += $prezzo_da_pagare;
				$totale_fatturato_pagato_parzialmente += $prezzo_pagato_parziale;
				$totale_fatturato_non_pagato += $prezzo_non_pagato;
				
				
				//print_r("1-->".$prezzo_pagato." 2--> ".$prezzo_non_pagato."3 --> ".$prezzo_da_pagare);die();
				//print_r("1--->".$totale_fatturato_da_pagare."2-->".$totale_fatturato_non_pagato);die();
			
				echo number_format($prezzo_articoli, 2, ',', '.'); 
			else:
				echo number_format($bill['Bill']['importo'], 2, ',', '.');
				$totale_acquisto += $bill['Bill']['importo'];
				endif;
			?>
		</td>
		<td>
			<?php
			$articolo = '';
			$prezzo_articoli_ivato = '';
			foreach($bill['Good'] as $articolo) {
				//$prezzo_articoli_ivato += ($articolo['prezzo'] * $articolo['quantita']) + (($articolo['prezzo'] * $articolo['quantita']) * $articolo['Iva']['percentuale']) / 100;
				$prezzo_articoli_ivato += ($articolo['prezzo']) + (($articolo['prezzo'] ) * $articolo['Iva']['percentuale']) / 100; 
			}
			echo number_format($prezzo_articoli_ivato, 2, ',', '.');
			$totale_ivato += $prezzo_articoli_ivato;
			?>
		</td>
		
	</tr>
<?php endforeach; ?>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Totale</strong></td>
	<td><?php echo number_format($totale_fatturato, 2, ',', '.'); /*echo number_format($totale_ivato, 2, ',', '.'); */?></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Totale pagato</strong></td>
	<td><?php echo number_format($totale_fatturato_pagato, 2, ',', '.'); ?></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Totale non pagato</strong></td>
	<td><?php  $risultato =$totale_fatturato_da_pagare+$totale_fatturato_non_pagato; echo number_format($risultato, 2, ',', '.'); ?>
	</td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Totale fatt. acquisto</strong></td>
	<td><?php echo number_format($totale_acquisto, 2, ',', '.'); ?></td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Risultato su fatturato</strong></td>
	<td><?php $risultato_lordo = $totale_fatturato - $totale_acquisto; echo number_format($risultato_lordo, 2, ',', '.'); ?>
	</td>
	<td></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td><strong>Risultato su incassi</strong></td>
	<td><?php $risultato_netto = $totale_fatturato_pagato + $totale_fatturato_pagato_parzialmente - $totale_acquisto; echo number_format($risultato_netto, 2, ',', '.'); ?></td>
	<td></td>
</tr>
	</table>