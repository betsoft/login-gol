<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?php
if(PAYPAL)
{
    echo $this->element('Form/formelements/indextitle',['indextitle'=>'Scontrini','indexelements' => ['paypalimport'=>'Importa transazioni paypal','addReceipt'=>'Nuovo scontrino'],'xlsLink'=>'indexReceipt']);
}
else
{
    echo $this->element('Form/formelements/indextitle',['indextitle'=>'Scontrini','indexelements' => ['addReceipt'=>'Nuovo scontrino'],'xlsLink'=>'indexReceipt']);
}
?>

<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
    <thead class ="flip-content">
    <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>

    <?php
    $currentYear = date("Y");
    ?>
    <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields, 'htmlElements' => [
            '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
            '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>']]); ?>
    </tr>
    <?php
    if(count($bills) == 0)
    {
        ?><tr><td colspan="8"><center>nessuno scontrino trovato</center></td></tr><?php
    }
    ?>
    <tbody class="ajax-filter-table-content">
    <?php
    $totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
    $totaleImponibile = 0;
    $totalVat = 0;
    foreach ($bills as $bill)
    {
        if(count($bill['Good']) > 0)
        {
            ?>
            <tr>
                <td><?= $bill['Bill']['numero_fattura']; ?></td>
                <td><?= $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
                <td class="table-max-width uk-text-truncate">
                    <?php echo $bill['Client']['ragionesociale']; ?>
                <td>
                    <?php
                    foreach ($bill['Good'] as $descrizioni)
                    {
                        echo $descrizioni['Oggetto'] . ' ' , $descrizioni['customdescription'].'</br>';
                    }
                    ?>
                </td>
                </td>

                <script>
                    $('#change<?php echo $bill['Bill']['id']; ?>').click(
                        function() {
                            $.ajax({
                                method: "GET",
                                url: "<?= $this->Html->url(["controller" => "bills","action" => "pagamento",$bill['Bill']['id']]) ?>",
                            })
                                .done(function(msg) {
                                    location.reload();
                                });
                        }
                    );
                </script>
                <td style="text-align:right;">
                    <?php
                    $totaleFattura = $Utilities->getReceiptTotal($bill['Bill']['id']);
                    $imponibile= $Utilities->getReceiptTaxable($bill['Bill']['id']);
                    $imponibile = ($totaleFattura / 1.22);
                    $totaleImponibile += $imponibile;
                    echo number_format($imponibile, 2,',','.');
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $vat = $Utilities->getReceiptVat($bill['Bill']['id']);
                    $totalVat += $vat;
                    echo number_format($vat, 2,',','.');
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php $totaleFattura = $Utilities->getReceiptTotal($bill['Bill']['id']);
                    $totale_fatturato += $totaleFattura;
                    echo number_format($totaleFattura,2,',','.');
                    ?>
                </td>

                <td class="actions" style="text-align:left;">

                    <?php
                    $accontato = false;
                    foreach($bill['Deadline'] as $deadline)
                    {
                        if(isset($deadline['Deadlinepayment']))
                        {
                            foreach ($deadline['Deadlinepayment'] as $payment)
                            {
                                if(isset($payment['payment_amount']))
                                {
                                    $accontato = true;
                                }
                            }
                        }
                    }
                    ?>
                    <?php
                    if ($accontato == false)
                    {
                        ?>
                        <?= $this->Html->link($iconaModifica, array('action' => 'editReceipt', $bill['Bill']['id']), array('title'=>__('Modifica scontrino'),'escape' => false));  ?>
                        <?= $this->Form->postLink($iconaElimina, array('action' => 'delete', $bill['Bill']['id']), array('title'=>__('Elimina scontrino'),'escape' => false), __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id'], 'indexReceipt')); ?>
                        <?php
                    }
                    else
                    {
                        echo 'Scontrino non modificabile';
                    }
                    ?>

                </td>
            </tr>
        <?php }
    }

    if (count($bills) > 0)
    {
        ?>
        <tr>
            <?= $this->Graphic->drawVoidColumn(3); ?>
            <td><strong>Totale</strong></td>
            <td style="text-align:right;">
                <b><?=	number_format($totaleImponibile, 2, ',', '.'); ?><b/>

            </td>
            <td style="text-align:right;">
                <b><?=	number_format($totalVat, 2, ',', '.'); ?><b/>
            </td>
            <td style="text-align:right;">
                <b><?=	number_format($totale_fatturato, 2, ',', '.'); ?><b/>


            </td>
            <?= $this->Graphic->drawVoidColumn(1); ?>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?=  $this->element('Form/Components/Paginator/component'); ?>
<?=  $this->element('Js/datepickercode'); ?>

