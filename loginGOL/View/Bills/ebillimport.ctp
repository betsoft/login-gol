<script>
	removeWaitPointer();
</script>

<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Importazione fatture elettroniche ricevute'); ?></span>
	</div>
</div>

<div><br/></div>



<div class="row">
    <?php if($_SESSION['Auth']['User']['group_id'] == 6){ ?>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura dal</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <input  type="datetime" id="ImportBilldateFrom" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura al </strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <input  type="datetime" id="ImportBilldateTo" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div  style="float:left;margin-left:10px;">
        <div class="form-controls">
          <input  type="hidden" name="download" value="0"  required />
        </div>
    </div>
    <div  style="float:left;margin-left:10px;">
        <div class="form-controls">
          <input  type="hidden" name="filterreceived" value="0"  required />
        </div>
    </div>
    <div class="row col-md-6">
        <label class="form-label form-margin-top"></label>
        <div class="form-controls">
            <div class=" col-md-1 blue-button ebill importbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Importa fatture da ixfe nel gestionale filtrate per data fattura</div>
        </div>
    </div>
</div>

</br>

<div class="row">
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data ricezione dal</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <input  type="datetime" id="ImportReceiveddateFrom" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data ricezione al </strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <input  type="datetime" id="ImportReceiveddateTo" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div  style="float:left;margin-left:10px;">
        <div class="form-controls">
          <input  type="hidden" name="download" value="0"  required />
        </div>
    </div>
    <div  style="float:left;margin-left:10px;">
        <div class="form-controls">
          <input  type="hidden" name="filterreceived" value="0" required />
        </div>
    </div>
    <div class="row col-md-6">
        <label class="form-label form-margin-top"></label>
        <div class="form-controls">
            <div class=" col-md-1 blue-button ebill importbillixfereceived" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Importa fatture da ixfe nel gestionale filtrate per data ricezione</div>
        </div>
    </div>
</div>


<?php } ?>
<form action="<?= $this->Html->url(["controller" => "bills","action" => "getIxfePassiveBills"]) ?>" method="post" id="downloadBillIxfeForm">
    <div class="row">
		<div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data fattura dal</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
              <input  type="datetime" id="ImportBilldateFromDownload" class="datepicker segnalazioni-input form-control" name="dateFrom" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data fattura al </strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
              <input  type="datetime" id="ImportBilldateToDownload" class="datepicker segnalazioni-input form-control" name="dateTo" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
              <input  type="hidden" name="download" value="1"  required />
            </div>
        </div>
        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
              <input  type="hidden" name="filterreceived" value="0"  required />
            </div>
        </div>
		<div class="row col-md-6">
            <label class="form-label form-margin-top"></label>
            <div class="form-controls">
				<div class=" col-md-1 blue-button ebill downloadbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Scarica fatture da ixfe filtrate per data fattura</div>
			</div>
        </div>
    </div>
</form>

<br/>
<?php if($_SESSION['Auth']['User']['group_id'] == 6){ ?>
    <form action="<?= $this->Html->url(["controller" => "bills","action" => "getIxfePassiveBills"]) ?>" method="post" id="downloadbillixfeReceivedFilter">
    <div class="row">
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data ricezione dal</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
              <input  type="datetime" id="ImportBilldateFromDownloadReceiVedFilter" class="datepicker segnalazioni-input form-control" name="dateFrom" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data ricezione al </strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
              <input  type="datetime" id="ImportBilldateToDownloadReceiVedFilter" class="datepicker segnalazioni-input form-control" name="dateTo" value="<?= date("d-m-Y") ?>"  required />
            </div>
        </div>
        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
              <input  type="hidden" name="download" value="1"  required />
            </div>
        </div>
        <div  style="float:left;margin-left:10px;">
            <div class="form-controls">
              <input  type="hidden" name="filterreceived" value="1"  required />
            </div>
        </div>
        <div class="row col-md-6">
            <label class="form-label form-margin-top"></label>
            <div class="form-controls">
                <div class=" col-md-1 blue-button ebill downloadbillixfeReceivedFilter" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Scarica fatture da ixfe filtrate per data di ricezione</div>
             </div>
        </div>
    </div>
</form>
<?php }?>


<script>

	// Importazione fattura manuale
	$(".importbillxml").click(
	function()
	{
		loadFileAsText();
	});


	/** IMPORTAZIONE FATTURE DA IXFE **/
	$(".importbillixfe").click(
		function()
		{
	    	addWaitPointer();
    		var datastart = $("#ImportBilldateFrom").val();
            var dataend = $("#ImportBilldateTo").val();
            var diffDays = getDateDiff(dataend,datastart);
            if(diffDays < 0 )
            {
            	$.alert({
	    			icon: 'fa fa-warning',
		    			title: 'Importazione fatture ixfe',
	    				content: 'Attenzione la data di fine non può precedere la data di inizio.',
	    				type: 'orange',
					});
					removeWaitPointer();
            }
            else
            {
	             if(diffDays > 31)
	             {
	        		$.alert({
	    				icon: 'fa fa-warning',
		    			title: 'Importazione fatture ixfe',
	    				content: 'L\'intervallo massimo selezionabile è pari a 31 giorni.',
	    				type: 'orange',
					});
					removeWaitPointer();
	             }
	             else
	             {
		    		$.ajax
		    		({
						method: "POST",
						url: "<?= $this->Html->url(["controller" => "bills","action" => "getIxfePassiveBills"]) ?>",
						async : true,
						data:
						{
							dateFrom: $("#ImportBilldateFrom").val(),
							dateTo: $("#ImportBilldateTo").val(),
							download: 0,
							filterreceived: 0,
						},
						success: function(data)
						{
							$.alert({
			    					icon: 'fa fa-ok',
				    				title: 'Importazione fatture ixfe',
			    				content: 'Importazione effettua con successo.',
			    				type: 'blue',
							});
							removeWaitPointer();
						},
						error: function(data)
						{
							removeWaitPointer();
						}
			    	})
	             }
    	}});

    	/** IMPORTAZIONE FATTURE DA IXFE **/
	$(".importbillixfereceived").click(
		function()
		{
	    	addWaitPointer();
    		var datastart = $("#ImportReceiveddateFrom").val();
            var dataend = $("#ImportReceiveddateTo").val();
            var diffDays = getDateDiff(dataend,datastart);
            addWaitPointer();
            if(diffDays < 0 )
            {
            	$.alert({
	    			icon: 'fa fa-warning',
		    			title: 'Importazione fatture ixfe',
	    				content: 'Attenzione la data di fine non può precedere la data di inizio.',
	    				type: 'orange',
					});
					removeWaitPointer();
            }
            else
            {
	             if(diffDays > 31)
	             {
	        		$.alert({
	    				icon: 'fa fa-warning',
		    			title: 'Importazione fatture ixfe',
	    				content: 'L\'intervallo massimo selezionabile è pari a 31 giorni.',
	    				type: 'orange',
					});
					removeWaitPointer();
	             }
	             else
	             {
		    		$.ajax
		    		({
						method: "POST",
						url: "<?= $this->Html->url(["controller" => "bills","action" => "getIxfePassiveBills"]) ?>",
						async : true,
						data:
						{
							dateFrom: $("#ImportReceiveddateFrom").val(),
							dateTo: $("#ImportReceiveddateTo").val(),
							download: 0,
							filterreceived: 1,
						},
						success: function(data)
						{
							$.alert({
			    					icon: 'fa fa-ok',
				    				title: 'Importazione fatture ixfe',
			    				content: 'Importazione effettua con successo.',
			    				type: 'blue',
							});
								removeWaitPointer();
						},
						error: function(data)
						{
							removeWaitPointer();
						}
			    	})
	             }
            }
    	});


	/* SCARICAMENTO FILE PER DATA*/
	$(".downloadbillixfe").click(
		function()
		{
			var datastart = $("#ImportBilldateFromDownload").val();
            var dataend = $("#ImportBilldateToDownload").val();
            var diffDays = getDateDiff(dataend,datastart);
            addWaitPointer();
            if(diffDays < 0 )
            {
            	$.alert({
	    			icon: 'fa fa-warning',
		    			title: 'Scaricamento fatture da ixfe',
	    				content: 'Attenzione la data di fine non può precedere la data di inizio.',
	    				type: 'orange',
					});
					removeWaitPointer();
            }
            else
            {
	             if(diffDays > 31)
	             {
	        		$.alert({
	    				icon: 'fa fa-warning',
		    			title: 'Scaricamento fatture da ixfe',
	    				content: 'L\'intervallo massimo selezionabile è pari a 31 giorni.',
	    				type: 'orange',
					});
					removeWaitPointer();
	             }
	             else
	             {
					$("#downloadBillIxfeForm").submit();
				//	removeWaitPointer();
	             }
            }

		});


	/* SCARICAMENTO FILE PER RICEZIONE*/
	$(".downloadbillixfeReceivedFilter").click(
		function()
		{
			var datastart = $("#ImportBilldateFromDownloadReceiVedFilter").val();
            var dataend = $("#ImportBilldateToDownloadReceiVedFilter").val();
            var diffDays = getDateDiff(dataend,datastart);

            addWaitPointer();
            if(diffDays < 0 )
            {
            	$.alert({
	    			icon: 'fa fa-warning',
		    			title: 'Scaricamento fatture da ixfe',
	    				content: 'Attenzione la data di fine non può precedere la data di inizio.',
	    				type: 'orange',
					});
					removeWaitPointer();
            }
            else
            {
	             if(diffDays > 31)
	             {
	        		$.alert({
	    				icon: 'fa fa-warning',
		    			title: 'Scaricamento fatture da ixfe',
	    				content: 'L\'intervallo massimo selezionabile è pari a 31 giorni.',
	    				type: 'orange',
					});
					removeWaitPointer();
	             }
	             else
	             {
					$("#downloadbillixfeReceivedFilter").submit();
					removeWaitPointer();
	             }
            }
	});





</script>

<script>


// Qeusto è per il singolo file
function loadFileAsText()
{
	addWaitPointer();
	var fileToLoad = document.getElementById("xmlbillfield").files[0];
	var fileReader = new FileReader();
	fileReader.onload = function(fileLoadedEvent)
	{
    	var textFromFileLoaded = fileLoadedEvent.target.result;

		$.ajax
		({
			method: "POST",
		    url: "<?= $this->Html->url(["controller" => "xmls","action" => "createNewEbillFromXml"]) ?>",
	    	data:
	    	{
	    		xmlEbill : textFromFileLoaded,
		    },
		    success: function(data)
			{
				switch (data)
				{
					case 'Err001':
						 $.alert({
    						icon: 'fa fa-warning',
							title: 'Importazione fattura',
							content: 'Fattura già importata, l\'importazione non è stata possibile',
    						type: 'red',
						});
						removeWaitPointer();
					break;
					default:
						$.alert({
		    				icon: 'fa fa-ok',
							title: 'Importazione fattura',
							content: 'Importazione effettuata correttamente.',
		    				type: 'blue',
						 });
					//	location.assign("<?php // $this->Html->url(["controller" => "bills","action" => "indexExtendedbuy"]) ?>")
						removeWaitPointer();
					break;
				}

		    },
			error: function(data)
			{
				 $.alert({
    				icon: 'fa fa-warning',
					title: 'Importazione fattura',
					content: 'Attenzione si sono verificati dei problemi durante l\'importazione della fattura.',
    				type: 'red',
				 });
				 removeWaitPointer();
			}
		});
	}

	fileReader.readAsText(fileToLoad, "UTF-8");
};

</script>

<?= $this->element('Js/datepickercode'); ?>
