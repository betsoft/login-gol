<div class="portlet-title">
    <div class="caption">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Riepilogo registrazioni acquisti'); ?></span>
    </div>
</div>

<div><br/></div>

<?= $this->Form->create(null, ['url' => ['action' => 'CreateResumeCategorizeBuy', 'controller' => 'bills'], 'class' => 'uk-form uk-form-horizontal','target'=>'_blank']); ?>

<div>
    <table style="width:100%">
        <tr style="width:100%">
            <td style="width:10%;vertical-align:middle  !important;padding:5px;"><label><b>Data dal</b></label></td>
            <td style="width:15%;"><input type="datetime" id="datepicker" class="form-control datepicker" name="data[dateFrom]" required/></td>
            <td style="width:5%;vertical-align:middle !important;padding-left:1%;"><label><b>al</b></label></td>
            <td style="width:15%;"><input type="datetime" id="datepicker2" class="form-control datepicker" name="data[dateTo]" required/></td>
            <td style="width:5%;"><input type="hidden" name="tipo" value="datafatt"></td>
            <td style="min-width:60%;max-width:60%;"><input class="btn blue-button new-bill" type="submit" style="margin-top:0px;min-width:60%;max-width: 60%" value="Genera riepilogo registrazioni acquisti"></td>
        </tr>
    </table>
    <br/>
    <div class="submit" style="margin-left:2%;"></div>
    <?= $this->Form->end();  ?>
</div>

<?= $this->Form->create(null, ['url' => ['action' => 'CreateResumeCategorizeBuy', 'controller' => 'bills'], 'class' => 'uk-form uk-form-horizontal','target'=>'_blank']); ?>

<div>
    <table style="width:100%">
        <tr style="width:100%">
            <td style="width:10%;vertical-align:middle  !important;padding:5px;"><label><b>Data ricezione dal</b></label></td>
            <td style="width:15%;"><input type="datetime" id="datepicker3" class="form-control datepicker" name="data[dateFrom]" required/></td>
            <td style="width:5%;vertical-align:middle !important;padding-left:1%;"><label><b>al</b></label></td>
            <td style="width:15%;"><input type="datetime" id="datepicker4" class="form-control datepicker" name="data[dateTo]" required/></td>
            <td style="width:5%;"><input type="hidden" name="tipo" value="dataric"></td>
            <td style="min-width:60%;max-width:60%;"><input class="btn blue-button new-bill" type="submit" style="margin-top:0px;min-width:60%;max-width: 60%" value="Genera riepilogo registrazioni acquisti" ></td>
        </tr>
    </table>
    <br/>
    <div class="submit" style="margin-left:2%;"></div>
    <?= $this->Form->end();  ?>
</div>

<?= $this->element('Js/datepickercode'); ?>