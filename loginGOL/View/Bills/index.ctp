<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader'); ?>
<?= $this->element('Form/Components/AjaxSort/loader'); ?>
<?= $this->element('Js/ixfe/ixfefunction'); ?>
<?= $this->element('Js/csv/csvfunction'); ?>

<?php
switch ($tipologia)
{
    case 1: // Fatture
        if($_SESSION['Auth']['User']['dbname'] == 'login_GE0044')
            echo $this->element('Form/formelements/indextitle', ['indextitle' => 'Fatture di vendita', 'indexelements' => ['resumecategorizesell' => 'resumecategorizesell', 'ebillsend'=>'Invio multiplo fatture', 'sentbillimport' => 'Esportazione fatture elettroniche', 'resumevatsell' => 'Registro Iva vendite', 'ribaflux' => 'Genera flusso riba', 'add' => 'Nuova fattura', 'statistics' => 'Statistiche'], 'passedValues' => $tipologia, 'xlsLink' => 'index']);
        else if($_SESSION['Auth']['User']['dbname'] == 'login_GE0046')
            echo $this->element('Form/formelements/indextitle', ['indextitle' => 'Fatture di vendita', 'indexelements' => ['resumecategorizesell' => 'resumecategorizesell', 'ebillsend'=>'Invio multiplo fatture', 'ebillImportFromXml'=>'Importa Fatture', 'sentbillimport' => 'Esportazione fatture elettroniche', 'resumevatsell' => 'Registro Iva vendite', 'ribaflux' => 'Genera flusso riba', 'add' => 'Nuova fattura'], 'passedValues' => $tipologia, 'xlsLink' => 'index']);
        else
            echo $this->element('Form/formelements/indextitle', ['indextitle' => 'Fatture di vendita', 'indexelements' => ['resumecategorizesell' => 'resumecategorizesell', 'ebillsend'=>'Invio multiplo fatture', 'sentbillimport' => 'Esportazione fatture elettroniche', 'resumevatsell' => 'Registro Iva vendite', 'ribaflux' => 'Genera flusso riba', 'add' => 'Nuova fattura'], 'passedValues' => $tipologia, 'xlsLink' => 'index']);
        break;
    case 8: // Pro-forma
        echo $this->element('Form/formelements/indextitle', ['sentbillimport' => 'Esportazione fatture elettroniche', 'indextitle' => 'Fatture pro forma', 'indexelements' => ['addProforma' => 'Nuova fattura pro forma'], 'passedValues' => $tipologia, 'xlsLink' => 'index']);
        break; //'proformaSummary'=>'Fatturazione proforma per periodo',
}
?>

<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
    <thead class="flip-content">
    <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
    <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' => $filterableFields,
            'afterAjaxfilterCallback' => 'indexbillAfterAjaxFilterCallback',
            'htmlElements' => [
                '<center><input type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="'.$startDate.'"  bind-filter-event="change"/>' . '' .
                '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="'.$endDate.'" bind-filter-event="change"/></center>'
            ]
        ]);
        ?>
    </tr>
    </thead>
    <tbody class="ajax-filter-table-content">
    <?php if (count($bills) == 0) : ?>
        <tr><td colspan="10"><center>Nessuna fattura trovata</center></td></tr>
    <?php else: ?>
        <?php
        $totaleImponibile = 0; $totaleIvato = 0; $totaleWithholdingtax = 0; $totaleAPagare = 0;
        foreach ($bills as $bill)
        {
            $bill['Bill']['changevalue'] != '' && $bill['Bill']['changevalue'] != null ? $billChange = $bill['Bill']['changevalue'] : null;
            isset($bill['Bill']['collectionfees']) ? $collectionFees = $bill['Bill']['collectionfees'] : $collectionFees = 0;
            isset($bill['Bill']['seal']) ? $seal = $bill['Bill']['seal'] : $seal = 0;
            ?>
            <tr>
                <td><?= $bill['Bill']['numero_fattura'] . $sectionals[$bill['Bill']['sectional_id']] ?></td>
                <td style="text-align:center"><?= $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
                <td class="table-max-width uk-text-truncate"><?php echo $bill['Bill']['client_name']; ?></td>
                <td style="max-width:200px;">
                    <?php
                    foreach ($bill['Good'] as $descrizione)
                    {
                        $printoggetto = $descrizione['Oggetto'] . ' ' . $descrizione['customdescription'];
                        if (strlen($printoggetto) > 40)
                        {
                            echo substr($printoggetto, 0, 39) . '...' . ' <br/>';
                        }
                        else
                        {
                            echo $printoggetto . ' <br/>';
                        }
                    }
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $totalWelfareBox = $Utilities->getWelfareBoxTotal($bill['Bill']['id']) - $Utilities->stornoImportoWelfareBox($bill['Bill']['id']);
                    $taxableIncome = $Utilities->getBillTaxable($bill['Bill']['id']);
                    $taxableIncome += $totalWelfareBox;
                    $taxableIncome = $taxableIncome / $billChange;
                    $totaleImponibile += $taxableIncome;
                    echo number_format($taxableIncome, 2, ',', '.');
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $ivato = $Utilities->getBillTotal($bill['Bill']['id']);
                    $ivato += $totalWelfareBox;
                    $bill['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal = $Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox / 100 : $welfarVatTotal = 0;
                    $ivato += $welfarVatTotal;
                    $ivato = $ivato / $billChange;
                    $totaleIvato += $ivato;
                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0046"){
                        if(isset($bill['Bill']['discount'])){
                            $ivato = $ivato - (($bill['Bill']['discount'] * $ivato )/ 100);
                        }
                    }
                    echo number_format($ivato, 2, ',', '.'); ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $withHoldingTax = $Utilities->getWitholdingTaxTotal($bill['Bill']['id']) / $billChange;
                    $withHoldingTax = $withHoldingTax - $Utilities->stornoImportoWitholdingTax($bill['Bill']['id']) / $billChange;
                    $totaleWithholdingtax += $withHoldingTax;
                    echo number_format($withHoldingTax, 2, ',', '.');
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    isset($coefficienteNC) ? null : $coefficienteNC = 1;
                    if($bill['Payment']['metodo']== 'Pagato'){
                        $bill['Payment']['metodo'] = 'Contanti'; //oppure pagato se dice che non va bene
                        $daPagareFattura=0;}
                    else{
                        $daPagareFattura = $coefficienteNC * $Utilities->getDeadlinesTotal($bill['Bill']['id']);
                        $totaleAPagare += $daPagareFattura;}
                    echo number_format($daPagareFattura, 2, ',', '.');
                    ?>
                </td>
                <td>
                    <?php
                    $color = '';
                    if($bill['Bill']['debitnote'] == 1)
                    {
                        echo 'Nota di debito';
                        if ($bill['Bill']['split_payment'] == 1)
                            echo ' - Split Payment';
                    }
                    else
                    {
                        echo 'Fattura';
                        if ($bill['Bill']['accompagnatoria'] == 1)
                            echo ' Accompagnatoria';
                        if ($bill['Bill']['split_payment'] == 1)
                            echo ' - Split Payment';
                    }

                    $editLink = 'edit';
                    $pdfAction = 'fatturaPdfExtendedVfs';
                    $sendMailAction = 'sendMailBill';
                    $color = 'rosso';
                    ?>
                </td>
                <?php
                if ($bill['Bill']['tipologia'] == 8)
                {
                    $pdfAction = 'fatturaPdfExtendedVfs';
                    $sendMailAction = 'sendMailBillPro';
                    $editLink = 'editproforma';
                    $color = 'rosso';
                }
                ?>
                <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") {?><td><?php echo $bill['Payment']['metodo']?></td><?php }?>
                <td class="actions" style="text-align:left;">
                    <?php
                    $accontato = false;
                    foreach ($bill['Deadline'] as $deadline)
                    {
                        if (isset($deadline['Deadlinepayment']))
                        {
                            foreach ($deadline['Deadlinepayment'] as $payment)
                            {
                                if (isset($payment['payment_amount']))
                                {
                                    if (isset($payment['state']) && $payment['state'] == 1)
                                    {
                                        $accontato = true;
                                    }
                                }
                            }
                        }
                    }

                    $canDoActions = true; $canResend = false;
                    $IXFE_validata = $IXFE_trasmessa = $IXFE_archiviata = true;
                    if($bill['Bill']['electronic_invoice'] == 1)
                    {
                        if($bill['Bill']['id_ixfe'] != -1 && $bill['Bill']['id_ixfe'] != '' && $bill['Bill']['id_ixfe'] != null)
                        {
                            if($bill['Bill']['validation_ixfe'] != null && $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1] == "#E74600")
                                $IXFE_validata = false;

                            if($bill['Bill']['validation_ixfe'] == 'SCARTATA' || $bill['Bill']['validation_ixfe'] == 'VALIDAZIONECONERRORI' || $bill['Bill']['processing_state_ixfe'] == 'SCARTATA' || $bill['Bill']['processing_state_ixfe'] == 'VALIDAZIONECONERRORI' || $bill['Bill']['processing_state_ixfe'] == 'ESITONEGATIVO')
                                $canResend = true;

                            if(!$IXFE_validata || $canResend)
                                $canDoActions = true;
                            else
                                $canDoActions = false;
                        }
                    }
                    else
                    {
                        $canDoActions = !$accontato;
                    }

                    // Modifica fattura
                    if($canDoActions)
                    {
                        echo $this->Html->link($iconaModifica, ['action' => $editLink, $bill['Bill']['id']], ['title' => __('Modifica fattura'), 'escape' => false]);
                    }
                    else
                    {
                        ?>
                        <img src="<?= $this->webroot ?>img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg"
                             alt="Modifica" class="golIcon iconamodificaoff<?= $bill['Bill']['id'] ?>"
                             title="Fattura non modificabile">
                        <?php
                    }

                    // Duplica fattura
                    echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica fattura"></i>', ['action' => 'billduplicate', $bill['Bill']['id'], $bill['Bill']['tipologia']], ['title' => __('Duplica fattura'), 'escape' => false]);

                    // Categorizzazione registrazioni
                    if (MODULO_CONTI)
                        echo $this->Html->link('<i class="fa fa-book" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Categorizza registrazioni"></i>', ['controller' => 'goods', 'action' => 'categorize', $bill['Bill']['id']], ['title' => __('Categorizza registrazioni'), 'escape' => false]);

                    // Generazione fattura da pro forma
                    if($bill['Bill']['tipologia'] == 8)
                        echo $this->Html->link($iconaGeneraFattura, ['action' => 'billduplicate', $bill['Bill']['id'], 1], ['title' => __('Crea fattura'), 'escape' => false]);

                    // PDF fattura
                    $parameter1 = str_replace('/', '', $bill['Client']['ragionesociale']) . '-' . str_replace("/", "", $bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d_m_Y', $bill['Bill']['date']);
                    echo $this->Html->link($iconaPdf, ['action' => $pdfAction, $parameter1, $bill['Bill']['id']], ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);

                    // XML fattura
                    if($bill['Bill']['electronic_invoice'] == 1)
                        echo $this->Html->link($iconaFatturaElettronica, ['action' => 'fattura_xmlvfs', str_replace("/", "", $bill['Client']['ragionesociale']) . '-' . str_replace("/", "", $bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d_m_Y', $bill['Bill']['date']), $bill['Bill']['id']], ['title' => __('Scarica Fattura Elettronica'), 'escape' => false]);


                    // Generazione nota di credito da fattura
                    //if($bill['Bill']['electronic_invoice'] == 1)
                    if(!in_array($bill['Bill']['id'], $arrayCreditnotes))
                        echo $this->Html->link($iconaGeneraNotaCredito, ['action' => 'createCreditnoteFromBill', $bill['Bill']['id']], ['title' => __('Crea nota di credito'), 'escape' => false]);
                    /*if(!in_array($bill['Bill']['id'] == "", $arrayScontrino))
                        echo $this->Html->link($iconaGeneraNotaCredito, ['action' => 'createCreditnoteFromBill', $bill['Bill']['id']], ['title' => __('Crea Scontrino'), 'escape' => false]);
                    */
                    // Invio mail con fattura
                    if ($bill['Client']['mail'])
                    {
                        $toSendFileName = str_replace(' ', '_', $bill['Client']['ragionesociale']) . "_" . str_replace("/", "", $bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d-m-Y', $bill['Bill']['date']);
                        switch ($bill['Bill']['mailsent'])
                        {
                            case 0:
                                echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id']], ['title' => __('Invia fattura come allegato'), 'escape' => false]);
                                break;
                            case 1:
                                echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id']], ['title' => __('Fattura inviata - (ripeti invio)'), 'escape' => false]);
                                break;
                            case 2:
                                echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;"></i>', ['action' => $sendMailAction, $bill['Bill']['id']], ['title' => __('Fattura inviata - (ripeti invio)'), 'escape' => false]);
                                break;
                        }

                        if ($bill['Client']['extra_mail'])
                        {
                            switch ($bill['Bill']['mailsent'])
                            {
                                case 0:
                                    echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id'], true], ['title' => __('Invia fattura come allegato al secondo indirizzo email'), 'escape' => false]);
                                    break;
                                case 1:
                                    echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id'], true], ['title' => __('Fattura inviata - (ripeti invio al secondo indirizzo email)'), 'escape' => false]);
                                    break;
                                case 2:
                                    echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;"></i>', ['action' => $sendMailAction, $bill['Bill']['id'], true], ['title' => __('Fattura inviata - (ripeti invio al secondo indirizzo email)'), 'escape' => false]);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        ?>
                        <i class="fa fa-envelope icon grigio" style="font-size:18px;margin-left:0px;" title="Email mancante nell'anagrafica cliente"></i>
                        <?php
                    }

                    // Icone IXFE
                    if ($bill['Bill']['electronic_invoice'] == 1)
                    {
                        if ($tipologia == 1 && MODULE_IXFE)
                        {
                            if ($bill['Bill']['id_ixfe'] != 'CURLE_OPERATION_TIMEDOUT')
                            {
                                if ($canDoActions)
                                {
                                    echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-Iixfe.jpg', ['alt' => 'Ixfe', 'class' => 'golIcon sendixfe jseal', 'title' => 'Invia ad IXFE', 'url' => ['action' => 'sendBillToIxfe', $bill['Bill']['id']]]);
                                }
                                else
                                {
                                    // Campo per la validazione
                                    if ($bill['Bill']['validation_ixfe'] == null)
                                    {
                                        echo '<img src="' . $this->webroot . 'img/' . $ixfeAttesa . '" title = "In attesa di validazione" style="margin-left:5px;" class="golIcon" / >';
                                    }
                                    else
                                    {
                                        switch ($statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1])
                                        {
                                            case '#ffdb03': // Attesa
                                                echo '<img src="' . $this->webroot . 'img/' . $ixfeWarning . '" title="' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0] . '" style="margin-left:5px;color:' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                            case "#E74600": // Errore
                                                echo '<img src="' . $this->webroot . 'img/' . $ixfeErrore . '" title="' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0] . '" style="margin-left:5px;color:' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                            case "#25AE88": // Validato
                                                echo '<img src="' . $this->webroot . 'img/' . $ixfeOk . '" title="' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0] . '" style="margin-left:5px;color:' . $statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                        }
                                    }

                                    // Campo per il processamento dello stato validazione dello sdi
                                    if ($bill['Bill']['processing_state_ixfe'] == null)
                                    {
                                        echo '<img src="' . $this->webroot . 'img/' . $ixfeAttesa . '" title = "In attesa di trasmissione sdi" style="margin-left:5px;" class="golIcon" / >';
                                    }
                                    else
                                    {
                                        switch ($statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1])
                                        {
                                            case '#ffdb03': // Attesa
                                                echo '<img src="' . $this->webroot . 'img/' . $ixfeWarning . '" title="' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0] . '" style="margin-left:5px;color:' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                            case "#E74600": // Errore
                                                if (($bill['Bill']['processing_state_ixfe'] == 'NONCONSEGNATA') && (($bill['Client']['codiceDestinatario'] == 'XXXXXXX') || ($bill['Client']['codiceDestinatario'] == '0000000' && (($bill['Client']['pec'] == '') || ($bill['Client']['pec'] == null)))))
                                                    echo '<img src="' . $this->webroot . 'img/' . $ixfeOkBlue . '" title="' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0] . '" style="margin-left:5px;color:589AB8" class="golIcon" / >';
                                                else
                                                    echo '<img src="' . $this->webroot . 'img/' . $ixfeErrore . '" title="' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0] . '" style="margin-left:5px;color:' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                            case "#25AE88": // Validato
                                                echo '<img src="' . $this->webroot . 'img/' . $ixfeOk . '" title="' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0] . '" style="margin-left:5px;color:' . $statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1] . '" class="golIcon" / >';
                                                break;
                                        }
                                    }

                                    // Se non è stata validata non mostro il reinvio
                                    if ($bill['Bill']['validation_ixfe'] != null)
                                    {
                                        if ($canResend)
                                            echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-Iixfe.jpg', ['alt' => 'Ixfe', 'class' => 'golIcon resendixfe', 'title' => 'Ripeti invio ad IXFE', 'url' => ['action' => 'sendBillToIxfe', $bill['Bill']['id']]]);
                                    }

                                    if ($bill['Bill']['state_ixfe'] == null)
                                    {
                                        echo '<img src="' . $this->webroot . 'img/' . $ixfeAttesa . '" title = "In attesa di conservazione sostitutiva" style="margin-left:5px;" class="golIcon" / >';
                                    }
                                    else
                                    {
                                        switch ($statoIXFE[$bill['Bill']['state_ixfe']][1])
                                        {
                                            case '#ffdb03': // Attesa
                                                echo '<span title="' . $statoIXFE[$bill['Bill']['state_ixfe']][0] . '" style="margin-left:5px;margin-top:5px;font-size:18px;color:' . $statoIXFE[$bill['Bill']['state_ixfe']][1] . '" class="golIcon fa fa-archive"></span>';
                                                $IXFE_archiviata = false;
                                                break;
                                            case "#E74600": // Errore
                                                echo '<span title="' . $statoIXFE[$bill['Bill']['state_ixfe']][0] . '" style="margin-left:5px;margin-top:5px;font-size:18px;color:' . $statoIXFE[$bill['Bill']['state_ixfe']][1] . '" class="golIcon fa fa-archive"></span>';
                                                $IXFE_archiviata = false;
                                                break;
                                            case "#25AE88": // Validato
                                                echo '<span title="' . $statoIXFE[$bill['Bill']['state_ixfe']][0] . '" style="margin-left:5px;margin-top:5px;font-size:18px;color:' . $statoIXFE[$bill['Bill']['state_ixfe']][1] . '" class="golIcon fa fa-archive"></span>';
                                                break;
                                        }

                                        if (!$IXFE_archiviata)
                                            echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reloadBlue.svg', ['alt' => 'Ixfe', 'class' => 'golIcon sendtoconservation', 'title' => 'Reinvia in archiviazione sostitutiva', 'url' => ['action' => 'resendInConservation', $bill['Bill']['id'], $this->request->params['action']]]);
                                    }

                                    echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reload.svg', ['alt' => 'Ixfe', 'class' => 'golIcon refreshIcon', 'title' => 'Controlla stato aggiornamento fattura', 'style' => 'margin-left:5px;cursor:pointer', 'url' => ['action' => 'refreshIXFEStates', $bill['Bill']['id'], $this->request->params['action']]]);
                                }
                            }
                            else
                            {
                                echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reloadBlack.svg', ['alt' => 'Ixfe', 'class' => 'golIcon jsSyncro', 'title' => 'Risolvi problema di sincronizzazione ixfe', 'url' => ['action' => 'ixfesyncro', $bill['Bill']['id'], $this->request->params['action']]]);
                            }
                        }
                    }

                    // Eliminazione fattura
                    if ($canDoActions)
                        echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $bill['Bill']['id']], ['title' => __('Elimina fattura'), 'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id']));
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <?= $this->Graphic->drawVoidColumn(3); ?>
            <td><strong>Totale</strong></td>
            <td style="text-align:right;"><b><?= number_format($totaleImponibile, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totaleIvato, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totaleWithholdingtax, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totaleAPagare, 2, ',', '.'); ?><b/></td>
            <?= $this->Graphic->drawVoidColumn(2); ?>
        </tr>
    <?php endif; ?>
    </tbody>
</table>

<?= $this->element('Form/Components/Paginator/component'); ?>

<script>


    addLoadEvent(
        function ()
        {
            createAccountantCsv('#createAccountantCsv', $("#selectBank").val());
            refreshIcon(".refreshIcon", "Si stanno aggiornando gli stati della fattura, continuare ?");
            syncro(".jsSyncro", "Si stanno risolvendo i problema di sincronizzazione ixfe, continuare ?");
            sendtoconservation(".sendtoconservation", "Si sta reinviando la fattura in archiviazione sostitutiva, continuare ?");
            sendixfe(".sendixfe", "Si sta inviando la fattura a ixfe, continuare ?  ");
            resendixfe(".resendixfe", "Si sta reinviando un fattura a ixfe, continuare ?");
        }
    )

    function indexbillAfterAjaxFilterCallback()
    {
        createAccountantCsv('#createAccountantCsv', $("#selectBank").val());
        refreshIcon(".refreshIcon", "Si stanno aggiornando gli stati della fattura, continuare ?");
        syncro(".jsSyncro", "Si stanno risolvendo i problema di sincronizzazione ixfe, continuare ? ");
        sendtoconservation(".sendtoconservation", "Si sta reinviando la fattura in archiviazione sostitutiva, continuare ?");
        sendixfe(".sendixfe", "Si sta inviando la fattura a ixfe, continuare ?");
        resendixfe(".resendixfe", "Si sta reinviando un fattura a ixfe, continuare ?");
    }
</script>

<?= $this->element('Js/datepickercode'); ?>
