<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<script>
    $(document).ready
    (
        function()
        {
            var fornitori = setSuppliers();

             // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori,"#BillSupplierId");

            loadSupplierCatalog(setArticles(),"Good","0",'fatture',setCodes());

            $('#BillImporto').hide();

            // Abilita il clonable
            enableCloning($("#BillClientId").val(),"#BillClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );
</script>

<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova fattura d\'acquisto') ?></span>

<div class="col-md-12"><hr></div>

<?php $attributes = ['legend' => false]; ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('numero_fattura', ['label' => false,'value' => $n_fatt,'class' => 'form-control','required'=>true,"pattern"=>"[0-9a-zA-Z-/]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" required />
        </div>
    </div>
    <div class="col-md-4" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('supplier_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','maxlength'=>false, 'required'=>true,'pattern'=>PATTERNBASICLATIN]); ?>
        </div>
    </div>
</div>

<?= $this->element('Form/supplier'); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=$this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','empty' => true,'required'=>true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.withholding_tax', ['label' => false,'class' => 'form-control','min'=>0,'max'=>100]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Spese incasso</strong></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.collectionfees', ['label' => false,'class' => 'form-control','min'=>0]); ?>
        </div>
    </div>
    <div class="col-md-2 jsAliquota" style="float:left;margin-left:10px;" hidden>
        <label class="form-label form-margin-top"><strong>Aliquota iva spese incasso</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.collectionfeesvatid', ['label' => false,'class' => 'form-control','min'=>0, 'options' => $vats, 'empty' =>true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Bollo a carico del fornitore</strong></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.seal', ['label' => false,'class' => 'form-control','min'=>0]); ?>
        </div>
    </div>
</div>

<!-- TIPOLOGIA FATTURE D'ACQUISTO -->
<?= $this->Form->hidden('tipologia',  ['value' => 2,'class' => 'form-control']); ?>

<div class="form-group col-md-12">
    <label class="form-label form-margin-top"><strong>Note</strong></label>
    <div class="form-controls">
        <?= $this->Form->input('note', ['div' => false,'label' => false,'class' => 'form-control']); ?>
    </div>
</div>

<!-- Cassa previdenziale -->
<br/>

<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Cassa previdenziale</div>

<div class="form-group col-md-12">
    <div class="col-md-4">
        <label class="form-label"><strong>Percentuale</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('welfare_box_percentage',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
        </div>
    </div>
    <div class="col-md-4" >
        <label class="form-label"><strong>Iva Applicata</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('welfare_box_vat_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $vats ]); ?>
        </div>
    </div>
    <div class="col-md-4" >
        <label style="margin-top:5px;"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
        <div class="form-controls ">
            <?= $this->Form->input('welfare_box_withholding_appliance',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => ['0'=>'No','1'=>'Sì']]); ?>
        </div>
    </div>
</div>

<span id="billPublic" ><?= $this->element('Form/bills/billpa'); ?></span>

<?= $this->Form->hidden('tipo', ['value' => 3,'class' => 'form-control']); ?>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe fattura</div>
    <div class="col-md-12"><hr></div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture"  class="col-md-12">
        <div class="principale contacts_row clonableRow originale ultima_riga">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden></span>
            <div class="col-md-12">
            <?php if(ADVANCED_STORAGE_ENABLED): ?>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.tipo', ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ]); ?>
                </div>
            <?php else: ?>
                <?= $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'value'=>'0']); ?>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Codice</strong></label>
                    <?= $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control jsCodice' ]); ?>
                </div>
            <?php endif; ?>
                <div class="col-md-3 jsRowFieldDescription">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?php
                        echo $this->Form->input('Good.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control jsDescription','pattern'=>PATTERNBASICLATIN]);
                        echo $this->Form->hidden('Good.0.storage_id');
                        echo $this->Form->hidden('Good.0.movable',['class'=>'jsMovable']);
                    ?>
                </div>
                <div class="col-md-3 jsRowField">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class'=>'form-control','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.quantita', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity', 'step'=> 0.01]);?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input('Good.0.unita', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units,'empty'=>true]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top form-label"><strong>Costo</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.prezzo', ['label' => false,'class' => 'form-control jsPrice']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top form-label"><strong>Importo</strong></label>
                    <?= $this->Form->input('Good.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                    <?= $this->Form->input('Good.0.discount', ['label' => false,'class' => 'form-control','div'=> false,'max'=>100]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.iva_id', ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats,'empty'=>true]); ?>
                </div>
            </div>
            <div class="col-md-12"><hr></div>
        </div>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
</div>

<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexExtendedBuy']); ?></center>

<?= $this->Form->end(); ?>

<?= $this->element('Js/datepickercode'); ?>

<script>
    // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
    // Controllo che non siano ste inserite più di 5 aliquote iva

    function getVat(row){ return $(row).find(".jsVat").val(); }
    function getPrice(row){ return $(row).find(".jsPrice").val(); }
    function getQuantity(row){ return $(row).find(".jsQuantity").val(); }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function(){setImporto(this); });
    $(".jsQuantity").change(function(){setImporto(this);  });
    $(".jsTipo").change(function(){setCodeRequired(this);});

    $("#BillAddExtendedbuyForm").on('submit.default',function(ev) {});

    $("#BillAddExtendedbuyForm").on('submit.validation',
        function(ev)
        {
            ev.preventDefault(); // to stop the form from submitting
            /* Validations go here */

            var arrayOfVat = [];
            var errore = 0;

            $(".clonableRow").each(
                function()
                {
                    var iva = getVat(this);
                    var prezzo = getPrice(this);
                    var quantita = getQuantity(this);

                    if(arrayOfVat.indexOf(iva) === -1)
                    {
                        arrayOfVat.push(iva);
                    }

                    if(quantita != '' && quantita !== undefined) { quantita = true; } else { quantita = false; }
                    if(prezzo != '' && prezzo !== undefined) { prezzo = true; } else { prezzo = false; }
                    if(iva !== undefined && iva != '') {iva = true; } else { iva = false; }

                    if((quantita && prezzo && iva) || (!quantita && !prezzo && !iva))
                    {
                    }
                    else if(iva && (quantita == false || prezzo == false))
                    {
                        errore = 1;
                    }
                    else if(iva == false && (quantita == true || prezzo == true))
                    {
                        errore = 2;
                    }
                    else
                    {
                        /* Not Possible */
                    }

                    if(arrayOfVat.length > 4)
                    {
                        errore = 3;
                    }
                }
            );

            var percentuale = $("#BillWelfareBoxPercentage").val();
            var iva = $("#BillWelfareBoxVatId").val();
            var ritenuta =$("#BillWelfareBoxWithholdingAppliance").val();

            if((iva == ''  && percentuale == '' && ritenuta == '') || (iva != ''  && percentuale != '' && ritenuta != ''))
            {
            }
            else
            {
            errore = 99;
            }

            checkBillDuplicate(errore)
        }
    );

    function checkBillDuplicate(errore)
    {
        var errore = errore;

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "checkBillDuplicate"]) ?>",
            data:
            {
                billnumber : $("#BillNumeroFattura").val(),
                date : $("#datepicker").val(),
                type :2,
                supplierName : $("#BillSupplierId").val(),
            },
            success: function(data)
            {
                if(data > 0)
                {
                    errore = 4;
                }

                switch (errore)
                {
                   case 0:
                        $("#BillAddExtendedbuyForm").trigger('submit.default');
                   break;
                   case 1:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 2:
                       $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 3:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione esiste già una fattura con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 99:
                         $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione sono stati compilati solo parzialmente i campi della cassa previdenziale. Se si vuole compilare la cassa previdenziale, tutti i campi devono essere compilati. In caso contrario devono essere tutti lasciati vuoti.',
                              type: 'orange',
                          });
                   break;
                }

            },
            error: function(data)
            {
            }
        });
    }
</script>

<?= $this->element('Js/checkifarenewarticle'); ?>

<script>
    $('#BillCollectionfees').change(function()
    {
        showCollectionfeesVat($(this).val());
    })

    function showCollectionfeesVat(valueOfCollectionFee)
    {
        if(valueOfCollectionFee > 0)
        {
            $('.jsAliquota').show();
            $('#BillCollectionfeesvatid').attr('required',true);
        }
        else
        {
            $('.jsAliquota').hide();
            $('#BillCollectionfeesvatid').attr('required',false);
        }
    }
</script>
