<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?= $this->element('Js/ixfe/ixfefunction'); ?>
<?= $this->element('Js/csv/csvfunction'); ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Note di credito','indexelements' => ['addExtendedcreditnotes'=>'Nuova nota di credito'],'xlsLink'=>'indexExtendedcreditnotes']); ?>

<?php $currentYear = date("Y");?>

<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
		<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
		<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
			'afterAjaxfilterCallback'=>'indexcreditnoteAfterAjaxFilterCallback',
			'htmlElements' => [
			'<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="01-01-'.$currentYear.'"  bind-filter-event="change"/>'.''.
			'<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="31-12-'.$currentYear.'" bind-filter-event="change"/></center>']]); ?>
		</tr>
	</thead>
	<tbody class="ajax-filter-table-content">
    <?php if (count($bills) == 0) : ?>
        <tr><td colspan="9"><center>Nessuna nota di credito trovata</center></td></tr>
    <?php else: ?>
		<?php
		$totale_fatturato = $totale_fatturato_pagato = $totale_fatturato_da_pagare = $prezzo_pagato_parziale = $totale_fatturato_pagato_parzialmente = $totale_fatturato_non_pagato = $totale_ivato = $totale_acquisto = 0;
		$totaleImponibile = $totaleVat = $totaleWithholdingtax = $totaleApagare = $apagare = 0;
		foreach ($bills as $bill)
		{
			$bill['Bill']['changevalue'] != '' && $bill['Bill']['changevalue'] != null ? $billChange = $bill['Bill']['changevalue'] : null;
			isset($bill['Bill']['seal']) ? $seal = $bill['Bill']['seal'] : $seal = 0;
		?>
			<tr>
				<td><?= $bill['Bill']['numero_fattura'] . $sectionals[$bill['Bill']['sectional_id']]; ?> </td>
				<td><?= $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
				<td class="table-max-width uk-text-truncate"><?php echo $bill['Bill']['client_name']; ?></td>
				<td style="max-width:200px;">
					<?php
						foreach ($bill['Good'] as $descrizione)
						{
							$printoggetto = $descrizione['Oggetto']. ' ' . $descrizione['customdescription'];
							if(strlen($printoggetto) >40)
							{
								echo substr($printoggetto,0,39) . '...' .' <br/>' ;
							}
							else
							{
								echo $printoggetto  .' <br/>' ;
							}
						}
					?>
				</td>
				<td style="text-align:right;">
				<?php
					$totalWelfareBox = $Utilities->getWelfareBoxTotal($bill['Bill']['id']) - $Utilities->stornoImportoWelfareBox($bill['Bill']['id']);
					$taxableIncome = $Utilities->getBillTaxable($bill['Bill']['id']) ;
					$taxableIncome += $totalWelfareBox;
					$taxableIncome = $taxableIncome / $billChange;
					$totaleImponibile += $taxableIncome ;
					echo number_format($taxableIncome  / $billChange,2,',','.');
				?>
				</td>
				<td style="text-align:right;">
				<?php
					$ivato = $Utilities->getBillTotal($bill['Bill']['id']);
					$ivato += $totalWelfareBox; 
					$bill['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal =  $Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox / 100  : $welfarVatTotal = 0;
					$ivato  += $welfarVatTotal ;
					$ivato = $ivato / $billChange;
					$totaleVat += $ivato;
					echo  number_format($ivato ,2,',','.'); 
				?>
				</td>
				<td style="text-align:right;">
				<?php 
					$withHoldingTax = $Utilities->getWitholdingTaxTotal($bill['Bill']['id']) /  $billChange;
					$totaleWithholdingtax += $withHoldingTax;
					echo number_format($withHoldingTax,2,',','.');
				?>
				</td>
				<td style="text-align:right;">
				<?php
                    isset($coefficienteNC) ? null : $coefficienteNC = 1;
					$daPagareFattura = $coefficienteNC * $Utilities->getDeadlinesTotal($bill['Bill']['id']);
					$totaleApagare += $daPagareFattura;
					echo number_format($daPagareFattura,2,',','.');
				?>
				</td>
				<td class="actions" style="text-align:left;">
		<?php
			$editLink = 'editExtendedcreditnotes';
			$pdfAction = 'fatturaPdfExtendedcreditnotes';
			$sendMailAction = 'sendmailBillCreditnote';

			$accontato = false;
			foreach($bill['Deadline'] as $deadline)
			{
				if(isset($deadline['Deadlinepayment']))
				{
					foreach ($deadline['Deadlinepayment'] as $payment)
					{
						if(isset($payment['payment_amount']))
						{
							$accontato = true;
						}
					}
				}
			}

            // Pubblica amministrazione fattura elettronica
            if($bill['Bill']['electronic_invoice'] == 1)
            {
                if($accontato == false)
                {
                    echo $this->Html->link($iconaModifica, ['action' => $editLink, $bill['Bill']['id']], ['title'=>__('Modifica nota di credito'), 'escape' => false, 'class' => 'iconamodifica'.$bill['Bill']['id']]);
                    ?>
                    <img src="<?= $this->webroot ?>img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica"  class="golIcon iconamodificaoff<?= $bill['Bill']['id'] ?>"  title = "Nota di credito non modificabile">
                    <?php
                }
                else
                {
                ?>
                    <img src="<?= $this->webroot ?>img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica"  class="golIcon iconamodificaoff<?= $bill['Bill']['id'] ?>"  title = "Nota di credito non modificabile">
                <?php
                }

                echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica nota di credito"></i>', ['action' => 'billduplicate', $bill['Bill']['id'],3], ['title' => __('Duplica nota di credito'), 'escape' => false]);

                if(MODULO_CONTI)
                    echo $this->Html->link('<i class="fa fa-book" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Categorizza registrazioni"></i>', ['controller' => 'goods', 'action' => 'categorize', $bill['Bill']['id']], ['title' => __('Categorizza registrazioni'), 'escape' => false]);

                $parameter1 = $bill['Client']['ragionesociale'] . '-' . str_replace("/","",$bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d_m_Y', $bill['Bill']['date']);

                echo $this->Html->link($iconaPdf, ['action' => $pdfAction, $parameter1, $bill['Bill']['id']], ['target'=>'_blank','title'=>__('Scarica PDF'),'escape' => false]);
                echo $this->Html->link($iconaFatturaElettronica, ['action' => 'fattura_xmlvfs', str_replace("/","",$bill['Client']['ragionesociale']) . '-' . str_replace("/","",$bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d_m_Y', $bill['Bill']['date']), $bill['Bill']['id']], ['title'=>__('Scarica Fattura Elettronica'),'escape' => false]);

                // Adesso la mail la invio anche per fatture elettroniche
                if($bill['Client']['mail'])
                {
                    $toSendFileName  =  str_replace(' ', '_', $bill['Client']['ragionesociale'])."_". str_replace("/","",$bill['Bill']['numero_fattura']) . '_' . $this->Time->format('d-m-Y', $bill['Bill']['date']);
                    switch($bill['Bill']['mailsent'])
                    {
                        case 0:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Invia fattura come allegato'),'escape' => false]);
                        break;
                        case 1:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;margin-top:4px;"></i>',['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Fattura inviata - (ripeti invio)'),'escape' => false]);
                        break;
                        case 2:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;"></i>',['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Fattura inviata - (ripeti invio)'),'escape' => false]);
                        break;
                    }
                }
                else
                {
                    ?><i class="fa fa-envelope icon grigio" style="font-size:18px;margin-left:0px;" title="Email mancante nell'anagrafica cliente"></i><?php
                }

                $IXFE_validata = $IXFE_trasmessa = $IXFE_archiviata = false;

                // Se abilitato modulo IXFE (accompagnatoria devo toglierlo appena sistemo)
                if(MODULE_IXFE)
                {
                    if($bill['Bill']['id_ixfe'] != 'CURLE_OPERATION_TIMEDOUT')
                    {
                        if(isset($bill['Bill']['id_ixfe']) && $bill['Bill']['id_ixfe'] != -1 && $bill['Bill']['id_ixfe'] != '' &&  $bill['Bill']['id_ixfe'] != null)
                        {
                            // Campo per la validazione
                            if($bill['Bill']['validation_ixfe'] == null )
                            {
                                echo '<img src="'.$this->webroot.'img/'.$ixfeAttesa.'" title = "In attesa di validazione" style="margin-left:5px;" class="golIcon" / >';
                                $IXFE_validata = true;
                            }
                            else
                            {
                                switch($statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1])
                                {
                                    case '#ffdb03': // Attesa
                                        echo '<img src="'.$this->webroot.'img/'.$ixfeWarning.'" title="'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0].'" style="margin-left:5px;color:'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1].'" class="golIcon" / >';
                                        $IXFE_validata = true;
                                    break;
                                    case "#E74600": // Errore
                                        echo '<img src="'.$this->webroot.'img/'.$ixfeErrore.'" title="'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0].'" style="margin-left:5px;color:'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1].'" class="golIcon" / >';
                                    break;
                                    case "#25AE88": // Validato
                                        echo '<img src="'.$this->webroot.'img/'.$ixfeOk.'" title="'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][0].'" style="margin-left:5px;color:'.$statoNotificaIXFE[$bill['Bill']['validation_ixfe']][1].'" class="golIcon" / >';
                                        $IXFE_validata = true;
                                    break;
                                }
                            }

                            // Campo per il processamento dello stato validazione dello sdi
                            if($bill['Bill']['processing_state_ixfe'] == null)
                            {
                                echo '<img src="'.$this->webroot.'img/'.$ixfeAttesa.'" title = "In attesa di trasmissione sdi" style="margin-left:5px;" class="golIcon" / >';
                                $IXFE_trasmessa = true; // Non faccio modificare nemmeno se non so in che stato siamo
                            }
                            else
                            {
                                switch($statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1])
                                {
                                    case '#ffdb03': // Attesa
                                        echo '<img src="'.$this->webroot.'img/'.$ixfeWarning.'" title="'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0].'" style="margin-left:5px;color:'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1].'" class="golIcon" / >';
                                        if($bill['Bill']['processing_state_ixfe'] == 'CONSEGNATA' || $bill['Bill']['processing_state_ixfe'] == 'TRASMESSA' || $bill['Bill']['processing_state_ixfe'] == 'DECORRENZATERMINI')
                                        {
                                            $IXFE_trasmessa = true;
                                        }
                                    break;
                                    case "#E74600": // Errore
                                        if(($bill['Bill']['processing_state_ixfe'] == 'NONCONSEGNATA') && (($bill['Client']['codiceDestinatario'] == 'XXXXXXX') || ($bill['Client']['codiceDestinatario'] == '0000000' && (($bill['Client']['pec'] == '') || ($bill['Client']['pec'] == null)))))
                                        {
                                            $IXFE_trasmessa = true;
                                            echo '<img src="'.$this->webroot.'img/'.$ixfeOkBlue.'" title="'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0].'" style="margin-left:5px;color:589AB8" class="golIcon" / >';
                                        }
                                        else
                                        {
                                            echo '<img src="'.$this->webroot.'img/'.$ixfeErrore.'" title="'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0].'" style="margin-left:5px;color:'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1].'" class="golIcon" / >';
                                        }
                                    break;
                                    case "#25AE88": // Validato
                                        echo '<img src="'.$this->webroot.'img/'.$ixfeOk.'" title="'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][0].'" style="margin-left:5px;color:'.$statoElaborazioneIXFE[$bill['Bill']['processing_state_ixfe']][1].'" class="golIcon" / >';
                                        $IXFE_trasmessa = true;
                                    break;
                                }
                            }

                            // Se non è stata validata non mostro il reinvio
                            if($bill['Bill']['validation_ixfe'] != null)
                            {
                                if( $bill['Bill']['validation_ixfe'] == 'SCARTATA' || $bill['Bill']['validation_ixfe'] == 'VALIDAZIONECONERRORI' || $bill['Bill']['processing_state_ixfe'] == 'SCARTATA'  || $bill['Bill']['processing_state_ixfe'] == 'VALIDAZIONECONERRORI' ||  $bill['Bill']['processing_state_ixfe'] == 'ESITONEGATIVO')
                                {
                                    echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-Iixfe.jpg', ['alt' => 'Ixfe', 'class'=>'golIcon resendixfe', 'title'=>'Ripeti invio ad IXFE', 'url'=>['action' => 'sendBillToIxfe', $bill['Bill']['id']]]);
                                }
                            }

                            if($bill['Bill']['state_ixfe'] == null)
                            {
                                echo '<img src="'.$this->webroot.'img/'.$ixfeAttesa.'" title = "In attesa di conservazione sostitutiva" style="margin-left:5px;" class="golIcon" / >';
                            }
                            else
                            {
                                switch($statoIXFE[$bill['Bill']['state_ixfe']][1])
                                {
                                    case '#ffdb03': // Attesa
                                        echo '<span title="'.$statoIXFE[$bill['Bill']['state_ixfe']][0].'" style="margin-left:5px;margin-top:5px;font-size:18px;color:'.$statoIXFE[$bill['Bill']['state_ixfe']][1].'" class="golIcon fa fa-archive"  ></span>';
                                        $IXFE_archiviata =false;
                                    break;
                                    case "#E74600": // Errore
                                        echo '<span title="'.$statoIXFE[$bill['Bill']['state_ixfe']][0].'" style="margin-left:5px;margin-top:5px;font-size:18px;color:'.$statoIXFE[$bill['Bill']['state_ixfe']][1].'" class="golIcon fa fa-archive"  ></span>';
                                        $IXFE_archiviata =false;
                                    break;
                                    case "#25AE88": // Validato
                                        echo '<span title="'.$statoIXFE[$bill['Bill']['state_ixfe']][0].'" style="margin-left:5px;margin-top:5px;font-size:18px;color:'.$statoIXFE[$bill['Bill']['state_ixfe']][1].'" class="golIcon fa fa-archive"  ></span>';
                                        $IXFE_archiviata = true;
                                    break;
                                }

                                // SE non è stato archiviato correttamente ma è validato e trasmesso posso tentare di inviare di nuovo all'archiviazione
                                if($IXFE_archiviata == false && $IXFE_trasmessa == true && $IXFE_validata == true )
                                {
                                    echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reloadBlue.svg', ['alt' => 'Ixfe', 'class' => 'golIcon sendtoconservation', 'title' => 'Reinvia in archiviazione sostitutiva', 'url' => ['action' => 'resendInConservation', $bill['Bill']['id'], $this->request->params['action']]]);
                                    //echo '<img src="'.$this->webroot.'img/'.$ixfeReloadBlue.'"  title="Reinvia in archiviazione sostitutiva" class = "golIcon sendtoconservation" style="margin-left:5px;cursor:pointer" resendId="'.$bill['Bill']['id'].'" / >';
                                }
                            }

                            echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reload.svg', ['alt' => 'Ixfe', 'class' => 'golIcon refreshIcon', 'title' => 'Controlla stato aggiornamento fattura', 'style' => 'margin-left:5px;cursor:pointer', 'url' => ['action' => 'refreshIXFEStates', $bill['Bill']['id'], $this->request->params['action']]]);
                            //echo '<img src="'.$this->webroot.'img/'.$ixfeReload.'"  title="Controlla stato aggiornamento fattura" class = "golIcon refreshIcon" style="margin-left:5px;cursor:pointer" refreshid="'.$bill['Bill']['id'].'" / >';
                        }
                        else
                        {
                            echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-Iixfe.jpg', ['alt' => 'Ixfe', 'class'=>'golIcon sendixfe', 'title'=>'Invia ad IXFE', 'url'=>['action' => 'sendBillToIxfe', $bill['Bill']['id']]]);
                            echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $bill['Bill']['id']], ['title'=>__('Elimina fattura'),'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id']));
                        }
                    }
                    else
                    {
                        echo $this->Html->image('gestionaleonlineicon/gestionale-online.net-reloadBlack.svg', ['alt' => 'Ixfe', 'class' => 'golIcon jsSyncro', 'title' => 'Risolvi problema di sincronizzazione ixfe', 'url' => ['action' => 'ixfesyncro', $bill['Bill']['id'], $this->request->params['action']]]);
                        //echo '<img src="'.$this->webroot.'img/'.$ixfeReloadBlack.'"  title="Risolvi problema di sincronizzazione ixfe" class = "golIcon jsSyncro" style="margin-left:5px;cursor:pointer" refreshid="'.$bill['Bill']['id'].'" / >';
                    }
                }
                else
                {
                    if($accontato == false)
                    {
                        echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $bill['Bill']['id']], ['title'=>__('Elimina fattura'),'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id']));
                    }
                }

                if($IXFE_validata && $IXFE_trasmessa )
                {
                    ?><script>$('.iconamodifica<?= $bill['Bill']['id'] ?>').hide();</script><?php
                }
                else
                {
                    if(!$accontato)
                    {
                        ?><script>$('.iconamodificaoff<?= $bill['Bill']['id'] ?>').hide();</script><?php
                    }
                }
            }
            else
            {
                // Se la nota di credito è stata accontata non può essere più modificata
                if($accontato == false)
                {
                     echo $this->Html->link($iconaModifica, ['action' => $editLink, $bill['Bill']['id']], ['title'=>__('Modifica fattura'),'escape' => false]);
                }

                $tempTxt = $bill['Client']['ragionesociale'] . '-' . str_replace("/","",$bill['Bill']['numero_fattura']) . '_' .$this->Time->format('d_m_Y', $bill['Bill']['date']);
                echo $this->Html->link($iconaPdf, ['action' => $pdfAction, $tempTxt, $bill['Bill']['id']], ['target'=>'_blank','title'=>__('Scarica PDF'),'escape' => false]);

                // Adesso la mail la invio anche per fatture elettroniche
                if($bill['Client']['mail'])
                {
                    $toSendFileName  =  str_replace(' ', '_', $bill['Client']['ragionesociale'])."_". $bill['Bill']['numero_fattura'] . '_' . $this->Time->format('d-m-Y', $bill['Bill']['date']);
                    switch($bill['Bill']['mailsent'])
                    {
                        case 0:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;margin-top:4px;"></i>', ['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Invia fattura come allegato'),'escape' => false]);
                        break;
                        case 1:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;margin-top:4px;"></i>',['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Fattura inviata - (ripeti invio)'),'escape' => false]);
                        break;
                        case 2:
                            echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;"></i>',['action' => $sendMailAction, $bill['Bill']['id']], ['title'=>__('Fattura inviata - (ripeti invio)'),'escape' => false]);
                        break;
                    }
                }
                else
                {
                    if($bill['Bill']['electronic_invoice'] == 0)
                    {
                    ?>
                        <i class="fa fa-envelope icon grigio" style="font-size:18px;margin-left:0px;" title="Email mancante nell'anagrafica cliente"></i>
                    <?php
                    }
                }
            }

            // Eliminazione fattura
            if ($accontato == false)
            {
                if($bill['Bill']['electronic_invoice'] == 0 )
                    echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $bill['Bill']['id']], ['title'=>__('Elimina fattura'),'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id']));
            }
            else
            {
                if($bill['Bill']['electronic_invoice'] == 0)
                {
                    // Qui devo modificare l'icona modifica (va messa ma devo sostituire anche quella attiva)
                    //echo $this->Form->postLink($iconaEliminaOff, ['action' => ''], ['title'=>__('Fattura non eliminabile')]);
                }
            }
            ?>
                </td>
	        </tr>
        <?php
		}
        ?>
            <tr>
                <?= $this->Graphic->drawVoidColumn(3); ?>
                <td><strong>Totale</strong></td>
                <td style="text-align:right;"><b><?= number_format($totaleImponibile, 2, ',', '.'); ?><b/></td>
                <td style="text-align:right;"><b><?= number_format($totaleVat, 2, ',', '.'); ?><b/></td>
                <td style="text-align:right;"><b><?= number_format($totaleWithholdingtax, 2, ',', '.'); ?><b/></td>
                <td style="text-align:right;"><b><?= number_format($totaleApagare, 2, ',', '.'); ?><b/></td>
                <td></td>
            </tr>
    <?php endif; ?>
    </tbody>
</table>
<?= $this->element('Form/Components/Paginator/component'); ?>

<script>
	addLoadEvent(
	    function()
	    {
            refreshIcon(".refreshIcon", "Si stanno aggiornando gli stati della nota di credito, continuare ?");
            syncro(".jsSyncro", "Si stanno risolvendo i problema di sincronizzazione ixfe, continuare ?");
            sendtoconservation(".sendtoconservation", "Si sta reinviando la nota di credito in archiviazione sostitutiva, continuare ?");
            sendixfe(".sendixfe","Si sta reinviando una nota di credito a ixfe, continuare ?");
            resendixfe(".resendixfe","Si sta reinviando una nota di credito a ixfe, continuare ?");
	    }
    )
	
	function indexcreditnoteAfterAjaxFilterCallback()
	{
        refreshIcon(".refreshIcon", "Si stanno aggiornando gli stati della nota di credito, continuare ?");
        syncro(".jsSyncro", "Si stanno risolvendo i problema di sincronizzazione ixfe, continuare ?");
        sendtoconservation(".sendtoconservation", "Si sta reinviando la nota di credito in archiviazione sostitutiva, continuare ?");
		sendixfe(".sendixfe","Si sta reinviando una nota di credito a ixfe, continuare ?");
		resendixfe(".resendixfe","Si sta reinviando una nota di credito a ixfe, continuare ?");
	}
</script>

<?= $this->element('Js/datepickercode'); ?>