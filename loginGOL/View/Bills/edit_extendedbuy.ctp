<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/suppliercatalog'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<script>
    $(document).ready
    (
        function()
        {
            var fornitori = setSuppliers();
            var articoli = setArticles();
            var codici = setCodes();

            // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori,"#BillSupplierId");

            //loadSupplierCatalog(articoli,"Good","0","Oggetto",'fatture',codici);
            loadSupplierCatalog(articoli,"Good","0",'fatture',codici);

            $('#BillImporto').hide();

            enableCloningedit(null,"#BillClientId");

            addcrossremoving();
        }
    );
</script>

<?php $prezzo_beni_riga  = $totale_imponibile = $totale_imposta   = 0 ?>

<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;">Modifica fattura d'acquisto</span>

<div class="col-md-12"><hr></div>

<?=  $this->Form->input('id'); ?>

<?= $this->Form->hidden('Supplier.name', ['value'=>$this->request->data['Supplier']['name']]); ?>
<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('numero_fattura', ['label' => false,'class' => 'form-control','required'=>true,"pattern"=>"[0-9a-zA-Z-/]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']);?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data fattura</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" class="datepicker segnalazioni-input form-control" id="datafattura" readonly  name="data[Bill][date]" value=<?= $this->Time->format('d-m-Y', $this->data['Bill']['date']); ?>  />
        </div>
    </div>
    <div class="form-group col-md-4">
        <label class="form-label form-margin-top"><strong>Fornitore</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'supplier_id',
                "aggregator" => '',
                "prefix" => "supplier_id",
                "list" => $suppliers,
                "options" => [ 'multiple' => false,'required'=> false],
            ]);
            ?>
        </div>
    </div>

</div>
<?php
    if($bills['Supplier']['name'] != $this->request->data['Supplier']['name'])
        echo '</br><span style="color:#ff0000;">La ragione sociale memorizzata sulla fattura "' . $bills['Supplier']['name'] . '" è differente da quella mostrata per aggiornarla cliccare su salva</span>.';
?>

<?= $this->element('Form/supplier'); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','required'=>true, 'empty'=>true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
        <div class="form-controls" >
        <?= $this->Form->input('Bill.withholding_tax', ['label' => false,'class' => 'form-control','min'=>0,'max'=>100]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Spese incasso</strong></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.collectionfees', ['label' => false,'class' => 'form-control','min'=>0]); ?>
        </div>
    </div>
    <div class="col-md-2 jsAliquota" style="float:left;margin-left:10px;" hidden>
        <label class="form-label form-margin-top"><strong>Aliquota iva spese incasso</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.collectionfeesvatid', ['label' => false,'class' => 'form-control','min'=>0, 'options' => $vats, 'empty' =>true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
         <label class="form-label form-margin-top"><strong>Bollo a carico del fornitore</strong></label>
        <div class="form-controls" >
            <?= $this->Form->input('Bill.seal', ['label' => false,'class' => 'form-control','min'=>0]); ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <label class="form-label"><strong>Note</strong></label>
    <div class="form-controls">
        <?= $this->Form->input('note', ['label' => false,   'class' => 'form-control','id' => 'lunghezza2','div' => false]); ?>
    </div>
</div>

<div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Cassa previdenziale</div>

<div class="form-group col-md-12">
    <div class="col-md-4" >
        <label class="form-label"><strong>Percentuale</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('welfare_box_percentage',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
        </div>
    </div>
    <div class="col-md-4" >
        <label class="form-label"><strong>Iva Applicata</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('welfare_box_vat_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $vats ]); ?>
        </div>
    </div>
    <div class="col-md-4" >
        <label style="margin-top:5px;"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
        <div class="form-controls ">
            <?= $this->Form->input('welfare_box_withholding_appliance',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => ['0'=>'No','1'=>'Sì']]); ?>
        </div>
    </div>
</div>

<span id="billPublic" ><?= $this->element('Form/bills/billpa'); ?></span>

<?= $this->data['Bill']['data_parziale']; ?>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe fattura</div>
    <div class="col-md-12"><hr></div>
    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture"  class="col-md-12">
        <?php
            $conto = count($this->data['Good']) - 1;
            $this->data = $bills;
        ?>
        <script>
            var articoli = setArticles();
            var codici = setCodes();
        </script>
        <?php
            foreach ($this->data['Good'] as $chiave => $oggetto)
            {
                $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
                $chiave == 0 ? $classe1 = "originale" : $classe1 = '';

                if($oggetto['quantita'] == null && $oggetto['prezzo'] == null && $oggetto['iva_id'] == null)
                {
                    $isNote = 'display:none' ;
                    $isDescriptionNote = ' col-md-12';
                }
                else
                {
                    $isNote = 'display:block';
                    $isDescriptionNote = 'col-md-3';
                }
            ?>
          <div class="principale clonableRow lunghezza contacts_row <?= $classe1 ?> <?= $classe ?>" id="'<?= $chiave ?>'">
            <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
            <?= $this->Form->input("Good.$chiave.id"); ?>
            <div class="col-md-12">
            <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '0'; ?>
            <?php if(ADVANCED_STORAGE_ENABLED): ?>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                <?php
                if(isset($oggetto['Storage']['movable']) )
                {
                echo $this->Form->input("Good.$chiave.tipo", ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true,'value'=>$oggetto['Storage']['movable'],'disabled'=>true]);
                }
                else
                {
                echo $this->Form->input("Good.$chiave.tipo", ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true]);
                }
                ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                <?=  $this->Form->input("Good.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control  jsCodice']); ?>
                </div>
            <?php else: ?>
                <?= $this->Form->hidden("Good.$chiave.tipo", ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]); ?>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                <label class="form-margin-top"><strong>Codice</strong></label>
                    <?= $this->Form->input("Good.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control jsCodice']); ?>
                </div>
            <?php endif; ?>
                <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                    <label class="form-margin-top"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input("Good.$chiave.oggetto", [ 'div' => false, 'label' => false, 'class' => 'form-control  jsDescription' ,'pattern'=>PATTERNBASICLATIN]); ?>
                    <?= $this->Form->hidden("Good.$chiave.storage_id"); ?>
                    <?= $this->Form->hidden("Good.$chiave.movable",['class'=>'jsMovable','value'=>$movableValue]); ?>
                </div>
                <div class="col-md-3 jsRowField" style="<?= $isNote ?>">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input("Good.$chiave.customdescription", ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                    <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input("Good.$chiave.quantita", ['label' => false,'class' => 'form-control jsQuantity','prodotto' => $oggetto['storage_id'],'div' => false, 'step'=> 0.01]); ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                    <label class="form-margin-top"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input("Good.$chiave.unita", ['div' => false,'label' => false,'class' => 'form-control', 'empty'=>true,'type'=>'select','options'=>$units]); ?>
                </div>
                <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                    <label class="form-label form-margin-top"><strong>Costo</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input("Good.$chiave.prezzo", ['label' => false,'class' => 'form-control jsPrice ' . $oggetto['storage_id']]); ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                    <label class="form-margin-top form-label"><strong>Importo</strong></label>
                    <?= $this->Form->input("Good.$chiave.importo", ['label' => false,'class' => 'jsImporto','class' => 'form-control jsImporto' ,'disabled'=>true, 'value'=> number_format($oggetto['quantita'] * $oggetto['prezzo'],2,',','')]); ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                    <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                    <?= $this->Form->input("Good.$chiave.discount", ['label' => false,'class' => 'form-control','div'=> false,'max'=>100]); ?>
                </div>
                <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                    <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input("Good.$chiave.iva_id", ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats, 'empty'=>true]); ?>
                </div>
                <div class="row" style="padding-bottom:10px;"><hr></div>
            </div>
        </div>
        <?php
            $prezzo_beni_riga = '';
            $prezzo_beni_riga = $oggetto['prezzo'] *  $oggetto['quantita'] * (1-$oggetto['discount']/100);
            $totale_imponibile += $prezzo_beni_riga;

            if(isset($oggetto['Iva']['percentuale'])){ $totale_imposta += ($prezzo_beni_riga / 100) *  $oggetto['Iva']['percentuale']; }
        ?>
        <script>
            loadSupplierCatalog(articoli,"Good","<?php echo $chiave; ?>",'fatture',codici);
        </script>
             <?php
            }
         ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>
</div>

<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexExtendedBuy']); ?></center>

<?= $this->Form->end(); ?>

<?= $this->element('Js/datepickercode'); ?>

<script>
    // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
    // Controllo che non siano ste inserite più di 5 aliquote iva

    function getVat(row){return $(row).find(".jsVat").val();}
    function getPrice(row){ return $(row).find(".jsPrice").val(); }
    function getQuantity(row){ return $(row).find(".jsQuantity").val(); }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function(){setImporto(this); });
    $(".jsQuantity").change(function(){setImporto(this);});
    $(".jsTipo").change(function(){setCodeRequired(this);});

    // Fine codice per gestione prezzo/quantity/tipo

    $("#BillEditExtendedBuyForm").on('submit.default',function(ev) {});

    $("#BillEditExtendedBuyForm").on('submit.validation',
        function(ev)
        {
            ev.preventDefault(); // to stop the form from submitting
            /* Validations go here */

            var arrayOfVat = [];
            var errore = 0;
            $(".lunghezza").each( // clonablero
                function()
                {
                    var iva = getVat(this);
                    var prezzo = getPrice(this);
                    var quantita = getQuantity(this);

                    if(arrayOfVat.indexOf(iva) === -1)
                        arrayOfVat.push(iva);

                    if(quantita != '' && quantita !== undefined) { quantita = true; } else { quantita = false; }
                    if(prezzo != '' && prezzo !== undefined) { prezzo = true; } else { prezzo = false; }
                    if(iva !== undefined && iva != '') {iva = true; } else { iva = false; }

                    if((quantita && prezzo && iva) || (!quantita && !prezzo && !iva))
                    {
                    }
                    else if(iva && (quantita == false || prezzo == false))
                    {
                        errore = 1;
                    }
                    else if(iva == false && (quantita == true || prezzo == true))
                    {
                        errore = 2;
                    }
                    else
                    {
                      /* Not Possible */
                    }

                    if(arrayOfVat.length > 5)
                    {
                        errore = 3;
                    }
                }
            );

            var percentuale = $("#BillWelfareBoxPercentage").val();
            var iva = $("#BillWelfareBoxVatId").val();
            var ritenuta =$("#BillWelfareBoxWithholdingAppliance").val();

            if((iva == ''  && percentuale == '' && ritenuta == '') || (iva != ''  && percentuale != '' && ritenuta != ''))
            {
            }
            else
            {
                errore = 99;
            }

            checkBillDuplicate(errore)
        }
    );

    function checkBillDuplicate(errore)
    {
        var errore = errore;

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "checkBillDuplicate"]) ?>",
            data:
            {
                billnumber : $("#BillNumeroFattura").val(),
                date : $("#datafattura").val(),
                type : 2,
                supplierId : $("#supplier_id_multiple_select").val(),
            },
            success: function(data)
            {
                if(data > 0 && ( $("#BillNumeroFattura").val() != '<?= $this->data['Bill']['numero_fattura']; ?>'))
                {
                    errore = 4;
                }

                switch (errore)
                {
                   case 0:
                        $("#BillEditExtendedBuyForm").trigger('submit.default');
                   break;
                   case 1:
                       $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura d\'acquisto',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 2:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura d\'acquisto',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 3:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura d\'acquisto',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 4:
                       $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica fattura d\'acquisto',
                            content: 'Attenzione esiste già una fattura con lo stesso numero nell\'anno di competenza per il sezionale selezionato.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 99:
                         $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura d\'acquisto',
                            content: 'Attenzione sono stati compilati solo parzialmente i campi della cassa previdenziale. Se si vuole compilare la cassa previdenziale, tutti i campi devono essere compilati. In caso contrario devono essere tutti lasciati vuoti.',
                              type: 'orange',
                          });
                   break;
                }
            },
            error: function(data)
            {
            }
        });
    }
</script>

<?= $this->element('Js/checkifarenewarticle'); ?>

<script>
    if($('#BillCollectionfees').val() > 0)
    {
        showCollectionfeesVat($('#BillCollectionfees').val());
    }

    $('#BillCollectionfees').change(function()
    {
        showCollectionfeesVat($(this).val());
    })

    function showCollectionfeesVat(valueOfCollectionFee)
    {
        if(valueOfCollectionFee > 0)
        {
            $('.jsAliquota').show();
            $('#BillCollectionfeesvatid').attr('required',true);
        }
        else
        {
            $('.jsAliquota').hide();
            $('#BillCollectionfeesvatid').attr('required',false);
        }
    }
</script>
