<?php
echo '<?xml version="1.0" encoding="UTF-8"?>' ;
echo '<?xml-stylesheet type="text/xsl" href="fatturapa_v1.2.xsl"?>';
$flagOmaggio = false;
if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){

}
?>
<p:FatturaElettronica versione="<?= $bill['Client']['pa'] == 1 ? 'FPA12' : 'FPR12'; ?>"
                      xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                      xmlns:p="http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <FatturaElettronicaHeader>
        <DatiTrasmissione>
            <IdTrasmittente>
                <IdPaese><?= $settings['Setting']['transmitter_nationality_code'] ?></IdPaese>
                <IdCodice><?=  $settings['Setting']['transmitter_fiscal_identification'] ?></IdCodice>
            </IdTrasmittente>
            <ProgressivoInvio><?=  $bill['Bill']['numero_fattura']. $suffix ?></ProgressivoInvio>
            <FormatoTrasmissione><?= $bill['Client']['pa'] == 1 ? 'FPA12' : 'FPR12'; ?></FormatoTrasmissione>
            <CodiceDestinatario><?= $bill['Client']['codiceDestinatario'] ?></CodiceDestinatario>
            <ContattiTrasmittente>
                <?= isset($settings['Setting']['tel']) && $settings['Setting']['tel'] != '' ? '<Telefono>'.str_replace(" ","",$settings['Setting']['tel']).'</Telefono>' : null; ?>
                <?= isset($settings['Setting']['email']) && $settings['Setting']['email'] != '' ? '<Email>'.$settings['Setting']['email'].'</Email>' : null; ?>
            </ContattiTrasmittente>
            <?php if($bill['Client']['codiceDestinatario'] == '0000000'){
                if(isset($bill['Client']['pec']) &&  $bill['Client']['pec'] != '') { ?>
                    <PECDestinatario><?= $bill['Client']['pec'] ?></PECDestinatario>
                <?php } } ?>
        </DatiTrasmissione>
        <CedentePrestatore>
            <DatiAnagrafici>
                <IdFiscaleIVA>
                    <IdPaese><?= $settings['Setting']['provider_nationality_code'] ?></IdPaese>
                    <IdCodice><?= $settings['Setting']['provider_fiscal_identification'] ?></IdCodice>
                </IdFiscaleIVA>
                <?= (isset($settings['Setting']['cf']) && $settings['Setting']['cf'] != '') ? '<CodiceFiscale>' . $settings['Setting']['cf'] . '</CodiceFiscale>': null ?>
                <Anagrafica>
                    <Denominazione><?= str_replace('<', '&lt;',str_replace('&', '&amp;',$settings['Setting']['name']));  ?></Denominazione>
                </Anagrafica>
                <RegimeFiscale><?= $settings['Setting']['fiscal_regime']  ?></RegimeFiscale>
            </DatiAnagrafici>
            <Sede>
                <Indirizzo><?=  str_replace("’", "&#39", str_replace("'", "&#39", str_replace('<', '&lt;',str_replace('&', '&amp;',$bill['Bill']['client_address'])))); ?></Indirizzo>
                <CAP><?= $settings['Setting']['cap']?></CAP>
                <Comune><?= str_replace("'","&#39",$settings['Setting']['citta'])?></Comune>
                <Provincia><?= strtoupper($settings['Setting']['prov']) ?></Provincia>
                <Nazione>IT</Nazione>
            </Sede>
            <?php if($settings['Setting']['rea_registered']) { ?>
                <IscrizioneREA>
                    <Ufficio><?= strtoupper($settings['Setting']['rea_province'])  ?></Ufficio>
                    <NumeroREA><?= $settings['Setting']['rea_number']  ?></NumeroREA>
                    <?= $settings['Setting']['share_capital'] > 0 ? '<CapitaleSociale>'. $settings['Setting']['share_capital'] .'</CapitaleSociale>' : null ?>
                    <?= (isset($settings['Setting']['partner_unique']) && $settings['Setting']['partner_unique'] != '') ? '<SocioUnico>'. $settings['Setting']['partner_unique'] . '</SocioUnico>' : null; ?>
                    <StatoLiquidazione><?= $settings['Setting']['closure_state']  ?></StatoLiquidazione>
                </IscrizioneREA>
            <?php } ?>
            <Contatti>
                <?= (isset($settings['Setting']['tel']) && $settings['Setting']['tel'] != '') ? '<Telefono>'.  str_replace(" ","",$settings['Setting']['tel']). '</Telefono>' : null ?>
                <?= (isset($settings['Setting']['fax']) && $settings['Setting']['fax'] != '') ? '<Fax>'.  str_replace(" ","",$settings['Setting']['fax']) . '</Fax>' : null  ?>
                <?= (isset($settings['Setting']['email']) && $settings['Setting']['email'] != '') ? '<Email>' .  $settings['Setting']['email'] .'</Email>' : null ?>
            </Contatti>
            <?= (isset($bill['Client']['ref_adm_company']) && $bill['Client']['ref_adm_company']) != ''? '<RiferimentoAmministrazione>'.$bill['Client']['ref_adm_company'].'</RiferimentoAmministrazione>' : null ?>
        </CedentePrestatore>
        <CessionarioCommittente>
            <DatiAnagrafici>
                <?php
                if($bill['Client']['piva'] != '')
                {
                    echo '<IdFiscaleIVA><IdPaese>'.$clientnationshortcode.'</IdPaese><IdCodice>' . $bill['Client']['piva'] . '</IdCodice></IdFiscaleIVA>';
                }
                if($bill['Client']['cf'] != '')
                {
                    echo '<CodiceFiscale>' . $bill['Client']['cf'] . '</CodiceFiscale>'	;
                }
                ?>
                <Anagrafica>
                    <Denominazione><?=  str_replace("'","&#39;",str_replace('<', '&lt;',str_replace('&', '&amp;',$bill['Client']['ragionesociale']))); ?></Denominazione>
                </Anagrafica>
            </DatiAnagrafici>
            <Sede>
                <Indirizzo><?=  str_replace("'", "&#39", str_replace('<', '&lt;',str_replace('&', '&amp;',$bill['Bill']['client_address']))); ?></Indirizzo>
                <CAP><?=  $bill['Bill']['client_cap'] ?></CAP>
                <Comune><?=  str_replace("'", "&#39", $bill['Bill']['client_city']) ?></Comune>
                <?= (isset($bill['Bill']['client_province']) && $bill['Bill']['client_province'] != '' ) ? '<Provincia>'. strtoupper($bill['Bill']['client_province']) .'</Provincia>' : null; ?>
                <Nazione><?= $clientnationshortcode ?></Nazione>
            </Sede>
        </CessionarioCommittente>
    </FatturaElettronicaHeader>
    <FatturaElettronicaBody>
        <DatiGenerali>
            <DatiGeneraliDocumento>
                <?php
                switch($bill['Bill']['tipologia'])
                {
                    case 1:
                        if($bill['Bill']['debitnote'] == 1)
                        {
                            $tipologia = 'TD05';
                        }
                        else
                        {
                            $tipologia = 'TD01';
                        }
                        break;
                    case 3:
                        $tipologia = 'TD04';
                        break;
                }

                if($bill['Bill']['typeofdocument'] != null)
                    $tipologia = $bill['Bill']['typeofdocument'];
                ?>
                <TipoDocumento><?= $tipologia; ?></TipoDocumento>
                <Divisa><?= $currencyCode ?></Divisa>
                <Data><?= $bill['Bill']['date']?></Data>
                <Numero><?= $bill['Bill']['numero_fattura'] . $suffix ?></Numero>
                <?php
                if($bill['Bill']['withholding_tax'] > 0)
                {
                    ?>
                    <DatiRitenuta>
                        <!--<TipoRitenuta><?=  $settings['Setting']['type_of_person'] ?></TipoRitenuta>-->
                        <TipoRitenuta><?=  $settings['Einvoicetypeofperson']['code'] ?></TipoRitenuta>
                        <ImportoRitenuta><?= number_format($importoRitenuta * $bill['Bill']['changevalue'] ,2,'.',''); ?></ImportoRitenuta>
                        <AliquotaRitenuta><?= number_format( $bill['Bill']['withholding_tax'] ,2,'.',''); ?></AliquotaRitenuta>
                        <CausalePagamento><?= $settings['Einvoicewithholdingtaxcausal']['code']; ?></CausalePagamento>
                    </DatiRitenuta>
                    <?php
                }
                ?>
                <?php
                if($bill['Bill']['seal'] > 0)
                {
                    ?>
                    <DatiBollo>
                        <BolloVirtuale><?= 'SI' ?></BolloVirtuale>
                        <ImportoBollo><?= number_format($bill['Bill']['seal'] * $bill['Bill']['changevalue'],2,'.',''); ?></ImportoBollo>
                    </DatiBollo>
                    <?php
                }

                if($bill['Bill']['welfare_box_code'] != '' )
                { ?>
                    <DatiCassaPrevidenziale>
                        <TipoCassa><?= $bill['Bill']['welfare_box_code'] ?></TipoCassa>
                        <AlCassa><?= $bill['Bill']['welfare_box_percentage'] ? number_format($bill['Bill']['welfare_box_percentage'],2,'.','') : '0.00';  ?></AlCassa>
                        <ImportoContributoCassa><?= number_format($welfareBox,2,'.',''); ?></ImportoContributoCassa>
                        <ImponibileCassa><?= number_format($welfareBoxTaxable,2,'.',''); ?></ImponibileCassa>
                        <AliquotaIVA><?= $welfareBoxVatPercentage != null ? number_format($welfareBoxVatPercentage,2,'.','') : null; ?></AliquotaIVA>
                        <?php if($bill['Bill']['welfare_box_withholding_appliance'] > 0 && $bill['Bill']['withholding_tax'] > 0 ) { ?>
                            <Ritenuta><?= 'SI' ?></Ritenuta>
                        <?php } ?>
                        <?=  $welfareBoxVatPercentage === '0' || $welfareBoxVatPercentage === '0.00' || $welfareBoxVatPercentage === 0 || $welfareBoxVatPercentage === 0.00 ?  '<Natura>'.  $welfareBoxVatNature .'</Natura>' : null; ?>
                        <?= (isset($bill['Client']['ref_adm_welfarebox']) && $bill['Client']['ref_adm_welfarebox']) ? '<RiferimentoAmministrazione>'.$bill['Client']['ref_adm_welfarebox'].'</RiferimentoAmministrazione>' : null ?>
                    </DatiCassaPrevidenziale>
                <?php }
                if($_SESSION['Auth']['User']['dbname'] == "login_GE0046"){
                    $importoTotaleFattura = number_format($importoTotaleFattura, 2, '.', '');
                    $totaleImponibile = $totaleIva = 0;
                    foreach($vatDetails as $vat)
                    {
                        $totaleImponibile += number_format($vat['imponibile'],2,'.','');
                        $totaleIva += number_format($vat['vat'],2,'.','');
                    }
                    $totaleImponibile = $totaleImponibile - ($totaleImponibile * $bill['Bill']['discount'] / 100);
                    $totaleIva = $totaleIva - ($totaleIva * $bill['Bill']['discount'] / 100);
                    $importoTotaleFattura = $totaleImponibile + $totaleIva;
                }
                else{
                    $totaleImponibile = $totaleIva = 0;
                    foreach($vatDetails as $vat)
                    {
                        $totaleImponibile += number_format($vat['imponibile'],2,'.','');
                        $totaleIva += number_format($vat['vat'],2,'.','');
                    }

                    $importoTotaleFattura = number_format($totaleImponibile + $totaleIva + $rounding,2,'.','');
                }

                ?>
                <ImportoTotaleDocumento><?= number_format($importoTotaleFattura, 2, '.', '') ?></ImportoTotaleDocumento>
                <?= (isset($rounding) && $rounding != null) ? '<Arrotondamento>'.$rounding.'</Arrotondamento>' : null; ?>
                <?php if($_SESSION['Auth']['User']['dbname'] != "login_GE0046"){ ?>
                <?= (isset($bill['Bill']['document_causal']) && $bill['Bill']['document_causal'] != '') ? '<Causale>'. $bill['Bill']['document_causal'].'</Causale>' : null; ?>
                <?php } ?>
            </DatiGeneraliDocumento>
            <?php if(isset($bill['Bill']['cig_type']) && $bill['Bill']['cig_type'] != null )
            { ?>
                <?php
                switch($bill['Bill']['cig_type'])
                {
                    case 0: echo '<DatiOrdineAcquisto>'; break;
                    case 1: echo '<DatiContratto>'; break;
                    case 2: echo '<DatiConvenzione>'; break;
                    case 3: echo '<DatiFattureCollegate>'; break;
                }
                ?>
                <RiferimentoNumeroLinea>1</RiferimentoNumeroLinea>
                <?= $bill['Bill']['cig_documentid'] != null && $bill['Bill']['cig_documentid'] != '' ? '<IdDocumento>'. $bill['Bill']['cig_documentid'].'</IdDocumento>' : null; ?>
                <?= ($bill['Bill']['cig_date'] != null && $bill['Bill']['cig_date'] != '1970-01-01') ?  '<Data>'. $bill['Bill']['cig_date'] . '</Data>' : null; ?>
                <?= $bill['Bill']['cig_num_item'] != null ? '<NumItem>' . $bill['Bill']['cig_num_item'] . '</NumItem>': null  ?>
                <?= $bill['Bill']['cig_codice_commessa'] != null ? '<CodiceCommessaConvenzione>' . $bill['Bill']['cig_codice_commessa'] . '</CodiceCommessaConvenzione>': null  ?>
                <?= $bill['Bill']['cup'] != null ? '<CodiceCUP>' . $bill['Bill']['cup'] . '</CodiceCUP>' : null ?>
                <?= $bill['Bill']['cig'] != null ? '<CodiceCIG>' . $bill['Bill']['cig'] . '</CodiceCIG>': null  ?>
                <?php
                switch($bill['Bill']['cig_type'])
                {
                    case 0: echo '</DatiOrdineAcquisto>'; break;
                    case 1: echo '</DatiContratto>'; break;
                    case 2: echo '</DatiConvenzione>'; break;
                    case 3: echo '</DatiFattureCollegate>'; break;
                }
                ?>
            <?php } ?>
            <?php
            if($bill['Bill']['tipologia'] == 3 && isset($bill['Bill']['bill_referred_number']) && $bill['Bill']['bill_referred_number'] != '' && isset($bill['Bill']['bill_referred_date']) && $bill['Bill']['bill_referred_date'] != '' )
            {
                ?>
                <DatiFattureCollegate>
                    <IdDocumento><?= $bill['Bill']['bill_referred_number'] ?></IdDocumento>
                    <Data><?= $bill['Bill']['bill_referred_date'] ?></Data>
                </DatiFattureCollegate>
            <?php }
            ?>
            <?php if($bill['Bill']['accompagnatoria'] == 0)
            {
                if(isset($bill['Bill']['mainrifddtnumber']) && isset($bill['Bill']['mainrifddtdate']) && $bill['Bill']['mainrifddtnumber'] != null && $bill['Bill']['mainrifddtdate'] != null)
                {
                    ?>	<DatiDDT>
                    <NumeroDDT><?= $bill['Bill']['mainrifddtnumber'] ?></NumeroDDT>
                    <DataDDT><?= $bill['Bill']['mainrifddtdate'] ?></DataDDT>
                </DatiDDT>
                    <?php
                }

                foreach ($arrayOfTransportReference as $referenceDdt)
                {
                    if(isset($referenceDdt['number']))
                    {
                        ?>
                        <DatiDDT>
                            <NumeroDDT><?= $referenceDdt['number'] ?></NumeroDDT>
                            <DataDDT><?= $referenceDdt['date'] ?></DataDDT>
                            <?php
                            $lineArray = explode('|',  $referenceDdt['rows']);
                            foreach($lineArray as $line)
                            {
                                ?>
                                <RiferimentoNumeroLinea><?= $line ?></RiferimentoNumeroLinea>
                                <?php
                            }
                            ?>
                        </DatiDDT>
                        <?php
                    }
                }
            } ?>
            <?php if($bill['Bill']['accompagnatoria'] == 1)
            { ?>
                <DatiTrasporto>
                    <?php if($bill['Bill']['transporter'] == 2) {
                        if(isset($bill['Carrier']['Nation']['shortcode']) && $bill['Carrier']['Nation']['shortcode'] != '' && isset($bill['Carrier']['piva']) && $bill['Carrier']['piva'] != '') {
                            ?>
                            <DatiAnagraficiVettore>
                                <IdFiscaleIVA>
                                    <IdPaese><?= $bill['Carrier']['Nation']['shortcode']; ?></IdPaese>
                                    <IdCodice><?= $bill['Carrier']['piva']; ?></IdCodice>
                                </IdFiscaleIVA>
                                <?= isset($bill['Carrier']['cf']) && $bill['Carrier']['cf'] != '' ?  '<CodiceFiscale>'.$bill['Carrier']['cf'].'</CodiceFiscale>' : null ; ?>
                                <?php if((isset($bill['Carrier']['name']) && $bill['Carrier']['name'] != '' ) || (isset($bill['Carrier']['eori_code']) && $bill['Carrier']['eori_code'] != '' )) { ?>
                                    <Anagrafica>
                                        <?= isset($bill['Carrier']['name']) && $bill['Carrier']['name'] != '' ? '<Denominazione>'.str_replace('<', '&lt;',str_replace('&', '&amp;',$bill['Carrier']['name'])). '</Denominazione>' : null; ?>
                                        <?= isset($bill['Carrier']['eori_code']) && $bill['Carrier']['eori_code'] != '' ? '<CodEORI>'.$bill['Carrier']['eori_code'].'</CodEORI>' : null; ?>
                                    </Anagrafica>
                                <?php } ?>
                            </DatiAnagraficiVettore>
                        <?php }
                    } ?>
                    <?= (isset($bill['Bill']['means_of_transport']) && $bill['Bill']['means_of_transport'] != '') ? '<MezzoTrasporto>'.$bill['Bill']['num_colli'].'</MezzoTrasporto>' : null; ?>
                    <?= $billCausal != '' ? '<CausaleTrasporto>'.$billCausal.'</CausaleTrasporto>' : null; ?>
                    <?= (isset($bill['Bill']['num_colli']) && $bill['Bill']['num_colli'] != '') ? '<NumeroColli>'.$bill['Bill']['num_colli'].'</NumeroColli>' : null; ?>
                    <?= (isset($bill['Bill']['aspect']) && $bill['Bill']['aspect'] != '') ? '<Descrizione>'.$bill['Bill']['aspect'].'</Descrizione>' : null; ?>
                    <UnitaMisuraPeso>Kg</UnitaMisuraPeso>
                    <?= (isset($bill['Bill']['weight']) && $bill['Bill']['weight'] != '') ? '<PesoLordo>'.number_format($bill['Bill']['weight'],2,'.','').'</PesoLordo>' : null ?>
                    <?= (isset($bill['Bill']['bill_net_weight']) && $bill['Bill']['bill_net_weight'] != '') ? '<PesoNetto>'.number_format($bill['Bill']['bill_net_weight'],2,'.','').'</PesoNetto>' : null ?>
                    <?= (isset($bill['Bill']['billaccdate']) && $bill['Bill']['billaccdate'] != null) ? '<DataInizioTrasporto>'.$bill['Bill']['billaccdate'].'</DataInizioTrasporto>' : null ?>
                </DatiTrasporto>
            <?php } ?>
        </DatiGenerali>
        <DatiBeniServizi>
            <?php $i=1;
            foreach($bill['Good'] as $numero => $oggetto)
            {
                if(isset($oggetto['Iva']['percentuale']))
                {
                    if(MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null)
                    {
                        $ivaPrezzo=  $oggetto['currencyprice']  * $oggetto['Iva']['percentuale'] / 100;
                        $prezzoIvato=$oggetto['currencyprice']  + $ivaPrezzo;
                    }
                    else
                    {
                        $ivaPrezzo=  $oggetto['prezzo'] * $oggetto['Iva']['percentuale'] / 100;
                        $prezzoIvato=$oggetto['prezzo'] + $ivaPrezzo;
                    }
                    ?>
                    <DettaglioLinee>
                        <NumeroLinea><?= $i; ?></NumeroLinea>
                        <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0046"){?>
                            <CodiceArticolo>
                                <CodiceTipo>
                                    EAN
                                </CodiceTipo>
                                <CodiceValore>
                                    <?= str_replace("'", "&#39", str_replace('>', '&lt;',str_replace('<', '&lt;', str_replace('&', '&amp;', $oggetto['codice'])))); ?>
                                </CodiceValore>
                            </CodiceArticolo>
                        <?php }?>
                        <Descrizione><?= str_replace('<', '&lt;',str_replace('&', '&amp;',$oggetto['oggetto']. ' '. $oggetto['customdescription'])); ?></Descrizione>
                        <Quantita><?= number_format($oggetto['quantita'],5,'.',''); ?></Quantita>
                        <?= isset($oggetto['Units']['description']) && $oggetto['Units']['description'] != '' ? '<UnitaMisura>'. $oggetto['Units']['description'] . '</UnitaMisura>' : null ; ?>

                        <PrezzoUnitario><?= (MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null) ? $oggetto['currencyprice'] : $oggetto['prezzo']; ?></PrezzoUnitario>

                        <?php
                            if(isset($oggetto['discount']) && $oggetto['discount'] > 0){ ?>
                                <ScontoMaggiorazione>
                                    <Tipo>SC</Tipo>
                                    <Percentuale><?= number_format(($oggetto['discount']),2 , '.', ''); ?></Percentuale>
                                </ScontoMaggiorazione>
                            <?php }
                         ?>
                            <PrezzoTotale><?= (MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null) ? number_format(($oggetto['currencyprice'] * $oggetto['quantita']*(1-$oggetto['discount']/100)),4,'.','') : number_format($oggetto['prezzo'] * $oggetto['quantita']*(1-$oggetto['discount']/100),4,'.',''); ?></PrezzoTotale>
                        <AliquotaIVA><?= number_format($oggetto['Iva']['percentuale'],2,'.','') ;?></AliquotaIVA>
                        <?= $oggetto['Iva']['percentuale'] == 0 ? '<Natura>'. $oggetto['Iva']['Einvoicevatnature']['code'].'</Natura>' : null; ?>
                        <?= (isset($bill['Client']['ref_adm_detailrow'])  && $bill['Client']['ref_adm_detailrow']) ? '<RiferimentoAmministrazione>'.$bill['Client']['ref_adm_detailrow'].'</RiferimentoAmministrazione>' : null ?>
                    </DettaglioLinee>

                    <?php
                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                        if($oggetto['complimentaryQuantity'] > 0){
                            $i++;
                            if($flagOmaggio == false){
                                $flagOmaggio = true;
                                $omaggio['imponibile'] = 0;
                            }
                            $omaggio['imponibile'] += number_format($oggetto['prezzo'] * $oggetto['complimentaryQuantity'],2,'.','') * -1;

                            ?>
                            <DettaglioLinee>
                                <NumeroLinea><?= $i; ?></NumeroLinea>
                                <Descrizione><?= str_replace('<', '&lt;',str_replace('&', '&amp;',$oggetto['oggetto']. ' '. $oggetto['customdescription'])); ?> - OMAGGIO</Descrizione>                                <Quantita><?= number_format($oggetto['complimentaryQuantity'],5,'.',''); ?></Quantita>
                                <?= isset($oggetto['Units']['description']) && $oggetto['Units']['description'] != '' ? '<UnitaMisura>'. $oggetto['Units']['description'] . '</UnitaMisura>' : null ; ?>
                                <PrezzoUnitario>-<?= (MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null) ? $oggetto['currencyprice'] : $oggetto['prezzo']; ?></PrezzoUnitario>
                                <?php if(isset($oggetto['discount']) && $oggetto['discount'] > 0): ?>
                                    <ScontoMaggiorazione>
                                        <Tipo>SC</Tipo>
                                        <Percentuale><?= number_format(($oggetto['discount'] * 100),2 , '.', ''); ?></Percentuale>

                                    </ScontoMaggiorazione>
                                <?php endif; ?>
                                <PrezzoTotale>-<?= (MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null) ? number_format(($oggetto['currencyprice'] * $oggetto['complimentaryQuantity']),4,'.','') : number_format(($oggetto['prezzo'] * $oggetto['complimentaryQuantity']),4,'.',''); ?></PrezzoTotale>
                                <AliquotaIVA>0.00</AliquotaIVA>
                                <?= '<Natura>N2.2</Natura>' ?>
                                <?= (isset($bill['Client']['ref_adm_detailrow'])  && $bill['Client']['ref_adm_detailrow']) ? '<RiferimentoAmministrazione>'.$bill['Client']['ref_adm_detailrow'].'</RiferimentoAmministrazione>' : null ?>
                            </DettaglioLinee>
                            <?php
                        }
                    }
                    $i++;
                }

            }
            if(isset($bill['Bill']['collectionfees']) &&  $bill['Bill']['collectionfees'] > 0 && isset($collectionVat))
            {
                ?>
                <DettaglioLinee>
                    <NumeroLinea><?= $i++; ?></NumeroLinea>
                    <TipoCessionePrestazione>AC</TipoCessionePrestazione>
                    <Descrizione>Spese d'incasso</Descrizione>
                    <PrezzoUnitario><?= number_format($bill['Bill']['collectionfees'],2,'.','') ?></PrezzoUnitario>
                    <PrezzoTotale><?= number_format($bill['Bill']['collectionfees'],4,'.','') ?></PrezzoTotale>
                    <AliquotaIVA><?= number_format($collectionVat['Ivas']['percentuale'],2,'.','') ?></AliquotaIVA>
                    <?= $collectionVat['Ivas']['percentuale'] == 0 ? '<Natura>'. $collectionVatInvoiceNature .'</Natura>' : null; ?>
                </DettaglioLinee>
                <?php
            }
            $foundCollectionFee = 0;
            $totaleVat = 0;
            foreach($vatDetails as $vat)
            {
                if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                    if($vat['percentuale'] == 0){
                        if($vat['codice'] == "N2.2" && $flagOmaggio){ ?>
                            <DatiRiepilogo>
                                <AliquotaIVA>0.00</AliquotaIVA>
                                <Natura>N2.2</Natura>
                                <ImponibileImporto><?= number_format($vatDetails[21]['imponibile'],2,'.',''); ?></ImponibileImporto>
                                <Imposta>0.00</Imposta>
                            </DatiRiepilogo>
                        <?php }else{
                            if($vat['percentuale'] != null)
                            {
                                $totaleVat += $vat['vat'];
                                ?>
                                <DatiRiepilogo>
                                    <AliquotaIVA><?= $vat['percentuale'] ?></AliquotaIVA>
                                    <?= $vat['percentuale'] == 0 ? '<Natura>'. $vat['codice'].'</Natura>' : null; ?>
                                    <ImponibileImporto><?= number_format($vat['imponibile'],2,'.',''); ?></ImponibileImporto>
                                    <Imposta><?= number_format($vat['vat'],2,'.',''); ?></Imposta>
                                    <EsigibilitaIVA><?= $bill['Bill']['einvoicevatesigibility'] ?></EsigibilitaIVA>
                                    <?php
                                    if ($vat['percentuale'] == 0)
                                    {
                                        switch($vat['codice'])
                                        {
                                            case 'N6':
                                                $vatDescription = "Inversione contabile";
                                                break;
                                            case 'N7':
                                                $vatDescription = "Iva assolta in altro stato UE";
                                                break;
                                            default:
                                                $vatDescription = $vat['description'];
                                                break;
                                        }
                                    }
                                    ?>
                                    <?= $vat['percentuale'] == 0 ? '<RiferimentoNormativo>'. $vatDescription .'</RiferimentoNormativo>' : null; ?>
                                </DatiRiepilogo>
                                <?php
                            }
                        }
                    }else{
                        if($vat['percentuale'] != null)
                        {
                            $totaleVat += $vat['vat'];
                            ?>
                            <DatiRiepilogo>
                                <AliquotaIVA><?= $vat['percentuale'] ?></AliquotaIVA>
                                <?= $vat['percentuale'] == 0 ? '<Natura>'. $vat['codice'].'</Natura>' : null; ?>
                                <ImponibileImporto><?= number_format($vat['imponibile'],2,'.',''); ?></ImponibileImporto>
                                <Imposta><?= number_format($vat['vat'],2,'.',''); ?></Imposta>
                                <EsigibilitaIVA><?= $bill['Bill']['einvoicevatesigibility'] ?></EsigibilitaIVA>
                                <?php
                                if ($vat['percentuale'] == 0)
                                {
                                    switch($vat['codice'])
                                    {
                                        case 'N6':
                                            $vatDescription = "Inversione contabile";
                                            break;
                                        case 'N7':
                                            $vatDescription = "Iva assolta in altro stato UE";
                                            break;
                                        default:
                                            $vatDescription = $vat['description'];
                                            break;
                                    }
                                }
                                ?>
                                <?= $vat['percentuale'] == 0 ? '<RiferimentoNormativo>'. $vatDescription .'</RiferimentoNormativo>' : null; ?>
                            </DatiRiepilogo>
                            <?php
                        }
                    }
                }else if ($_SESSION['Auth']['User']['dbname'] == "login_GE0046") {
                    if($vat['percentuale'] != null)
                    {
                        $totaleVat += $vat['vat'];
                        $sconto = ($vat['vat'] * $bill['Bill']['discount'] / 100);
                        $sconti = ($vat['imponibile'] * $bill['Bill']['discount'] / 100);
                        ?>
                        <DatiRiepilogo>
                            <AliquotaIVA><?= $vat['percentuale'] ?></AliquotaIVA>
                            <?= $vat['percentuale'] == 0 ? '<Natura>'. $vat['codice'].'</Natura>' : null; ?>
                            <ImponibileImporto><?= number_format(($vat['imponibile'] - $sconti),2,'.',''); ?></ImponibileImporto>
                            <Imposta><?= number_format((($vat['vat'] - $sconto)),2,'.',''); ?></Imposta>
                            <EsigibilitaIVA><?= $bill['Bill']['einvoicevatesigibility'] ?></EsigibilitaIVA>
                            <?php
                            if ($vat['percentuale'] == 0)
                            {
                                switch($vat['codice'])
                                {
                                    case 'N6':
                                        $vatDescription = "Inversione contabile";
                                        break;
                                    case 'N7':
                                        $vatDescription = "Iva assolta in altro stato UE";
                                        break;
                                    default:
                                        $vatDescription = $vat['description'];
                                        break;
                                }
                            }
                            ?>
                            <?= $vat['percentuale'] == 0 ? '<RiferimentoNormativo>'. $vatDescription .'</RiferimentoNormativo>' : null; ?>
                        </DatiRiepilogo>
                    <?php  }
                }else{
                    if($vat['percentuale'] != null)
                    {
                        $totaleVat += $vat['vat'];
                        ?>
                        <DatiRiepilogo>
                            <AliquotaIVA><?= $vat['percentuale'] ?></AliquotaIVA>
                            <?= $vat['percentuale'] == 0 ? '<Natura>'. $vat['codice'].'</Natura>' : null; ?>
                            <ImponibileImporto><?= number_format($vat['imponibile'],2,'.',''); ?></ImponibileImporto>
                            <Imposta><?= number_format($vat['vat'],2,'.',''); ?></Imposta>
                            <EsigibilitaIVA><?= $bill['Bill']['einvoicevatesigibility'] ?></EsigibilitaIVA>
                            <?php
                            if ($vat['percentuale'] == 0)
                            {
                                switch($vat['codice'])
                                {
                                    case 'N6':
                                        $vatDescription = "Inversione contabile";
                                        break;
                                    case 'N7':
                                        $vatDescription = "Iva assolta in altro stato UE";
                                        break;
                                    default:
                                        $vatDescription = $vat['description'];
                                        break;
                                }
                            }
                            ?>
                            <?= $vat['percentuale'] == 0 ? '<RiferimentoNormativo>'. $vatDescription .'</RiferimentoNormativo>' : null; ?>
                        </DatiRiepilogo>
                    <?php  }
                }
            }
            ?>
        </DatiBeniServizi>
        <?php
        if(MULTICURRENCY)
        {
            // SEAL OLD VS SEAL NEW
            if(strtotime(date("Y-m-d",strtotime($bill['Bill']['date']))) < strtotime(date('2019-01-01')))
            {
                $totaleAPagare = number_format($importoTotaleFattura  - number_format($importoRitenuta * $bill['Bill']['changevalue'] ,2,'.','') +   number_format($bill['Bill']['seal'] * $bill['Bill']['changevalue'],2,'.',''),2,'.','');
            }
            else
            {
                $totaleAPagare = number_format($importoTotaleFattura  - number_format($importoRitenuta * $bill['Bill']['changevalue'] ,2,'.',''),2,'.','');
            }
        }
        else
        {
            // SEAL OLD VS SEAL NEW
            if(strtotime(date("Y-m-d",strtotime($bill['Bill']['date']))) < strtotime(date('2019-01-01')))
            {
                $totaleAPagare = number_format($importoTotaleFattura  - number_format($importoRitenuta,2,'.','') +   number_format($bill['Bill']['seal'],2,'.',''),2,'.','');
            }
            else
            {
                $totaleAPagare = number_format($importoTotaleFattura  - number_format($importoRitenuta,2,'.',''),2,'.','');
            }
        }

        $totaleAPagare = $totaleAPagare ;

        $totaleScadenze = 0;
        foreach($deadlines as $deadline)
        {
            $totaleScadenze += number_format($deadline['Deadlines']['amount'],2,'.','');
        }
        if( $bill['Bill']['einvoicevatesigibility'] == 'S')
        {
            $difference = $totaleAPagare - $totaleScadenze - $totaleVat;
        }
        else {
            $difference = $totaleAPagare - $totaleScadenze;
        }
        $i = 0;
        foreach($deadlines as $key => $deadline)
        {
            if(++$i === count($deadlines)){ $arrotondamento = $difference; } else { $arrotondamento = 0; }
            ?>
            <DatiPagamento>
                <CondizioniPagamento><?= $bill['Payment']['Einvoicepaymenttype']['code'] ?></CondizioniPagamento>
                <DettaglioPagamento>
                    <ModalitaPagamento><?= $bill['Payment']['Einvoicepaymentmethod']['code'] ?></ModalitaPagamento>
                    <DataScadenzaPagamento><?= $deadline['Deadlines']['deadline']; ?></DataScadenzaPagamento>
                    <ImportoPagamento><?= number_format($deadline['Deadlines']['amount']  + $arrotondamento ,2,'.',''); ?></ImportoPagamento>
                    <?= isset($bill['Payment']['Bank']['iban']) && $bill['Payment']['Bank']['iban'] != '' ? '<IBAN>' . $bill['Payment']['Bank']['iban'] . '</IBAN>' : null ?>
                </DettaglioPagamento>
            </DatiPagamento>
        <?php } ?>
    </FatturaElettronicaBody>
</p:FatturaElettronica>
