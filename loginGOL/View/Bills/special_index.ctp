 <style>
        .impostazioni{
            position: absolute;
            top: 10%;
            right: 10%;
        }
        .bottoni{
                width:40%;
                height:20%;
                border-radius: 25px !important;
                text-align:center;
                vertical-align: center;
                padding: 70px 0;
                line-height:1.1em;
                font-size:1.1em;
                background-color: #3d5afe;
                color: white;
        }
        .page{
              padding-top: 10%;
              padding-left: 35%;
              text-align: center;
        }
    </style>
<div class="page">
    <div class="bottoni portlet-title">
        <div class="caption">
            <?php echo $this->Html->link("Scarica Fatture Vendita", array('controller' => 'bills','action'=> 'sentbillimport', ), array( 'style'=>'color: white', 'class' => '')); ?>

        </div>
    </div>
    <br>
    </br>
    <div class="bottoni portlet-title">
        <div class="caption">
            <?php echo $this->Html->link("Scarica Fatture Acquisto", array('controller' => 'bills','action'=> 'ebillimport', ), array( 'style'=>'color: white', 'class' => '')); ?>

        </div>
    </div>
</div>
<div class="impostazioni">
    <?php echo $this->Html->link('<i class="fa fa-cog" style="font-size:50px;margin-left:3px;color:black;margin-top:4px;"></i>', ['controller'=>'settings', 'action' => 'edit'], ['title' => __('Impostazioni ixfe'), 'escape' => false]); ?>
</div>
</html>
