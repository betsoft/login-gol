BETON DUCA SNC di Duca Ermete & C.<br>
Strada Comunale di Campagna, 38 - 23017 MORBEGNO SO<br>
C.F. 00642560148
<br>
<br>
<p align="center"><h1>Stampa Fatture del protocollo</h1><br><h2>Dal <?= $dataInizio?> al <?= $dataFine ?></h2></p>
<table style="width:200mm;border: 2px solid; border-collapse: collapse"  >
    <tr style="border: 2px solid">
        <th>N. Doc.</th>
        <th>Data</th>
        <th>Cliente</th>
        <th>&nbsp;Tot. Merce</th>
        <th>&nbsp;Importo IVA</th>
        <th>Totale</th>
        <th>C. Pag.</th>
    </tr>

    <?php
    foreach( $definitivo as $riga) {

        ?>
        <tr >
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['nDoc'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['date'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['cliente'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= number_format($riga['merce'], 2, ",", ".") ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= number_format($riga['iva'], 2, ",", ".")?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= number_format($riga['totale'], 2, ",", ".") ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['metodo'] ?></td>
        </tr>
        <?php
    }
    ?>
    <tr style="border: 2px solid">
        <td colspan="3">Totale Protocollo</td>
        <td><?= $merce?></td>
        <td><?= $iva ?></td>
        <td><?= $totale ?></td>
        <td></td>
    </tr>
</table>


