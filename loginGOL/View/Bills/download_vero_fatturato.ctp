BETON DUCA SNC di Duca Ermete & C.<br>
Strada Comunale di Campagna, 38 - 23017 MORBEGNO SO<br>
C.F. 00642560148
<br>
<br>
<p align="center"><h1>Fatturato Clienti per l'anno <?= $year?></h1></p>
<table style="width:200mm;border: 2px solid; border-collapse: collapse"  >
    <tr style="border: 2px solid">
        <th>Codice</th>
        <th>Ragione Sociale</th>
        <th>Pr</th>
        <th>&nbsp;Città</th>
        <th>&nbsp;Imponibile</th>
        <th>&nbsp;%</th>
    </tr>

    <?php
    foreach( $definitivo as $riga) {

        ?>
        <tr >
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['codice'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['cliente'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['provincia'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['citta'] ?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= number_format($riga['imponibile'], 2, ",", ".")?></td>
            <td style="padding: 5px;border-right: 2px solid"><?= $riga['percentuale'] ?></td>
        </tr>
        <?php
    }
    ?>

</table>
<p align="right">Totale Fatturato: <?= number_format(fatturato, 2, ",", ".") ?></p>

