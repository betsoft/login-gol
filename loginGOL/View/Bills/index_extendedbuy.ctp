<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?= $this->element('Form/formelements/indextitle',['indextitle'=>'Fatture d\'acquisto','indexelements' => ['resumecategorizebuy'=>'resumecategorizebuy','resumevatbuy'=>'Registro IVA acquisto','importebill'=>'Importazione fatture elettroniche','addExtendedbuy'=>'Nuova fattura d\'acquisto'],'xlsLink'=>'indexExtendedbuy']); ?>
<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
	<thead class ="flip-content">
        <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
        <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
                'htmlElements' => [
                    '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="'.$startDate.'"  bind-filter-event="change"/>'.''.
                    '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="'.$endDate.'" bind-filter-event="change"/></center>'
                ]
            ]);  ?>
        </tr>
    </thead>
    <tbody class="ajax-filter-table-content">
    <?php if (count($bills) == 0): ?>
        <tr><td colspan="8"><center>nessuna fattura trovata</center></td></tr>
    <?php else: ?>
    <?php
        $totaleImponibile = $totale_fatturato = $totaleIvato = $totWithholdingtax = 0;
		foreach ($bills as $bill)
		{ 
			switch($bill['Bill']['typeofdocument'])
			{
				case 'TD04':
					$coefficienteMoltiplicativo = -1;
				break;
				default:
					$coefficienteMoltiplicativo = 1;
				break;
			}
		?>
        <tr>
            <td><?= $bill['Bill']['numero_fattura']; ?> </td>
            <td><?= $this->Time->format('d-m-Y', $bill['Bill']['date']); ?></td>
            <td class="table-max-width uk-text-truncate"><?= $bill['Supplier']['name'] ; ?></td>
		    <td style="text-align:right">
			<?php
			if($bill['Bill']['imported_bill'] == 1)
			{
                $imponibile = 0.00;
                foreach($Utilities->getBillGestionalData($bill['Bill']['id']) as $row)
                {
                    if($row['Billgestionaldata']['imponibileimporto'] >= 0)
				 	    $imponibile += $coefficienteMoltiplicativo * $row['Billgestionaldata']['imponibileimporto'];
                    else
                        $imponibile +=  $row['Billgestionaldata']['imponibileimporto'];
                }
                $totaleImponibile += $imponibile;
                echo number_format($imponibile,2,',','.');
			}
			else
			{
				$totalWelfareBox = $Utilities->getWelfareBoxTotal($bill['Bill']['id']) ;
				$taxableIncome = $Utilities->getBillTaxable($bill['Bill']['id']);
				$taxableIncome += $totalWelfareBox;
				$totaleImponibile += $taxableIncome ;
				echo number_format($taxableIncome,2,',','.');
			}
			?>
            </td>
            <td style="text-align:right">
			<?php
			if($bill['Bill']['imported_bill'] == 1)
			{
                if($bill['Bill']['importo'] >= 0)
                {
                    $totaleIvato += $coefficienteMoltiplicativo * $bill['Bill']['importo'];
                    echo number_format($coefficienteMoltiplicativo * $bill['Bill']['importo'],2,',','');
                }
                else
                {
                    $totaleIvato += $bill['Bill']['importo'];
                    echo number_format($bill['Bill']['importo'],2,',','');
                }
			}
			else
			{
				$ivato = $Utilities->getBillTotal($bill['Bill']['id']);
				$ivato += $totalWelfareBox; // Aggiungo la cassa
				$bill['Bill']['welfare_box_vat_id'] > 0 ? $welfarVatTotal =  $Utilities->getVatFromId($bill['Bill']['welfare_box_vat_id'])['Ivas']['percentuale'] * $totalWelfareBox / 100  : $welfarVatTotal = 0;
				$ivato  += $welfarVatTotal;
				$totaleIvato += $ivato;
				echo number_format($ivato,2,',','.');
			}
			?>
			</td>
            <td style="text-align:right;">
			<?php
				if($bill['Bill']['imported_bill'] == 1)
				{
					$ritenuta = 0.00;
					foreach($Utilities->getBillGestionalData($bill['Bill']['id']) as $row)				
					{
					 	$ritenuta += $coefficienteMoltiplicativo * $row['Billgestionaldata']['importedwithholdingvalue'];
					}

					$totWithholdingtax += $ritenuta;
					echo number_format($ritenuta,2,',','');
				}
				else
				{
					$withHoldingTax = $Utilities->getWitholdingTaxTotal($bill['Bill']['id']);
					$totWithholdingtax += $withHoldingTax;
					echo number_format($withHoldingTax,2,',','.');
				}
			?>
            </td>
            <td style="text-align:right">
	   			<?php
	   			if($bill['Bill']['imported_bill'] == 1)
				{
					// Se ho le scadenze stampo il totale delle scadenze altrimenti stampo l'importo totale
					$Utilities->getDeadlinesTotal($bill['Bill']['id']) != null ? $apagareImportato = $Utilities->getDeadlinesTotal($bill['Bill']['id']) : $apagareImportato = $coefficienteMoltiplicativo * $bill['Bill']['importo'];
					$totale_fatturato += $coefficienteMoltiplicativo * $apagareImportato;

					if($Utilities->getDeadlinesTotal($bill['Bill']['id']) >= 0)
					{
					    echo number_format($coefficienteMoltiplicativo * $apagareImportato,2,',','');
					}
					else
					{
						echo number_format($apagareImportato, 2, ',', '');
					}
				}
				else
				{
					$daPagareFattura = $Utilities->getDeadlinesTotal($bill['Bill']['id']);
					$totale_fatturato += $daPagareFattura;
					echo number_format($daPagareFattura ,2,',','.');
				}
			?>
            </td>
            <td class="actions" style="text-align:left;">
			<?php 
			if($bill['Bill']['imported_bill'] == 0)
			{
				echo $this->Html->link($iconaModifica, array('action' => 'editExtendedBuy', $bill['Bill']['id']), array('title'=>__('Modifica fattura acquisto'),'escape' => false));
                echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica fattura"></i>', ['action' => 'billduplicate', $bill['Bill']['id'],2], ['title' => __('Duplica fattura d\'acquisto'), 'escape' => false]);
			}
			else
			{ ?>
				<img src="<?= $this->webroot ?>img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica"  class="golIcon iconamodificaoff<?= $bill['Bill']['id'] ?>"  title = "Le fatture importate non sono modificabili">
                <i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#6A6A6A;" title="Duplica fattura"></i>
                <?php
			}

            // Categorizzazione
            if(MODULO_CONTI)
                echo $this->Html->link('<i class="fa fa-book" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Categorizza registrazioni"></i>', ['controller' => 'goods', 'action' => 'categorize', $bill['Bill']['id']], ['title' => __('Categorizza registrazioni'), 'escape' => false]);

			if($bill['Bill']['imported_bill'] == 1)
				echo $this->Html->link($iconaPdf, ['action' => 'showReceivedBill',  $bill['Bill']['id']], ['target'=>'_blank','title'=>__('Scarica PDF'),'escape' => false]);
			else
				echo $iconaPdfOff;
			?>

			<?= $this->Form->postLink($iconaElimina, array('action' => 'delete', $bill['Bill']['id']), array('title'=>__('Elimina fattura d\'acquisto'),'escape' => false), __('Siete sicuri di voler eliminare questo documento?', $bill['Bill']['id'])); ?>
            </td>
        </tr>
<?php }
?>
        <tr>
            <?= $this->Graphic->drawVoidColumn(2); ?>
            <td><strong>Totale</strong></td>
            <td style="text-align:right;"><b><?= number_format($totaleImponibile, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totaleIvato, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totWithholdingtax, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totale_fatturato, 2, ',', '.'); ?><b/></td>
            <?= $this->Graphic->drawVoidColumn(1); ?>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
<?=  $this->element('Form/Components/Paginator/component'); ?>
<div id="element"></div>
<?=  $this->element('Js/datepickercode'); ?>