<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?php $prezzo_beni_riga  = $totale_imponibile = $totale_imposta = 0 ?>
<?= $this->Form->create('Bill', ['class' => 'uk-form uk-form-horizontal']); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;">Modifica nota di credito</span>
    <div class="col-md-12"><hr></div>
    <?=  $this->Form->input('id'); ?>
    <div class="form-group col-md-12">
        <div class="col-md-4">
            <div class="form-controls">
                <?= $this->Form->hidden('Bill.client_name', ['value'=>$this->request->data['Client']['ragionesociale']]); ?>
                <label class="form-label form-margin-top"><strong>Cliente</strong></label>
                <div class="form-controls">
                     <?= $this->element('Form/Components/FilterableSelect/component', [
                        "name" => 'client_id',
                        "aggregator" => '',
                        "prefix" => "client_id",
                        "list" => $clients,
                        "options" => [ 'multiple' => false,'required'=> false],
                        ]);
                    ?>
                </div>
                <?php
                    if($bills['Bill']['client_name'] != $this->request->data['Client']['ragionesociale'])
                        echo '</br><span style="color:red;">La ragione sociale memorizzata sulla fattura "' . $bills['Bill']['client_name'] . '" è differente da quella mostrata per aggiornarla cliccare su salva</span>.';
                ?>
             </div>
        </div>
        <div class="col-md-2" >
           <label class="form-label " style="margin-top:0px;"><strong>Fattura Elettronica</strong></label>
            <div class="form-controls" >
                 <?= $this->Form->hidden('Bill.electronic_invoice', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
                 <?= $bills['Bill']['electronic_invoice'] == 1 ? '<span style="color:#577400"><b>SI</b></span>' : '<span style="color:#ea5d0b"><b>NO</b></span>'; ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
            <div class="form-controls" style="margin-top:8px;">
                <?= $this->Form->input('Bill.split_payment', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
            </div>
        </div>
    </div>
    <div class="col-md-12"><hr></div>
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label"><strong>Numero nota di credito</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('numero_fattura', ['label' => false,'class' => 'form-control','required'=>true,"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']);?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?=  $this->Form->input('sectional_id', ['label' => false, 'options'=>$sectionals, 'class' => 'form-control jsSectional','disabled'=>true]);?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Data nota di credito</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input  type="datetime" id="datanotadicredito" class="datepicker segnalazioni-input form-control" name="data[Bill][date]" value=<?= $this->Time->format('d-m-Y', $this->data['Bill']['date']); ?>  required />
            </div>
        </div>
    </div>
    <?= $this->element('Form/client'); ?>
<?php if($alternativeAddress != null): ?>
    <div class="form-group col-md-12" id="differentAddress">
        <div class="col-md-12">
            <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('alternativeaddress_id', ['div' => false,'type' => 'text','label' => false,'class' => 'form-control','empty'=>true,'disabled'=>true, 'value'=>$alternativeAddress]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="form-group col-md-12">
        <div class="col-md-2" style="float:left;">
            <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.payment_id', ['label' => false,'class' => 'form-control','empty'=>true,'required'=>true]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Ritenuta d'acconto (%)</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.withholding_tax', ['label' => false, 'class' => 'form-control jsWithholdingTax', 'min' => 0, 'max' => 100]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Spese incasso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.collectionfees', ['label' => false, 'class' => 'form-control', 'min' => 0]); ?>
            </div>
        </div>
        <div class="col-md-2 jsAliquota" style="float:left;margin-left:10px;" hidden>
            <label class="form-label form-margin-top"><strong>Aliquota IVA spese incasso</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.collectionfeesvatid', ['label' => false, 'class' => 'form-control', 'min' => 0, 'options' => $vats, 'empty' => true]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Bollo a tuo carico</strong><i class="fa fa-question-circle jsSeal" style="color:#ea5d0b;cursor:pointer;"></i></label>
            <div class="form-controls" >
                <?= $this->Form->input('Bill.seal', ['label' => false,'class' => 'form-control','min'=>0]); ?>
            </div>
        </div>
    <?php if(MULTICURRENCY && $currencyName != 'EUR'): ?>
        <div class="col-md-2 jsMulticurrency" style="float:left;margin-left:10px;" >
            <label class="form-label form-margin-top"><strong>Cambio (EUR / <?= '<span class="jsValueCode">'.$currencyName.'</span>' ?>)</strong></label>
            <div class="form-controls" >
                <?= $this->Form->input('Bill.changevalue', ['label' => false,'class' => 'form-control jsChangeValue','min'=>0]); ?>
            </div>
        </div>
    <?php endif; ?>
    </div>
<?php if($settings['Setting']['welfare_box_vat_id'] != '' || $settings['Setting']['welfare_box_percentage'] > 0 ||  $settings['Setting']['welfare_box_withholding_appliance'] != '' || $settings['Setting']['welfare_box_code']): ?>
    <div class="form-group col-md-12">
        <div class=" col-md-3" style="float:left">
            <label class="form-label"><strong>Tipologia cassa previdenziale</strong></label>
            <div class="form-controls" >
                <?= $this->Form->input('welfare_box_code',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $welfareBoxes ]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="form-label"><strong>Percentuale</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_percentage',['empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control jsBillWelfareBox' ]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="form-label"><strong>Iva Applicata</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_vat_id',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => $vats ]); ?>
            </div>
        </div>
        <div class="col-md-3" >
            <label class="" style="margin-top:5px"><strong>Applicazione ritenuta d'acconto su cassa</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('welfare_box_withholding_appliance',['type'=>'select' ,'empty'=> true,'div' => false, 'label' => false, 'class'=>'form-control','options' => ['0'=>'No','1'=>'Sì']]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="form-group col-md-12">
        <div class="col-md-2">
            <label class="form-label form-margin-top "><strong>Arrotondamento</strong><!--i class="fa fa-question-circle jsDescriptionRounding" style="color:#589ab8;cursor:pointer;" ></i--></label>
            <div class="form-controls">
                <?= $this->Form->input('rounding',['div' => false,'label' => false,'class' => 'form-control','type'=>'select','empty'=>true,'options'=>['0.01'=>'0.01','-0.01'=>'-0.01']]); ?>
            </div>
        </div>
        <div class="col-md-10">
            <label class="form-label"><strong>Note</strong><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;" ></i></label>
            <div class="form-controls">
                <?= $this->Form->input('note', ['label' => false,   'class' => 'form-control','id' => 'lunghezza2','div' => false]); ?>
            </div>
        </div>
    </div>
    <?= $this->data['Bill']['data_parziale']; ?>
    <span id="billPublic" ><?= $this->element('Form/bills/billpa'); ?></span>
    <?php if($bills['Bill']['electronic_invoice'] == 1):  ?>
        <script>
            loadclientispa('<?= addslashes($this->request->data['Client']['ragionesociale']) ?>');
        </script>
    <?php endif; ?>
    <div class="col-md-12"><hr></div>
    <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Righe nota di credito</div>
        <div class="col-md-12"><hr></div>
        <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
        <fieldset id="fatture" class="col-md-12">
        <?php
            $conto = count($this->data['Good']) - 1; $this->data = $bills; $i = -1;

            foreach ($this->data['Good'] as $chiave => $oggetto)
            {
                $i++;
                $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
                $chiave == 0 ? $classe1 = "originale" : $classe1 = '';

                if($oggetto['quantita'] == null && $oggetto['prezzo'] == null && $oggetto['iva_id'] == null)
                {
                    $isNote = 'display:none' ;
                    $isDescriptionNote = ' col-md-12';
                }
                else
                {
                    $isNote = 'display:block';
                    $isDescriptionNote = 'col-md-3';
                }
            ?>
                <div class="principale lunghezza contacts_row   <?= $classe1 ?>  <?= $classe ?>" id="' <?= $chiave ?> '">
                    <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                    <?= $this->Form->input("Good.$chiave.id"); ?>
                    <div class="col-md-12">
                    <?php
                        isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '0';
                        if(ADVANCED_STORAGE_ENABLED)
                        {
                    ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = null;  ?>
                            <?= $this->Form->input("Good.$chiave.tipo", ['required'=>true, 'div' => false, 'label' => false,'class' => 'form-control jsTipo' ,'maxlenght'=>11,'type'=>'select','options'=>[ '1'=>'Articolo','0'=>'Voce descrittiva'],'empty'=>true,'value'=>$movableValue,'disabled'=>true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control  jsCodice' ]); ?>
                        </div>
                    <?php
                        }
                        else
                        {
                            echo $this->Form->hidden("Good.$chiave.tipo", ['div' => false, 'label' => false,'class' => 'form-control jsTipo','value'=>0]);
                    ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong></label>
                            <?= $this->Form->input("Good.$chiave.codice", [ 'div' => false, 'label' => false, 'class' => 'form-control jsCodice']); ?>
                        </div>
                    <?php
                        }
                    ?>
                        <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                            <label class="form-margin-top"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;" ></i></label>
                            <?=  $this->Form->input("Good.$chiave.oggetto", [ 'div' => false, 'label' => false, 'class' => 'form-control  jsDescription','pattern'=>PATTERNBASICLATIN]); ?>
                            <?= $this->Form->hidden("Good.$chiave.storage_id",['class'=>'jsStorage','type'=>'text']); ?>
                            <?= $this->Form->hidden("Good.$chiave.variation_id"); ?>
                            <?= $this->Form->hidden("Good.$chiave.movable",['class'=>'jsMovable','value'=>$movableValue]); ?>
                        </div>
                        <div class="col-md-3 jsRowField" style="<?= $isNote ?>">
                            <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                            <?= $this->Form->input("Good.$chiave.customdescription", ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px','pattern'=>PATTERNBASICLATIN]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.quantita", ['label' => false,'class' => 'form-control jsQuantity','prodotto' => $oggetto['storage_id'],'div' => false, 'step'=>"0.001", 'min'=>"0",]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Unità di misura</strong></label>
                            <?= $this->Form->input("Good.$chiave.unita", ['div' => false,'label' => false,'class' => 'form-control','empty'=>true,'type'=>'select','options'=>$units]); ?>
                        </div>
                        <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                            <label class="form-label form-margin-top"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.prezzo", ['label' => false,'class' => 'jsPrice form-control  ' . $oggetto['storage_id'],'required'=>true]); ?>
                        </div>
                    <?php if(MULTICURRENCY): ?>
                        <div class="col-md-2 jsMulticurrency">
                            <label class="form-margin-top form-label"><strong>Prezzo (<?= '<span class="jsValueCode">'.$currencyName.'</span>' ?>)</strong></label>
                            <?= $this->Form->input('Good.0.currencyprice', ['label' => false,'class' => 'form-control jsChange', 'style'=>'background-color:#fafafa;' ,'value'=>$oggetto['currencyprice']]); ?>
                        </div>
                    <?php endif; ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top form-label"><strong>Importo</strong></label>
                            <?= $this->Form->input("Good.$chiave.importo", ['label' => false,'class' => 'jsImporto','class' => 'form-control jsImporto' ,'disabled'=>true, 'value'=> number_format($oggetto['quantita'] * $oggetto['prezzo'],2,',','')]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                            <?= $this->Form->input("Good.$chiave.discount", ['label' => false,'class' => 'form-control','div'=> false,'max'=>100]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Iva</strong><i class="fa fa-asterisk"></i></label>
                            <?php isset($oggetto['Iva']['id']) ? $iva = $oggetto['Iva']['id'] : $iva =  ''; ?>
                            <?= $this->Form->input("Good.$chiave.iva_id", ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats, 'empty'=>true,  'value' => $iva]); ?>
                        </div>
                        <div class="col-md-2 jsWithholdingAppliance jsRowField" hidden>
                            <label class="form-margin-top form-label"><strong>Applica ritenute/contributi</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Good.$chiave.withholdingapplied", ['label' => false,'class' => 'form-control jsPrice' , 'options'=>['1'=>'Sì','0'=>'No']]); ?> <!-- Fissato altrimenti me ne da 5 -->
                        </div>
                        <div class="row" style="padding-bottom:10px;"><hr></div>
                    </div>
               </div>
        <?php
            $prezzo_beni_riga = '';
            $prezzo_beni_riga = $oggetto['prezzo'] *  $oggetto['quantita'] * (1-$oggetto['discount']/100);
            $totale_imponibile += $prezzo_beni_riga;
            if(isset($oggetto['Iva']['percentuale'])){ $totale_imposta += ($prezzo_beni_riga / 100) *  $oggetto['Iva']['percentuale']; }
        ?>
            <script>
               loadClientCatalog(<?= $i  ?>,'<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId","Good");
            </script>
        <?php
            }
        ?>
        </fieldset>
    </div>
    </br>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexExtendedcreditnotes']); ?></center>
<?= $this->Form->end(); ?>

<script>
    $(document).ready(
        function()
        {
            var clienti = setClients();

            // Carico il listino
            loadClientCatalog(0,'<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId","Good");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti,"#BillClientId");

            $('#BillImporto').hide();

            enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#BillClientId");

            showCollectionfeesVat($("#BillCollectionfees").val());
        }
    );
</script>

<?= $this->element('Js/datepickercode'); ?>

<script>
    function getVat(row){ return $(row).find(".jsVat").val(); }

    function getPrice(row){ return $(row).find(".jsPrice").val(); }

    function getQuantity(row){ return $(row).find(".jsQuantity").val(); }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function(){
        setImporto(this);
        <?php if(MULTICURRENCY){ ?>setCurrencyChange(this,'toCurrency');  <?php } ?>
    });

    $(".jsChange").change(function(){
        <?php if(MULTICURRENCY){ ?>setCurrencyChange(this,'toEuro');  <?php } ?>
        setImporto(this);
    });

    $(".jsQuantity").change(function(){
        setImporto(this);
    });

    $(".jsTipo").change(function(){setCodeRequired(this);});

    $("#BillEditExtendedcreditnotesForm").on('submit.default',function(ev) { });

    $("#BillEditExtendedcreditnotesForm").on('submit.validation',function(ev)
    {
        ev.preventDefault(); // to stop the form from submitting
        /* Validations go here */

        var arrayOfVat = [];
        var errore = 0;
        $(".lunghezza").each( // clonablero

        function()
        {
            var iva = getVat(this);
            var prezzo = getPrice(this);
            var quantita = getQuantity(this);

             if(arrayOfVat.indexOf(iva) === -1)
             {
                arrayOfVat.push(iva);
             }

            if(quantita != '' && quantita !== undefined) { quantita = true; } else { quantita = false; }
            if(prezzo != '' && prezzo !== undefined) { prezzo = true; } else { prezzo = false; }
            if(iva !== undefined && iva != '') {iva = true; } else { iva = false; }

            if((quantita && prezzo && iva) || (!quantita && !prezzo && !iva))
            {
                 /* nothing, is correct */
               //  errore= 0;
            }
            else if(iva && (quantita == false || prezzo == false))
            {
                errore = 1;
            }
            else if(iva == false && (quantita == true || prezzo == true))
            {
                errore = 2;
            }
            else
            {
              /* Not Possible */
            }

            if(arrayOfVat.length > 5)
            {
                errore = 3;
            }
        })

        var datanotadicredito = $("#datanotadicredito").val();
        var datafatturanotadicredito = $("#datepickerreferreddate").val();
        var diffDays = getDateDiff(datafatturanotadicredito,datanotadicredito);
        if(diffDays > 0)
        {
            errore = 111;
        }

        checkBillDuplicate(errore)
    });

    function checkBillDuplicate(errore)
    {
        var errore = errore;

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "checkBillDuplicate"]) ?>",
            data:
            {
                billnumber : $("#BillNumeroFattura").val(),
                date : $("#datanotadicredito").val(),
                type : 3,
                sectional : $(".jsSectional").val(),
            },
            success: function(data)
            {
                if(data > 0 && ( $("#BillNumeroFattura").val() != '<?= $this->data['Bill']['numero_fattura']; ?>'))
                {
                    errore = 4;
                }

                switch (errore)
                {
                   case 0:
                        $("#BillEditExtendedcreditnotesForm").trigger('submit.default');
                   break;
                   case 1:
                       $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica nota di credito',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 2:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica nota di credito',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 3:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica nota di credito',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Modifica nota di credito',
                            content: 'Attenzione esiste già una nota di credito con lo stesso numero nell\'anno di competenza per il sezionale selezionato.',
                            type: 'orange',
                        });
                        return false;
                   break;
                   case 111:
                       $.alert({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione la data della fattura collegata non può essere sucessiva la data della nota di credito',
                            type: 'orange',
                        });
                        return false;
                    break;
                }
            },
            error: function(data)
            {
            }
        });
    }
</script>

<?= $this->element('Js/checkifarenewarticle'); ?>

<script>
    $(".jsSeal").click(function()	{ showHelpMessage('seal'); });
    $(".jsDescriptionHelper").click(function()	{ showHelpMessage('note'); });

    function showHelpMessage(message)
	{
		switch(message)
		{
            case 'seal':
                $.alert({
                    icon: 'fa fa-question-circle',
                    title: '',
                    content: "In questa cella va indicato il bollo a proprio carico. Se il bollo deve essere sommato al totale del documento (bollo a carico del cliente), si deve aggiungere una voce al dettaglio, per esempio \"Rimborso spese bollo\", con quantità 1, importo del bollo e IVA esente articolo 15.",
                    type: 'orange',
                });
            break;
			case 'note':
			    $.alert({
    	            icon: 'fa fa-question-circle',
    		        title: '',
			        content: "Attenzione i campi note non verranno salvati nella fattura elettronica.",
    		        type: 'blue',
		        });
		    break;
		}
	}
</script>

<script>
    manageWithholdingApplianceVisibility();

    $(".jsWithholdingTax").change(function () {
        manageWithholdingApplianceVisibility();
    });

    $(".jsBillWelfareBox").change(function () {
        manageWithholdingApplianceVisibility();
    });

    function manageWithholdingApplianceVisibility()
    {
        if ($(".jsWithholdingTax").val() > 0 || $(".jsBillWelfareBox").val())
            $(".jsWithholdingAppliance").show();
        else
            $(".jsWithholdingAppliance").hide();
    }
</script>

<script>
    if ($('#BillCollectionfees').val() > 0)
    {
        showCollectionfeesVat($('#BillCollectionfees').val());
    }

    $('#BillCollectionfees').change(function () {
        showCollectionfeesVat($(this).val());
    })

    function showCollectionfeesVat(valueOfCollectionFee)
    {
        if (valueOfCollectionFee > 0)
        {
            $('.jsAliquota').show();
            $('#BillCollectionfeesvatid').attr('required', true);
        }
        else
        {
            $('.jsAliquota').hide();
            $('#BillCollectionfeesvatid').attr('required', false);
        }
    }
</script>