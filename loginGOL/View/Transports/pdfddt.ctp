<?php if($_SESSION['Auth']['User']['dbname'] != 'login_GE0043'): ?>
    <?= $this->element('pdfCss/default_transport_css'); ?>
    <htmlpageheader name="myheader"><?= $this->element($pdfHeader); ?></htmlpageheader>
    <sethtmlpageheader name="myheader" value="on" show-this-page="1"/>
    <sethtmlpagefooter name="myfooter" value="on"/>
    <html>
        <body style="position:absolute; top:100px"><?= $this->element($pdfBody); ?></body>
    </html>
<?php else: ?>
    <?= $this->element('pdfCss/default_transport_css'); ?>
    <htmlpageheader name="myheader"><?= $this->element($pdfHeader); ?></htmlpageheader>
    <sethtmlpageheader name="myheader" value="on" show-this-page="1"/>
    <sethtmlpagefooter name="myfooter" value="on"/>
    <html>
        <body><?= $this->element($pdfBody); ?></body>
    </html>
<?php endif; ?>
