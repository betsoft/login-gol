<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>

<?= $this->Form->create('Transport'); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Bolla'); ?></span>
<div class="col-md-12">
    <hr>
</div>

<?php echo $this->Form->input('id'); ?>
<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Bolla</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('transport_number', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false, 'required' => true)); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:20px;">
        <label class="form-margin-top form-label"><strong>Data bolla</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="databolla" class="datepicker segnalazioni-input form-control"
                   name="data[Transport][date]"
                   value=<?= $this->Time->format('d-m-Y', $this->data['Transport']['date']); ?> required/>
        </div>
    </div>
    <!-- Se non è una fattura di reso -->
    <?php if ($this->request->data['Transport']['causal_id'] != 3 && $this->request->data['Transport']['causal_id'] != 7): ?>
        <div class="col-md-4" style="float:left;margin-left:20px;">
            <label class="form-label"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls" float:left;>
                <?= $this->Form->input('client_name', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false, 'required' => true]); ?>
            </div>
        </div>
        <?= $this->Form->hidden('client_id', array('value' => $this->request->data['Transport']['client_id'], 'class' => 'form-control')); ?>
    <?php else: ?>
        <div class="col-md-4" style="float:left;margin-left:20px;">
            <label class="form-label"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('supplier_name', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false, 'required' => true]); ?>
            </div>
        </div>
        <?= $this->Form->hidden('supplier_id', array('value' => $this->request->data['Transport']['supplier_id'], 'class' => 'form-control')); ?>
    <?php endif; ?>
    <!-- Se modulo di magazzino attivato -->
    <?php if (ADVANCED_STORAGE_ENABLED): ?>
        <div class="col-md-3" style="float:left;margin-left:5px;">
            <label class="form-label"><strong>Deposito di prelievo</strong></label>
            <div class="form-controls">
                <?= $this->Form->select('deposit_id', $deposits, ['value' => $this->request->data['Transport']['deposit_id'], 'class' => 'form-control', 'required' => true, 'empty' => false]); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-2" style="float:left;margin-left:0px;">
        <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=$this->Form->input('sectional_id', ['label' => false, 'value' => $defaultSectional['Sectionals']['id'], 'options' => $sectionals, 'class' => 'form-control jsSectional', 'required' => true]); ?>
        </div>
    </div>
</div>

<?php if ($transport['Transport']['causal_id'] == 3 || $transport['Transport']['causal_id'] == 7): ?>
    <?= $this->element('Form/supplier'); ?>
<?php else: ?>
    <?= $this->element('Form/client'); ?>
<?php endif; ?>

<!-- Se non è una fattura di reso -->
<?php if ($this->request->data['Transport']['causal_id'] != 3 && $this->request->data['Transport']['causal_id'] != 7): ?>
    <div class="form-group col-md-12" id="differentAddress">
        <div class="col-md-12">
            <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('alternativeaddress_id', ['div' => false, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'empty' => true, 'options' => $alternativeAddress]); ?>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="form-group col-md-12" id="differentSupplierAddress">
        <div class="col-md-12">
            <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('supplier_alternativeaddress_id', ['div' => false, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'empty' => true, 'options' => $supplierAlternativeAddress]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-margin-top form-label"><strong>Note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', array('label' => false, 'div' => false, 'class' => 'form-control', 'id' => 'lunghezza2')); ?>
        </div>
    </div>
</div>

</br><span id="transport"><?= $this->element('Form/transports/transport'); ?></span></br>
<div class="col-md-12">
    <hr>
</div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12"
         style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe bolla
    </div>
    <div class="col-md-12">
        <hr>
    </div>

    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>

    <fieldset id="fatture" class="col-md-12">
        <?php
        $i = -1;
        $conto = count($transport['Transportgood']) - 1;
        foreach ($transport['Transportgood'] as $chiave => $oggetto) {
            $i++;
            $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
            $chiave == 0 ? $classe1 = "originale" : $classe1 = '';

            if (($oggetto['quantita'] == null || $oggetto['quantita'] == 0) && ($oggetto['prezzo'] == null || $oggetto['prezzo'] == 0) && $oggetto['iva_id'] == null) {
                $isNote = 'display:none';
                $isDescriptionNote = ' col-md-12';
                $isRequired = false;
            } else {
                $isNote = 'display:block';
                $isDescriptionNote = 'col-md-3';
                $isRequired = true;
            }
            ?>
            <div class="principale  clonableRow lunghezza contacts_row   <?= $classe1 ?>  <?= $classe ?>"
                 id="' <?= $chiave ?> '">
                <span class="remove icon rimuoviRigaIcon cross<?= $oggetto['id'] ?> fa fa-remove"
                      title="Rimuovi riga"></span>
                <?= $this->Form->hidden("Transportgood.$chiave.id"); ?>
                <div class="col-md-12">

                    <?php
                    isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = '';
                    if (ADVANCED_STORAGE_ENABLED) {
                        ?>

                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Transportgood.$chiave.tipo", ['required' => $isRequired, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo', '0' => 'Voce descrittiva'], 'empty' => true, 'value' => $movableValue, 'disabled' => true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Transportgood.$chiave.codice", ['label' => false, 'div' => false, 'class' => 'form-control jsCodice', 'readonly' => true]); ?>
                        </div>

                        <?php
                    } else {
                        echo $this->Form->hidden("Transportgood.$chiave.tipo", ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]);
                        ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong></label>
                            <?= $this->Form->input("Transportgood.$chiave.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'readonly' => true, 'style' => 'background-color:#fafafa']); ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Transportgood.$chiave.oggetto", ['label' => false, 'div' => false, 'class' => 'form-control jsDescription']); ?>
                        <?= $this->Form->hidden("Transportgood.$chiave.storage_id", ['type' => 'text']); ?>
                        <?= $this->Form->hidden("Transportgood.$chiave.movable", ['class' => 'jsMovable', 'type' => 'text', 'value' => $movableValue]); ?>
                    </div>
                    <div class="col-md-3 jsRowField" style="<?= $isNote ?>">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input("Transportgood.$chiave.customdescription", ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px']); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top form-label"><strong>Quantità <i
                                        class="fa fa-warning Transportgood.0.iconwarning"
                                        style="color:red;display:none;"></i></strong><i
                                    class="fa fa-asterisk"></i></label>
                        <?php
                        // Setto come max il valore di magazzino massimo più quello già inserito  !!
                        $availableQuantity = $getAviableQuantity->getAvailableQuantity($oggetto['storage_id'], $transport['Transport']['deposit_id']);
                        $availableQuantity < 0 ? $availableQuantity = 0 : null;
                        echo $this->Form->input("Transportgood.$chiave.quantita", ['required' => $isRequired, 'label' => false, 'class' => 'form-control jsQuantity', 'maxquantity' => $availableQuantity + $oggetto['quantita'], 'step' => "0.001", 'min' => "0"]);
                        ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top form-label"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input("Transportgood.$chiave.unita", ['label' => false, 'class' => 'form-control', 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                    </div>
                    <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                        <label class="form-label form-margin-top"><strong>Prezzo</strong>
                            <!--i class="fa fa-asterisk"></i--></label>
                        <?= $this->Form->input("Transportgood.$chiave.prezzo", ['label' => false, 'class' => 'jsPrice form-control  ' . $oggetto['storage_id']]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top form-label"><strong>Importo</strong></label>
                        <?= $this->Form->input("Transportgood.$chiave.importo", ['label' => false, 'class' => 'jsImporto', 'class' => 'form-control jsImporto', 'disabled' => true, 'value' => number_format($oggetto['quantita'] * $oggetto['prezzo'], 2, ',', '')]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                        <?= $this->Form->input("Transportgood.$chiave.discount", ['label' => false, 'class' => 'form-control', 'div' => false, 'max' => 100]); ?>
                    </div>
                    <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                        <label class="form-margin-top"><strong>Iva</strong><!--i class="fa fa-asterisk"></i--></label>
                        <?php isset($oggetto['iva_id']) ? $iva = $oggetto['iva_id'] : $iva = ''; ?>
                        <?= $this->Form->input("Transportgood.$chiave.iva_id", ['label' => false, 'class' => 'form-control jsVat', 'div' => false, 'options' => $vats, 'empty' => true, 'value' => $iva]); ?>
                    </div>
                    <div class="row" style="padding-bottom:10px;">
                        <hr>
                    </div>
                </div>
            </div>

            <script>
                loadClientCatalog(<?= $i ?>, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#TransportClientId", "Transportgood");
            </script>
        <?php } ?>
    </fieldset>
</div>

<?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
<center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function () {

            var clienti = setClients();
            var fornitori = setSuppliers();

            // Carico il listino
            loadClientCatalog(0, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#TransportClientId", "Transportgood");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#TransportClientId");
            setClientAutocomplete(clienti, "#TransportClientName");

            setSupplierAutocomplete(fornitori, "#TransportSupplierId");
            setSupplierAutocomplete(fornitori, "#TransportSupplierName");

            enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#TransportClientId");

            $("#datepicker").val('<?=  date("d-m-Y", strtotime($this->request->data['Transport']['date'])) ?>');

            // Annulla l'eventuale valore del destino diverso
            $("#TransportReferredclientId").change(function () {
                if ($(this).val() != 'empty') {
                    $("#TransportAlternativeaddressId").val('');
                }
            });

            $("#TransportAlternativeaddressId").change(function () {
                if ($(this).val() != 'empty') {
                    $("#TransportReferredclientId").val('');
                }
            });

            addcrossremoving();

        });
</script>

<script>
    $("#TransportEditForm").on('submit.default', function (ev) {
    });

    $("#TransportEditForm").on('submit.validation', function (ev) {
        ev.preventDefault(); // to stop the form from submitting
        checkBillDuplicate()
    });

    function checkBillDuplicate() {
        var errore = 0;
        var erroreQuantity = 0;

        // Gestione dei maxquantity dinamici da magazzino
        $(".jsQuantity").each(
            function () {
                if ($(this).val() > parseFloat($(this).attr("maxquantity"))) {
                    $(this).css('border-color', "red");
                    $(this).parent().find(".fastErroreMessage").remove();
                    var maxAttr = $(this).attr("maxquantity");

                    if (maxAttr < 0) {
                        maxAttr = 0;
                    }

                    $(this).parent().append('<span class="fastErroreMessage" style="color:red">Quantità disponibile : ' + maxAttr + '</span>');
                    erroreQuantity = 11;
                }
            }
        )

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "transports", "action" => "checkTransportDuplicate"]) ?>",
            data:
                {
                    transportnumber: $("#TransportTransportNumber").val(),
                    date: $("#databolla").val(),
                },
            success: function (data) {
                if (data > 0 && ($("#TransportTransportNumber").val() != '<?= $this->data['Transport']['transport_number']; ?>')) {
                    errore = 4;
                }

                switch (errore) {
                    case 0:
                        if (erroreQuantity == 11) {
                            $.confirm({
                                title: 'Modifica Bolla.',
                                content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
                                type: 'orange',
                                buttons:
                                    {
                                        Ok: function () {
                                            action:
                                            {
                                                $("#TransportEditForm").trigger('submit.default');
                                            }
                                        },
                                        annulla: function () {
                                            action:
                                            {
                                                // Nothing
                                            }
                                        },
                                    }
                            });
                        } else {
                            $("#TransportEditForm").trigger('submit.default');
                        }
                        break;
                    case 4:
                        $.alert({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione esiste già una bolla con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });

                        return false;
                        break;
                }
            },
            error: function (data) {
            }
        });
    }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(
        function () {
            setImporto(this);
        }
    );

    $(".jsQuantity").change(
        function () {
            setImporto(this);
        }
    );

    $(".jsTipo").change(
        function () {
            setCodeRequired(this);
        }
    );
</script>

<!--
<script>
    $("#TransportClientName").change(function () {
        $("#TransportClientId").val('');
    });
</script>
-->
