<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Fatturazione bolle per periodo'); ?></span>
        </div>
    </div>

    <div><br/></div>

<?= $this->Form->create(null, ['url' => ['action' => 'createBillForTransportSummary', 'controller' => 'transports'], 'class' => 'uk-form uk-form-horizontal']); ?>
    <div class="ajax-filters-container">
        <table style="width:100%">
            <tr style="width:100%">
                <td style="width:10%;vertical-align:middle  !important;padding:5px;"><label><b>Bolle dal</b></label></td>
                <td style="width:20%;"><input  type="datetime" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]"  bind-filter-event="change"/></td>
                <td style="width:5%;vertical-align:middle !important;padding-left:1%;"><label><b>al</b></label></td>
                <td style="width:20%;"><input type="datetime" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  bind-filter-event="change" /></td>
                <td style="width:15%;vertical-align:middle !important;padding-left:1%;"><label><b>Data personalizzata fattura</b></label></td>
                <td style="width:20%;"><input type="datetime" id="datepicker3" class="form-control  datepicker" name="data[date3]"   /></td>
            </tr>
        </table>
        <br/>
        <table style="width:100%">
            <tr style="width:100%">
                <td style="width:10%;vertical-align:middle !important;padding:5px;"><label><b>Clienti</b></label><i class="fa fa-question-circle jsClient" style="color:#589ab8;cursor:pointer;"></i></td>
                <td  style="width:45%;"><?=  $this->Form->input('filters.clients', ['label' => false,'type'=>'select','multiple'=>true, 'options'=>$clients,'class'=>"form-control ajax-filter-input", "bind-filter-event"=>"change"]); ?></td>
                <td style="width:50%"></td>
            </tr>
        </table>
    </div>

    <br/>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
        <div class="form-controls" style="margin-top:8px;">
            <?= $this->Form->input('Transport.split_payment', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Fattura Elettronica</strong></label>
        <div class="form-controls" style="margin-top:8px;">
            <?= $this->Form->input('Transport.electronic_invoice', ['label' => false,'class' => 'form-control', 'type' => 'checkbox', 'checked' => true]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Fattura dettagliata</strong></label>
        <div class="form-controls" style="margin-top:8px;">
            <?= $this->Form->input('Transport.detailed', ['label' => false,'class' => 'form-control','type'=>'checkbox', 'checked' => true]); ?>
        </div>
    </div>

    <div class="submit" style="margin-left:2%;">
        <input class="btn blue-button new-bill" type="submit" style="margin-top:0px;min-width:20%" value="Crea fattura dei clienti selezionati" >
    </div>

<?php echo  $this->Form->end();  ?>
    <table id="table_example" class="table table-bordered table-striped table-condensed flip-content uk-table-hover">
        <thead class ="flip-content">
        <tr>
            <th><?php echo $this->Paginator->sort('client_id','Cliente'); ?></th>
            <th><?php echo $this->Paginator->sort('transport_id','Bolle'); ?></th>
        </tr>
        </thead>
        <tbody class="ajax-filter-table-content">
        <?php
        $client = '';
        $clientArray = [];

        foreach ($transports as $transport)
        {
            // Se il cliente è lo stesso
            if($client == $transport['Client']['ragionesociale'])
            {
                if(isset($clientArray[$client]['bolle']))
                {
                    $clientArray[$client]['bolle'] .= '<br/> Bolla n. ' . $transport['Transport']['transport_number'] . ' del ' . $this->Time->format('d-m-Y', $transport['Transport']['date']);
                }
                else
                {
                    $clientArray[$client]['bolle'] = '<br/> Bolla n. ' . $transport['Transport']['transport_number'] . ' del ' . $this->Time->format('d-m-Y', $transport['Transport']['date']);
                }
            }
            else
            {
                $client = $transport['Client']['ragionesociale'];
                $clientArray[$client]['ragionesociale'] = $transport['Client']['ragionesociale'];
                $clientArray[$client]['bolle'] = 'Bolla n. ' .  $transport['Transport']['transport_number'] . ' del ' . $this->Time->format('d-m-Y', $transport['Transport']['date']);
            }
        }
        ?>

        <?php foreach($clientArray as $ca): ?>
            <tr>
                <td><?= isset($ca['ragionesociale']) ? $ca['ragionesociale'] : null; ?></td>
                <td><?= $ca['bolle']; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?= $this->element('Form/Components/Paginator/component'); ?>

    <script>
        $(".jsClient").click(
            function()
            {
                $.alert({
                    icon: 'fa fa-question-circle',
                    title: '',
                    content: "<?= addslashes($helperMessage); ?>",
                    type: 'blue',
                });
            }
        );
    </script>

<?= $this->element('Js/datepickercode'); ?>
