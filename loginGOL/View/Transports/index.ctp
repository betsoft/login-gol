<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<?php if(DDT_BILL_FOR_PERIOD): ?>
    <?= $this->element('Form/formelements/indextitle',['indextitle'=>'Bolle','indexelements' => ['transportSummary'=>'Fatturazione bolle per periodo','add'=>'Nuova bolla']]); ?>
<?php else: ?>
    <?= $this->element('Form/formelements/indextitle',['indextitle'=>'Bolle','indexelements' => ['add'=>'Nuova bolla']]); ?>
<?php endif; ?>

<table id="table_example" class="table table-bordered table-striped table-condensed flip-content uk-table-hover">
    <thead class ="flip-content">
    <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
    <tr>
        <?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
            'htmlElements' => [
                '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="'.$startDate.'"  bind-filter-event="change"/>'.''.
                '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="'.$endDate.'" bind-filter-event="change"/></center>'
            ]
        ]);
        ?>
    </tr>
    </thead>
    <tbody class="ajax-filter-table-content">
    <?php if(count($transports) == 0): ?>
        <tr><td colspan="9"><center>nessuna bolla trovata</center></td></tr>
    <?php else: ?>
        <?php foreach ($transports as $transport) : ?>
            <tr>
                <td><?= $transport['Transport']['transport_number']; ?></td>
                <td style="text-align:center;">
                    <?php if($transport['Transport']['date']): ?>
                        <?= $this->Time->format('d-m-Y', $transport['Transport']['date']); ?>
                    <?php endif; ?>
                </td>
                <td class="table-max-width uk-text-truncate">
                    <?php
                    switch($transport['Transport']['causal_id'])
                    {
                        case 1:
                            echo "Vendita";
                            $name = $transport['Transport']['client_name'];
                            break;
                        case 2:
                            echo "c/ riparazione";
                            $name = $transport['Transport']['client_name'];
                            break;
                        case 4:
                            echo "Noleggio";
                            $name = $transport['Transport']['client_name'];
                            break;
                        case 3:
                            echo "Reso fornitore";
                            $name = $transport['Transport']['supplier_name'];
                            break;
                        case 5:
                            echo "Tentata vendita";
                            $name = $transport['Transport']['client_name'];
                            break;
                        case 6:
                            echo "C/Omaggio / Campionatura";
                            $name = $transport['Transport']['client_name'];
                            break;
                        case 7:
                            echo "Uscita c/lavorazione";
                            $name = $transport['Transport']['supplier_name'];
                            break;
                        default:
                            echo "";
                            $name = $transport['Transport']['client_name'];
                            break;
                    }
                    ?>
                </td>
                <td><?php echo $name; ?></td>
                <td style="max-width:200px;">
                    <?php
                    foreach($transport['Transportgood'] as $descrizione)
                    {
                        $printoggetto = $descrizione['oggetto']. ' ' . $descrizione['customdescription'];
                        if(strlen($printoggetto) >90)
                            echo substr($printoggetto,0,89) . '...' .' <br/>' ;
                        else
                            echo $printoggetto  .' <br/>' ;
                    }
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php foreach( $transport['Transportgood'] as $quantita): ?>
                        <?= $quantita['quantita'].'<br/>'; ?>
                    <?php endforeach; ?>
                </td>
                <td style="text-align:right;">
                    <?php foreach($transport['Transportgood'] as $quantita): ?>
                        <?= $quantita['prezzo'].'<br/>'; ?>
                    <?php endforeach; ?>
                </td>
                <td class="table-max-width uk-text-truncate">
                    <?php
                    if($transport['Bill']['numero_fattura'] != null)
                        echo $transport['Bill']['numero_fattura'] . ' / '.date('Y',strtotime($transport['Bill']['date']));
                    ?>
                </td>
                <td class="actions">
                    <?php
                    if($transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7) // Se non è una bolla di reso
                        $transport['Transport']['client_id'] == null ? $client = " " : $client = $transport['Transport']['client_id'];
                    else
                        $transport['Transport']['supplier_id'] == null ? $supplier =" " : $supplier = $transport['Transport']['supplier_id'];

                    $transport['Transport']['date'] == null ? $date = " ": $date = $transport['Transport']['date'];
                    $transport['Transport']['note'] == null ? $note = " " : $note = $transport['Transport']['note'];
                    ?>
                    <?php if($transport['Bill']['numero_fattura'] == null && $transport['Transport']['state'] == 1): ?>
                        <?= $this->Html->link($iconaModifica, ['action' => 'edit', $transport['Transport']['id']],['title'=>__('Modifica ddt'),'escape'=>false]); ?>
                        <?= $this->Html->link('<i class="fa fa-check icon" style="margin-left: 0px;font-size:20px;color:#009600;"></i>', ['action' => 'closeTransport', $transport['Transport']['id']], ['title' => __('Chiudi ddt'), 'escape' => false]); ?>
                        <?php if($transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7): ?>
                            <a title="Genera fattura" onclick="createBill(<?= $transport['Transport']['id']; ?>)"><?= $iconaGeneraFattura ?></a>
                        <?php endif; ?>
                        <?= $this->Html->link($iconaPdf, array('action' => 'pdfddt', $transport['Client']['ragionesociale'] . '-' . $transport['Transport']['id'] . '_' . $this->Time->format('d.m.Y', $transport['Transport']['date']), $transport['Transport']['id']),array('target'=>'_blank','title'=>__('Creazione pdf'),'escape'=>false)); ?>
                        <?= $this->Form->postLink($iconaElimina, array('action' => 'delete', $transport['Transport']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare la seguente bolla ?', $transport['Transport']['id'])); ?>
                    <?php else: ?>
                        <?= $this->Html->link($iconaPdf, array('action' => 'pdfddt', $transport['Client']['ragionesociale'] . '-' . $transport['Transport']['id'] . '_' . $this->Time->format('d.m.Y', $transport['Transport']['date']), $transport['Transport']['id']),array('target'=>'_blank','title'=>__('Creazione pdf'),'escape'=>false)); ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
<?=  $this->element('Form/Components/Paginator/component'); ?>
<?=  $this->element('Js/datepickercode'); ?>

<script>
    function createBill(id)
    {
        var rows = "<table style=\"width:100%;\">";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\"  style=\"height:15px;\"  id=\"check_fsp\"></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\">"+ "Fattura split payment "  + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\" checked=\"checked\" id=\"check_fel\" ></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\" >"+ "Fattura elettronica "  + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\" checked=\"checked\" style=\"height:15px;\"  id=\"check_detail\"></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\">"+ "Fattura dettagliata (indicazione rif. ddt per ogni fattura) "  + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "</table>";

        Frizzysaveconfirm("","Creazione fattura da bolla",function(userChoice)
        {
            if(userChoice == enhancedDialogsTypes.SAVE)
            {
                $.ajax ({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "transports","action" => "fatturaExtended"]) ?>"+"/"+id+"/"+$("#check_detail").prop("checked")+"/"+$("#check_fsp").prop("checked")+"/"+$("#check_fel").prop("checked"),
                    data:
                        {
                        },
                    success: function(data)
                    {
                        window.location.assign("<?= $this->Html->url(["controller" => "bills","action" => "edit"]) ?>"+"/"+data);
                    },
                    error : function(data)
                    {
                    },
                });
            }
        },"<form id=\"scartaccinix\" ><br/><b>Creazione fattura da bolla</b><br/><br/>Seleziona le eventuali opzioni per la creazione della fattura<br/><br/>" + rows + '</form>')
    }
</script>
