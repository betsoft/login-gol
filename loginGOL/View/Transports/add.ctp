<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/supplierautocompletefunction'); ?>

<?= $this->Form->create('Transport'); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova bolla') ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero bolla</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('transport_number', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false, 'required' => true, 'value' => $transportNumber, 'required' => true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data bolla</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="datepickerDataBolla" class="datepicker segnalazioni-input form-control jsDataBollaAdd" name="data[Transport][date]" value="<?= date("d-m-Y"); ?>" required/>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=$this->Form->input('sectional_id', ['label' => false, 'value' => $defaultSectional['Sectionals']['id'], 'options' => $sectionals, 'class' => 'form-control jsSectional', 'required' => true]); ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <label class="form-label"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
    <div class="form-controls">
        <?= $this->Form->input('client_id', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false, 'required' => true]); ?>
    </div>
</div>
<div class="form-group col-md-12">
    <label class="form-label"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
    <div class="form-controls">
        <?= $this->Form->input('supplier_id', ['type' => 'text', 'label' => false, 'class' => 'form-control', 'div' => false]); ?>
    </div>
</div>

<?= $this->element('Form/client'); ?>
<?= $this->element('Form/supplier'); ?>

<div class="form-group" id="differentAddress" hidden>
    <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
    <div class="form-controls">
        <?= $this->Form->input('alternativeaddress_id', ['div' => false, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'empty' => true]); ?>
    </div>
</div>
<div class="form-group" id="differentSupplierAddress">
    <label class="form-label form-margin-top"><strong>Destino diverso</strong></label>
    <div class="form-controls">
        <?= $this->Form->input('supplier_alternativeaddress_id', ['div' => false, 'type' => 'select', 'label' => false, 'class' => 'form-control', 'empty' => true]); ?>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-label form-margin-top"><strong>Note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
</div>

<?php if (ADVANCED_STORAGE_ENABLED): // Se modulo di magazzino attivato ?>
    <div class="form-group col-md-12">
        <label class="form-label form-margin-top"><strong>Deposito di prelievo</strong></label>
        <div class="form-controls">
            <?= $this->Form->select('deposit_id', $deposits, ['value' => $mainDeposit['Deposits']['id'], 'class' => 'form-control', 'required' => true, 'empty' => false]); ?>
        </div>
    </div>
<?php endif; ?>



</br><span id="transport"><?= $this->element('Form/transports/transport'); ?></span></br>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe bolla</div>
    <div class="col-md-12"><hr></div>

    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>

    <fieldset id="fatture" class="col-md-12">
        <div class="principale contacts_row clonableRow originale ultima_riga">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
            <div class="col-md-12">
                <?php if (ADVANCED_STORAGE_ENABLED): ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label"><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Transportgood.0.tipo', ['required' => true, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo', '0' => 'Voce descrittiva'], 'empty' => true, 'value' => 1]); ?>
                    </div>
                    <div class="col-md-2  jsRowField">
                        <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Transportgood.0.codice', ['label' => false, 'class' => 'form-control jsCodice ', 'div' => false, 'required' => true]); ?>
                    </div>
                <?php else: // Tolgo il jsTipo ?>
                    <?= $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]); ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label jsRowField"><strong>Codice</strong></label>
                        <?= $this->Form->input('Good.0.codice', ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                    </div>
                <?php endif; ?>
                <div class="col-md-3 jsRowFieldDescription">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Transportgood.0.oggetto', ['label' => false, 'class' => 'form-control jsDescription', 'div' => false]); ?>
                    <?= $this->Form->hidden('Transportgood.0.storage_id'); ?>
                    <?= $this->Form->hidden('Transportgood.0.variation_id', ['type' => 'text']); ?>
                    <?= $this->Form->hidden('Transportgood.0.movable', ['class' => 'jsMovable', 'value' => 1]); ?>
                </div>
                <div class="col-md-3 jsRowField">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input('Transportgood.0.customdescription', ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label">
                        <strong>Quantità <i class="fa fa-warning Transportgood.0.iconwarning"
                                            style="color:red;display:none;"></i></strong><i class="fa fa-asterisk"></i>
                    </label>
                    <?= $this->Form->input('Transportgood.0.quantita', ['label' => false, 'required' => true, 'class' => 'form-control jsQuantity', 'div' => false, 'step' => "0.001", 'min' => "0"]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input('Transportgood.0.unita', ['label' => false, 'empty' => 'sel.', 'default' => 'sel.', 'class' => 'form-control', 'div' => false, 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top form-label"><strong>Prezzo</strong></label>
                    <?= $this->Form->input('Transportgood.0.prezzo', ['label' => false, 'class' => 'form-control jsPrice']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top form-label"><strong>Importo</strong></label>
                    <?= $this->Form->input('Transportgood.0.importo', ['label' => false, 'class' => 'form-control jsImporto', 'disabled' => true]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Sconto</strong></label>
                    <?= $this->Form->input('Transportgood.0.discount', ['label' => false, 'class' => 'form-control', 'div' => false, 'max' => 100]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-margin-top"><strong>Iva</strong></label>
                    <?= $this->Form->input('Transportgood.0.iva_id', ['label' => false, 'class' => 'form-control jsVat', 'div' => false, 'options' => $vats, 'empty' => true, 'required' => false]); ?>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </fieldset>
</div>

<?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
<center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function () {
            // Nascondo a priori
            $("#differentAddress").hide();
            $("#differentSupplierAddress").hide();

            // Metto required alla prima riga
            $("#Transportgood0Oggetto").prop('required', true);

            var clienti = setClients();
            var fornitori = setSuppliers();

            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0, $("#TransportClientId").val(), "#TransportClientId", "Transportgood");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#TransportClientId");

            // Definisce quel che succede all'autocomplete del cliente
            setSupplierAutocomplete(fornitori, "#TransportSupplierId");

            // Abilita il clonable
            enableCloning($("#TransportClientId").val(), "#TransportClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();

            $("#TransportShippingDate").val('<?= date("d-m-Y H:i"); ?>');


            // Nascondo fornitori
            $("#TransportSupplierId").parents('.form-group').hide();
            $(".supplierdetails").hide();
        }
    );
</script>

<script>

  $('.jsSectional').change(function() {
    $.ajax({
      method: "POST",
      url: "<?= $this->Html->url(["controller" => "sectionals", "action" => "getSectionalNextNumber"]) ?>",
      data:
        {
          sectionalId: $(this).val(),
        },
      success: function (data)
      {
        $("#TransportTransportNumber").val(data);
      }
    });
  })

    $("#TransportAddForm").on('submit.default', function (ev) {});

    $("#TransportAddForm").on('submit.validation', function (ev) {
        ev.preventDefault(); // to stop the form from submitting
        checkBillDuplicate()
    });

    $("#TransportCausalId").change(
        function () {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "Causals", "action" => "getCausalType"]) ?>",
                data:
                    {
                        causalid: $(this).val(),
                    },
                success: function (data) {
                    if (data == 'F') {
                        if ($("#TransportSupplierId").is(":hidden")) {
                            showSupplier();
                        }
                    } else {
                        if ($("#TransportClientId").is(":hidden")) {
                            showClient();
                        }
                    }
                }
            })
        }
    );

    function showClient() {
        $("#TransportClientId").parents('.form-group').show();
        $("#TransportSupplierId").parents('.form-group').hide();
        $('.clientdetails').show();
        $('.supplierdetails').hide();
        $("#differentSupplierAddress").hide();
        $("#TransportClientId").attr('required', true);
        $("#TransportSupplierId").attr('required', false);
    }

    function showSupplier() {
        $("#TransportClientId").parents('.form-group').hide();
        $("#TransportSupplierId").parents('.form-group').show();
        $('.supplierdetails').show();
        $('.clientdetails').hide();
        $("#differentAddress").hide();
        $("#TransportClientId").attr('required', false);
        $("#TransportSupplierId").attr('required', true);
    }

    function checkBillDuplicate() {
        var errore = 0;
        var erroreQuantity = 0;

        // Gestione dei maxquantity dinamici da magazzino
        $(".jsQuantity").each(
            function () {
                if ($(this).val() > parseFloat($(this).attr("maxquantity"))) {
                    $(this).css('border-color', "red");
                    $(this).parent().find(".fastErroreMessage").remove();
                    var maxAttr = $(this).attr("maxquantity");

                    if (maxAttr < 0) {
                        maxAttr = 0;
                    }

                    $(this).parent().append('<span class="fastErroreMessage" style="color:#ff0000">Quantità disponibile : ' + maxAttr + '</span>');
                    erroreQuantity = 11;
                }
            }
        )

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "transports", "action" => "checkTransportDuplicate"]) ?>",
            data:
            {
                transportnumber: $("#TransportTransportNumber").val(),
                date: $("#datepickerDataBolla").val(),
                sectional: $(".jsSectional").val(),
            },
            success: function (data) {
                if (data > 0) {
                    errore = 4;
                }

                switch (errore) {
                    case 0:
                        if (erroreQuantity == 11) {
                            $.confirm({
                                title: 'Creazione bolla',
                                content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
                                type: 'orange',
                                buttons:
                                    {
                                        Ok: function () {
                                            action:
                                            {
                                                $("#TransportAddForm").trigger('submit.default');
                                            }
                                        },
                                        annulla: function () {
                                            action:
                                            {
                                                // Nothing
                                            }
                                        },
                                    }
                            });
                        } else {
                            $("#TransportAddForm").trigger('submit.default');
                        }
                        break;
                    case 4:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: '',
                            content: 'Attenzione esiste già una bolla con lo stesso numero nell\'anno di competenza.',
                            type: 'orange',
                        });
                        return false;
                        break;
                }
            },
            error: function (data) {
            }
        });
    }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(
        function () {
            setImporto(this);
        }
    );

    $(".jsQuantity").change(
        function () {
            setImporto(this);
        }
    );

    $(".jsTipo").change(
        function () {
            setCodeRequired(this);
        }
    );

    $(".jsDataBollaAdd").change(
        function () {
            var today = new Date();
            var ora = today.getHours();
            var minuti = today.getMinutes();

            if (ora < 10) {
                ora = '0' + ora;
            }

            if (minuti < 10) {
                minuti = '0' + minuti;
            }

            var time = ora + ":" + minuti;
            $('#TransportShippingDate').val($(this).val() + ' ' + time);
        }
    );
</script>
