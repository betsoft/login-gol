<!DOCTYPE html>



<html lang="en" class="no-js" style="background-color:#ffffff;">

<head>
<meta charset="utf-8">
<title>Gestionale-online</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Per impedire alla maggior parte dei motori di ricerca di indicizzare la pagina -->
<meta name="robots" content="noindex">
<!-- Per impedire solo a Google di indicizzare la pagina -->
<meta name="googlebot" content="noindex">

<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!--link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

<?= $this->Html->script	([ 'extensions.js','addLoadEvent.js',]) ?>

<?=
	$this->Html->css
	([
		//	'uikit.almost-flat.min.css',
			'plugins/font-awesome/css/font-awesome.min',
			'plugins/simple-line-icons/simple-line-icons.min',
			'plugins/bootstrap/css/bootstrap',
			'plugins/uniform/css/uniform.default',
			'plugins/jquery-minicolors/jquery.minicolors.css',
			'plugins/morris/morris',
			'components',
			'plugins',
			'layout',
			'custom',
			'nyroModal'
	]);
?>
<?php //   $this->element('MegaMenu/loader') ?> 
<?= $this->Html->script
	([ 
  		 
		  'uikit.min.js',
		  'plugins/jquery.min.js',
		  'plugins/jquery-migrate.min.js',
		  'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js',
		  'plugins/bootstrap/js/bootstrap.js',
		  'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		  'plugins/jquery-minicolors/jquery.minicolors.min.js',
		  'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		  'plugins/jquery.blockui.min.js',
		  'plugins/jquery.cokie.min.js',
		  'plugins/uniform/jquery.uniform.min.js',
		  'plugins/morris/morris.min.js',
		  'plugins/morris/raphael-min.js',
		  'plugins/jquery.sparkline.min.js',
		  'scripts/metronic.js',
		  'jquery.nyroModal.custom.min'
	])
?>

<?= $this->element('Form/Components/Lightbox/loader') ?>
<?= $this->element('Form/Components/EnhancedDialogs/loader') ?>

</head>
	<body>
		<div class="lightbox"></div>
			<div  >
				<?php 
				if (!$this->Session->read('Auth.User'))
				{
				?>
				<center>
					<div  class="col-md-12" style="margin-top:8%;margin-bottom:1%;">
						<?= $this->Html->image('gestionale-online.png', array("class" => "uk-container-center", 'style'=>'max-width:300px;width:300px;')); ?>
					</div>
				</center>
				<?php 
				}
				else 
				{ 
					session_destroy();
					?>
					<center>
					<div  class="col-md-12" style="margin-top:8%;margin-bottom:1%;">
						<?=$this->Html->image('gestionale-online.png', array("class" => "uk-container-center", 'style'=>'max-width:300px;width:300px;')); ?>
					</div>
					</center>
				<?php
					// echo $this->element('MegaMenu/component') ?> 
		  <?php } ?>
	</div>
</div>
<div class="page-container" >
	<div class="page-content" style="height:auto;">
		<div class="container">
			
		  	<div id="content" class="col-md-12" style="border:0px;min-height:auto;"  >
				<?= $this->fetch('content'); ?>
				<div style="margin-top:15%;">
					<?= $this->Element('/noratech/footerWhite'); ?>
				</div>
		  	</div>
		</div>
	</div>
</div>


</body>
</html>


