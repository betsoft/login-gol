<!DOCTYPE html>
<!--
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<title>Gestionale-online</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">

    <?= $this->Html->css
        ([
            'plugins/font-awesome/css/font-awesome.min',
            'plugins/simple-line-icons/simple-line-icons.min',
            'plugins/bootstrap/css/bootstrap',
            'plugins/uniform/css/uniform.default',
            'plugins/jquery-minicolors/jquery.minicolors.css',
            'plugins/morris/morris',
            'components',
            'plugins',
            'layout',
            'custom',
            'nyroModal',
            'jquery-confirm.min',
        ]);
    ?>

</head>
<body>
	<div>
		<center>
		<?php if (!$this->Session->read('Auth.User')) { ?>
			<div class="dark-band">
				<?= $this->Html->image('gestionale-online.png', ["class" => "uk-container-center"]); ?>
			</div>
		<?php } else { ?>
				<?php //  $this->element('MegaMenu/component') ?>
		<?php } ?>
		</center>
	</div>
</div>
<div class="page-container"  >
	<div class="page-content" style="background-color:white;">
		<div class="container" style="width:100%">
			<div class="flash form-margin-top ">
				<?= $this->Session->flash() ?>
			</div>
		  	<div id="content" class="col-md-12 col-sm-12"  style="border:0px;min-height: 0vh;">
				<?= $this->fetch('content'); ?>
		  	</div>
		</div>
	</div>

</div>
	<?=  $this->element('MegaMenu/loader') ?>
</body>
</html>


