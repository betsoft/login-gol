<!DOCTYPE html>

<html lang="en" class="no-js">

    <head>
        <meta charset="utf-8">
        <title>Gestionale-online</title>

        <!-- Per impedire alla maggior parte dei motori di ricerca di indicizzare la pagina -->
        <meta name="robots" content="noindex">
        <!-- Per impedire solo a Google di indicizzare la pagina -->
        <meta name="googlebot" content="noindex">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Barlow Semi Condensed' rel='stylesheet'>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <?= $this->Html->script	(['extensions.js','addLoadEvent.js','vladhutilities.js']) ?>

        <?= $this->element('Form/Components/SupportBox/loader'); ?>

        <?=
            $this->Html->css([
                    //'uikit.almost-flat.min.css',
                    'plugins/font-awesome/css/font-awesome.min',
                    'plugins/simple-line-icons/simple-line-icons.min',
                    //'plugins/bootstrap/css/bootstrap.min',
                    'plugins/bootstrap/css/bootstrap',
                    //'../metronic/assets/global/plugins/bootstrap/css/bootstrap',
                    'plugins/uniform/css/uniform.default',
                    'plugins/jquery-minicolors/jquery.minicolors.css',
                    'plugins/morris/morris',
                    'components',
                    'plugins',
                    'layout',
                    'custom',
                    'jquery-confirm.min', // Per la gestione dei messaggi*/
            ]);
        ?>
        <?= $this->element('MegaMenu/loader') ?>
        <?= $this->element('Form/Components/Lightbox/loader') ?>
        <?= $this->element('Form/Components/EnhancedDialogs/loader') ?>

        <?=
            $this->Html->script([
                'plugins/jquery.min.js',
                'plugins/jquery-migrate.min.js',
                'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js',
                //'../metronic/assets/global/plugins/bootstrap/js/bootstrap.js',
                //'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
                'plugins/jquery-minicolors/jquery.minicolors.min.js',
                'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                'plugins/jquery.blockui.min.js',
                'plugins/jquery.cokie.min.js',
                'plugins/uniform/jquery.uniform.min.js',
                'plugins/morris/morris.min.js',
                'plugins/morris/raphael-min.js',
                'plugins/jquery.sparkline.min.js',
                '../metronic/assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect',
                'jquery-confirm',
            ])
        ?>
    </head>

    <body>
        <div class="lightbox"></div>
        <?= $this->element('Form/Components/EnhancedDialogs/component') ?>
        <center>
        <div>
            <?php if (!$this->Session->read('Auth.User')){ ?>
                <div class="dark-band">
                    <?= $this->Html->image('gestionale-online.png', ["class" => "uk-container-center"]); ?>
                </div>
            <?php } else { ?>
                    <?=  $this->element('MegaMenu/component') ?>
            <?php } ?>
        </div>
        </center>
        <div class="page-container">
            <div class="page-content">
                <div class="container">
                    <div class="flash form-margin-top" id="flash-container">
                        <?php echo $this->Session->flash() ?>
                    </div>
                    <div id="content" class="col-md-12 col-sm-12"  >
                        <?php  echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if($_SESSION['Auth']['User']['group_id'] == 6): ?>
            <?= $this->element('Form/Components/SupportBox/component'); ?>
        <?php endif; ?>

        <?php
            if($_SESSION['Auth']['User']['group_id'] != 6)
                $this->Element('noratechFooter');
        ?>
    </body>
</html>