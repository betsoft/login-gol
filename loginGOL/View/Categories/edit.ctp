
<?=  $this->Form->create(); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica categoria merceologica'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('id',array('class'=>'form-control','id'=>'lunghezza2')); ?>

    <div class="form-group col-md-12">
        <div class="col-md-3">
            <label class="form-margin-top form-label"><strong>Codice</label><i class="fa fa-asterisk"></i></strong>
            <div class="form-controls">
                <?= $this->Form->input('code', array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true)); ?>
            </div>
        </div>
       <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Categoria merceologica</label><i class="fa fa-asterisk"></i></strong>
         <div class="form-controls">
	           <?= $this->Form->input('description', array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true)); ?>
         </div>
       </div>
    </div>
 	 <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?=  $this->Form->end(); ?>
	
	



