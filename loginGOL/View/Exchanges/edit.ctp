    <?= $this->Form->create('Exchanges'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo cambio') ?></span>
  	 <div class="col-md-12"><hr></div>
    
    <div class="form-group col-md-12">
        <!--div class="col-md-2" style="float:left;">
        <label class="form-margin-top form-label"><strong>Data inizio</label><i class="fa fa-asterisk"></i></strong>
        <div class="form-controls">
	        <?php // $this->Form->hidden('start_date', ['div' => false, 'label' => false, 'class'=>'form-control datepicker','type'=>'text','required'=>true,'value'=>date('d-m-Y')]); ?>
        </div>
        </div-->
        <?= $this->Form->hidden('start_date', ['div' => false, 'label' => false, 'class'=>'form-control datepicker','type'=>'text','required'=>true,'value'=>date('Y-m-d')]); ?>
        <?= $this->Form->hidden('currency_id', ['div' => false, 'label' => false, 'class'=>'form-control datepicker','type'=>'text','required'=>true,'value'=>$this->request->data['Exchanges']['currency_id']]); ?>
       <div class="col-md-2" style="float:left;margin-left:10px;">
          <label class="form-margin-top form-label"><strong>Tasso di cambio - 1 € = <?= $currencyName ?></strong><i class="fa fa-asterisk"></i></label>
          <div class="form-controls">
	          <?= $this->Form->input('rate', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
     </div>
     <div class="col-md-12"><hr></div>
	    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>

    <?php //$this->element('Js/datepickercode'); ?>