<?= $this->element('Form/Components/Paginator/loader') ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/formelements/indextitle',['indextitle'=>'Fidelities Card','indexelements' => ['generateFidelityEmpty' => 'Crea fidelity vuote', 'add' => 'Nuova Fidelity Card']]); ?>

<div class="clients-index">
    <div class="banks index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <?php if(count($fidelity) > 0) { ?>
                <tr>
                    <?= $this->element('Form/Components/AjaxFilter/component', ['elements' => $filterableFields]) ?>
                </tr>
            <?php }
            else
            {
                ?><tr><td colspan="5"><center>nessuna fidelity card trovata</center></td></tr><?php
            }
            ?>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php foreach($fidelity as $fidelity)
            {
                $punti = $utilities->getFidelityPoints($fidelity['Fidelity']['id']);
                ?>
                <tr>
                    <td><?= h($fidelity['Fidelity']['id']); ?></td>
                    <td><?= h($fidelity['Fidelity']['client_name']) ?></td>
                    <td><?= h($fidelity['Fidelity']['note']) ?></td>
                    <td><?= $punti; ?></td>
                    <td class="actions">
                        <?= $this->element('Form/Simplify/Actions/edit',['id' => $fidelity['Fidelity']['id']]); ?>
                        <?= $this->Html->link($iconaPlus, ['action' => 'load', $fidelity['Fidelity']['id']],['title'=>__('Carico'),'escape'=>false]); ?>
                        <?php if($punti > 0): ?>
                            <?= $this->Html->link($iconaMinus, ['action' => 'unload', $fidelity['Fidelity']['id']],['style'=>'margin-right:3px','title'=>__('Scarico'), 'escape'=>false]) ?>
                        <?php endif; ?>
                        <?= $this->Html->link($iconaQr, ['action' => 'qrCode', $fidelity['Fidelity']['id']], ['style'=>'margin-right:3px','title'=>__('Genera QrCode'), 'escape'=>false]) ?>
                        <?= $this->Form->postLink($iconaElimina, ['action' => 'delete', $fidelity['Fidelity']['id']], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare la fidelity ?', $fidelity['Fidelity']['id'])); ?>

                    </td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
        <?= $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>
