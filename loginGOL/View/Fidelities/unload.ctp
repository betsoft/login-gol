
<?= $this->Form->create('FidelityPoints', array('class' => 'uk-form uk-form-horizontal' , 'enctype' => 'multipart/form-data')); ?>

<?php $punti = $utilities->getFidelityPoints($fidelity['Fidelity']['id']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Scaricamento punti Fidelity'); ?></span>
<div class="col-md-12"><hr></div>
<div class="form-group col-md-12">

    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Punti da scaricare </strong></label> <!-- ( echo h($selectedFidelity['FidelityPoints']['punti'])  ) -->
        <div class="form-controls">
            <?= $this->Form->input('punti',['div'=>false, 'label'=>false,'class'=>'form-control jsPunti','required'=>true, 'max' => $punti]); ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Importo da scontare</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('importo',['div'=>false, 'label'=>false, 'class'=>'form-control jsImporto']); ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Punti / Euro</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('rapporto' ,['div'=>false, 'label'=>false, 'class'=>'form-control jsRapporto', 'value' => 1]); ?> <!--  -->
        </div>
    </div>

    <center><div class="col-md-3">
            <label class="form-margin-top form-label"><strong>Decalcificazione</strong></label>
            <br><br>
            <div class="form-controls">
                <?= $this->Form->input('decal', ['label' => false, 'type' => 'checkbox', 'style' => 'width:25px;height:25px;margin-top:-9px;']); ?>
            </div>
        </div></center>
</div>
<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-margin-top form-label"><strong>Note (sullo scarico)</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note',['div'=>false, 'label'=>false,'class'=>'form-control'])?>
        </div>
    </div>
</div>
<div class="col-md-12"></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

<script>
    $(document).ready(
        function()
        {
            $("#FidelityPointsPunti").change(
                function()
                {
                    var punti = $("#FidelityPointsPunti").val();

                    var massimo = <?= $punti ?> ;

                    if(punti > massimo)
                    {
                        $("#FidelityPointsPunti").val(massimo);
                    }
                }
            );
        }
    );
</script>

<script>


    $(".jsRapporto").change(
        function()
        {
            setRapporto(this);
        }
    );

    $(".jsPunti").change(
        function()
        {
            setRapporto(this);
        }
    );

    function setRapporto(row)
    {
        var rapporto = $(".jsRapporto").val();
        var punti = $(".jsPunti").val();
        var importo = rapporto * punti ;
        $(".jsImporto").val(Math.floor(importo));
    }


</script>

