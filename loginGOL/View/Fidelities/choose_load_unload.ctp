<style>
    .outPopUp {
        position: absolute;
        width: 300px;
        height: 200px;
        z-index: 15;
        top: 50%;
        left: 50%;
        margin: -100px 0 0 -150px;
        background: red;
    }
    .center {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 200px;
        border: 3px solid green;
        width: 300px;
    }
</style>
<div style="top: 15%; left: 40%; position: absolute">

    <?=  $this->Html->link("Carico", ['action' => 'load', $id], ['style'=>' font-size:36px; color: white; background-color: #ea5d0b','title'=>__('Genera QrCode'), 'class'=>'center', 'escape'=>false]) ?>
    <div style="height: 100px"></div>
    <?=  $this->Html->link("Scarico", ['action' => 'unload', $id], ['style'=>'font-size:36px; color: white; background-color: #589AB8','title'=>__('Genera QrCode'), 'class'=>'center', 'escape'=>false]) ?>

</div>