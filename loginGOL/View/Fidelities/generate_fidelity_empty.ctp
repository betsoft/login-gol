<div class="portlet-title">
    <div class="caption">
        <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Generazione Fidelity Card Vuote'); ?></span>
    </div>
</div>

<div><br/></div>

<form action="<?= $this->Html->url(["controller" => "fidelities","action" => "generateFidelityEmpty"]) ?>" method="post" id="downloadBillIxfeForm">
    <div class="row">
        <div class="col-md-2" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Numero di fidelity da generare</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <input type="number" name="num" id="num" default="200" required/>
            </div>
        </div>
        <div class="row col-md-6">
            <label class="form-label form-margin-top"></label>
            <div class="form-controls">
                <div class=" col-md-1 blue-button ebill downloadbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Genera Fidelity Vuote</div>
            </div>
        </div>
    </div>
</form>
<br/>
<script>

    /* SCARICAMENTO FILE PER DATA*/
    $(".downloadbillixfe").click(
        function() {
            var num = $("#num").val();
            addWaitPointer();

            $("#downloadBillIxfeForm").submit();
            removeWaitPointer();
        }

    );
</script>

<?= $this->element('Js/datepickercode'); ?>
<script>
    removeWaitPointer();
</script>
