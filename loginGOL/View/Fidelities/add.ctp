<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->Form->create(); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova fidelity card') ?></span>
<div class="col-md-12"><hr></div>
<div class="form-group col-md-12">
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('client_id', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'required' => 'required', 'maxlength' => false, 'pattern' => PATTERNBASICLATIN]); ?>
        </div>
    </div>
</div>
<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-margin-top form-label"><strong>Note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['div' => false, 'label' => false, 'class' => 'form-control']) ?>
        </div>
    </div>
</div>
<div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']) ?></center>
<?= $this->Form->end() ?>

<script>
    $(document).ready(
        function () {
            // Nascondo a priori

            var clienti = setClients();
            console.log(clienti);

            setClientAutocomplete(clienti, "#FidelityClientId");
        });

    function setClients()
    {
        <?php if(isset($clients)) { ?>
        var clienti = [
            <?php
            foreach ($clients as $client)
            {
                echo '"' . addslashes($client) . '",';
            }
            ?>
        ];

        return clienti;

        <?php } ?>
    }
</script>
