<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>

<script>

    $(document).ready(
        function() {
            var clienti = [
                <?php
                foreach($clients as $client)
                {
                    echo '"' . addslashes($client) . '",';
                }
                ?>];

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#FidelityClientId");
            setClientAutocomplete(clienti, "#cliente");

            //$( "#cliente" ).autocomplete({source: clienti});
        });
</script>
<?= $this->Form->create(); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Fidelity Card'); ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('client_name', ['type' => 'text','id'=>'cliente', 'label' => false, 'class' => 'form-control', 'div' => true, 'required' => true]); ?>
        </div>
    </div>
    <?= $this->Form->hidden('client_id', array('value' => $this->request->data['Fidelity']['client_id'], 'class' => 'form-control')); ?>
</div>

<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-margin-top form-label"><strong>Note</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['div' => false, 'label' => false, 'class' => 'form-control']) ?>
        </div>
    </div>

</div>

<div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']) ?></center>
<?= $this->Form->end() ?>
