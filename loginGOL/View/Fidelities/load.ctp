<?=

$this->Form->create('FidelityPoints', array('class' => 'uk-form uk-form-horizontal' , 'enctype' => 'multipart/form-data')); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Caricamento punti Fidelity'); ?></span>
<div class="col-md-12"><hr></div>
<div class="form-group col-md-12">
    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Importo<i class="fa fa-asterisk"></i></strong></label>
        <div class="form-controls">
            <?= $this->Form->input('importo', ['div' => false, 'label' => false, 'class' => 'form-control jsImporto', 'required'=>true]) ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Rapporto</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('euro_punti',['div'=>false, 'label'=>false, 'class'=>'form-control jsRapporto','required'=>true, 'value'=>$euro_punti]); ?>
        </div>
    </div>
    <div class="col-md-3">
        <label class="form-margin-top form-label"><strong>Punti da caricare</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('punti',['div'=>false, 'step'=>1 ,'label'=>false,'class'=>'form-control jsPunti','required'=>true]); ?>
        </div>
    </div>
    <center><div class="col-md-3">
            <label class="form-margin-top form-label"><strong>Decalcificazione</strong></label>
            <br><br>
            <div class="form-controls">
                <?= $this->Form->input('decal', ['label' => false, 'type' => 'checkbox', 'style' => 'width:25px;height:25px;margin-top:-9px;']); ?>
            </div>
        </div></center>
</div>
<div class="form-group col-md-12">
    <div class="col-md-12">
        <label class="form-margin-top form-label"><strong>Note (sul carico)</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('note',['div'=>false, 'label'=>false,'class'=>'form-control'])?>
        </div>
    </div>
</div>
<div class="col-md-12"></div>
<center><?= $this->element('Form/Components/Actions/component', ['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>
<script>

    $(".jsRapporto").change(
        function()
        {
            setRapporto(this);
        }
    );

    $(".jsImporto").change(
        function()
        {
            setRapporto(this);
        }
    );

    function setRapporto(row)
    {
        var rapporto = $(".jsRapporto").val();
        var importo = $(".jsImporto").val();
        var punti = importo / rapporto;
        $(".jsPunti").val(Math.floor(punti));
    }

</script>


