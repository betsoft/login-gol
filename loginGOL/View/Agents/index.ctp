<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<!--<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Agenti','indexelements' => ['add'=>'Nuovo Agente']]); ?>-->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Agenti']); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
                <?php
                if (count($agents) < 1)
				{
					?><tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">Nessun Agente trovato</td></tr><?php
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php foreach ($agents as $agent)
				{
				?>
					<tr>
                        <td><?= h($agent['Agent']['name']); ?></td>
                        <td><?= h($agent['Agent']['surname']); ?></td>
                        <td><?= h($agent['Deposit']['code']); ?></td>
						<td class="actions">
                            <?php
                                echo $this->Html->link($iconaModifica, ['action' => 'edit', $agent['Agent']['id']],['title'=>__('Modifica'),'escape'=>false]);
                                echo $this->Html->link('<i class="fa fa-list  verde" style="vertical-align:middle;font-size:20px;margin-right:5px;" agentId = "'. $agent['Agent']['id'].'" ></i>', ['action' => 'orderAgents','controller'=>'agents', $agent['Agent']['id']],['title'=>__('Visualizza ordini agente'),'escape'=>false]);
                                echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $agent['Agent']['id']]   , ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare l\'agente ?'  , $agent['Agent']['id']));
							?>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
