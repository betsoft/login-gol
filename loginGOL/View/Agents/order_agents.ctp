<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>


<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
                <?php
                if (count($orders) < 1)
				{
					?><tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">Nessun Ordine trovato</td></tr><?php
				}else{
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php foreach ($orders as $order)
            $totale = 0;
				{
				?>
					<tr>
                        <td><?= h($order['Orders']['numero_ordine']); ?></td>
                        <td><?= h($order['Orders']['delivery_date']); ?></td>
                        <?php
                            $value = $Utilities->getOrderValues($order['Orders']['id']);
                            $totale += $value['totale'];
                        ?>
                        <td><?= h(number_format($value['totale'], 2, ',', '.')); ?></td>
						<td class="actions">
                            <?php
                                echo $this->Html->link($iconaModifica, ['action' => 'edit', 'controller'=>'Orders', $order['Orders']['id']],['title'=>__('Modifica'),'escape'=>false]);
							?>
						</td>
					</tr>
				<?php
				}
				?>
				<tr>
                            <?= $this->Graphic->drawVoidColumn(1); ?>
                            <td style="text-align:right;"><strong>Totale</strong></td>
                            <td style="text-align:right;"><b><?= number_format($totale, 2, ',', '.'); ?><b/></td>
                            <?= $this->Graphic->drawVoidColumn(1); ?>
                        </tr><?php }?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
