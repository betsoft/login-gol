<?=  $this->Form->create('Agents'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica tecnici'); ?></span>
	 <div class="col-md-12"><hr></div>
	<?= $this->Form->input('Agent.id',['class'=>'form-control']); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Nome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Cognome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('surname', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;">
            <label class="form-label form-margin-top"><strong>Furgone</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'deposit_id',
                    "aggregator" => '',
                    "prefix" => "deposit_id",
                    "list" => $deposits,
                    "options" => ['style'=>'opacity: 1;', 'multiple' => false, 'required' => true, 'empty' => true],
                ]);
                ?>
            </div>
        </div>

</div>
	 <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>

