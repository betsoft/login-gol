
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Casse Previdenziali','indexelements' => ['add'=>'Nuova cassa previdenziale']]); ?>
<div class="clients index">
    <div class="banks index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class ="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <?php if (count($einvoice_welfare_box) > 0) { ?>
                <tr>
                    <?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
                </tr>
            <?php }
            else
            {
                ?><tr><td colspan="5"><center>nessuna cassa previdenziale trovata</center></td></tr><?php
            }
            ?>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php foreach ($einvoice_welfare_box as $withholding)
            {
                ?>
                <tr>
                    <td><?php echo h($withholding['Withholding']['code']); ?></td>
                    <td><?php echo h($withholding['Withholding']['description']); ?></td>
                    <td class="actions">
                        <?=	$this->element('Form/Simplify/Actions/edit',['id' => $withholding['Withholding']['id']]); ?>
                        <?= $this->element('Form/Simplify/Actions/delete',['id' =>$withholding['Withholding']['id'],'message' => 'Sei sicuro di voler eliminare una cassa previdenziale ?']);  ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?=  $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>
