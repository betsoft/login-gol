<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/checkmaxstoragequantityerror'); ?>

<?= $this->Form->create('Order', ['class' => 'uk-form uk-form-horizontal']); ?>
<?= $this->Form->hidden('tipologia', ['value' => $tipologia, 'class' => 'form-control']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= 'Nuovo Ordine' ?></span>
<div class="col-md-12">
    <hr>
</div>
<?php $attributes = ['legend' => false]; ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Ordine</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('numero_ordine', ['label' => false, 'value' => $nextOrderNumber, 'class' => 'form-control', 'required' => true, "pattern" => "[0-9a-zA-Z]+", 'title' => 'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('sectional_id', ['label' => false, 'value' => $defaultSectional['Sectionals']['id'], 'options' => $sectionals, 'class' => 'form-control jsSectional']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data Ordine</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="orderdatepicker" class="datepicker segnalazioni-input form-control"
                   name="data[Order][date]" value="<?= date("d-m-Y") ?>" required/>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data Consegna Ordine</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" id="deliverydatepicker" class="datepicker segnalazioni-input form-control"
                   name="data[Order][delivery_date]" value="<?= date("d-m-Y") ?>" required/>
        </div>
    </div>
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('client_id', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'required' => 'required', 'maxlength' => false, 'pattern' => PATTERNBASICLATIN]); ?>
        </div>
    </div>
</div>

<!--<div class="form-group col-md-12">
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('client_id', ['div' => false, 'type' => 'text', 'label' => false, 'class' => 'form-control', 'required' => 'required', 'maxlength' => false, 'pattern' => PATTERNBASICLATIN]); ?>
        </div>
    </div>
</div>-->

<?= $this->element('Form/client'); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i
                    class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('payment_id', ['label' => false, 'class' => 'form-control', 'required' => 'true', 'empty' => true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Banca di destinazione del pagamento</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component',
                [
                    "name" => 'bank_id',
                    "aggregator" => '',
                    "prefix" => "bank_id",
                    "list" => $banks,
                    "options" => ['multiple' => false, 'required' => false],
                    "actions" =>
                        [
                            $this->element('Form/Components/MegaForm/component', ['megaFormIdSuffix' => "add_bank", 'linkClass' => 'fa fa-plus', 'buttonOnly' => true, 'buttonTitle' => 'Aggiungi una nuova banca'])
                        ]
                ]);

            ?>
        </div>
    </div>

        <div class="col-md-4" style="float: left;">
            <label class="form-label form-margin-top "><strong>Note</strong><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('note', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>
            </div>
        </div>

    <div class="col-md-4">
        <label class="form-label form-margin-top "><strong>Deposito</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('deposit_id', ['label' => false, 'options' => $deposits, 'class' => 'form-control']); ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe Ordine</div>
        <div class="col-md-12"><hr></div>

        <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
        <fieldset id="ordini" class="col-md-12">
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
                <div class="col-md-12">
                    <div class="col-md-2 jsRowField">
                        <label class="form-label"><strong>Codice</strong></label>
                        <?= $this->Form->input('Orderrow.0.codice', ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                    </div>
                    <div class="col-md-3 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;"></i></label>
                        <?php
                            echo $this->Form->input('Orderrow.0.oggetto', ['div' => false, 'label' => false, 'class' => 'form-control jsDescription goodDescription', 'required' => 'required', 'pattern' => PATTERNBASICLATIN]);
                            echo $this->Form->hidden('Orderrow.0.storage_id', ['class' => 'jsStorage']);
                        ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.quantita', ['label' => false, 'class' => 'form-control jsQuantity', 'step' => '0.001', 'min' => '0']); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input('Orderrow.0.unita', ['label' => false, 'class' => 'form-control', 'div' => false, 'options' => $units, 'empty' => true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.prezzo', ['label' => false, 'class' => 'form-control jsPrice', 'step' => '0.01']); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label "><strong>Importo</strong></label>
                        <?= $this->Form->input('Orderrow.0.importo', ['label' => false, 'class' => 'form-control jsImporto', 'disabled' => true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                        <?= $this->Form->input('Orderrow.0.discount', ['label' => false, 'class' => 'form-control', 'div' => false, 'max' => 100]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>IVA</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.iva_id', ['label' => false, 'class' => 'form-control jsVat', 'div' => false, 'options' => $vats, 'empty' => true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong> Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.complimentaryQuantity', ['label' => false, 'class' => 'form-control', 'type'=>'number', 'div' => false, 'empty' => true, 'step'=>'0.001']); ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </div>
    <center><?= $this->element('Form/Components/Actions/component', ['redirect' => $redirect]); ?></center>

    <?= $this->Form->end(); ?>
    <?= $this->element('Js/datepickercode'); ?>

    <script>
        $(document).ready(
            function () {
                // Nascondo a priori

                var clienti = setClients();

                // Carico il listino passando chiave e id cliente
                loadClientCatalog(0, $("#OrderClientId").val(), "#OrderClientId", "Orderrow");

                // Definisce quel che succede all'autocomplete del cliente
                setClientAutocomplete(clienti, "#OrderClientId");

                /** Se splitpayment .. */

                // Se clicco e non è accompagnatoria rimuovo tutti i maxquantity (ma se riclicco )
                <?php if(ADVANCED_STORAGE_ENABLED): ?>
                $("#BillAccompagnatoria").click(
                    function () {

                        $(".jsQuantity").each(
                            function () {
                                $(this).removeAttr('maxquantity');
                            }
                        )
                    }

                )
        <?php endif; ?>
        // Abilita il clonable
            enableCloning($("#OrderClientId").val(), "#OrderClientId");

        // Aggiungi il rimuovi riga
            addcrossremoving();
            }
        );
    </script>

    <script>
        // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
        // Controllo che non siano ste inserite più di 5 aliquote iva

        function getVat(row) {
            return $(row).find(".jsVat").val();
        }

        function getPrice(row) {
            return $(row).find(".jsPrice").val();
        }

        function getQuantity(row) {
            return $(row).find(".jsQuantity").val();
        }



        // Inizio codice per gestione prezzo/quantity/tipo
        $(".jsPrice").change(
            function () {
                setImporto(this);
            }
        );

        $(".jsChange").change(function () {
            setImporto(this);
        });

        $(".jsQuantity").change(function () {
            setImporto(this);
        });

        $(".jsTipo").change(function () {
            setCodeRequired(this);
        });
        // Fine codice per gestione prezzo/quantity/tipo

        var formName = "#OrderAddForm";

        $(formName).on('submit.default', function (ev) {
        });

        $(formName).on('submit.validation', function (ev) {
            ev.preventDefault(); // to stop the form from submitting
            /* Validations go here */

            var arrayOfVat = [];
            var errore = 0;
            $(".clonableRow").each(
                function () {
                    var iva = getVat(this);
                    var prezzo = getPrice(this);
                    var quantita = getQuantity(this);

                    if (arrayOfVat.indexOf(iva) === -1) {
                        arrayOfVat.push(iva);
                    }

                    if (quantita != '' && quantita !== undefined) {
                        quantita = true;
                    } else {
                        quantita = false;
                    }
                    if (prezzo != '' && prezzo !== undefined) {
                        prezzo = true;
                    } else {
                        prezzo = false;
                    }
                    if (iva !== undefined && iva != '') {
                        iva = true;
                    } else {
                        iva = false;
                    }

                    if ((quantita && prezzo && iva) || (!quantita && !prezzo && !iva)) {
                        /* nothing, is correct */
                    } else if (iva && (quantita == false || prezzo == false)) {
                        errore = 1;
                    } else if (iva == false && (quantita == true || prezzo == true)) {
                        errore = 2;
                    } else {
                        /* Not Possible */
                    }

                    if (arrayOfVat.length > 4) {
                        errore = 3;
                    }
                });

            // Errore per cassa previdenziale

            checkBillDuplicate(errore);
        });


        function checkBillDuplicate(errore) {
            var erroreQuantity = 0;

            // Gestione dei maxquantity dinamici da magazzino solo se è accompagnatoria

            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "orders", "action" => "checkOrderDuplicate"]) ?>",
                data:
                    {
                        orderNumber: $("#OrderNumeroOrdine").val(),
                        date: $("#orderdatepicker").val(),
                        type: <?= $tipologia ?>,
                        sectional: $(".jsSectional").val(),
                    },
                success: function (data) {
                    if (data > 0) {
                        errore = 4;
                    }

                    switch (errore) {
                        case 0:
                                <?php
                                if($tipologia == 1)
                                {
                                ?>var formName = "#OrderAddForm";
                                <?php
                                }
                                else
                                {
                                ?>var formName = "#BillAddProformaForm";<?php
                        }
                        ?>
                            if (erroreQuantity == 11) {
                                $.confirm({
                                    title: 'Ordine di vendita.',
                                    content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti in magazzino, continuare comunque?',
                                    type: 'orange',
                                    buttons: {
                                        Ok: function () {
                                            action:
                                            {
                                                    <?php
                                                    if($tipologia == 1)
                                                    {
                                                    ?>var formName = "#OrderAddForm";
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>var formName = "#BillAddProformaForm";<?php
                                                }
                                                ?>

                                                $(formName).trigger('submit.default');
                                            }
                                        },
                                        annulla: function () {
                                            action:
                                            {
                                                // Nothing
                                                $(".enhanced-dialog-container").show();
                                            }
                                        },
                                    }
                                });
                            } else {
                                $(formName).trigger('submit.default');
                            }
                            break;
                        case 1:
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Creazione Ordine',
                                content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                                type: 'orange',
                            });
                            return false;
                            break;
                        case 2:
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Creazione Ordine',
                                content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                                type: 'orange',
                            });
                            return false;
                            break;
                        case 4:
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                                title: 'Creazione Ordine',
                                content: 'Attenzione esiste già un ordine con lo stesso numero nell\'anno di competenza per il sezionale selezionato.',
                                type: 'orange',
                            });
                            return false;
                            break;
                        case 748:
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                                title: 'Creazione Ordine',
                                content: 'Attenzione è stata selezionata la tipologia di documento ma non è stato correttamente inserito il numero di documento.',
                                type: 'orange',
                            });
                            return false;
                            break;
                    }
                },
                error: function (data) {
                }
            });
        }
    </script>

    <script>
        $('.jsSectional').change(
            function () {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "sectionals", "action" => "getSectionalNextNumber"]) ?>",
                    data:
                        {
                            sectionalId: $(this).val(),
                        },
                    success: function (data) {
                        $("#OrderNumeroOrdine").val(data);
                    }
                });
            }
        )


    </script>

    <?= $this->element('Js/checkifarenewarticle'); ?>

