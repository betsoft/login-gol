<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<?= $this->Form->create('Order', ['class' => 'uk-form uk-form-horizontal']); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= 'Crea fattura da più ordini'; ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data Ordine dal</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateFrom" class="datepicker segnalazioni-input form-control" name="data[Order][delivery_date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data Ordine al </strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="DateTo" class="datepicker segnalazioni-input form-control" name="data[Order][delivery_date]" value="<?= date("d-m-Y") ?>"  required />
        </div>
    </div>
        <div class="col-md-3" style="float:left;">
            <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'client_id',
                    "aggregator" => '',
                    "prefix" => "client_id",
                    "list" => $clients,
                    "options" => ['multiple' => false, 'required' => true],
                ]);
                ?>
            </div>
    </div>
    <div class="row col-md-4">
        <div class="form-group col-md-12" id="differentAddress" hidden>
            <label class="form-label form-margin-top" ><strong>Destino diverso</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('alternativeaddress_id', ['div' => false,'type' => 'select','label' => false,'class' => 'form-control','empty'=>true]); ?>
            </div>
        </div>
    </div>

    <div class="row col-md-5">
        <label class="form-label form-margin-top"></label>
        <div class="form-controls">
            <div class=" col-md-1 blue-button ebill sendbillixfe" style="float:left;width:20%;margin-left:10px;padding:3px;width:80%;margin-top:5px;" >Crea Fattura</div>
        </div>
    </div>
</div>

<?= $this->Form->end(); ?>
<?= $this->element('Js/datepickercode'); ?>



<script>
    $("#client_id_multiple_select").change(
        function()
        {


            var clientId = $("#client_id_multiple_select").val();

            if(clientId != '')
            {
                //enableCloning(clientId, "#BillClientId");

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "clients","action" => "getClientData"]) ?>",
                    data:
                        {
                            clientId : clientId,
                        },
                    success: function(data)
                    {
                        data = JSON.parse(data);

                        if(data['Clientdestination'].length > 0)
                        {
                            $("#differentAddress").show();
                            $('#OrderAlternativeaddressId').append($("<option></option>").attr("value", 'empty').text(""));

                            $.each(data['Clientdestination'], function (key, element) {
                                if (element.nation != null)
                                    var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province + '  ' + element.nation;
                                else
                                    var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province;

                                $('#OrderAlternativeaddressId').append($("<option></option>").attr("value", element.id).text(indirizzo));
                            })
                        }

                    }
                });
            }
        }
    );

    $(".sendbillixfe").click(
        function()
        {
            addWaitPointer();
            var datastart = $("#DateFrom").val();
            var dataend = $("#DateTo").val();
            var clientId = $("#client_id_multiple_select").val();
            if($("#OrderAlternativeaddressId").val() == ''){
                var destinationId = 0;
            }else{
                var destinationId = $("#OrderAlternativeaddressId").val();
            }
            console.log(destinationId);
            var diffDays = getDateDiff(dataend,datastart);

            if(diffDays < 0)
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: 'Creazione Fattura',
                    content: 'Attenzione la data di fine non può precedere la data di inizio.',
                    type: 'orange',
                });

                removeWaitPointer();
            }
            else
            {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "orders","action" => "multiOrderBill"]) ?>",
                    async : true,
                    data:
                        {
                            dateFrom: datastart,
                            dateTo: dataend,
                            clientId: clientId,
                            destinationId: destinationId
                        },
                    success: function(data)
                    {
                        window.location.assign("<?= $this->Html->url(["controller" => "bills", "action" => "edit"]) ?>" + "/" + data);
                        removeWaitPointer();
                    },
                    error: function(data)
                    {

                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Errore Creazione Fattura',
                            content: 'Errore nella creazione',
                            type: 'orange',
                        });
                        removeWaitPointer();
                    }
                })
            }
        }
    );


</script>
