<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/showhideelectronicinvoice'); ?>
<?= $this->element('Js/showhideaccompagnatoria'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/checkmaxstoragequantityerror'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?php $prezzo_beni_riga = $totale_imponibile = $totale_imposta = 0 ?>
<?= $this->Form->create('Order', ['class' => 'uk-form uk-form-horizontal']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= $tipologia == 1 ? 'Modifica Ordine' : 'Modifica Ordine'; ?></span>
<div class="col-md-12"><hr></div>

<?= $this->Form->input('id'); ?>


<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero Ordine</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('numero_ordine', ['label' => false, 'class' => 'form-control', 'required' => true, "pattern" => "[0-9a-zA-Z]+", 'title' => 'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Sezionale</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('sectional_id', ['label' => false, 'options' => $sectionals, 'class' => 'form-control jsSectional']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Data Ordine</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" class="datepicker segnalazioni-input form-control" id="dataordine" readonly
                   name="data[Order][date]"
                   value=<?= $this->Time->format('d-m-Y', $this->data['Order']['date']); ?> />
        </div>
    </div>
    <div class="col-md-2">
    <label class="form-label form-margin-top"><strong>Completato?</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls" style="margin-top:8px;">
                            <?= $this->Form->input('Order.completed', ['label' => false, 'type'=>'checkbox' , 'style'=>'width:25px;height:25px;margin-top:-9px;']); ?>
            </div>
    </div>
</div>
<br>
<div class="form-group col-md-12">
    <div class="col-md-3" style="float:left;">
        <div class="form-controls">
            <?= $this->Form->hidden('Order.client_name', ['value' => $this->request->data['Client']['ragionesociale']]); ?>
            <label class="form-label form-margin-top"><strong>Cliente</strong></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', [
                        "name" => 'client_id',
                        "aggregator" => '',
                        "prefix" => "client_id",
                        "list" => $clients,
                        "options" => ['multiple' => false, 'required' => false],
                    ]);
                ?>
            </div>
            <?php if($orders['Order']['client_name'] != $this->request->data['Client']['ragionesociale']): ?>
                <?= '</br><span style="color:red;">La ragione sociale memorizzata sull\'ordine "' . $orders['Order']['client_name'] . '" è differente da quella mostrata, per aggiornarla cliccare su salva</span>.' ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= $this->element('Form/client'); ?>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Metodo di Pagamento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('Order.payment_id', ['label' => false, 'class' => 'form-control', 'empty' => true, 'required' => true]); ?>
        </div>
    </div>
    <?php if (MULTICURRENCY && $currencyName != 'EUR'): ?>
        <div class="col-md-2 jsMulticurrency" style="float:left;margin-left:10px;">
            <label class="form-label form-margin-top"><strong>Cambio (EUR / <?= '<span class="jsValueCode">' . $currencyName . '</span>' ?>)</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('Bill.changevalue', ['label' => false, 'class' => 'form-control jsChangeValue', 'min' => 0]); ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="form-group col-md-12">
    <div class="col-md-10">
        <label class="form-label"><strong>Note</strong><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('note', ['label' => false, 'class' => 'form-control', 'id' => 'lunghezza2', 'div' => false]); ?>
        </div>
    </div>
</div>


<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Righe Ordine</div>
    <div class="col-md-12"><hr></div>
    <?php
        if (FIXED_COST_PERCENTAGE)
        {
            if ($settings['Setting']['fixed_cost_percentage_enabled'])
                echo $this->element('Form/Simplify/action_fixed_cost_percentage_button');
        }
    ?>

    <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    <fieldset id="fatture" class="col-md-12">
    <?php
        $conto = count($this->data['Orderrow']) - 1;
        $this->data = $orders;
        $i = -1;

        if(count($this->data['Orderrow']) != 0)
        {
            foreach ($this->data['Orderrow'] as $chiave => $oggetto) {
                $i++;
                $chiave == $conto ? $classe = "ultima_riga" : $classe = '';
                $chiave == 0 ? $classe1 = "originale" : $classe1 = '';

                $readOnly = false;
                $backgroundColorReadonly = "#fff";
                if (($oggetto['quantita'] == null || $oggetto['quantita'] == 0) && ($oggetto['prezzo'] == null || $oggetto['prezzo'] == 0) && $oggetto['iva_id'] == null) {
                    $isNote = 'display:none';
                    $isDescriptionNote = ' col-md-12';
                } else {
                    $isNote = 'display:block';
                    $isDescriptionNote = 'col-md-3';
                }
                ?>
                <div class="principale lunghezza clonableRow contacts_row <?= $classe1 ?> <?= $classe ?>" id="'<?= $chiave ?>'">
                <?php if (isset($oggetto['transport_id']) && $oggetto['transport_id'] != null): ?>
                    <span class="remove rimuoviRigaIcon icon fa fa-remove" style="color:#dedede;" title="Articoli provenienti da bolla, rimozione riga disabilitata"></span>
                <?php else: ?>
                    <span class="remove rimuoviRigaIcon icon cross<?= $oggetto['id'] ?> fa fa-remove" title="Rimuovi riga"></span>
                <?php endif; ?>
                <?= $this->Form->input("Orderrow.$chiave.id"); ?>
                    <div class="col-md-12">
                    <?php isset($oggetto['Storage']['movable']) ? $movableValue = $oggetto['Storage']['movable'] : $movableValue = null; ?>
                    <?php if (ADVANCED_STORAGE_ENABLED): ?>
                        <div class="col-md-2 jsRowField">
                            <label class="form-label "><strong>Tipo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Orderrow.$chiave.tipo", ['required' => true, 'div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'maxlenght' => 11, 'type' => 'select', 'options' => ['1' => 'Articolo', '0' => 'Voce descrittiva'], 'empty' => true, 'value' => $movableValue, 'disabled' => true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Orderrow.$chiave.codice", ['div' => false, 'label' => false, 'readonly' => $readOnly, 'class' => 'form-control  jsCodice', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                        </div>
                    <?php else: ?>
                        <?= $this->Form->hidden("Orderrow.$chiave.tipo", ['div' => false, 'label' => false, 'class' => 'form-control', 'value' => 0]); ?>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Codice</strong></label>
                            <?= $this->Form->input("Orderrow.$chiave.codice", ['div' => false, 'label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsCodice', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                        </div>
                    <?php endif; ?>
                        <div class="<?= $isDescriptionNote ?> jsRowFieldDescription">
                            <label class="form-margin-top"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;"></i></label>
                            <?= $this->Form->input("Orderrow.$chiave.oggetto", ['div' => false, 'readonly' => $readOnly, 'label' => false, 'class' => 'form-control jsDescription ', 'pattern' => PATTERNBASICLATIN, 'required' => 'required', 'style' => 'background-color:' . $backgroundColorReadonly . '']); ?>
                            <?= $this->Form->hidden("Orderrow.$chiave.storage_id", ['class' => 'jsStorage', 'type' => 'text']); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                            <?php if (ADVANCED_STORAGE_ENABLED)
                                echo $this->Form->input("Orderrow.$chiave.quantita", ['label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsQuantity', 'prodotto' => $oggetto['storage_id'], 'div' => false, 'maxquantity' => $getAviableQuantity->getAvailableQuantity($oggetto['storage_id'], $orders['Order']['deposit_id']) + $oggetto['quantita'], 'step' => '0.001', 'min' => '0']);
                            else
                                echo $this->Form->input("Orderrow.$chiave.quantita", ['label' => false, 'readonly' => $readOnly, 'class' => 'form-control jsQuantity', 'prodotto' => $oggetto['storage_id'], 'div' => false, 'step' => 1]);
                            ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Unità di misura</strong></label>
                            <?= $this->Form->input("Orderrow.$chiave.unita", ['div' => false, 'label' => false, 'class' => 'form-control', 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                        </div>
                        <div class="col-md-2 aa jsRowField" style="<?= $isNote ?>">
                            <label class="form-label form-margin-top"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Orderrow.$chiave.prezzo", ['label' => false, 'class' => 'jsPrice form-control  ' . $oggetto['storage_id']]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top form-label"><strong>Importo</strong></label>
                            <?= $this->Form->input("Orderrow.$chiave.importo", ['label' => false, 'class' => 'jsImporto', 'class' => 'form-control jsImporto', 'disabled' => true]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Sconto (%)</strong></label>
                            <?= $this->Form->input("Orderrow.$chiave.discount", ['label' => false, 'class' => 'form-control', 'div' => false, 'max' => 100]); ?>
                        </div>
                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>IVA</strong><i class="fa fa-asterisk"></i></label>
                            <?php isset($oggetto['Iva']['id']) ? $iva = $oggetto['Iva']['id'] : $iva = ''; ?>
                            <?= $this->Form->input("Orderrow.$chiave.iva_id", ['label' => false, 'class' => 'form-control jsVat', 'div' => false, 'options' => $vats, 'empty' => true, 'value' => $iva]); ?>
                        </div>

                        <div class="col-md-2 jsRowField" style="<?= $isNote ?>">
                            <label class="form-margin-top"><strong>Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                                <?= $this->Form->input("Orderrow.$chiave.complimentaryQuantity", ['label' => false, 'class' => 'form-control','step'=>'0.001', 'div' => false, 'empty' => true]); ?>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom:10px;"><hr></div>
                </div>
            <?php
                $prezzo_beni_riga = '';
                $prezzo_beni_riga = $oggetto['prezzo'] * $oggetto['quantita'] * (1 - $oggetto['discount'] / 100);
                $totale_imponibile += $prezzo_beni_riga;

                if (isset($oggetto['Iva']['percentuale']))
                    $totale_imposta += ($prezzo_beni_riga / 100) * $oggetto['Iva']['percentuale'];
            ?>
                <script>loadClientCatalog(<?= $i ?>, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#BillClientId", "Orderrow");</script>
            <?php
            }
        }
        else
        {
            ?>
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"></span>
                <div class="col-md-12">
                    <div class="col-md-3 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i><i class="fa fa-question-circle jsDescriptionHelper" style="color:#589ab8;cursor:pointer;display:none;" ></i></label>
                        <?php
                        echo $this->Form->input('Orderrow.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control jsDescription goodDescription','required'=>'required','pattern'=>PATTERNBASICLATIN]);
                        echo $this->Form->hidden('Orderrow.0.storage_id',['class'=>'jsStorage']);
                        ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.quantita', ['label' => false,'default' => 1,'class' => 'form-control jsQuantity', 'step'=> '0.001', 'min' => '0']);?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input('Orderrow.0.unita', ['label' => false,'class' => 'form-control','div'=> false, 'options' => $units,'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label"><strong>Prezzo</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.prezzo', ['label' => false,'class' => 'form-control jsPrice' , 'step'=>'0.01']); ?> <!-- Fissato altrimenti me ne da 5 -->
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top form-label "><strong>Importo</strong></label>
                        <?= $this->Form->input('Orderrow.0.importo', ['label' => false,'class' => 'form-control jsImporto' ,'disabled'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>IVA</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Orderrow.0.iva_id', ['label' => false,'class' => 'form-control jsVat','div'=> false, 'options' => $vats,'empty'=>true]); ?>
                    </div>
                    <div class="col-md-2 jsRowField">
                        <label class="form-margin-top"><strong>Quantità Omaggio</strong><i class="fa fa-asterisk"></i></label>
                            <?= $this->Form->input("Orderrow.0.complimentaryQuantity", ['label' => false, 'class' => 'form-control','step'=>'0.001', 'div' => false, 'empty' => true]); ?>
                    </div>
                    <div class="row" style="padding-bottom:10px;"><hr></div>
                </div>
            </div>
            <?php
        }
     ?>
    </fieldset>
    <?= $this->element('Form/Simplify/action_add_clonable_row_bottom'); ?>
    </br>
    <?php $totale_complessivo = $totale_imponibile + $totale_imposta; ?>
    <center><?= $this->element('Form/Components/Actions/component', ['redirect' => $redirect]); ?></center>
    <?php echo $this->Form->end(); ?>
</div>

<script>
    $(document).ready
    (
        function ()
        {
            var clienti = setClients();


            // Carico il listino
            loadClientCatalog(0, '<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#OrderClientId", "Orderrow");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#OrderClientId");


            $('#OrderImporto').hide();

            enableCloningedit('<?= addslashes($this->request->data['Client']['ragionesociale']); ?>', "#OrderClientId");

            addcrossremoving();

            // Se spese incasso maggiroe di zero mostro il campo
        });

</script>

<?= $this->element('Js/datepickercode'); ?>

<script>$('.segnalazioni-input').css('cursor', 'pointer'); </script>

<script>

    // Controllo che siano tutti presenti iva/quantita/prezzo o assenti
    // Controllo che non siano ste inserite più di 5 aliquote iva

    function getVat(row) {
        return $(row).find(".jsVat").val();
    }

    function getPrice(row) {
        return $(row).find(".jsPrice").val();
    }

    function getQuantity(row) {
        return $(row).find(".jsQuantity").val();
    }

    // Inizio codice per gestione prezzo/quantity/tipo
    $(".jsPrice").change(function () {
        setImporto(this);
        <?php if(MULTICURRENCY && $currencyName != 'EUR'){ ?>setCurrencyChange(this, 'toCurrency');  <?php } ?>
    });
    $(".jsChange").change(function () {
        <?php if(MULTICURRENCY && $currencyName != 'EUR'){ ?>setCurrencyChange(this, 'toEuro');  <?php } ?>
        setImporto(this);
    });
    $(".jsQuantity").change(function () {
        setImporto(this);
    });

    $(".jsTipo").change(function () {
        setCodeRequired(this);
    });

    // Fine codice per gestione prezzo/quantity/tipo


    var formName = "#OrderEditForm"

    $(formName).on('submit.default', function (ev) {
    });

    $(formName).on('submit.validation', function (ev) {

        ev.preventDefault(); // to stop the form from submitting

        /* Validations go here */
        var arrayOfVat = [];
        var errore = 0;
        $(".lunghezza").each( // clonablero

            function () {
                var iva = getVat(this);
                var prezzo = getPrice(this);
                var quantita = getQuantity(this);

                if (arrayOfVat.indexOf(iva) === -1) {
                    arrayOfVat.push(iva);
                }

                if (quantita != '' && quantita !== undefined) {
                    quantita = true;
                } else {
                    quantita = false;
                }
                if (prezzo != '' && prezzo !== undefined) {
                    prezzo = true;
                } else {
                    prezzo = false;
                }
                if (iva !== undefined && iva != '') {
                    iva = true;
                } else {
                    iva = false;
                }

                if ((quantita && prezzo && iva) || (!quantita && !prezzo && !iva)) {
                    /* nothing, is correct */
                    //  errore= 0;
                } else if (iva && (quantita == false || prezzo == false)) {
                    errore = 1;
                } else if (iva == false && (quantita == true || prezzo == true)) {
                    errore = 2;
                } else {
                    /* Not Possible */
                }

                if (arrayOfVat.length > 5) {
                    errore = 3;
                }
            });


        checkOrderDuplicate(errore)

    });

    function checkOrderDuplicate(errore) {
        // var errore = errore;

        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "checkBillDuplicate"]) ?>",
            data:
                {
                    billnumber: $("#OrderNumeroOrdine").val(),
                    date: $("#dataordine").val(),
                    type: <?= $tipologia ?>,
                    sectional: $(".jsSectional").val(),
                },
            success: function (data) {
                if (data > 0 && ($("#OrderNumeroOrdine").val() != '<?= $this->data['Order']['numero_ordine']; ?>')) {
                    errore = 4;
                }

                switch (errore) {
                    case 111:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione fattura',
                            content: 'Attenzione il peso netto non può essere superiore al peso lordo',
                            type: 'orange',
                        });
                        break;
                        return false;
                    case 0:
                        var formName = "#OrderEditForm";

                            $(formName).trigger('submit.default');
                        break;
                    case 1:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica Ordine',
                            content: 'Attenzione sono presenti righe in cui è stata selezionata l\'iva, ma non correttamente importo e quantità.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 2:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica ordine',
                            content: 'Attenzione sono presenti righe in cui sono selezionati importo o quantità, ma non è stata indicata alcuna aliquota iva.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 3:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Modifica ordine',
                            content: 'Attenzione sono presenti più di cinque aliquote iva. Il numero massimo consentito è 5.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case  747:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione Ordine',
                            content: 'Attenzione è stato compilato almeno un campo nella sezione "Riferimento ordine documento", ma non è stata selezionata la tipolgia di documento.',
                            type: 'orange',
                        });
                        return false;
                        break;
                    case 748:
                        $.alert
                        ({
                            icon: 'fa fa-warning',
                            title: 'Creazione Ordine',
                            content: 'Attenzione è stata selezionata la tipologia di documento ma non è stato correttamente inserito il numero di documento.',
                            type: 'orange',
                        });
                        return false;
                        break;
                }
            },
            error: function (data) {
            }
        });
    }

</script>

<?= $this->element('Js/checkifarenewarticle'); ?>
