<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader'); ?>
<?= $this->element('Form/Components/AjaxSort/loader'); ?>
<?= $this->element('Js/ixfe/ixfefunction'); ?>
<?= $this->element('Js/csv/csvfunction'); ?>

<?php echo $this->element('Form/formelements/indextitle', ['indextitle' => 'Ordini di Vendita', 'indexelements' => ['add' => 'Nuovo ordine', "multiOrdersBillCreate"=>"Crea Fattura da più ordini"], 'passedValues' => $tipologia, 'xlsLink' => 'index']);


?>

<table id="table_example" class="table table-bordered table-hover table-striped flip-content">
    <thead class="flip-content">
    <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
    <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' => $filterableFields,
            'afterAjaxfilterCallback' => 'indexbillAfterAjaxFilterCallback',
            'htmlElements' => [
                '<center><input type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date1]" value="' . $startDate . '"  bind-filter-event="change"/>' . '' .
                '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input filter_ragionesociale datepicker" name="data[filters][date2]"  value="' . $endDate . '" bind-filter-event="change"/></center>',
                $this->Form->input('filters.tipologia', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $tipologia , 'bind-filter-event'=>"change"]),
                $this->Form->input('filters.payment', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $stati , 'bind-filter-event'=>"change"]),
                $this->Form->input('filters.agenti', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $agenti , 'bind-filter-event'=>"change"]),
                $this->Form->input('filters.bill_id', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $fatturato , 'bind-filter-event'=>"change"]),

            ]
        ]);
        ?>
    </tr>
    </thead>
    <tbody class="ajax-filter-table-content">
    <?php if (count($orders) == 0) : ?>
        <tr>
            <td colspan="10">
                <center>Nessun Ordine trovato</center>
            </td>
        </tr>
    <?php else: ?>
        <?php
        $totaleImponibile = 0;
        $totaleIvato = 0;
        $totaleWithholdingtax = 0;
        $totaleAPagare = 0;
        foreach ($orders as $order) {
            ?>
            <tr>
                <td><?= $order['Order']['numero_ordine'] . $sectionals[$order['Order']['sectional_id']] ?></td>
                <td style="text-align:center"><?= $this->Time->format('d-m-Y', $order['Order']['date']); ?></td>
                <td class="table-max-width uk-text-truncate"><?php echo $order['Order']['client_name']; ?></td>
                <td style="max-width:200px;">
                    <?php
                    foreach ($order['Orderrow'] as $descrizione) {
                        $printoggetto = $descrizione['Oggetto'];
                        if (strlen($printoggetto) > 40) {
                            echo substr($printoggetto, 0, 39) . '...' . ' <br/>';
                        } else {
                            echo $printoggetto . ' <br/>';
                        }
                    }
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $taxableIncome = $Utilities->getOrderTaxable($order['Order']['id']);
                    $totaleImponibile += $taxableIncome;
                    echo number_format($taxableIncome, 2, ',', '.');
                    ?>
                </td>
                <td style="text-align:right;">
                    <?php
                    $ivato = $Utilities->getOrderTotal($order['Order']['id']);
                    $ivato = $ivato;
                    $totaleIvato += $ivato;
                    echo number_format($ivato, 2, ',', '.'); ?>
                </td>
                <td>
                    <?php
                    if ($order['Order']['tipologia'] == 1)
                        echo "Ordine";
                    else
                        echo "Tentata Vendita";

                    $editLink = 'edit';
                    $pdfAction = 'ordinePdfExtendedVfs';
                    $sendMailAction = 'sendMailBill';
                    $color = 'rosso';
                    ?>
                </td>
                <td><?php
                    if($order['Payment']['id'] == '36')
                        echo 'Pagato';
                    else
                        echo 'Da pagare';

                    ?></td>
                <td class="actions" style="text-align:left;">
                    <?php

                    $cURLConnection = curl_init();
                    // Se la fattura è stata accontata non può essere più modificata
                    if ($order['Order']['bill_id'] == null) {
                        echo $this->Html->link($iconaModifica, ['action' => $editLink, $order['Order']['id']], ['title' => __('Modifica ordine'), 'escape' => false]);
                        if ($order['Order']['completed'] == 1 && $order['Order']['bill_id'] == null): ?>
                            <a title="Genera fattura"
                              onclick="createBill(<?= $order['Order']['id']; ?>)"><?= $iconaGeneraFattura ?></a>
                        <?php endif;
                        echo $this->Html->link('<i class="fa fa-copy" style="font-size:20px;vertical-align: middle;margin-right:10px;color:#589AB8;" title="Duplica ordine"></i>', ['action' => 'orderduplicate', $order['Order']['id'], $order['Order']['tipologia']], ['title' => __('Duplica ordine'), 'escape' => false]);

                        $tempTxt = $order['Client']['ragionesociale'] . '-' . str_replace("/", "", $order['Order']['numero_ordine']) . '_' . $this->Time->format('d_m_Y', $order['Order']['date']);
                        echo $this->Html->link($iconaPdf, ['action' => $pdfAction, $tempTxt, $order['Order']['id']], ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);
                        echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $order['Order']['id']], ['title' => __('Elimina ordine'), 'escape' => false], __('Siete sicuri di voler eliminare questo documento?', $order['Order']['id']));
                    }else{
                        $tempTxt = $order['Client']['ragionesociale'] . '-' . str_replace("/", "", $order['Order']['numero_ordine']) . '_' . $this->Time->format('d_m_Y', $order['Order']['date']);
                        echo $this->Html->link($iconaPdf, ['action' => $pdfAction, $tempTxt, $order['Order']['id']], ['target' => '_blank', 'title' => __('Scarica PDF'), 'escape' => false]);
                    }//$agents[$order['Order']['agent_id']]
                    ?>
                </td>
                <td><?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") {
                        if ($order['Order']['agent_id'] == 1) {
                            echo 'Simone Adoni';
                        }
                        if ($order['Order']['agent_id'] == 2) {
                            echo 'Fabio Ronconi';
                        }
                        if ($order['Order']['agent_id'] == 3) {
                            echo 'Lorenzo Gianola';
                        }
                        if ($order['Order']['agent_id'] == null) {
                            echo 'Non ci sono agenti';
                        }
                    }
                    if($_SESSION['Auth']['User']['dbname'] == "login_GE0041") {
                        if ($order['Order']['agent_id'] == 4) {
                            echo 'Fabio';
                        }
                        if ($order['Order']['agent_id'] == 5) {
                            echo 'Ronconi';
                        }
                        if ($order['Order']['agent_id'] == null) {
                            echo 'Non ci sono agenti';
                        }
                    }
                    ?></td>
                <td>
                    <?php
                    if ($order['Order']['bill_id'] == null) {
                        echo 'Non ancora fatturato';
                    } else {
                        echo 'Già fatturato';
                    }
                    ?>
                </td>
            </tr>
            <?php // }
        }
        ?>
        <tr>
            <?= $this->Graphic->drawVoidColumn(3); ?>
            <td><strong>Totale</strong></td>
            <td style="text-align:right;"><b><?= number_format($totaleImponibile, 2, ',', '.'); ?><b/></td>
            <td style="text-align:right;"><b><?= number_format($totaleIvato, 2, ',', '.'); ?><b/></td>
            <?= $this->Graphic->drawVoidColumn(2); ?>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<?= $this->element('Form/Components/Paginator/component'); ?>

<script>

    function createBill(id) {
        var rows = "<table style=\"width:100%;\">";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\"  style=\"height:15px;\"  id=\"check_fsp\"></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\">" + "Fattura split payment " + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\" checked=\"checked\" id=\"check_fel\" ></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\" >" + "Fattura elettronica " + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "<tr class=\"alertRow\">";
        rows = rows + "<td><input type=\"checkbox\" checked=\"checked\" style=\"height:15px;\"  id=\"check_detail\"></td>";
        rows = rows + "<td style=\"width:80%;\"><div style=\"text-align:left;font-size:13px;\">" + "Fattura dettagliata (indicazione rif. ddt per ogni fattura) " + "</div></td>";
        rows = rows + "</tr>";

        rows = rows + "<tr><td><br/></td></tr>";

        rows = rows + "</table>";

        Frizzysaveconfirm("", "Creazione fattura da ordine", function (userChoice) {
            if (userChoice == enhancedDialogsTypes.SAVE) {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "orders", "action" => "fatturaExtended"]) ?>" + "/" + id + "/" + $("#check_detail").prop("checked") + "/" + $("#check_fsp").prop("checked") + "/" + $("#check_fel").prop("checked"),
                    data:
                        {},
                    success: function (data) {
                        window.location.assign("<?= $this->Html->url(["controller" => "bills", "action" => "edit"]) ?>" + "/" + data);
                    },
                    error: function (data) {
                    },
                });
            }
        }, "<form id=\"scartaccinix\" ><br/><b>Creazione fattura da ordine</b><br/><br/>Seleziona le eventuali opzioni per la creazione della fattura<br/><br/>" + rows + '</form>')
    }
</script>

<?= $this->element('Js/datepickercode'); ?>
