<?php $_SESSION['Auth']['User']['dbname'] != 'login_GE0043' && $_SESSION['Auth']['User']['dbname'] != 'login_GE0033' ? $customize = false : $customize = true; ?>

<?php if(!$customize) : ?>
    <?= $this->element('pdfHeaders/logoeintestazione'); ?>
<?php endif; ?>

<!-- INTESTAZIONI -->
<table style="width:100%;border:0px solid;border-collapse: collapse;font-family: Courier New, Courier, monospace;font-size: 9pt;" >
    <tbody>
        <?php if($customize) : ?>
            <?= $this->element('pdfHeaders/customizelogoeintestazione'); ?>
        <?php endif; ?>
        <tr>
        <?php
            if($bill['Bill']['referredclient_id'] > 0 || $bill['Bill']['alternativeaddress_id'] > 0)
            {
                echo $this->element('pdfHeaders/default_bill_clientbox');
                echo "<td style='width:10mm;'></td>";
                echo $this->element('pdfHeaders/default_bill_second_address_box');
            }
            else if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                echo $this->element('pdfHeaders/default_bill_clientbox');
                echo "<td style='width:10mm;'></td>";
                echo "<td style='width:85mm;padding-right:10px;padding-top:5px;'></td>";
            }
            else
            {
                echo "<td style='width:85mm;padding-left:10px;padding-top:5px;'></td>";
                echo "<td style='width:10mm;'></td>";
                echo $this->element('pdfHeaders/default_bill_clientbox');
            }
        ?>
        </tr>
	</tbody>
</table>

<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>

<!-- Numero data e pagina -->
<?= $this->element('/pdfHeaders/default_bill_number_and_date'); ?>

<!-- SPAZIO -->


<!-- Pagamenti -->
<?= $this->element('/pdfHeaders/default_bill_payments'); ?>
