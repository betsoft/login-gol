<?= $this->element('pdfHeaders/logoeintestazione'); ?>

<!-- INTESTAZIONI -->
<table style ="width:100%;border:0px solid;border-collapse: collapse;font-family: Courier New, Courier, monospace;font-size: 9pt;" >
	<tbody>
		<tr >
			<td style="background-color:#cdcdcd;width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;">
				<span style="font-size: 8pt; color: #555555; font-family: sans;">Spett.le:</span>
				<br />
				<?php $conteggio = 1; ?>
				<?= $bill['Bill']['client_name']; ?>
				<?php strlen($bill['Bill']['client_name']) >40 ?  $conteggio++ : null;  ?>
				<br /><br />
				<?php strlen($bill['Bill']['client_address']) > 40 ? $conteggio += 2 : $conteggio += 1;  	?>
				<?= $bill['Bill']['client_address']; ?><br />
				<?php strlen($bill['Bill']['client_cap'] . ' ' . $bill['Bill']['client_city'] . ' ' . $bill['Bill']['client_province']. ' - ' .$bill['Nation']['name']) > 40 ? $conteggio += 2 : $conteggio +1; ?>
				<?= $bill['Bill']['client_cap'] . ' ' . $bill['Bill']['client_city'] . ' ' . $bill['Bill']['client_province']; ?>
				<?= isset($bill['Nation']['name']) ? ' - ' . $bill['Nation']['name'] : null;  ?>
				<br /><br />
				<?php
					for ($i =0; $i < 5-$conteggio; $i++)
					{
						echo '<br />';
					}
				?>
			</td>
			<td style="width:10mm;"></td>
			<td style="width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;"><span style="font-size: 8pt; color: #555555; font-family: sans;">Destinatario fattura </span>
				<br />
				<?php
				// Se è abilitata la gestione dei rivenditori aggiungo questo if
				if($bill['Bill']['referredclient_id'] > 0)
				{
						?>
						<?= $bill['Bill']['client_shipping_name']; ?><br /><br /><?= $bill['Bill']['client_shipping_address']; ?><br /><?= $bill['Bill']['client_shipping_cap'] . ' ' . $bill['Bill']['client_shipping_city'] . ' ' . $bill['Bill']['client_shipping_province']; ?>
						<?= isset($bill['Bill']['client_shipping_nation']) ? ' - ' . $bill['Bill']['client_shipping_nation'] : null;  ?>
						<br /><br />
						<?php
				}
				else
				{
					//if(count($bill['Client']['Clientdestination']) > 0)
					if($bill['Bill']['alternativeaddress_id'] > 0)
					{
						?>
						<?= $bill['Bill']['client_shipping_name']; ?><br /><br /><?= $bill['Bill']['client_shipping_address']; ?><br /><?= $bill['Bill']['client_shipping_cap'] . ' ' . $bill['Bill']['client_shipping_city'] . ' ' . $bill['Bill']['client_shipping_province']; ?>
						<?= isset($bill['Bill']['client_shipping_nation']) ? ' - ' . $bill['Bill']['client_shipping_nation'] : null;  ?>
						<br /><br />
					<?php
					}
					else // Riscrivo l'indirizzo
					{
						?>
						<?= $bill['Bill']['client_name'];?><br /><br /><?= $bill['Bill']['client_address']; ?><br /><?= $bill['Bill']['client_cap'] . ' ' . $bill['Bill']['client_city'] . ' ' . $bill['Bill']['client_province']; ?>
						<?= isset($bill['Nation']['name']) ? ' - ' . $bill['Nation']['name'] : null;  ?>
						<br /><br />
						<?php
					}
				}
				?>

			</td>
		</tr>
	</tbody>
</table>

<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>

<!-- NUMERO FATTURA con sezionali -->
<?php

//	isset($bill['Sectional']['prefix']) ? $prefissoSezionale = $bill['Sectional']['prefix'] : $prefissoSezionale = '';
	isset($bill['Sectional']['suffix']) ? $suffissoSezionale = $bill['Sectional']['suffix'] : $suffissoSezionale = '';

?>
<table style=" border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="width:111mm;"></td>
			<td style="width:39mm;border:1px solid black;">&nbsp;<?= $billTitle ?></td>
			<td style="width:4mm;"></td>
			<td style="width:20mm;border:1px solid black;text-align:center;">Data</td>
			<td style="width:4mm;"></td>
			<td style="width:15mm;border:1px solid black;text-align:center;">PAG. </td>
		</tr>
		<tr>
			<td style="width:96mm;"></td>
			<!--td style="width:56mm;border:1px solid black;text-align:right;font-weight:bold;"><span style="font-size:13px;"><?php // $prefissoSezionale . $bill['Bill']['numero_fattura'] . $suffissoSezionale,'/'. $this->Time->format('Y', $bill['Bill']['date']);  ?></span></td-->
			<!--td style="width:56mm;border:1px solid black;text-align:right;font-weight:bold;"><span style="font-size:13px;"><?php // $prefissoSezionale . $bill['Bill']['numero_fattura'] . $suffissoSezionale;  ?></span></td-->
			<td style="width:56mm;border:1px solid black;text-align:right;font-weight:bold;"><span style="font-size:13px;"><?= $bill['Bill']['numero_fattura'] . $suffissoSezionale;  ?></span></td>
			<td style="width:4mm;"></td>
			<td style="width:20mm;border:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?= $this->Time->format('d/m/Y', $bill['Bill']['date']);  ?></td>
			<td style="width:4mm;"></td>
			<td style="width:15mm;border:1px solid black;text-align:right;font-weight:bold;vertical-align:bottom;"><?= '{PAGENO} / {nbpg}' ?></td>
		</tr>

	</tbody>
</table>
<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<!-- HEADE CENTRALE -->
<table style=" border-collapse: collapse;font-size:9pt;" >
	<tbody>
		<tr>
			<!--td style="width:55mm;border:1px solid black;">DIVISA <B>EUR</B></td>
			<td style="width:4mm;"></td-->
			<td  style="width:195mm;border:1px solid black;font-size:10pt;padding-left:2mm;"><span><b>Cod. cliente: </b></span> <span style="font-size:9pt;"><?= $bill['Client']['code']; ?></span>&nbsp;&nbsp; <b>P.iva: </b>   <span style="font-size:9pt;"><?=  $bill['Client']['piva']?></span>&nbsp;&nbsp;<b>Codice fiscale:</b>  <span style="font-size:9pt;"><?=  $bill['Client']['cf']?></span></td>
		</tr>
		<tr>

			<td colspan ="5" style="widht:195mm;border-left:1px solid black;border-right:1px solid black;font-size:10pt;padding-left:2mm;"><span><strong>Pagamento</strong></span></td></tr>
		<?php
			// Se è una riba
			if($bill['Payment']['riba'] == 1)
			{
		?>
		<tr>
				<td style="border-right:1px solid black;border-left:1px solid black;padding-left:2mm;">
						<?=  $bill['Payment']['Bank']['description']
						?></td></tr>
						<tr>
						<td style="padding-left:2mm;border-left:1px solid black;border-right:1px solid black;font-size:10pt;margin-right:2mm;border-bottom:1px solid black;"><?= ' ABI ' .$bill['Payment']['Bank']['abi'] . ' CAB ' . $bill['Payment']['Bank']['cab'] ; ?></td>
						</tr>
				</td></tr>

		<?php
			}
			else
			{
			?>
			<tr>
				<td style="border-right:1px solid black;border-left:1px solid black;padding-left:2mm;font-size:10pt;">
						<?=  isset($bill['Payment']['metodo']) ? $bill['Payment']['metodo'] : '<br />' ?></td></tr>
						<tr>
							<td style="padding-left:2mm;border-left:1px solid black;border-right:1px solid black;font-size:10pt;margin-right:2mm;border-bottom:1px solid black;"><?= '<b> Banca bonifico: </b>' . strtolower($bill['Payment']['Bank']['description']). '<b> IBAN: </b>' . strtoupper($bill['Payment']['Bank']['iban']); ?></td>
						</tr>
				</td></tr>
		<?php
			}
		?>

	</tbody>
</table>




