<?= $this->element('pdfHeaders/logoeintestazione'); ?>

<!-- INTESTAZIONI -->
<table style ="width:100%;border-collapse: collapse;" >
    <tbody>
    <tr>
        <td style="width:10mm;"></td>
        <td style="width:100mm; padding-left:10px;padding-top:5px;">
        <td style="width:50mm; padding-left:10px;padding-top:5px;"><span style="font-size: 8pt; color: #555555; font-family: sans;">Spett.le:</span><br />
            <strong><?= $order['Order']['client_name']; ?></strong><br />
            <?= $order['Order']['client_address']; ?><br />
            <?= $order['Order']['client_cap'] . ' ' . $order['Order']['client_city'] . ' ' . $order['Order']['client_province']; ?>
        </td>


        </td>
    </tr>
    </tbody>
</table>
<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<span style="font-size: 12px;">Ordine N. <?= $order['Order']['numero_ordine'],'/'. $this->Time->format('Y', $order['Order']['date']) .' del '. $this->Time->format('d/m/Y',$order['Order']['date']); ?></span>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
