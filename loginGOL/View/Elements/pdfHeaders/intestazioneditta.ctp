<b><?= isset($settings['Setting']['name']) ? $settings['Setting']['name'] : null; ?></b>
<br />
<?= isset($settings['Setting']['indirizzo']) ? $settings['Setting']['indirizzo'] : null; ?>
<?php
    if(isset($settings['Setting']['note']) && $settings['Setting']['note'] != '')
        echo ' - ' ;
    else
    {
        if(strlen($settings['Setting']['name']) < 60)
            echo '<br/>';
        else
            echo ' - ';
    }
?>

<?= isset($settings['Setting']['cap']) ? $settings['Setting']['cap'] : null; ?>
<?= isset($settings['Setting']['citta']) ? ' ' .$settings['Setting']['citta'] : null; ?>
<?= isset($settings['Setting']['prov']) ? ' ('.$settings['Setting']['prov'] .')' : null; ?>
<br />

<?php $void = 0; ?>
<?php if(isset($settings['Setting']['tel']) && $settings['Setting']['tel'] != '') { echo 'tel: '.$settings['Setting']['tel'];   } else { $void++; }  ?>
<?php if(isset($settings['Setting']['fax']) && $settings['Setting']['fax'] != '') {  echo ' fax: '.$settings['Setting']['fax'];   } else { $void++; }  ?>
<?php if(isset($settings['Setting']['email']) && $settings['Setting']['email'] != '') { echo ' email: '.$settings['Setting']['email']; } else { $void++; }  ?>

<?php
    $bp1 = '';

    if($void == 3)
    {
        $newVar = 0;
        if(isset($settings['Setting']['piva']) && $settings['Setting']['piva'] != '')
        {
            $newVar = 1;
            echo ' P.IVA: '.$settings['Setting']['piva'];
        }

        if(isset($settings['Setting']['cf']) && $settings['Setting']['cf'] != '')
        {
            $newVar = 1;
            echo ' CF: '.$settings['Setting']['cf'];
        }

        if(isset($settings['Setting']['share_capital']) && $settings['Setting']['share_capital'] != 0)
        {
            echo "<br/>";
            echo "Cap Soc: €".$settings['Setting']['share_capital'];

            if($settings['Setting']['partner_unique'] == 'SU')
                echo ' - La società è a socio unico';
            if($settings['Setting']['partner_unique'] == 'SM')
                echo ' - La società NON è a socio unico';
        }

        if ($newVar == 1 )
            $bp1 = '<br/>';
    }
    else
    {
        echo '<br/>';

        if(isset($settings['Setting']['piva']) && $settings['Setting']['piva'] != '')
            echo ' P.IVA: '.$settings['Setting']['piva'];

        if(isset($settings['Setting']['cf']) && $settings['Setting']['cf'] != '')
            echo ' CF: '.$settings['Setting']['cf'];

        if(isset($settings['Setting']['share_capital']) && $settings['Setting']['share_capital'] != 0)
        {
            echo "<br/>";
            echo "Cap Soc: €".$settings['Setting']['share_capital'];

            if($settings['Setting']['partner_unique'] == 'SU')
                echo ' - La società è a socio unico';
            if($settings['Setting']['partner_unique'] == 'SM')
                echo ' - La società NON è a socio unico';
        }

        $bp1 = '';
    }

    if(isset($settings['Setting']['note']) && $settings['Setting']['note'] != '')
        echo  '<br/>' . $settings['Setting']['note'];
    else
        echo '<br/>';

    echo $bp1;
?>
