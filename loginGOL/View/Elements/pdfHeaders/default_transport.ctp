<?php $_SESSION['Auth']['User']['dbname'] != 'login_GE0043' && $_SESSION['Auth']['User']['dbname'] != 'login_GE0033' ? $customize = false : $customize = true; ?>

<?php if(!$customize) : ?>
    <?= $this->element('pdfHeaders/logoeintestazione'); ?>
<?php endif; ?>

<!-- INTESTAZIONI -->
<table style ="width:100%;border:0px solid;border-collapse: collapse;font-size: 9pt;font-family: Courier New, Courier, monospace;max-height:35mm;height:35mm;" >
    <tbody>
    <?php if($customize) : ?>
        <?= $this->element('pdfHeaders/customizelogoeintestazione'); ?>
    <?php endif; ?>
    <tr>
        <?php

        if( $transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7)
        {
        ?>

        <td style="background-color:#cdcdcd;width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;">
            <span style="font-size: 8pt; color: #555555; font-family: sans;">Spett.le:</span>
            <br />
            <?php $conteggio = 1; ?>
            <?= $transport['Transport']['client_name']; ?>
            <?= strlen($transport['Transport']['client_name']) >40 ?  $conteggio++ : null;  ?>
            <br /><br />
            <?php strlen($transport['Transport']['client_address']) > 40 ? $conteggio += 2 : $conteggio += 1;  	?>
            <?= $transport['Transport']['client_address']; ?><br />
            <?php
            strlen($transport['Transport']['client_cap'] . ' ' . $transport['Transport']['client_city'] . ' ' . $transport['Transport']['client_province']. ' - ' .$transport['Nation']['name']) > 40 ? $conteggio += 2 : $conteggio +1; ?>
            <?= $transport['Transport']['client_cap'] . ' ' . $transport['Transport']['client_city'] . ' ' . $transport['Transport']['client_province']; ?>
            <?php //  isset($transport['Nation']['name']) ? ' - ' . $transport['Nation']['name'] : null;  ?>
            <?=  $clientNation  ?>

            <br /><br />
            <?php
            }

            if( $transport['Transport']['causal_id'] == 3 || $transport['Transport']['causal_id'] == 7 )
            {
            ?>
        <td style="background-color:#cdcdcd;width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;">
            <span style="font-size: 8pt; color: #555555; font-family: sans;">Spett.le:</span>
            <br />
            <?php $conteggio = 1; ?>
            <?= $transport['Transport']['supplier_name']; ?>
            <?= strlen($transport['Transport']['supplier_name']) >40 ?  $conteggio++ : null;  ?>
            <br /><br />
            <?php strlen($transport['Transport']['supplier_address']) > 40 ? $conteggio += 2 : $conteggio += 1;  	?>
            <?= $transport['Transport']['supplier_address']; ?><br />
            <?php strlen($transport['Transport']['supplier_cap'] . ' ' . $transport['Transport']['supplier_city'] . ' ' . $transport['Transport']['supplier_province']. ' - ' . $transport['Nation']['name']) > 40 ? $conteggio += 2 : $conteggio +1; ?>
            <?= $transport['Transport']['supplier_cap'] . ' ' . $transport['Transport']['supplier_city'] . ' ' . $transport['Transport']['supplier_province']; ?>
            <?php // isset($transport['Nation']['name']) ? ' - ' . $transport['Nation']['name'] : null;  ?>
            <?=  $supplierNation  ?>
            <br /><br />
            <?php
            }

            for ($i =0; $i < 5-$conteggio; $i++)
            {
                echo '<br />';
            }
            ?>
        </td>
        <td style="width:10mm;"></td>
        <td style="width:85mm;border: 0.2mm solid #000000; padding-left:10px;padding-top:5px;">
            <span style="font-size: 8pt; color: #555555; font-family: sans;">Destinatario della merce (se diverso dall'intestatario) </span>
            <br />
            <?php
            // Se è abilitata la gestione dei rivenditori aggiungo questo if
            /*	if($transport['Transport']['referredclient_id'] > 0)
                {
                        ?>
                        <?= $transport['Transport']['client_shipping_name']; ?><br /><br /><?= $transport['Transport']['client_shipping_address']; ?><br /><?= $transport['Transport']['client_shipping_cap'] . ' ' . $transport['Transport']['client_shipping_name'] . ' ' . $transport['Transport']['client_shipping_province']; ?>
                        <?= isset($transport['Transport']['client_shipping_nation']) ? ' - ' . $transport['Transport']['client_shipping_nation'] : null;  ?>
                        <br /><br />
                        <?php
                }
                else
                {		*/
            if($transport['Transport']['alternativeaddress_id'] > 0)
            {
                ?>
                <?= $transport['Transport']['client_shipping_name']; ?><br /><br /><?= $transport['Transport']['client_shipping_address']; ?><br /><?= $transport['Transport']['client_shipping_cap'] . ' ' . $transport['Transport']['client_shipping_city'] . ' ' . $transport['Transport']['client_shipping_province']; ?>
                <?php // isset($transport['Transport']['client_shipping_nation']) ? ' - ' . $transport['Transport']['client_shipping_nation'] : null;  ?>
                <?= $clientShippingNation  ?>
                <br /><br />
                <?php
            }

            if($transport['Transport']['supplier_alternativeaddress_id'] > 0)
            {
                ?>
                <?= $transport['Transport']['supplier_shipping_name']; ?><br /><br /><?= $transport['Transport']['supplier_shipping_address']; ?><br /><?= $transport['Transport']['supplier_shipping_cap'] . ' ' . $transport['Transport']['supplier_shipping_city'] . ' ' . $transport['Transport']['supplier_shipping_province']; ?>
                <?php // isset($transport['Transport']['supplier_shipping_nation']) ? ' - ' . $transport['Transport']['supplier_shipping_nation'] : null;  ?>
                <?= $supplierShippingNation  ?>
                <br /><br />
                <?php
            }

            // }
            ?>

        </td>
    </tr>
    </tbody>
</table>
<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<!-- DATI CLIENTE PRIMA RIGA -->
<table	style="border:0.1px solid black;font-size: 9pt; border-collapse: collapse;width:100%" >
    <thead>
    <tr>
        <th class="littleFont" width="14%"><?= $transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7 ? 'Codice cliente' : 'Codice fornitore' ?></th>
        <th class="littleFont" width="30%"></th>
        <th class="littleFont" width="28%">Causale</th>
        <th class="littleFontcenter" width="14%">Numero documento</th>
        <th class="littleFontcenter" width="14%">Data documento</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="normalFont"><?= $transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7 ? $transport['Client']['code'] : $transport['Supplier']['code']; ?></td>
        <td class="normalFontcenter" style="padding-top:-6px;">Documento di trasporto</td>
        <td class="normalFontcenter"><?= $transportCausal; ?></td>
        <td class="normalFontcenter"><?= $transport['Transport']['transport_number']; ?></td>
        <td class="normalFontcenter"><?= $this->Time->format('d/m/Y', $transport['Transport']['date']); ?></td>
    </tr>
    </tbody>
</table>
<!-- DATI CLIENTE SECONDA RIGA -->
<table	style="border-left:0.1px solid black;border-right:0.1px solid black;border-bottom:0.1px solid black;font-size: 9pt; border-collapse: collapse;width:100%" >
    <thead>
    <tr>
        <th class="littleFont" width="40%" >Codice fiscale</th>
        <th class="littleFont" width="40%">P. IVA</th>
        <th class="littleFontLast" width="20%">Telefono</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <?php if($transport['Transport']['causal_id'] != 3 && $transport['Transport']['causal_id'] != 7) { ?>
            <td class="normalFont"><?= (isset($transport['Client']['cf']) && ($transport['Client']['cf'] != '')) ? $transport['Client']['cf'] : '<br />'; ?></td>
            <td class="normalFont"><?=  $transport['Client']['piva'] ; ?>
            <td class="normalFontLast"><?= $transport['Client']['telefono']; ?></td>
        <?php }
        else
        {
            ?>
            <td class="normalFont"><?= (isset($transport['Supplier']['cf']) && ($transport['Supplier']['cf'] != '')) ? $transport['Supplier']['cf'] : '<br />'; ?></td>
            <td class="normalFont"><?=  $transport['Supplier']['piva'] ; ?>
            <td class="normalFontLast"><?= $transport['Supplier']['telefono']; ?></td>
            <?php
        }
        ?>
        </td>
    </tr>
    </tbody>
</table>
