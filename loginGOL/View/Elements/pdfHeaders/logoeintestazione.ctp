<?php
	// Se è presente un logo
	if(isset($settings['Setting']['header']) && $settings['Setting']['header'] != '')
	{
		// Se è settato non visualizzazione automatica dati azienda
		if($settings['Setting']['bill_print_society_data_visible'] == 0)
		{
                switch($settings['Setting']['bill_print_logo_align'])
                {
                    case 0:
                       ?>
                <table width="100%" style="" cellpadding="10" style="font-size: 8.5pt;font-family: serif;">
                    <tr>
                        <td width="40%" height="10%" style="background-color:border: 0mm solid #000;float:left;" ><img style="max-height:18mm;height:18mm;min-height:18mm;" src="img/societa/<?= $companyLogo ?>"></td>
                        <td width="60%" style="border: 0mm solid #000;float:right;margin-top:-30px;" ></td>
                    </tr>
                </table>
                <?php
                    break;
                    case 1:
                ?>
                    <table width="100%" style="" cellpadding="10" style="font-size: 8.5pt;font-family: serif;">
                        <tr>
                            <td style="text-align:center;">
                                <img style="max-height:18mm;height:18mm;min-height:18mm;" src="img/societa/<?= $companyLogo ?>">
                            </td>
                        </tr>
                    </table>
                <?php
                    break;
                    case 2:
                ?>
                    <table width="100%" style="" cellpadding="10" style="font-size: 8.5pt;font-family: serif;">
                        <tr>
                            <td width="40%" style="border: 0mm solid #000;margin-top:-30px;" ></td>
                            <td width="60%" style="border: 0mm solid #000;text-align:right;" >
                                <img style="0px;max-height:18mm;height:18mm;min-height:18mm;" src="img/societa/<?= $companyLogo ?>">
                            </td>
                        </tr>
                    </table>
                <?php
                    break;
                }
		}
		else
		{
		    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){ ?>
                <table width="100%" cellpadding="10" style="font-size: 8.5pt;font-family: serif;">
                    <tr>
                        <td width="40%" style="border: 0mm solid #000;float:left;" ><img style="max-height:30mm;height:30mm;" src="img/societa/<?= $companyLogo ?>"></td>
                        <td width="60%" style="border: 0mm solid #000;float:right;margin-top:-30px;" ><?=  $this->element('pdfHeaders/intestazioneditta'); ?></td>
                    </tr>
                </table>
		    <?php
		    }else{
		    ?>
                <table width="100%" cellpadding="10" style="font-size: 8.5pt;font-family: serif;">
                    <tr>
                        <td width="40%" style="border: 0mm solid #000;float:left;" ><img style="max-height:18mm;height:18mm;" src="img/societa/<?= $companyLogo ?>"></td>
                        <td width="60%" style="border: 0mm solid #000;float:right;margin-top:-30px;" ><?=  $this->element('pdfHeaders/intestazioneditta'); ?></td>
                    </tr>
                </table>
		    <?php
		    }
		}
	}
	else // Mancanza del logo
	{
		switch($settings['Setting']['logoalign'])
		{
		    // Allineamento scritte a sinistra
            case 0:
                ?>
                <table width="100%" style="font-size: 8.5pt;font-family: serif;margin-left:-3mm;" cellpadding="10"  >
                    <tr>
                        <td width="100%" style="border: 0mm solid #000;text-aling:left;" >
                            <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                            <br/>
                        </td>
                    </tr>
                </table>
                <?php
            break;
            // Allineamento scritte centrato
            case 1:
                ?>
                <table width="100%" style="font-size: 8.5pt;font-family: serif;" cellpadding="10" >
                <tr>
                    <td width="100%" style="border: 0mm solid #000;text-align:center;" >
                        <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                        <br/>
                    </td>
                </tr>
                </table>
                <?php
            break;
            // Allineamento scritte a destra
            case 2:
                ?>
                <table width="100%" style="font-size: 8.5pt;font-family: serif;" cellpadding="10" >
                    <tr>
                        <td width="100%" style="border: 0mm solid #000;text-aling:right;" >
                            <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                            <br/>
                        </td>
                    </tr>
                </table>
                <?php
            break;
        }
	}
?>
