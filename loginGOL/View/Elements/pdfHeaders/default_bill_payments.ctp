<?php
isset($bill['Payment']['metodo']) ? $metodoDiPagamento = $bill['Payment']['metodo'] : $metodoDiPagamento = '';

if($bill['Payment']['riba'] == 1)
{
    $title1 = 'ABI';
    $title2 = 'CAB';
    $value1 = isset($bill['Client']['Bank']['abi']) ? $bill['Client']['Bank']['abi'] : '<br/>';
    $value2 = isset($bill['Client']['Bank']['cab']) ? $bill['Client']['Bank']['cab'] : '<br/>';
    if($bill['Bill']['alternative_bank_id'] != 0){
        $value3 = isset($alternativeBank['Bank']['abi']) ? $alternativeBank['Bank']['abi'] : '<br/>';
        $value4 = isset($alternativeBank['Bank']['cab']) ? $alternativeBank['Bank']['cab'] : '<br/>';
        $title3 = 'ABI';
        $title4 = 'CAB';
    }
}
else
{
    if($bill['Bill']['bank_id'] != 0)
    {
        $title1 = 'Banca';
        $title2 = 'Iban';
        $title5 = 'BIC';
        $value1 = $banca['Bank']['description'];
        $value2 = $banca['Bank']['iban'];
        $value5 = $banca['Bank']['bic'];
        if($bill['Bill']['alternative_bank_id'] != 0){
            $value3 = $alternativeBank['Bank']['description'];
            $value4 = $alternativeBank['Bank']['iban'];
            $value6 = $alternativeBank['Bank']['bic'];
            $title3 = 'Banca alt';
            $title4 = 'Iban alt';
            $title6 = 'BIC alt';
            if (strlen($value3) > 34) {
                $title3 = $title3 . ': <b>' . substr($value3, 0, 20)  . '</b>';
                $value3 = substr($value3, 20, strlen($value3));
            }
        }

        if (strlen($value1) > 34) {
            $title1 = $title1 . ': <b>' . substr($value1, 0, 25) . '</b>' ;
            $value1 = substr($value1, 25, strlen($value1));
        }
    }
    else
    {
        $title1 = 'Banca';
        $title2 = 'Iban';
        $title5 = 'BIC';
        $title3 = 'Banca alt';
        $title4 = 'Iban alt';
        $title6 = 'BIC alt';
        $value1 = isset($bill['Payment']['Bank']['description']) ? $bill['Payment']['Bank']['description'] : '<br/>';
        $value2 = isset($bill['Payment']['Bank']['iban']) ? $bill['Payment']['Bank']['iban'] : '<br/>';
        $value5 = isset($bill['Payment']['Bank']['bic']) ? $bill['Payment']['Bank']['bic'] : '<br/>';

        $value3 = isset($bill['Payment']['alternative_bank_id']) ? $alternativeBank['Bank']['description'] : '<br/>';
        $value4 = isset($bill['Payment']['alternative_bank_id']) ? $alternativeBank['Bank']['iban'] : '<br/>';
        $value6 = isset($bill['Payment']['alternative_bank_id']) ? $alternativeBank['Bank']['bic'] : '<br/>';

        if (strlen($value1) > 35)
        {
            $title1 = $title1 . ': <b>' . substr($value1, 0, 25) . '</b>' ;
            $value1 = substr($value1, 25, strlen($value1));
        }

        if (strlen($value3) > 35)
        {
            $title3 = $title3 . ': <b>' . substr($value3, 0, 20)  . '</b>';
            $value3 = substr($value3, 20, strlen($value3));
        }
    }
}
?>
<table style=" border-collapse: collapse; font-size: 9pt;">
    <tbody>
    <tr>
        <td style="width:50mm;border-top:1px solid black;border-left:1px solid black;text-align:center;">Pagamento </td>
        <td style="width:70mm;border-top:1px solid black;border-left:1px solid black;text-align:center;"><?= strtolower($title1) ?></td>
        <td style="width:70mm;border-top:1px solid black;border-left:1px solid black;text-align:center;border-right:1px solid black"><?= strtoupper($title2) ?></td>
        <?php if(($value5 == "<br/>" || $value5 == null) && ($value6 == "<br/>" || $value6 == null)){}else{?><td style="width:70mm;border-top:1px solid black;border-left:1px solid black;text-align:center;border-right:1px solid black;"><?= strtoupper($title5) ?></td><?php }?>
    </tr>
    <tr>
        <td style="width:50mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;padding-right:5px;">
            <span style="font-size:13px;"><?= strtolower($metodoDiPagamento); ?></span>
        </td>
        <td  style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?= strtoupper($value1)  ?></td>
        <td style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;border-right:1px solid black;"><?=  strtoupper($value2)  ?></td>
        <?php if(($value5 == "<br/>" || $value5 == null) && ($value6 == "<br/>" || $value6 == null)){}else{?><td style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;border-right:1px solid black;"><?=  strtoupper($value5) ?></td><?php }?>
    </tr>
    <?php if (isset($bill['Bill']['alternative_bank_id'])): ?>
        <?php $widhtDefault = 100?>
        <tr>
            <td style="width:71mm;border-top:1px solid black;border-left:1px solid black;text-align:center;"></td>
            <td style="width:70mm;border-top:1px solid black;border-left:1px solid black;text-align:center;"><?= strtolower($title3) ?></td>
            <td style="width:70mm;border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;text-align:center;"><?= strtolower($title4) ?></td>
            <?php if(($value5 == "<br/>" || $value5 == null) && ($value6 == "<br/>" || $value6 == null)){}else{?><td style="width:70mm;border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;text-align:center;"><?= strtolower($title6) ?></td><?php }?>
        </tr>
        <tr>
            <td style="width:71mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;padding-right:5px;">
            </td>
            <td  style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;"><?= strtoupper($value3)  ?></td>
            <td style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;border-right:1px solid black;"><?=  strtoupper($value4)  ?></td>
            <?php if(($value5 == "<br/>" || $value5 == null) && ($value6 == "<br/>" || $value6 == null)){}else{?><td style="width:70mm;border-left:1px solid black;border-bottom:1px solid black;text-align:center;font-weight:bold;vertical-align:bottom;font-size:13px;border-right:1px solid black;"><?=  strtoupper($value6)  ?></td><?php  } ?>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
