<?= $this->element('pdfHeaders/logoeintestazione'); ?>

<?php
    $T_Bordered = "width:40%;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;text-align:center;padding-left:10px;padding-top:5px;background-color: #CDCDCD";
    $LR_Bordered = "width:50mm;border-left: 1px solid black;border-right: 1px solid black;padding-left:5px;";
    $LRB_Bordered = "width:50mm;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;padding-left:5px;";
?>

<!-- INTESTAZIONI -->
<br/>
<table style ="border-collapse: collapse;width:100%;" >
	<tbody>
        <tr>
            <td style="<?= $T_Bordered ?>;"><b>CLIENTE</b></span><br /></td>
            <td style="width:10%"></td>
            <td style="<?= $T_Bordered ?>"><b>SCHEDA D'INTERVENTO</b></span></td>
        </tr>
        <tr>
            <td style="<?= $LR_Bordered; ?>">&nbsp;</td>
            <td style="width:10mm;">&nbsp;</td>
            <td style="<?= $LR_Bordered; ?>"></td>
        </tr>
        <tr>
            <td style="<?= $LR_Bordered; ?>"><?= $maintenance['Client']['ragionesociale']; ?></td>
            <td style="width:10mm;">&nbsp;</td>
            <td style="<?= $LR_Bordered; ?>">N. <?= $maintenance['Maintenance']['maintenance_number']; ?></td>
        </tr>
        <tr>
            <td style="<?= $LR_Bordered; ?>"><?= $maintenance['Client']['indirizzo']; ?></td>
            <td style="width:10mm;">&nbsp;</td>
            <td style="<?= $LR_Bordered; ?>">Del <?= date('d-m-Y',strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
        </tr>
        <tr>
        <?php
            $provincia = '';
            if($maintenance['Client']['provincia'] != '')
                $province = ' ('.$maintenance['Client']['provincia'].')';
        ?>
            <td style="<?= $LRB_Bordered ?>"><?= $maintenance['Client']['cap'] . ' ' . $maintenance['Client']['citta'] ; ?></td>
            <td style="width:10mm;"></td>
            <td style="<?= $LRB_Bordered ?>">&nbsp;</td>
        </tr>
	</tbody>
</table>