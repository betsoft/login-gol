<?php
	// Se è presente un logo
	if(isset($settings['Setting']['header']) && $settings['Setting']['header'] != '')
	{
	    if($_SESSION['Auth']['User']['dbname'] == 'login_GE0043')
	    {
            ?>
        <tr>
            <td width="40%" colspan="2" style="border: 0mm solid #000;float:left;" ><img style="height:4cm;" src="img/societa/<?= $companyLogo ?>"></td>
            <td width="60%" style="border: 0mm solid #000;float:right;font-size: 13px;"><?= $this->element('pdfHeaders/intestazionedittadolce'); ?></td>
        </tr>
            <?php
        }
	    else
        {
            // Se è settato non visualizzazione automatica dati azienda
            if($settings['Setting']['bill_print_society_data_visible'] == 0)
            {
                switch($settings['Setting']['bill_print_logo_align'])
                {
                    case 0:
                        ?>
                        <tr>
                            <td width="60%" style="border: 0mm solid #000;text-align:left;">
                                <img style="height:3.5cm;" src="img/societa/<?= $companyLogo ?>">
                            </td>
                        </tr>
                        <?php
                        break;
                    case 1:
                        ?>
                        <tr>
                            <td width="60%" style="border: 0mm solid #000;text-align:center;">
                                <img style="height:3.5cm;" src="img/societa/<?= $companyLogo ?>">
                            </td>
                        </tr>
                        <?php
                        break;
                    case 2:
                        ?>
                        <tr>
                            <td width="60%" style="border: 0mm solid #000;text-align:right;">
                                <img style="height:3.5cm;" src="img/societa/<?= $companyLogo ?>">
                            </td>
                        </tr>
                        <?php
                    break;
                }
            }
            else
            {
                ?>
                <tr>
                    <td width="40%" colspan="2" style="border: 0mm solid #000;float:left;" ><img style="height:3.5cm;" src="img/societa/<?= $companyLogo ?>"></td>
                    <td width="60%" style="border: 0mm solid #000;float:right;font-size: 13px;"><?= $this->element('pdfHeaders/intestazionedittafireservice'); ?></td>
                </tr>
            <?php
            }
	    }
	}
	else // Mancanza del logo
	{
		switch($settings['Setting']['logoalign'])
		{
            // Allineamento scritte a sinistra
            case 0:
                ?>
                <tr>
                    <td width="100%" style="border: 0mm solid #000;text-aling:left;" >
                        <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                        <br/>
                    </td>
                </tr>
                <?php
            break;
            // Allineamento scritte centrato
            case 1:
                ?>
                <tr>
                    <td width="100%" style="border: 0mm solid #000;text-align:center;" >
                        <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                        <br/>
                    </td>
                </tr>
                <?php
            break;
            // Allineamento scritte a destra
            case 2:
                ?>
                <tr>
                    <td width="100%" style="border: 0mm solid #000;text-aling:right;" >
                        <?= $this->element('pdfHeaders/intestazioneditta'); ?>
                        <br/>
                    </td>
                </tr>
                <?php
            break;
	    }
	}
?>