<?= $this->element('pdfHeaders/logoeintestazione'); ?>

<!-- INTESTAZIONI -->
<table style ="width:100%;border:0px solid;border-collapse: collapse;font-family: Courier New, Courier, monospace;font-size: 9pt;" >
    <tbody>
    <tr >
        <?php
        if($bill['Bill']['referredclient_id'] > 0 || $bill['Bill']['alternativeaddress_id'] > 0)
        {
            echo $this->element('pdfHeaders/default_bill_clientbox');
        }
        ?>
        <td style="width:10mm;"></td>
        <?php
        if($bill['Bill']['referredclient_id'] > 0 || $bill['Bill']['alternativeaddress_id'] > 0)
        {
            echo $this->element('pdfHeaders/default_bill_second_address_box');
        }
        else
        {
            ?>
            <td style="width:85mm;padding-left:10px;padding-top:5px;"></td>
            <?php
            echo $this->element('pdfHeaders/default_bill_clientbox');
        }
        ?>
    </tr>
    </tbody>
</table>

<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>

<!-- Numero data e pagina -->
<?= $this->element('/pdfHeaders/default_bill_number_and_date'); ?>

<!-- SPAZIO -->
<table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>

<!-- Pagamenti -->
<?= $this->element('/pdfHeaders/default_bill_payments'); ?>