<?php
	$littleFont = '6pt;';
	$normalFont = '8pt;';
	$bigfont = '10pt;'
?>
	
<style>

body {font-family: sans-serif;
    font-size: 10pt;
}
p {    margin: 0pt;
}
td { vertical-align: top; }
.items td {
    border-left: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
    text-align: center;
    border: 0.1mm solid #000000;
}
.items td.blanktotal {
    background-color: #FFFFFF;
    border: 0mm none #000000;
    border-top: 0.1mm solid #000000;
    border-right: 0.1mm solid #000000;
}
.items td.totals {
    text-align: right;
    border: 0.1mm solid #000000;
}

.items td.white {
   
    border: 0.0mm solid #ffffff;
}

.littlefont
{
	font-size:7pt;	
	text-align:left;
	border-right: 0.1mm solid #000000;
}

.littlefontNoRight
{
	font-size:7pt;	
	text-align:left;
}

.littleFontLast
{
	font-size:<?= $littleFont; ?>
	text-align:left;
}

.littleFontcenter
{
	font-size:<?= $littleFont; ?>;	
	text-align:center;
	/*border-left: 0.1mm solid #000000;
	border-top: 0.1mm solid #000000;*/
	border-right: 0.1mm solid #000000;
}

.littleFontcenternoright
{
	font-size:<?= $littleFont; ?>;	
	text-align:center;
}

.littleFontNoBottomBordered
{
	font-size:<?= $littleFont; ?>;	
	/*border-bottom:0px;*/
	border-bottom:none;
}

.littleFontNoBottomBorderedLast
{
	font-size:<?= $littleFont; ?>;	
	border-bottom:none;
}

.normalFont
{
	font-size:<?= $normalFont; ?>;
	border-right: 0.1mm solid #000000;
}

.normalFontRight
{
	font-size:<?= $normalFont; ?>;
	border-right: 0.1mm solid #000000;
	text-align:right;
	padding-right:5px;
}

.normalFontLast
{
	font-size:<?= $normalFont; ?>;
	border-right: 0.1mm solid #000000;
}

.normalFontnoright
{
	font-size:<?= $normalFont; ?>;
}

.normalFontcenter
{
	font-size:<?= $normalFont; ?>;
/*	border-left: 0.1mm solid #000000;*/
	border-right: 0.1mm solid #000000;
	text-align:center;
}

.normalFontcenterLast
{
	font-size:<?= $normalFont; ?>;
	text-align:center;
}

.pageNumber:before
{
	content:counter(page);
}

.noborder
{
	border : 0px solid;	
}
</style>