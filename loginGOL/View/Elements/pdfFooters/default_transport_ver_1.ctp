<!-- NUOVO -->
<?php
	isset($transport['Transport']['collectionfees']) ? $speseincasso = $transport['Transport']['collectionfees'] :$speseincasso = ''; 
	isset($transport['Transport']['seal']) ? $seal = $transport['Transport']['seal'] :$seal = '';
?>

<table style ="border-collapse: collapse;font-size:8pt;margin-bottom:10px;"  width="100%"  >
	<tbody>
		<tr>
			<td style="width:5mm;text-align:center;border:1px solid black;">Colli</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">Peso (kg)</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">Volume (mc)</td>
			<td style="width:25mm;text-align:center;border:1px solid black;">Trasporto a cura del</td>
			<td style="width:25mm;text-align:center;border:1px solid black;">Aspetto dei beni</td>
		</tr>
		
		<tr>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($transport['Transport']['num_colli']) ? $transport['Transport']['num_colli'] : '' ?> </td>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($transport['Transport']['weight']) ? number_format($transport['Transport']['weight'],2,',','.') : '' ?></td>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($transport['Transport']['volume']) ? number_format($transport['Transport']['volume'],2,',','.') : '' ?></td>
			<td style="width:25mm;text-align:center;border:1px solid black;">
				<?php
				 switch ($transport['Transport']['transporter'])
				 {
					case 0: echo 'Mittente'; break;
					case 1: echo 'Destinatario'; break;
					case 2: echo 'Vettore'; break;
				 }
				 ?>
			</td>
			<td style="width:25mm;text-align:center;border:1px solid black;"><?= $transport['Transport']['aspect']; ?></td>
		
		</tr>
		<tr>
			<td colspan="2" style="width:10mm;text-align:center;border:1px solid black;">VETTORE</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">DATA PARTENZA</td>
			<td colspan="1" style="width:18mm;text-align:center;border-right:1px solid black;">FIRMA MITTENTE</td>
            <td colspan="1" style="width:18mm;text-align:center;border-right:1px solid black;">FIRMA DESTINATARIO</td>
		</tr>
		<tr>
			<td colspan="2" style="width:10mm;text-align:center;border:1px solid black;"><?= isset($transport['Carrier']['name']) ? $transport['Carrier']['name'] : ''; ?> </td>
			<td style="width:18mm;text-align:center;border:1px solid black;"><?= isset($transport['Transport']['shipping_date']) ? $transport['Transport']['shipping_date'] : '' ; ?></td>
			<td colspan="1" style="width:18mm;text-align:center;border-bottom:1px solid black;border-right:1px solid black;"><br /></td>
            <td colspan="1" style="width:18mm;text-align:center;border-bottom:1px solid black;border-right:1px solid black;"><br /></td>
		</tr>
		<?php if($settings['Setting']['ddtdepositenabled']) { ?>
			<tr>
				<td colspan="5" style="width:18mm;text-align:left;border-bottom:1px solid black;border:1px solid black;">&nbsp; DEPOSITO DI PRELIEVO: <?= $transport['Deposit']['deposit_name'] ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<?php
    if($settings['Setting']['show_conai'] != 0)
    {
?>
    <table style ="width:100%;border-collapse: collapse;font-size:10px;">
        <tbody>
        <tr><td>Contributo ambientale Conai assolto ove dovuto</td></tr>
        </tbody>
    </table>
<?php
    }
?>
<?php
    if($settings['Setting']['bill_subscript_note'] != '' && $settings['Setting']['bill_subscript_note'] != null)
    {
        echo $this->element('pdfFooters/subscript_note');
    }
?>	
<?php
    if($settings['Setting']['bill_subscript'] != '' && $settings['Setting']['bill_subscript'] != null)
    {
        echo $this->element('pdfFooters/subscript_image');
    }
?>