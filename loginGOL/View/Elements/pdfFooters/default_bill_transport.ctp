<?php 
	isset($bill['Bill']['collectionfees']) ? $speseincasso = $bill['Bill']['collectionfees'] :$speseincasso = ''; 
	isset($bill['Bill']['seal']) ? $seal = $bill['Bill']['seal'] :$seal = '';
?>

<table style ="border-collapse: collapse;font-size:8pt;margin-bottom:10px;" width="100%"  >
	<tbody>
		<tr>
			<td style="width:5mm;text-align:center;border:1px solid black;">Colli</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">Peso (kg)</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">Volume (mc)</td>
			<td style="width:25mm;text-align:center;border:1px solid black;">Trasporto a cura del</td>
			<td style="width:25mm;text-align:center;border:1px solid black;">Aspetto dei beni</td>
		</tr>
		<tr>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($bill['Bill']['num_colli']) ? $bill['Bill']['num_colli'] : '' ?> </td>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($bill['Bill']['weight']) ? $bill['Bill']['weight'] : '' ?></td>
			<td style="width:5mm;text-align:right;border:1px solid black;padding-right:2px;"><?= isset($bill['Bill']['volume']) ? $bill['Bill']['volume'] : '' ?></td>
			<td style="width:25mm;text-align:center;border:1px solid black;">
				<?php
				 switch ($bill['Bill']['transporter'])
				 {
					case 0: echo 'Mittente'; break;
					case 1: echo 'Destinatario'; break;
					case 2: echo 'Vettore'; break;
				 }
				 ?>
			</td>
			<td style="width:25mm;text-align:center;border:1px solid black;"><?= $bill['Bill']['aspect']; ?></td>
		
		</tr>
	
		<tr>
			<td colspan="2" style="width:10mm;text-align:center;border:1px solid black;">VETTORE</td>
			<td style="width:5mm;text-align:center;border:1px solid black;">DATA PARTENZA</td>
			<td colspan="2" style="width:18mm;text-align:center;border-right:1px solid black;">FIRMA</td>
		</tr>
		<tr>
			<td colspan="2" style="width:10mm;text-align:center;border:1px solid black;"><?= isset($bill['Carrier']['name']) ? $bill['Carrier']['name'] : ''; ?> </td>
			
			<td style="width:18mm;text-align:center;border:1px solid black;"><?= isset($bill['Bill']['billaccdate']) ? $this->Time->format('d/m/Y',($bill['Bill']['billaccdate'])) : '' ; ?></td>
		
			<td colspan="2" style="width:18mm;text-align:center;border-bottom:1px solid black;border-right:1px solid black;"><br /></td>
		</tr>
	</tbody>
</table>


	<table style ="border-collapse: collapse;font-family: Courier New, Courier, monospace;margin-top:-70px;font-size: 9pt;">
	<tbody>
	<tr>
		<td style="width:11mm;text-align:center;border:1px solid black;">C.IVA</td>
		<td style="width:20mm;text-align:center;border:1px solid black;">IMPONIBILE</td>
		<td style="width:14mm;text-align:center;border:1px solid black;">Al.IVA</td>
		<td style="width:55mm;text-align:center;border:1px solid black;">IMPOSTA</td>
		<td style="width:3mm;"></td>
		<td style="width:18mm;text-align:center;border:1px solid black;">IMPORTO</td>
		<td style="width:18mm;text-align:center;border:1px solid black;">SCADENZA</td>
		<td style="width:3mm;"></td>
		<td colspan="2" style="width:38mm;text-align:center;border:1px solid black;background-color:#dedede;">TOTALE DOCUMENTO</td>
	</tr>
		<?php
			isset($bill['Bill']['collectionfees']) ? $speseincasso = $bill['Bill']['collectionfees'] * $bill['Bill']['changevalue'] : $speseincasso = '';
			isset($bill['Bill']['seal']) ? $seal = $bill['Bill']['seal'] * $bill['Bill']['changevalue']  : $seal = '';

			$totaleImponibile = 0;
			foreach($arrayImponibili as $imp){$totaleImponibile += $imp;}
			
			$totaleIvato = 0;
			foreach($arrayIva as $iva){$totaleIvato += number_format($iva,2,'.','');}

			// $scritteArray2 = ['Totale imponibile','Totale IVA','Totale fattura','Spese incasso','Bolli'];
			$scritteArray2 = 
			[
				$settings['Setting']['bill_footer_total_taxableincome_desc'],
				$settings['Setting']['bill_footer_total_vat_desc'],
				$settings['Setting']['bill_footer_total_desc'],
				$settings['Setting']['bill_footer_seal_desc'],
			];
			
		$i =-1;
		
		
		// Totale deadlines messo per cambio valuta
		$totalDeadlines = 0;
		$totaleDeadlineWithoutChange = 0;

		foreach($deadlines as $deadline)
		{
			$i++;
			$arrayDeadline[$i]['data'] = date('d/m/Y',strtotime($deadline['Deadlines']['deadline'])); 
			$arrayDeadline[$i]['importo'] = number_format($deadline['Deadlines']['amount'] * $bill['Bill']['changevalue'],2);
			
			// Totale deadlines messo per cambio valuta
			$totalDeadlines += number_format($deadline['Deadlines']['amount'] * $bill['Bill']['changevalue'],2);
		}

		$lastDeadlineCounter = $i;
		
		$totaleImponibile = number_format($totaleImponibile,2,'.','');
		$totaleRitenuta = number_format($totaleRitenuta,2,'.','');
		$speseincasso = number_format($speseincasso,2,'.','');
		$seal = number_format($seal,2,'.','');
		$totaleIvato = number_format($totaleIvato,2,'.','');
        $rounding = number_format($rounding,2,'.','');

		if($bill['Bill']['withholding_tax'] > 0) 
		{	 
			$ritenuta = $bill['Bill']['withholding_tax'] ;
			$imponibileRitentuta =	$totaleImponibile - $imponibileStornoRitenutaAcconto;// Modifiche ritenuta acconto su articoli;
			$totaleRitenuta =  $imponibileRitentuta * $bill['Bill']['withholding_tax'] / 100; 
			
			// Se è settato che lo tolgo il 	
			if($bill['Bill']['welfare_box_withholding_appliance'] == 0)
			{
				$totaleRitenuta = $totaleRitenuta - ($welfareBox_imponibile_iva * $bill['Bill']['withholding_tax'] / 100);
				$imponibileRitentuta = $imponibileRitentuta -  $welfareBox_imponibile_iva;
			}
		}
		else
		{
			$ritenuta = ''; 
			$imponibileRitentuta = '';
			$totaleRitenuta = '';
		}
			
		if($sp)
		{
			// $importototaleFattura = number_format($totaleImponibile  - $totaleRitenuta + $speseincasso+$seal, 2,'.','');
			// SEAL OLD VS SEAL NEW
			if(strtotime(date("Y-m-d",strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01')))
			{
				$importototaleFattura = number_format($totaleImponibile  - $totaleRitenuta +$seal, 2,'.','');
			}
			else
			{
				$importototaleFattura = number_format($totaleImponibile  - $totaleRitenuta + $rounding, 2,'.','');
			}
		}
		else
		{
			// $importototaleFattura = number_format($totaleImponibile + $totaleIvato  - $totaleRitenuta + $speseincasso+$seal, 2,'.','');
			// SEAL OLD VS SEAL NEW
			if(strtotime(date("Y-m-d",strtotime($this->request->data['Bill']['date']))) < strtotime(date('2019-01-01')))
			{
				$importototaleFattura = number_format($totaleImponibile + $totaleIvato  - $totaleRitenuta + $seal, 2,'.','');
			}
			else
			{
				$importototaleFattura = number_format($totaleImponibile + $totaleIvato  - $totaleRitenuta +$rounding , 2,'.','');
			}
		}

		$difference = number_format($importototaleFattura - $totalDeadlines,2,'.','');

		$arrayDeadline[$lastDeadlineCounter]['importo'] = $arrayDeadline[$lastDeadlineCounter]['importo'] + $difference;

		for($i = 0;  $i < 5 ; $i++) // Minimo 3 righe e massimo 3 aliquote iva previste
		{
				
		?>
		<tr>
			
			<?php if(array_values($arrayImponibili)[$i] > 0) { ?>
			<td style="text-align:center;border:1px solid black;"><?= isset(array_values($arrayCodici)[$i]) ? array_values($arrayCodici)[$i] : ''; ?></td>
			<td style="text-align:right;border:1px solid black;padding-right:1mm"><?= isset(array_values($arrayImponibili)[$i]) ? number_format(array_values($arrayImponibili)[$i],2,',','.') : '' ?></td>
			
			<?php
				if(isset(array_values($arrayPercentuali)[$i]))
				{
					if(array_values($arrayPercentuali)[$i] > 0)
					{
						?>
							<!--td style="text-align:center;border:1px solid black;"><?php // isset(array_keys($arrayImponibili)[$i]) ? array_keys($arrayImponibili)[$i] : '' ?></td-->
							<td style="text-align:center;border:1px solid black;"><?= isset(array_values($arrayPercentuali)[$i]) ? array_values($arrayPercentuali)[$i] : '' ?></td>
							<td style="text-align:center;border:1px solid black;text-align:right;"><?= isset(array_values($arrayIva)[$i]) ? number_format(array_values($arrayIva)[$i],2,',','.') : '' ?></td>
				<?php 
					}
					else
					{
						?>
							<td colspan = "2" style="text-align:left;border:1px solid black;padding-left:2mm;"><?=  array_values($arrayDescrizione)[$i]; ?></td>
						<?php
					}
				
				}
				else
				{?>
					<!--td style="text-align:center;border:1px solid black;"><?php // isset(array_keys($arrayImponibili)[$i]) ? array_keys($arrayImponibili)[$i] : '' ?></td-->
					<td style="text-align:center;border:1px solid black;"><?= isset(array_values($arrayPercentuali)[$i]) ? array_values($arrayPercentuali)[$i] : '' ?></td>
					<td style="text-align:center;border:1px solid black;text-align:right;"><?= isset(array_values($arrayIva)[$i]) ? number_format(array_values($arrayIva)[$i],2,',','.') : '' ?></td>
				<?php }?>
			<?php }
			else{ ?>
					<!-- imponibile nullo -->
					<td style="text-align:center;border:1px solid black;"></td>
					<td style="text-align:right;border:1px solid black;padding-right:1mm"></td>
					<td style="text-align:center;border:1px solid black;"></td>
					<td style="text-align:center;border:1px solid black;text-align:right;"></td>	
				<?php 
			} 
			?>
			<td style=""></td>
			<td style="text-align:right;border:1px solid black;padding-right:1mm"><?= isset($arrayDeadline[$i]['importo']) && ($arrayDeadline[$i]['importo'] > 0) ? number_format($arrayDeadline[$i]['importo'],2,',','.') : ''; ?></td>
			<td style="text-align:right;border:1px solid black;padding-right:1mm"><?= isset($arrayDeadline[$i]['data']) && ($arrayDeadline[$i]['importo'] > 0) ? $arrayDeadline[$i]['data'] : ''; ?></td>
			<td style="width:3mm;"></td>
			<td style="width:30mm;text-align:left;border:1px solid black;">
			<?php
            if($rounding == '0.01' || $rounding == '-0.01')
            {
                switch ($i)
                {
                    case 0:
                        echo $scritteArray2[$i] ;
                        break;
                    case 1:
                        echo $scritteArray2[$i] ;
                        break;
                    case 2:
                        echo 'Arrotondamento';
                        break;
                    case 3:
                        echo $scritteArray2[$i-1] ;
                        break;
                    case 4:
                        if($seal != ''){ echo $scritteArray2[$i];  }else {echo '<br/>';}
                        break;
                }
            }
            else{
				switch ($i)
				{
					case 3:
							if($seal != ''){ echo $scritteArray2[$i];  }else {echo '<br/>';}
						break;
					case 4:
					break;
					default	:				
						echo $scritteArray2[$i] ;
					break;
				}
            }
			?></td>
			<td style="width:20mm;text-align:right;border:1px solid black;">
				<?php
                if($rounding == '0.01' || $rounding == '-0.01')
                {
                    switch ($i) {
                        case 0:
                            echo number_format($totaleImponibile, 2, ',', '.');
                            break;
                        case 1:
                            echo number_format($totaleIvato, 2, ',', '.');
                            break;
                        case 2:
                            echo number_format($rounding, 2, ',', '.');
                            break;
                        case 3:
                            echo number_format($totaleImponibile + $totaleIvato + $rounding, 2, ',', '.');
                            break;
                        case 4:
                            if ($seal != '') {
                                echo number_format($seal, 2, ',', '.');
                            } else {
                                echo '<br/>';
                            }
                            break;
                    }
                }else{
				switch ($i)
				{
					case 0:
						 echo number_format($totaleImponibile,2,',','.') ;
						break;
					case 1:
						echo number_format($totaleIvato,2,',','.') ;
						break;
					case 2:
						echo number_format($totaleImponibile+$totaleIvato,2,',','.') ;
						break;
					case 3:
						if($seal != ''){ echo number_format($seal,2,',','.') ; }else{ echo '<br/>';}
						break;
					case 4:
						echo '<br/>';
					break;
				}}
				?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
		<td colspan="10" style="height:10px;"></td>	
		</tr>

			<?php 
			/* if($bill['Bill']['withholding_tax'] > 0) 
			{ 
				$ritenuta = $bill['Bill']['withholding_tax'] ;
				$imponibileRitentuta =	$totaleImponibile ;
				$totaleRitenuta =  $imponibileRitentuta * $bill['Bill']['withholding_tax'] / 100;
			}
			else
			{
				$ritenuta = ''; 
				$imponibileRitentuta = '';
				$totaleRitenuta = '';
			} */
			?>
			
			<tr>
				<td colspan="3"  style="width:37mm;text-align:left;border-left:1px solid black;border-right:1px solid black;border-top:1px solid black;">Rit. acconto <?= $bill['Bill']['withholding_tax'] > 0 ? $bill['Bill']['withholding_tax'] : ''; ?> %  </td>
				<td colspan="1" rowspan="2"  style="text-align:left;border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;text-align:right;vertical-align:middle;border-top:1px solid black;"><?= number_format($totaleRitenuta,2,',','.'); ?></td>
				<td></td>
				<?php
				// Disabilitato momentaneamente		perché non richiesto
				// (MULTICURRENCY && strtoupper($currencyIsoCode == 'EUR' ))  ? $changeNotice = ' ( 1€ = '.$bill['Bill']['changevalue'].' '.$currencySymbol.' )' : $changeNotice = ''; 
				$changeNotice = '';
				?>
				<td  rowspan="2" colspan="4" style="font-weight:bold;width:87mm;text-align:left;border:1px solid black;vertical-align:middle;background-color:#cdcdcd;">&nbsp; <?= $settings['Setting']['bill_footer_topay_desc'] ?> <?= $currencySymbol . $changeNotice; ?></td>
				<?php
					if(isset($sp) && $sp == true)
					{
						?>
						<?php if(strtotime(date("Y-m-d",strtotime($bill['Bill']['date']))) < strtotime(date('2019-01-01'))) { ?>
						<td  rowspan="2"  style="font-weight:bold;width:35mm;text-align:right;border:1px solid black;vertical-align:middle;background-color:#cdcdcd;"><?=  number_format($totaleImponibile- $totaleRitenuta+$seal +$rounding,2,',','.') ?></td>
						<?php } else { ?>
						<td  rowspan="2"  style="font-weight:bold;width:35mm;text-align:right;border:1px solid black;vertical-align:middle;background-color:#cdcdcd;"><?= number_format($totaleImponibile- $totaleRitenuta +$rounding,2,',','.') ?></td>
						<?php } ?>
						<?php
					}
					else
					{
					?>
						<?php if(strtotime(date("Y-m-d",strtotime($bill['Bill']['date']))) < strtotime(date('2019-01-01'))) { ?>
						<td  rowspan="2"  style="font-weight:bold;width:35mm;text-align:right;border:1px solid black;vertical-align:middle;background-color:#cdcdcd;"><?= number_format($totaleImponibile +$totaleIvato  - $totaleRitenuta  + $seal+$rounding, 2,',','.') ?></td>
						<?php } else { ?>
						<td  rowspan="2"  style="font-weight:bold;width:35mm;text-align:right;border:1px solid black;vertical-align:middle;background-color:#cdcdcd;"><?= number_format($totaleImponibile +$totaleIvato  - $totaleRitenuta+$rounding , 2,',','.') ?></td>
						<?php } ?>
					<?php
					}?>	
			</tr>
			<tr>
				<td colspan="3"  style="width:37mm;text-align:left;border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Su <?= $currencySymbol; ?> <?= number_format($imponibileRitentuta,2,',','.'); ?>  </td>
			
				<td></td>
			</tr>
</tbody>
</table>
	<?php 
		if($settings['Setting']['bill_subscript_note'] != '' && $settings['Setting']['bill_subscript_note'] != null)
		{
			echo $this->element('pdfFooters/subscript_note');
		}
?>	
	<?php 
		if($settings['Setting']['bill_subscript'] != '' && $settings['Setting']['bill_subscript'] != null)
		{
			echo $this->element('pdfFooters/subscript_image');
		}
	?>	
