<!--
	**  Menu visualizzazione per cammion **
-->

<nav class="uk-navbar dark">
	<ul class="uk-navbar-nav">
		<li class="logo">
			<?php echo $this->Html->image('gestionale-online.png');?>
		</li>
    </ul>
    <div class="uk-navbar-flip">
	    <ul class="uk-navbar-nav">
			<li class="menu-voice">
				<?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?>
			</li>
	    </ul>
	</div>
</nav>
