	<p class="uk-text-center">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Pagina {:page} di {:pages}')
	));
	?>	</p>

	<div class="paging uk-text-center">
	<?php
		echo $this->Paginator->prev('<i class="fa fa-arrow-circle-o-left"></i> &nbsp;', array('escape'=>false,'class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' - '));
		echo $this->Paginator->next('&nbsp; <i class="fa fa-arrow-circle-o-right"></i>', array('escape'=>false,'class' => 'next disabled'));
	?>
	</div>