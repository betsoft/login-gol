<?php
$tableHeaderStyle = "border:1px solid black;font-size:10px;background-color:#dadada;";
$tableHeaderStyleLeft = "border:1px solid black;font-size:10px;background-color:#dadada;text-align:left;padding-left:4px;";
$tableBodyStyle = "border:1px solid black;font-size:10px;";
$tableBodyStyleRight = "border:1px solid black;font-size:10px;text-align:right;";
$tableBodyStyleNoBorder =  "font-size:10px;";
$tableBodyStyleRightNoBorder = "font-size:10px;text-align:right;;";
?>

<table style="border:0px;margin-top:130px;margin-left:-3px;"><tbody><tr><td></td></tr></tbody></table>

<?php if(isset($order['Order']['note']) && $order['Order']['note'] != '') { ?>

    <table style="border-spacing: 0;">
        <tbody>
        <tr><td style="font-size:12px;"><?= $order['Order']['note'] ?></td></tr>
        </tbody>
    </table>

    <table style="min-height:30mm;"><tbody><tr><td></td></tr></tbody></table>
    <table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
    <table style="min-height:10mm;"><tbody><tr><td></td></tr></tbody></table>
<?php } ?>
<table style="border-spacing: 0;">
    <thead>
    <tr>
        <th style="width:80mm;<?= $tableHeaderStyle; ?>">Descrizione articolo</th>
        <th style="width:30mm;<?= $tableHeaderStyle; ?>">Quantità</th>
        <th style="width:25mm;<?= $tableHeaderStyle; ?>">U.M.</th>
        <th style="width:25mm;<?= $tableHeaderStyle; ?>">Prezzo unitario</th>
        <th style="width:25mm;<?= $tableHeaderStyle; ?>">Totale</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $totale = 0;
    foreach($order['Orderrow'] as $numero => $oggetto)
    {
        if(!isset($oggetto['storage_id']))
        {
            ?>
            <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
            <tr>
                <td style="<?= $tableBodyStyleNoBorder; ?>">
                    <?php
                    echo $oggetto['oggetto'];
                    $totaleRigaSingola = ($oggetto['prezzo'] + (($oggetto['prezzo'] * $oggetto['Iva']['percentuale'])/100))*$oggetto['quantita'];
                    ?>
                </td>
                <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['quantita']) ? number_format($oggetto['quantita'], 2, ',', '.') : null ?></td>
                <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['Units']['description']) ? $oggetto['Units']['description'] : null ?></td>
                <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['prezzo']) ? number_format($oggetto['prezzo'], 2, ',', '.') . ' €' : null ?></td>
                <td style="<?= $tableBodyStyleRightNoBorder; ?>"> <?= isset($oggetto['importo']) && isset($oggetto['Iva']['percentuale']) ? number_format($totaleRigaSingola, 2, ',', '.') . ' €' : null ?></td>
            </tr>
            <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
            <?php
        }
        else
        {
            ?>
            <tr>
                <td style="<?= $tableBodyStyle; ?>">
                    <?php
                    echo $oggetto['oggetto'];
                    ?>
                </td>
                <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['quantita']) ? number_format($oggetto['quantita'], 2, ',', '.') : null ?></td>
                <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['Units']['description']) ? $oggetto['Units']['description'] : null ?></td>
                <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['prezzo']) ? number_format($oggetto['prezzo'], 2, ',', '.') . ' €' : null ?></td>
                <td style="<?= $tableBodyStyleRight; ?>"> <?= isset($oggetto['importo']) && isset($oggetto['Iva']['percentuale']) ? number_format($oggetto['importo'] + (($oggetto['importo'] * $oggetto['IVA']['percentuale'])/100), 2, ',', '.') . ' €' : null ?></td>
            </tr>
            <?php
        }


    $totale += $oggetto['importo'] + (($oggetto['importo'] * $oggetto['quantity'])/100);
    $subtotale += $oggetto['prezzo'] * $oggetto['quantita'];
    }
    ?>

    <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
    <tr><td colspan="5" style="<?= $tableBodyStyleNoBorder; ?>">&nbsp;</td></tr>
    <tr>
        <td style="font-size:12px;"><b>TOTALE</b></td><td></td><td></td><td></td><td style="text-align:right;font-size:12px;"><b><?= number_format($totale,2,',','.'). ' €' ; ?></b></td>
    </tr>
    </tbody>
</table>

<br/>


<table style="width:100%;border-spacing: 0;">
    <thead>
    <tr><th style="width:100%;<?= $tableHeaderStyleLeft; ?>">PER CONFERMA</th></tr>
    </thead>
    <tbody>
    <tr><td>&nbsp;</td></tr>
    <tr><td style="font-size:10px;">Data: </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td style="font-size:10px;">Timbro/Firma: ________________________ </td></tr>
    <tr><td>&nbsp;</td></tr>
    </tbody>
</table>
<table>
    <tbody>
    <tr>
        <td width="20%" style="font-size:10px;text-align:left;">Grazie e cordiali saluti,</td>
        <td width="80%"></td>
    </tr>
    <tr>
        <td width="20%" style="font-size:10px;"><?= $settings['Setting']['name'] ?></td>
        <td width = "80%"></td>
    </tr>
    </tbody>
</table>
<htmlpagefooter name="myfooter"><?= $this->element($pdfFooter); ?></htmlpagefooter>
