
<?php

$numeroRigheCentrali = 169; //18; //19
$numeroRigheFinale = 169; // 21; //22 // Righe vuote per chiudere pagina
$lunghezzaRiga = 74; // 68

$coefficienteRiga = 3;
$coefficienteRigaFinale = 3;

$altezzaBody = '150mm';
$whiteField = '<span style="color:#ffffff;">_</span>';
?>


<table  cellpadding="1" width="100%"  style="margin-top:245px;border:0.2mm solid black;font-family: Courier New, Courier, monospace;font-size: 9pt; border-collapse: collapse;max-height:<?=	$altezzaBody ?>;" >
    <?= $this->element('pdfBodies/tableheader_default_transport'); ?>

    <tbody >
    <?php
    $totaleAltezzaRighe = 0;

    if($settings['Setting']['bill_subscript'] != null && $settings['Setting']['bill_subscript'] != '' )
    {
        //	$totaleAltezzaRighe+=31;
        $totaleAltezzaRighe+=37;
    }

    if($settings['Setting']['bill_subscript_note'] != null && $settings['Setting']['bill_subscript_note'] != '' )
    {
        $lunghezzaRigaNotaPedice = 164;
        if(strlen($settings['Setting']['bill_subscript_note']) > $lunghezzaRigaNotaPedice)
        {
            $numeroRighe =  floor(strlen($bill['Bill']['bill_subscript_note']) / $lunghezzaRigaNotaPedice) + 1;
            $coefficienteFissoNotaPedice = 3;
            $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFissoNotaPedice ;
            $totaleAltezzaRighe +=  ($altezzaRiga) ;
        }
    }


    foreach($transport['Transportgood'] as $numero => $oggetto)
    {

        $oggetto['oggetto'] = $oggetto['oggetto'] . ' '. $oggetto['customdescription'];

        /* Fine controllo listino modificato */
        $prezzo_riga = $oggetto['quantita'] * $oggetto['prezzo'];
        $classe='';
        $numero % 2 != 0 ? $classe = 'style="background-color: #eee;"' : null;

        if(strlen($oggetto['oggetto']) > $lunghezzaRiga)
        {
            $numeroRighe =  floor(strlen($oggetto['oggetto']) / $lunghezzaRiga) + 1.1;
            $coefficienteFisso = 2;
        }
        else
        {
            $numeroRighe = 1;
            $coefficienteFisso = 2;
        }

        $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFisso ;
        $totaleAltezzaRighe +=  ($altezzaRiga) ;

        // SE supero il totale cambio pagina
        if($totaleAltezzaRighe > $numeroRigheCentrali)
        {
            $totaleAltezzaRighe = $totaleAltezzaRighe - $numeroRigheCentrali;
            if($totaleAltezzaRighe < 5){ $totaleAltezzaRighe = 5;}
            echo $this->element('pdfBodies/newpage_default_transport');
        }
        ?>

        <tr <?= $classe; ?>>
            <td class="normalFontFirst" style="padding:3px;"><?= $oggetto['codice']; ?></td>
            <td class="normalFont" style="padding:3px;">
                <?php
                $descrizioneLisino = $oggetto['oggetto'];
                echo $oggetto['oggetto'] ;
                ?>
            </td>
            <td class="normalFont" style="padding:3px;"> <?= $oggetto['Units']['description'];  ?></td>
            <td class="normalFont" style="padding:3px;" align="right"><?= $oggetto['quantita']; ?></td>
        </tr>
        <?php
        $importo_iva += ($prezzo_riga/100) * $oggetto['Iva']['percentuale'];
        $imponibile_iva += $prezzo_riga;
    }

    $totale = $importo_iva + $imponibile_iva;

    if($transport['Transport']['note'] != '')
    {
        $numeroRighenota =  floor(strlen($transport['Transport']['note'] / $lunghezzaRiga) + 1);
        $altezzaRiga = $numeroRighenota * $coefficienteRiga;
        $totaleAltezzaRighe +=  ($altezzaRiga) ;
        ?>
        <tr <?= $classe; ?>>
            <td class="normalFontFirst" style="padding:3px;"></td>
            <td class="normalFont" style="padding:3px;"><?= $transport['Transport']['note'] ?></td>
            <td class="normalFont" style="padding:3px;"></td>
            <td class="normalFont" style="padding:3px;" align="right"></td>
        </tr>
        <?php

    }

    for($i = $totaleAltezzaRighe +  $coefficienteRiga; $i< $numeroRigheFinale ;$i=$i +1+  $coefficienteRiga)
    {

        ?>
        <tr <?= $classe; ?> style="background-color:white;">
            <td class="normalFontFirst"><br /></td>
            <td class="normalFont"></td>
            <td class="normalFont"></td>
            <td class="normalFont"></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>

</div>
<htmlpagefooter name="myfooter"><?= $this->element($pdfFooter); ?></htmlpagefooter>
</body>
</html>
