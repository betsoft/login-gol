<?php

// Se è una fattura o una fattura pro-forma ed è accompagnatoria
switch($settings['Setting']['pdf_header_version'])
{
    case 0:
        $defaultBodyFromHeaderPixel = '295px';
        $aggiuntaBodySize = 0;
        break;
    case 1:
        $_SESSION['Auth']['User']['dbname'] != 'login_GE0043' && $_SESSION['Auth']['User']['dbname'] != 'login_GE0033' && $_SESSION['Auth']['User']['dbname'] != 'login_GE0047' ? $defaultBodyFromHeaderPixel = '68mm' : $defaultBodyFromHeaderPixel = '84mm';
        if($_SESSION['Auth']['User']['dbname'] == 'login_GE0047' && $bill['Bill']['alternative_bank_id'] != 0 && $bill['Payment']['alternative_bank_id'] != 0){
            $defaultBodyFromHeaderPixel = "85mm";
        }
        $aggiuntaBodySize = 12;
        break;
}

if($bill['Bill']['accompagnatoria'] == 1 && ($bill['Bill']['tipologia'] == 1 || $bill['Bill']['tipologia'] == 8 ))  //non  Accompagnatoria
{
    $numeroRigheCentrali = 100 + $aggiuntaBodySize;
    $numeroRigheFinale = 114 + $aggiuntaBodySize;
    $lunghezzaRiga = 45;
}
else
{
    $numeroRigheCentrali = 120 + $aggiuntaBodySize;
    $numeroRigheFinale = 100 + $aggiuntaBodySize;
    $lunghezzaRiga = 45;
}

$coefficienteRiga = 3.1;
$coefficienteRigaFinale = 3;

$removeColumnQuantity = $settings['Setting']['bill_print_remove_quantity'] ;
$removeColumnDiscount = $settings['Setting']['bill_print_remove_discount'] ;

$imponibileStornoRitenutaAcconto = 0;

// Aggiungo arrotondamenti
$rounding = $bill['Bill']['rounding'];
?>
<table class="items"   style="font-size: 9pt;font-family: Courier New, Courier, monospace;margin-top:<?= $defaultBodyFromHeaderPixel ?>;border:0.1px solid black; border-collapse: collapse;" cellpadding="6">
    <?= $this->element('pdfBodies/tableheader_default_bill'); ?>
    <tbody>
    <?php
    $totaleAltezzaRighe = 0;

    if($settings['Setting']['bill_subscript'] != null && $settings['Setting']['bill_subscript'] != '' )
        $totaleAltezzaRighe += 37;

    if($settings['Setting']['bill_subscript_note'] != null && $settings['Setting']['bill_subscript_note'] != '' )
    {
        $lunghezzaRigaNotaPedice = 164;

        if(strlen($settings['Setting']['bill_subscript_note']) > $lunghezzaRigaNotaPedice)
        {
            $numeroRighe =  floor(strlen($bill['Bill']['bill_subscript_note']) / $lunghezzaRigaNotaPedice) + 1;
            $coefficienteFissoNotaPedice = 3;
            $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFissoNotaPedice ;
            $totaleAltezzaRighe +=  ($altezzaRiga);
        }
    }

    $totaleRigaPaginaNuova = $totaleAltezzaRighe;

    // Aggiunta la causale
    if(isset($bill['Bill']['document_causal']) && $bill['Bill']['document_causal'] != '')
    {
        if(strlen($bill['Bill']['document_causal']) > $lunghezzaRiga)
        {
            $numeroRighe =  floor(strlen($bill['Bill']['document_causal']) / $lunghezzaRiga) + 1;
            $coefficienteFisso = 5;
        }
        else
        {
            $numeroRighe = 1;
            $coefficienteFisso = 2;
        }

        $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFisso ;
        $totaleAltezzaRighe +=  ($altezzaRiga) ;

        ?>
        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <?php if($_SESSION['Auth']['User']['dbname'] == 'login_GE0046'){ ?>
                <td></td>
            <?php } else { ?>
                <td class="normalFontFirst"><?= $bill['Bill']['document_causal']; ?></td>
            <?php } ?>
            <td class="normalFont"></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td class="normalFont"></td>
        </tr>
        <?php
    }

    // Inserisco un eventuale nota
    if(isset($bill['Bill']['note']) && $bill['Bill']['note'] != '')
    {
        if(strlen($bill['Bill']['note']) > $lunghezzaRiga)
        {
            $numeroRighe =  floor(strlen($bill['Bill']['note']) / $lunghezzaRiga) + 1;
            $coefficienteFisso = 5;
        }
        else
        {
            $numeroRighe = 1;
            $coefficienteFisso = 2;
        }

        $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFisso ;
        $totaleAltezzaRighe +=  ($altezzaRiga);
        ?>
        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <?php if (($_SESSION['Auth']['User']['dbname'] == 'login_GE0050') || ($_SESSION['Auth']['User']['dbname'] == 'login_GE0046')) { ?>
                <td class="normalFontFirst"></td>
            <?php } else { ?>
                <td class="normalFontFirst"><?= $bill['Bill']['note']; ?></td>
            <?php } ?>
            <td class="normalFont"></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td class="normalFont"></td>
        </tr>
        <?php
    }

    // Aggiungo il rif ddt generale al documento 2019/22/02
    if((isset($bill['Bill']['mainrifddtnumber']) && $bill['Bill']['mainrifddtnumber'] != null) && (isset($bill['Bill']['mainrifddtdate']) && $bill['Bill']['mainrifddtdate']))
    {
        $numeroRighe = 1;
        $coefficienteFisso = 2;
        $altezzaRiga = $coefficienteRiga + $coefficienteFisso;
        $totaleAltezzaRighe +=  ($altezzaRiga); ?>
        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <td class="normalFontFirst"><?= 'Rif DDT n°' . $bill['Bill']['mainrifddtnumber'] . ' del ' . date('d-m-Y',strtotime($bill['Bill']['mainrifddtdate'])); ?></td>
            <td class="normalFont"></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td class="normalFont"></td>
        </tr>
        <?php
    }

    // Se è settata la bill
    if(isset($billTransports) && $billTransports != null && $bill['Bill']['not_detailed'] == 1)
    {
        // Per ogni bolla - se non è stato selezionato "Dettagliato"
        foreach ($billTransports as $transportReferred)
        {
            $numeroRighe = 1;
            $coefficienteFisso = 2;
            $altezzaRiga = $coefficienteRiga + $coefficienteFisso;
            $totaleAltezzaRighe +=  ($altezzaRiga);
            ?>
            <tr <?= $classe; ?> style="background-color:#ffffff;">
                <td class="normalFontFirst"><?= 'Rif DDT n°' . $transportReferred['Transports']['transport_number'] . ' del ' . date('d-m-Y',strtotime($transportReferred['Transports']['date'])); ?></td>
                <td class="normalFont"></td>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
                <td class="normalFont"></td>
            </tr>
            <?php
        }
    }
    ?>

    <?php
    // Aggiungo riferimento fattura per nota di credito
    $altezzaRiga = 1 * $coefficienteRiga + 5;
    $totaleAltezzaRighe += ($altezzaRiga);

    if(isset($bill['Bill']['bill_referred_number']) && isset($bill['Bill']['bill_referred_date']) && $bill['Bill']['bill_referred_number'] != null && $bill['Bill']['bill_referred_date'] != null)
    {
        ?>
        <tr <?php echo $classe; ?>>
            <td> Rif. fatt. n. <?=  $bill['Bill']['bill_referred_number'] ?> del <?= date('d/m/Y',strtotime($bill['Bill']['bill_referred_date'])); ?> </td><td ></td><td ></td><td></td><td ></td><td ></td><td ></td>
        </tr>
        <?php
    }
    ?>

    <?php
    $transportString = '';
    foreach($bill['Good'] as $numero => $oggetto)
    {
        // La descrizione dell'oggetto + descrizione più customdescription
        $oggetto['oggetto'] = $oggetto['oggetto'] . ' ' . $oggetto['customdescription'];
        $customdescription = '';

        if(strlen($oggetto['oggetto']) > 60)
        {

            for($i = 0; $i < strlen($oggetto['oggetto']); $i += 60)
            {
                $customdescription .= '</br>'.substr($oggetto['oggetto'], $i, 60);
            }
        }
        else
        {
            $customdescription = $oggetto['oggetto'];
        }

        $oggetto['oggetto'] = $customdescription;

        // Applico il cambio
        if(MULTICURRENCY && isset($oggetto['currencyprice']) && $oggetto['currencyprice'] != null)
        {
            $oggetto['prezzo'] = $oggetto['currencyprice'];
        }
        else
        {
            $oggetto['prezzo'] = $oggetto['prezzo']; // Inutile
        }

        // Calcolo il prezzo riga
        $prezzo_riga = $oggetto['quantita'] * $oggetto['prezzo'] * (1 - $oggetto['discount']/100);

        // Aggiungo la cassa riga per riga ??

        // Aggiunta ritenuta acconto

        $classe='';
        $numero % 2 != 0 ?  $classe = 'style="background-color: #eee;"' : null;

        if(strlen($oggetto['oggetto']) > $lunghezzaRiga)
        {
            $numeroRighe =  floor(strlen($oggetto['oggetto']) / $lunghezzaRiga) + 1;
            $coefficienteFisso = 4;
        }
        else
        {
            $numeroRighe = 1;
            $coefficienteFisso = 3;
        }

        // Controllo unità di misura delle righe descrizioni
        if($numeroRighe == 1)
        {
            $numeroRighe +=	floor(strlen($oggetto['Units']['description']) / 4);
        }

        // Se la fattura è dettagliata - Alzo di una riga
        if($bill['Bill']['not_detailed'] == 0 && $oggetto['transport_id'] > 0 )
        {
            $numeroRighe++;
        }

        $altezzaRiga = $numeroRighe * $coefficienteRiga + $coefficienteFisso ;
        $totaleAltezzaRighe +=  ($altezzaRiga) ;

        // SE supero il totale cambio pagina
        $A =$totaleAltezzaRighe;
        $B = $numeroRigheCentrali;
        if($totaleAltezzaRighe >= $numeroRigheCentrali)
        {
            $totaleAltezzaRighe = $totaleAltezzaRighe - $numeroRigheCentrali;
            $totaleAltezzaRighe = $totaleRigaPaginaNuova + $altezzaRiga;
            //$totaleAltezzaRighe = $altezzaRiga;

            echo $this->element('pdfBodies/newpage_default_bill');
        }
        ?>
        <?php
        if($bill['Bill']['not_detailed'] == 0 && $oggetto['transport_id'] > 0 )
        {
            $dataTransport = $oggetto['Transports']['transport_number'] . ' del ' . date('d-m-Y',strtotime($oggetto['Transports']['date']));
            if($transportString != $dataTransport)
            {
                ?>

                <tr <?= $classe; ?>>
                    <td><?= 'Rif DDT n° ' . $dataTransport;  ?></td>
                    <?php if($removeColumnQuantity == 0): ?>
                        <td></td>
                        <td></td>
                        <td></td>
                    <?php endif; ?>
                    <td></td>
                    <?php if($removeColumnDiscount == 0):  ?>
                        <td></td>
                    <?php endif; ?>
                    <td></td>
                </tr>
                <?php
            }

            $transportString = $dataTransport;
        }
        ?>
        <tr <?= $classe; ?>>
            <td><?= $oggetto['oggetto'];  ?></td>
            <?php if($removeColumnQuantity == 0): ?>
                <td align="right"><?php echo $oggetto['Units']['description']; ?><?php //$totaleAltezzaRighe; ?></td>
                <td align="right"><?php if($oggetto['quantita'] != 0 ) { echo   number_format($oggetto['quantita'],2,',','.'); } else{  echo '';} ?></td>
                <td align="right"><?= $oggetto['quantita'] != 0 ? $_SESSION['Auth']['User']['dbname'] == "login_GE0047" ? number_format($oggetto['prezzo'],4,',','.') : number_format($oggetto['prezzo'],2,',','.') : '' ?></td>
            <?php endif; ?>
            <td align="right"><?= $oggetto['quantita'] != 0 ? number_format($oggetto['quantita'] * $oggetto['prezzo'],2,',','.') : null; ?></td>
            <?php if($removeColumnDiscount == 0):  ?>
                <td align="right"><?=  $oggetto['discount'] > 0 ? number_format($oggetto['discount'],2,',','.'). '%' : '' ; ?></td>
            <?php endif; ?>
            <td align="right">
                <?php
                if($oggetto['quantita'] != 0)
                    echo number_format($oggetto['Iva']['percentuale'],2,',','.'). '%';
                ?>
            </td>
        </tr>
        <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") {
        if($oggetto['complimentaryQuantity'] > 0){
            ?>
            <tr <?= $classe; $prezzo= $oggetto['prezzo']*$oggetto['complimentaryQuantity']?>>

                <td>Omaggio</td>
                <?php if($removeColumnQuantity == 0): ?>
                    <td align="right"><?php //$totaleAltezzaRighe; ?></td>
                    <td align="right"><?= number_format($oggetto['complimentaryQuantity'],2,',','.'); ?></td>
                    <td align="right"></td>
                <?php endif; ?>
                <td align="right">-<?=  number_format($prezzo,2,',','.'); ?></td>

                <td align="right"></td>

                <td align="right">
                    <?php
                    if($oggetto['quantita'] != 0)
                        echo number_format($oggetto['Iva']['percentuale'],2,',','.'). '%';
                    ?>
                </td>
            </tr>
        <?php }}?>

        <?php
        $importo_iva += ($prezzo_riga/100) * $oggetto['Iva']['percentuale'];
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            if($oggetto['complimentaryQuantity'] > 0){
                $imponibile_iva += $prezzo_riga - ($oggetto['complimentaryQuantity']*$oggetto['prezzo']);
            }else{
                $imponibile_iva += $prezzo_riga;
            }
        }else{
            $imponibile_iva += $prezzo_riga;
        }
        isset($imponibile_iva[$oggetto['Iva']['percentuale']]['imponibile']) ? $imponibile_iva[$oggetto['Iva']['percentuale']]['imponibile'] +=  $prezzo_riga : $imponibile_iva[$oggetto['Iva']['percentuale']]['imponibile'] =  $prezzo_riga ;

        // Raggruppo per tipologie di iva imponibili e iva
        if($oggetto['Iva'] != null) // Questo if è stato messe per la gestione delle note (che non hanno ovviamente aliquota iva)
        {
            $arrayIva[$oggetto['Iva']['descrizione']]   += ($prezzo_riga/100) * $oggetto['Iva']['percentuale'];
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                if($oggetto['complimentaryQuantity'] > 0){
                    $arrayImponibili[$oggetto['Iva']['descrizione']] += $prezzo_riga - ($oggetto['complimentaryQuantity']*$oggetto['prezzo']);
                }else{
                    $arrayImponibili[$oggetto['Iva']['descrizione']] += $prezzo_riga;
                }
            }else{
                $arrayImponibili[$oggetto['Iva']['descrizione']] += $prezzo_riga;
            }
            $arrayCodici[$oggetto['Iva']['descrizione']] = $oggetto['Iva']['codice'];
            $arrayDescrizione[$oggetto['Iva']['descrizione']] = $oggetto['Iva']['descrizione'];
            $arrayPercentuali[$oggetto['Iva']['descrizione']] = $oggetto['Iva']['percentuale'];
        }

        // Aggiungo eventuali collectionfee
        if($totaleAltezzaRighe >= $numeroRigheCentrali)
        {
            $totaleAltezzaRighe = $totaleAltezzaRighe - $numeroRigheCentrali;
            if($totaleAltezzaRighe < 5){ $totaleAltezzaRighe = 5;}
            echo $this->element('pdfBodies/newpage_default_bill');
            $totaleAltezzaRighe = 0;
        }

        // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
        if($oggetto['withholdingapplied'] == 0)
        {
            if($oggetto['quantita'] != 0)
            {
                $imponibileStornoRitenutaAcconto += number_format($oggetto['quantita'] * $oggetto['prezzo'],2,'.','');
            }
        }
    }

    //  Spese fisse (vanno messe nell'imponibile)
    if(isset($bill['Bill']['collectionfees']) && $bill['Bill']['collectionfees'] > 0)
    {
        $totaleAltezzaRighe += 4;
        ?>
        <tr <?php echo $classe; ?>>
            <td>Spese d'incasso </td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td align ="right">
                <?php
                // Aggiungo sulle spese d'incasso
                $collection_importo_iva =  number_format($bill['Bill']['collectionfees'] *  $collectionVat['Ivas']['percentuale'] /100,2,'.','');
                $arrayIva[$collectionVat['Ivas']['descrizione']]  +=  number_format($collection_importo_iva,2,'.','');
                $arrayImponibili[$collectionVat['Ivas']['descrizione']] += number_format($bill['Bill']['collectionfees'],2,'.','');// $imponibile_iva;
                $arrayCodici[$collectionVat['Ivas']['descrizione']] = $collectionVat['Ivas']['codice'];
                $arrayDescrizione[$collectionVat['Ivas']['descrizione']] = $collectionVat['Ivas']['descrizione'];
                $arrayPercentuali[$collectionVat['Ivas']['descrizione']] = $collectionVat['Ivas']['percentuale'];
                echo number_format($bill['Bill']['collectionfees'],2,',','');
                ?>
            </td>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td align ="right"><?= number_format($collectionVat['Ivas']['percentuale'],2,',',''); ?>%</td>
        </tr>
        <?php
        $importo_iva +=	$collection_importo_iva;
        $imponibile_iva += $bill['Bill']['collectionfees'];
    }

    // Aggiungo cassa previdenziale
    if(isset($bill['Bill']['welfare_box_code']) && $bill['Bill']['welfare_box_code'] != '' && $bill['Bill']['welfare_box_code'] != null && $bill['Bill']['welfare_box_vat_id'] != null && $bill['Bill']['welfare_box_percentage'] != null &&  $bill['Bill']['welfare_box_withholding_appliance'] != null)
    {
        $totaleAltezzaRighe += 4;
        $welfareBoxTotal = 0;
        ?>
        <tr <?php echo $classe; ?>>
            <td>Contributo cassa previdenziale <?= number_format($bill['Bill']['welfare_box_percentage'],2,',','') . ' %'; ?></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td align="right">
                <?php

                $welfareBoxTotal = number_format(($imponibile_iva -$imponibileStornoRitenutaAcconto) * $bill['Bill']['welfare_box_percentage'] / 100 ,2,'.','');
                echo number_format($welfareBoxTotal,2,',','');
                // Aggiungo la cassa all'iva
                $welfareBox_imponibile_iva = $welfareBoxTotal; // number_format($imponibile_iva,2,'.','') + number_format($welfareBoxTotal,2,'.','');
                $welfareBox_importo_iva =  number_format($welfareBoxTotal *  $welfareVat['Ivas']['percentuale'] /100,2,'.','');

                // Aggiungo i calcoli sulla cassa
                $arrayIva[$welfareVat['Ivas']['descrizione']]   +=  $welfareBox_importo_iva;
                $arrayImponibili[$welfareVat['Ivas']['descrizione']] += $welfareBox_imponibile_iva;// $imponibile_iva;
                $arrayCodici[$welfareVat['Ivas']['descrizione']] = $welfareVat['Ivas']['codice'];
                $arrayDescrizione[$welfareVat['Ivas']['descrizione']] = $welfareVat['Ivas']['descrizione'];
                $arrayPercentuali[$welfareVat['Ivas']['descrizione']] = $welfareVat['Ivas']['percentuale'];
                ?>
            </td>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td align="right">
                <?= number_format($welfareVat['Ivas']['percentuale'],2,',','') . '%'; ?>
            </td>
        </tr>
        <?php
    }
    ?>

    <?php
    if(isset($bill['Bill']['cig_type']) && $bill['Bill']['cig_type'] != null)
    {
        $totaleAltezzaRighe += 5;
        switch($bill['Bill']['cig_type'])
        {
            case 0: $typeDecription = 'Ordine d\'acquisto'; break;
            case 1: $typeDecription = 'Contratto'; break;
            case 2: $typeDecription = 'Convenzione'; break;
            case 3: $typeDecription = 'Fattura collegata'; break;
        }
        (isset($bill['Bill']['cig_date']) && $bill['Bill']['cig_date'] != null && $bill['Bill']['cig_date'] != '1970-01-01') ? $cigdate = 'del '. date('d/m/Y', $bill['Bill']['cig_date']) : $cigdate ='';
        ?>
    <tr <?php echo $classe; ?>>
        <td> <?= $typeDecription ?> num. <?= $bill['Bill']['cig_documentid'] ?> </td><td ></td><td ></td><td></td><td ></td><td ></td><td ></td>
        </tr><?php
    }


    $numeroDocuemnto = '';
    (isset($bill['Bill']['cig_num_item']) && $bill['Bill']['cig_num_item'] != null) ?  $numeroDocuemnto  .= 'Rif. numero riga: ' . $bill['Bill']['cig_num_item'] : null;

    if($numeroDocuemnto != '')
    {
        $totaleAltezzaRighe += 5;
        ?>
    <tr <?php echo $classe; ?>>
        <td><?= $numeroDocuemnto ?></td><td ></td><td ></td><td></td><td ></td><td ></td><td ></td>
        </tr><?php
    }


    $cignumitemecommessa ='';
    (isset($bill['Bill']['cig_codice_commessa']) && $bill['Bill']['cig_codice_commessa'] != null)  ? $cignumitemecommessa  .= 'Codice commessa/convenzione: ' . $bill['Bill']['cig_codice_commessa'] : null;
    if($cignumitemecommessa != '')
    {
        $thisNumeroRighe +=  floor(strlen($cignumitemecommessa) / $lunghezzaRiga) + 1;
        $altezzaRiga = $thisNumeroRighe *4 + $coefficienteFisso ;
        $totaleAltezzaRighe +=  ($altezzaRiga) ;
        ?>
    <tr <?php echo $classe; ?>>
        <td><?= $cignumitemecommessa ?></td><td ></td><td ></td><td></td><td ></td><td ></td><td ></td>
        </tr><?php
    }

    if(isset($bill['Bill']['cig']) && $bill['Bill']['cig'] != null)
    {
        $totaleAltezzaRighe += 5;
        ?>
    <tr <?php echo $classe; ?>>
        <td>Codice CIG:  <?= $bill['Bill']['cig'] ?></td><td ></td><td ></td><td></td><td ></td><td ></td><td ></td>
        </tr><?php
    }

    if(isset($bill['Bill']['cup']) && $bill['Bill']['cup'] != null)
    {
        $totaleAltezzaRighe += 5;
        ?>
    <tr <?php echo $classe; ?>>
        <td>Codice CUP:  <?= $bill['Bill']['cup'] ?></td><td ></td><td ></td><td ></td><td ></td><td ></td><td ></td>
        </tr><?php
    }

    $totale = $importo_iva + $imponibile_iva;

    if($bill['Bill']['tipologia'] == 8) // Pro-forma
    {
        $totaleAltezzaRighe += (5 * $coefficienteRiga)+4; // Qui ho messo 4 era 0
        ?>

        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <td class="font-size:9px;">	Il presente documento non costituisce fattura valida ai fini del DpR 633 26/10/1972 e successive modifiche. La fattura definitiva verrà emessa all’atto del pagamento del corrispettivo (articolo 6, comma 3, DpR 633/72).</td>
            <td class="normalFont"></td>
            <td class="normalFont"></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
        </tr>
        <?php
    }

    if($sp == true)
    {
        $totaleAltezzaRighe += (3 * $coefficienteRiga) + 4; // Era 0.2 ....
        ?>

        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <td class="font-size:9px;">Scissione dei pagamenti (art.17/ter DPR 633/72 introdotto dal comma 629 art. unico legge stabilità 2015) </td>
            <td class="normalFont"></td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td class="normalFont"></td>
        </tr>

        <?php
    }

    // Dicitura da inserire a partire dal 01/01/2019 se attivo modulo ixfe
    if(MODULE_IXFE) // Solo per chi è obbligato con la fattura elettronica
    {
        if(strtotime(date('Y-m-d')) > strtotime('2018-12-31') && strtotime($bill['Bill']['date']) > strtotime('2018-12-31'))
        {
            $totaleAltezzaRighe += (3 * $coefficienteRiga) + 12; // + 4
            ?>
            <tr <?= $classe; ?> style="background-color:#ffffff;">
                <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0028") { ?>
                    <td class="font-size:9px;">L'impresa affidataria, ai sensi e per gli effetti di quanto previsto dall'art.1 comma 43-bis della Legge 234/2021 s.m,i., in qualità di datore di lavoro, dichiara di applicare i contratti collettivi del settore edile, nazionale e territoriali, stipulati dalle associazioni datoriali e sindacali comparativamente più rappresentative sul piano nazionale e, nello specifico, il CCNL Artigianato - ANAEPA/CONFARTIGIANATO EDILIZIA, CNA Costruzioni, FIAE-CASARTIGIANI, CLAAI EDILIZIA, FENEAL-UIL, FILCA-CISL, FILLEA-CIGL ed altresì il Contratto Integrativo Provinciale dell'Edilizia del 4/7/2018 tra ANCE LECCO SONDRIO, ANAEPA CONFARIGIANATO IMPRESE SONDRIO, FENEAL-UIL, FILCA-CISL e FILLEA-CGIL.<br>Stampa priva di valenza giuridico-fiscale ai sensi dell’articolo 21 (Dpr 633/72), salvo per i soggetti non titolari di partita iva e/o non residenti ai sensi del comma 909 art.1 L.205/2017) </td>
                <?php }else{ ?>
                    <td class="font-size:9px;">Stampa priva di valenza giuridico-fiscale ai sensi dell’articolo 21 (Dpr 633/72), salvo per i soggetti non titolari di partita iva e/o non residenti ai sensi del comma 909 art.1 L.205/2017) </td>
                <?php } ?>
                <td class="normalFont"></td>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
                <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
                <td class="normalFont"></td>
            </tr>
            <?php
        }
    }

    for($i = $totaleAltezzaRighe +  $coefficienteRiga +3; $i< $numeroRigheFinale ;$i= $i +  $coefficienteRiga +3.1)
    {
        ?>
        <tr <?= $classe; ?> style="background-color:#ffffff;">
            <td class="normalFontFirst">&nbsp;</td>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnQuantity == 0)  { ?><td class="normalFont"></td><?php } ?>
            <?php if($removeColumnDiscount == 0)  { ?><td class="normalFont"></td><?php } ?>
            <td class="normalFont"></td>
            <td class="normalFont"></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>

<?php if(isset($bill['Bill']['accompagnatoria'])): ?>
    <?php if($bill['Bill']['accompagnatoria'] == 1): ?>
        <htmlpagefooter name="myfooter">
            <?= $this->element($pdfFooter,compact('arrayIva', 'arrayImponibili','arrayCodici','arrayRitenute','arrayDescrizione','arrayPercentuali','welfareBox_imponibile_iva','imponibileStornoRitenutaAcconto','rounding')); ?>
        </htmlpagefooter>
    <?php else: ?>
        <htmlpagefooter name="myfooter">
            <?= $this->element($pdfFooter,compact('arrayIva', 'arrayImponibili','arrayCodici','arrayRitenute','arrayDescrizione','arrayPercentuali','welfareBox_imponibile_iva','imponibileStornoRitenutaAcconto','rounding')); ?>
        </htmlpagefooter>
    <?php endif; ?>
<?php else: ?>
    <htmlpagefooter name="myfooter">
        <?= $this->element($pdfFooter,compact('arrayIva', 'arrayImponibili','arrayCodici','arrayRitenute','arrayDescrizione','arrayPercentuali','welfareBox_imponibile_iva','imponibileStornoRitenutaAcconto','rounding')); ?>
    </htmlpagefooter>
<?php endif;?>
