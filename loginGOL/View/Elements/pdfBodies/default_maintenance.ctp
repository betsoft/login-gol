<?php
	// Se è una fattura o una fattura pro-forma ed è accompagnatoria
    $defaultBodyFromHeaderPixel = '135px';
    $T_Bordered = "width:100mm;border-top: 1px solid black;border-right: 1px solid black;border-left: 1px solid black;text-align:center;padding-left:10px;padding-top:5px;background-color: #CDCDCD";
    $LR_Bordered = "width:50mm;border-left: 1px solid black;border-right: 1px solid black;padding-left:5px;";
    $L_Bordered = "width:35mm;border-left: 1px solid black;padding-left:5px;";
    $R_Bordered = "width:35mm;border-right: 1px solid black;padding-left:5px;";
    $N_Bordered = "width:35mm;padding-left:5px;";
    $LB_Bordered = "width:35mm;border-left: 1px solid black;padding-left:5px;border-bottom:1px solid black;";
    $RB_Bordered = "width:35mm;border-right: 1px solid black;padding-left:5px;border-bottom:1px solid black;";
    $NB_Bordered = "width:35mm;padding-left:5px;border-bottom:1px solid black;";
    $LRB_Bordered = "width:50mm;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;padding-left:5px;";
?>

<table style="border-collapse: collapse;width:100%;margin-top:<?= $defaultBodyFromHeaderPixel ?>" >
    <tr><td style="<?= $T_Bordered ?>;"><b>DATI CANTIERE</b></span><br /></td></tr>
    <tbody>
        <tr><td style="<?= $LR_Bordered; ?>"><?= $maintenance['Constructionsite']['name'] . ' - ' . $maintenance['Constructionsite']['description']; ?>&nbsp;</td></tr>
        <tr><td style="<?= $LR_Bordered; ?>"><?= $maintenance['Maintenance']['maintenance_address']; ?>&nbsp;</td></tr>
        <?php $maintenance['Maintenance']['maintenance_province'] ? $province = ' ('. $maintenance['Maintenance']['maintenance_province'] .') ': $province = ''; ?>
        <tr><td style="<?= $LRB_Bordered; ?>"><?= $maintenance['Maintenance']['maintenance_cap'] . ' ' .$maintenance['Maintenance']['maintenance_city'] . $province ; ?>&nbsp;</td></tr>
    </tbody>
</table>

<table style ="border-collapse: collapse;width:100%;margin-top:15px;">
    <tr><td style="<?= $T_Bordered ?>;" colspan ="4"><b>ORARI D'INTERVENTO</b></span><br /></td></tr>
    <tbody>
    <?php $countMaintenanceHours = 0; ?>
    <?php foreach($maintenance['Maintenancehour'] as $hour): ?>
        <?php
            $countTechnicians = 0;
            foreach($hour['Maintenancehourstechnician'] as $technician)
                $countTechnicians++;

            $countOutsideOperators = 0;
            foreach($hour['Maintenancehoursoutsideoperator'] as $operator)
                $countOutsideOperators++;
        ?>
        <tr>
        <?php $countMaintenanceHours++; ?>
        <?php if($countMaintenanceHours == count($maintenance['Maintenancehour'])): ?>
            <td style="<?= $LB_Bordered; ?>">Inizio intervento: <?= $hour['hour_from'] ?>&nbsp;</td>
            <td style="<?= $NB_Bordered; ?>">Fine intervento: <?= $hour['hour_to'] ?>&nbsp;</td>
            <td style="<?= $NB_Bordered; ?>">Tecnici: <?= $countTechnicians ?>&nbsp;</td>
            <td style="<?= $RB_Bordered; ?>">Operatori esterni: <?= $countOutsideOperators ?> </td>
        <?php else: ?>
            <td style="<?= $L_Bordered; ?>">Inizio intervento: <?= $hour['hour_from'] ?>&nbsp;</td>
            <td style="<?= $N_Bordered; ?>">Fine intervento: <?= $hour['hour_to'] ?>&nbsp;</td>
            <td style="<?= $N_Bordered; ?>">Tecnici: <?= $countTechnicians ?>&nbsp;</td>
            <td style="<?= $R_Bordered; ?>">Operatori esterni: <?= $countOutsideOperators ?>  </td>
        <?php endif; ?>
        </tr>
        <tr></tr>
        <tr></tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table style ="border-collapse: collapse;width:100%;margin-top:15px" >
    <tr><td style="<?= $T_Bordered ?>;"><b>DESCRIZIONE DELL'INTERVENTO</b></span><br /></td></tr>
    <tbody>
        <tr><td style="<?= $LRB_Bordered; ?>"><?= nl2br($maintenance['Maintenance']['intervention_description']); ?> &nbsp;</td></tr>
    </tbody>
</table>

<table style ="border-collapse: collapse;width:100%;margin-top:15px" >
    <tr><td style="<?= $T_Bordered ?>;" colspan="3"><b>DDT DI RIFERIMENTO</b></span><br /></td></tr>
    <tbody>
        <?php if(count($maintenance['Maintenanceddt']) > 0): ?>
            <?php $i = 0; ?>
            <?php foreach($maintenance['Maintenanceddt'] as $ddt): ?>
                <tr>
                    <?php $i++; ?>
                    <?php if($i == count($maintenance['Maintenanceddt'])): ?>
                        <?php if($ddt['number'] != '' && $ddt['date'] != '' &&  $ddt['state'] == 1): ?>
                            <td style="<?= $LB_Bordered; ?>">Numero ddt: <?= $ddt['number'] ?> &nbsp;</td>
                            <td style="<?= $NB_Bordered; ?>">Del: <?= date('d-m-Y',strtotime($ddt['date'])); ?> &nbsp;</td>
                            <td style="<?= $RB_Bordered; ?>">Fornitore: <?= isset($ddt['Supplier']['name']) ?  $ddt['Supplier']['name'] : ''?> &nbsp;</td>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($ddt['number'] != '' && $ddt['date'] != '' &&  $ddt['state'] == 1): ?>
                            <td style="<?= $L_Bordered; ?>">Numero ddt: <?= $ddt['number'] ?> &nbsp;</td>
                            <td style="<?= $N_Bordered; ?>">Del: <?= date('d-m-Y',strtotime($ddt['date'])); ?> &nbsp;</td>
                            <td style="<?= $R_Bordered; ?>">Fornitore: <?= isset($ddt['Supplier']['name']) ?  $ddt['Supplier']['name'] : ''?> &nbsp;</td>
                        <?php endif; ?>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr><td colspan="3" style="<?= $LRB_Bordered; ?>">&nbsp;</td></tr>
        <?php endif; ?>
    </tbody>
</table>

<table style ="border-collapse: collapse;width:100%;margin-top:15px" >
    <tbody>
        <tr colspan="2"><td colspan="2" style="<?= $T_Bordered ?>;"><b>PER PRESA VISIONE</b></span><br /></td></tr>
        <tr>
            <td style="width:60mm;border-left: 1px solid black;padding-left:5px">
                Cognome e nome : <?= $maintenance['Maintenance']['handling']  ?>&nbsp;
                <br/><br/>
                Indirizzo email : <?= $maintenance['Maintenance']['email'] ?>
            </td>
             <?php if(isset($maintenance['Maintenance']['signimage']) && $maintenance['Maintenance']['signimage'] != ''): ?>
                <td rowspan="1" style="width:50%;border-right:1px solid black;"> <img style="width:400px;" src="<?=($maintenance['Maintenance']['signimage']); ?>" ></td>
            <?php else: ?>
                 <td rowspan="1" style="width:50%;border-right:1px solid black;"> </td>
            <?php endif; ?>
        </tr>
        <tr>
            <td style="<?= $LB_Bordered; ?>" ></td>
            <td style="text-align:center;<?= $RB_Bordered; ?>;">Firma del cliente &nbsp;</td>
        </tr>
    </tbody>
</table>

<table style ="border-collapse: collapse;width:100%;margin-top:15px">
    <?php $technicianIdAlreadyPrinted = []; ?>
    <?php foreach($maintenance['Maintenancehour'] as $maintenanceHour): ?>
        <?php foreach ($maintenanceHour['Maintenancehourstechnician'] as $maintenanceHourTechnician): ?>
            <?php if(!in_array($maintenanceHourTechnician['technician_id'], $technicianIdAlreadyPrinted)): ?>
                <?php array_push($technicianIdAlreadyPrinted, $maintenanceHourTechnician['technician_id']); ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
    <?php if(count($technicianIdAlreadyPrinted) > 1): ?>
        <tr><td colspan="2" style="<?= $T_Bordered ?>;"><b>TECNICI RESPONSABILI</b></span><br/></td></tr>
    <?php else: ?>
        <tr><td colspan="2" style="<?= $T_Bordered ?>;"><b>TECNICO RESPONSABILE</b></span><br/></td></tr>
    <?php endif; ?>
    <tbody>
    <?php $technicianIdAlreadyPrinted = []; ?>
    <?php foreach($maintenance['Maintenancehour'] as $maintenanceHour): ?>
        <?php foreach ($maintenanceHour['Maintenancehourstechnician'] as $maintenanceHourTechnician): ?>
            <?php if(!in_array($maintenanceHourTechnician['technician_id'], $technicianIdAlreadyPrinted)): ?>
                <?php array_push($technicianIdAlreadyPrinted, $maintenanceHourTechnician['technician_id']); ?>
                <tr>
                    <td style="<?= $L_Bordered; ?>">Cognome Nome : <?= $maintenanceHourTechnician['Technician']['surname']. ' '. $maintenanceHourTechnician['Technician']['name'] ; ?></td>
                    <td style="<?= $R_Bordered; ?>"><center>Firma</center> &nbsp;</td>
                    <?php if(isset($maintenanceHourTechnician['Technician']['sign']) && $maintenanceHourTechnician['Technician']['sign'] != ''): ?>
                        <img src="data:image/jpg;base64,<?= $maintenancetechnician['Technician']['sign'] ?>" />
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
        <tr><td colspan="2" style="border-top: 1px solid black;"></td></tr>
    </tbody>
</table>

<htmlpagefooter name="myfooter">
<?php // echo $this->element($pdfFooter,compact('arrayIva', 'arrayImponibili','arrayCodici','arrayRitenute','arrayDescrizione','arrayPercentuali','welfareBox_imponibile_iva','imponibileStornoRitenutaAcconto','rounding')); ?>
</htmlpagefooter>