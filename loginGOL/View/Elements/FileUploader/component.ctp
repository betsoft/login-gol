<?= $this->element('FileViewer/loader') ?>
<?php
    /*
	$url
		url della risorsa da visualizzare
	*/
	
	if(!isset($id)) 
		$id = null;
		
	if(!isset($url)) 
		$url = null;
		
	if(!isset($downloadable))
		$downloadable = true;

	if(!isset($inputName))
		$inputName = 'uploaded_file';
		
	if(!isset($inputLabel))
		$inputLabel = 'File upload';
		
	$defaultInputOptions = ['label'=>false,'class'=>'hidden file-upload-input','id'=>'uploaded_file','capture'=>'camera'];
	if(!isset($inputOptions))
		$inputOptions = [];
		
	$inputOptions = array_merge($defaultInputOptions, $inputOptions);
	
	if(!isset($class))
		$class = "";
?>

<div class="file-uploader <?= $class ?>">
	<div class="row col-md-12">
		<?= $this->Form->file($inputName, $inputOptions); ?>
		<label class="btn color" for="<?= $inputOptions['id'] ?>"><?= $inputLabel ?></label>
	</div>
	<div class="row col-md-12" style="margin-top: 15px">
		<label for="<?= $inputOptions['id'] ?>" class="filename"></label>
	</div>
	<div class="row col-md-12" style="margin-top: 15px">
		<?= 
			$this->element('FileViewer/component', [
				'url'=> $url,
				'id'=> $inputOptions['id'],
			])
		?>
	</div>
</div>