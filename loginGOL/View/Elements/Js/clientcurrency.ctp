<script>
    function loadClientCurrency(clientInputId)
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "loadClientCurrency"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success : function(data)
            {
                // Se esiste la valuta
                data = JSON.parse(data);

                if(data.currency_name === undefined || data.currency_name == '' || data.currency_name == 'EUR')
                {
                    $(".jsMulticurrency").hide();
                    $(".jsChangeValue").val(data.currency_change); // Carico lo stesso il cambio che nel caso euro è 1
                }
                else // Se la valuta è euro
                {
                    $(".jsMulticurrency").show();
                    $(".jsChangeValue").val(data.currency_change);
                    $(".jsValueCode").html(data.currency_name);
                }
            }
        });
    }
</script>