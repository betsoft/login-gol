<script>
    function loadClientDifferentAddress(clientInputId,sezioneDiProvenienza)
    {
        /* Vado a prendere il listino passando eventuali destini diversi passando dalla ragione sociale */
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "getClientDifferentAddress"]) ?>",
            data:
            {
                clientId: $(clientInputId).val(),
            },
            success: function (data)
            {
                data = JSON.parse(data);
                switch (sezioneDiProvenienza)
                {
                    case 'fatture':
                        alternativeAddress = '#BillAlternativeaddressId';
                    break;
                    case  'ddt':
                        alternativeAddress = '#TransportAlternativeaddressId';
                    break;
                    case 'scontrino':
                        alternativeAddress = 'exit';
                    break;
                    case 'preventivi':
                        alternativeAddress = 'exit';
                    break;
                    default: // Aggiunto per nuova sezione Schede d'intervento
                        alternativeAddress = 'exit';
                    break;
                }

                // Metto il destino alternativo
                if (alternativeAddress != 'exit')
                {
                    $(alternativeAddress).find('option').remove().end();

                    if (data != null && data.length != 0)
                    {
                        $("#differentAddress").show();

                        $(alternativeAddress).append($("<option></option>").attr("value", 'empty').text(""));

                        $.each(data, function (key, element) {
                            if (element.nation != null)
                                var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province + '  ' + element.nation;
                            else
                                var indirizzo = element.name + ' - ' + element.address + '  ' + element.cap + '  ' + element.city + '  ' + element.province;
                            $(alternativeAddress).append($("<option></option>").attr("value", element.id).text(indirizzo));
                        })
                    }
                    else
                    {
                        $(alternativeAddress).val('');
                        $("#differentAddress").hide();
                    }
                }
            }
        });
    }
 </script>