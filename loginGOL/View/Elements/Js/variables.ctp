<script>
    function setArticles()
    {
        var articoli = [
            <?php
            if(isset($magazzini)){
            foreach ($magazzini as $magazzino)
            {
            $_id      = $magazzino['Storage']['id'];
            $_article =  $magazzino['Storage']['descrizione'];
            $_price   = $magazzino['Storage']['prezzo'];
            $_codice  = $magazzino['Storage']['codice'];
            $_unit  = $magazzino['Storage']['unit_id'];
            $_vat  = $magazzino['Storage']['vat_id'];
            $_barcode  = $magazzino['Storage']['barcode'];
            $_movable = $magazzino['Storage']['movable'];
            $_last_buy_price = $magazzino['Storage']['last_buy_price'];
            ?>
            {
                label: <?=  '"' . addslashes(str_replace(["\n","\r"], '', $_article)) . '"'; ?>,
                descrizione: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_article)) . '"'; ?>,
                value: <?= '"' . $_id . '"'; ?>,
                prezzo: <?= '"' . $_price . '"'; ?>,
                codice: <?= '"' . $_codice . '"'; ?>,
                unit: <?= '"' . $_unit . '"'; ?>,
                vat: <?= '"' . $_vat . '"'; ?>,
                barcode: '<?php '"' . addslashes(str_replace(["\n","\r"], '',$_barcode)) . '"'; ?>',
                movable: <?= '"' . $_movable . '"'; ?>,
                prezzoacquisto: <?= '"' . $_last_buy_price . '"'; ?>,
            },
        <?php
        }
            }?>

        ];
        return articoli;
    }

    function setCodes()
    {
        var codici = [
            <?php
            if(isset($magazzini)){
            foreach ($magazzini as $magazzino)
            {
            $_id      = $magazzino['Storage']['id'];
            $_article = $magazzino['Storage']['descrizione'];
            $_price   = $magazzino['Storage']['prezzo'];
            $_codice  = $magazzino['Storage']['codice'];
            $_unit  = $magazzino['Storage']['unit_id'];
            $_vat  = $magazzino['Storage']['vat_id'];
            $_barcode  = $magazzino['Storage']['barcode'];
            $_movable = $magazzino['Storage']['movable'];
            $_last_buy_price = $magazzino['Storage']['last_buy_price'];
            ?>
            {
                label: <?= '"' .  addslashes(str_replace(["\n","\r"], '',$_codice)) . '"'; ?>,
                value: <?= '"' . $_id . '"'; ?>,
                prezzo: <?= '"' . $_price . '"'; ?>,
                codice: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_codice)) . '"'; ?>,
                descrizione: <?= '"' . addslashes(str_replace(["\n","\r"], '',$_article)) . '"'; ?>,
                unit: <?= '"' . $_unit . '"'; ?>,
                vat: <?= '"' . $_vat . '"'; ?>,
                barcode: '<?php '"' . addslashes(str_replace(["\n","\r"], '',$_barcode)) . '"'; ?>',
                movable: <?= '"' . $_movable . '"'; ?>,
                prezzoacquisto: <?= '"' . $_last_buy_price . '"'; ?>,
            },
        <?php
        }

            }?>

        ];
        return codici;
    }

    function setArticlesNew(magazzini)
    {
        var articoli = [];
        $(magazzini).each(function(key, magazzino)
        {
            articoli.push({
                label: magazzino.Storage.descrizione,
                descrizione: magazzino.Storage.descrizione,
                value: magazzino.Storage.id,
                prezzo: magazzino.Storage.prezzo || "",
                codice: magazzino.Storage.codice,
                unit: magazzino.Storage.unit_id,
                vat: magazzino.Storage.vat_id,
                barcode: magazzino.Storage.barcode,
                codice: magazzino.Storage.codice,
                movable: magazzino.Storage.movable,
                prezzoacquisto: magazzino.Storage.last_buy_price,
            })
        });
        return articoli;
    }

    function setCodesNew(magazzini)
    {
        var codici = [];
        $(magazzini).each(function(key, magazzino)
        {
            codici.push({
                codice: magazzino.Storage.codice,
                value: magazzino.Storage.id,
                prezzo: magazzino.Storage.prezzo || "",
                codice: magazzino.Storage.codice,
                descrizione: magazzino.Storage.descrizione,
                unit: magazzino.Storage.unit_id,
                vat: magazzino.Storage.vat_id,
                barcode: magazzino.Storage.barcode,
                movable: magazzino.Storage.movable,
                prezzoacquisto: magazzino.Storage.last_buy_price,
            })
        });
        return codici;
    }

    function setClients()
    {
        <?php if(isset($clients)) { ?>
        var clienti = [
            <?php
            foreach ($clients as $client)
            {
                echo '"' . addslashes($client) . '",';
            }
            ?>
        ];

        return clienti;

        <?php } ?>
    }

    function setSuppliers()
    {
         <?php if(isset($suppliers)) { ?>

         var suppliers = [
            <?php

            foreach ($suppliers as $supplier)
              {
                echo '"' . addslashes($supplier) . '",';
              }
            ?>
        ];

         return suppliers;

        <?php } ?>
    }
</script>
