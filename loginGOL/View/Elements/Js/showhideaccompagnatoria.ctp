<script>
    function showHideAccompagnatoria()
    {
        if($("#BillAccompagnatoria").prop("checked") == true)
        {
            $("#billTransport").show();
            $("#BillDepositId").attr("required", "true");
        }
        else
        {
            $("#billTransport").hide();
            $("#BillDepositId").removeAttr("required");
        }

        $("#BillAccompagnatoria").click(
            function()
            {
                if($(this).prop("checked") == true)
                {
                    $("#billTransport").show();
                    $("#BillDepositId").attr("required", "true");
                }
                else
                {
                    $("#billTransport").hide();
                    $("#BillDepositId").removeAttr("required");
                }
            }
        );
    }
</script>
