  <script>

        // Controllo gli articoli se vengono salvati        
        function checkIfAreNewArticles(formName,clientId)
        {
            var savedArticles = [];

            $(".jsDescription").each(
                function()
                {
                    if(savedArticles.indexOf($(this).val()) === -1) 
                    {
                        savedArticles.push($(this).val());
                    }
                });
               
             if(clientId == null)
             {
                 myClient = -100;
             }
             else
             {
                 myClient = $(clientId).val();
             }

            $.ajax
            ({
    		    method: "POST",
    			url: "<?= $this->Html->url(["controller" => "storages","action" => "checkIfStorageArticleExist"]) ?>",
    			data: 
    			{
                    articles: savedArticles,   
                    clientId: myClient,
    			},
    			success: function(data)
    			{    
    			    console.log('test');
    			    console.log(data);
    			     data = JSON.parse(data);
    			     var counter = 0;
    			     var rows = "<table style=\"width:100%;\">";
    			     
    			     $(data).each(function(index, value)
    			     {
    			        counter++;
    			        var myArticles = $('<select class="form-control">'); 
                        myArticles.append($("<option></option>").attr("value",1).text('Articolo di magazzino')); 
                        
                        if(formName != "#LoadgoodAddForm")
                        {
    			            myArticles.append($("<option></option>").attr("value",0).text('Voce descrittiva')); 
                        }
    			        
    			        rows = rows + "<tr class=\"alertRow\">";
    			        rows = rows + "<td style=\"width:50%;padding:5px;vertical-align:middle;text-align:left;font-size:12px;background-color:#dadfe0;border:1px solid black;\"><div class=\"alertRowValue\" >" + value + "</div></td>";
    			        rows = rows + "<td style=\"width:50%;\"><div class=\"alertRowChoice \">"+ myArticles[0].outerHTML  + "</div></td>";
                        rows = rows + "</tr>";	        
    			     });
    			     
    			     rows = rows + "</table>";

    			    if(counter > 0)
    			    {
                        Frizzysaveconfirm("<form id=\"scartaccini4\" ><br/><b>I seguenti articoli / descrizioni non presenti a sistema: </b><br/><br/><br/>" + rows + '</form>' ,"Aggiunta articoli a magazzino",function(userChoice) 
            			{
        	    		    if(userChoice == enhancedDialogsTypes.SAVE) 
        	    		    {
            	    		    if($("#scartaccini4")[0].reportValidity())
        	        			{
        	        			    $(".alertRow").each(
        	        			        function()
        	        			        {
        	        			            var articleDescription = $(this).find('.alertRowValue').text();
        	        			            var articleType = $(this).find('.alertRowChoice select').val();
        	        			            $(".jsDescription").each(
        	        			                function()
        	        			                  {
        	        			                      if($(this).val() == articleDescription)
        	        			                      {
        	        			                          $(this).parent().find(".jsMovable").val(articleType);
        	        			                      }
        	        			                  });
        	        			            });
        	        			        $(formName).trigger('submit.default');
            	        			}
        	    		        }
                             })
    			    }
    			    else
    			    {
    			       $(formName).trigger('submit.default');
    			    }
    			}
            });
        }
        </script>