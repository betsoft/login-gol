<script>
    /* Recupero i dati del cliente */
    function loadClientDiscount(clientInputId,sezioneDiProvenienza)
    {
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "getClientDiscount"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success: function(data)
            {
                var exit = false;
                data = JSON.parse(data);
                $.each(data,
                    function(key, element)
                    {
                        switch(sezioneDiProvenienza)
                        {
                            case 'fatture':
                                discount = '#Good0Discount';
                            break;
                            case 'ddt':
                                discount = '#Transportgood0Discount';
                            break;
                            default:
                                exit = true;
                            break;
                        }

                        // Se non esistono gli oggetti dove andare ad inserire i valori
                        try
                        {
                            if(exit == false)
                            {
                                $(".contacts_row.clonableRow").each(
                                    function(index)
                                    {
                                        var discountField = $(this).find($("[name*='discount']"));
                                        var discountValue = discountField.val();

                                        if(discountValue == '')
                                        {
                                            discountField.val(element.discount);
                                        }
                                    }
                                );
                            }
                        }
                        catch (err) // segnalo solo l'errore
                        {
                        }
                    }
                )
            }
        });
    }
</script>