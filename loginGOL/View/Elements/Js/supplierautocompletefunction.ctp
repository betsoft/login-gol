<?= $this->element('Js/supplierdifferentaddress'); ?>
<script>
    function setSupplierAutocomplete(fornitori,supplierInputId)
    {
         startSupplierId = supplierInputId;

          $(supplierInputId).autocomplete(
          {
            source: fornitori,
            change: function (event, ui) 
            {
                $.ajax
                ({
    			    method: "POST",
    				url: "<?= $this->Html->url(["controller" => "bills","action" => "getSupplierAddress"]) ?>",
    				data: 
    				{
    				     clientId : $(supplierInputId).val(),
    				},
    				success: function(data)
    				{
    				    data = JSON.parse(data);
    					$.each(data,
    					    function(key, element)
    					    {
                                $("#BillSupplierAddress").val(element.address);
                                $("#BillSupplierCap").val(element.cap);
                                $("#BillSupplierCity").val(element.city);
                                $("#BillSupplierProvince").val(element.province);
                                try
                                {
                                    $("#supplier_nation_multiple_select").val(element.nation);
                                    $("#supplier_nation_multiple_select").multiselect('rebuild');
                                }
                                catch(isNotAError)
                                {
                                    
                                }
                                // Bolle
                                loadSupplierDifferentAddress($(supplierInputId).val(),'ddt');
                                $("#TransportSupplierAddress").val(element.address);
                                $("#TransportSupplierCap").val(element.cap);
                                $("#TransportSupplierCity").val(element.city);
                                $("#TransportSupplierProvince").val(element.province);
                                try
                                {
                                    $("#supplier_nation_multiple_select").val(element.nation);
                                    $("#supplier_nation_multiple_select").multiselect('rebuild');
                                }
                                catch(isNotAError)
                                {
                                    
                                }
    					   })
				    }
                });
                    
                return false;
            },
                    
        });
    }
</script>
        