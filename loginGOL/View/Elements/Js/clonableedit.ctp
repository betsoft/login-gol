<script>

    function enableCloningedit(clientName, clientInputId)
    {
        // Sbindo
        $("#aggiungi_riga").unbind('click');
        $("#aggiungi_nota").unbind('click');
        $("#aggiungi_subtotale").unbind('click');
        $("#aggiungi_opt").unbind('click');

        var clonato;
        var nuova_chiave = $(".lunghezza").length - 1;

        /** Identifico quale è la tipologia chiamante  */
        switch(clientInputId)
        {
            case '#QuoteClientId':
                sezioneDiProvenienza = 'preventivi';
                campoDescrizione = "Description";
                nomeOggetti = "Quotegoodrow";
                break;
            case '#BillClientId':
                sezioneDiProvenienza = 'fatture';    // E note di credito // E scontrini // Entrata merce
                campoDescrizione = "Oggetto";
                nomeOggetti = "Good";
                break;
            case '#OrderClientId':
                sezioneDiProvenienza = 'ordinivendita';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Orderrow";
                break;
            case '#TransportClientId':
                sezioneDiProvenienza = 'ddt';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Transportgood";
                break;
            case '#LoadgoodSupplierId':
                sezioneDiProvenienza = 'entratamerce';
                campoDescrizione = "Description";
                nomeOggetti = "Loadgoodrow";
                break;
            // Solo per versione cone gestione rapportini
            case "#MaintenanceClientId":
                sezioneDiProvenienza = 'schedaintervento';
                campoDescrizione = "Description";
                nomeOggetti = "Maintenancerow";
                break;
            case "#PurchaseorderSupplierId":
                sezioneDiProvenienza = 'ordineacquisto';
                campoDescrizione = "Description";
                nomeOggetti = "Purchaseorderrow";
                break;
        }

        $("#"+nomeOggetti+"0"+campoDescrizione).prop('required',true);

        $(".originale .remove").hide();

        $("#aggiungi_riga").click(
            function()
            {
                MakeTheClone(this,'riga',clientName,clientInputId,sezioneDiProvenienza,campoDescrizione,nomeOggetti);
            }
        );

        $("#aggiungi_nota").click(
            function()
            {
                MakeTheClone(this,'nota',clientName,clientInputId,sezioneDiProvenienza,campoDescrizione,nomeOggetti);
            }
        );

        $("#aggiungi_subtotale").click(
            function ()
            {
                MakeTheClone(this, 'subtotale', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        $("#aggiungi_opt").click(
            function ()
            {
                MakeTheClone(this, 'optrow', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        function MakeTheClone(objectToClone,typeofcloning,clientName,clientInputId,sezioneDiProvenienza,campoDescrizione,nomeOggetti)
        {
            if (sezioneDiProvenienza == 'preventivi')
                var array = $.parseJSON("<?= isset($this->data['Quotegoodrow']) ? addslashes(json_encode($this->data['Quotegoodrow'])) : null; ?>");

            if (sezioneDiProvenienza == 'ordinivendita')
                var array = $.parseJSON("<?= isset($this->data['Orderrow']) ? addslashes(json_encode($this->data['Orderrow'])) : null; ?>");
            if (sezioneDiProvenienza == 'entratamerce')
                var array = $.parseJSON("<?= isset($this->data['Loadgoodrow']) ? addslashes(json_encode($this->data['Loadgoodrow'])) : null; ?>");

            if (sezioneDiProvenienza == 'ddt')
                var array = $.parseJSON("<?= isset($this->data['Transportgood']) ? addslashes(json_encode($this->data['Transportgood'])) : null; ?>");

            if (sezioneDiProvenienza == 'fatture')
                var array = $.parseJSON("<?= isset($this->data['Good']) ? addslashes(json_encode($this->data['Good'])) : null; ?>");

            if (sezioneDiProvenienza == 'schedaintervento')
                var array = $.parseJSON("<?= isset($this->data['Maintenancerow']) ? addslashes(json_encode($this->data['Maintenancerow'])) : null; ?>");

            if (sezioneDiProvenienza == 'ordineacquisto')
                var array = $.parseJSON("<?= isset($this->data['Purchaseorderrow']) ? addslashes(json_encode($this->data['Purchaseorderrow'])) : null; ?>");

            if(array[0] != undefined)
            {
                var firstId = $('.originale').attr('id');
                firstId = firstId.replace(/'/gi, "");
                var newId = $(".lunghezza").length + 1;
            }

            nuova_chiave = nuova_chiave + 1;
            var clonableRowDescription = ".clonato #" + nomeOggetti + nuova_chiave;

            var clonableFieldType = clonableRowDescription + "Tipo";
            var clonableFieldCode = clonableRowDescription + "Codice";
            var clonableFieldDescription = clonableRowDescription + "Oggetto";

            clonato = $('.originale').clone();
            $(clonato).removeClass("originale");
            $(clonato).addClass("clonato");
            $(clonato).addClass("ultima_riga");
            $(clonato).insertAfter(".ultima_riga");

            $(clonato).removeClass("principale" + firstId);
            $(clonato).addClass("principale" + newId);

            $(".ultima_riga").removeClass("ultima_riga");

            if (sezioneDiProvenienza == 'fatture') {

                <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047") { ?>
                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'TransportId', 'Id', 'VariationId', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied', 'ComplimentaryQuantity'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'transport_id', 'id', 'variation_id', 'tipo', 'importo', 'currencyprice', 'withholdingapplied', 'complimentaryQuantity'];

                if (typeofcloning == 'nota') {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', newId, '', '', '', '', '1', '0']
                } else {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', newId, '', '', '', '', '1', '0'];
                }
                <?php } else { ?>
                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'TransportId', 'Id', 'VariationId', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'transport_id', 'id', 'variation_id', 'tipo', 'importo', 'currencyprice', 'withholdingapplied'];

                if (typeofcloning == 'nota') {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', newId, '', '', '', '', '1']
                } else {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', newId, '', '', '', '', '1'];
                }
                <?php } ?>


                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + firstId + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + firstId + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                    $(clonableRowDescription + Campi[i]).removeAttr('readonly');
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();

                if (typeofcloning == 'nota') {
                    $(".clonato").find(".jsRowField").hide();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").show();
                    $(".clonato").find(".jsDescriptionHelper").unbind();
                    $(".clonato").find(".jsDescriptionHelper").click(function () {
                        showHelpMessage('note');
                    });
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");

                    $(clonableFieldDescription).autocomplete({disabled: true});
                } else {
                    // $(".clonato").find(".jsRowField").show();
                    // Nascondo l'helper
                    $(".clonato").find(".jsDescriptionHelper").hide();

                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                    <?php
                    if(MULTICURRENCY)
                    { ?>
                    $(".clonato").find(".jsMulticurrency").show();
                    <?php }
                    ?>
                }
            }

            if (sezioneDiProvenienza == 'ddt') {
                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'TransportId', 'Id', 'VariationId', 'Tipo', 'Importo'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'transport_id', 'id', 'variation_id', 'tipo', 'importo'];

                if (typeofcloning == 'nota') {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', newId, '', '', ''];
                } else {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', newId, '', '', ''];
                }

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();

                if (typeofcloning == 'nota') {
                    $(".clonato").find(".jsRowField").hide();
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");

                    $(clonableFieldDescription).autocomplete({disabled: true});
                } else {
                    $(".clonato").find(".jsRowField").show();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                    <?php
                    if(MULTICURRENCY)
                    { ?>
                    $(".clonato").find(".jsMulticurrency").show();
                    <?php }
                    ?>
                }
            }
            if (sezioneDiProvenienza == 'ordinivendita') {
                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied', 'ComplimentaryQuantity'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'tipo', 'importo', 'currencyprice', 'withholdingapplied', 'complimentaryQuantity'];

                if (typeofcloning == 'nota')
                {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', '', '', '1', 0];
                }
                else
                {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', '', '', '1', 0];
                }

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                    $(clonableRowDescription + Campi[i]).removeAttr('readonly');

                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();

                if (typeofcloning == 'nota') {
                    $(".clonato").find(".jsRowField").hide();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").show();
                    $(".clonato").find(".jsDescriptionHelper").unbind();
                    $(".clonato").find(".jsDescriptionHelper").click(function () {
                        showHelpMessage('note');
                    });
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");

                    $(clonableFieldDescription).autocomplete({disabled: true});
                } else {
                    // $(".clonato").find(".jsRowField").show();
                    // Nascondo l'helper
                    $(".clonato").find(".jsDescriptionHelper").hide();

                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }

            if (sezioneDiProvenienza == 'entratamerce') {

                var clonableFieldDescription = clonableRowDescription + "Description";

                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Quantity', 'LoadGoodRowPrice', 'LoadGoodRowVatId', 'Barcode', 'Movable', 'StorageId', 'Tipo', 'Importo', 'Suggestedsellprice'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'quantity', 'load_good_row_price', 'load_good_row_vat_id', 'barcode', 'movable', 'storage_id', 'tipo', 'importo', 'suggestedsellprice'];
                var newLineValue = ['', '', '', 1, '', '', '', '', '', 1, '', 0];

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();

            }

            if (sezioneDiProvenienza == 'preventivi') {
                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Quantity', 'QuoteGoodRowPrice', 'QuoteGoodRowVatId', 'Movable', 'StorageId', 'VariationId', 'Tipo', 'Importo'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'quantity', 'quote_good_row_price', 'quote_good_row_vat_id', 'movable', 'storage_id', 'variation_id', 'Tipo', 'Importo'];

                if (typeofcloning == 'nota' || typeofcloning == 'subtotale' || typeofcloning == 'optrow') {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', ''];
                } else {
                    var newLineValue = ['', '', '', 1, '', '', '', '', '', '', ''];
                }

                var clonableFieldDescription = clonableRowDescription + "Description";

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                    $(clonableRowDescription + Campi[i]).removeAttr('readonly');
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);


                $(".clonato .remove").show();

                console.log(typeofcloning);
                if (typeofcloning == 'nota' || typeofcloning == 'subtotale' || typeofcloning == 'optrow') {
                    $(".clonato").find(".jsRowField").hide();
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");

                    // Assegno il valore dei movable per subtotale e optrow
                    switch(typeofcloning)
                    {
                        case 'subtotale':
                            $(".clonato").find(".jsMovable").val('-10');
                            $(".clonato .jsStrongDesc").html('SUBTOTALE');
                            $(".clonato .jsStrongDesc").css('color','#ea5d0b');
                            $(".clonato .fa-asterisk").remove();
                            break;
                        case 'optrow' :
                            $(".clonato").find(".jsMovable").val('-20');
                            $(".clonato .jsStrongDesc").html('SEZIONE OPZIONALE');
                            $(".clonato .jsStrongDesc").css('color','#589AB8');
                            $(".clonato .fa-asterisk").remove();
                            break;
                        case 'nota' :
                            //  $(".clonato").find(".jsMovable").val('-30');
                            break;
                    }

                    $(clonableFieldDescription).autocomplete({disabled: true});
                } else {
                    $(".clonato").find(".jsRowField").show();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }

            /** SCHEDA INTERVENTO **/
            if (sezioneDiProvenienza == 'schedaintervento')
            {
                var Campi = ['Codice', 'Description', 'Quantity', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'TransportId', 'Id', 'VariationId', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied'];
                var Label = ['codice', 'description', 'quantity', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'transport_id', 'id', 'variation_id', 'tipo', 'importo', 'currencyprice', 'withholdingapplied'];

                var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', newId, '', '', '', '', '1'];

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                    $(clonableRowDescription + Campi[i]).removeAttr('readonly');
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();

                $(".clonato").find(".jsRowField").show();

                // Nascondo l'helper
                $(".clonato").find(".jsDescriptionHelper").hide();

                $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                $(clonableFieldDescription).autocomplete({disabled: false});
                <?php
                if(MULTICURRENCY) { ?>
                $(".clonato").find(".jsMulticurrency").show();
                <?php }
                ?>
            }
            /** FINE SCHEDA INTERVENTO **/

            /** ORDINI D'ACQUISTO **/
            if (sezioneDiProvenienza == 'ordineacquisto') {
                var clonableFieldDescription = clonableRowDescription + "Description";

                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Quantity', 'Price', 'VatId', 'Barcode', 'Movable', 'StorageId', 'Tipo', 'Importo'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'quantity', 'price', 'vat_id', 'barcode', 'movable', 'storage_id', 'tipo', 'importo'];
                var newLineValue = ['', '', '', 1, '', '', '', 1, '', 1, ''];

                for (i = 0; i < Campi.length; i++) {
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                    $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                    $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
                }

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(clonableFieldDescription).prop('required', true);

                $(".clonato .rimuoviRigaIcon").css('color', 'red');
                $(".clonato .rimuoviRigaIcon").attr('title', 'Rimuovi riga');
                $(".clonato .remove").show();
            }
            /** FINE ORDINI D'ACQUISTO **/

            $(clonato).addClass("ultima_riga");
            $(clonato).attr('id', nuova_chiave);
            $(clonato).removeClass("clonato");

            // Prendo la x che cancella la riga (per i campi aggiunti dinamicamente)
            var temp = $(".ultima_riga .rimuoviRigaIcon");

            // E ne aggiungo l'evento click che elimina la riga
            $(temp).first(".rimuoviRigaIcon").click(
                function ()
                {
                    if ($(this).parent().hasClass('ultima_riga')) {
                        $(this).parent().prev('.lunghezza').addClass("ultima_riga");
                        $(this).parent().remove();
                    } else {
                        $(this).parent().remove();
                    }
                }
            )

            if (clientName != null) // Il catalogo è solo per clienti
            {
                loadClientCatalog(nuova_chiave, clientName, clientInputId, nomeOggetti);
            }
            else
            {
                var articoli = setArticles();

                $(codici).each(
                    function (key, val)
                    {
                        val.label = val.codice;
                    }
                )

                loadSupplierCatalog(articoli, nomeOggetti, nuova_chiave, sezioneDiProvenienza, codici);
            }

            // Aggiungo il calcolo degli importi
            $(".jsPrice").change(function () {
                setImporto(this);
                <?php if(MULTICURRENCY){ ?>setCurrencyChange(this, 'toCurrency');  <?php } ?>
            });
            $(".jsChange").change(function () {
                <?php if(MULTICURRENCY){ ?>setCurrencyChange(this, 'toEuro');  <?php } ?>
                setImporto(this);
            });
            $(".jsQuantity").change(function () {
                setImporto(this);
            });
            $(".jsTipo").change(function () {
                setCodeRequired(this);
            });

            // Aggiungo il rimuovi icona
            addcrossremoving();
        }

        $('.contacts_row .clonato').removeClass("clonato");
    }

</script>
