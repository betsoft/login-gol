<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/addcrossremovingddt'); ?>
<?= $this->element('Js/addcrossremovinghour'); ?>
<script>
    jQuery(function($){
        $.datepicker.regional['it'] = {
            closeText: 'Chiudi',
            prevText: '&#x3c;Prec',
            nextText: 'Succ&#x3e;',
            currentText: 'Oggi',
            monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
                'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
            monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
                'Lug','Ago','Set','Ott','Nov','Dic'],
            dayNames: ['Domenica','Luned&#236','Marted&#236','Mercoled&#236','Gioved&#236','Venerd&#236','Sabato'],
            dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
            dayNamesMin: ['Do','Lu','Ma','Me','Gio','Ve','Sa'],
            dateFormat: 'dd/mm/yy', firstDay: 1,
            isRTL: false};
        $.datepicker.setDefaults($.datepicker.regional['it']);
    });

    function clonableadvanced(addRowButtonId,fields, label, newlinevalue, nomeOggetti,functionToExecute, functionparameters,codici)
    {
        var clonato;
        var nuova_chiave = 0 ;

        $(addRowButtonId).click(
            function()
            {
                nuova_chiave = nuova_chiave + 1;

                clonato = $('.originale').clone();
                $(clonato).removeClass("originale");
                $(clonato).addClass("clonato");
                $(".ultima_riga").removeClass("ultima_riga");

                $(clonato).insertAfter($(".clonableRow").last());

                for (i = 0; i<fields.length; i++)
                {
                    $(".clonato #"+nomeOggetti+"0"+fields[i]).attr('name', "data["+nomeOggetti+"][" + nuova_chiave + "]["+label[i]+"]");
                    $(".clonato #"+nomeOggetti+"0"+fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]);
                    $(".clonato #"+nomeOggetti+ nuova_chiave + fields[i]).val(newlinevalue[i]);
                }

                $(".clonato .rimuoviRigaIcon").show();

                $(clonato).addClass("ultima_riga");
                $(clonato).removeClass("clonato");

                // Aggiungo il rimuovi icona
                addcrossremoving()

                switch(functionToExecute)
                {
                    // Trasferimento tra depositi
                    case "indexdepositmovementclonable":
                        indexdepositmovementclonable(nuova_chiave,functionparameters,codici);
                        break;
                }
            }
        );
    }

    function clonableadvancedDdt(addRowButtonId,fields, label, newlinevalue, nomeOggetti)
    {
        var clonato;
        var nuova_chiave = <?= isset($this->request->data['Maintenanceddt']) ? count($this->request->data['Maintenanceddt']) : 1; ?> - 1;
        jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});

        $(addRowButtonId).click(
            function ()
            {
                nuova_chiave = nuova_chiave + 1;

                jQuery("#Ddt0Ddtdate").datepicker("destroy");

                if($('.originaleddt').length == 0)
                {
                    $(".clonableRowddt").each(
                        function(index)
                        {
                            if(index == 0)
                            {
                                $(this).addClass('originaleddt');
                            }
                        }
                    );
                }

                clonato = $('.originaleddt').clone();
                $(clonato).removeClass("originaleddt");
                $(clonato).addClass("clonatoddt");
                $(".ultima_rigaddt").removeClass("ultima_rigaddt");

                $(clonato).insertAfter($(".clonableRowddt").last());

                for (i = 0; i < fields.length; i++)
                {
                    switch (fields[i])
                    {
                        case 'ddtdate':
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + 'Ddtdate').attr('id', nomeOggetti + nuova_chiave + fields[i]);
                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + 'Ddtdate').val(newlinevalue[i]);

                            jQuery(".clonatoddt #" + nomeOggetti + nuova_chiave + "ddtdate").datepicker({
                                "dateFormat": "dd-mm-yy",
                                "showAnim": "slideDown"
                            });

                            jQuery("#Ddt0Ddtdate").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});
                        break;
                        case 'supplier_id_multiple_select':
                            $(".clonatoddt #supplier_id_multiple_select").attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #supplier_id_multiple_select").attr('id', "supplier_id_multiple_select" + nuova_chiave);
                            $(".clonatoddt #supplier_id_multiple_select" + nuova_chiave).val('');
                            $(".clonatoddt #supplier_id_multiple_select" + nuova_chiave).attr('required', 'trow(ue');

                            var id = $(clonato).find('.multiselect-native-select').find('select').attr('id');
                            initializeFilterableSelects(id);)
                            $(clonato).find('.multiselect-native-select').find('.btn-group')[1].remove();
                        break;
                        default:
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + label[i] + "]");
                            $(".clonatoddt #" + nomeOggetti + "0" + fields[i]).attr('id', nomeOggetti + nuova_chiave + fields[i]);
                            $(".clonatoddt #" + nomeOggetti + nuova_chiave + fields[i]).val(newlinevalue[i]);
                        break;
                    }
                }

                $(".clonatoddt .rimuoviRigaIconddt").show();

                $(clonato).addClass("ultima_rigaddt");
                $(clonato).removeClass("clonatoddt");

                // Aggiungo il rimuovi icona
                addcrossremovingddt();
            }
        );
    }
</script>
