<script>
    function loadClientPaymentMethod(clientInputId, destinationField, returnName = false)
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "getClientPaymentMethod"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success : function(data)
            {
                if(returnName == false)
                {
                    $(destinationField).val(data);
                }
                else
                {
                    $.ajax({
                        method: "POST",
                        url: "<?= $this->Html->url(["controller" => "payments", "action" => "getPaymentName"]) ?>",
                        data:
                        {
                            paymentId: data,
                        },
                        success: function (data)
                        {
                            $(destinationField).val(data);
                        }
                    });
                }
            }
        });
	
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "getDefaultClientPaymentMethodCollectionfees"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success : function(data)
            {
                if(data > 0)
                {
                    $("#BillCollectionfees").val(data);
                    $(".jsAliquota").show();
                    $('#BillCollectionfeesvatid').attr('required',true);
                }
            },
        });
    }
</script>