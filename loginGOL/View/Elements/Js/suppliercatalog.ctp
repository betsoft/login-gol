<script>
function loadSupplierCatalog(articoli,nomeOggetti,classePerIncremento,sezioneDiProvenienza,codici) {
    var rowDescription = "#" + nomeOggetti + classePerIncremento;
    var fieldDescription = rowDescription + "Oggetto";
    var fieldCode = rowDescription + "Codice";

    switch (sezioneDiProvenienza) {
        case 'fatture':
            break;
        case 'entratamerce':
            fieldDescription = rowDescription + "Description";
            break;
        case 'ordineacquisto':
            fieldDescription = rowDescription + "Description";
            break;
    }

    $(fieldDescription).autocomplete
    ({
        focus: function (event, ui) {
            event.preventDefault();
            return false;
        },
        select: function (event, ui) {
            event.preventDefault();
            autocompleteItem(sezioneDiProvenienza, classePerIncremento, ui.item, 'descrizione');
        },
        change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
        {
            if (ui.item == null) // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
            {
                // Rimozione tipo / Codice
                setReadonlyValues('descrizione', this, articoli, classePerIncremento, sezioneDiProvenienza);
            }
        },
        source: articoli,
    });

    // Ricerca del campo codice
    $(fieldCode).autocomplete({
        focus: function (event, ui) {
            event.preventDefault();
            return false;
        },
        select: function (event, ui) {
            event.preventDefault();
            autocompleteItem(sezioneDiProvenienza, classePerIncremento, ui.item, 'codice');
        },
        change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
        {
            if (ui.item == null) // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
            {
                setReadonlyValues('codice', this, articoli, classePerIncremento, sezioneDiProvenienza);
            }
        },
        source: codici
    });

    function autocompleteItem(sezioneDiProvenienza, classePerIncremento, item, typeOfAutocomplete) {


            var rowDescription = "#" + nomeOggetti + classePerIncremento;
            switch (sezioneDiProvenienza) {
                case 'fatture':
                    var fieldDescription = rowDescription + "Oggetto";
                    var fieldUnitOfMeasure = rowDescription + "Unita";
                    var fieldPrice = rowDescription + "Prezzo";
                    var fieldVat = rowDescription + 'IvaId';
                    break;
                case 'entratamerce':
                    var fieldDescription = rowDescription + "Description";
                    var fieldUnitOfMeasure = rowDescription + "UnitOfMeasureId";
                    var fieldPrice = rowDescription + "LoadGoodRowPrice";
                    var fieldVat = rowDescription + 'LoadGoodRowVatId';
                    $(rowDescription + "Suggestedsellprice").val(item.prezzo);
                    break;
                case 'ordineacquisto':
                    var fieldDescription = rowDescription + "Description";
                    var fieldUnitOfMeasure = rowDescription + "UnitOfMeasureId";
                    var fieldPrice = rowDescription + "Price";
                    var fieldVat = rowDescription + 'VatId';
                    break;
            }

            var fieldCode = rowDescription + "Codice";
            var fieldStorageId = rowDescription + "StorageId";
            var fieldBarcode = rowDescription + "Barcode";
            var fieldType = rowDescription + "Tipo";
            var fieldMovable = rowDescription + "Movable";
            var fieldImporto = rowDescription + "Importo";
            var fieldQuantity = rowDescription + "Quantita";

            if (typeOfAutocomplete == 'descrizione') {
                $(fieldCode).attr("readonly", true);
                $(fieldCode).css("background", "#f7f7f7");
                $(fieldDescription).attr("readonly", false);
                $(fieldDescription).css("background", "#ffffff");
                $(fieldDescription).val(item.descrizione);
            }

            if (typeOfAutocomplete == 'codice') {
                if (item.codice == '' || item.codice == null ) {
                    $(fieldDescription).attr("readonly", false);
                    $(fieldDescription).css("background", "#ffffff");
                    $(fieldDescription).val('');
                } else {
                    $(fieldDescription).attr("readonly", true);
                    $(fieldDescription).css("background", "#f7f7f7");
                    $(fieldCode).attr("readonly", false);
                    $(fieldCode).css("background", "#ffffff");
                    $(fieldDescription).val(item.descrizione);
                }
            }


                $(fieldPrice).val(item.prezzoacquisto);
                $(fieldVat).val(item.vat);
                $(fieldStorageId).val(item.value);
                $(fieldCode).val(item.codice);
                $(fieldUnitOfMeasure).val(item.unit);
                $(fieldBarcode).val(item.barcode);

            // A Seconda della tipologia modifico il tipo e metto disabled
            $(fieldType).val(item.movable);
            $(fieldType).attr("disabled", true);
            $(fieldMovable).val(item.movable);

            // Ridondo il required - Carico l'iva del cliente
            if (item.vat == null) {
                loadClientVat(clientId, "#" + nomeOggetti + classePerIncremento + fieldVat);
            } else {
                $("#" + nomeOggetti + classePerIncremento + fieldVat).val(item.vat);
            }

            if (item.movable == 1) {
                $(fieldCode).attr("required", true);
                $(fieldCode).parents(".col-md-2").find(".fa.fa-asterisk").show();
            }

            if (item.movable == 0) {
                $(fieldCode).removeAttr("required");
                $(fieldCode).parents(".col-md-2").find(".fa.fa-asterisk").hide();
            }

            // Calcolo l'importo
            //var quantity = $(fieldQuantity).val();
            var quantity = 1;
            var prezzo = $(fieldPrice).val();
            var importo = quantity * prezzo;
            $(fieldImporto).val(importo.toFixed(2));

    }

    // Funzione per il settaggio dei readonly
    function setReadonlyValues(typeOfAutocomplete, thisElement, valori, classePerIncremento, sezioneDiProvenienza) {

        var rowDescription = "#" + nomeOggetti + classePerIncremento;

        switch (sezioneDiProvenienza) {
            case 'entratamerce':
                var fieldDescription = rowDescription + "Description";
                var removeMaxQuantity = true;
                break;
            case 'fatture':
                var fieldDescription = rowDescription + "Oggetto";
                var removeMaxQuantity = true;
                break;
        }

        var fieldCode = rowDescription + "Codice";
        var fieldStorageId = rowDescription + "StorageId";
        var fieldBarcode = rowDescription + "Barcode";
        var fieldType = rowDescription + "Tipo";
        var fieldMovable = rowDescription + "Movable";
        var fieldQuantity = rowDescription + "Quantita";
        var fieldImporto = rowDescription + "Importo";

        var txtValue = $(thisElement).val();
        var forceAutocomplete = false;
        var selectedItem = null;

        // Change della descrizione
        if (typeOfAutocomplete == 'descrizione') {
            // I valori erano solo codici
            $(valori).each(
                function () {
                    if (this.descrizione != null) {
                        if (this.descrizione.toLowerCase() == txtValue.toLowerCase()) {
                            forceAutocomplete = true;
                            selectedItem = this;
                        } else {
                        }
                    }
                }
            )

            // Se c'è un forceautocomplete
            if (forceAutocomplete) {
                resetFields(fieldCode, fieldDescription);
                // verrà ricompilato dall'autocomplete
                $(fieldDescription).on("autocompleteselect", autocompleteItem(sezioneDiProvenienza, classePerIncremento, selectedItem, 'descrizione'));
            } else {
                manageFields(fieldType, fieldCode);
            }

            switch (sezioneDiProvenienza) {
                case 'ddt':
                    var removeMaxQuantity = true;
                    break;
                case 'fatture':
                    var removeMaxQuantity = true;
                    break;
                case 'preventivi':
                    var removeMaxQuantity = false;
                    break;
            }

            if (removeMaxQuantity == true) {
                $(fieldStorageId).removeAttr('value');
                $(fieldQuantity).removeAttr('maxquantity');
            }
        }

        // Change del codice
        if (typeOfAutocomplete == 'codice') {
            // I valori erano solo codici
            $(valori).each(
                function () {
                    if (this.codice != null) {
                        if (this.codice.toLowerCase() == txtValue.toLowerCase()) {
                            forceAutocomplete = true;
                            selectedItem = this;
                            return;
                        } else {
                        }
                    }
                }
            )

            // Se c'è un forceautocomplete
            if (forceAutocomplete) {
                // Resetto i campi
                resetFields(fieldCode, fieldDescription);

                // Forzo l'autocomplete
                $(fieldCode).on("autocompleteselect", autocompleteItem(sezioneDiProvenienza, classePerIncremento, selectedItem, 'codice'));
            } else {
                manageFields(fieldType, fieldDescription);
            }

            switch (sezioneDiProvenienza) {
                case 'ddt':
                    var removeMaxQuantity = true;
                    break;
                case 'fatture':
                    var removeMaxQuantity = true;
                    break;
                case 'preventivi':
                    var removeMaxQuantity = false;
                    break;
            }

            if (removeMaxQuantity == true) {
                $(fieldStorageId).removeAttr('value');
                $(fieldQuantity).removeAttr('maxquantity');
            }
        }
    }

    function resetFields(fieldCode, fieldType) {
        // Resetto il campo codice
        $(fieldCode).removeAttr('readonly');
        $(fieldCode).css("background", "#ffffff");
        $(fieldCode).val('');

        // Resetto il field Type
        $(fieldType).val('');
        $(fieldType).removeAttr('readonly');
        $(fieldType).css("background", "#ffffff");
    }


    function manageFields(fieldType, fieldDescriptionOrCode) {
        if ($(fieldType).attr('disabled') !== undefined) {
            $(fieldType).val('');
            $(fieldType).removeAttr('disabled');
            $(fieldType).css("background", "#ffffff");
        }

        // Resetto il campo codice
        if ($(fieldDescriptionOrCode).attr('readonly') !== undefined) {
            $(fieldDescriptionOrCode).val('');
            $(fieldDescriptionOrCode).removeAttr('readonly');
            $(fieldDescriptionOrCode).css("background", "#ffffff");
        }
    }
}
</script>


