<script>
function refreshIcon(classe)
{
	$(classe).click(function(){
	    	addWaitPointer();
	    	var me = $(this);
    		$.ajax({
			method: "POST",
			url: "<?= $this->Html->url(["controller" => "bills","action" => "refreshIXFEStates"]) ?>",
			async : false,
			data:
			{
				billId :  $(me).attr('refreshid'), //billId,
			},
			success: function(data)
			{
				location.reload();
			},
			error: function(data)
			{
			   location.reload();
			}
	    })
    })
    removeWaitPointer();
}
</script>