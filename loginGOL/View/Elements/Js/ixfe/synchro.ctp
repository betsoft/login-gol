<script>
    function syncro(classe, message)
    {
        $(classe).click(function()
        {
            var me = $(this);
            event.preventDefault();

            $.confirm({
                title: 'IXFE',
                content: message,
                type: 'blue',
                buttons:
                    {
                        Ok: function ()
                        {
                            action:
                            {
                                $(me).unbind('click').click();
                                $(me).parent().css("visibility", "hidden");
                            }
                        },
                        annulla: function ()
                        {
                            action:
                            {
                                event.preventDefault();
                            }
                        },
                    }
            })
        });
    }
</script>