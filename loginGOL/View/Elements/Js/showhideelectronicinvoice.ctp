<script>
    function showHideElectronicInvoice()
    {
        if($("#BillElectronicInvoice").prop("checked") == true)
        {
            $(".jsEsigibilityVat").show();

            $("#BillEinvoicevatesigibility").attr("required", "true");
       
            if($("#BillSplitPayment").prop("checked") == true)
                $("#BillEinvoicevatesigibility").val("S");
        }
        else
        {
            $(".jsEsigibilityVat").hide();
            $("#BillEinvoicevatesigibility").removeAttr("required");
        }

        // Se fatttura elettronica selezionato mostra i dati per fattura elettronica */
        $("#BillElectronicInvoice").click(function()
        {
            if($(this).prop("checked") == true)
            {
                $(".jsEsigibilityVat").show();
                $("#BillEinvoicevatesigibility").attr("required", "true");

                if($("#BillSplitPayment").prop("checked") == true)
                    $("#BillEinvoicevatesigibility").val("S");
            }
            else
            {
                $(".jsEsigibilityVat").hide();
                $("#BillEinvoicevatesigibility").removeAttr("required");
            }
        });
    }
</script>