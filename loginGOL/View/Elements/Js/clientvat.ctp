<script>
    function loadClientVat(clientInputId,fieldToFill, successCallback)
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "getClientVat"]) ?>",
            data:
            {
                clientId : clientInputId,
            },
            success : function(data)
            {
                if($.isFunction(successCallback))
                    successCallback(data, fieldToFill);
                else
                    $(fieldToFill).val(data);
            }
        });
    }
</script>