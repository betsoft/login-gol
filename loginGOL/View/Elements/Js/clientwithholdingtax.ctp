<script>
    function loadClientWithholdingTax(clientInputId)
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "loadClientWithholdingTax"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success : function(data)
            {
                if(data > 0)
                    $("#BillWithholdingTax").val(data);
            }
        })
    }
</script>