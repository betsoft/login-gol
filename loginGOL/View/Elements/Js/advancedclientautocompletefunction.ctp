<?= $this->element('Js/advancedclientcatalog'); ?>

<?php if(MULTICURRENCY): ?>
    <?= $this->element('Js/clientcurrency'); ?>
<?php endif; ?>

<script>
    function changeclientData(clientId,sectionId)
    {
        /* Vado a prendere il listino passando la ragione sociale */
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "getClientCatalog"]) ?>",
            data:
            {
                clientId: clientId,
                clientInputId: sectionId,
            },
            success: function (data)
            {
                // Sostituisco l'oggetto del catalogo
                var articoli = JSON.parse(data);
                var codici = JSON.parse(data);

                $(codici).each(function (key, val) {
                    val.label = val.codice;
                })

                switch (sectionId)
                {
                    case '#BillClientId':
                        sezioneDiProvenienza = 'fatture';
                        campoDescrizione = "#Good0Oggetto";
                        loadClientCatalog(0, clientId, startClientId, "Good");
                    break;
                }

                $(campoDescrizione).autocomplete({
                    focus: function (event, ui)
                    {
                        event.preventDefault();
                        return false;
                    },
                    select: function (event, ui)
                    {
                        event.preventDefault();

                        // Fatture
                        if (sezioneDiProvenienza == 'fatture') {
                            $("#Good0Oggetto").val(ui.item.descrizione);
                            $("#Good0Prezzo").val(ui.item.prezzo);
                            $("#Good0StorageId").val(ui.item.value);
                            $("#Good0Codice").val(ui.item.codice);
                            $("#Good0Unita").val(ui.item.unit);
                        }
                    },
                    source: articoli,
                    change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
                    {
                        // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                        if (ui.item == null) {
                            if (sezioneDiProvenienza == 'ddt') {
                                $("#Transportgood0StorageId").removeAttr('value');
                                $("#Transportgood0Quantita").removeAttr('maxquantity');
                            }
                        }
                    }
                });
            },
            error: function (data) { }
        })

        // Carico il metodo di pagamento del cliente
        <?php if(MULTICURRENCY): ?>
            loadClientCurrency("#client_id_multiple_select");
        <?php endif; ?>
    }

    function setClientAutocomplete(clienti,clientInputId)
    {
        /** Modifica listino - quando seleziono il cliente vado a caricare il listino del cliente  **/
        startClientId = clientInputId;

        switch  (clientInputId)
        {
            case "#BillClientId":
                newInput = $("#client_id_multiple_select").change(function(){ changeclientData( $("#client_id_multiple_select").val(), clientInputId) });
            break;
             case "QuoteClientId":
                newInput = $("#client_id_multiple_select").change(function(){ changeclientData( $("#client_id_multiple_select").val(), clientInputId) });
            break;
        }

        /** AUTOCOMPLETE A SELEZIONE DEL CLIENTE **/
        $(clientInputId).autocomplete({
            source: clienti ,
            change: function (event, ui)
            {
                var clientData = $(clientInputId).val();
                var arrayClientData = clientData.split(':');

                if(arrayClientData.length > 1)
                {
                    var clientName = arrayClientData[1].substring(1);
                    $(clientInputId).val(clientName);
                }

                /* Vado a prendere il listino passando la ragione sociale */
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "bills","action" => "getClientCatalog"]) ?>",
                    data:
                    {
                        clientId : $(clientInputId).val(),
                        clientInputId: startClientId,
                    },
                    success: function(data)
                    {

                       // Sostituisco l'oggetto del catalogo
                       var articoli = JSON.parse(data);
                       var codici = JSON.parse(data);
                       $(codici).each(function(key, val)
                        {
                           val.label = val.codice;
                        })
                        switch(startClientId)
                        {
                            case '#QuoteClientId':
                                sezioneDiProvenienza = 'preventivi';
                                campoDescrizione =  "#Good0Description";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                            break;
                            case '#BillClientId':
                                sezioneDiProvenienza = 'fatture';
                                campoDescrizione =  "#Good0Oggetto";
                                loadClientCatalog(0,$(clientInputId).val(),startClientId,"Good");
                            break;
                            case "#TransportClientId":
                                sezioneDiProvenienza = 'ddt';
                                campoDescrizione =  "#Transportgood0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Transportgood");
                                break;
                            case "#ReceiptClientId":
                                sezioneDiProvenienza = 'scontrino';
                                campoDescrizione =  "#Good0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                           break;
                            case 'MaintenanceClientId':
                                sezioneDiProvenienza = 'schedaintervento';
                                campoDescrizione =  "#Maintenance0Description";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                                break;
                        }

                        $(campoDescrizione).autocomplete
                        (
                            {
                                focus: function( event, ui )
                                {
                                    event.preventDefault();
                                    return false;
                                },
                                select: function(event, ui)
                                {
                                    event.preventDefault();

                                    // Fatture

                                    if(sezioneDiProvenienza == 'fatture')
                                    {
                                        $("#Good0Oggetto").val(ui.item.descrizione);
                                        $("#Good0Prezzo").val(ui.item.prezzo);
                                        $("#Good0StorageId").val(ui.item.value);
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Unita").val(ui.item.unit);
                                    }

                                    if(sezioneDiProvenienza == 'scontrino')
                                    {
                                        $("#Good0Oggetto").val(ui.item.descrizione);
                                        $("#Good0StorageId").val(ui.item.value);
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Unita").val(ui.item.unit);
                                    }

                                    // Preventivo
                                    if(sezioneDiProvenienza == 'preventivi')
                                    {
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Description").val(ui.item.descrizione);
                                        $("#Good0UnitOfMeasureId").val(ui.item.unit);
                                        $("#Good0QuoteGoodRowPrice").val(ui.item.prezzo);
                                        $("#Good0StorageId").val(ui.item.value);
                                    }

                                    if(sezioneDiProvenienza == 'ddt')
                                    {
                                        $("#Transportgood0Oggetto").val(ui.item.descrizione);
                                        $("#Transportgood0Prezzo").val(ui.item.prezzo);
                                        $("#Transportgood0StorageId").val(ui.item.value);
                                        $("#Transportgood0Codice").val(ui.item.codice);
                                        $("#Transportgood0Unita").val(ui.item.unit);
                                    }

                                    // Preventivo
                                    if(sezioneDiProvenienza == 'schedaintervento')
                                    {
                                        $("#Maintenancerow0Codice").val(ui.item.codice);
                                        $("#Maintenancerow0Description").val(ui.item.descrizione);
                                        $("#Maintenancerow0UnitOfMeasureId").val(ui.item.unit);
                                        $("#Maintenancerow0StorageId").val(ui.item.value);
                                    }
                                },
                                source: articoli,
                                change: function(event,ui) // Fix per rimuovere i value vuoti quando non autocomplete
                                {
                                    // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                                    if(ui.item == null)
                                    {
                                        if(sezioneDiProvenienza == 'ddt')
                                        {
                                            $("#Transportgood0StorageId").removeAttr('value');
                                            $("#Transportgood0Quantita").removeAttr('maxquantity');
                                        }
                                    }
                                }

                            }
                        );
                    },
                    error: function(data)
                    {
                    }
                })

                    // Carico il metodo di pagamento del cliente
                    <?php if(MULTICURRENCY): ?>
				        loadClientCurrency(clientInputId);
                    <?php endif; ?>

                    // Abilita il clonable
                    enableCloning($(clientInputId).val(),clientInputId);
                    return false;
                },
            });
    }
</script>
