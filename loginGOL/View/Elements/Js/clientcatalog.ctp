<?= $this->element('Js/clientvat'); ?>
<?= $this->element('Js/setmaxstoragequantity'); ?>

<script>
    var magazzinoCliente = null;

    function loadClientCatalog(classePerIncremento, clientId, clientInputId, nomeOggetti)
    {
        if(magazzinoCliente == null)
        {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "bills", "action" => "getClientCatalog"]) ?>",
                async: false,
                data:
                {
                    clientId: clientId,
                    clientInputId: clientInputId,
                    action: '<?= $this->action; ?>',
                },
                success: function (data)
                {
                    magazzinoCliente = data;
                    setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti);
                }
            });
        }
        else
        {
            setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti);
        }

        function setDataToRow(classePerIncremento, clientId, clientInputId, nomeOggetti)
        {
            var articoli = JSON.parse(magazzinoCliente);
            var codici = JSON.parse(magazzinoCliente);

            $(codici).each(function (key, val) {
                val.label = val.codice;
            })

            switch (clientInputId)
            {
                case '#QuoteClientId':
                    sezioneDiProvenienza = 'preventivi';
                    campoDescrizione = "Description";
                    campoCodice = "Codice";
                break;
                case '#BillClientId':
                    sezioneDiProvenienza = 'fatture';
                    campoDescrizione = "Oggetto";
                    campoCodice = "Codice";
                break;
                case '#OrderClientId':
                    sezioneDiProvenienza = 'ordinivendita';
                    campoDescrizione = "Oggetto";
                    campoCodice = "Codice";
                break;
                case '#ReceiptClientId':
                    sezioneDiProvenienza = 'scontrino';
                    campoDescrizione = "Oggetto";
                    campoCodice = "Codice";
                break;
                case '#TransportClientId':
                    sezioneDiProvenienza = 'ddt';
                    campoDescrizione = "Oggetto";
                    campoCodice = "Codice";
                break;
                case '#MaintenanceClientId':
                    sezioneDiProvenienza = 'schedaintervento';
                    campoDescrizione = "Description";
                    campoCodice = "Codice";
                break;
            }

            $("#" + nomeOggetti + classePerIncremento + campoDescrizione).autocomplete({
                focus: function (event, ui)
                {
                    event.preventDefault();
                    return false;
                },
                select: function (event, ui)
                {
                    event.preventDefault();
                    autocompleteItem(sezioneDiProvenienza, clientId, classePerIncremento, ui.item, 'descrizione');
                },
                change: function (event, ui)
                {
                    if (ui.item == null)
                    {
                        setReadonlyValues('descrizione', clientId, this, articoli, classePerIncremento, sezioneDiProvenienza);
                    }
                },
                source: articoli
            });

            $("#" + nomeOggetti + classePerIncremento + campoCodice).autocomplete({
                focus: function (event, ui)
                {
                    event.preventDefault();
                    return false;
                },
                select: function (event, ui)
                {
                    event.preventDefault();
                    autocompleteItem(sezioneDiProvenienza, clientId, classePerIncremento, ui.item, 'codice');
                },
                change: function (event, ui)
                {
                    if (ui.item == null)
                    {
                        setReadonlyValues('codice', clientId, this, codici, classePerIncremento, sezioneDiProvenienza);
                    }
                },
                source: codici
            });
        }

        // Funzione per il settaggi odei readonly
        function setReadonlyValues(typeOfAutocomplete,clientId,thisElement,valori,classePerIncremento,sezioneDiProvenienza)
        {
	        var rowDescription  = "#" + nomeOggetti + classePerIncremento;

            switch(sezioneDiProvenienza)
            {
                case 'ddt':
                    var fieldDescription = rowDescription + "Oggetto";
                    var removeMaxQuantity = true;
                break;
                case 'fatture':
                    var fieldDescription = rowDescription + "Oggetto";
                    var removeMaxQuantity = true;
                break;
                case 'ordinivendita':
                    var fieldDescription = rowDescription + "Oggetto";
                    var removeMaxQuantity = true;
                break;
                case 'scontrino':
                    var fieldDescription = rowDescription + "Oggetto";
                    var removeMaxQuantity = true;
                break;
                case 'preventivi':
                    var fieldDescription = rowDescription + "Description";
                    var fieldQuantity =  rowDescription + "Quantity";
                    var removeMaxQuantity = false;
                break;
                case 'schedaintervento':
                    var fieldDescription = rowDescription + "Description";
                    var fieldQuantity =  rowDescription + "Quantity";
                break;
            }

            var fieldCode = rowDescription + "Codice";
            var fieldStorageId = rowDescription + "StorageId";
            var fieldBarcode =  rowDescription + "Barcode";
            var fieldType =  rowDescription + "Tipo";
            var fieldMovable =  rowDescription + "Movable";
            var fieldQuantity =  rowDescription + "Quantita";
            var fieldImporto =  rowDescription + "Importo";

            var txtValue = $(thisElement).val();
            var forceAutocomplete = false;
            var selectedItem = null;

            // Change della descrizione
            if(typeOfAutocomplete == 'descrizione')
            {
               // I valori erano solo codici
                $(valori).each(
                    function()
                    {
                        if(this.descrizione != null)
                        {
                            if(this.descrizione.toLowerCase() == txtValue.toLowerCase())
                            {
                                forceAutocomplete =  true;
                                selectedItem = this;
                            }
                        }
                    }
                )

                // Se c'è un forceautocomplete
                if(forceAutocomplete)
                {
                    resetFields(fieldCode,fieldDescription);
                    // verrà ricompilato dall'autocomplete
                    $(fieldDescription).on("autocompleteselect", autocompleteItem(sezioneDiProvenienza, clientId, classePerIncremento, selectedItem,'descrizione'));
                }
                else
                {
                    manageFields(fieldType,fieldCode);
                }

                if($(fieldType).val() == '' || $(fieldType).val() === undefined || $(fieldType).val() == null )
                {
                    $(fieldMovable).val('');
                    $(fieldCode).parent('div').find('.fa-asterisk').hide();
                }

                switch(sezioneDiProvenienza)
                {
                    case 'ddt': var removeMaxQuantity = true; break;
                    case 'fatture': var removeMaxQuantity = true; break;
                    case 'ordinivendita': var removeMaxQuantity = true; break;
                    case 'scontrino': var removeMaxQuantity = true; break;
                    case 'preventivi': var removeMaxQuantity = false; break;
                    case 'schedaintervento': var removeMaxQuantity = true; break;
                }

                if(removeMaxQuantity == true)
                {
                    $(fieldStorageId).removeAttr('value');
                    $(fieldQuantity).removeAttr('maxquantity');
                }
            }

            // Change del codice
            if(typeOfAutocomplete == 'codice')
            {
                // I valori erano solo codici
                $(valori).each(function() {
                    if(this.codice != null)
                    {
                        if(this.codice.toLowerCase() == txtValue.toLowerCase())
                        {
                            forceAutocomplete =  true;
                            selectedItem = this;
                            return;
                        }
                    }
                })

                // Se c'è un forceautocomplete
                if(forceAutocomplete)
                {
                    // Resetto i campi
                    resetFields(fieldCode,fieldDescription);
                    // Forzo l'autocomplete
                    $(fieldCode).on( "autocompleteselect", autocompleteItem(sezioneDiProvenienza, clientId, classePerIncremento, selectedItem,'codice'));
                }
                else
                {
                     manageFields(fieldType,fieldDescription);
                }

                if($(fieldType).val() == '' || $(fieldType).val() === undefined || $(fieldType).val() == null )
                {
                    $(fieldMovable).val('');
                    $(fieldCode).parent('div').find('.fa-asterisk').hide();
                }

                switch(sezioneDiProvenienza)
                {
                    case 'ddt': var removeMaxQuantity = true; break;
                    case 'fatture': var removeMaxQuantity = true; break;
                    case 'ordinivendita': var removeMaxQuantity = true; break;
                    case 'scontrino': var removeMaxQuantity = true; break;
                    case 'preventivi': var removeMaxQuantity = false; break;
                    case 'schedaintervento': var removeMaxQuantity = true; break;
                }

                if(removeMaxQuantity == true)
                {
                    $(fieldStorageId).removeAttr('value');
                    $(fieldQuantity).removeAttr('maxquantity');
                }
            }
        }

        function resetFields(fieldCode,fieldType)
        {
            // Resetto il campo codice
            $(fieldCode).removeAttr('readonly');
            $(fieldCode).css("background", "#ffffff");
            $(fieldCode).val('');

            // Resetto il field Type
            $(fieldType).val('');
            $(fieldType).removeAttr('readonly');
            $(fieldType).css("background", "#ffffff");
        }

        function manageFields(fieldType,fieldDescriptionOrCode)
        {
            if($(fieldType).attr('disabled') !== undefined)
            {
                $(fieldType).val('');
                $(fieldType).removeAttr('disabled');
                $(fieldType).css("background", "#ffffff");
            }

            // Resetto il campo codice
            if($(fieldDescriptionOrCode).attr('readonly') !== undefined)
            {
                $(fieldDescriptionOrCode).val('');
                $(fieldDescriptionOrCode).removeAttr('readonly');
                $(fieldDescriptionOrCode).css("background", "#ffffff");
            }
        }

        /** Codice che viene esegueti all'autocomplete **/
        function autocompleteItem(sezioneDiProvenienza, clientId, classePerIncremento, itemRaw, typeOfAutocomplete)
        {
            // Ripristino eventuale errore
            var item = [];
            item['label'] = itemRaw.label;
            item['descrizione'] = itemRaw.descrizione;
            item['value'] = itemRaw.value;
            item['codice'] = itemRaw.codice;

            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "storages","action" => "getStorageElements"]) ?>",
                data:
                {
                    storage_id : item.value,
                    client_id: clientId
                },
                success: function(data)
                {
                    var storage = JSON.parse(data);
                    item['prezzo'] = storage.Storage.prezzo;
                    item['vat'] = storage.Storage.vat_id;
                    item['unit'] = storage.Storage.unit_id;
                    item['movable'] = storage.Storage.movable;
                }
            }).then(
                function (res)
                {
                    $('.fastErroreMessage').remove();
                    $('.jsQuantity').css('border-color','#e5e5e5');

                    var rowDescription  = "#" + nomeOggetti + classePerIncremento;

                    switch(sezioneDiProvenienza)
                    {
                        case 'fatture':
                            var fieldDescription =  rowDescription + "Oggetto";
                            var fieldPrice = rowDescription + "Prezzo";
                            var fieldUnitOfMeasure =rowDescription + "Unita";
                            var fieldVat = rowDescription + 'IvaId';
                            var fieldQuantity =  rowDescription + "Quantita";
                        break;
                        case 'ordinivendita':
                            var fieldDescription =  rowDescription + "Oggetto";
                            var fieldPrice = rowDescription + "Prezzo";
                            var fieldUnitOfMeasure =rowDescription + "Unita";
                            var fieldVat = rowDescription + 'IvaId';
                            var fieldQuantity =  rowDescription + "Quantita";
                        break;
                        case 'scontrino':
                            var fieldDescription =  rowDescription + "Oggetto";
                            var fieldPrice = rowDescription + "Prezzo";
                            var fieldUnitOfMeasure =rowDescription + "Unita";
                            var fieldVat = rowDescription + 'IvaId';
                            var fieldQuantity =  rowDescription + "Quantita";
                        break;
                        case 'preventivi':
                            var fieldDescription = rowDescription + "Description";
                            var fieldPrice = rowDescription + "QuoteGoodRowPrice";
                            var fieldUnitOfMeasure = rowDescription +"UnitOfMeasureId";
                            var fieldVat = rowDescription + 'QuoteGoodRowVatId';
                            var fieldQuantity =  rowDescription + "Quantity";
                        break;
                        case 'ddt':
                            var fieldDescription = rowDescription + "Oggetto";
                            var fieldPrice = rowDescription + "Prezzo";
                            var fieldUnitOfMeasure = rowDescription + "Unita";
                            var fieldVat = rowDescription +'IvaId';
                            var fieldQuantity =  rowDescription + "Quantita";
                        break;
                        case 'schedaintervento':
                            var fieldDescription = rowDescription + "Description";
                            var fieldUnitOfMeasure = rowDescription + "UnitOfMeasureId";
                            var fieldQuantity =  rowDescription + "Quantity";
                        break;
                    }

                    var fieldCode = rowDescription + "Codice";
                    var fieldStorageId = rowDescription + "StorageId";
                    var fieldBarcode =  rowDescription + "Barcode";
                    var fieldType =  rowDescription + "Tipo";
                    var fieldMovable =  rowDescription + "Movable";
                    var fieldImporto =  rowDescription + "Importo";

                    if(typeOfAutocomplete == 'descrizione')
                    {
                        $(fieldDescription).attr("readonly", false);
                        $(fieldDescription).css("background", "#ffffff");
                        $(fieldDescription).val(item.descrizione);
                    }

                    if(typeOfAutocomplete == 'codice')
                    {
                        $(fieldCode).attr("readonly", false);
                        $(fieldCode).css("background", "#ffffff");
                        $(fieldDescription).val(item.descrizione);
                    }

                    $(fieldPrice).val(item.prezzo);

                    <?php if(MULTICURRENCY): ?>
                        $(rowDescription+"Currencyprice").val((item.prezzo * $("#BillChangevalue").val()).toFixed(2));
                    <?php endif; ?>

                    $(fieldVat).val(item.vat);
                    $(fieldStorageId).val(item.value);
                    $(fieldCode).val(item.codice);
                    $(fieldUnitOfMeasure).val(item.unit);
                    $(fieldBarcode).val(item.barcode);

                    // A Seconda della tipologia modifico il tipo e metto disabled
                    $(fieldType).val(item.movable);
                    $(fieldType).attr("disabled", true);
                    $(fieldMovable).val(item.movable);

                    loadClientVat(clientId,null, function(data,fieldToFill) {
                        if(data == "" || data == 0)
                        {
                        }
                        else
                        {
                            loadClientVat(clientId,fieldVat);
                        }
                    });

                    if(item.movable == 1)
                    {
                        $(fieldCode).attr("required", true);
                        $(fieldCode).parent(".col-md-2").find(".fa.fa-asterisk").show();
                    }

                    if(item.movable == 0)
                    {
                        $(fieldCode).removeAttr("required");
                        $(fieldCode).parents(".col-md-2").find(".fa.fa-asterisk").hide();
                    }

                    // Metto il maxquantity a fatture
                    if(sezioneDiProvenienza == 'fatture')
                    {
                        <?php if(isset($bills['Bill']['accompagnatoria']) && $bills['Bill']['accompagnatoria'] == 1): ?>
                            setMaxStorageQuantity($("#BillDepositId").val(),item.value,fieldQuantity,0);
                        <?php else: ?>
                            if($("#BillAccompagnatoria").prop("checked") == true)
                                setMaxStorageQuantity($("#BillDepositId").val(),item.value,fieldQuantity,0);
                        <?php endif; ?>
                    }

                    // metto il maxquantity a bolle
                    if(sezioneDiProvenienza == 'ddt' )
                    {
                        // Recupero massimo dell'articolo a deposito
                        $.ajax({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
                            data:
                            {
                                deposit : $("#TransportDepositId").val(),
                                storageId : item.value,
                            },
                            success: function(data)
                            {
                                // Aggiorno il valore max quantity
                                $(fieldQuantity).attr('maxquantity',data);
                            }
                        })
                    }

                    // metto il maxquantity gli scontrini
                    if(sezioneDiProvenienza == 'scontrino' )
                    {
                        // Recupero massimo dell'articolo a deposito
                        $.ajax({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
                            data:
                            {
                                deposit : $("#BillDepositId").val(),
                                storageId : item.value,
                            },
                            success: function(data)
                            {
                                // Aggiorno il valore max quantity
                                $(fieldQuantity).attr('maxquantity',data);
                            }
                        })
                    }

                    // Calcolo l'importo
                    var quantity =  $(fieldQuantity).val();
                    var prezzo  =  $(fieldPrice).val();
                    var importo = quantity * prezzo;
                    $(fieldImporto).val(importo.toFixed(2));
                }
            );
        }
    }
</script>
