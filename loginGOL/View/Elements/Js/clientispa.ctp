<script>
    /* Recupero i dati del cliente */
    function loadclientispa(clientInputId)
    {
        var clientName = '';

        try
        {
            if($(clientInputId).val() != undefined)
                clientName = $(clientInputId).val();  // Recupero il valore della casella input
            else
                clientName = clientInputId; // Recupero il valore dalla ragione sociale passata
        }
        catch (err)
        {
            clientName = clientInputId; // Recupero il valore dalla ragione sociale passata
        }

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "clients","action" => "isClientPa"]) ?>",
            data:
            {
                clientId : clientName, // $(clientInputId).val(),
            },
            success : function(data)
            {
                if(data != 0)
                {
                    if($("#BillCig").is(':visible'))
                    {
                    //	$("#BillCigDocumentid").attr("required",true);
                    //	$("#BillCigDocumentid").parents(".col-md-2").find(".fa.fa-asterisk").show();
                    //	$("#BillCig").attr("required",true);
                    //	$("#BillCig").parents(".col-md-2").find(".fa.fa-asterisk").show();
                    //	$("#BillCup").attr("required",true);
                    //	$("#BillCup").parents(".col-md-2").find(".fa.fa-asterisk").show();
                    }
                }
                else
                {
                    // $("#BillCigDocumentid").removeAttr("required",false);
                    // $("#BillCigDocumentid").parents(".col-md-2").find(".fa.fa-asterisk").hide();
                    // $("#BillCig").removeAttr("required");
                    // $("#BillCig").parents(".col-md-2").find(".fa.fa-asterisk").hide();
                    // $("#BillCup").removeAttr("required");
                    // $("#BillCup").parents(".col-md-2").find(".fa.fa-asterisk").hide();
                }
            },
            error : function(data)
            {
            }
        });
    }
</script>