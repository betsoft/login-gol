<script>
    function checkMaxStorageQuantityError(cellId,errore)
    {
        $(cellId).each(
            function()
            {
                if($(this).val() > parseFloat($(this).attr("maxquantity")))
                {
                    $(this).css('border-color',"red"); 
                    $(this).parent().find(".fastErroreMessage").remove();
                    var maxAttr = $(this).attr("maxquantity");
                    if (maxAttr < 0){ maxAttr = 0; }
                    $(this).parent().append('<span class="fastErroreMessage" style="color:red">Quantità disponibile : '+ maxAttr +'</span>');
                    errore = 11;
                }
                else
                {
                }
            }
        )

        return errore;
    }
</script>