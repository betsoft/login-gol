<script>

    function enableCloning(clientName,clientInputId)
    {
        $("#aggiungi_riga").unbind('click');
        $("#aggiungi_nota").unbind('click');
        $("#aggiungi_subtotale").unbind('click');
        $("#aggiungi_opt").unbind('click');

        switch (clientInputId)
        {
            case '#QuoteClientId':
                sezioneDiProvenienza = 'preventivi';
                campoDescrizione = "Description";
                nomeOggetti = "Good";
            break;
            case '#BillClientId':
                sezioneDiProvenienza = 'fatture';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Good";
            break;
            case '#OrderClientId':
                sezioneDiProvenienza = 'ordinivendita';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Orderrow";
            break;
            case '#ReceiptClientId':
                sezioneDiProvenienza = 'scontrino';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Good";
            break;
            case '#TransportClientId':
                sezioneDiProvenienza = 'ddt';
                campoDescrizione = "Oggetto";
                nomeOggetti = "Transportgood";
            break;
            case "#LoadgoodSupplierId":
                sezioneDiProvenienza = 'entratamerce';
                campoDescrizione = "Description";
                nomeOggetti = "Good";
            break;
            // SOLO PER VERSIONE RAPPORTINI
            case "#MaintenanceClientId":
                //case "#client_id_multiple_select":
                sezioneDiProvenienza = 'schedaintervento';
                campoDescrizione = "Description";
                nomeOggetti = "Good";
            break;
            case "#PurchaseorderSupplierId":
                sezioneDiProvenienza = 'ordineacquisto';
                campoDescrizione = "Description";
                nomeOggetti = "Good";
            break;
        }

        $("#" + nomeOggetti + "0" + campoDescrizione).prop('required', true);

        $(".originale .remove").hide();

        var clonato;
        var nuova_chiave = 0;

        $("#aggiungi_riga").click(
            function ()
            {
                MakeTheClone(this, 'riga', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        $("#aggiungi_nota").click(
            function ()
            {
                MakeTheClone(this, 'nota', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        $("#aggiungi_subtotale").click(
            function ()
            {
                MakeTheClone(this, 'subtotale', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        $("#aggiungi_opt").click(
            function ()
            {
                MakeTheClone(this, 'optrow', clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti);
            }
        );

        function MakeTheClone(objectToClone, typeofcloning, clientName, clientInputId, sezioneDiProvenienza, campoDescrizione, nomeOggetti)
        {
            nuova_chiave = nuova_chiave + 1;

            clonableRowDescription = ".clonato #" + nomeOggetti + nuova_chiave;

            var clonableFieldType = clonableRowDescription + "Tipo";
            var clonableFieldCode = clonableRowDescription + "Codice";
            var clonableFieldDescription = clonableRowDescription + "Oggetto";

            if($('.originale').length == 0)
            {
                $(".contacts_row.clonableRow").each(
                    function(index)
                    {
                        if(index == 0)
                        {
                            $(this).addClass('originale');
                        }
                    }
                );
            }

            clonato = $('.originale').clone();
            $(clonato).removeClass("originale");
            $(clonato).addClass("clonato");
            $(".ultima_riga").removeClass("ultima_riga");

            $(clonato).insertAfter($(".clonableRow").last());

            if (sezioneDiProvenienza == 'fatture' || sezioneDiProvenienza == 'scontrino')
            {
                <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){ ?>
                   var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied', 'ComplimentaryQuantity'];
                   var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'tipo', 'importo', 'currencyprice', 'withholdingapplied', 'complimentaryQuantity'];

                   if (typeofcloning == 'nota')
                   {
                       var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', '', '', '1', '0'];
                   }
                   else
                   {
                       var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', '', '', '1', '0'];
                   }
                <?php  } else { ?>
                    var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied'];
                    var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'tipo', 'importo', 'currencyprice', 'withholdingapplied'];

                    if (typeofcloning == 'nota')
                    {
                        var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', '', '', '1'];
                    }
                    else
                    {
                        var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', '', '', '1'];
                    }
                <?php } ?>
                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                // Rimuove segni avvisi rossi per ddt
                $('.fastErroreMessage').remove();
                $('.jsQuantity').css('border-color', '#e5e5e5');
                $(".clonato .rimuoviRigaIcon").show();

                if (typeofcloning == 'nota')
                {
                    $(".clonato").find(".jsRowField").hide();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").show();
                    $(".jsDescriptionHelper").unbind();

                    $(".jsDescriptionHelper").click(
                        function ()
                        {
                            showHelpMessage('note');
                        }
                    );

                    // Rimuovo i required in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");
                    $(clonableFieldDescription).autocomplete({disabled: true});
                }
                else
                {
                    // $(".clonato").find(".jsRowField").show();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").hide();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }
            if(sezioneDiProvenienza == 'ordinivendita'){

                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'Tipo', 'Importo', 'Currencyprice', 'Withholdingapplied', 'ComplimentaryQuantity'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'tipo', 'importo', 'currencyprice', 'withholdingapplied', 'complimentaryQuantity'];

                if (typeofcloning == 'nota')
                {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', 0];
                }
                else
                {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', '', '', '1', '', 0];
                }

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                // Rimuove segni avvisi rossi per ddt
                $('.fastErroreMessage').remove();
                $('.jsQuantity').css('border-color', '#e5e5e5');
                $(".clonato .rimuoviRigaIcon").show();

                if (typeofcloning == 'nota')
                {
                    $(".clonato").find(".jsRowField").hide();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").show();
                    $(".jsDescriptionHelper").unbind();

                    $(".jsDescriptionHelper").click(
                        function ()
                        {
                            showHelpMessage('note');
                        }
                    );

                    // Rimuovo i required in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");
                    $(clonableFieldDescription).autocomplete({disabled: true});
                }
                else
                {
                    // $(".clonato").find(".jsRowField").show();
                    // Mostro l'heleper
                    $(".clonato").find(".jsDescriptionHelper").hide();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }

            if (sezioneDiProvenienza == 'ddt')
            {
                var Campi = ['Codice', 'Oggetto', 'Quantita', 'Prezzo', 'Discount', 'IvaId', 'Unita', 'Movable', 'StorageId', 'Customdescription', 'Tipo', 'Importo'];
                var Label = ['codice', 'oggetto', 'quantita', 'prezzo', 'discount', 'iva_id', 'unita', 'movable', 'storage_id', 'customdescription', 'tipo', 'importo'];

                if (typeofcloning == 'nota')
                {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', ''];
                }
                else
                {
                    var newLineValue = ['', '', 1, '', '', '', '', '', '', '', '', ''];
                }

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                // Rimuove segni avvisi rossi per ddt
                $('.fastErroreMessage').remove();
                $('.jsQuantity').css('border-color', '#e5e5e5');
                $(".clonato .rimuoviRigaIcon").show();

                if (typeofcloning == 'nota')
                {
                    $(".clonato").find(".jsRowField").hide();
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");
                    $(clonableFieldDescription).autocomplete({disabled: true});
                }
                else
                {
                    $(".clonato").find(".jsRowField").show();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }

            // Sezione entratamerce
            if (sezioneDiProvenienza == 'entratamerce')
            {
                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Quantity', 'LoadGoodRowPrice', 'LoadGoodRowVatId', 'Barcode', 'Movable', 'StorageId', 'VariationId', 'Tipo', 'Importo', 'Suggestedsellprice'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'quantity', 'load_good_row_price', 'load_good_row_vat_id', 'barcode', 'movable', 'storage_id', 'variation_id', 'tipo', 'importo', 'suggestedsellprice'];
                var newLineValue = ['', '', '', 1, '', '', '', 1, '', '', 1, '', ''];

                var clonableFieldDescription = clonableRowDescription + "Description";

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");

                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");
                $(".clonato .rimuoviRigaIcon").show();
            }

            if (sezioneDiProvenienza == 'preventivi')
            {
                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Customdescription', 'Quantity', 'QuoteGoodRowPrice', 'QuoteGoodRowVatId', 'Movable', 'StorageId', 'VariationId', 'Tipo', 'Importo'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'customdescription', 'quantity', 'quote_good_row_price', 'quote_good_row_vat_id', 'movable', 'storage_id', 'variation_id', 'tipo', 'importo'];

                if (typeofcloning == 'nota' || typeofcloning == 'subtotale' || typeofcloning == 'optrow')
                {
                    var newLineValue = ['', '', '', '', '', '', '', '', '', '', '', ''];
                }
                else
                {
                    var newLineValue = ['', '', '', '', 1, '', '', '', '', '', '', ''];
                }

                var clonableFieldDescription = clonableRowDescription + "Description";

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                $(".clonato .rimuoviRigaIcon").show();

                if (typeofcloning == 'nota' || typeofcloning == 'subtotale' || typeofcloning == 'optrow')
                {
                    $(".clonato").find(".jsRowField").hide();
                    // Rimuovo i reuqired in quanto non servono
                    $(".clonato").find(".jsRowField").find('input').removeAttr('required');
                    $(".clonato").find(".jsRowField").find('select').removeAttr('required');
                    // Nascondo le colonne
                    $(".clonato").find(".jsMulticurrency").hide(); // Per le valute lo tolgo sempre
                    $(".clonato").find(".jsRowFieldDescription").removeClass("col-md-3");
                    $(".clonato").find(".jsRowFieldDescription").addClass(" col-md-12");
                    // Assegno il valore dei movable per subtotale e optrow
                    switch(typeofcloning)
                    {
                        case 'subtotale':
                            $(".clonato").find(".jsMovable").val('-10');
                            $(".clonato .jsStrongDesc").html('SUBTOTALE');
                            $(".clonato .jsStrongDesc").css('color','#ea5d0b');
                            $(".clonato .fa-asterisk").remove();
                        break;
                        case 'optrow' :
                            $(".clonato").find(".jsMovable").val('-20');
                            $(".clonato .jsStrongDesc").html('SEZIONE OPZIONALE');
                            $(".clonato .jsStrongDesc").css('color','#589AB8');
                            $(".clonato .fa-asterisk").remove();
                        break;
                        case 'nota' :
                            // $(".clonato").find(".jsMovable").val('-30');
                        break;
                    }

                    $(clonableFieldDescription).autocomplete({disabled: true});
                }
                else
                {
                    $(".clonato").find(".jsRowField").show();
                    $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                    $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                    $(clonableFieldDescription).autocomplete({disabled: false});
                }
            }

            if (sezioneDiProvenienza == 'schedaintervento')
            {
                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Customdescription','Quantity', 'Price', 'VatId', 'Movable', 'StorageId', 'VariationId', 'Tipo', 'Importo'];
                var Label = ['codice', 'description', 'unit_of_measure_id','customdescription', 'quantity', 'price', 'vat_id', 'movable', 'storage_id', 'variation_id', 'tipo', 'importo'];
                var newLineValue = ['', '', '', '',  1,'', '', '', '', '', '',''];
                var clonableFieldDescription = clonableRowDescription + "Description";

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                $(".clonato .rimuoviRigaIcon").show();

                $(".clonato").find(".jsRowField").show();
                $(".clonato").find(".jsRowFieldDescription").removeClass(" col-md-12");
                $(".clonato").find(".jsRowFieldDescription").addClass("col-md-3");
                $(clonableFieldDescription).autocomplete({disabled: false});
            }

            // Sezione entratamerce
            if (sezioneDiProvenienza == 'ordineacquisto')
            {
                var Campi = ['Codice', 'Description', 'UnitOfMeasureId', 'Quantity', 'Price', 'VatId', 'Movable', 'StorageId', 'VariationId', 'Tipo', 'Importo'];
                var Label = ['codice', 'description', 'unit_of_measure_id', 'quantity', 'price', 'vat_id', 'movable', 'storage_id', 'variation_id', 'tipo', 'importo'];
                var newLineValue = ['', '', '', 1, '', '', 1, '', '', '', ''];

                var clonableFieldDescription = clonableRowDescription + "Description";

                cloneAllField(Campi, Label, newLineValue);

                $(clonableFieldType).removeAttr("disabled");
                $(clonableFieldCode).removeAttr("readonly");
                $(clonableFieldCode).css("background", "#ffffff");
                $(clonableFieldCode).removeAttr("required");
                $(clonableFieldDescription).removeAttr("readonly");
                $(clonableFieldDescription).css("background", "#ffffff");

                $(".clonato .rimuoviRigaIcon").show();
            }

            if(sezioneDiProvenienza == 'ddt' || sezioneDiProvenienza == 'fatture' || sezioneDiProvenienza == 'ordinivendita')
            {
                var discountField = $('.originale').find($("[name*='discount']"));
                var discountValue = discountField.val();
                $(clonato).find($("[name*='discount']")).val(discountValue);
            }

            $(clonato).addClass("ultima_riga");
            $(clonato).removeClass("clonato");

            // Carico i listini
            if (clientName != undefined) // Il catalogo è solo per clienti
            {
                loadClientCatalog(nuova_chiave, clientName, clientInputId, nomeOggetti);
            }
            else
            {
                var articoli = setArticles();
                var codici = setCodes();
                loadSupplierCatalog(articoli, nomeOggetti, nuova_chiave, sezioneDiProvenienza, codici);
            }

            // Aggiungo il rimuovi icona
            addcrossremoving();

            // Aggiungo il calcolo degli importi
            $(".jsPrice").change(function () {
                setImporto(this);
                <?php if(MULTICURRENCY){ ?>setCurrencyChange(this, 'toCurrency');  <?php } ?>
            });

            $(".jsChange").change(function () {
                <?php if(MULTICURRENCY){ ?>setCurrencyChange(this, 'toEuro');  <?php } ?>
                setImporto(this);
            });

            $(".jsQuantity").change(function () {
                setImporto(this);
            });

            $(".jsTipo").change(function () {
                setCodeRequired(this);
            });
        }

        $('.contacts_row .clonato').removeClass("clonato");

        function cloneAllField(Campi, Label, newLineValue)
        {
            for (i = 0; i < Campi.length; i++)
            {
                $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('name', "data[" + nomeOggetti + "][" + nuova_chiave + "][" + Label[i] + "]");
                $(".clonato #" + nomeOggetti + "0" + Campi[i]).attr('id', nomeOggetti + nuova_chiave + Campi[i]);
                $(clonableRowDescription + Campi[i]).val(newLineValue[i]);
            }
        }
    }
</script>
