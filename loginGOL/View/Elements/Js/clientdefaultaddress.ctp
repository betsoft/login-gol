<script>
    function loadClientDefaultAddress(clientInputId,sezioneDiProvenienza)
    {
        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills","action" => "getClientAddress"]) ?>",
            data:
            {
                clientId : $(clientInputId).val(),
            },
            success: function(data)
            {
                var exit = false;
                data = JSON.parse(data);

                $.each(data, function(key, element) {
                    switch(sezioneDiProvenienza)
                    {
                        case 'fatture':
                            address = '#BillClientAddress';
                            cap = "#BillClientCap";
                            city = "#BillClientCity";
                            province = "#BillClientProvince";
                        break;
                        case 'ordinivendita':
                            address = '#OrderClientAddress';
                            cap = "#OrderClientCap";
                            city = "#OrderClientCity";
                            province = "#OrderClientProvince";
                        break;
                        case  'ddt':
                            address = '#TransportClientAddress';
                            cap = "#TransportClientCap";
                            city = "#TransportClientCity";
                            province = "#TransportClientProvince";
                        break;
                        case 'preventivi':
                            address = '#QuoteClientAddress';
                            cap = "#QuoteClientCap";
                            city = "#QuoteClientCity";
                            province = "#QuoteClientProvince";
                        break;
                        default:
                            exit = true;
                        break;
                    }

                     // Se non esistono gli oggetti dove andare ad inserire i valori
                     try
                     {
                        if(exit == false)
                        {
                            $(address).val(element.address);
                            $(cap).val(element.cap);
                            $(city).val(element.city);
                            $(province).val(element.province);
                            $("#client_nation_multiple_select").val(element.nation);
                            $("#client_nation_multiple_select").multiselect('rebuild');
                        }
                     }
                    catch (err) // segnalo solo l'errore
                    {
                    }
                })
            }
        });
    }
</script>
