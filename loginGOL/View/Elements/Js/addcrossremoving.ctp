<script>
    function addcrossremoving()
    {
        $(".rimuoviRigaIcon").unbind('click');
        $(".rimuoviRigaIcon").click(function()
        {
            if($(".rimuoviRigaIcon").length > 1)
            {
                if($(this).parent($("[class*='principale']")).hasClass('ultima_riga'))
                {
                    $(this).parent($("[class*='principale']")).remove();
                    $("[class*='principale']").last().addClass('ultima_riga');
                }
                else
                {
                    $(this).parent($("[class*='principale']")).remove();
                }
                addcrossremoving();
            }
            else
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    content: 'Attenzione, deve essere sempre presente almeno una riga.',
                    type: 'orange',
                });
            }
        });
    }
    /*
    function addcrossremoving()
    {
        $(".rimuoviRigaIcon").unbind('click');
        $(".rimuoviRigaIcon").click(function()
        {
            if($(".rimuoviRigaIcon").length > 1)
            {
                if($(this).parent('.principale').hasClass('ultima_riga'))
                {
                    $(this).parent('.principale').remove();
                    $('.principale').last().addClass('ultima_riga');
                }
                else
                {
                    $(this).parent('.principale').remove();
                }
                addcrossremoving();
            }
            else
            {
                $.alert
    	        ({
    				icon: 'fa fa-warning',
	    			title: '',
    				content: 'Attenzione, deve essere sempre presente almeno una riga.',
        			type: 'orange',
				});
            }
        });
    }*/
</script>
