<?= $this->element('Js/clientpaymentmethod'); ?>
<?= $this->element('Js/clientsplitpayment'); ?>
<?= $this->element('Js/clientvat'); ?>
<?= $this->element('Js/clientdiscount'); ?>
<?= $this->element('Js/clientispa'); ?>

<?php if(MULTICURRENCY){ echo $this->element('Js/clientcurrency'); }?>


<?= $this->element('Js/clientwithholdingtax'); ?>
<?= $this->element('Js/clientdefaultaddress'); ?>
<?= $this->element('Js/clientcatalog'); ?>
<?= $this->element('Js/clientdifferentaddress'); ?>
<?= $this->element('Js/clientdifferentrefferedaddress'); ?>

<script>
    /** NEW FIX **/
    function changeclientData(clientId,sectionId) {

        /* Vado a prendere il listino passando la ragione sociale */
        $.ajax
        ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "bills", "action" => "getClientCatalog"]) ?>",
            data:
                {
                    clientId: clientId,
                    clientInputId: sectionId,
                },
            success: function (data) {

                // Sostituisco l'oggetto del catalogo
                var articoli = JSON.parse(data);
                var codici = JSON.parse(data);
                $(codici).each(function (key, val) {
                    val.label = val.codice;
                })
                switch (sectionId) {
                    case '#BillClientId':
                        sezioneDiProvenienza = 'fatture';
                        campoDescrizione = "#Good0Oggetto";
                        loadClientCatalog(0, $(startClientId).val(), startClientId, "Good");
                        break;
                }

                $(campoDescrizione).autocomplete
                (
                    {
                        focus: function (event, ui) {
                            event.preventDefault();
                            return false;
                        },
                        select: function (event, ui) {
                            event.preventDefault();

                            // Fatture
                            if (sezioneDiProvenienza == 'fatture') {
                                $("#Good0Oggetto").val(ui.item.descrizione);
                                $("#Good0Prezzo").val(ui.item.prezzo);
                                $("#Good0StorageId").val(ui.item.value);
                                $("#Good0Codice").val(ui.item.codice);
                                $("#Good0Unita").val(ui.item.unit);
                                // Carico l'iva sugli articoli
                                //?loadClientVat($(clientInputId).val(),"#Good0IvaId");
                            }
                        },
                        source: articoli,
                        change: function (event, ui) // Fix per rimuovere i value vuoti quando non autocomplete
                        {
                            // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                            if (ui.item == null) {
                                if (sezioneDiProvenienza == 'ddt') {
                                    $("#Transportgood0StorageId").removeAttr('value');
                                    $("#Transportgood0Quantita").removeAttr('maxquantity');
                                }
                            }
                        }
                    }
                );
            },
            error: function (data) {
            }
        })
        if(sezioneDiProvenienza != 'scontrino') {
            if (sezioneDiProvenienza == 'preventivi') {
                loadClientPaymentMethod("#client_id_multiple_select", "#QuotePayment",true);
            } else {
                loadClientPaymentMethod("#client_id_multiple_select", "#BillPaymentId",false);
            }
        }

        // Carico il metodo di pagamento del cliente
        loadclientispa("#client_id_multiple_select");

        // Carico il metodo di pagamento del cliente
        <?php
        if(MULTICURRENCY)
        {
        ?>
        loadClientCurrency("#client_id_multiple_select");
        <?php
        }
        ?>

        // Carico la ritenuta d'acconto
        loadClientWithholdingTax("#client_id_multiple_select");

        // Carico il metodo di pagamento del cliente
        loadClientSplitPayment("#client_id_multiple_select");

        // Carico indirizzo del cliente
        loadClientDefaultAddress("#client_id_multiple_select",sezioneDiProvenienza);

        loadClientDiscount("#client_id_multiple_select",sezioneDiProvenienza);


        // Carico differente indirizzo di spedizione
        // loadClientDifferentAddress(sectionId,sezioneDiProvenienza);
        loadClientDifferentAddress("#client_id_multiple_select",sezioneDiProvenienza);

        // Carico indirizzo alternativo clienti rivenditori
        // loadClientDifferentReferredAddress(clientInputId,sezioneDiProvenienza);

        // Abilita il clonable
        // enableCloning($(clientInputId).val(),clientInputId);
    }

    function setClientAutocomplete(clienti,clientInputId)
    {
        /** Modifica listino - quando seleziono il cliente vado a caricare il listino del cliente  **/
        startClientId = clientInputId;

        switch  (clientInputId)
        {
            case "#BillClientId":
                newInput = $("#client_id_multiple_select").change(function(){ changeclientData( $("#client_id_multiple_select").val(), clientInputId) });
                break;
            case "QuoteClientId":
                newInput = $("#client_id_multiple_select").change(function(){ changeclientData( $("#client_id_multiple_select").val(), clientInputId) });
                break;
        }


        /** AUTOCOMPLETE A SELEZIONE DEL CLIENTE **/
        $(clientInputId).autocomplete({
            source: clienti ,
            change: function (event, ui)
            {
                var clientData = $(clientInputId).val();
                var arrayClientData = clientData.split(':');

                if(arrayClientData.length > 1)
                {
                    var clientName = arrayClientData[1].substring(1);
                    $(clientInputId).val(clientName);
                }

                /* Vado a prendere il listino passando la ragione sociale */
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "bills","action" => "getClientCatalog"]) ?>",
                    data:
                        {
                            clientId : $(clientInputId).val(),
                            clientInputId:startClientId,
                        },
                    success: function(data)
                    {

                        // Sostituisco l'oggetto del catalogo
                        var articoli = JSON.parse(data);
                        var codici = JSON.parse(data);
                        $(codici).each(function(key, val)
                        {
                            val.label = val.codice;
                        })
                        switch(startClientId)
                        {
                            case '#QuoteClientId':
                                sezioneDiProvenienza = 'preventivi';
                                campoDescrizione =  "#Good0Description";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                                break;
                            case '#BillClientId':
                                sezioneDiProvenienza = 'fatture';
                                campoDescrizione =  "#Good0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                                break;
                            case '#OrderClientId':
                                sezioneDiProvenienza = 'ordini';
                                campoDescrizione =  "#Orderrow0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Orderrow");
                                break;
                            case "#TransportClientId":
                                sezioneDiProvenienza = 'ddt';
                                campoDescrizione =  "#Transportgood0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Transportgood");
                                break;
                            case "#ReceiptClientId":
                                sezioneDiProvenienza = 'scontrino';
                                campoDescrizione =  "#Good0Oggetto";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                                break;
                            case 'MaintenanceClientId':
                                sezioneDiProvenienza = 'schedaintervento';
                                campoDescrizione =  "#Maintenance0Description";
                                loadClientCatalog(0,$(startClientId).val(),startClientId,"Good");
                                break;
                        }

                        $(campoDescrizione).autocomplete
                        (
                            {
                                focus: function( event, ui )
                                {
                                    event.preventDefault();
                                    return false;
                                },
                                select: function(event, ui)
                                {
                                    event.preventDefault();

                                    // Fatture

                                    if(sezioneDiProvenienza == 'fatture')
                                    {
                                        $("#Good0Oggetto").val(ui.item.descrizione);
                                        $("#Good0Prezzo").val(ui.item.prezzo);
                                        $("#Good0StorageId").val(ui.item.value);
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Unita").val(ui.item.unit);

                                        // Carico l'iva sugli articoli
//?                                            loadClientVat($(clientInputId).val(),"#Good0IvaId");
                                    }

                                    if(sezioneDiProvenienza == 'scontrino')
                                    {
                                        $("#Good0Oggetto").val(ui.item.descrizione);
                                        $("#Good0StorageId").val(ui.item.value);
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Unita").val(ui.item.unit);

                                        // Carico l'iva sugli articoli
//?                                            loadClientVat($(clientInputId).val(),"#Good0IvaId");
                                    }

                                    // Preventivo
                                    if(sezioneDiProvenienza == 'preventivi')
                                    {
                                        $("#Good0Codice").val(ui.item.codice);
                                        $("#Good0Description").val(ui.item.descrizione);
                                        $("#Good0UnitOfMeasureId").val(ui.item.unit);
                                        $("#Good0QuoteGoodRowPrice").val(ui.item.prezzo);
                                        $("#Good0StorageId").val(ui.item.value);

                                        // Carico l'iva sugli articoli
                                        //loadClientVat($(clientInputId).val(),"#Good0QuoteGoodRowVatId");

                                        // Se non è definita l'iva sull'articolo
//?                                            if(ui.item.vat == null)
//?                                            {
//?                                                // Carico l'iva del cliente
//?                                                loadClientVat($(clientInputId).val(),"#Good0QuoteGoodRowVatId");
//?                                            }
//?                                            else
//?                                            {
//?                                                $("#Good0QuoteGoodRowVatId").val(ui.item.vat);
//?                                            }
                                    }

                                    if(sezioneDiProvenienza == 'ddt')
                                    {
                                        $("#Transportgood0Oggetto").val(ui.item.descrizione);
                                        $("#Transportgood0Prezzo").val(ui.item.prezzo);
                                        $("#Transportgood0StorageId").val(ui.item.value);
                                        $("#Transportgood0Codice").val(ui.item.codice);
                                        $("#Transportgood0Unita").val(ui.item.unit);
                                    }

                                    // Preventivo
                                    if(sezioneDiProvenienza == 'schedaintervento')
                                    {
                                        $("#Maintenancerow0Codice").val(ui.item.codice);
                                        $("#Maintenancerow0Description").val(ui.item.descrizione);
                                        $("#Maintenancerow0UnitOfMeasureId").val(ui.item.unit);
                                        $("#Maintenancerow0StorageId").val(ui.item.value);
                                    }
                                },
                                source: articoli,
                                change: function(event,ui) // Fix per rimuovere i value vuoti quando non autocomplete
                                {
                                    // Rimozione dei valori nel caso non sia stato selezionato il valore da autocomplete
                                    if(ui.item == null)
                                    {
                                        if(sezioneDiProvenienza == 'ddt')
                                        {
                                            $("#Transportgood0StorageId").removeAttr('value');
                                            $("#Transportgood0Quantita").removeAttr('maxquantity');
                                        }
                                    }
                                }

                            }
                        );

                        /* Rimuovo tutte le righe se cambia il cliente */
                        /* ('.contacts_row').each(function()
                        {
                            if($(this).hasClass('originale'))
                            {
                                // Niente
                            }
                            else
                            {
                                // Rimuovi riga
                                $(this).remove();
                            }
                        });
                        $('.originale').addClass('ultima_riga'); */

                    },
                    error: function(data)
                    {
                    }
                })

                // Carico il metodo di pagamento del cliente a meno che non sia uno scontrino
                if(sezioneDiProvenienza != 'scontrino') {
                    if (sezioneDiProvenienza == 'preventivi') {
                        loadClientPaymentMethod(clientInputId, "#QuotePayment",true);
                    } else {
                        loadClientPaymentMethod(clientInputId, "#BillPaymentId",false);
                    }
                }

                // Carico il metodo di pagamento del cliente
                loadclientispa(clientInputId);

                // Carico il metodo di pagamento del cliente
                <?php
                if(MULTICURRENCY)
                {
                ?>
                loadClientCurrency(clientInputId);
                <?php
                }
                ?>

                // Carico la ritenuta d'acconto
                loadClientWithholdingTax(clientInputId);

                // Carico il metodo di pagamento del cliente
                loadClientSplitPayment(clientInputId);

                // Carico indirizzo del cliente
                loadClientDefaultAddress(clientInputId,sezioneDiProvenienza);

                // Carico differente indirizzo di spedizione
                loadClientDifferentAddress(clientInputId,sezioneDiProvenienza);

                loadClientDiscount(clientInputId,sezioneDiProvenienza);

                // Carico indirizzo alternativo clienti rivenditori
                // loadClientDifferentReferredAddress(clientInputId,sezioneDiProvenienza);

                // Abilita il clonable
                enableCloning($(clientInputId).val(),clientInputId);
                return false;
            },
        });
    }
</script>
