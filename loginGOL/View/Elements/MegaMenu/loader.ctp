<script>
    addLoadEvent(function() {
        $('#mega_menu').find('[id^="sub_"]').each(function() {

            var menuItem = $(this);
        	var id = menuItem.attr('id');
        	var parentMenuItem = menuItem.parents('.level-zero').first();
        	var parentId = parentMenuItem.attr('id');
        	
        	$(this).find('a').first().click(function(e) {
        	    e.preventDefault();
        	    e.stopImmediatePropagation();
        	    e.stopPropagation();
        	    
        	    var menuItem = $(this);
        	    var request = $.ajax({
    				url: "<?= 
    					 $this->Html->url([
                            "controller" => "Menus",
                            "action" => "ajaxSetActiveMenuItem"
                        ]);
    				?>",
    				method: "POST",
    				data: { 
    					ajax: 1,
    					id: id,
    					parentId: parentId
    				},
    				complete: function(jqXHR, textStatus) {
        			    if(jqXHR.status == 200)
                              window.location.replace(menuItem.attr('href'));
                          else
                              window.location.reload()
        			},
        			error: function () {
        			   window.location.reload()
        			}
    			});
        	});
        });
        
        $('#mega_menu').find('[id^="main_"]').each(function() {

            var menuItem = $(this);
        	var id = menuItem.attr('id');
        	
        	$(this).find('a').first().click(function(e) {
        	    e.preventDefault();
        	    e.stopImmediatePropagation();
        	    e.stopPropagation();
        	    
        	    var menuItem = $(this);
        	    var request = $.ajax({
    				url: "<?= 
    					 $this->Html->url([
                            "controller" => "Menus",
                            "action" => "ajaxSetActiveMenuItem"
                        ]);
    				?>",
    				method: "POST",
    				data: { 
    					ajax: 1,
    					id: "sub_0",
    					parentId: id
    				},
        			complete: function(jqXHR, textStatus) {
        			    if(jqXHR.status == 200)
        			    {
        			        var url = menuItem.attr('href');
        			        if(url == "javascript:;") {
        			            url = menuItem.parents('[id^="main_"]').first().find('[id^="sub_"] a').first().attr('href');
        			        }
                            window.location.replace(url);
        			    }
                        else{
                            window.location.reload()
                       }
        			},
        			error: function () {
        			   window.location.reload()
        			}
    			});
    			  
        	});
        });
    });
</script>


<style>
   
   
   <?php
      $grigio = "#ffffff"; // $LOGOMENU[0]['Settings']['base_css_background'];
      $nero = $LOGOMENU[0]['Setting']['base_css_fontcolor'];
      $azzurro = $LOGOMENU[0]['Setting']['base_css_forecolor'];
      //$azzurro =
   ?>
   /**  LEVEL ZERO **/
   
   .navbar
   {
   	   border-bottom: 5px solid #589AB8;
   }
   
   .page-header {
	    background-color:<?= $grigio ?>;
	    height:auto;
    }    

	.level-zero
	{
	    padding-left: 0px; 
	    background-color:#ffffff ;
	    padding-bottom:0px;
	    color:#ffffff !important;
	}
	
	.level-zero > a
	{
	 	border-radius: 0px 12px 0px 0px !important;
	 	border-right:1px solid #dadfe0;
	}
	
	.level-zero:hover > a
	{
	 	border-radius: 0px 12px 0px 0px !important;
	}
	
	#main_0
	{
		border-radius: 12px 0px 0px 0px !important;
	
	}
	
	#main_0 > a
	{
	 	border-radius: 12px 12px 0px 0px !important;
	 	border-right:1px solid #dadfe0;
	}
	
	#main_0:hover > a
	{
	 	border-radius: 12px 12px 0px 0px !important;
	}
	
	
	.final
	{
		border-radius: 0px 12px 0px 0px !important;
	
	}
	
	.final > a
	{
	 	border-radius: 0px 12px 0px 0px !important;
	 	border-right:1px solid #dadfe0;
	}
	
	.final:hover > a
	{
	 	border-radius: 0px 12px 0px 0px !important;
	}
	
	
	
	
	.menu-voice:hover > a
	{
		background: #589AB8 !important; 
    	border-radius: 0px 12px 0px 0px !important; 
	}
	
	.level-zero .menu-voice>a:hover 
	{
    	background: #589AB8 !important;
    	border-radius: 0px 12px 0px 0px !important;
	}
	
	.level-zero .open
	{
		background: #589AB8 !important;
		border-radius: 0px 12px 0px 0px !important;
	}
	
	.nav .open>a
	{
		background: #589AB8 !important;
		border-radius: 0px 12px 0px 0px !important;
	}
	
	.navbar-nav>li>a
	{
		padding:10px !important;
		padding-bottom:5px !important;
		line-height:21px !important;
	}
	
	.level-zero.menu-voice
	{
		margin-top:0px;
	}

	.level-zero.open .level-uno
	{
		 display:none;
	}

	.level-zero:hover .container-level-uno
	{
		display:block;
	}
	
	.level-zero.open:hover .container-level-uno
	{
		display:block;
	}

	.level-zero.open:hover .level-uno
	{
		display:block;
	}


	/** FINE LEVEL ZERO **/
	
	/** LEVEL UNO **/

	/*.container-level-uno
	{
		
	} */
	.level-uno
	{
		/* background-color:<?php //$grigio ?>; */
		background-color:#ffffff !important; 
	    color:#333333 !important;
	    font-family: 'Barlow Semi Condensed' !important;
	    font-size: 9pt;
	}
	
	.level-uno.active> a
	{
	/*	color:<?php // $grigio ?>; */
		color:#ffffff; 
		background-color:#589AB8 !important; 
	     /* background-color:<?php //$nero ?> !important; */
	   
	
	}
	
	.level-uno > a:hover
	{
		color:<?= $grigio ?> !important;
	    background-color:<?= $nero ?> !important;
	}

	/* FINE LEVEL 1 */
	
	.footer.uk-text-center
	{
		background-color:<?= $grigio ?>;
		color:<?= $nero ?>;
	}
	
	
	.uk-navbar-flip
	{
		background-color:<?= $grigio ?>;
	}
	
	.menu-voice
	{
		margin-top:34px;
		border-bottom:0px;
	}
	
	.menu-voice:hover
	{
	   color:<?= $grigio ?> !important;
	}
	
	.menu-voice:hover > a
	{
	  color:<?= $grigio ?> !important;
	}
	
	.menu-voice>a:hover
	{
		color:<?= $grigio ?> !important;
	}
	
	
	.nav .open>a
	{
	    background-color:<?= $nero ?>;    
	    color:<?= $grigio ?> !important;
	}
	
	.attivo>a
	{
		border-bottom: 5px solid <?= $nero ?> !important;
		color:<?= $grigio ?> !important;
	}
	

	
	.blue-button
	{
		/* background-color:<?php // $grigio ?>; */
		color:<?= $nero ?>;
		border: 1px solid <?= $grigio ?>;
	}
	
	.blue-button:focus
	{
		background-color:<?= $grigio ?>;
		color:<?= $nero ?>;
		border: 1px solid <?= $grigio ?>;	
	}
	
	.blue-button:hover
	{
		background-color:#ea5d0b;
		color:#ffffff !important;
		text-decoration: none !important;
		border : 1px solid <?= $grigio ?> !important;
	}
	
	
	.blue-button-reverse
	{
		color:<?= $grigio ?>;
	/* 	background-color:<?= $nero ?>; */
		background-color:#fff;
		border: 1px solid <?= $grigio ?>;
	}
	
	.blue-button-reverse:focus
	{
		color:<?= $grigio ?>;
		background-color:<?= $nero ?>;
		border: 1px solid <?= $grigio ?>;	
	}
	
	.blue-button-reverse:hover
	{
		color:<?= $grigio ?>;
		background-color:<?= $nero ?> !important;
		border : 1px solid <?= $grigio ?> !important;
	}
	
	/* .h1
	{
		color:<?php // $nero ?> !important;
	}*/
	
	.tools .icon
	{
			color:<?= $nero ?> !important;
	}
	
	.accordion-toggle
	{
			color:<?= $nero ?> !important;
	}
	
	
</style>

<script>
	
	$(document).ready(function()
	{
			var openExist = 0;

			try
			{
				$('.level-zero.open').each(
				function()
				{
					openExist++;	
				});
			}
			catch(ecc)
			{
				
			}
			
			 if(openExist == 0)
			{
				
				$('.level-zero').each
				(
					function()
					{
						
						if($(this).find('a').first().text().trim() == 'Fatture')
						{
							$(this).addClass('open');
						}
						else
						{
						}
					}
				)
				
			} 

        if(indice2 != undefined) {
            var indice2 = +$('.level-zero.open').attr('id').replace('main_', '') - 1;

            $("#" + "main_" + indice2).css("background-color", "#589AB8");
            $("#" + "main_" + indice2).find('a').css("background-color", "#ffffff");

            $('.level-zero').hover
            (
                function () {
                    var indice = +$(this).attr('id').replace('main_', '') - 1;

                    $("#" + "main_" + indice).css("background-color", "#589AB8");
                    $("#" + "main_" + indice).find('a').css("background-color", "#ffffff");

                    if (indice == 1) {
                        $("#" + "main_" + indice).find('a').css("background-color", "#ffffff");
                        $("#" + "main_" + indice).css("border-radius", "0px 25px 0px 0px !important;");
                        $("#" + "main_" + indice).find('a').css("border-radius", "0px 25px 0px 0px !important;");
                    }
                },
                function () {
                    $(".level-zero").css("background-color", "#ffffff");
                    $("#" + "main_" + indice2).css("background-color", "#589AB8");
                }
            )

        }
	});
	
</script>

