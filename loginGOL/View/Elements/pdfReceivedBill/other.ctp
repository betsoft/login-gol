<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';
    
    $left9 = 'float:left;font-size:8px;width:9%;';
    $left10 = 'float:left;font-size:8px;width:10%;';
    $left15 = 'float:left;font-size:8px;width:15%;';
    $left19 = 'float:left;font-size:8px;width:19%;';
    $left20 = 'float:left;font-size:8px;width:20%;';
    $left21 = 'float:left;font-size:8px;width:21%;';
    $left23 = 'float:left;font-size:8px;width:23%;';
    $left24 = 'float:left;font-size:8px;width:24%;';
    $left25 = 'float:left;font-size:8px;width:25%;';
    $left33 = 'float:left;font-size:8px;width:33%;';
    
    $left9b = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left19b = 'float:left;font-size:9px;width:19%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left21b = 'float:left;font-size:9px;width:21%;font-weight:bold;';
    $left24b = 'float:left;font-size:9px;width:24%;font-weight:bold;';
    $left25b = 'float:left;font-size:9px;width:25%;font-weight:bold;';
    $left33b = 'float:left;font-size:9px;width:33%;font-weight:bold;';
    $left34b = 'float:left;font-size:9px;width:34%;font-weight:bold;';
?>

<div style="width:200mm;">&nbsp;</div>

<table style="width:200mm;border: 1px solid;border-collapse: collapse;">
    <tr>
        <td style="<?= $left20b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Modalità pagamento</td>
        <td style="<?= $left21b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">IBAN</td>
        <td style="<?= $left20b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Istituto</td>
        <td style="<?= $left19b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Data scadenza</td>
        <td style="<?= $left19b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Importo</td>
    </tr>
<?php foreach($bill['Deadline'] as $deadline):  ?>
    <tr>
    <?php
        switch ($deadline['imported_payment_method'])
        {
            case MP01: $description = 'Contanti' ; break;
            case MP02: $description = 'Assegno' ; break;
            case MP03: $description = 'Assegno circolare' ; break;
            case MP04: $description = 'contanti presso tesoreria' ; break;
            case MP05: $description = 'bonifico' ; break;
            case MP06: $description = 'vaglia cambiario' ; break;
            case MP07: $description = 'bollettino bancario' ; break;
            case MP08: $description = 'carta di pagamento' ; break;
            case MP09: $description = 'RID' ; break;
            case MP10: $description = 'RID utenze' ; break;
            case MP11: $description = 'RID veloce' ; break;
            case MP12: $description = 'RIBA' ; break;
            case MP13: $description = 'MAV' ; break;
            case MP14: $description = 'quietanza erario stato' ; break;
            case MP15: $description = 'giroconto su conti di contabilità speciale' ; break;
            case MP16: $description = 'domiciliazione bancaria' ; break;
            case MP17: $description = 'domiciliazione postale' ; break;
            case MP18: $description = 'bollettino di c/c postale' ; break;
            case MP19: $description = 'SEPA Direct Debit' ; break;
            case MP20: $description = 'SEPA Direct Debit CORE' ; break;
            case MP21: $description = 'SEPA Direct Debit B2B' ; break;
            case MP22: $description = 'Trattenuta su somme già riscosse' ; break;
         }
    ?>
        <td style="<?= $left20 ?>border-right: 1px solid #0f0f0f;" align="left">&nbsp;<?= $description ?></td>
        <td style="<?= $left21 ?>border-right: 1px solid #0f0f0f;" align="left">&nbsp;<?= $deadline['imported_IBAN'] ?></td>
        <td style="<?= $left20 ?>border-right: 1px solid #0f0f0f;" align="left">&nbsp;<?= $deadline['imported_bank'] ?></td>
        <td style="<?= $left19 ?>border-right: 1px solid #0f0f0f;" align="center">&nbsp;<?= $deadline['deadline'] != '0000-00-00' ? date('d/m/Y',strtotime($deadline['deadline'])) : ''; ?></td>
        <td style="<?= $left19 ?>" align="right">&nbsp;<?= number_format($deadline['amount'],2,',','.') ?></td>
    </tr>
<?php endforeach; ?>
</table>

<div style="width:200mm;">&nbsp;</div>

<table style="width:200mm;border: 1px solid;border-collapse: collapse;">
    <tr>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Aliquota ritenuta</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Importo ritenuta</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Aliquota cassa</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Importo cassa</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Aliquota iva cassa</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Natura</td>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center">Ritenuta</td>
    </tr>
<?php foreach($bill['Billgestionaldata'] as $billgestionaldata): ?>
    <tr>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="right"><?= isset($billgestionaldata['importedwithholdingpercentage']) ?  number_format($billgestionaldata['importedwithholdingpercentage'],2,',','') :  '&nbsp;'  ?></td>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="right"><?= isset($billgestionaldata['importedwithholdingvalue']) ? number_format($billgestionaldata['importedwithholdingvalue'],2,',','') : '&nbsp;'  ?></td>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="right"><?= isset($billgestionaldata['importedwelfareboxpercentage']) ? number_format($billgestionaldata['importedwelfareboxpercentage'],2,',','') : '&nbsp;'  ?></td>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="right"><?= isset($billgestionaldata['importedwelfareboxvalue'])  ? number_format($billgestionaldata['importedwelfareboxvalue'],2,',','') : '&nbsp;'  ?></td>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="right"><?= isset($billgestionaldata['importedwelfareboxvatpercentage'])  ? number_format($billgestionaldata['importedwelfareboxvatpercentage'],2,',','') : '&nbsp;'  ?></td>
        <td style="<?= $left15 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center"><?= isset($billgestionaldata['importedwelfareboxvatnature'])  ? $billgestionaldata['importedwelfareboxvatnature'] : '&nbsp;'  ?></td>
        <td style="<?= $left10 ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;" align="center"><?= isset($billgestionaldata['importedwelfareboxwithholdingappliance']) ? $billgestionaldata['importedwelfareboxwithholdingappliance'] : '&nbsp;' ; ?></td>
    </tr>
<?php endforeach; ?>
</table>