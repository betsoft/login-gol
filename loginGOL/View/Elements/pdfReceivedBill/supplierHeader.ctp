<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';
    
    $left9 = 'float:left;font-size:9px;width:9%;';
    $left10 = 'float:left;font-size:9px;width:10%;';
    $left15 = 'float:left;font-size:9px;width:15%;';
    $left19 = 'float:left;font-size:9px;width:19%;';
    $left20 = 'float:left;font-size:9px;width:20%;';
    $left23 = 'float:left;font-size:9px;width:23%;';
    $left50 = 'float:left;font-size:9px;width:50%;';
    $left70 = 'float:left;font-size:9px;width:70%;';
    $left100 = 'float:left;font-size:9px;width:100%;';

    $left9b = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left19b = 'float:left;font-size:9px;width:19%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left23b = 'float:left;font-size:9px;width:23%;font-weight:bold;';
    $left50b = 'float:left;font-size:9px;width:50%;font-weight:bold;';
    $left70b = 'float:left;font-size:9px;width:70%;font-weight:bold;';
    $left99b = 'float:left;font-size:9px;width:99%;font-weight:bold;';
    $left100b = 'float:left;font-size:9px;width:100%;font-weight:bold;';
?>

<div style="width:200mm;border: 1px solid #0f0f0f;">
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50b; ?>">MITTENTE</div>
        <div style="<?= $left50b; ?>">DESTINATARIO</div>
    </div>
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50; ?>"> Identificativo fiscale ai fini IVA: <?= 'IT' . $bill['Supplier']['piva']; ?></div>
        <div style="<?= $left50; ?>">Identificativo fiscale ai fini IVA: <?= $clientnation . $settings['Setting']['piva']; ?></div>
    </div>
    <?php if(isset($bill['Supplier']['cf']) && $bill['Supplier']['cf'] != '' ): ?>
        <div style="width:200mm;padding-left:2px;">
            <div style="<?= $left50; ?>"> Codice fiscale: <?= $bill['Supplier']['cf']; ?></div>
            <div style="<?= $left50; ?>">Codice fiscale: <?= $settings['Setting']['piva']; ?></div>
        </div>
    <?php endif; ?>
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50; ?>"> Denominazione: <?= $bill['Bill']['supplier_name']; ?></div>
        <div style="<?= $left50; ?>">Denominazione: <?= $bill['Bill']['client_name']; ?></div>
    </div>
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50; ?>"> Indirizzo: <?=$bill['Bill']['supplier_address']; ?></div>
        <div style="<?= $left50; ?>">Indirizzo: <?= $bill['Bill']['client_address']; ?></div>
    </div>
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50; ?>"> Comune: <?= $bill['Bill']['supplier_city']; ?> Provincia: <?= $bill['Bill']['supplier_province'] ?></div>
        <div style="<?= $left50; ?>">Comune: <?= $bill['Bill']['client_city']; ?>  Provincia: <?= $bill['Bill']['client_province']; ?></div>
    </div>
    <div style="width:200mm;padding-left:2px;">
        <div style="<?= $left50; ?>"> Cap: <?= $bill['Bill']['supplier_cap'] ?> Nazione: <?= $nation ?> </div>
        <div style="<?= $left50; ?>">Cap: <?= $bill['Bill']['client_cap']; ?> Nazione: <?= $clientnation ?> </div>
    </div>
</div>
<div style="width:200mm;">&nbsp;</div>