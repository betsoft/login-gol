<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';
    
    $left9 = 'float:left;font-size:9px;width:9%;';
    $left10 = 'float:left;font-size:9px;width:10%;';
    $left15 = 'float:left;font-size:9px;width:15%;';
    $left20 = 'float:left;font-size:9px;width:20%;';
    $left23 = 'float:left;font-size:9px;width:23%;';
    $left24 = 'float:left;font-size:9px;width:24%;';
    $left25 = 'float:left;font-size:9px;width:25%;';
    $left33 = 'float:left;font-size:9px;width:33%;';
    
    $left9b = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left24b = 'float:left;font-size:9px;width:24%;font-weight:bold;';
    $left25b = 'float:left;font-size:9px;width:25%;font-weight:bold;';
    $left33b = 'float:left;font-size:9px;width:33%;font-weight:bold;';
    $left34b = 'float:left;font-size:9px;width:34%;font-weight:bold;';
?>

<div style="width:200mm;">&nbsp;</div>

<div style="width:200mm;border: 1px solid #0f0f0f;">
    <div style="<?= $left25b ?>border-right: 1px solid #0f0f0f;" align="center">Importo bollo</div>
    <div style="<?= $left25b ?>border-right: 1px solid #0f0f0f;" align="center">Sc. Mag.</div>
    <div style="<?= $left25b ?>border-right: 1px solid #0f0f0f;" align="center">valuta</div>
    <div style="<?= $left24b ?>" align="center">Totale fattura</div>
</div>

<div style="width:200mm;border: 1px solid #0f0f0f;">
     <?php
        $totTaxableVat = $totVat = $total = 0;
            
        foreach ($vatValues as $vat)
        {
            $totTaxableVat +=  number_format($vat['imponibile'] + $bill['Bill']['rounding'],2,'.','') ;
            $totVat +=  number_format($vat['vat'],2,'.','') ;
        }
        $total +=  ($totTaxableVat + $totVat );
    ?>
    <div style="<?= $left25 ?>border-right: 1px solid #0f0f0f;" align="center"><?= isset($bill['Bill']['seal'] ) ? $bill['Bill']['seal'] : '&nbsp;' ?> </div>
    <div style="<?= $left25 ?>border-right: 1px solid #0f0f0f;" align="center">&nbsp;</div>
    <div style="<?= $left25 ?>border-right: 1px solid #0f0f0f;" align="center"><?= isset($currency) ?  $currency : '&nbsp;'?></div>
    <div style="<?= $left24 ?>" align="right">
        <?php
        if($total !=  $bill['Bill']['importo'] &&  $bill['Bill']['importo'] > 0)
            echo $bill['Bill']['importo'];
        else
            echo number_format($total,2,',','');
        ?>
    </div>
</div>