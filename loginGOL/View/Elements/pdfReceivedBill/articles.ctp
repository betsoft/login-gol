<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';

    $left4 = 'float:left;font-size:9px;width:4%;';
    $left5 = 'float:left;font-size:9px;width:5%;';
    $left7 = 'float:left;font-size:9px;width:7%;';
    $left8 = 'float:left;font-size:9px;width:8%;';
    $left9 = 'float:left;font-size:9px;width:9%;';
    $left10 = 'float:left;font-size:9px;width:10%;';
    $left15 = 'float:left;font-size:9px;width:15%;';
    $left20 = 'float:left;font-size:9px;width:20%;';
    $left23 = 'float:left;font-size:9px;width:23%;';
    $left27 = 'float:left;font-size:9px;width:27%;';
    $left30 = 'float:left;font-size:9px;width:30%;';
    $left34 = 'float:left;font-size:9px;width:34%;';
    $left38 = 'float:left;font-size:9px;width:38%;';

    $left4m = 'float:left;font-size:7px;width:4%;';
    $left5m = 'float:left;font-size:7px;width:5%;';
    $left7m = 'float:left;font-size:7px;width:7%;';
    $left8m = 'float:left;font-size:7px;width:8%;';
    $left9m = 'float:left;font-size:7px;width:9%;';
    $left10m = 'float:left;font-size:7px;width:10%;';
    $left15m = 'float:left;font-size:7px;width:15%;';
    $left20m = 'float:left;font-size:7px;width:20%;';
    $left23m = 'float:left;font-size:7px;width:23%;';
    $left27m = 'float:left;font-size:7px;width:27%;';
    $left30m = 'float:left;font-size:7px;width:30%;';
    $left34m = 'float:left;font-size:7px;width:34%;';
    $left38m = 'float:left;font-size:7px;width:38%;';

    $left4b = 'float:left;font-size:9px;width:4%;font-weight:bold;';
    $left5b = 'float:left;font-size:9px;width:5%;font-weight:bold;';
    $left7b = 'float:left;font-size:9px;width:7%;font-weight:bold;';
    $left8b = 'float:left;font-size:9px;width:8%;font-weight:bold;';
    $left9b = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left23b = 'float:left;font-size:9px;width:23%;font-weight:bold;';
    $left27b = 'float:left;font-size:9px;width:27%;font-weight:bold;';
    $left30b = 'float:left;font-size:9px;width:30%;font-weight:bold;';
    $left34b = 'float:left;font-size:9px;width:34%;font-weight:bold;';
    $left38b = 'float:left;font-size:9px;width:38%;font-weight:bold;';
    $left70b = 'float:left;font-size:9px;width:70%;font-weight:bold;';
?>

<table style="width:200mm;border: 1px solid;border-collapse: collapse;">
    <tr>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; ">&nbsp;Cod. Articolo</td>
        <td style="<?= $left38b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; ">&nbsp;Descrizione</td>
        <td style="<?= $left8b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; ">&nbsp;Quantità</td>
        <td style="<?= $left4b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align='center'>&nbsp;UM</td>
        <td style="<?= $left8b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align='center'>&nbsp;Prezzo Un.</td>
        <td style="<?= $left8b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align='center'>&nbsp;Sc.Mag.</td>
        <td style="<?= $left8b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align='center'>&nbsp;%IVA</td>
        <td style="<?= $left5b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align='center'>&nbsp;Natura</td>
        <td style="<?= $left9b ?>border-bottom: 1px solid #0f0f0f;" align='center'>Prezzo Tot.</td>
        <td  style="border-bottom: 1px solid #0f0f0f;" ></td>
    </tr>
<?php
    $savedNote = 0;
    $minSplittotal = 2;
    $minsplitprice = 2;

    foreach($bill['Good'] as $row)
    {
        $explodedArray = explode('.',$row['imported_total_row'] + 0 );
        if(isset($explodedArray[1]))
        {
            if(strlen($explodedArray[1]) > $minSplittotal)
                $minSplittotal = strlen($explodedArray[1]);
        }

        $explodedArray2 = explode('.',$row['prezzo'] + 0 );

        if(isset($explodedArray2[1]))
        {
            if(strlen($explodedArray2[1]) > $minsplitprice)
                $minsplitprice = strlen($explodedArray2[1]);
        }
    }

    foreach($bill['Billgestionaldata'] as $billdata)
    {
        // Riferimento DDT collegato ad intero documento
        if('' . $billdata['rifddt'] != '')
        {
            $savedNote++;
            isset($billdata['rifddt']) && $billdata['rifddt'] != null ? $rifddt = 'Rif. DDT. : '.$billdata['rifddt'] : $rifddt = '';
            isset($billdata['dateddt']) && $billdata['dateddt'] != null ? $dateddt = 'del '.$billdata['dateddt'] : $dateddt = '';
        ?>
            <tr>
                <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
                <td style="<?= $left38m ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= $rifddt . ' ' . $dateddt; ?></td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left9m ?>align='right'"></td>
                <td></td>
            </tr>
           <?php
       }

        // Riferimenti documenti collegati all'intero documento
        if('' . $billdata['IdDocumento'] != '')
        {
            $savedNote++;
            isset($billdata['codiceCIG']) ? $codiceCig = 'Codice CIG: '.$billdata['codiceCIG'] : $codiceCig = '';
            isset($billdata['codiceCUP']) ? $codiceCup = 'Codice CUP: '.$billdata['codiceCUP'] : $codiceCup = '';
            isset($billdata['NumItem']) ? $numitem = $billdata['NumItem'] : $numitem = '';
            isset($billdata['CodiceCommessaConvenzione']) ? $CodiceCommessaConvenzione = 'Codice conmmessa/convenzione : '.$numitem['CodiceCommessaConvenzione'] : $CodiceCommessaConvenzione = '';

            isset($billdata['data']) && $billdata['data'] != null ? $dataDocument = ' del ' . date("d/m/Y",strtotime($billdata['Data']))  : $dataDocument = '';

            switch($billdata['document_type'])
            {
                case 0: $documento = 'Vs. ordine'; break;
                case 1: $documento = 'Vs. Contratto'; break;
                case 2: $documento = 'Vs. commessa / convenzione'; break;
            }
        ?>
            <tr>
                <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
                <td style="<?= $left38m ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= $documento . ' ' . $billdata['IdDocumento'] . ' ' . $dataDocument . ' ' .$numitem. ' ' . $codiceCig . ' ' . $codiceCup; ?></td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left9m ?>align='right'"></td>
                <td></td>
            </tr>
        <?php
        }
    }

    // CIG
    if($bill['Bill']['cig_documentid'])
    {
        $savedNote++;
        switch($bill['Bill']['cig_type'] )
        {
            case 0: $cigType = "ordine d'acquisto"; break;
            case 1: $cigType = "contratto"; break;
            case 2: $cigType = "convenzione"; break;
            case 3: $cigType = "fattura collegata"; break;
            default: $cigType = ''; break;
        }

        $bill['Bill']['cig'] != '' ? $cig = ' Cig: '.$bill['Bill']['cig'] : $cig = '';
        $bill['Bill']['cup']!= '' ?  $cup =' Cup ' . $bill['Bill']['cup'] : $cup = '';
        $bill['Bill']['cig_date'] != null ? $cigDate = ' del ' .date("d-m,Y", strtotime($bill['Bill']['cig_date'])) : $cigDate = '';
    ?>
        <tr>
            <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
            <td style="<?= $left38 ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= 'Vs.' . $cigType . ' Num.' .$bill['Bill']['cig_documentid'] . $cigDate . $cig . $cup ?>&nbsp;</td>
            <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left4 ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
            <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left5 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left9 ?>" align='right'>&nbsp;</td>
            <td></td>
        </tr>
        <?php
    }
?>
<!-- SCRITTURA DELLE RIGHE ARTICOLO -->
<?php if($savedNote > 0): ?>
   <tr>
       <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
       <td style="<?= $left38m ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= '----------------------------------------------------------------------------------------' ?></td>
       <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
       <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
       <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
       <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
       <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
       <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
       <td style="<?= $left9m ?>align='right'"></td>
       <td></td>
<?php endif;?>
<?php
$rifatt = "";
    foreach($bill['Good'] as $row)
    {
        foreach ($row['Goodgestionaldata'] as $gestionalData)
        {
            // Riferimento documenti legati alle singole righe
            if('' . $gestionalData['IdDocumento'] != '')
            {
                $documento= '';
                isset($gestionalData['codiceCIG']) ? $codiceCig = 'Codice CIG: '.$gestionalData['codiceCIG'] : $codiceCig = '';
                isset($gestionalData['codiceCUP']) ? $codiceCup = 'Codice CUP: '.$gestionalData['codiceCUP'] : $codiceCup = '';
                isset($gestionalData['NumItem']) ? $numitem  = $numitem['NumItem'] : $numitem = '';
                isset($gestionalData['CodiceCommessaConvenzione']) ? $CodiceCommessaConvenzione = 'Codice conmmessa/convenzione : '.$numitem['CodiceCommessaConvenzione'] : $CodiceCommessaConvenzione = '';
                isset($billdata['data']) && $billdata['data'] != null ? $dataDocument = ' del ' . date("d/m/Y",strtotime($billdata['Data']))  : $dataDocument = '';

                switch($gestionalData['document_type'])
                {
                    case 0: $documento = 'Vs. ordine';break;
                    case 1: $documento = 'Vs. Contratto'; break;
                    case 2: $documento = 'Vs. commessa / convenzione'; break;
                }
            ?>
                <tr>
                    <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
                    <td style="<?= $left38m ?>border-right: 1px solid #0f0f0f;"><?= $documento . ' ' . $gestionalData['IdDocumento'] . ' ' . $dataDocument . ' ' .$numitem. ' ' . $codiceCig . ' ' . $codiceCup; ?>&nbsp;</td>
                    <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                    <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
                    <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                    <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                    <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                    <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                    <td style="<?= $left9m ?>align='right'"></td>
                    <td></td>
                </tr>
            <?php
            }

            // Riferimento DDT collegato alle singole righe
            if('' . $gestionalData['rifddt'] != '')
            {
                isset($gestionalData['rifddt']) ? $rifddt = 'Rif. DDT. : '.$gestionalData['rifddt'] : $rifddt = '';
                isset($gestionalData['dateddt']) && $gestionalData['dateddt'] != null ? $dateddt = 'del '. date("d-m-Y", strtotime($gestionalData['dateddt'])) : $dateddt = '';
                if($rifatt == ""){
                    ?>
                    <tr>
                        <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
                        <td style="<?= $left38b ?>border-right: 1px solid #0f0f0f;"><strong><?= $rifddt . ' ' . $dateddt; ?></strong>&nbsp;</td>
                        <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                        <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
                        <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                        <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                        <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                        <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                        <td style="<?= $left9m ?>align='right'"></td>
                        <td></td>
                    </tr>
                    <?php
                }
                if(isset($gestionalData['rifddt']) && $rifatt == ""){
                    $rifatt = $rifddt;
                }
                else{
                }

                if($rifddt !== $rifatt){
                    $rifatt = $rifddt;
        ?>
            <tr>
                <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
                <td style="<?= $left38b ?>border-right: 1px solid #0f0f0f;"><strong><?= $rifddt . ' ' . $dateddt; ?></strong>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'> </td>
                <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
                <td style="<?= $left9m ?>align='right'"></td>
                <td></td>
            </tr>
            <?php
            }
        }}

        $row['tipocessioneprestazione'] == null ? $tipocessioneprestazione = '' : $tipocessioneprestazione =  '(' .  $row['tipocessioneprestazione'] . ')';
        $articlecode = '';
        isset($row['importedarticlevalue']) ? $articlecode .=  $row['importedarticlevalue'] : null;

        $periodo = '';
        isset($row['importedstartperiod']) ? $periodo .= 'Periodo dal '.date('d/m/Y' ,strtotime($row['importedstartperiod'])) : null;
        isset($row['importedendperiod']) ? $periodo .= ' a '. date('d/m/Y' ,strtotime($row['importedendperiod'])) : null;
    ?>
    <tr>
        <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;"><?= $articlecode ?>&nbsp;</td>
        <td style="<?= $left38 ?>border-right: 1px solid #0f0f0f;"><?= $row['oggetto'] . ' '. $row['customdescription'] .' '. $tipocessioneprestazione ?>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'><?= isset($row['quantita'])  ? $row['quantita'] : '&nbsp;'  ?>&nbsp;</td>
        <td style="<?= $left4 ?>border-right: 1px solid #0f0f0f;" align='center'><?= isset($row['unita']) && $row['unita'] != '' ? $row['unita'] : '&nbsp;' ?>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'><?= number_format($row['prezzo'] + 0,$minsplitprice) ?>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'><?= $row['discount'] > 0 ? $row['discount'] . ' %' : '&nbsp;'?> <br> <?= $row['discount2'] > 0 ? $row['discount2'].'%' : '&nbsp;'?></td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'><?= count($row['Iva']) > 0 ? $row['Iva']['percentuale'] :  '&nbsp;'  ?>&nbsp;</td>
        <td style="<?= $left5 ?>border-right: 1px solid #0f0f0f;" align='right'><?= count($row['Iva']) > 0 ? $row['Iva']['codice'] :  '&nbsp;'  ?>&nbsp;</td>
        <td style="<?= $left9 ?>" align='right'><?= number_format($row['imported_total_row'] + 0,$minSplittotal); ?></td>
        <td></td>
    </tr>
    <?php if(isset($row['importedstartperiod']) || isset($row['importedendperiod'] )): ?>
    <tr>
        <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
        <td style="<?= $left38 ?>border-right: 1px solid #0f0f0f;"><?= $periodo ?>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
        <td style="<?= $left4 ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
        <td style="<?= $left8 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
        <td style="<?= $left5 ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
        <td style="<?= $left9 ?>" align='right'>&nbsp;</td>
        <td></td>
    </tr>
<?php endif; ?>
    <?php
        foreach ($row['Goodgestionaldata'] as $gestionalData)
        {
            $detailRow = '';

            if('' . $gestionalData['ref_text'] != '')
                $detailRow .= $gestionalData['ref_text'];

            if('' . $gestionalData['data_type'] != '')
                $detailRow == '' ? $detailRow .= $gestionalData['data_type'] : $detailRow .= ' ' . $gestionalData['data_type'];

            if('' . $gestionalData['ref_number'] != '')
                $detailRow == '' ? $detailRow .= $gestionalData['ref_number'] : $detailRow .= ' ' . $gestionalData['ref_number'];

            if('' . $gestionalData['ref_text'] != '')
                $detailRow == '' ? $detailRow .= $gestionalData['ref_text'] : $detailRow .= ' ' . $gestionalData['ref_text'];
    ?>
    <?php if($detailRow != ''): ?>
         <tr>
            <td style="<?= $left10m ?>border-right: 1px solid #0f0f0f;">&nbsp;</td>
            <td style="<?= $left38m ?>border-right: 1px solid #0f0f0f;"><?= $gestionalData['ref_text'] . ' '. $gestionalData['data_type'] . ' '. $gestionalData['ref_number']. ' '. $gestionalData['ref_data'] ?>&nbsp;</td>
            <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left4m ?>border-right: 1px solid #0f0f0f;" align='center'>&nbsp;</td>
            <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left8m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left5m ?>border-right: 1px solid #0f0f0f;" align='right'>&nbsp;</td>
            <td style="<?= $left9m ?>align='right'"></td>
            <td></td>
        </tr>
    <?php endif; ?>
    <?php
        }
    }
?>
</table>