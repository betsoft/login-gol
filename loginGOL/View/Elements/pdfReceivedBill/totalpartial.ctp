<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';
    
    $left9 = 'float:left;font-size:9px;width:9%;';
    $left10 = 'float:left;font-size:9px;width:10%;';
    $left15 = 'float:left;font-size:9px;width:15%;';
    $left19 = 'float:left;font-size:9px;width:19%;';
    $left20 = 'float:left;font-size:9px;width:20%;';
    $left23 = 'float:left;font-size:9px;width:23%;';
    $left30 = 'float:left;font-size:9px;width:30%;';
    $left70 = 'float:left;font-size:9px;width:70%;';
    
    $left9b = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left19b = 'float:left;font-size:9px;width:19%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left23b = 'float:left;font-size:9px;width:23%;font-weight:bold;';
    $left30b = 'float:left;font-size:9px;width:30%;font-weight:bold;';
    $left50b = 'float:left;font-size:9px;width:50%;font-weight:bold;';
    $left70b = 'float:left;font-size:9px;width:70%;font-weight:bold;';
?>

<div style="width:200mm;">&nbsp;</div>

<table style="width:200mm;border: 1px solid;border-collapse: collapse;">
    <tr>
        <td style="<?= $left30b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align="center">Dati riepilogo</td>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align="center">%IVA</td>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align="center">Natura</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align="center">Totale imposta</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f; " align="center">Spese accessorie</td>
        <td style="<?= $left19b ?>border-bottom: 1px solid #0f0f0f; " align="center">Imponibile</td>
    </tr>
    <?php foreach($datiriepilogo as $linea): ?>
        <?php if($linea['Billgestionaldata']['importedcausals'] == null && $linea['Billgestionaldata']['aliquotaIVA'] != null ): ?>
            <tr>
                <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;">
                <?php
                    switch ($linea['Billgestionaldata']['einvoicevatesigibility'])
                    {
                        case 'I':
                            echo "Esigiblità immediata";

                            if(isset($linea['Billgestionaldata']['riferimentonormativo']))
                                echo '<br/>' . $linea['Billgestionaldata']['riferimentonormativo'];
                        break;
                        case 'D':
                            echo "Eesigiblità differita" ;

                            if(isset($linea['Billgestionaldata']['riferimentonormativo']))
                                echo '<br/>' . $linea['Billgestionaldata']['riferimentonormativo'];
                        break;
                        case 'S':
                            echo "Split Payment" ;

                            if(isset($linea['Billgestionaldata']['riferimentonormativo']))
                                echo '<br/>' . $linea['Billgestionaldata']['riferimentonormativo'];
                        break;
                        default :
                            if(isset($linea['Billgestionaldata']['riferimentonormativo']))
                                echo $linea['Billgestionaldata']['riferimentonormativo'];
                            else
                                echo "&nbsp;" ;
                        break;
                    }
                ?>
                </td>
                <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;" align="right">&nbsp;<?= $linea['Billgestionaldata']['aliquotaIVA'] ? number_format($linea['Billgestionaldata']['aliquotaIVA'],2,',','.') : null; ?>&nbsp;</td>
                <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;text-align:right;" >&nbsp;<?= $linea['Billgestionaldata']['natura'] != null ? $linea['Billgestionaldata']['natura']  : '&nbsp;' ?>&nbsp;</td>
                <td style="<?= $left15 ?>border-right: 1px solid #0f0f0f;" align="right">&nbsp;<?= number_format($linea['Billgestionaldata']['imposta'],2,',','.') ?>&nbsp;</td>
                <td style="<?= $left15 ?>border-right: 1px solid #0f0f0f;" align="right">&nbsp;</td>
                <td style="<?= $left19 ?>" align="right">&nbsp;<?= number_format($linea['Billgestionaldata']['imponibileimporto'] + $linea['Billgestionaldata']['arrotondamento'],2,',','&nbsp;') ?>&nbsp;</td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
</table>