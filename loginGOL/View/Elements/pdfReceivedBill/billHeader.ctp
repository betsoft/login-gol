<?php
    $firstColumn = 'width:140mm;font-size:11px;';
    $secondColumn = 'width:60mm;font-size:11px;';
    
    $left9 = 'float:left;font-size:9px;width:9%;';
    $left10 = 'float:left;font-size:9px;width:10%;';
    $left15 = 'float:left;font-size:9px;width:15%;';
    $left19 = 'float:left;font-size:9px;width:19%;';
    $left20 = 'float:left;font-size:9px;width:20%;';
    $left23 = 'float:left;font-size:9px;width:23%;';
    $left24 = 'float:left;font-size:9px;width:24%;';
    $left25 = 'float:left;font-size:9px;width:25%;';
    $left33 = 'float:left;font-size:9px;width:33%;';
    
    $left9b  = 'float:left;font-size:9px;width:9%;font-weight:bold;';
    $left10b = 'float:left;font-size:9px;width:10%;font-weight:bold;';
    $left15b = 'float:left;font-size:9px;width:15%;font-weight:bold;';
    $left19b = 'float:left;font-size:9px;width:19%;font-weight:bold;';
    $left20b = 'float:left;font-size:9px;width:20%;font-weight:bold;';
    $left24b = 'float:left;font-size:9px;width:24%;font-weight:bold;';
    $left25b = 'float:left;font-size:9px;width:25%;font-weight:bold;';
    $left33b = 'float:left;font-size:9px;width:33%;font-weight:bold;';
    $left34b = 'float:left;font-size:9px;width:34%;font-weight:bold;';
?>

<table style="width:200mm;border: 1px solid;border-collapse: collapse;">
    <tr>
        <td style="<?= $left19b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;">&nbsp;Tipologia documento</td>
        <td style="<?= $left25b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;;">&nbsp;Causale</td>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;">&nbsp;Art 73</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;">&nbsp;Numero</td>
        <td style="<?= $left10b ?>border-bottom: 1px solid #0f0f0f;border-right:1px solid #0f0f0f;">&nbsp;Data</td>
        <td style="<?= $left15b ?>border-bottom: 1px solid #0f0f0f;">&nbsp;Codice destinatario</td>
    </tr>
    <tr>
        <td style="<?= $left19 ?>border-right: 1px solid #0f0f0f;">
        <?php
            switch($bill['Bill']['typeofdocument'])
            {
                case "TD01": $typeOfDocument = "Fattura"; break;
                case "TD02": $typeOfDocument = "Acconto/anticipo su fattura"; break;
                case "TD03": $typeOfDocument = "Acconto/anticipo su parcella"; break;
                case "TD04": $typeOfDocument = "Nota di credito"; break;
                case "TD05": $typeOfDocument = "Nota di debito"; break;
                case "TD06": $typeOfDocument = "Parcella"; break;
                default : $typeOfDocument = "&nbsp;"; break;
            }
        ?>
            &nbsp;<?= $typeOfDocument; ?>
        </td>
    <?php 
        $importedcausals = '';    
        foreach($bill['Billgestionaldata'] as $gestionalData)
        {
            if($gestionalData['importedcausals'] != null)
                $importedcausals .= $gestionalData['importedcausals'];
        }
    ?>
        <td style="<?= $left25 ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= $importedcausals ?></td>
        <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= isset($bill['Bill']['art73']) && strtoupper($bill['Bill']['art73']) == 'SI' ? $bill['Bill']['art73'] : '&nbsp;' ?></td>
        <td style="<?= $left15 ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= $bill['Bill']['numero_fattura'] ?></td>
        <td style="<?= $left10 ?>border-right: 1px solid #0f0f0f;">&nbsp;<?= date('d/m/Y',strtotime($bill['Bill']['date'])) ?></td>
        <td style="<?= $left15 ?>">&nbsp;<?= $bill['Bill']['destcode'] ?></td>
    </tr>
</table>

<div style="width:200mm;">&nbsp;</div>