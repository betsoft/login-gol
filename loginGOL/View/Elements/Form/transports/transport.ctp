     <div class="form-group col-md-12">
          <div class="col-md-2" style="float:left;">
              <label class="form-label form-margin-top"><strong>Numero colli</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('num_colli', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
           </div>
          <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Peso (kg)</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('weight', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
            </div>
           <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Volume (mc)</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('volume', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
            </div>
           <div class="col-md-2" style="float:left;margin-left:10px;">
              <label class="form-label form-margin-top"><strong>Data e ora di trasporto</strong></label>
               <div class="form-controls">
                <?= $this->Form->input('shipping_date', array('label' => false, 'div' => false, 'class'=>'form-control')); ?>
	           </div>
            </div>
   
         </div>

      <div class="form-group col-md-12">
        <?php
         if($this->request->action == 'add')
         {
        ?>
         <div class="col-md-2" style="float:left;">
        	<label class="form-label"><strong>Causale di trasporto</strong></label>
        		<div class="form-controls">
          	<?= $this->Form->select('causal_id', $causali, array('value' => 1,'class' => 'form-control')); ?>
       		</div>
      	</div>
        <?php
         }
             if($this->request->action == 'edit')
             {
         ?>
             <div class="col-md-2" style="float:left;">
        	    <label class="form-label"><strong>Causale di trasporto</strong></label>
        		<div class="form-controls">
          	        <?= $this->Form->select('causal_id', $causali, array('class' => 'form-control')); ?>
       		    </div>
      	    </div>
        <?php     
         } 
         ?>
          <div class="col-md-2" style="float:left;margin-left:5px;" >
              <label class="form-label form-margin-top"><strong>Aspetto esteriore dei beni</strong></label>
              <div class="form-controls">
              <?=  $this->Form->input('aspect', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => 'A Vista')); ?> <!-- Predefinito a vista -->
              </div>
          </div>

          <div class="col-md-2" style="float:left;margin-left:5px;" >
              <label class="form-label form-margin-top"><strong>Trasporto effettuato da</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('transporter', array('label' => false, 'div' => false, 'class' => 'form-control','type'=>'select', 'options'=>['0'=>'Mittente','1'=>'Destinatario','2'=>'Vettore'],'empty'=> true)); ?>
              </div>
          </div>

      
           <div class="col-md-2 vectorDiv" style="float:left;margin-left:5px;" hidden>
        	<label class="form-label"><strong>Vettore</strong></label>
        		<div class="form-controls">
          	<?= $this->Form->select('carrier_id', $carriers, array('value' => 1,'class' => 'form-control')); ?>
       		</div>
      	</div>
      </div>
      
     
      <?php isset($this->data['Bill']['carrier_id']) ? $carrierId = $this->data['Bill']['carrier_id'] : $carrierId = 'empty'; ?>
         <script>
      
        /** All'edit **/
        
        if($("#TransportTransporter").val() != 2)
              {
                  $(".vectorDiv").hide();
              }
              else
              {
                $(".vectorDiv").show();
                $("#BillCarrierId").val('<?= $carrierId ?>');
              }

       /** Al change **/
          $("#TransportTransporter").change(function()
          {
              if($(this).val() != 2)
              {
                $(".vectorDiv").hide();
              }
              else
              {
                $(".vectorDiv").show();
              }
          });
      </script>