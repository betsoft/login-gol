
<?= $this->Html->script	([ 'plugins/bootstrap/js/bootstrap.js']); ?>

<div class ="form-group clientdetails col-md-12">
	<div class="col-md-2" style="float:left">
		<label class="form-label form-margin-top"><strong>Indirizzo</strong></label>
   		<div class="form-controls">
			<?= $this->Form->input('client_address',['label' => false, 'div' =>false, 'class'=>'form-control']);?>
		</div>
	</div>
	
	<div class="col-md-2" style="float:left;margin-left:10px;">
		<label class="form-label form-margin-top"><strong>CAP</strong></label>
   		<div class="form-controls">
	<?=  $this->Form->input('client_cap',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5 ]); ?>
		</div>
	</div>
	
	<div class="col-md-2" style="float:left;margin-left:10px;">
		<label class="form-label form-margin-top"><strong>Città</strong></label>
   		<div class="form-controls">
		<?= $this->Form->input('client_city',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	
	<div class="col-md-2" style="float:left;margin-left:10px;">
		<label class="form-label form-margin-top"><strong>Provincia</strong></label>
   		<div class="form-controls">
		<?= $this->Form->input('client_province',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
		</div>
	</div>
	
	<div class="col-md-2" style="float:left;margin-left:10px;">
	<label class="form-label form-margin-top"><strong>Nazione</strong></label>
   <div class="form-controls">
		<?= $this->element('Form/Components/FilterableSelect/component', [
			"name" => 'client_nation',
			"aggregator" => '',
			"prefix" => "client_nation",
			"list" => $nations,
			"options" => [ 'multiple' => false,'required'=> false],
			]); 
	?>
   </div>
   </div>
  </div>