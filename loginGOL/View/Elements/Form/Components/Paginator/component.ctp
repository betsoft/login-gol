<?php
	$defaultElements = [
		'first' => __('prima'),
		'prev' => '< ',
		'numbers' => true,
		'next' => ' >',
		'last' => __('ultima'),
		'counter' => false//'Pagina {{page}} di {{pages}}'
	];

	$newElements = [];
	if(isset($elements))
	{
		foreach($elements as $key => $element)
		{
			//se non già settati, setto gli eventuali valori che sono chiave nell'array di default utilizzandone il rispettivo valore es. ['first'] o [0=>'first'] diventano ['first'=>$defaultElements['first']]
			if(isset($defaultElements[$element]))
				$newElements[$element] = $defaultElements[$element];
			else
				$newElements[$key] = $element;
		}
	}
	else 
		$newElements = $defaultElements;
		
	if(!isset($ajax))
		$ajax = true;
?>

<div class="paginator <?= $ajax ? 'paginator-ajax' : '' ?>">
	<ul class="pagination">
		<?php foreach($newElements as $key => $element): ?>
			<?php switch($key): 
				case "first": ?> 
					<?= $this->Paginator->first($element) ?>
				<?php break; ?> 
				<?php case "prev": ?> 
					<?= $this->Paginator->prev($element) ?>
				<?php break; ?> 
				<?php case "numbers": ?> 
					<?= $element !== false ? $this->Paginator->numbers() : '' ?>
				<?php break; ?> 
				<?php case "next": ?> 
					<?= $this->Paginator->next($element) ?>
				<?php break; ?> 
				<?php case "last": ?> 
					<?= $this->Paginator->last($element) ?>
				<?php break; ?> 
			<?php endswitch; ?>
		<?php endforeach; ?>
	</ul>
	<?php if(isset($newElements['counter']) && $newElements['counter']): ?>
		<p><?= $this->Paginator->counter($newElements['counter']) ?></p>
	<?php endif; ?>
</div>