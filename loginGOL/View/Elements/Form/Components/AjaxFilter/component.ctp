<?php /* if(!isset($bindFilterEvent)) { $bindFilterEvent = 'input'; } ?>

<?php foreach($elements as $element): ?>
	    <td>
			<?php if($element != null): ?>
	    		<?php
	    			echo $this->Form->input('filters.' . $element,array('type'=>'text', 'class'=>'form-control ajax-filter-input filter_'.$element, 'label'=>false, 'bind-filter-event'=>$bindFilterEvent)); 
	    		?>
			<?php endif; ?>
	    </td>
<?php endforeach; */ ?>


<?php
    /*
	$number
		lista degli elementi che comporranno le azioni del form
	*/
	
	if(!isset($bindFilterEvent)) 
		$bindFilterEvent = 'input';
		
	if(!isset($url)) 
		$url = null;

	if(!isset($beforeAjaxfilterCallback))
		$beforeAjaxfilterCallback = null;
		
	if(!isset($afterAjaxfilterCallback)) 
		$afterAjaxfilterCallback = null;
		
	if(!isset($ajaxFilterTableContentId))
		$ajaxFilterTableContentId = null;
		
	if(!isset($htmlElements))
		$htmlElements = [];
		
	if(!isset($shouldSetAjaxFiltersContainerClass))
		$shouldSetAjaxFiltersContainerClass = true;
?>


<?php isset($this->Session->read('arrayOfFilters')[$this->params['controller']][$this->action]) ? $arrayOfFilterValues = $this->Session->read('arrayOfFilters')[$this->params['controller']][$this->action] : $arrayOfFilterValues = null; ?>

<tr class="<?= $shouldSetAjaxFiltersContainerClass ? 'ajax-filters-container' : '' ?>" url="<?= $url ?>" before-ajax-filter-callback="<?= $beforeAjaxfilterCallback ?>" after-ajax-filter-callback="<?= $afterAjaxfilterCallback ?>" ajax-filter-table-content-id="<?= $ajaxFilterTableContentId ?>">
	<?php foreach($elements as $key => $element): ?>
		<?php if($element == "#actions"): ?>
			<td class="actions"></td>
		<?php else: ?>
		    <td>
				<?php if($element != null): ?>
		    		<?php if(isset($htmlElements[$htmlElementsIndex = str_replace(['#htmlElements[',']'], '', $element)])): ?>
						<?= $htmlElements[$htmlElementsIndex] ?>
		    			<?php unset($htmlElements[$htmlElementsIndex]); ?>
					<?php else: ?>
					<?php 
					if(isset($arrayOfFilterValues[$element]))
					{
		    			echo  $this->Form->input('filters.' . (is_numeric(trim($key)) ? $element : $key),['type'=>'text', 'class'=>'form-control ajax-filter-input filter_' . (is_numeric(trim($key)) ? $element : $key), 'label'=>false, 'bind-filter-event'=>$bindFilterEvent, 'value'=>$arrayOfFilterValues[$element]]); 
		    		}	
		    		else
		    		{
						echo  $this->Form->input('filters.' . (is_numeric(trim($key)) ? $element : $key),['type'=>'text', 'class'=>'form-control ajax-filter-input filter_' . (is_numeric(trim($key)) ? $element : $key), 'label'=>false, 'bind-filter-event'=>$bindFilterEvent]); 		    				
		    		}
		    		?>
					<?php endif; ?>
				<?php endif; ?>
		    </td>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php foreach($htmlElements as $key => $htmlElement): ?>
	    <td>
			<?= $htmlElement; ?>
	    </td>
	<?php endforeach; ?>
</tr>
