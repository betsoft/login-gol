<script>
	
	addLoadEvent(function() {
		initializeAjaxFilter()
	}, 'ajaxFilterLoader')
	
	function initializeAjaxFilter() {
		var filterInputs = $(".ajax-filter-input");
		filterInputs.each(function() {
			var filterInput = $(this)
			var event = filterInput.attr('bind-filter-event');
			
			filterInput.unbind(event + ".ajaxFilterLoader")
			filterInput.on(event + ".ajaxFilterLoader", function() {
				var filtersContainer = filterInput.parents('.ajax-filters-container').first()
				if(filtersContainer.attr('url'))
                {
                    var url = filtersContainer.attr('url');
                }
				else
                {
                    //var url = "<?=  Router::url( $this->here, true ); ?>";
                    var url = "<?=  Router::url( ['controller'=> $this->params['controller'],'action'=>$this->params['action']], true ); ?>";
                }
					
					
				if(filtersContainer.attr('ajax-filter-table-content-id'))
					var tableContent = $('#' + filtersContainer.attr('ajax-filter-table-content-id'))

				ajaxFilterPerform(url, filtersContainer, undefined, tableContent, filtersContainer.attr('before-ajax-filter-callback'), filtersContainer.attr('after-ajax-filter-callback'));
			});
		})
		
		try {
			//add ajax on paginator link's click
			initializeAjaxPaginator(function(link, href) {
				ajaxFilterPerform(href);
			});
		}
		catch(err) {
			console.warn(err)
		}
		
		try {
			//add ajax on sort link's click
			initializeAjaxSort(function(link, href) {
				ajaxFilterPerform(href);
			});
		}
		catch(err) {
			console.warn(err)
		}
	}
	
	function ajaxFilterGetParameters(filtersContainer) {
		if(filtersContainer)
			return filtersContainer.find('.ajax-filter-input').serialize();
		else
		    return $('.ajax-filter-input').serialize();
	} 
	
	var lastAjax = null;
	function ajaxFilterPerform(url, filtersContainer, pageContainer, tableContent, beforeAjaxFilterCallback, afterAjaxFilterCallback) {
		if(!beforeAjaxFilterCallback)
			beforeAjaxFilterCallback = 'beforeAjaxFilterCallback';
			
		if(!afterAjaxFilterCallback)
			afterAjaxFilterCallback = 'afterAjaxFilterCallback';
			
		pageContainer = pageContainer || (filtersContainer ? filtersContainer.parents('.page-content').first() : $('html'))
		tableContent = tableContent || $('.ajax-filter-table-content').attr('id','')

		if(lastAjax != null) {
			lastAjax.abort();
		}

		if(!isLoading)
			ajaxFilterStartLoading();
		
		if(!beforeAjaxFilterCallback)
	        beforeAjaxFilterCallback = window[beforeAjaxFilterCallback];
		if($.isFunction(beforeAjaxFilterCallback)) {
	        try {
		        beforeAjaxFilterCallback(url)
            }
            catch(err) {
                console.warn(err)
            }
        }
		
		lastAjax = $.ajax({
			url: url,
			type: "POST",
			data: ajaxFilterGetParameters(filtersContainer),
			success:function(data) {
				if(tableContent.attr('id'))
					tableContent.html($(data).find("#" + tableContent.attr('id')).html());
				else
					tableContent.html($(data).find(".ajax-filter-table-content").html());
					
				pageContainer.find(".ajax-sort").parents('tr').html($(data).find(".ajax-sort").parents('tr').html());
				pageContainer.find(".paginator").html($(data).find(".paginator").html());
				
				try {
					initializeAjaxPaginator(function(link, href) {
	        			ajaxFilterPerform(href);
	        		});
				}
				catch(err) {
					console.warn(err)
				}
				
        		try {
					initializeAjaxSort(function(link, href) {
						ajaxFilterPerform(href);
					});
				}
				catch(err) {
					console.warn(err)
				}
		        		
				ajaxFilterStopLoading();
				
				afterAjaxFilterCallback = window[afterAjaxFilterCallback];
				if($.isFunction(afterAjaxFilterCallback)) {
			        try {
				        afterAjaxFilterCallback(data)
		            }
		            catch(err) {
		                console.warn(err)
		            }
		        }
			},
			error: function(request, error)  {},
			complete: function(request, error) {
				lastAjax = null;
			}
		});
	}
	
	var isLoading = false;
	function ajaxFilterStartLoading(parentSelector) {
		parentSelector = parentSelector || 'table';
		
		isLoading = true;
		$('.ajax-filter-loading-container').remove();
		
		//var loading = $('<?= $this->Html->image("../metronic/assets/global/img/loading.gif", ['class'=>'ajax-filter-loading-image','alt'=>'loading']) ?>');
		var loading = $('<?= addslashes(str_replace(["\n","\r"], '', $this->element('Form/Components/AjaxFilter/animation'))) ?>');
		var loadingContainer = $("<div>", {"class":"ajax-filter-loading-container"});
		
		loadingContainer.append(loading);
		
		$(".ajax-filter-table-content").parents(parentSelector).first().css('position','relative').append(loadingContainer);
		$(".ajax-filter-table-content").addClass('ajax-filter-loading-background');
	}
	
	function ajaxFilterStopLoading() {
		$('.ajax-filter-loading-container').remove();
		$(".ajax-filter-table-content").removeClass('ajax-filter-loading-background');
		isLoading = false;
	}

</script>

<?= $this->Html->css('AjaxFilter/default') ?>
<?= $this->Html->css('AjaxFilter/custom') ?>