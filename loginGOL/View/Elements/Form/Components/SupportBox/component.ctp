<div class="contact support-box-toggler menu-item" style="text-align: center">
    <i><?= __('Assistenza') ?></i>
</div>
<div id="support_box" class="form large-9 medium-8 columns content hidden" style="width:0px">
	<div class="portlet box">
		<i class="fa fa-close support-box-toggler close-icon hidden"></i>
		<div class="portlet-title">
			<div class="caption" style="width:300px">
				<?= __('Richiesta di assistenza') ?>
			</div>
		</div>
		<?= $this->Form->create('support',['url' => ['controller'=>'Utilities', 'action' => 'support'], 'class'=>'form-horizontal']) ?>
		    <div class="portlet-body form">
            	<fieldset>			
            		<div class="form-body">
            			<div class="form-group">
            				<div class="col-md-12">
                                <div class="input-icon right">
            					    <?= $this->Form->textarea('mail_text',['placeholder'=>__('Inserisci qui la tua richiesta...'), 'class'=>'form-control', 'label'=>false]); ?>
									<div class="success hidden" style="opacity:0">
										<i class="fa fa-check-circle-o"></i>
								    </div>
                                </div>
	    	    				<?= $this->Form->button(__('Invia'),['class'=>'btn save-color send-mail']); ?>
                            </div>
            			</div>
                    </div>
            	</fieldset>
            </div> 
            <center>
            </center>
		<?= $this->Form->end() ?>
	</div>
</div>

