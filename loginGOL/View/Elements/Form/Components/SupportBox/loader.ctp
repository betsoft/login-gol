<style>
    #support_box {
        position: fixed;
        bottom: 0;
        right: 0;
        width: 300px;
        height: 400px;
        border-right: 0px;
        border-bottom: 0px;
        background-color: #fff;

        border-top-left-radius: 10px !important;
        box-shadow: 0px 3px 3px #666;
    }

    #support_box > .portlet.box {
        width: 300px;
    }

    #support_box textarea {
        height: 265px;
        resize: none;
    }

    #support_box .portlet.box>.portlet-title {
        background: #589AB8;
        border-top-left-radius: 10px !important;
    }

    #support_box .portlet.box>.portlet-title>.caption {
        color: #fff;
    }

    .support-box-toggler.menu-item.contact {
        position: fixed;
        width: 100px;
        height: 40px;
        right: 0;
        top: 0px;
        padding: 0;
        margin-right: -60px;
        margin-top: 100px;
        line-height: 32px;

        background-color: #f9f9f9;
        border-left: 7px solid #589AB8;
        cursor: pointer;

        border-bottom-right-radius: 10px !important;
        border-bottom-left-radius: 10px !important;
        border-top-left-radius: 0px !important;

        transform: rotate(90deg);
        transform-origin: bottom left;
        -o-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform-origin: bottom left;
        -moz-transform-origin: bottom left;
        -webkit-transform-origin: bottom left;
        -ms-transform-origin: bottom left;
        -webkit-box-shadow: 1px 3px 3px #999;
        box-shadow: 1px 3px 3px #999;
    }

    .backend-layout .support-box-toggler.menu-item.contact {
        margin-top: 255px;
    }

    .frontend-reserved-layout .support-box-toggler.menu-item.contact {
        margin-top: 670px;
    }

    .frontend-layout .support-box-toggler.menu-item.contact {
        margin-top: 535px;
    }

    .support-box-toggler.close-icon {
        position: absolute;
        right: 11px;
        top: 11px;
        font-size: 20px;
        color: #fff;
    }

    .support-box-toggler.close-icon:hover {
        right: 10.5px;
        top: 10.5px;
        font-size: 22px;
    }

    #support_box .success {
        position: absolute;
        top:0px;
        left:0px;
        right:0px;
        width:100%;
        height:100%;
        text-align: center;
        display:flex;
        justify-content:center;
        align-items:center;
        background-color: #fff;
        color: green;
        cursor: pointer;
    }

    #support_box .success i {
        font-size: 130px;
        color: #589AB8;
    }

    #support_box .send-mail {
        margin-top: 10px;
    }
</style>

<script>
    addLoadEvent(function() {
        $('.support-box-toggler').click(function() {
            var supportBox = $('#support_box');
            var supportBoxTogglerCloseIcon = $('.support-box-toggler.close-icon');

            var visible = !supportBox.hasClass('hidden');
            if(visible) {
                supportBoxTogglerCloseIcon.fadeOut(200, function() {
                    supportBox.animate({width: '0px'}, 500, function() {$(this).addClass('hidden')});
                })
            }
            else {
                supportBox.removeClass('hidden').animate({width: '300px'}, function() {
                    supportBoxTogglerCloseIcon.removeClass('hidden')
                    supportBoxTogglerCloseIcon.fadeIn(0)
                });
            }
        })

        $('#support_box form').submit(function(e) {
            e.preventDefault();

            var supportBox = $('#support_box');
            $.ajax({
	            url: "<?= $this->Html->url([ 'controller'=>'Utilities', 'action' => 'support'],true) ?>",
	            method: $(this).attr('method'),
	            data: $( this ).serialize(),
	            success: function(data)
                {
	                if(data != 'false')
	                {
                        supportBox.find('.success').removeClass('hidden');
                        supportBox.find('.success').animate({opacity: '1'}, 500,  function() {
                            setTimeout(function() {
                                supportBox.find('.success').click();
                            }, 2000);
                        });
                    }

	                supportBox.find('textarea').val('');
	            },
	            error: function(err) {},
	        });
        })

        $('.success').click(function() {
            $(this).animate({opacity: '0'}, 500,  function() {
                $(this).addClass('hidden')
            });
        })
    })
</script>
