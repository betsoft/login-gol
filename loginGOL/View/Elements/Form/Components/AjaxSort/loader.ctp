<script>
	
    /*addLoadEvent(function() {
        initializeAjaxSort(function() {
            console.warn('No ajax callback set for sort element');
            return false;
        })
    }, 'ajaxSortLoader');*/
    
    function initializeAjaxSort(callback) {
        $('.ajax-sort a').on('click.AjaxSort', function() {
        	var link = $(this);
			var href = link.attr('href');

            if($.isFunction(callback)) {
		        try {
                    callback(link, href)
                }
                catch(err) {
                    console.warn(err)
                }
	        }
            return false;
		});
    }
</script>

<?= $this->Html->css('AjaxSort/default') ?>
<?= $this->Html->css('AjaxSort/custom') ?>