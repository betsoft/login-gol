<?php
    /*
	$elements:
		lista degli elementi che comporranno le azioni del form
	$type:
		la tipologia di template da utilizzare per l'elemento corrente
	$template:
		array dei nomi degli elementi che verrenno visualizzati all'interno dell'elemento corrente, rispettandone l'ordine
	$class:
		classe/i dell'elemento più esterno
	*/
	
	if(!isset($ajaxCall))
		$ajaxCall = false;
			
	if(!isset($nowrap))		
		$nowrap = false; 
			
	if(!isset($passedValue))
	 $passedValue = null;
			
	if(!isset($template))
	{
		if(!isset($type))
			$type = "";

		if($type == "advanced")
			$template = ["save", "saveAndNew", "cancel", "saveAndCloseAjax", "cancelAjax"];
		
		else if($type == "cancel")
			$template = ["cancel", "cancelAjax"];
		
		else if($type == "back")
			$template = ["back", "backAjax"];
			
		else if($type == "blank")
			$template = [];
		
		else // default
			$template = ["save", "cancel", "saveAndCloseAjax", "cancelAjax","saveAjax"];
	}

	if(!isset($elements))
		$elements = [];

	if(!$ajaxCall && !$AJAX) // $AJAX globale, usare il parametro $ajaxCall per sovrascriverlo
	{
		foreach($template as $key => $item)
		{
			if($passedValue)
			{
				if($item === "save")
				    $elements[] = $this->Form->button(__('Salva'),array('onClick' => 'alert("Stop")','name'=>'redirect', 'value' => $passedValue, 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
				else if($key === "save")
				    $elements[] = $this->Form->button(__($item),array('onClick' => 'alert("Test")','name'=>'redirect', 'value' =>  $passedValue, 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
			}
			else
			{
				if($item === "save")
				    $elements[] = $this->Form->button(__('Salva'),array('onClick' => 'alert("Test")','name'=>'redirect', 'value' => $redirect, 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
				else if($key === "save")
				    $elements[] = $this->Form->button(__($item),array('onClick' => 'alert("Test")','name'=>'redirect', 'value' =>  $redirect, 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
			}

			if($item === "cancel")
			    $elements[] = $this->Html->link(__('Annulla'), ['action' => $redirect,$passedValue],array('class'=>'btn blue-button-reverse new-bill cancel', 'escape' =>false,'style'=>'background-color:#dadfe0;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
			else if($key === "cancel")
			    $elements[] = $this->Html->link(__($item), ['action' =>$redirect, $passedValue],array('class'=>'btn blue-button-reverse new-billcolor', 'escape' =>false,'style'=>'background-color:#dadfe0;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));
		}
	}
	else // Caricato da un ajax
	{
		foreach($template as $key => $item)
		{
			$redirect = '<input type="hidden" name="redirect" value="ajax"></input>';
			
			if($item === "saveAjax")
				$elements[] ='<span name="redirect" value="ajax" class="btn btn blue-button save-color save-button">Salva</span>'.$redirect;
			else if($key === "saveAjax")
				$elements[] ='<span name="redirect" value="ajax" class="btn btn blue-button save-color save-button">'.__($item).'</span>'.$redirect;

		    if($item === "cancelAjax")
			    $elements[] = '<span name="redirect" value="ajax" class="btn btn blue-button cancel-color cancel-button" data-dismiss="modal">Annulla</span>'.$redirect;
			else if($key === "cancelAjax")
			    $elements[] = '<span name="redirect" value="ajax" class="btn btn blue-button cancel-color cancel-button" data-dismiss="modal">'.__($item).'</span>'.$redirect;

		}
	}


	if(!isset($class)) 
	{
		$class = "row" . ($ajaxCall ? " ajax-actions" : "");
	}
?>

<?php if (!$nowrap): ?>
	<div <?= $class != '' ? 'class="'.$class.'"' : '' ?>>
		<div class="form-group col-md-12">
			<div class="form-actions">
				<div class="col-md-offset-0">
			    <?php foreach($elements as $element): ?>
			        <?= $element ?>
				<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php else: ?>
  <?php foreach($elements as $element): ?>
       <?= $element ?>
	<?php endforeach; ?>
<?php endif; ?>
