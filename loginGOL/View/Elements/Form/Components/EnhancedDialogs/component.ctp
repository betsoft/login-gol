<?php
	/*
	$title:
		titolo dell'alert
	$message:
		messaggio dell'alert
	*/
	
	if(!isset($title))
		$title = "";
	
	if(!isset($titleIconClass))
		$titleIconClass = "fa fa-info";
			
	if(!isset($message))
		$message = ""; 
		
	if(!isset($help))
		$message = ""; 
?>

<div class="enhanced-dialog-container enhanced-dialog-fade">
	<div class="enhanced-dialog" id="enhanced-dialog">
		<div class="enhanced-dialog-title">
			<span class="enhanced-dialog-title-icon"><i class="<?php // $titleIconClass ?>"></i></span>
			<span class="enhanced-dialog-title-text"><?= $title ?></span>
		</div>
		<div class="enhanced-dialog-message">
			<label class="enhanced-dialog-message-text">
				<?= $message ?>
			</label>
		</div>
		<div class="actions enhanced-dialog-buttons">
			<button class="btn btn-lg enhanced-dialog-button-confirm save-color hidden" enhanced-dialog-button-type="0">Ok</button>
			<button class="btn btn-lg enhanced-dialog-button-yes save-color hidden" enhanced-dialog-button-type="1">Si</button>
			<button class="btn btn-lg enhanced-dialog-button-no cancel-color hidden" enhanced-dialog-button-type="2">No</button>
			<button class="btn btn-lg enhanced-dialog-button-yes save-color hidden" enhanced-dialog-button-type="5">Salva</button>
			<button class="btn btn-lg enhanced-dialog-button-cancel color hidden" enhanced-dialog-button-type="3">Annulla</button>
			<button class="btn btn-lg enhanced-dialog-button-close color hidden" enhanced-dialog-button-type="4">Chiudi</button>
		</div>
	</div>
</div>