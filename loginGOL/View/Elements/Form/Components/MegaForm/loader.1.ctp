<script>
    addLoadEvent(function() {
        initializeMegaForm();
    }, 'megaFormLoader');

    if(!megaFormLoadedUrls)
        var megaFormLoadedUrls = {}
    
    function initializeMegaForm(afterAjaxCallbackFunction, ids, beforeAjaxCallbackFunction, beforePerformShowOrHideCallbackFunction, forceAjaxReload, sender) {
        $('.mega-form-button').each(function() {
            var megaFormButton = $(this);
            var megaFormHref = megaFormButton.attr('href');
            var megaForm = $(megaFormHref);
            
            if(megaForm.attr('id') in megaFormLoadedUrls)
                return;
                
            sender = sender === undefined ? $('.mega-form-button[href="' + megaFormButton.prop('hash') + '"]') : sender
            
            var megaFormRefreshIcon = megaForm.find('.mega-form-refresh:not(.mega.form .mega-form-refresh)').first();
            var megaFormCloseIcon = megaForm.find('.mega-form-close:not(.mega.form .mega-form-close)').first();

            var outerMegaFormHref = megaForm.attr('outer-mega-form-href');
            var url = megaForm.attr('url');

            //callbacks initialization
            var beforeAjaxCallback = megaForm.attr('before-ajax-callback');
            var afterAjaxCallback = megaForm.attr('after-ajax-callback');
            var afterSaveCallback = megaForm.attr('after-save-callback');
            var afterCancelCallback = megaForm.attr('after-cancel-callback');
            var initializedInnerMegaFormCallback = megaForm.attr('initialized-inner-mega-form-callback');
            var beforePerformShowOrHideCallback = megaForm.attr('before-perform-show-or-hide-callback');

            var megaFormAjaxEnded = false;

            //TO BE REMOVED, IF STILL BUGGY
            //initializedInnerMegaFormCallback = beforePerformShowOrHideCallback;
            
            var megaFormHeader = megaForm.find('.modal-header');
            var megaFormDataContainer = megaForm.find('.mega-form-data-container');
            
            if(ids == undefined || ids.length == 0 || $.inArray(megaForm.attr('id'), ids) != -1) {
                megaForm.unbind('show.bs.modal.megaFormLoader')
                megaForm.on('show.bs.modal.megaFormLoader', function() {
                    megaFormButton.addClass('no-pointer-events')
                    megaForm.addClass('loading')
                })
                    
                megaForm.unbind('shown.bs.modal.megaFormLoader')
                megaForm.on('shown.bs.modal.megaFormLoader', function() {
                    megaFormButton.addClass('no-pointer-events')

                    megaFormRemoveInnerMegaForms(megaFormDataContainer)
                    megaFormInitializeInnerMegaForms(megaFormDataContainer, initializedInnerMegaFormCallback)
                    
                    if(megaFormAjaxEnded) {
                        if($.isFunction(beforePerformShowOrHideCallbackFunction)) {
                            beforePerformShowOrHideCallbackFunction(megaFormButton, megaForm);
                        } else if($.isFunction(window[beforePerformShowOrHideCallback])) {
                            beforePerformShowOrHideCallbackFunction = window[beforePerformShowOrHideCallback];
                            beforePerformShowOrHideCallbackFunction(megaFormButton, megaForm);
                        }
                    }

                    focusFirstInput(megaForm)

                    megaForm.removeClass('loading')
                })
                
    			var saveButton;
    		    var cancelButton;
    			
                var params = {
                    method: "GET",
                    data: null,
                    url: url
                }
                
                if($.isFunction(beforeAjaxCallbackFunction)) {
                    var params = beforeAjaxCallbackFunction(params)
                }
    			
                if($.isFunction(window[beforeAjaxCallback])) {
                    beforeAjaxCallbackFunction = window[beforeAjaxCallback];
                    var params = beforeAjaxCallbackFunction(params)
                }
                
    			megaFormDataContainer.html('');

    			megaFormLoadedUrls[megaForm.attr('id')] = url;

                var ajax = $.ajax({
                    url: params.url,
                    async: true,
        			method: params.method,
        			data: params.data,
                    success: function(data) {
                        var htmlContent = $.parseHTML(data, document, true);
                        megaFormDataContainer.html(htmlContent);

                        var breadcumbs = buildBreadcrumb(megaFormHref);
                        megaFormHeader.find('.mega-form-breadcrumbs').html('').prepend(breadcumbs);
                        
                     //   megaFormRemoveInnerMegaForms(megaFormDataContainer)
                     //    megaFormInitializeInnerMegaForms(megaFormDataContainer, initializedInnerMegaFormCallback)
            			
                        if($.isFunction(afterAjaxCallbackFunction)) {
                            afterAjaxCallbackFunction(htmlContent, megaFormButton, megaForm)
                        } else if($.isFunction(window[afterAjaxCallback])) {
                            afterAjaxCallbackFunction = window[afterAjaxCallback];
                            afterAjaxCallbackFunction(htmlContent, megaFormButton, megaForm)
                        }

                        saveButton = megaFormDataContainer.find(".save-button");
    		            cancelButton = megaFormDataContainer.find(".cancel-button");
    		            
    		            megaForm.unbind('click.megaFormLoader')
    		            megaForm.on('click.megaFormLoader', function() {
    		                if(megaForm.attr('outer-mega-form-href'))
        		            	$(megaForm.attr('outer-mega-form-href')).find('.mega-form-button').removeClass('no-pointer-events')
                            else
        		            	megaForm.parents('.page-content').first().find('.mega-form-button').removeClass('no-pointer-events')
    		            })
    		            
    		            megaFormRefreshIcon.unbind('click.megaFormLoader')
    		            megaFormRefreshIcon.on('click.megaFormLoader', function() {
                    	    megaForm.trigger('show.bs.modal.megaFormLoader')
    		                megaFormRefreshIcon.animateRotate(720, {
    		                    duration: 1500,
    		                    complete: function() {
                    	            megaForm.trigger('shown.bs.modal.megaFormLoader')
    		                    }
    		                })
    		            })
    		            
    		            megaFormCloseIcon.unbind('click.megaFormLoader')
    		            megaFormCloseIcon.on('click.megaFormLoader', function() {
    		                megaFormClose(megaFormButton, megaForm)
    		            })
    		                		            
    		            cancelButton.unbind('click.megaFormLoader')
    		            cancelButton.on('click.megaFormLoader', function() {
    		                var cancelButton = $(this);
                		    var afterCancelCallbackFunction = window[afterCancelCallback];
                		    
            		        if($.isFunction(afterCancelCallbackFunction)) {
                		        try {
                    		        afterCancelCallbackFunction(cancelButton, megaFormButton, megaForm)
                                }
                                catch(err) {
                                    console.warn(err)
                                }
            		        }
            		        
                            if(megaForm.attr('outer-mega-form-href'))
        		            	$(megaForm.attr('outer-mega-form-href')).find('.mega-form-button').removeClass('no-pointer-events')
                            else
        		            	megaForm.parents('.page-content').first().find('.mega-form-button').removeClass('no-pointer-events')   
        		            	
                            focusFirstInput($(megaForm.attr('outer-mega-form-href')))

                            setTimeout(function() {
                                $(megaForm.attr('outer-mega-form-href')).find('.full-calendar-page-scroller.portlet-body').scrollTop(0)
        		                $('.full-calendar-page-scroller.portlet-body').scrollTop(0)
                            }, 1000)
    		            });
                		
                        saveButton.unbind('click.megaFormLoader')
                        saveButton.on('click.megaFormLoader', function() {
                            var saveButton = $(this);
            		        var form = saveButton.parents("form").first();
            		        
                		    var formData = new FormData(form[0]);
                		    var formValidity = form[0].reportValidity();
                            
                            formData.append('submit', true);
                		    
                            if(megaForm.attr('outer-mega-form-href'))
        		            	$(megaForm.attr('outer-mega-form-href')).find('.mega-form-button').removeClass('no-pointer-events')
                            else
        		            	megaForm.parents('.page-content').first().find('.mega-form-button').removeClass('no-pointer-events')
        		            	
                		    if(formValidity) {
                                megaForm.addClass('loading')
                                $.ajax({
                                    url: form.attr('action'),
                                    async: true,
                                    method:'POST',
                                    data: formData,
                                    contentType: false,
                                    processData: false,
                                    success: function(data) { 
                                        try {
                                            var	data = JSON.parse(data)
                                            var entityData = data.entityData
                                            var flashMessage = data.flashMessage
                                        }
                                        catch(ex) {
                                            console.warn("Could not convert JSON data in MegaForm:initialize() -> afterSaveCallbackFunction")
                                            var entityData = undefined;
                                            var flashMessage = undefined;
                                        }
                                        
                        		        var afterSaveCallbackFunction = window[afterSaveCallback];
                        		        if($.isFunction(afterSaveCallbackFunction)) {
                            		        try {
                                		        afterSaveCallbackFunction(entityData, saveButton, megaFormButton, megaForm, sender)
                                		        
                                		        if(megaForm.hasClass('in')) {
                                            	    megaForm.trigger('show.bs.modal.megaFormLoader')
                                            	    megaForm.trigger('shown.bs.modal.megaFormLoader')
                                            	}
                                            }
                                            catch(err) {
                                                console.warn(err)
                                            }
                        		        }
                        		        else
                        		            console.warn('No function ' + afterSaveCallback + ' found')
                        		            
                                        //set flash
                                        //$('#flash-container').first().append($(flashMessage).find('.flash'))
                                        $('#flash-container').first().append($(flashMessage))
                                        console.log(flashMessage);
                                        //initializeFlashMessage()
                                        
                                        focusFirstInput($(megaForm.attr('outer-mega-form-href')))
                                        
                                        if(saveButton.attr('modal-close-on-click') === undefined)
                                            var closeOnClick = true;
                                        else
                                            var closeOnClick = saveButton.attr('modal-close-on-click') !== "false";
                                        
                                        if(closeOnClick && $(flashMessage).find('.flash-error').length == 0) {
                        		            cancelButton.click()
                        		            
                        		            //clear form
                                            form.trigger('reset');
                                        }
                                    },
                                    error: function(data) {
                                    	console.warn('Unable to save entity');
                                    },
            						complete: function(request, error) {
                                        megaForm.removeClass('loading')
                                    }
                                });
                		    }
            		        return false;
                    	});
                    	
                    	megaFormAjaxEnded = true
                    	if(megaForm.hasClass('in')) {
                    	    megaForm.trigger('show.bs.modal.megaFormLoader')
                    	    megaForm.trigger('shown.bs.modal.megaFormLoader')
                    	}
                    },
                    error: function(data) {
                    }
                });
            }
        });
        
        megaFormLoadedUrls = {}
    }
    
    function megaFormHideContent(megaForm) {
        megaForm.find('.mega-form-data-container').first().css('opacity','0')
    }
    
    function megaFormShowContent(megaForm) {
        megaForm.find('.mega-form-data-container').first().css('opacity','1')
    }
    
    function megaFormClose(megaFormButton, megaForm) {
        if(megaForm.hasClass('in'))
            megaFormButton.click();
    }
    
    function megaFormOpen(megaFormButton, megaForm) {
        if(!megaForm.hasClass('in'))
            megaFormButton.click();
    }
    
    var innerMegaFormsIds = []
    function megaFormRemoveInnerMegaForms(innerMegaFormButtonHref) {
        var innerMegaFormsIds = [];
        $('.mega-form:not(.mega-form .mega-form)').first().nextAll('.mega-form[outer-mega-form-href]').each(function() {
            if($.inArray($(this).attr('id'), innerMegaFormsIds) == -1)
                innerMegaFormsIds.push($(this).attr('id'))
            else
                $(this).remove();
        })
    }
    
    function megaFormInitializeInnerMegaForms(megaFormDataContainer, initializedInnerMegaFormCallback) {
        var megaForm = megaFormDataContainer.parents('.mega-form').first()
        var megaFormButton = $('[href="#' + megaForm.attr('id') + '"]')
        
        megaFormDataContainer.find('.mega-form-button:not(.mega-form .mega-form .mega-form-button)').each(function() {
            var innerMegaFormButton = $(this);
            var innerMegaFormButtonHref = innerMegaFormButton.attr('href');
            var innerMegaForm = megaFormDataContainer.find(innerMegaFormButtonHref).first();
            var innerMegaFormId = innerMegaForm.attr('id');
            
            if(innerMegaForm.attr('initialized-inner-mega-form-callback') == undefined)
                innerMegaForm.attr('initialized-inner-mega-form-callback', initializedInnerMegaFormCallback)

            var outerMegaFormHref = '#' + innerMegaForm.parents('.mega-form').first().attr('id');
            if(outerMegaFormHref != undefined)
                innerMegaForm.attr('outer-mega-form-href', outerMegaFormHref);

            if(innerMegaForm[0]) {
                var innerMegaFormHtml = innerMegaForm[0].outerHTML;
                innerMegaForm.remove();
                
                $(outerMegaFormHref).last().after(innerMegaFormHtml);
            }
            
    	    var initializedInnerMegaFormCallbackFunction = window[initializedInnerMegaFormCallback];
            if($.isFunction(initializedInnerMegaFormCallbackFunction)) {
    	        try {
    		        initializeMegaForm(function() {
    	                initializedInnerMegaFormCallbackFunction(innerMegaFormButton, innerMegaForm)
    	            }, [innerMegaFormId]);
                }
                catch(err) {
                    console.warn(err)
                }
            }
            else {
                initializeMegaForm(null, [innerMegaFormId]);
            }
    	})
    }
    
    function buildBreadcrumb(megaFormHref) {
        var breadcrumb = $('<ol>').addClass('breadcrumb');
        var breadcrumbsArray = getBreadcrumbArray(megaFormHref);

        for(var key in breadcrumbsArray) {
            var firstElement = key == 0;
            var megaForm = $(breadcrumbsArray[key]);
            var megaFormTitle = megaForm.find('.portlet-title').first()
            var megaFormTitleText = firstElement && megaForm.attr('base-mega-form-name') !== undefined ? megaForm.attr('base-mega-form-name') : megaFormTitle.find('.caption').first().text();

            var link = $('<a>')
                .text(megaFormTitleText)
                .attr('href', breadcrumbsArray[key])
                .unbind('click.buildBreadcrumb')
                .on('click.buildBreadcrumb', function() {
                    $(this).parents('li').first().nextAll('li').each(function() {
                        var href = $(this).find('a').attr('href')
                        var megaForm = $(href);
                        var megaFormButton = $("#" + megaForm.attr('id'));
                        
                        megaFormClose(megaFormButton, megaForm);
                    }) 
                    return false;
                })
                
            var li = $('<li>').append(link);
            breadcrumb.append(li);
        }
        breadcrumb.find('li').last().addClass('active');
        return breadcrumb;
    }
    
    function getBreadcrumbArray(megaFormHref) {
        var breadcrumb = [];
        var megaForm = $(megaFormHref);
        while(megaForm !== undefined) {
            breadcrumb.unshift('#' + megaForm.attr('id'));
            if(megaForm.attr('outer-mega-form-href') === undefined) {
                if(megaForm.attr('base-mega-form-href') !== undefined) {
                    breadcrumb.unshift('#' + megaForm.attr('base-mega-form-href'));
                }
                break;
            }
            
            megaForm = $(megaForm.attr('outer-mega-form-href')).first();
        }
        return breadcrumb
    }
    
    function focusFirstInput(megaForm) {
        if(megaForm.attr('should-focus-first-input')) {
            var inputs = megaForm.find('input.form-control')
            var input = inputs.first()
    
            setTimeout(function() {
                var temp = input.focus().val(); 
                input.val('').val(temp);
                megaForm.find('.form-body').first().scrollTop(0)
            }, 0)
        }
    }
</script>

<?= $this->Html->css('MegaForm/default') ?>
<?= $this->Html->css('MegaForm/custom') ?>

