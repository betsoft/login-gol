
<div class="col-md-12 " style="margin-top:20px;">
  <div class="form-group">
      <?php
      $currentAction = $this->request->params['action'] ;
      $currentController =  $this->request->params['controller'] ;
      // Aggiunta subtotale solo per modulo cantieri
      if(($currentAction == 'add' || $currentAction == 'edit' || $currentAction == 'editChange' || $currentAction == 'editAdd') &&  $currentController == 'quotes' && MODULO_CANTIERI)
      { ?>
          <a id="aggiungi_subtotale_bottom" href="javascript:;" style="float:right;">
        <span class="blue-button btn-outline dropdown-toggle "
              style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                <?= 'Aggiungi subtotale' ?>
            </span>
          </a>
          <a id="aggiungi_opt_bottom" href="javascript:;" style="float:right;">
          <span class="blue-button btn-outline dropdown-toggle "
                style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                <?= 'Aggiungi sezione opzionale' ?>
            </span>
          </a>
      <?php } ?>
    <?php if ($currentAction != 'indexdepositmovement' && $currentAction != 'addExtendedbuy' && $currentAction != 'editExtendedBuy' && !($currentController == 'loadgoods' && $currentAction== 'add')
    && !($currentController == 'loadgoods' && $currentAction == 'edit')) {   ?>
        <?php if ($currentController != 'payments' && $currentController != 'maintenances'  && $currentController != 'purchaseorders' ) { ?>
    <a id="aggiungi_nota_bottom" href="javascript:;"  style="float:right;" >
        <span class = "blue-button btn-outline dropdown-toggle " style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                <?= 'Aggiungi nota' ?>
        </span>
    </a>
    <?php } ?>
    <?php } ?>
    <a id="aggiungi_riga_bottom" href="javascript:;"  style="float:right;">
        <span class = "blue-button btn-outline dropdown-toggle add_row_clonable_2" style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                <?php
                    if(isset($buttonTitle))
                    {
                        echo  'Aggiungi articolo da trasferire' ;
                    }
                    else
                    {
                        if($currentController == 'payments')
                        {
                            echo 'Aggiungi scadenza';
                        }
                        else
                        {
                            echo 'Aggiungi riga';
                        }
                    }
                ?>
                <?php // isset($buttonTitle) ? 'Aggiungi articolo da trasferire'  :  'Aggiungi riga'; ?>
        </span>
    </a>
    </div>
</div>

<script>
    $("#aggiungi_riga_bottom").click(
        function() { $("#aggiungi_riga").click();
        });
    
    $("#aggiungi_nota_bottom").click(
        function() {
            $("#aggiungi_nota").click();
        })

    $("#aggiungi_subtotale_bottom").click(
        function() {
            $("#aggiungi_subtotale").click();
        })

    $("#aggiungi_opt_bottom").click(
        function() {
            $("#aggiungi_opt").click();
        })
</script>
