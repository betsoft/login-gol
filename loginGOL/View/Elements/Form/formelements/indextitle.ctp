<?= $this->element('Form/Components/AjaxFilter/loader'); ?>

<div class="portlet-title">
	<div class="caption">
        <span class="caption-subject bold uppercase" style="color:#ea5d0b;font-size:16px;"><?= __($indextitle); ?></span>
        <?php
            if(MULTICSV)
            {
                if($this->params['controller'] == 'storages' && $this->params['action'] == 'index')
                {
                    echo $iconaExcelCsvComma;
                }
            }
        ?>
        <?php
            //$this->params['controller'] != 'constructionsites' &&
            if($this->params['controller'] != 'maintenances')
                echo $iconaExcel;
        ?>
        <?php
            if($this->params['controller'] == 'deadlines' && $this->params['action'] == 'indexscadatt')
            {
                echo $iconaExcelCsvComma;
            }
        ?>
        <?php
        if($this->params['controller'] == 'deadlines' && $this->params['action'] == 'indexscadpass')
        {
            echo $iconaExcelCsvComma;
        }
        ?>
        <div style="min-height:54px;margin-right:20px;backgorund-color:#589ab8;float:right;">
			<?php
            $buttonStyleBlue = 'text-transform:none;margin-left:5px;padding:10px !important;color:#555555 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b';
			$buttonStyleOrange = 'text-transform:none;margin-left:5px;padding:10px !important;color:#555555 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b';
            if(isset($indexelements))
            {
                foreach ($indexelements as $key => $element)
                {
                    switch ($key)
                    {
                        // Serve per iportare mostrare la possibilità di importare una fattura elettronica xml
                        case 'importebill':
                            if (true)
                            {
                                echo $this->Html->link('Importazione fatture elettroniche', ['action' => 'ebillimport'], ['title' => __('Importazioni fatture elettroniche'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                            }
                        break;
                        case 'resumecategorizesell':
                            if (MODULO_CONTI)
                            {
                                echo $this->Html->link('Riepilogo registrazioni (vendite)', ['controller' => 'bills', 'action' => 'resumecategorizesell'], ['title' => __('Genera riepilogo registrazioni (acquisti)'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                            }
                        break;
                        case 'resumecategorizebuy':
                            if (MODULO_CONTI)
                            {
                                echo $this->Html->link('Riepilogo registrazioni (acquisti)', ['controller' => 'bills', 'action' => 'resumecategorizebuy'], ['title' => __('Genera riepilogo registrazioni (vendite)'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                            }
                        break;
                        case 'resumevatsell':
                            echo $this->Html->link('Genera riepilogo iva', ['controller' => 'resumevats', 'action' => 'ResumeVatSell'], ['title' => __('Genera riepilogo iva'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                        break;
                        case 'resumevatbuy':
                            echo $this->Html->link('Genera riepilogo iva', ['controller' => 'resumevats', 'action' => 'ResumeVatBuy'], ['title' => __('Genera riepilogo iva'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                        break;
                        case 'ribaflux':
                            if (isset($tipologia))
                            {
                                if (RIBA_FLOW && $tipologia == 1) // Se è selezionato fatture
                                {
                                    echo $this->Html->link('Genera flusso riba', ['action' => 'createRibaFlow'], ['title' => __('Genera Flusso Riba'), 'escape' => false, 'class' => "blue-button btn-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;background-color:#dcd6d6 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                                }
                            }
                        break;
                        case 'editsettings':
                            echo $this->Html->link($element, ['action' => 'edit', $settings['Setting']['id']], ['title' => __('edit'), 'escape' => false, 'class' => "blue-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;color:#555555 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                            ?>
                            <script>
                                $(".createExcel").hide();
                            </script>
                        <?php
                        break;
                        // Serve per mettere l'xls anche quando non ci sono bottoni
                        case 'nothing':
                        break;
                        default:
                            if (isset($postValue))
                            {
                                echo $this->Html->link($element, ['action' => $key, $postValue], ['title' => __($element), 'escape' => false, 'class' => "blue-button btn-outline dropdown-toggle topbutton", 'style' => 'text-transform:none;margin-left:5px;padding:10px !important;color:#555555 !imporant;margin-right:20px;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;background-color:#ea5d0b']);
                            }
                            else
                            {
                                echo $this->Html->link($element, ['action' => $key], ['title' => __($element), 'escape' => false, 'class' => "blue-button btn-outline dropdown-toggle topbutton", 'style' =>$buttonStyleOrange]);
                            }
                        break;
                    }
                }
            }
			else
			{
				?>
				<script>$(".createExcel").hide();</script>
				<?php
			}
            ?>
		    </div>
        <?php
            if($_SESSION['Auth']['User']['group_id'] != 6)
            {
                ?>
                <script>$(".createExcel").hide();</script>
                <?php
            }
        ?>

<script>

	$(".createExcel").click
	(
		<?php
			isset($xlsLink) ? null : $xlsLink = "index";
			isset($passedValues) ? $url = ["action" => $xlsLink, $passedValues ] : $url = ["action" => $xlsLink];
		?>
        <?php
            isset($xlsLink) ? null : $xlsLink = "indexscadatt";
            isset($passedValues) ? $url = ["action" => $xlsLink, $passedValues ] : $url = ["action" => $xlsLink];
        ?>
		function()
		{
			if(typeof arrayToPost !== 'undefined')
			{
				if(arrayToPost.length == 0)
				{
					var datiStringa = ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls";
				}
				else
				{
					var datiStringa = "data[createCsv]=xls"+"&data[arrayToPost]="+arrayToPost;
				}
			}
			else
			{
				var datiStringa = ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls";
			}

            ajaxFilterStartLoading();

			if($(this).hasClass('performOnSelectedOnly'))
			{
				$.ajax({
					method: "POST",
					url: "<?= $this->Html->url($url) ?>",
					data:   datiStringa,
					success: function(data)
					{
                        ajaxFilterStopLoading();

						csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
						var a = document.createElement('A');
						a.href = csvData;
						a.download = '<?= 'elenco_' . strtolower(addslashes($indextitle)) ?>.csv';
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					},
					error: function(data)
					{
						console.log('Gestionale-online error: 125');
					}
				})
			}
			else
            {
				$.ajax({
					method: "POST",
					url: "<?php // $this->Html->url($url) ?>",
					data: ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls",
					success: function(data)
					{
                        ajaxFilterStopLoading();

						csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
						var a = document.createElement('A');
						a.href = csvData;
						a.download = '<?php // 'elenco_' . strtolower(addslashes($indextitle)) ?>.csv';
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					},
					error: function(data)
					{
						console.log('Gestionale-online error: 125');
					}
				})
			}
		}
	)

	$(".createExcelcomma").click
	(
		<?php
			isset($xlsLink) ? null : $xlsLink = "index";
			isset($passedValues) ? $url = ["action" => $xlsLink, $passedValues ] : $url = ["action" => $xlsLink];
		?>
        <?php
            isset($xlsLink) ? null : $xlsLink = "indexscadatt";
            isset($passedValues) ? $url = ["action" => $xlsLink, $passedValues ] : $url = ["action" => $xlsLink];
        ?>
		function()
		{
			if(typeof arrayToPost !== 'undefined')
			{
				if(arrayToPost.length == 0)
				{
					var datiStringa = ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls2";
				}
				else
				{
					var datiStringa = "data[createCsv]=xls2"+"&data[arrayToPost]="+arrayToPost;
				}
			}
			else
			{
				var datiStringa = ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls2";
			}

			if($(this).hasClass('performOnSelectedOnly')) {
				$.ajax({
					method: "POST",
					url: "<?= $this->Html->url($url) ?>",
					data:   datiStringa,
					success: function(data)
					{
						csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
						var a = document.createElement('A');
						a.href = csvData;
						a.download = '<?= 'elenco_' . strtolower(addslashes($indextitle) . ' - con separatore modificato' ) ?>.csv';
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					},
					error: function(data)
					{
						console.log('Gestionale-online error: 125');
					}
				})
			}  else {
				$.ajax({
					method: "POST",
					url: "<?php // $this->Html->url($url) ?>",
					data: ajaxFilterGetParameters($(".ajax-filters-container"))+"&data[createCsv]=xls2",
					success: function(data)
					{
						csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(data);
						var a = document.createElement('A');
						a.href = csvData;
						a.download = '<?php // 'elenco_' . strtolower(addslashes($indextitle)) ?>.csv';
						document.body.appendChild(a);
						a.click();
						document.body.removeChild(a);
					},
					error: function(data)
					{
						console.log('Gestionale-online error: 125');
					}
				})
			}
		}
	)

</script>

<style>
	.topbutton:hover
	{
		color:white !important;
		background-color:#589AB8 !important;
		text-decoration:none !important;
	}
</style>


