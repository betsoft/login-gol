
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per fattura accompagnatoria</div>
        <?php
        // Se abilitato modulo di magazzino avanzato
        if(ADVANCED_STORAGE_ENABLED)
		{
		 ?>
            <div class="form-group col-md-4 col-md-offset-4">
                    	<label class="form-label"><strong>Deposito di prelievo</strong><i class="fa fa-asterisk"></i></label>
                    	<div class="form-controls">
                      	    <!--$this->Form->select('deposit_id', $deposits, array('value'=>$mainDeposit['Deposits']['id'],'class' => 'form-control','required'=>true,'empty'=>true));-->
                            <?= $this->Form->input('deposit_id', ['label' => false, 'options' => $deposits, 'class' => 'form-control']); ?>
                        </div>
            </div>
		<?php
    	}
		?>

       <div class="form-group col-md-12">

          <div class="col-md-2" style="float:left;">
              <label class="form-label form-margin-top"><strong>Numero colli</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('num_colli', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
           </div>

          <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Peso lordo (kg)</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('weight', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
            </div>
         <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Peso netto (kg)</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('bill_net_weight', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
            </div>

           <!--div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Volume (mc)</strong></label>
              <div class="form-controls">
              <?php // $this->Form->input('volume', array( 'label' => false, 'div' => false, 'class'=>'form-control','min'=>0)); ?>
              </div>
              </div-->


           <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Data inizio trasporto</strong></label>
              <div class="form-controls">
    	           <input  type="datetime" id="datepickeraccompagnatoria" class="datepicker segnalazioni-input form-control" name="data[Bill][billaccdate]" value = "<?= date("d-m-Y"); ?>"  />
	          </div>
          </div>

          <div class="col-md-2" style="float:left;margin-left:5px;">
        	<label class="form-label"><strong>Causale di trasporto</strong></label>
        		<div class="form-controls">
          	<?= $this->Form->select('causal_id', $causali, array('value' => 1,'class' => 'form-control')); ?>
       		</div>
      	</div>

         </div>

         <div class="form-group col-md-12">
           <div class="col-md-2" style="float:left;">
              <label class="form-label form-margin-top"><strong>Mezzo di trasporto</strong></label>
              <div class="form-controls">
              <?=  $this->Form->input('means_of_transport', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
              </div>
          </div>
          <div class="col-md-2" style="float:left;margin-left:5px;">
              <label class="form-label form-margin-top"><strong>Aspetto esteriore dei beni</strong></label>
              <div class="form-controls">
              <?=  $this->Form->input('aspect', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => 'A Vista')); ?> <!-- Predefinito a vista -->
              </div>
          </div>

          <div class="col-md-2" style="float:left;margin-left:5px;" >
              <label class="form-label form-margin-top"><strong>Trasporto effettuato da</strong></label>
              <div class="form-controls">
              <?= $this->Form->input('transporter', array('label' => false, 'div' => false, 'class' => 'form-control','type'=>'select', 'options'=>['0'=>'Mittente','1'=>'Destinatario','2'=>'Vettore'],'empty'=> true)); ?>
              </div>
          </div>

           <div class="col-md-2 vectorDiv" style="float:left;margin-left:5px;" hidden>
        	<label class="form-label"><strong>Vettore</strong></label>
        		<div class="form-controls">
          	<?= $this->Form->select('carrier_id', $carriers, array('value' => 1,'class' => 'form-control')); ?>
       		</div>
      	</div>
      </div>


      <?php isset($this->data['Bill']['carrier_id']) ? $carrierId = $this->data['Bill']['carrier_id'] : $carrierId = 'empty'; ?>
         <script>

        /** All'edit **/

        if($("#BillTransporter").val() != 2)
              {
                  $(".vectorDiv").hide();
              }
              else
              {
                $(".vectorDiv").show();
                $("#BillCarrierId").val('<?= $carrierId ?>');
              }

      /** Al change **/
          $("#BillTransporter").change(function()
          {
              if($(this).val() != 2)
              {
                $(".vectorDiv").hide();
              }
              else
              {
                $(".vectorDiv").show();
              }
          });
      </script>
