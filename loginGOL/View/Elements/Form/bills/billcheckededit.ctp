<div class="form-group" style="margin-top:20px;">
    <?php if ($tipologia != 8) { ?>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Fattura accompagnatoria</strong></label>
        <div class="form-controls" style="margin-top:8px;">
             <?= $this->Form->hidden('Bill.accompagnatoria', ['label' => false,'class' => 'form-control','type'=>'checkbox','style'=>'margin-top:10px;']); ?>
         <?=  $bills['Bill']['accompagnatoria'] == 1 ? '<span style="color:#577400"><b>SI</b></span>' : '<span style="color:#ea5d0b"><b>NO</b></span>'; ?>
        </div>
    </div>
    <?php } ?>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
        <div class="form-controls" style="margin-top:8px;">
            <?= $this->Form->hidden('Bill.split_payment', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
            <?=  $bills['Bill']['split_payment'] == 1 ? '<span style="color:#577400"><b>SI</b></span>' : '<span style="color:#ea5d0b"><b>NO</b></span>'; ?>
        </div>
    </div>
    <?php if ($tipologia != 8) { ?>
     <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Fattura Elettronica</strong></label>
        <div class="form-controls" style="margin-top:8px;">
             <?= $this->Form->hidden('Bill.electronic_invoice', ['label' => false,'class' => 'form-control','type'=>'checkbox']); ?>
             <?=  $bills['Bill']['electronic_invoice'] == 1 ? '<span style="color:#577400"><b>SI</b></span>' : '<span style="color:#ea5d0b"><b>NO</b></span>'; ?>
        </div>
    </div>
    <?php } ?>
</div>