<?php if($this->action != 'addExtendedbuy' && $this->action != 'editExtendedBuy'): // Se non è una fattura d'acquisto ?>
    <div class="col-md-12"><hr class="jsEsigibilityVat"></div>
    <div class="form-group caption-subject bold uppercase col-md-12  jsEsigibilityVat" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Dati per fattura elettronica</div>
    <br/>
    <div class="form-group col-md-12 jsEsigibilityVat">
        <div class="col-md-2" style="float:left;margin-left:5px;">
            <label class="form-label form-margin-top"><strong>Esigibilità iva</strong><i class="fa fa-asterisk"></i></label>
            <div class="form-controls">
                <?= $this->Form->input('einvoicevatesigibility', ['label' => false,'class' => 'form-control','type'=>'select','empty'=>true,'options'=>$einvoiceVatEsigibility,'default'=>'I']); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:5px;">
            <label class="form-label form-margin-top"><strong>Causale documento</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('document_causal', ['label' => false,'class' => 'form-control','pattern'=>PATTERNBASICLATIN, 'maxlength' => 200]); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:5px;">
            <label class="form-label form-margin-top"><strong>Art. 73</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('art73', ['label' => false,'type'=>'select','options'=>[ 'SI'=>'SI'],'empty'=>true, 'class' => 'form-control']); ?>
            </div>
        </div>
        <?php if($this->action == 'add' || $this->action == 'edit'): ?>
            <div class="col-md-2" style="float:left;margin-left:5px;">
                <label class="form-label form-margin-top"><strong>Numero Rif. ddt</strong><i class="fa fa-question-circle jsmainrifddtnumber" style="color:#ea5d0b;cursor:pointer;"></i></label>
                <div class="form-controls">
                    <?= $this->Form->input('mainrifddtnumber', ['label' => false, 'class' => 'form-control']); ?>
                </div>
            </div>
            <div class="col-md-2" style="float:left;margin-left:10px;">
                <label class="form-label form-margin-top"><strong>Data Rif. ddt</strong><i class="fa fa-question-circle jsmainrifddtdate" style="color:#ea5d0b;cursor:pointer;"></i></label>
                <div class="form-controls">
                    <?php if($this->action == 'add'): ?>
                        <input  type="datetime" id="datepickerrifddt" class="datepicker segnalazioni-input form-control" name="data[Bill][mainrifddtdate]"    />
                    <?php endif; ?>
                    <?php if($this->action == 'edit'): ?>
                        <?php $this->data['Bill']['mainrifddtdate'] != null ? $dataValue =  $this->Time->format('d-m-Y', $this->data['Bill']['mainrifddtdate']) : $dataValue = ''; ?>
                        <input type="datetime" id="datepickerrifddt" class="datepicker segnalazioni-input form-control" name="data[Bill][mainrifddtdate]"  value = "<?= $dataValue ?>"  />
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if($this->action == 'addExtendedcreditnotes' || $this->action == 'editExtendedcreditnotes'): // Se è una nota di credito ?>
            <div class="col-md-2" style="float:left;margin-left:5px;">
                <label class="form-label form-margin-top"><strong>Numero fattura riferimento</strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('bill_referred_number', ['label' => false,'class' => 'form-control']); ?>
                </div>
            </div>
            <div class="col-md-2" style="float:left;margin-left:10px;">
                <label class="form-label form-margin-top"><strong>Data fattura</strong></label>
                <div class="form-controls">
                    <?php if($this->action == 'addExtendedcreditnotes'): ?>
                        <input type="datetime" id="datepickerreferreddate" class="datepicker segnalazioni-input form-control" name="data[Bill][bill_referred_date]" />
                    <?php else: ?>
                        <?php if(isset($this->data['Bill']['cig_date']) && ($this->data['Bill']['cig_date'] != '1970-01-01')): ?>
                            <input type="datetime" id="datepickerreferreddate" class="datepicker segnalazioni-input form-control" name="data[Bill][bill_referred_date]" value=<?= $this->Time->format('d-m-Y', $this->data['Bill']['bill_referred_date']); ?> />
                        <?php else: ?>
                            <input type="datetime" id="datepickerreferreddate" class="datepicker segnalazioni-input form-control" name="data[Bill][bill_referred_date]" />
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<div class="col-md-12"><hr></div>

<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">
    Riferimento Documento ( ordine / contratto / convenzione )
</div><br/>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>N. documento</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cig_documentid', ['label' => false,'class' => 'form-control',"pattern"=>"[A-Za-z0-9!\"#$%&'()*+,-./:;<=>?@[\]^_{}~ àèéìòù]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:5px;">
        <label class="form-label form-margin-top"><strong>Tipo documento</strong></label>
        <div class="form-controls">
            <?php
            if($this->action == 'addExtendedcreditnotes' || $this->action == 'editExtendedcreditnotes')
                $arrayOfDocumentType = ['0'=>'Ordine d\'acquisto', '1'=>'Contratto', '2'=>'Convenzione'];
            else
                $arrayOfDocumentType = ['0'=>'Ordine d\'acquisto', '1'=>'Contratto', '2'=>'Convenzione','3'=>'Fattura collegata'];

            echo $this->Form->input('cig_type', ['label' => false,'class' => 'form-control', 'type'=>'select' , 'empty' =>true, 'options'=>$arrayOfDocumentType]);
            ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:5px;">
        <label class="form-label form-margin-top"><strong>Data documento</strong></label>
        <div class="form-controls">
            <?php if(isset($this->data['Bill']['cig_date']) && ($this->data['Bill']['cig_date'] != '1970-01-01')): ?>
                <input type="text" id="datepickercigdate" class="datepicker segnalazioni-input form-control" name="data[Bill][cig_date]" value=<?=  $this->Time->format('d-m-Y', $this->data['Bill']['cig_date']) ?> />
            <?php else: ?>
                <input type="text" id="datepickercigdate" class="datepicker segnalazioni-input form-control" name="data[Bill][cig_date]" />
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-3" style="float:left;">
        <label class="form-label form-margin-top"><strong>Identificativo riga documento</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cig_num_item', ['label' => false,'class' => 'form-control',"pattern"=>"[0-9a-zA-Z-_]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:5px;">
        <label class=" form-margin-top" style="margin-top:5px;"><strong>Codice commessa/convenzione</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cig_codice_commessa', ['label' => false,'class' => 'form-control',"pattern"=>"[0-9a-zA-Z-_]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:5px;">
        <label class="form-label form-margin-top"><strong>Codice CIG</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cig', ['label' => false,'class' => 'form-control',"pattern"=>"[0-9a-zA-Z-_]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:5px;">
        <label class="form-label form-margin-top"><strong>Codice Cup</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('cup', ['label' => false,'class' => 'form-control',"pattern"=>"[0-9a-zA-Z-_]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici']); ?>
        </div>
    </div>
</div>

<script>
    $(".jsmainrifddtdate").click(
        function()
        {
            $.alert({
                icon: 'fa fa-question-circle',
                title: '',
                content: "Da utilizzare nel caso si debba inserire un numero riferimento ddt, per ddt non creati dalla funzione di creazione fattura da bolla.",
                type: 'orange',
            });
        }
    );

    $(".jsmainrifddtnumber").click(
        function()
        {
            $.alert({
                icon: 'fa fa-question-circle',
                title: '',
                content: "Da utilizzare nel caso si debba inserire una data riferimento ddt, per ddt non creati dalla funzione di creazione fattura da bolla.",
                type: 'orange',
            });
        }
    );
</script>
