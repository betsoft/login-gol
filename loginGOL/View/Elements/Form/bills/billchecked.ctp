<div class="row">
<?php if ($tipologia != 8) { ?>
    <div class="col-md-2 col-md-offset-1">
        <label class="form-label form-margin-top"><strong>Fattura accompagnatoria</strong></label>
        <div class="form-controls" style="margin-top:8px;">
             <?= $this->Form->input('Bill.accompagnatoria', ['label' => false,'type'=>'checkbox', 'style'=>'width:25px;height:25px;margin-top:-9px;']); ?>
        </div>
    </div>
    <?php } ?>
    <div class="col-md-2">
        <label class="form-label form-margin-top"><strong>Split Payment</strong></label>
        <div class="form-controls " style="margin-top:8px;">
             <?= $this->Form->input('Bill.split_payment', ['label' => false,'type'=>'checkbox' , 'style'=>'width:25px;height:25px;margin-top:-9px;']); ?>
        </div>
    </div>
    <?php if ($tipologia != 8) { ?>
     <div class="col-md-2">
        <label class="form-label form-margin-top"><strong>Fattura Elettronica</strong></label>
        <div class="form-controls " style="margin-top:8px;">
             <?= $this->Form->input('Bill.electronic_invoice', ['label' => false, 'style'=>'width:25px;height:25px;margin-top:-9px;','type'=>'checkbox','checked'=>true]); ?>
        </div>
    </div>
    <?php } ?>
     <div class="col-md-1">
        <label class="form-label form-margin-top"><strong>Nota di debito</strong></label>
        <div class="form-controls " style="margin-top:8px;">
             <?= $this->Form->input('Bill.debitnote', ['label' => false,'type'=>'checkbox' , 'style'=>'width:25px;height:25px;margin-top:-9px;']); ?>
        </div>
    </div>
    </div>
   
