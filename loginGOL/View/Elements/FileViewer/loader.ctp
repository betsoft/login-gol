<script>
	addLoadEvent(function() {
		initializeFileViewer()
	},'initializeFileViewer')
	
	function initializeFileViewer() {
		$('.file-viewer .preview:not(.preview-expanded)').each(function() {
			var preview = $(this)
			if(preview.attr('expandible')) {
				preview.mouseenter(function() {
					var image = $(this).clone(true, true)
						.removeAttr('expandible')
						.addClass('preview-expanded triangle-border left')
						.css('top',preview.position().top + " px")
						.css('left',preview.position().left + " 10 px")
					
					$(this).parent().append(image)
				})
				preview.mouseleave(function() {
					$(this).parents('.file-viewer').first().find('.preview-expanded').remove() 
				})
			}
		})
	}
	
	function fileViewerLoadImage(id, url) {
        $(id).find('.preview').attr('src', url);
        $(id).find('.download').attr('href', url);
    }
</script>

<?= $this->Html->css('FileViewer/default') ?>
<?= $this->Html->css('FileViewer/custom') ?>