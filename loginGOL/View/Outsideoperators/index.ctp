<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?= $this->element('Form/formelements/indextitle', ['indextitle' => 'Operatori esterni', 'indexelements' => ['add' => 'Nuovo operatore esterno']]); ?>

<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' => $filterableFields]); ?></tr>
            <?php
            if (count($outsideoperators) == 0) {
                ?>
                <tr>
                <td colspan="<?= count($sortableFields) ?>" style="text-align: center">Nessun operatore esterno trovato</td></tr><?php
            }
            ?>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php foreach ($outsideoperators as $outsideoperator) {
                ?>
                <tr>
                    <td><?= h($outsideoperator['Outsideoperator']['surname']); ?></td>
                    <td><?= h($outsideoperator['Outsideoperator']['name']); ?></td>
                    <td style="text-align:right"><?= h($outsideoperator['Outsideoperator']['labour_cost']) . ' €'; ?></td>
                    <td style="text-align:right"><?= h($outsideoperator['Outsideoperator']['labour_cost_night']). ' €'; ?></td>
                    <td style="text-align:right"><?= h($outsideoperator['Outsideoperator']['labour_cost_weekend']). ' €'; ?></td>
                    <td class="actions">
                        <?php
                            echo $this->Html->link($iconaModifica, ['action' => 'edit', $outsideoperator['Outsideoperator']['id']], ['title' => __('Modifica'), 'escape' => false]);
                            echo $this->Html->link('<i class="fa fa-list  verde" style="vertical-align:middle;font-size:20px;margin-right:5px;" outsideoperatorId = "'. $outsideoperator['Outsideoperator']['id'].'" ></i>', ['action' => 'operatordetails','controller'=>'Outsideoperators', $outsideoperator['Outsideoperator']['id']],['title'=>__('Visualizza riepilogativo ore operatore'),'escape'=>false]);
                            echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $outsideoperator['Outsideoperator']['id']], ['title' => __('Elimina'), 'escape' => false], __("Sei sicuro di voler eliminare l'operatore esterno ?", $outsideoperator['Outsideoperator']['id']));
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?= $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>