<?=  $this->Form->create('Outsideoperators'); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica operatore esterno'); ?></span>

<div class="col-md-12"><hr></div>

<?= $this->Form->input('id',['class'=>'form-control']); ?>

<div class="form-group col-md-12">

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Nome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>

    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Cognome</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('surname', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Orario</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Notturno</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_night', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Costo Weekend</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('labour_cost_weekend', ['div' => false, 'label' => false, 'class'=>'form-control', 'min'=>"0"]); ?>
        </div>
    </div>
</div>
<div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

