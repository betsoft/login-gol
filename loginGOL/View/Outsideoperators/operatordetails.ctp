<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?= $this->Html->link('indietro', ['controller'=>'Outsideoperators','action' => 'index'], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;float:right;width:10%;text-align:center;']); ?>

<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Schede d\'intervento tecnico' . ' - ' . $OperatorName]); ?>
<?php $totalWork = $totalNightwork = 0; ?>
<div class="clients index">
    <div class="units index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class ="flip-content">
            <tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
            <tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
            <?php
            if (count($OperatorHours) < 1)
            {
                ?><tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">nessun tecnico trovato</td></tr><?php
            }
            ?>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php foreach ($OperatorHours as $hour)
            {


                //foreach ($hour['Maintenancehoursoutsideoperator'] as $row) {

                       if ($hour['Maintenancehoursoutsideoperator']['state'] == 1 && $hour['Maintenancehour']['state'] == 1 && isset( $hour['Maintenancehour']['Maintenance']  )) {
                            ?>
                            <tr>

                                <td style="text-align: center;"><?= $hour['Maintenancehour']['Maintenance']['maintenance_number']; ?></td>
                                <td style="text-align: center;"><?= date("d-m-Y", strtotime($hour['Maintenancehour']['Maintenance']['maintenance_date'])); ?></td>
                                <td><?= $hour['Maintenancehour']['Maintenance']['intervention_description']; ?></td>
                                <td style="text-align: center;"><?= $hour['Maintenancehour']['hour_from']; ?></td>
                                <td style="text-align: center;"><?= $hour['Maintenancehour']['hour_to']; ?></td>
                                <td class="right">
                                    <?php
                                    echo $utilities->differencesInMinutes(date("H:i", strtotime(  $hour['Maintenancehour']['hour_from'])), date("H:i", strtotime( $hour['Maintenancehour']['hour_to'])));
                                    $totalWork += $utilities->differencesInMinutes(date("H:i", strtotime( $hour['Maintenancehour']['hour_from'])), date("H:i", strtotime( $hour['Maintenancehour']['hour_to'])), false);
                                    ?>
                                </td>
                                <td style="text-align: center;"> <?= $utilities->isWeekend($hour['Maintenancehour']['Maintenance']['maintenance_date']) ? '' : 'NO'; ?></td>
                                <td class="right">
                                    <?php
                                    echo $utilities->formatTimeInHourAndMinutes($utilities->getNightTimeHourAndMinutes( $hour['Maintenancehour']['hour_from'], $hour['Maintenancehour']['hour_to']));
                                    $totalNightwork += $utilities->getNightTimeHourAndMinutes( $hour['Maintenancehour']['hour_from'], $hour['Maintenancehour']['hour_to']);
                                    ?>
                                </td>
                                <td><?= $hour['Maintenancehour']['Maintenance']['Constructionsite']['name'] . ' - ' . $hour['Maintenancehour']['Maintenance']['Constructionsite']['description']; ?></td>
                                <?php



                           /* ?>
                                <!--td style="text-align: center;"><?= $hour['Maintenance']['maintenance_number']; ?></td>
                                <td style="text-align: center;"><?= date("d-m-Y", strtotime($hour['Maintenance']['maintenance_date'])); ?></td>
                                <td><?= $hour['Maintenance']['intervention_description']; ?></td>
                                <td style="text-align: center;"><?= $row['hour_from']; ?></td>
                                <td style="text-align: center;"><?= $row['hour_to']; ?></td>
                                <td class="right">
                                    <?php
                                    echo $utilities->differencesInMinutes(date("H:i", strtotime( $row['hour_from'])), date("H:i", strtotime($row['hour_to'])));
                                    $totalWork += $utilities->differencesInMinutes(date("H:i", strtotime($row['hour_from'])), date("H:i", strtotime($row['hour_to'])), false);
                                    ?>
                                </td>
                                <td style="text-align: center;"> <?= $utilities->isWeekend($hour['Maintenance']['maintenance_date']) ? '' : 'NO'; ?></td>
                                <td class="right">
                                    <?php
                                    echo $utilities->formatTimeInHourAndMinutes($utilities->getNightTimeHourAndMinutes( $row['hour_from'], $row['hour_to']));
                                    $totalNightwork += $utilities->getNightTimeHourAndMinutes( $row['hour_from'], $row['hour_to']);
                                    ?>
                                </td>
                                <td><?= $hour['Constructionsite']['name'] . ' - ' . $hour['Constructionsite']['description']; ?></td-->
                            <php */ ?>

                            </tr>
                            <?php
                          }
                   // }
            }

            ?>
            <tr><td colspan="5"></td><td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalWork) ?></b></td><td></td><td class="right"><b><?= $utilities->formatTimeInHourAndMinutes($totalNightwork) ?></b></td><td></td></tr>
            </tbody>
        </table>
        <?=  $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>


