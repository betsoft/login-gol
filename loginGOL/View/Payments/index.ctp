<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>


<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Metodi di pagamento','indexelements' => ['add'=>'Nuovo metodo di pagamento']]); ?>


<div class="clients index">
	<div class="ivas index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($payments) > 0) { ?>
					<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
				<?php }
				else
				{
					?><tr><td colspan="6"><center>nessun metodo di pagamento trovato</center></td></tr><?php
				}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($payments as $payment) { ?>
					<tr>
						<td><?= h($payment['Payment']['metodo']); ?></td>
						<td><?= $payment['Payment']['endofmonth'] == 1 ? '<center><span style="color:green;"><b>SI</b></span></center>' : '<center><span style="color:red"><b>NO</b></span></center>' ?></td>
						<td><?= $payment['Payment']['riba'] == 1 ? '<center><span style="color:green"><b>SI</b></span></center>' : '<center><span style="color:red"><b>NO</b></span></center>' ?></td>
						<?php
							if(BILLS_RECEIPT)
							{
							?>
								<td style="text-align:center;"><?= $payment['Payment']['default_receipt'] == 0 ? '<i  class="fa fa-star-o  jsStarReceipt" starid="'. $payment['Payment']['id'].'" ></i>' : '<i  class="fa fa-star selected " style="color:orange;">' ; ?></td>
							<?php
							}
						?>

						<td>
							<?php
								$i=0;
								foreach($payment['Paymentdeadlines'] as $paymentdeadline)
								{
									$paymentdeadline['deadlineday'] == '0' ? $paymentdeadline['deadlineday'] = 'immediata' : null ;
									$i == 0 ? $paymentdeadline = $paymentdeadline['deadlineday'] : $paymentdeadline = ' / ' . $paymentdeadline['deadlineday'] ;
									$i++;
									echo $paymentdeadline;
								}
								?>
						</td>
						<td><?= h($payment['Bank']['description']); ?></td>
						<td><?= h($payment['AlternativeBank']['description']); ?></td>
						<td class="actions">
							<?php
								echo $this->Html->link($iconaModifica, array('action' => 'edit', $payment['Payment']['id']),array('title'=>__('View'),'escape'=>false));
								echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $payment['Payment']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare il metodo pagamento ?', $payment['Payment']['id']));
							?>
						</td>

					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
<script>
	$(".jsStarReceipt").click
	(
		function()
		{
			var id = $(this).attr('starid');

			$.ajax({
				method: "POST",
				url: "<?= $this->Html->url(["controller" => "payments","action" => "setDefaultReceiptPayment"]) ?>",
				data:
				{
					id:id,
				},
				success: function(data)
				{
					  location.reload();
				}});
		});
</script>
