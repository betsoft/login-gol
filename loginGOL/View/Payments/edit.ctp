<?= $this->element('Js/addcrossremoving'); ?>
<
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica Metodo di pagamento') ?></span>
	 <div class="col-md-12"><hr></div>


    <?= $this->Form->create('Payment', ['class' => 'uk-form uk-form-horizontal']); ?>
   <div class="form-group col-md-12">
       <div class="col-md-6">
	   <label class="form-label"><strong>Metodo di pagamento<i class="fa fa-asterisk"></i></strong></label>
          <div class="form-controls">
	        <?= $this->Form->input('metodo',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'lunghezza2']); ?>
	      </div>
        </div>
    </div>

  <div class="form-group col-md-12">
     <div class="col-md-2" style="float:left">
     <label class="form-margin-top form-label"><strong>Banca d'appoggio</strong></label>
   <div class="form-controls">
	    <?= $this->Form->input('bank_id', ['div' => false, 'label' => false, 'class'=>'form-control', 'id'=>'lunghezza2','options'=>$banks,'empty'=>true]); ?>
	 </div>
   </div>

      <div class="col-md-2" style="float:left">
          <label class="form-margin-top form-label"><strong>Banca d'appoggio alternativa</strong></label>
          <div class="form-controls">
              <?= $this->Form->input('alternative_bank_id', ['div' => false, 'label' => false, 'class'=>'form-control', 'id'=>'lunghezza2','options'=>$banks,'empty'=>true]); ?>
          </div>
      </div>


		<div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Fatturazione fine mese</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <?php
        	  $arrayOfOptions = ['0'=>'NO','1'=>'SI'];
            echo $this->Form->select('endofmonth', $arrayOfOptions, ['class' => 'form-control','empty'=>false]);
        ?>
       </div>
      </div>

  	  <div class="col-md-2" style="float:left;margin-left:10px;" id="dayofmonth">
      <label class="form-label"><strong>Giorno mese successivo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
          <?php $arrayOfDay = ['5'=>'5','10'=>'10','15'=>'15','20'=>'20']; ?>
          <?=  $this->Form->select('next_month_day', $arrayOfDay, ['class' => 'form-control','empty'=>true]); ?>
       </div>
   </div>


	  <?php // Gestione flussi riba ?>
  <?php   if(RIBA_FLOW) { ?>

	  	<div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Generazione riba<i class="fa fa-asterisk"></i></strong></label><i class="fa fa-question-circle jsRiba" style="color:#589ab8;cursor:pointer;"></i>
        <div class="form-controls">
          <?php
        	  $arrayOfOptions = ['0'=>'NO','1'=>'SI'];
            echo $this->Form->select('riba', $arrayOfOptions, ['class' => 'form-control','empty'=>false]);
          ?>
       </div>
    </div>

  	<div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Spese d'incasso </strong></label>
         <div class="form-controls">
	           <?= $this->Form->input('paymentFixedCost', ['div' => false, 'label' => false, 'class'=>'form-control','type'=>'number']); ?>
         </div>
    </div>
        <?php } ?>
    </div>
    <!-- Fine intestazione -->

	  <br/><br/>
	  <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Dati per Fattura elettronica</div>
      <div class="form-group col-md-12">
          <div class="col-md-3" style="float:left;">
         <label class="form-label form-margin-top"><strong>Tipo di pagamento</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
             <?= $this->Form->input('einvoicepaymenttype_id', ['label' => false,'class' => 'form-control','type'=>'select','empty'=>true,'options'=>$einvoicePaymentType,'required'=>true]); ?>
         </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
         <label class="form-label form-margin-top"><strong>Metodo di pagamento</strong><i class="fa fa-asterisk"></i></label>
         <div class="form-controls">
             <?= $this->Form->input('einvoicepaymentmethod_id', ['label' => false,'class' => 'form-control','type'=>'select','empty'=>true,'options'=>$einvoicePaymentMethod,'required'=>true]); ?>
        </div>
    </div>
    </div>

 <div class="col-md-12"><hr></div>
      <div class="col-md-12">
        <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Scadenze in giorni</div>
         <div class="col-md-12"><hr></div>
          <div class="col-md-12">
        	<strong>Inserire le scadenze in giorni ( Ad esempio per una scadenza 30, 60, 90 inserire tre righe coi rispettivi valori). Inserire 0 per scadenze immediate.</strong>
        	</div>
         <div class="col-md-12"><hr></div>
         <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
         <fieldset id="fatture"  class="col-md-12">
    <?php
    	$countValue = count($deadlines) - 1;
    	$i = -1;
    	foreach ($deadlines as $key => $deadline)
    	{
    	    $i++;
    	    $i > 0 ? $hidden = '' :  $hidden = 'hidden';
    		$key == $countValue ? $classe = "ultima_riga":$classe = '';
    		$key == 0 ? $classe1 = "originale": $classe1 = '';
    ?>
        	<div class="principale<?= $deadline['Paymentdeadlines']['id'] ?> lunghezza contacts_row ' . '  <?= $classe1 . ' ' .   $classe ?> '" id="' . $chiave . '">
        		<span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"   <?= $hidden ?> ></span>
        		<div class="col-md-12">
        	    <div class=" col-md-3">
        				<label class="form-margin-top">
        					<strong>Scadenza</strong><i class="fa fa-asterisk"></i>
        				</label>
		        		<?=
							 $this->Form->input('Deadline.'.$key.'.Day', [
						    'div' => false,
						    'label' => false,
							'type'=>'number',
					    	'step'=>'30',
					    	'value'=> $deadline['Paymentdeadlines']['deadlineday'],
					    	'class'=>'form-control remove-space deadlineclass',
					    	'required'=>true,
							]);
						?>
        			</div>
        		</div>
        		 <div class="col-md-12"><hr></div>
        	</div>
        <?php } ?>
    </fieldset>

<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?=  $this->Form->end(); ?>
	<script>

            /*CLONAZIONE*/
            var clonato;
            var nuova_chiave = $(".lunghezza").length -1;

            $("#aggiungi_riga").click
            (
                function()
                {


                    var newId=$(".lunghezza").length;

                    nuova_chiave = nuova_chiave + 1;
                    clonato = $('.originale').clone();
                    $(clonato).removeClass("originale");
                    $(clonato).addClass("clonato");
                    $(clonato).addClass("ultima_riga");
                    $(clonato).insertAfter(".ultima_riga");
                    $(clonato).addClass("principale"+newId);
                    $(".ultima_riga").removeClass("ultima_riga");

                    $(".clonato #Deadline0Day").attr('name', "data[Deadline][" + nuova_chiave + "][Day]");
                    $(".clonato #Deadline0Day").attr('id', "Deadline" + nuova_chiave + "Day");

                    $('.deadlineclass').unbind('change');
                    $('.deadlineclass').change(function(){controlDeadlines();});

                     $(".clonato .rimuoviRigaIcon").show();

                    $(clonato).addClass("ultima_riga");
                    $(clonato).first(".rimuoviRigaIcon").show();
                    $(clonato).first(".rimuoviRigaIcon").removeAttr("hidden");
                    $(clonato).css('backgorund-color','blue');
                    $(clonato).removeClass(".clonato");

                     // Prendo la x che cancella la riga (per i campi aggiunti dinamicamente)
					var temp = $(".ultima_riga .rimuoviRigaIcon");

					// E ne aggiungo l'evento click che elimina la riga
					$(temp).first(".rimuoviRigaIcon").click
					(
						function()
						{
							if($(this).parent().hasClass('ultima_riga'))
							{
								$(this).parent().prev('.lunghezza').addClass("ultima_riga");
								$(this).parent().remove();
							}
							else
							{
								$(this).parent().remove();
							}
						}
					)

                    $("#Deadline"+ nuova_chiave +"Day").val('');
                }
            );

            $('.contacts_row .clonato').removeClass("clonato");
            $('.deadlineclass').change(function(){controlDeadlines();});

           // CONTROLLO SCADENZE
           function controlDeadlines()
           {
              var salvatagioabilitato = true;
              var flag = -1;
              $('.deadlineclass').each(function()
              {
                  newflag = $(this).val();
                  if(newflag !== undefined && newflag !== '')
                  {
                      if(+newflag > +flag)
                      {
                           flag = newflag;
                      }
                      else
                      {
                          salvatagioabilitato = false;
                      }
                  }
              })

              if(salvatagioabilitato == false)
              {
                  $.alert
                    ({
        			    icon: 'fa fa-warning',
	        			title: '',
    	    			content: 'Attenzione le scadenze devono essere progressive',
    		    		type: 'orange',
				    });
                $(".blue-button.new-bill").attr('disabled','disabled');
              }
              else
              {
                $(".blue-button.new-bill").removeAttr('disabled');
              }
          }

         // Nuova gestione del fine mese + x
         if($("#PaymentEndofmonth").val() == 1)
         {
            $("#dayofmonth").show();
         }
         else
         {
            $("#dayofmonth").hide();
         }

         $("#PaymentEndofmonth").change(
            function()
            {
                  if($("#PaymentEndofmonth").val() == 1)
                  {
                       $("#dayofmonth").show();
                  }
                  else
                  {
                        $("#dayofmonth").hide();
                  }
            });


		// E ne aggiungo l'evento click che elimina la riga
		$(".rimuoviRigaIcon").click
		(
		    function()
			{
			    if($(this).parent().hasClass('ultima_riga'))
				{
				    $(this).parent().prev('.lunghezza').addClass("ultima_riga");
					$(this).parent().remove();
				}
				else
				{
				    $(this).parent().remove();
				}
			}
		)

    </script>

    <script>
		$(".jsRiba").click(function()
		{
		   	$.alert
    	        ({
    				icon: 'fa fa-question-circle',
	    			title: '',
    				content: "<?= addslashes($helperMessage); ?>",
        			type: 'blue',
				});
		});
    </script>

