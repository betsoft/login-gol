<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php echo __('Configurazione parametri'); ?></span>
	</div>
</div>
<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr>
					<th><?php echo $this->Paginator->sort('Descrizione'); ?></th>
					<th><?php echo $this->Paginator->sort('Stato'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($configurations as $configuration) 
				{
				?>
					<tr>
						<td><?= $configuration['Configurations']['descrizione'] ?></td>
						<td><?=  $configuration['Configurations']['settingvalue'] == 0 ? '<b><span style="color:red">DISATTIVATO</span></b>' : '<b><span style="color:green">ATTIVATO</span></b>'; ?></td>
						<td class="actions">
						<?php
						if($configuration['Configurations']['settingvalue'] == 0)
						{
							echo $this->Form->postLink('<i class="fa fa-square-o"></i>', ['action' => 'changestate', $configuration['Configurations']['id']], ['title'=>__('Abilita'),'escape'=>false]); 	
						}
						else
						{
							echo $this->Form->postLink('<i class="fa fa-check-square-o"></i>', ['action' => 'changestate', $configuration['Configurations']['id']], ['title'=>__('Disabilita'),'escape'=>false]); 	
						}
						?>
						
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
	</div>
</div>

