<?=  $this->Form->create(); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica SCadenza'); ?></span>
    <div class="col-md-12"><hr></div>
<?= $this->Form->input('id',['class'=>'form-control']); ?>

    <div class="form-group col-md-12">
        <div class="col-md-3">
            <label class="form-margin-top form-label"><strong>Importo scadenza</label><i class="fa fa-asterisk"></i></strong>
            <div class="form-controls">
                <?= $this->Form->input('amount', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">
        <div class="col-md-3">
            <label class="form-margin-top form-label"><strong>Data scadenza</label><i class="fa fa-asterisk"></i></strong>
            <div class="form-controls">
            <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Deadline][deadline]" value="<?= $this->Time->format('d-m-Y', $this->data['Deadline']['deadline']) ?>"  required />
            </div>
        </div>
    </div>

    <div class="col-md-12"><hr></div>
    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexscadatt']); ?></center>
    <?= $this->element('Js/datepickercode'); ?>

<?=  $this->Form->end(); ?>
