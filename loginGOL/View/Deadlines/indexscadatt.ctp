<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader'); ?>
<?= $this->element('Form/Components/AjaxSort/loader'); ?>
<?= $this->element('Js/csv/csvfunction'); ?>

<?php echo $this->element('Form/formelements/indextitle', ['indextitle' => 'Scadenziario Attivo']);
?>
<div class="clients index">
    <div class="scadatt index">
        <table class="table table-bordered table-striped table-condensed flip-content">
            <thead class="flip-content">
            <tr>
                <?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?>
            </tr>
            <tr>
                <?= $this->element('Form/Components/AjaxFilter/component', ['elements' => $filterableFields,
                    'afterAjaxfilterCallback' => 'indexscadattAfterAjaxFilterCallback',
                    'htmlElements' => [
                        '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;"
				id="datepicker" class="form-control ajax-filter-input  datepicker" name="data[filters][date1]" value="'. $date1 .'"   bind-filter-event="change"/>' . '' .
                        '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;"
				id="datepicker2" class="form-control ajax-filter-input  datepicker" name="data[filters][date2]"  value="'. $date2 .'"  bind-filter-event="change"/></center>',
                        '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;"
				id="datepicker3" class="form-control ajax-filter-input  datepicker" name="data[filters][date3]" value="'. $date3 .'"   bind-filter-event="change"/>' . '' .
                        '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" 
				id="datepicker4" class="form-control ajax-filter-input  datepicker" name="data[filters][date4]"  value="'. $date4 .'"  bind-filter-event="change"/></center>',
                        $this->Form->input('filters.tipologia', ['div' => false, 'label' => false, 'class' => 'form-control ajax-filter-input',
                            'type' => 'select', 'options' => $tipologia,
                            'empty' => true, 'bind-filter-event' => "change"])
                    ]]);
                ?>
            </tr>
            </thead>
            <tbody class="ajax-filter-table-content">
            <?php
            $totalDeadlines = $totalPayed = 0;

            foreach ($deadlines as $deadline) {
                $deadline['Bill']['tipologia'] == 3 ? $coefficienteNC = -1 : $coefficienteNC = 1;
                ?>
                <?php
                $totalPaymentDeadline = 0;
                $lastPayment = null;
                $statofattura = 'D';
                if (!isset($deadline['Deadlinepayment']) || ($deadline['Deadlinepayment'] == null)) {
                    $statofattura = 'D';

							// Segno come insoluto
							if($deadline['Deadline']['unpaid'] == 1)
							{
								$statofattura = 'I';
							}
						}
						else
						{
							foreach($deadline['Deadlinepayment'] as $payment)
							{
                                if($payment['state'] == 1) {
                                    $totalPaymentDeadline += $payment['payment_amount'];  //$deadline['Deadline']['Deadlinepayment']['payment_amount'];
                                    $lastPayment = $payment['payment_date'];
                                }
							}

							if($coefficienteNC < 0)
							{
									$statofattura = 'A';
									// Segno come parzialmente insoluto
									if($deadline['Deadline']['unpaid'] == 1)
									{
										$statofattura = 'P';
									}
							}
							else
							{
								// refix20200623 if((number_format($totalPaymentDeadline,2) < (number_format($deadline['Deadline']['amount'],2) + number_format($deadline['Deadline']['vat'],2))) && $totalPaymentDeadline > 0)
								//if(($totalPaymentDeadline < ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat'])) && $totalPaymentDeadline > 0)
									 if(round($totalPaymentDeadline,2) < round(($deadline['Deadline']['amount'] + $deadline['Deadline']['vat']),2) && $totalPaymentDeadline > 0)
								{
									$statofattura = 'A';
									// Segno come parzialmente insoluto
									if($deadline['Deadline']['unpaid'] == 1)
									{
										$statofattura = 'P';
									}
								}
							}

							// Refix20200623 if(number_format($totalPaymentDeadline,2) == $coefficienteNC * (number_format($deadline['Deadline']['amount'],2) + number_format($deadline['Deadline']['vat'],2)))
							// if($totalPaymentDeadline == $coefficienteNC * ($deadline['Deadline']['amount'] + $deadline['Deadline']['vat']))
								if(round($totalPaymentDeadline,2) == round($coefficienteNC * ($deadline['Deadline']['amount']+ $deadline['Deadline']['vat']),2))

							{
								$statofattura = 'S';
							}
						}

						if($statofattura == 'D' && $deadline['Bill']['ribaemitted'] == 1)
						{
							$statofattura = 'R';
						}
                $scadenza = $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                if($totalPaymentDeadline > 0)
                    $saldo = $scadenza - $totalPaymentDeadline;
                else
                    $saldo = $scadenza;

                if (!isset($saldacconto) || $saldacconto == $statofattura) {
                    ?>
                    <tr>
                    <?php
                    if (sizeof($deadline['Bill']['Client']) > 0 && sizeof($deadline['Bill']['Payment']) > 0) {
                        ?>
                        <td><?= h($deadline['Bill']['numero_fattura']); ?></td>
                        <td style="text-align:center;"><?= h(date("d-m-y", strtotime($deadline['Bill']['date']))); ?></td>
                        <td style="text-align:center;"><?= h(date("d-m-y", strtotime($deadline['Deadline']['deadline']))); ?></td>
                        <td><?= h($deadline['Bill']['Client']['ragionesociale']); ?></td>
                        <!--td style="text-align:right;"><?php // h(number_format($deadline['Deadline']['amount'],2,',','.'));
                        ?></td>
								<td style="text-align:right;"><?php // h(number_format($deadline['Deadline']['vat'],2,',','.'));
                        ?></td-->
                        <td style="text-align:right;">
                            <?php
                            if (in_array($deadline['Deadline']['bill_id'], $unbalanced)) {
                                ?>
                                <i class="fa fa-warning" style="float:left;color:#ea5d0b;margin-top: 5px;"
                                   title="Attenzione il totale delle scadenze non corrisponde al totale della fattura"></i>
                                <?php
                            }
                            echo number_format($coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'], 2, ',', '.');
                            ?>
                        </td>
                        <td style="text-align:right;"><?= h(number_format($totalPaymentDeadline, 2, ',', '.')); ?></td>
                        <?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){ ?>
                        <td><?=  number_format($saldo, 2,',','.'); ?></td>
                            <?php } ?>
                        <td style="text-align:right;"><?= $lastPayment != null ? h(date("d-m-Y", strtotime($lastPayment))) : null; ?></td>
                        <td style="text-align:left"><?= isset($deadline['Bill']['Payment']['metodo']) ? $deadline['Bill']['Payment']['metodo'] : null; ?></td>
                        <td style="text-align:center;">

                            <?php
                            switch ($statofattura) {
                                case 'D':
                                    ?><span class="rosso">Da pagare</span><?php
                                    break;
                                case 'A':
                                    ?><span class="arancione">Accontata</span><?php
                                    break;
                                case 'S':
                                    ?><span class="verde">Saldata</span><?php
                                    break;
                                case 'I':
                                    ?><span class="rosso" style="padding:5px;">(Insoluto)</span><?php
                                    break;
                                case 'P':
                                    ?><span class="arancione" style="padding:5px;">(Insoluto parziale)</span><?php
                                    break;
                                case 'R' :
                                    ?><span class="azzurro" style="padding:5px;">In pagamento (Ri.BA.)</span><?php
                                    break;
                            }
                            ?>
                        </td>
                        <?php $modifyOffImage = '<img src="' . $this->webroot . 'img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica" class="golIcon"  title = "Scadenza non modificabile">' ?>
                        <td class="actions">
                            <?php
                            $result = 0;
                            //$totalPaymentDeadline = $totalPaymentDeadline * $coefficienteNC;
							$totalPaymentDeadline = $totalPaymentDeadline;
                            switch ($statofattura) {
                                case 'D':
                                    if ($deadline['Bill']['id_ixfe'] != null) {
                                        echo $modifyOffImage;
                                    } else {
                                        echo $this->element('Form/Simplify/Actions/edit', ['id' => $deadline['Deadline']['id']]);
                                    }
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    echo $this->Html->link('<i class="fa fa-money set-payed rosso"  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'settle', $deadline['Deadline']['id']], ['title' => __('Salda\acconta scadenza'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true rosso"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']], ['title' => __('Seleziona scadenza come insoluta'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                case 'A':
                                    echo $modifyOffImage;
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'] - $totalPaymentDeadline;
                                    echo $this->Html->link('<i class="fa fa-money set-payed arancione"   deadlineid = "' . $deadline['Deadline']['id'] . '"  toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'settle', $deadline['Deadline']['id']], ['title' => __('Salda\acconta scadenza'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-list show-payment arancione "  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'showPayment', $deadline['Deadline']['id']], ['title' => __('Visualizza elenco  pagamenti'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true arancione"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']], ['title' => __('Seleziona scadenza come parzialmente insoluta'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                case 'S':
                                    echo $modifyOffImage;
                                    echo $this->Html->link('<i class="fa fa-list show-payment verde"  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'showPayment', $deadline['Deadline']['id']], ['title' => __('Visualizza elenco pagamenti'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                case 'P': // Insoluto parziale
                                    echo $modifyOffImage;
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'] - $totalPaymentDeadline;
                                    echo $this->Html->link('<i class="fa fa-list show-payment arancione"  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'showPayment', $deadline['Deadline']['id']], ['title' => __('Visualizza elenco  pagamenti'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa fa-thumbs-up set-unpaid-false arancione"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidFalse', $deadline['Deadline']['id']], ['title' => __('Rimuovi insoluto parziale'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                case 'I':  // Insoluto
                                    echo $modifyOffImage;
                                    echo $this->Html->link('<i class="fa fa fa-thumbs-up set-unpaid-false rosso"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidFalse', $deadline['Deadline']['id']], ['title' => __('Rimuovi insoluto'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                case 'R':
                                    echo $modifyOffImage;
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    echo $this->Html->link('<i class="fa fa-money set-payed azzurro"  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'settle', $deadline['Deadline']['id']], ['title' => __('Salda\acconta scadenza'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true rosso"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']], ['title' => __('Seleziona scadenza come insoluta'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                                default:
                                    $totalDeadlines += $coefficienteNC * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
                            }
                            $totalToPay = $totalDeadlines - $totalPayed;
                            ?>
                        </td>
                        </tr>
                        <?php
                    }
                } ?>
            <?php } ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align:right;"><b><?= number_format($totalDeadlines, 2, '.', ','); ?></b></td>
                <td style="text-align:right;"><b><?= number_format($totalPayed, 2, '.', ','); ?></b></td>
                <td style="text-align:right;"><b><?= number_format($totalToPay, 2, '.', ','); ?></b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
        <?= $this->element('Form/Components/Paginator/component'); ?>
    </div>
</div>

<?= $this->element('Js/datepickercode'); ?>

<script>
    addLoadEvent(function(){
        setPayedClick();
        showPaymentClick();
        setUnpaidClickTrue();
        setUnpaidClickFalse();
    })

    function setUnpaidClickTrue()
    {
        $('.set-unpaid-true').on('click.setUnpaidClickTrue',
            function (e)
            {
                e.preventDefault()
                var setUnpaid = $(this);

                Frizzyconfirm("<form id=\"scartaccini3\"><br/>Segnare la scadenza come insoluta ?</form >", "Data di pagamento", function (userChoice) {
                    if (userChoice == enhancedDialogsTypes.YES)
                    {
                        $.ajax({
                            method: "POST",
                            url: "<?=  $this->Html->url(["controller" => "deadlines", "action" => "setDeadlineUnpaidTrue"]) ?>",
                            data:
                            {
                                payment_deadline_id: setUnpaid.attr('deadlineid'),
                            },
                            success: function (data)
                            {
                                location.reload();
                            }
                        });

                        return false;
                    }
                })

                return false;
            }
        )
    }

    function setUnpaidClickFalse() {
        $('.set-unpaid-false').on('click.setUnpaidClickFalse', function (e) {
            e.preventDefault()
            var setUnpaid = $(this);
            Frizzyconfirm("<form id=\"scartaccini4\"><br/>Rimuovere insoluto ?</form >", "Data di pagamento", function (userChoice) {
                if (userChoice == enhancedDialogsTypes.YES) {
                    $.ajax({
                        method: "POST",
                        url: "<?=  $this->Html->url(["controller" => "deadlines", "action" => "setDeadlineUnpaidFalse"]) ?>",
                        data:
                            {
                                payment_deadline_id: setUnpaid.attr('deadlineid'),
                            },
                        success: function (data) {
                            location.reload();
                        },
                    });
                    return false;
                }
            })
        })
    }

    function setPayedClick()
    {
        $('.set-payed').on('click.setPayedClick',
            function (e)
            {
                e.preventDefault()
                var setPayed = $(this);

                var paymentDateInput = $('<input style="text-align:center" readonly value="<?= date("d-m-Y"); ?>" >').addClass('form-control payment-date datepicker').val("<?= date("d-m-Y"); ?>");

                if ($(this).attr('toSettle') > 0)
                    var paymentAmount = $('<input style="text-align:right" type="number" step="0.01"  max="' + $(this).attr('toSettle') + '" value="' + $(this).attr('toSettle') + '">').addClass('form-control payment-amount ');
                else
                    var paymentAmount = $('<input style="text-align:right" type="number" step="0.01"   min="' + $(this).attr('toSettle') + '"  max="0"  value="' + $(this).attr('toSettle') + '">').addClass('form-control payment-amount ');

                var paymentNote = $('<input>').addClass('form-control payment-note');

                var canSave = true;

                Frizzyconfirm("<form id=\"scartaccini\"><br/>Inserire una data di pagamento <br/>" + paymentDateInput[0].outerHTML + "<br/>Inserire l'importo saldato <br/>" + paymentAmount[0].outerHTML + "<br/>Inserire eventuale nota <br/>" + paymentNote[0].outerHTML + '</form >', "Data di pagamento", function (userChoice) {
                    if (userChoice == enhancedDialogsTypes.YES)
                    {
                        if ($("#scartaccini")[0].reportValidity() && canSave)
                        {
                            canSave = false;

                            $.ajax({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "deadlines", "action" => "addPayment"]) ?>",
                                data:
                                {
                                    payment_date: $(".payment-date").val(),
                                    payment_amount: $(".payment-amount").val(),
                                    payment_note: $(".payment-note").val(),
                                    payment_deadline_id: setPayed.attr('deadlineid'),
                                },
                                success: function (data)
                                {
                                    location.reload();
                                }
                            });
                        }

                        return false;
                    }
                })

                jQuery(".datepicker").datepicker({"dateFormat": "dd-mm-yy", "showAnim": "slideDown"});

                return false;
            }
        )
    }

    function showPaymentClick() {
        $('.show-payment').on('click.showPaymentClick', function (e) {
            var showpayment = $(this);
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "deadlines", "action" => "showPayment"]) ?>",
                data:
                    {
                        payment_deadline_id: showpayment.attr('deadlineid'),
                    },
                success: function (data) {
                    data = JSON.parse(data);
                    var riga = '';
                    $.each(data,
                        function (key, element) {
                            riga = riga + '<tr id="rowtodeletescadatt' + element.id + '"><td>' + element.payment_date + '</td><td>' + element.payment_amount + '</td><td>' + element.note + '</td><td idToDelete="' + element.id + '"><i class="fa fa-trash icon scadattdelete"></i></td></tr>';
                        })
                    Frizzydialog("Lista pagamenti effettuati", "<table class=\"table table-bordered table-striped table-condensed flip-content\"><tbody><tr><td>Data pagamento</td><td>Importo pagato</td><td>Note</td><td>Azioni</td></tr>" + riga + "</tbody></table>", function () {
                    }, [enhancedDialogsTypes.CLOSE]);
                    $(".scadattdelete").click(function () {
                        removeScadattPayment($(this).parent('td').attr('idToDelete'), showpayment.attr('deadlineid'))
                    });
                }
            });

            return false;
        })
    }


    function removePaymentRow() {
        $(".eliminapagamentoscadatt").click(console.log('nextremove'));
    }

    function indexscadattAfterAjaxFilterCallback() {
        setPayedClick();
        showPaymentClick();
        setUnpaidClickTrue();
        setUnpaidClickFalse();
    }

    function afterAjaxFilterCallback() {
        indexscadattAfterAjaxFilterCallback();
    }


    // Rimozioni pagamenti
    function removeScadattPayment(paymentId, deadlineId) {
        $(".enhanced-dialog-container").hide();
        $.confirm({
            title: 'Eliminazione pagamento della scadenza.',
            content: 'Siete sicuri di voler eliminare il pagamento selezionato ?',
            type: 'blue',
            buttons: {
                Ok: function () {
                    action:
                    {
                        $.ajax({
                            method: "POST",
                            url: "<?= $this->Html->url(["controller" => "deadlines", "action" => "deletePayment"]) ?>",
                            data:
                                {
                                    paymentid: paymentId,
                                },
                            success: function (data) {
                                $.ajax
                                ({
                                    method: "POST",
                                    url: "<?= $this->Html->url(["controller" => "deadlines", "action" => "hasPayments"]) ?>",
                                    data:
                                        {
                                            deadlineId: deadlineId,
                                        },
                                    success: function (data2) {
                                        if (data2 == 0) {
                                            location.reload();
                                        }
                                    },
                                    error: function (data2) {
                                        // nothing
                                    },
                                });
                                $("#rowtodeletescadatt" + paymentId).remove();
                                $(".enhanced-dialog-container").show();
                            },
                            error: function (data) {
                                $(".enhanced-dialog-container").show();
                            },
                        });
                    }
                },
                annulla: function () {
                    action:
                    {
                        // Nothing
                        $(".enhanced-dialog-container").show();
                    }
                },
            }
        });
    }

</script>

