<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<?=	$this->element('Form/formelements/indextitle',['indextitle'=>__('Scadenzario passivo')]); ?>

<div class="clients index">
	<div class="scadpass index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' =>$sortableFields]); ?></tr>
                <tr>
                <?= $this->element('Form/Components/AjaxFilter/component', [
                        'elements' => $filterableFields,
                        'afterAjaxfilterCallback'=>'indexscadpassAfterAjaxFilterCallback',
                        'htmlElements' => [
                            '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input  datepicker" name="data[filters][date1]" value="'. $date1 .'"   bind-filter-event="change"/>'.''.
                            '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input  datepicker" name="data[filters][date2]"  value="'. $date2 .'"  bind-filter-event="change"/></center>',
                            '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker3" class="form-control ajax-filter-input  datepicker" name="data[filters][date3]" value="'. $date3 .'"   bind-filter-event="change"/>'.''.
                            '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker4" class="form-control ajax-filter-input  datepicker" name="data[filters][date4]"  value="'. $date4 .'"  bind-filter-event="change"/></center>',
                            $this->Form->input('filters.tipologia', [  'div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $tipologia , 'empty'=>true, 'bind-filter-event'=>"change"])
                        ]
                    ]);
                ?>
                </tr>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php
				$totalDeadlines = $totalPayed = 0; $coefficienteMoltiplicativo = 1;

				foreach ($deadlines as $deadline)
				{
                    $totalPaymentDeadline = 0;
                    $lastPayment = null;
                    $statofattura = 'D';

                    foreach($deadline['Deadlinepayment'] as $key => $deadlinePayment)
                    {
                        if($deadlinePayment['state'] == 0)
                            unset($deadline['Deadlinepayment'][$key]);
                    }

                    if(!isset($deadline['Deadlinepayment']) || ($deadline['Deadlinepayment'] == null))
                    {
                        $statofattura = 'D';

                        // Segno come insoluto
                        if($deadline['Deadline']['unpaid'] == 1)
                            $statofattura = 'I'; // Insoluto
                    }
                    else
                    {
                        foreach($deadline['Deadlinepayment'] as $payment)
                        {
                            $totalPaymentDeadline += $payment['payment_amount'];
                            $lastPayment = $payment['payment_date'];
                        }

                        // Aggiunto per saldo note di creditp
                            if(round($totalPaymentDeadline,2) > round(($deadline['Deadline']['amount'] + $deadline['Deadline']['vat']),2) && $totalPaymentDeadline < 0)
                            {
                                $statofattura = 'A';

                                // Segno come parzialmente insoluto
                                if($deadline['Deadline']['unpaid'] == 1)
                                {
                                    $statofattura = 'P'; // Insoluto parziale
                                }
                            }

                        else
                        {
                            if(round($totalPaymentDeadline,2) < round(($deadline['Deadline']['amount'] + $deadline['Deadline']['vat']),2) && $totalPaymentDeadline > 0)
                            {

                                $statofattura = 'A';
                                // Segno come parzialmente insoluto
                                if($deadline['Deadline']['unpaid'] == 1)
                                {
                                    $statofattura = 'P'; // Insoluto parziale
                                }
                            }
                        }

                        if(round($totalPaymentDeadline,2) == round(($deadline['Deadline']['amount'] + $deadline['Deadline']['vat']),2))
                        {
                            $statofattura = 'S';
                        }
                    }


                    if(!isset($saldacconto) || $saldacconto == $statofattura)
                    {
					?>
					<tr>
						<?php
						if(sizeof($deadline['Bill']['Supplier']) > 0  && (sizeof($deadline['Bill']['Payment']) > 0 || $deadline['Bill']['imported_bill'] == 1))
						{
							if($deadline['Bill']['imported_bill'] == 1)
							{
								switch($deadline['Bill']['typeofdocument'])
								{
									case 'TD04':
										$coefficienteMoltiplicativo = -1;
										/* fix per valori negativi */
										if($deadline['Deadline']['amount'] < 0)
										    $coefficienteMoltiplicativo = 1;
									break;
									default:
										$coefficienteMoltiplicativo = 1;
									break;
								}
							}
							else
							{
							    $coefficienteMoltiplicativo = 1;
							}
						?>
						<td><?= h($deadline['Bill']['numero_fattura']); ?></td>
						<td style="text-align:center;"><?= h(date("d-m-y",strtotime($deadline['Bill']['date']))); ?></td>
						<td style="text-align:center;"><?= h(date("d-m-y",strtotime($deadline['Deadline']['deadline']))); ?></td>
						<td><?= h($deadline['Bill']['Supplier']['name']); ?></td>
						<td style="text-align:right;"><?=  h(number_format($coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'],2,',','.')); ?></td>
						<td style="text-align:right;"><?=  h( number_format($coefficienteMoltiplicativo * $totalPaymentDeadline,2,',','.')); ?></td>
                    <?php if(isset($deadline['Bill']['Payment']['metodo'])): ?>
                        <td style="text-align:left"><?= $deadline['Bill']['Payment']['metodo']; ?></td>
                    <?php else: ?>
                        <td style="text-align:left"><?= isset($paymentmethodscodes[$deadline['Deadline']['imported_payment_method']]) ? $paymentmethodscodes[$deadline['Deadline']['imported_payment_method']] : '' ?></td>
                    <?php endif; ?>
						<td style="text-align:right;"><?= $lastPayment != null ? h(date("d-m-Y", strtotime($lastPayment))) : null; ?></td>
						<td>
							<?php
								switch($statofattura)
								{
									case 'D':
										?><span class="rosso" >Da pagare</span><?php
										break;
									case 'A':
										?><span class="arancione" >Accontata</span><?php
										break;
									case 'S':
										?><span class="azzurro">Saldata</span><?php
										break;
									case 'I':
										?><span class="rosso" style="padding:5px;">( Insoluto )</span><?php
										break;
									case 'P':
										?><span class="arancione" style="padding:5px;">( Insoluto parziale )</span><?php
										break;
								}
							?>
						</td>
                            <?php $modifyOffImage = '<img src="' . $this->webroot . 'img/gestionaleonlineicon/gestionale-online.net-modifica-off.svg" alt="Modifica" class="golIcon"  title = "Scadenza non modificabile">' ?>

                            <td class="actions">
							<?php
							$result = 0;
							switch($statofattura)
							{
								case 'D':
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    echo $this->Html->link('<i class="fa fa-money set-payed rosso"  deadlineid = "'. $deadline['Deadline']['id'] .'" toSettle="'.	number_format($result,2,'.','').'"></i>', ['action' => 'settle', $deadline['Deadline']['id']],['title'=>__('Salda\acconta scadenza'),'escape'=>false]);
									echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true rosso"  deadlineid = "'. $deadline['Deadline']['id'] .'" toUnpaid="'.	number_format($result,2,'.','').'"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']],['title'=>__('Seleziona scadenza come insoluta'),'escape'=>false]);
									$totalDeadlines +=  $coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
									$totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;
								break;
								case 'A':/*
                                    $result =$deadline['Deadline']['amount'] + $deadline['Deadline']['vat'] - $totalPaymentDeadline;
                                    echo $this->Html->link('<i class="fa fa-money set-payed arancione "   deadlineid = "'. $deadline['Deadline']['id'] .'"  toSettle="'.	number_format($result,2,'.','').'"></i>', ['action' => 'settle', $deadline['Deadline']['id']],['title'=>__('Salda\acconta scadenza'),'escape'=>false]);
                                    echo $this->Html->link('<i class="fa fa-list show-payment arancione "  deadlineid = "'. $deadline['Deadline']['id'] .'" toSettle="'.	number_format($result,2,'','.').'"></i>', ['action' => 'settle', $deadline['Deadline']['id']],['title'=>__('Visualizza elenco  pagamenti'),'escape'=>false]);
                                    echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true arancione "  deadlineid = "'. $deadline['Deadline']['id'] .'" toUnpaid="'.	number_format($result,2,'.','').'"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']],['title'=>__('Seleziona scadenza come parzialmente insoluta'),'escape'=>false]);
                                    $totalDeadlines +=  $coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;*/
                                    echo $modifyOffImage;
                                    $result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'] - $totalPaymentDeadline;
                                    echo $this->Html->link('<i class="fa fa-money set-payed arancione"   deadlineid = "' . $deadline['Deadline']['id'] . '"  toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'settle', $deadline['Deadline']['id']], ['title' => __('Salda\acconta scadenza'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-list show-payment arancione "  deadlineid = "' . $deadline['Deadline']['id'] . '" toSettle="' . number_format($coefficienteNC * $result, 2, '.', '') . '"></i>', ['action' => 'showPayment', $deadline['Deadline']['id']], ['title' => __('Visualizza elenco  pagamenti'), 'escape' => false]);
                                    echo $this->Html->link('<i class="fa fa-thumbs-down set-unpaid-true arancione"  deadlineid = "' . $deadline['Deadline']['id'] . '" toUnpaid="' . number_format($result, 2, '.', '') . '"></i>', ['action' => 'setDeadlineUnpaidTrue', $deadline['Deadline']['id']], ['title' => __('Seleziona scadenza come parzialmente insoluta'), 'escape' => false]);
                                    $totalDeadlines += $coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
                                    $totalPayed += $totalPaymentDeadline;
                                    break;
								case 'S':
									echo $this->Html->link('<i class="fa fa-list show-payment azzurro"  deadlineid = "'. $deadline['Deadline']['id'] .'" toSettle="'.	number_format($result,2,'','.').'"></i>', ['action' => 'settle', $deadline['Deadline']['id']],['title'=>__('Visualizza elenco pagamenti'),'escape'=>false]);
									$totalDeadlines +=  $coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
									$totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;
								break;
								case 'P': // Insoluto parziale
									$result = $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'] - $totalPaymentDeadline;
									echo $this->Html->link('<i class="fa fa-list show-payment arancione"  deadlineid = "'. $deadline['Deadline']['id'] .'" toSettle="'.	number_format($result,2,'.','').'"></i>', ['action' => 'showPayment', $deadline['Deadline']['id']],['title'=>__('Visualizza elenco  pagamenti'),'escape'=>false]);
									echo $this->Html->link('<i class="fa fa fa-thumbs-up set-unpaid-false arancione"  deadlineid = "'. $deadline['Deadline']['id'] .'" toUnpaid="'.	number_format($result,2,'.','').'"></i>', ['action' => 'setDeadlineUnpaidFalse', $deadline['Deadline']['id']],['title'=>__('Rimuovi insoluto parziale'),'escape'=>false]);
									$totalDeadlines += $coefficienteMoltiplicativo * $deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
									$totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;
								break;
								case 'I':  // Insoluto
									echo $this->Html->link('<i class="fa fa fa-thumbs-up set-unpaid-false rosso"  deadlineid = "'. $deadline['Deadline']['id'] .'" toUnpaid="'.	number_format($result,2,'.','').'"></i>', ['action' => 'setDeadlineUnpaidFalse', $deadline['Deadline']['id']],['title'=>__('Rimuovi insoluto'),'escape'=>false]);
									$totalDeadlines += $coefficienteMoltiplicativo *$deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
									$totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;
								break;
								default:
									$totalDeadlines += $coefficienteMoltiplicativo *$deadline['Deadline']['amount'] + $deadline['Deadline']['vat'];
									$totalPayed += $coefficienteMoltiplicativo * $totalPaymentDeadline;
								break;
							}
							?>
						</td>
					</tr>
					<?php
						}
                    }
                }
            ?>
                <tr>
                    <td colspan="4"></td>
                    <td><b>Pagato</b></td>
                    <td style="text-align:right;"><?= number_format($totalPayed,2,'.',','); ?></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                    <td><b>Da pagare</b></td>
                    <td style="text-align:right;"><?= number_format($totalDeadlines - $totalPayed,2,'.',','); ?></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                    <td><b>Totale</b></td>
                    <td style="text-align:right;"><?= number_format($totalDeadlines,2,'.',','); ?></td>
                    <td colspan="4"></td>
                </tr>
			</tbody>
		</table>
		<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>

<?= $this->element('Js/datepickercode'); ?>

<script>
	addLoadEvent(function(){
        setPayedClick();
        showPaymentClick();
        setUnpaidClickTrue();
        setUnpaidClickFalse();
    })

	function setPayedClick()
    {
		$('.set-payed').on('click.setPayedClick',
            function(e)
            {
                e.preventDefault()
                var setPayed = $(this);

                var paymentDateInput = $('<input style="text-align:center" readonly value="<?= date("d-m-Y"); ?>" >').addClass('form-control payment-date datepicker').val("<?= date("d-m-Y"); ?>");

                if($(this).attr('toSettle') > 0)
                    var paymentAmount = $('<input style="text-align:right" type="number" step="0.01" min="0" max="'+$(this).attr('toSettle')+'" value="'+$(this).attr('toSettle')+'">').addClass('form-control payment-amount ');
                else
                    var paymentAmount = $('<input style="text-align:right" type="number" step="0.01"   min="'+$(this).attr('toSettle')+'"  max="0" value="'+$(this).attr('toSettle')+'">').addClass('form-control payment-amount ');

                var paymentNote = $('<input>').addClass('form-control payment-note');

			    var canSave = true;

                Frizzyconfirm("<form id=\"scartaccini\" ><br/>Inserire una data di pagamento <br/>" + paymentDateInput[0].outerHTML +"<br/>Inserire l'importo saldato <br/>"+ paymentAmount[0].outerHTML +"<br/>Inserire eventuale nota <br/>"+ paymentNote[0].outerHTML + '</form>' ,"Data di pagamento",function(userChoice) {
                    if(userChoice == enhancedDialogsTypes.YES)
                    {
                        if($("#scartaccini")[0].reportValidity() && canSave)
                        {
                            canSave = false;

                            $.ajax({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "deadlines","action" => "addPayment"]) ?>",
                                data:
                                {
                                    payment_date : $(".payment-date").val(),
                                    payment_amount : $(".payment-amount").val(),
                                    payment_note : $(".payment-note").val(),
                                    payment_deadline_id : setPayed.attr('deadlineid'),
                                },
                                success: function(data)
                                {
                                    location.reload();
                                }
                            });
                        }

                        return false;
                    }
                })

	    	    jQuery(".datepicker").datepicker({	"dateFormat" : "dd-mm-yy" ,	"showAnim" : "slideDown"	}) ;

		        return false;
		    }
        )
	}

    function setUnpaidClickTrue()
	{
		$('.set-unpaid-true').on('click.setUnpaidClickTrue',
            function(e)
		    {
                e.preventDefault()
                var setUnpaid = $(this);

			    Frizzyconfirm("<form id=\"scartaccini3\"><br/>Segnare la scadenza come insoluta ?</form >" ,"Data di pagamento",function(userChoice) {
                    if(userChoice == enhancedDialogsTypes.YES)
                    {
		    			$.ajax({
							method: "POST",
							url: "<?=  $this->Html->url(["controller" => "deadlines","action" => "setDeadlineUnpaidTrue"]) ?>",
							data:
							{
								payment_deadline_id : setUnpaid.attr('deadlineid'),
							},
							success: function(data)
							{
								location.reload();
							}
		    			});

					    return false;
	    		    }
		        })

		        return false;
		    }
        )
	}

	function setUnpaidClickFalse()
	{
		$('.set-unpaid-false').on('click.setUnpaidClickFalse',
            function(e)
		    {
                e.preventDefault()
                var setUnpaid = $(this);

			    Frizzyconfirm("<form id=\"scartaccini4\"><br/>Rimuovere insoluto ?</form >" ,"Data di pagamento",function(userChoice) {
                    if(userChoice == enhancedDialogsTypes.YES)
                    {
                        $.ajax({
                            method: "POST",
                            url: "<?=  $this->Html->url(["controller" => "deadlines","action" => "setDeadlineUnpaidFalse"]) ?>",
                            data:
                            {
                                payment_deadline_id : setUnpaid.attr('deadlineid'),
                            },
                            success: function(data)
                            {
                                location.reload();
                            },
                        });

                        return false;

                    }
		        })
		    }
        )
	}

	function showPaymentClick()
	{
		$('.show-payment').on('click.showPaymentClick',
            function(e)
            {
                var showpayment = $(this);

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "deadlines","action" => "showPayment"]) ?>",
                    data:
                    {
                        payment_deadline_id : showpayment.attr('deadlineid'),
                    },
                    success: function(data)
                    {
                        data = JSON.parse(data);
                        var riga = '';

                        $.each(data, function(key, element)
                            {
                                riga = riga + '<tr id="rowtodeletescadpass'+element.id+'"><td>'+element.payment_date+'</td><td >'+element.payment_amount+'</td><td>'+element.note+'</td><td idToDelete="'+element.id+'"><i class="fa fa-trash icon scadpassdelete"></i></td></tr>';
                            }
                        )

                        Frizzydialog("Lista pagamenti effettuati","<table class=\"table table-bordered table-striped table-condensed flip-content\"><tbody><tr><td>Data pagamento</td><td>Importo pagato</td><td>Note</td><td>Azioni</td></tr>"+riga+"</tbody></table>",
                            function()
                            {
                            }, [ enhancedDialogsTypes.CLOSE ]
                        );

                        $(".scadpassdelete").click(function(){ removeScadpassPayment($(this).parent('td').attr('idToDelete'),showpayment.attr('deadlineid')) });
                    }
                });

                return false;
	        }
        )
	}

    function indexscadpassAfterAjaxFilterCallback()
    {
        setPayedClick();
        showPaymentClick();
        setUnpaidClickTrue();
        setUnpaidClickFalse();
    }

    function afterAjaxFilterCallback()
    {
        indexscadpassAfterAjaxFilterCallback();
    }

    // Rimozioni pagamenti
	function removeScadpassPayment(paymentId,deadlineId)
	{
		$(".enhanced-dialog-container").hide();

		$.confirm({
			title: 'Eliminazione pagamento della scadenza.',
    		content: 'Siete sicuri di voler eliminare il pagamento selezionato ?',
    		type: 'blue',
    		buttons:
            {
        		Ok: function ()
        		{
        			action:
        			{
						$.ajax({
							method: "POST",
							url: "<?= $this->Html->url(["controller" => "deadlines","action" => "deletePayment"]) ?>",
							data:
							{
								paymentid : paymentId,
							},
							success: function(data)
							{
								$.ajax({
									method: "POST",
									url: "<?= $this->Html->url(["controller" => "deadlines","action" => "hasPayments"]) ?>",
									data:
									{
										deadlineId : deadlineId,
									},
									success: function(data2)
									{
										if(data2 == 0)
										{
											location.reload();
										}
									},
									error: function (data2)
									{
									},
								});

								$("#rowtodeletescadpass"+paymentId).remove();
								$(".enhanced-dialog-container").show();
							},
							error: function(data)
							{
								$(".enhanced-dialog-container").show();
							},
						});
        			}
        		},
        		annulla: function ()
        		{
        			action:
        			{
                        $(".enhanced-dialog-container").show();
        			}
        		},
            }
        });
	}

</script>
