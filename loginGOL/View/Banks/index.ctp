
<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Banche','indexelements' => ['add'=>'Nuova Banca']]); ?>
<div class="clients index">
	<div class="banks index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<?php if (count($banks) > 0) { ?>
				<tr>
					<?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?>
				</tr>
				<?php }
					else
					{
						?><tr><td colspan="5"><center>nessuna banca trovata</center></td></tr><?php				
					}
				?>
			</thead>
			<tbody class="ajax-filter-table-content">
				<?php foreach ($banks as $bank) 
				{
				?>
					<tr>
						<td><?php echo h($bank['Bank']['description']); ?></td>
						<?php if (RIBA_FLOW) { ?>	
						<td><?php echo h($bank['Bank']['abi']); ?></td>
						<td><?php echo h($bank['Bank']['cab']); ?></td>
						<?php } ?>	
						<td><?php echo h($bank['Bank']['iban']); ?></td>
						<td class="actions">
							<?=	$this->element('Form/Simplify/Actions/edit',['id' => $bank['Bank']['id']]); ?>
							<?= $this->element('Form/Simplify/Actions/delete',['id' =>$bank['Bank']['id'],'message' => 'Sei sicuro di voler eliminare la banca ?']);  ?>
						</td>
					</tr>
				<?php 
				}
				?>
			</tbody>
		</table>
			<?=  $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
