<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableedit'); ?>
<?= $this->element('Js/clonableadvanced'); ?>
<?= $this->element('Js/addcrossremoving'); ?>
<?= $this->element('Js/addcrossremovingddt'); ?>

<?php if($this->request->data['Maintenance']['signimage'] == ''): ?>
    <?= $this->element('Form/Components/SignaturePad/loader'); ?>
<?php endif;?>

<?= $this->Form->create('Maintenance'); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __("Modifica scheda d'intervento") ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-2 col-sm-6" >
        <label class="form-label"><strong>Numero scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_number', ['label' => false,'class' => 'form-control','required'=>true,'readonly'=>'readonly']); ?>
        </div>
    </div>
    <div class="col-md-2 col-sm-6" >
        <label class="form-label form-margin-top"><strong>Data scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input  type="datetime" id="datepicker" class="datepicker segnalazioni-input form-control" name="data[Maintenance][maintenance_date]" value="<?= date("d-m-Y", strtotime($this->request->data['Maintenance']['maintenance_date'])); ?>"  required />
        </div>
    </div>
</div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-6 col-sm-6" >
        <label class="form-label form-margin-top;" ><strong>Cliente</strong></label>
        <div class="form-controls">
            <?=
                $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'client_id',
                    "aggregator" => '',
                    "prefix" => "client_id_maintenances",
                    "list" => $clients,
                    "options" =>  [  'multiple' => false ,'required'=>false ],
                ]);
            ?>
        </div>
    </div>
    <div class="col-md-6 col-sm-6" >
        <label class="form-label form-margin-top"><strong>Cantiere</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'constructionsite_id',
                    "aggregator" => '',
                    "prefix" => "constructionsite_id",
                    "list" => $constructionsites,
                    "options" => [ 'multiple' => false,'required'=> true,'value'=>$this->request->data['Maintenance']['constructionsite_id']],
                ]);
            ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12">
        <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'quote_id',
                    "aggregator" => '',
                    "prefix" => "quote_id",
                    "list" => $quotes,
                    "options" => [ 'multiple' => false,'required'=> false,'value'=>$this->request->data['Maintenance']['quote_id']],
                ]);
            ?>
        </div>
    </div>
</div>

<div class ="form-group clientdetails col-md-12 col-sm-12 ">
    <div class="col-md-4 col-sm-4">
        <label class="form-label form-margin-top"><strong>Indirizzo cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_address',['label' => false, 'div' =>false, 'class'=>'form-control']);?>
        </div>
    </div>
    <div class="col-md-2 col-sm-4" >
        <label class="form-label form-margin-top"><strong>CAP cantiere</strong></label>
        <div class="form-controls">
            <?=  $this->Form->input('maintenance_cap',['label' => false, 'div' =>false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5 ]); ?>
        </div>
    </div>
    <div class="col-md-2 col-sm-4" >
        <label class="form-label form-margin-top"><strong>Città cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_city',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2 col-sm-5" >
        <label class="form-label form-margin-top"><strong>Provincia cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_province',['label' => false, 'div' =>false, 'class'=>'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2 col-sm-5">
        <label class="form-label form-margin-top"><strong>Nazione cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'maintenance_nation',
                    "aggregator" => '',
                    "prefix" => "maintenance_nation",
                    "list" => $nations,
                    "options" => [ 'multiple' => false,'required'=> false],
                ]);
            ?>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12"><hr></div>

<div class="col-md-12 col-sm-12">
    <div class="form-group caption-subject bold uppercase col-md-12 col-sm-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Orari intervento</div>
    <div class="col-md-3 col-md-offset-9 maintenanceshours" id="maintenanceshours">
        <div class="form-group">
            <a id="aggiungi_orario" href="javascript:;">
                <span class="blue-button btn-outline dropdown-toggle add_row_clonable" style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                    <?= 'Aggiungi riga'; ?>
                </span>
            </a>
        </div>
    </div>
    <fieldset id="maintenances_hours" class="col-md-12 col-sm-12">
    <?php
        $contohour = count($this->request->data['Maintenancehour']) - 1;
        $j = -1;
    ?>
    <?php foreach ($this->request->data['Maintenancehour'] as $chiavehour => $oggettohour): ?>
            <?php
                $j++;
                $chiavehour == $contohour ? $classehour = "ultima_rigaHours" : $classehour = '';
                $chiavehour == 0 ? $classe1hour = "originaleHours" : $classe1hour = '';
            ?>
            <div class="principaleHours clonableRowHours contacts_row <?= $classe1hour ?>  <?= $classehour ?>" id="<?= $chiavehour ?>">
                <span class="remove icon rimuoviRigaIconHours cross fa fa-remove" title="Rimuovi riga" style="float:right" hidden></span>
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-2 col-sm-12" >
                        <label class="form-label form-margin-top"><strong>Inizio intervento</strong><i class="fa fa-asterisk"></i></label>
                        <div class="form-controls">
                            <input type="time" class="segnalazioni-input form-control hour_from" id="Maintenances<?= $chiavehour ?>maintenance_hour_from" name="data[Hours][<?= $chiavehour ?>][maintenance_hour_from]" value = <?=$oggettohour['hour_from']?> required/>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12" >
                        <label class="form-label form-margin-top"><strong>Fine intervento</strong><i class="fa fa-asterisk"></i></label>
                        <div class="form-controls">
                            <input type="time" class="segnalazioni-input form-control hour_to" id="Maintenances<?= $chiavehour ?>maintenance_hour_to" name="data[Hours][<?= $chiavehour ?>][maintenance_hour_to]" value = <?=$oggettohour['hour_to']?> required/>
                        </div>
                    </div>
                    <div class="form-group col-md-1 col-sm-12" id="technician">
                        <label class="form-label form-margin-top" ><strong>Tecnico</strong></label>
                        <div class="form-controls">
                            <?= $this->Form->input('partecipating', [ 'id'=>"Maintenances". $chiavehour ."maintenance_partecipating",'name'=>"data[Hours][$chiavehour][maintenance_partecipating]", 'div' => false,'type' => 'select','label' => false,'class' => 'form-control','options'=>['1'=>'Si','0'=>'No'],'empty'=>false,'default'=>1,'value' => $oggettohour['partecipating']]); ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <label class="form-label "><strong>Tecnici</strong></label>
                        <div class="form-controls">
                        <?php
                            $arrayOfSelected = [];

                            foreach ($oggettohour['Maintenancehourstechnician'] as $hourTechnician)
                                $arrayOfSelected[] = $hourTechnician['technician_id'];

                            $chiavehour == 0 ? $tempValue =  '' : $tempValue = $chiavehour;
                        ?>
                        <?=
                            $this->element('Form/Components/FilterableSelect/component', [
                                "name" => 'technician_id',
                                "aggregator" => '',
                                "prefix" => "technician_id",
                                "suffix" => '',
                                "list" => $technicians,
                                "id" => 'technician_id_multiple_select'.$tempValue,
                                "options" => ['multiple' => true, 'value' => $arrayOfSelected, 'empty'=>false]
                            ]);
                        ?>
                        </div>

                        <script>
                            $("#technician_id_multiple_select<?= $tempValue ?>").attr("name", "data[Hours][<?= $chiavehour ?>][technician_id_multiple_select][]" )
                        </script>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <label class="form-label "><strong>Operatori esterni</strong></label>
                        <div class="form-controls">
                        <?php
                            $arrayOfSelected = [];

                            foreach ($oggettohour['Maintenancehoursoutsideoperator'] as $opHour)
                                $arrayOfSelected[] = $opHour['operator_id'];

                            $chiavehour == 0 ? $tempValue =  '' : $tempValue = $chiavehour;
                        ?>
                        <?=
                            $this->element('Form/Components/FilterableSelect/component', [
                                "name" => 'operator_id',
                                "aggregator" => '',
                                "prefix" => "operator_id",
                                "suffix" => '',
                                "list" => $outsideoperators,
                                "id" => 'operator_id_multiple_select'.$tempValue,
                                "options" => ['multiple' => true,'value' => $arrayOfSelected,'empty'=>false]
                            ]);
                        ?>
                        </div>

                        <script>
                              $("#operator_id_multiple_select<?= $tempValue ?>").attr("name", "data[Hours][<?= $chiavehour ?>][operator_id_multiple_select][]" )
                        </script>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </fieldset>
</div>

<div class="col-md-12 col-sm-12"><hr></div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12"  >
       <label class="form-margin-top form-label"><strong>Descrizione dell'intervento</strong></label>
       <div class="form-controls">
           <?= $this->Form->textarea('intervention_description', ['div' => false, 'label' => false, 'class'=>'form-control','rows'=>5]); ?>
       </div>
    </div>
</div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12"   >
        <label class="form-margin-top form-label"><strong>Note intervento</strong></label>
        <div class="form-controls">
            <?= $this->Form->textarea('notes', ['div' => false, 'label' => false, 'class'=>'form-control','rows'=>5]); ?>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12"><hr></div>

<div class="col-md-12 col-sm-12">
    <div class="form-group caption-subject bold uppercase col-md-12 col-sm-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">DDT Di riferimento
        <span class="blue-button btn-outline dropdown-toggle enablemaintenanceddtrowddt " style="min-width:250px;text-align:center;float:right;margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;cursor:pointer;'" >
            <?= 'Aggiungi bolle materiale' ?>
        </span>
    </div>
    <div class="col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-md-3 col-md-offset-9 col-sm-offset-9 col-sm-3 maintenancesddt" id="maintenancesddt" hidden>
        <div class="form-group col-md-12 col-sm-12">
            <a id="aggiungi_bolla" href="javascript:;">
                 <span class = "blue-button btn-outline dropdown-toggle add_row_clonable" style="min-width:112px;text-align:center;float:right;margin-right:10px;padding:4px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                    <?= 'Aggiungi riga'; ?>
                </span>
            </a>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">&nbsp;</div>
    <fieldset id="maintenancesddt" class="col-md-12 col-sm-12 maintenancesddt" hidden>
        <?php if(count($this->request->data['Maintenanceddt']) > 0): ?>
            <?php
                $contoddt = count($this->request->data['Maintenanceddt']) - 1;
                $j = -1;
            ?>
             <?php foreach ($this->request->data['Maintenanceddt'] as $chiaveddt => $oggettoddt): ?>
                <?php if($oggettoddt['state'] == 1): ?>
                    <?php
                        $j++;
                        $chiaveddt == $contoddt ? $classeddt = "ultima_rigaddt" : $classeddt = '';
                        $j == 0 ? $classe1ddt = "originaleddt" : $classe1ddt = '';
                    ?>
                    <div class="principaleddt lunghezzaddt contacts_row clonableRowddt <?= $classe1ddt ?>  <?= $classeddt ?>" id="<?= $j ?>">
                        <span class="remove icon rimuoviRigaIconddt cross fa fa-remove" title="Rimuovi riga" style="float:right"></span>
                        <div class="col-md-12">
                            <div class="col-md-2 col-sm-2 jsRowFieldDescription">
                                <label class="form-label"><strong>Numero ddt</strong></label>
                                <?= $this->Form->input("Ddt.$j.ddtnumber", ['label' => false, 'class'=>'form-control','div' => false,'value'=>$oggettoddt['number']]); ?>
                            </div>
                            <div class="col-md-2 col-sm-3" >
                                <label class="form-label form-margin-top"><strong>Data ddt</strong></label>
                                <div class="form-controls">
                                    <input  type="datetime"  class="datepicker segnalazioni-input form-control" name="data[Ddt][<?= $j ?>][ddtdate]"  id="Ddt<?= $j ?>Ddtdate" value="<?= date("d-m-Y",strtotime($oggettoddt['date'])); ?>"    />
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" >
                                <label class="form-label form-margin-top"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
                                <div class="form-controls">
                                    <?php $j == 0 ?  $tempValue =  ''  :  $tempValue = $j; ?>
                                    <?=
                                        $this->element('Form/Components/FilterableSelect/component', [
                                            "name" =>"rowsupplier_id",
                                            "aggregator" => '',
                                            "prefix" => "supplier_id",
                                            "list" => $suppliers,
                                            "id" => 'supplier_id_multiple_select'.$tempValue,
                                            "options" => ['multiple' => false,'value'=>$oggettoddt['supplier_id']],
                                        ]);
                                    ?>
                                    <script>
                                      $("#supplier_id_multiple_select<?= $tempValue ?>").attr("name", "data[Ddt][<?= $j ?>][supplier_id_multiple_select]" )
                                    </script>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-3 jsRowField">
                                <label class="form-label"><strong>Valore</strong></label>
                                <?= $this->Form->input("Ddt.$j.ddtvalue", ['label' => false, 'class'=>'form-control', 'div' => false,'value'=>$oggettoddt['value']]); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="principaleddt contacts_row clonableRowddt originaleddt ultima_rigaddt">
                <span class="remove icon rimuoviRigaIconddt cross fa fa-remove" title="Rimuovi riga" style="float:right" hidden></span>
                <div class="col-md-12">
                    <div class="col-md-2 jsRowFieldDescription">
                        <label class="form-label"><strong>Numero ddt</strong></label>
                        <?= $this->Form->input('Ddt.0.ddtnumber', ['label' => false, 'class' => 'form-control', 'div' => false]); ?>
                    </div>
                    <div class="col-md-2" style="float:left;">
                        <label class="form-label form-margin-top"><strong>Data ddt</strong></label>
                        <div class="form-controls"> <!-- QUI -->
                            <input type="datetime" class="datepicker2 segnalazioni-input form-control" name="Ddt.0.ddtdate" id="Ddt0Ddtdate" value="<?= date("d-m-Y"); ?>"/>
                        </div>
                    </div>
                    <div class="col-md-4" style="float:left;margin-left:10px;">
                        <label class="form-label form-margin-top supplier-label"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
                        <div class="form-controls">
                            <?= $this->element('Form/Components/FilterableSelect/component', [
                                "name" => 'rowsupplier_id',
                                "aggregator" => '',
                                "prefix" => "supplier_id",
                                "list" => $suppliers,
                                "options" => ['multiple' => false],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-1 jsRowField">
                        <label class="form-label"><strong>Valore</strong></label>
                        <?= $this->Form->input('Ddt.0.ddtvalue', ['label' => false, 'class' => 'form-control', 'div' => false]); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </fieldset>
</div>

<div class="col-md-12 col-sm-12"><hr></div>

<div class="col-md-12 col-sm-12">
    <div class="form-group caption-subject bold uppercase col-md-12 col-sm-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Materiali utilizzati
        <span class="blue-button btn-outline dropdown-toggle enablemaintenanceaddrow " style="min-width:250px;text-align:center;float:right;margin-right:10px;padding:5px;font-size:13pt;text-align:center;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;cursor:pointer;'">
            <?= 'Aggiungi articoli' ?>
        </span>
    </div>
    <span class="maintenances" hidden>
        <?= $this->element('Form/Simplify/action_add_clonable_row'); ?>
    </span>
    <div class="col-md-12 col-sm-12">&nbsp;</div>
    <fieldset id="maintenances" class="col-md-12 col-sm-12 maintenances" hidden>
        <?php if(count($this->request->data['Maintenancerow']) > 0): ?>
            <?php
                $conto = count($this->request->data['Maintenancerow']) - 1;
                $i = -1;
            ?>
            <script>
                var articoli = setArticles();
                var codici = setCodes();
            </script>
            <?php foreach ($this->request->data['Maintenancerow'] as $chiave => $oggetto): ?>
        <?php $chiave == $conto ? $classe = "ultima_riga" : $classe = ''; ?>
        <?php if($oggetto['state'] == 1): ?>
        <?php
        $i++;
        $i == 0 ? $classe1 = "originale" : $classe1 = '';
        ?>
            <div class="principale lunghezza contacts_row clonableRow  <?= $classe1 ?>  <?= $classe ?>" id="<?= $i ?>">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden ></span>
                <div class="col-md-12 col-sm-12">
                    <?php if(ADVANCED_STORAGE_ENABLED): ?>
                        <div class="col-md-2 col-sm-4  jsRowField">
                            <label class="form-label"><strong>Codice</strong></label>
                            <?= $this->Form->input("Maintenancerow.$i.codice", ['label' => false, 'class'=>'form-control jsCodice ','div' => false]); ?>
                        </div>
                    <?php else: ?>
                        <?= $this->Form->hidden("Maintenancerow.$i.tipo", ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]); ?>
                        <div class="col-md-2  col-sm-4 jsRowField">
                            <label class="form-label jsRowField"><strong>Codice</strong></label>
                            <?= $this->Form->input("Maintenancerow.$i.codice", ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-3 col-sm-8 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input("Maintenancerow.$i.description", ['label' => false, 'class'=>'form-control jsDescription','div' => false]); ?>
                        <?= $this->Form->hidden("Maintenancerow.$i.storage_id"); ?>
                        <?= $this->Form->hidden("Maintenancerow.$i.variation_id",['type'=>'text']); ?>
                        <?= $this->Form->hidden("Maintenancerow.$i.movable",['class'=>'jsMovable','value'=>1]); ?>
                    </div>
                    <div class="col-md-3 col-sm-6 jsRowField">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input("Maintenancerow.$i.customdescription", ['label' => false, 'class'=>'form-control ','div' => false, 'type'=>'textarea','style'=>'height:29px']); ?>
                    </div>
                    <div class="col-md-2 col-sm-2 jsRowField">
                        <label class="form-label"><strong>Quantità <i class="fa fa-warning Transportgood.0.iconwarning" style="color:red;display:none;" ></i></strong><i class="fa fa-asterisk"></i></label>
                        <?=  $this->Form->input("Maintenancerow.$i.quantity", ['label' => false, 'required'=>true,  'class'=>'form-control jsQuantity', 'div' => false]); ?>
                    </div>
                    <div class="col-md-2 col-sm-4 jsRowField">
                        <label class="form-label"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input("Maintenancerow.$i.unit_of_measure_id", ['label' => false, 'empty' => 'sel.', 'default' => 'sel.','class'=>'form-control', 'div' => false,'empty'=>true,'type'=>'select','options'=>$units]); ?>
                    </div>
                </div>
            </div>

            <script>
                loadClientCatalog(<?= $i ?>,'<?= addslashes($this->request->data['Client']['ragionesociale']); ?>',"#MaintenanceClientId","Maintenancerow");
            </script>
        <?php endif; ?>
        <?php endforeach; ?>
        <?php else: ?>
            <div class="principale contacts_row clonableRow originale ultima_riga">
                <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
                <div class="col-md-12">
                    <?php if (ADVANCED_STORAGE_ENABLED): ?>
                        <div class="col-md-2  col-sm-4 jsRowField">
                            <label class="form-label"><strong>Codice</strong></label>
                            <?= $this->Form->input('Good.0.codice', ['label' => false, 'class' => 'form-control jsCodice ', 'div' => false]); ?>
                        </div>
                    <?php else: ?>
                        <?= $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]); ?>
                        <div class="col-md-2 col-sm-4 jsRowField">
                            <label class="form-label jsRowField"><strong>Codice</strong></label>
                            <?= $this->Form->input('Good.0.codice', ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-3 col-sm-8 jsRowFieldDescription">
                        <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.description', ['label' => false, 'class' => 'form-control jsDescription jsDescription2', 'div' => false]); ?>
                        <?= $this->Form->hidden('Good.0.storage_id'); ?>
                        <?= $this->Form->hidden('Good.0.variation_id', ['type' => 'text']); ?>
                        <?= $this->Form->hidden('Good.0.movable', ['class' => 'jsMovable', 'value' => 1]); ?>
                    </div>
                    <div class="col-md-3 col-sm-6 jsRowField">
                        <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                        <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px']); ?>
                    </div>
                    <div class="col-md-2 col-sm-2 jsRowField">
                        <label class="form-label">
                            <strong>Quantità<i class="fa fa-warning Transportgood.0.iconwarning" style="color:red;display:none;"></i></strong><i class="fa fa-asterisk"></i>
                        </label>
                        <?= $this->Form->input('Good.0.quantity', ['label' => false, 'class' => 'form-control jsQuantity', 'div' => false]); ?>
                    </div>
                    <div class="col-md-2 col-sm-4 jsRowField">
                        <label class="form-label"><strong>Unità di misura</strong></label>
                        <?= $this->Form->input('Good.0.unit_of_measure_id', ['label' => false, 'empty' => 'sel.', 'default' => 'sel.', 'class' => 'form-control', 'div' => false, 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12"><hr></div>
            </div>
        <?php endif; ?>
    </fieldset>
</div>

<center>
    <?php if($this->request->data['Maintenance']['signimage'] != ''): ?>
        <div class="col-md-6 col-sm-12" >
            <label class="form-label"><strong>Cognome e nome</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('handling', ['label' => false,'class' => 'form-control','required'=>true , 'readonly'=>'readonly']); ?>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <label class="form-label"><strong>Email per invio scheda d'inervento</strong> </label>
            <div class="form-controls">
                <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="col-md-12 col-sm-12"  >
        <img  src="<?= $this->request->data['Maintenance']['signimage']; ?>" >
    </div>
    <?php else: ?>
        <div class="col-md-6 col-sm-12" >
            <label class="form-label"><strong>Cognome e nome</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('handling', ['label' => false,'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <label class="form-label"><strong>Email, per invio scheda d'inervento</strong> </label>
            <div class="form-controls">
                <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control']); ?>
            </div>
        </div>
        <div class="col-md-12 col-sm-12">&nbsp;</div>
        <div id="sketchcontainer" class="col-md-12 col-sm-12" style="margin-bottom:20px;">
            <div class="col-md-1 col-sm-1"> </div>
            <div class="col-md-10 col-sm-10">
                <?= $this->element('Form/Components/SignaturePad/component'); ?>
            </div>
            <div class="col-md-1 col-sm-1"> </div>
        </div>
    <?php endif; ?>

    <?= $this->Form->button(__('Salva'),array('name'=>'redirect', 'value' => 'technicianadd', 'class'=>'btn blue-button new-bill salva', 'id'=>'save','style'=>'background-color:#ea5d0b;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;'));?>
    <?= $this->Html->link(__('Annulla'), ['action' => 'technicianindex'],array('class'=>'btn blue-button-reverse new-bill cancel', 'escape' =>false,'style'=>'background-color:#dadfe0;font-weight:normal;font-family: \'Barlow Semi Condensed\' !important;text-transform:none;')); ?>
</center>

<?= $this->Form->end(); ?>

<script>
    function addcrossremovinghours()
    {
        $(".rimuoviRigaIconHours").unbind('click');
        $(".rimuoviRigaIconHours").click(function () {

            if ($(".rimuoviRigaIconHours").length > 1)
            {
                if ($(this).parent('.principaleHours').hasClass('ultima_rigaHours'))
                {
                    $(this).parent('.principaleHours').remove();
                    $('.principaleHours').last().addClass('ultima_rigaHours');
                }
                else
                {
                    $(this).parent('.principaleHours').remove();
                }

                addcrossremoving();
            }
            else
            {
                $.alert({
                    icon: 'fa fa-warning',
                    title: '',
                    content: 'Attenzione, deve essere sempre presente almeno una riga.',
                    type: 'orange',
                });
            }
        });
    }

    var tecnico = <?= json_encode($technician) ?>;

    var clonato;
    var nuova_chiave = <?= count($this->request->data['Maintenancehour']); ?>;
    addcrossremovinghours();

    $("#aggiungi_orario").click(
        function ()
        {
            nuova_chiave = nuova_chiave + 1;

            if($('.originaleHours').length == 0)
            {
                $(".contacts_row.clonableRowHours").each(
                    function(index)
                    {
                        if(index == 0)
                        {
                            $(this).addClass('originaleHours');
                        }
                    }
                );
            }

            clonato = $(".originaleHours").clone();
            $(clonato).removeClass("originaleHours");
            $(clonato).addClass("clonatoHours");
            $(".ultima_rigaHours").removeClass("ultima_rigaHours");

            var fields = ['maintenance_hour_from', 'maintenance_hour_to', 'maintenance_partecipating', 'technician_id_multiple_select_', 'operator_id_multiple_select_'];

            for (i = 0; i < fields.length; i++)
            {
                switch (fields[i])
                {
                    case 'maintenance_hour_from':
                        $(clonato).find("[id*=maintenance_hour_from]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_hour_from]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + 'maintenance_hour_from').val("");
                    break;
                    case 'maintenance_hour_to':
                        $(clonato).find("[id*=maintenance_hour_to]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_hour_to]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + 'maintenance_hour_to').val("");
                    break;
                    case 'maintenance_partecipating':
                        $(clonato).find("[id*=maintenance_partecipating]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_partecipating]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + 'maintenance_hour_to').val("");
                    break;
                    case 'technician_id_multiple_select_':
                        $(clonato).find("[id*=technician_id_multiple_select_]").attr('id', "technician_id_multiple_select_" + nuova_chiave);
                        $(clonato).find("#technician_id_multiple_select_" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][technician_id_multiple_select_]");

                        //select
                        $(clonato).find(".multiselect-native-select").find("[id*=technician_id_multiple_select]").attr('id', "technician_id_multiple_select" + nuova_chiave);
                        $(clonato).find("#technician_id_multiple_select" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][technician_id_multiple_select_][]");

                        var selectHtml3 =  $(clonato).find("#technician_id_multiple_select"+ nuova_chiave).parent('.multiselect-native-select').html();
                        var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').remove();
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).multiselect('rebuild');
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                        break;
                    case 'operator_id_multiple_select_':
                        $(clonato).find("[id*=operator_id_multiple_select_]").attr('id', "operator_id_multiple_select_" + nuova_chiave);
                        $(clonato).find("#operator_id_multiple_select_" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][operator_id_multiple_select_]");

                        //select
                        $(clonato).find(".multiselect-native-select").find("[id*=operator_id_multiple_select]").attr('id', "operator_id_multiple_select" + nuova_chiave);
                        $(clonato).find("#operator_id_multiple_select" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][operator_id_multiple_select_][]");

                        var selectHtml3 =  $(clonato).find("#operator_id_multiple_select"+ nuova_chiave).parent('.multiselect-native-select').html();
                        var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').remove();
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).multiselect('rebuild');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                        break;
                    /*
                    case 'operator_id_multiple_select_':
                        $(clonato).find("#operator_id_multiple_select").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "][]");
                        $(clonato).find("#operator_id_multiple_select").attr('id', "operator_id_multiple_select" + nuova_chiave);
                        var selectHtml3 =  $(clonato).find("#operator_id_multiple_select" + nuova_chiave).parent('.multiselect-native-select').html();
                        var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').remove();
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).val('');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).multiselect('rebuild');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                    break;
                    */
                }
            }

            $(clonato).insertAfter($(".clonableRowHours").last());

            $(".clonatoHours .rimuoviRigaIconHours").show();

            $(clonato).addClass("ultima_rigaHours");
            $(clonato).removeClass("clonatoHours");

            addcrossremovinghours();
        }
    );

    $(document).ready(
        function()
        {
            var clienti = setClients();

            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0, $("#client_id_maintenances_multiple_select").val(), "#MaintenanceClientId", "Maintenancerow");

            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#MaintenanceClientId");

            // Abilita il clonable sul cliente del cantiere che è required
            enableCloningedit($("#client_id_maintenances_multiple_select").val(), "#MaintenanceClientId");

            // Aggiungi il rimuovi riga
            addcrossremoving();
            addcrossremovingddt();

            canvasStart = document.getElementById('signature-pad');

            $("#MaintenanceTechnicianeditForm").on('submit.default', function (ev) { });

            $("#MaintenanceTechnicianeditForm").on('submit.validation', function (ev) {

                ev.preventDefault();

                <?php if($this->request->data['Maintenance']['signimage'] == '') { ?>
                    var image = new Image();
                    image.src = canvasStart.toDataURL("image/png");
                <?php } ?>

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "maintenances", "action" => "checkDuplicate"]) ?>",
                    data:
                    {
                        maintenanceNumber: $("#MaintenancesMaintenanceNumber").val(),
                        maintenanceDate: $(".segnalazioni-input").val(),
                    },
                    success: function (data)
                    {
                        if(data > 0)
                        {
                            $.alert({
                                icon: 'fa fa-warning',
                                title: 'Salvataggio scheda d\'intervento',
                                content: 'Attenzione esiste già una scheda d\'intervento con la stessa numerazione nell\'anno corrente, provare ad aumentare la numerazione',
                                type: 'orange',
                            });
                        }
                        else
                        {
                            <?php if($this->request->data['Maintenance']['signimage'] == '') { ?>
                                canvas = document.getElementById('signature-pad');
                                var image = new Image();
                                image.src = canvas.toDataURL("image/png");
                                if(!signaturePad.isEmpty())
                                {
                                    $("#MaintenancesSignimage").val(canvas.toDataURL("image/png"));

                                    if ($("#MaintenanceHandling").val() == "")
                                    {
                                        $.confirm({
                                            title: 'Scheda d\'intervento',
                                            content: 'Attenzione è presente una firma ma non è correttametne compilato il campo "Cognome e nome"',
                                            type: 'orange',
                                            buttons:
                                            {
                                                Ok: function ()
                                                {
                                                    action:
                                                    {
                                                        $("#MaintenanceHandling").css('border-color', 'red');
                                                    }
                                                },
                                            }
                                        });
                                    }
                                    else
                                    {
                                        $("#MaintenanceTechnicianeditForm").trigger('submit.default');
                                    }
                                }
                                else
                                {
                                    $("#MaintenanceTechnicianeditForm").trigger('submit.default');
                                }
                            <?php } else { ?>
                                $("#MaintenanceTechnicianeditForm").trigger('submit.default');
                            <?php }?>
                        }
                    },
                    error: function (data)
                    {
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Salvataggio scheda d\'intervento',
                            content: 'Attenzione si sono verificati dei problemi durante il salvataggio',
                            type: 'orange',
                        });
                    }
                });
            });
        }
    );
</script>

<script>
    /* Aggiorno l'indirizzo sul cantiere */
    $("#constructionsite_id_multiple_select").change(function() {

        $.ajax({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getAddress"]) ?>",
            data:
            {
                constructionsiteId: $(this).val(),
            },
            success: function (data)
            {
                data = JSON.parse(data);

                $.each(data, function (key, element) {
                    $("#MaintenanceMaintenanceAddress").val(element.address);
                    $("#MaintenanceMaintenanceCity").val(element.city);
                    $("#MaintenanceMaintenanceCap").val(element.cap);
                    $("#MaintenanceMaintenanceProvince").val(element.province);
                    $("#maintenance_nation_multiple_select").val(element.nation);
                    $("#maintenance_nation_multiple_select").multiselect('rebuild');
                });
            }
        })

        if($(this).val() > 0)
        {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getClientId"]) ?>",
                data:
                {
                    constructionsiteId: $(this).val(),
                },
                success: function (data)
                {
                    $("#client_id_maintenances_multiple_select").val(data);
                    $("#client_id_maintenances_multiple_select").multiselect('rebuild');
                },
                error: function(data)
                {
                    console.log('errore');
                }
            })
        }
    });
</script>

<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(function() {

        if($(".lunghezzaddt").length > 0)
        {
            // Se è stata compilata almeno una riga
            if($("#Ddt0Ddtnumber").val() != "" && $("#Ddt0Ddtnumber").val() != undefined)
                $(".maintenancesddt").show();
            else
                $(".maintenancesddt").hide();
        }
        else
        {
            $(".maintenancesddt").hide();
        }

        if($(".lunghezza").length > 0)
        {
            // Se è stata compilata almeno una riga
            if($("#Maintenancerow0Description").val() != "")
                $(".maintenances").show();
            else
                $(".maintenances").hide();
        }
        else
        {
            $(".maintenances").hide();
        }

        $(".jsCodice").removeAttr('required');
        $(".jsDescription").removeAttr('required');
        $(".jsQuantity").removeAttr('required');

        $(".enablemaintenanceaddrow").click(function()
        {
            $(".maintenances").toggle();
            if($(".jsCodice").is(":hidden")) { $(".jsCodice").removeAttr('required'); } else { $(".jsCodice").attr('required','required'); }
            if($(".jsDescription").is(":hidden")) { $(".jsDescription").removeAttr('required'); } else { $(".jsDescription").attr('required','required'); }
            if($(".jsQuantity").is(":hidden")) { $(".jsQuantity").removeAttr('required');} else { $(".jsQuantity").attr('required','required'); }
        })

        $(".enablemaintenanceddtrowddt").click(function() {
            $(".maintenancesddt").toggle();
            $("#supplier_id_multiple_select").attr('required', true);
        });

        /** Definizione del clonable **/
        var fields = ['Ddtnumber','ddtdate','supplier_id_multiple_select','Ddtvalue'];
        var labels = ['ddtnumber','ddtdate','supplier_id_multiple_select','ddtvalue'];
        var newlinesvalue = ['','','',''];
        clonableadvancedDdt("#aggiungi_bolla", fields, labels, newlinesvalue, 'Ddt');
    });
</script>

<script>
    $("#client_id_maintenances_multiple_select").change(function() {

        if($(this).val() > 0)
        {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientConstructionsites"]) ?>",
                data:
                {
                    clientId: $(this).val(),
                },
                success: function (data)
                {
                    $("#constructionsite_id_multiple_select").find('option').remove();
                    data = JSON.parse(data);
                    $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value","").text('Seleziona un valore'));

                    $.each(data, function (key, element) {
                        $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                    });

                    $("#constructionsite_id_multiple_select").multiselect('rebuild');
                    $("#constructionsite_id_multiple_select").change();
                }
            });

            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientQuotes"]) ?>",
                data:
                {
                    clientId: $(this).val(),
                },
                success: function (data)
                {
                    $("#quote_id_multiple_select").find('option').remove();
                    data = JSON.parse(data);
                    $("#quote_id_multiple_select").append($("<option></option>").attr("value","").text("Seleziona un valore"));

                    $.each(data, function (key, element) {
                        $("#quote_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                    });

                    $("#quote_id_multiple_select").multiselect('rebuild');
                    $("#quote_id_multiple_select").change();
                }
            });
        }
        else
        {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientConstructionsites"]) ?>",
                data:
                {
                    clientId: 'all',
                },
                success: function (data)
                {
                    $("#constructionsite_id_multiple_select").find('option').remove();
                    data = JSON.parse(data);
                    $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value","").text('Seleziona un valore'));

                    $.each(data, function (key, element) {
                        $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                    });

                    $("#constructionsite_id_multiple_select").multiselect('rebuild');
                    $("#constructionsite_id_multiple_select").change();
                }
            });
        }
    });
</script>