<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>"Scheda d'intervento",'indexelements' => ['technicianadd'=>"Nuova scheda d'intervento"]]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr>
                    <?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
                            'htmlElements' => [
                                '<center><input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker" class="form-control ajax-filter-input f datepicker" name="data[filters][date1]" value="01-'.date('m-Y').'"  bind-filter-event="change"/>'.''.
                                '<input  type="datetime" style="height:24px;padding:0px;width:100px;text-align:center;" id="datepicker2" class="form-control ajax-filter-input  datepicker" name="data[filters][date2]"  value="31-12-'.date('Y').'" bind-filter-event="change"/></center>'
                            ]
                        ]);
                    ?>
                </tr>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php if(count($maintenances) == 0): ?>
                <tr><td colspan="7" style="text-align: center">nessuna scheda d'intervento trovata</td></tr>
            <?php else: ?>
				<?php foreach ($maintenances as $maintenance): ?>
                    <tr>
                        <td><?= h($maintenance['Maintenance']['maintenance_number']); ?></td>
                        <td><?= date("d-m-Y",strtotime($maintenance['Maintenance']['maintenance_date'])); ?></td>
                        <td><?= h($maintenance['Client']['ragionesociale']); ?></td>
                        <td><?= h($maintenance['Constructionsite']['name']); ?></td>
                        <td><?= h($maintenance['Constructionsite']['description']); ?></td>
                        <td><?= h($maintenance['Maintenance']['intervention_description']); ?></td>
                        <td class="actions">
                        <?php if($maintenance['Maintenance']['sent'] == 1 || date_diff(date_create($maintenance['Maintenance']['maintenance_date']), date_create(date('Y-m-d')))->format('%d') > 7): ?>
                            <?= $iconaModificaOff; ?>
                        <?php else: ?>
                        <?= $this->Html->link($iconaModifica, ['action' => 'technicianedit', $maintenance['Maintenance']['id']], ['title' => __('Modifica'), 'escape' => false]); ?>
                        <?php endif; ?>
                        <?php if(MODULO_CANTIERI): ?>
                            <?= $this->Html->link($iconaPdf, ['action' => 'maintenancepdf', $maintenance['Maintenance']['id']], ['target'=>'_blank','title'=>__('Scarica PDF'),'escape' => false]); ?>
                            <?php
                                if($maintenance['Maintenance']['email'] != null && $maintenance['Maintenance']['email'] != '')
                                {
                                    if($maintenance['Maintenance']['sent'] == 0)
                                        echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#d75a4a;vertical-align: middle;"></i>', ['action' => 'sendMaintenanceMail', $maintenance['Maintenance']['id']], ['target' => '_blank', 'title' => __('Invia la scheda di intervento al seguente indirizzo email: '.$maintenance['Maintenance']['email']), 'escape' => false]);
                                    else
                                        echo $this->Html->link('<i class="fa fa-envelope icon" style="font-size:18px;margin-left:3px;color:#23a24d;vertical-align: middle;"></i>',['action' => 'sendMaintenanceMail', $maintenance['Maintenance']['id']], ['title'=>__('Scheda d\'intervento inviata - ( '.$maintenance['Maintenance']['email'].' )'),'escape' => false]);
                                }
                                else
                                {
                            ?>
                                    <i class="fa fa-envelope icon grigio" style="font-size:18px;margin-left:0px;" title="Email mancante nella scheda d'intervento"></i>
                            <?php
                                }
                            ?>
                        <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
			</tbody>
		</table>
		<?= $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>
<?=  $this->element('Js/datepickercode'); ?>