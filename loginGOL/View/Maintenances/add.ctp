<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->element('Js/clientautocompletefunction'); ?>
<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonable'); ?>
<?= $this->element('Js/clonableadvanced'); ?>
<?= $this->element('Js/addcrossremoving'); ?>

<?= $this->Form->create('Maintenances'); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" crossorigin="anonymous"></script>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __("Nuova scheda d'intervento") ?></span>
<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Numero scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_number', ['label' => false, 'value' => $maintenance_next_number, 'class' => 'form-control', 'required' => true]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Data scheda d'intervento</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <input type="datetime" class="datepicker segnalazioni-input form-control" name="data[Maintenances][maintenance_date]" value="<?= date("d-m-Y"); ?>" required/>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label form-margin-top"><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'client_id',
                "aggregator" => '',
                "prefix" => "client_id",
                "list" => $clients,
                "options" => ['multiple' => false, 'required' => true],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Cantiere</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'constructionsite_id',
                "aggregator" => '',
                "prefix" => "constructionsite_id",
                "list" => $constructionsites,
                "options" => ['multiple' => false, 'required' => true],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Preventivo</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'quote_id',
                "aggregator" => '',
                "prefix" => "quote_id",
                "list" => $quotes,
                "options" => ['multiple' => false, 'required' => false],
            ]);
            ?>
        </div>
    </div>
    <div class="col-md-3" style="float:left;margin-left:10px;">
        <label class="form-label"><strong>Email per invio scheda d'inervento</strong> </label>
        <div class="form-controls">
            <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
</div>

<div class="form-group clientdetails col-md-12">
    <div class="col-md-2" style="float:left">
        <label class="form-label form-margin-top"><strong>Indirizzo cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_address', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>CAP cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_cap', ['label' => false, 'div' => false, 'class' => 'form-control', "pattern" => "[0-9]+", 'title' => 'Il campo può contenere solo caratteri numerici.', 'minlength' => 5]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Città cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_city', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Provincia cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('maintenance_province', ['label' => false, 'div' => false, 'class' => 'form-control']); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-label form-margin-top"><strong>Nazione cantiere</strong></label>
        <div class="form-controls">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                "name" => 'maintenance_nation_id',
                "aggregator" => '',
                "prefix" => "maintenance_nation_id",
                "list" => $nations,
                "options" => ['multiple' => false, 'required' => false],
            ]);
            ?>
        </div>
    </div>
</div>
<div class="col-md-12"><hr></div>
<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">Orari intervento</div>
    <div class="col-md-3 col-md-offset-9 maintenanceshours" id="maintenanceshours">
        <div class="form-group">
            <a id="aggiungi_orario" href="javascript:;">
                <span class="blue-button btn-outline dropdown-toggle add_row_clonable" style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                    <?= 'Aggiungi riga'; ?>
                </span>
            </a>
        </div>
    </div>
    <fieldset id="maintenances_hours" class="col-md-12">
        <div class="principaleHours clonableRowHours contacts_row originaleHours ultima_rigaHours" id="0">
            <span class="remove icon rimuoviRigaIconHours cross fa fa-remove" title="Rimuovi riga" style="float:right" hidden></span>
            <div class="col-md-12">
                <div class="col-md-2" style="float:left;">
                    <label class="form-label form-margin-top"><strong>Inizio intervento</strong><i class="fa fa-asterisk"></i></label>
                    <div class="form-controls">
                        <input type="time" class="segnalazioni-input form-control hour_from" id="Maintenances0maintenance_hour_from" name="data[Hours][0][maintenance_hour_from]" required/>
                    </div>
                </div>
                <div class="col-md-2" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top"><strong>Fine intervento</strong><i class="fa fa-asterisk"></i></label>
                    <div class="form-controls">
                        <input type="time" class="segnalazioni-input form-control hour_to" id="Maintenances0maintenance_hour_to" name="data[Hours][0][maintenance_hour_to]" required/>
                    </div>
                </div>
                <div class="col-md-1" id="outsideoperator">
                    <label class="form-label form-margin-top" ><strong>Tecnico</strong></label>
                    <div class="form-controls">
                        <?= $this->Form->input('partecipating', [ 'id'=>"Maintenances0maintenance_partecipating",'name'=>"data[Hours][0][maintenance_partecipating]", 'div' => false,'type' => 'select','label' => false,'class' => 'form-control','options'=>['1'=>'Si','0'=>'No'],'empty'=>false,'default'=>1]); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <label class="form-label "><strong>Tecnici</strong></label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'technician_id',
                            "aggregator" => '',
                            "prefix" => "technician_id",
                            "suffix" => '',
                            "list" => $technicians,
                            "options" => ['multiple' => true,'empty'=>false],
                        ]); ?>

                    </div>
                </div>
                <div class="col-md-3">
                    <label class="form-label "><strong>Operatori esterni</strong></label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'operator_id',
                            "aggregator" => '',
                            "prefix" => "operator_id",
                            "suffix" => '',
                            "list" => $outsideoperators,
                            "options" => ['multiple' => true,'empty'=>false],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="col-md-12"><hr></div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-11" style="float:left;">
        <label class="form-margin-top form-label"><strong>Descrizione dell'intervento</strong></label>
        <div class="form-controls">
            <?= $this->Form->textarea('intervention_description', ['div' => false, 'label' => false, 'class' => 'form-control', 'rows' => 5]); ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-11" style="float:left;">
        <label class="form-margin-top form-label"><strong>Note intervento</strong></label>
        <div class="form-controls">
            <?= $this->Form->textarea('notes', ['div' => false, 'label' => false, 'class' => 'form-control', 'rows' => 5]); ?>
        </div>
    </div>
</div>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">
        DDT Di riferimento
        <span class="blue-button btn-outline dropdown-toggle enablemaintenanceddtrowddt " style="margin-left:300px;margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;cursor:pointer;'">
            <?= 'Aggiungi bolle materiale' ?>
        </span>
    </div>
    <div class="col-md-3 col-md-offset-9 maintenancesddt" id="maintenancesddt" hidden>
        <div class="form-group">
            <a id="aggiungi_bolla" href="javascript:;">
                <span class="blue-button btn-outline dropdown-toggle add_row_clonable" style="margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;'">
                    <?= 'Aggiungi riga'; ?>
                </span>
            </a>
        </div>
    </div>
    <fieldset id="maintenancesddt" class="col-md-12 maintenancesddt" hidden>
        <div class="principaleddt contacts_row clonableRowddt originaleddt ultima_rigaddt">
            <span class="remove icon rimuoviRigaIconddt cross fa fa-remove" title="Rimuovi riga" style="float:right" hidden></span>
            <div class="col-md-12">
                <div class="col-md-2 jsRowFieldDescription">
                    <label class="form-label"><strong>Numero ddt</strong></label>
                    <?= $this->Form->input('Ddt.0.ddtnumber', ['label' => false, 'class' => 'form-control', 'div' => false]); ?>
                </div>
                <div class="col-md-2" style="float:left;">
                    <label class="form-label form-margin-top"><strong>Data ddt</strong></label>
                    <div class="form-controls"> <!-- QUI -->
                        <input type="datetime" class="datepicker2 segnalazioni-input form-control" name="Ddt.0.ddtdate" id="Ddt0Ddtdate" value="<?= date("d-m-Y"); ?>"/>
                    </div>
                </div>
                <div class="col-md-4" style="float:left;margin-left:10px;">
                    <label class="form-label form-margin-top supplier-label"><strong>Fornitore</strong><i class="fa fa-asterisk"></i></label>
                    <div class="form-controls">
                        <?= $this->element('Form/Components/FilterableSelect/component', [
                            "name" => 'rowsupplier_id',
                            "aggregator" => '',
                            "prefix" => "supplier_id",
                            "list" => $suppliers,
                            "options" => ['multiple' => false],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-1 jsRowField">
                    <label class="form-label"><strong>Valore</strong></label>
                    <?= $this->Form->input('Ddt.0.ddtvalue', ['label' => false, 'class' => 'form-control', 'div' => false]); ?>
                </div>
            </div>
        </div>
    </fieldset>
</div>

<div class="col-md-12"><hr></div>

<div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;">
        Materiali utilizzati
        <span class="blue-button btn-outline dropdown-toggle enablemaintenanceaddrow " style="margin-left:300px;margin-right:10px;padding:5px;font-size:13pt;font-weight:normal;text-transform:uppercase !important;font-family: 'Barlow Semi Condensed' !important;cursor:pointer;'">
            <?= 'Aggiungi articoli' ?>
        </span>
    </div>
    <div class="col-md-12"><hr></div>
    <span class="maintenances" hidden><?= $this->element('Form/Simplify/action_add_clonable_row'); ?></span>
    <fieldset id="maintenances" class="col-md-12 maintenances" hidden>
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
        <div class="principale contacts_row clonableRow originale ultima_riga">
            <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga" hidden></span>
            <div class="col-md-12">
                <?php
                if (ADVANCED_STORAGE_ENABLED) {
                    ?>
                    <div class="col-md-2  jsRowField">
                        <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
                        <?= $this->Form->input('Good.0.codice', ['label' => false, 'class' => 'form-control jsCodice ', 'div' => false, 'required' => true]); ?>
                    </div>
                    <?php
                } else {
                    // Tolgo il jsTipo
                    echo $this->Form->hidden('Good.0.tipo', ['div' => false, 'label' => false, 'class' => 'form-control jsTipo', 'value' => 0]);
                    ?>
                    <div class="col-md-2 jsRowField">
                        <label class="form-label jsRowField"><strong>Codice</strong></label>
                        <?= $this->Form->input('Good.0.codice', ['div' => false, 'label' => false, 'class' => 'form-control jsCodice', 'maxlenght' => 11]); ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-3 jsRowFieldDescription">
                    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.description', ['label' => false, 'class' => 'form-control jsDescription jsDescription2', 'div' => false]); ?>
                    <?= $this->Form->hidden('Good.0.storage_id'); ?>
                    <?= $this->Form->hidden('Good.0.variation_id', ['type' => 'text']); ?>
                    <?= $this->Form->hidden('Good.0.movable', ['class' => 'jsMovable', 'value' => 1]); ?>
                </div>
                <div class="col-md-3 jsRowField">
                    <label class="form-label"><strong>Descrizione aggiuntiva</strong></label>
                    <?= $this->Form->input('Good.0.customdescription', ['label' => false, 'class' => 'form-control ', 'div' => false, 'type' => 'textarea', 'style' => 'height:29px']); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Quantità <i class="fa fa-warning Transportgood.0.iconwarning"
                                                                  style="color:red;display:none;"></i></strong><i
                                class="fa fa-asterisk"></i></label>
                    <?= $this->Form->input('Good.0.quantity', ['label' => false, 'class' => 'form-control jsQuantity', 'div' => false, 'default' => 1,'type'=>"number",'step'=>"0.001", 'min'=>"0",]); ?>
                </div>
                <div class="col-md-2 jsRowField">
                    <label class="form-label"><strong>Unità di misura</strong></label>
                    <?= $this->Form->input('Good.0.unit_of_measure_id', ['label' => false, 'empty' => 'sel.', 'default' => 'sel.', 'class' => 'form-control', 'div' => false, 'empty' => true, 'type' => 'select', 'options' => $units]); ?>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
    </fieldset>
</div>

<center><?= $this->element('Form/Components/Actions/component', ['redirect' => 'index']); ?></center>
<?= $this->Form->end(); ?>

<script>
    function addcrossremovinghours()
    {
        $(".rimuoviRigaIconHours").unbind('click');
        $(".rimuoviRigaIconHours").click(
            function ()
            {
                if ($(".rimuoviRigaIconHours").length > 1)
                {
                    if ($(this).parent('.principaleHours').hasClass('ultima_rigaHours'))
                    {
                        $(this).parent('.principaleHours').remove();
                        $('.principaleHours').last().addClass('ultima_rigaHours');
                    }
                    else
                    {
                        $(this).parent('.principaleHours').remove();
                    }

                    addcrossremoving();
                }
                else
                {
                    $.alert({
                        icon: 'fa fa-warning',
                        title: '',
                        content: 'Attenzione, deve essere sempre presente almeno una riga.',
                        type: 'orange',
                    });
                }
            }
        );
    }

    var formName = "#MaintenancesAddForm";
    var tecnici = <?= json_encode($technicians) ?>;
    var clonato;
    var nuova_chiave = 0;
    addcrossremovinghours();

    $("#aggiungi_orario").click(
        function()
        {
            nuova_chiave = nuova_chiave + 1;

            if($('.originaleHours').length == 0)
            {
                $(".contacts_row.clonableRowHours").each(
                    function(index)
                    {
                        if(index == 0)
                        {
                            $(this).addClass('originaleHours');
                        }
                    }
                );
            }

            clonato = $(".originaleHours").clone(false, false);
            $(clonato).removeClass("originaleHours");
            $(clonato).addClass("clonatoHours");
            $(".ultima_rigaHours").removeClass("ultima_rigaHours");

            var fields = ['maintenance_hour_from', 'maintenance_hour_to', 'maintenance_partecipating', 'technician_id_multiple_select_', 'operator_id_multiple_select_'];

            for (i = 0; i < fields.length; i++)
            {
                switch (fields[i])
                {
                    case 'maintenance_hour_from':
                        $(clonato).find("[id*=maintenance_hour_from]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_hour_from]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + 'maintenance_hour_from').val("");
                        break;
                    case 'maintenance_hour_to':
                        $(clonato).find("[id*=maintenance_hour_to]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_hour_to]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + 'maintenance_hour_to').val("");
                        break;
                    case 'maintenance_partecipating':
                        $(clonato).find("[id*=maintenance_partecipating]").attr('name', "data[Hours][" + nuova_chiave + "][" + fields[i] + "]");
                        $(clonato).find("[id*=maintenance_partecipating]").attr('id', "Maintenances" + nuova_chiave + fields[i]);
                        $(clonato).find("#Maintenances" + nuova_chiave + "maintenance_maintenance_partecipating").val(1);
                        break;
                    case 'technician_id_multiple_select_':
                        $(clonato).find("[id*=technician_id_multiple_select_]").attr('id', "technician_id_multiple_select_" + nuova_chiave);
                        $(clonato).find("#technician_id_multiple_select_" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][technician_id_multiple_select_]");

                        //select
                        $(clonato).find(".multiselect-native-select").find("[id*=technician_id_multiple_select]").attr('id', "technician_id_multiple_select" + nuova_chiave);
                        $(clonato).find("#technician_id_multiple_select" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][technician_id_multiple_select_][]");

                        var selectHtml3 =  $(clonato).find("#technician_id_multiple_select"+ nuova_chiave).parent('.multiselect-native-select').html();
                        var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').remove();
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).multiselect('rebuild');
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                        $(clonato).find("#technician_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                        break;
                    case 'operator_id_multiple_select_':
                        //input
                        $(clonato).find("[id*=operator_id_multiple_select_]").attr('id', "operator_id_multiple_select_" + nuova_chiave);
                        $(clonato).find("#operator_id_multiple_select_" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][operator_id_multiple_select_]");

                        //select
                        $(clonato).find(".multiselect-native-select").find("[id*=operator_id_multiple_select]").attr('id', "operator_id_multiple_select" + nuova_chiave);
                        $(clonato).find("#operator_id_multiple_select" + nuova_chiave).attr('name', "data[Hours][" + nuova_chiave + "][operator_id_multiple_select_][]");

                        var selectHtml3 =  $(clonato).find("#operator_id_multiple_select"+ nuova_chiave).parent('.multiselect-native-select').html();
                        var finalSelectedHtml = selectHtml3.substr(0, selectHtml3.lastIndexOf('</select>')) + '</select>';
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent().parent('.multiple-select-container').append(finalSelectedHtml);
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').remove();
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).multiselect('rebuild');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').css('width', '100%');
                        $(clonato).find("#operator_id_multiple_select"+nuova_chiave).parent('.multiselect-native-select').find('.btn-group').find('.multiselect').css('width', '100%');
                        break;
                }
            }

            $(clonato).insertAfter($(".clonableRowHours").last());

            $(".clonatoHours #Maintenances" + nuova_chiave + "maintenance_persons_names").tagsinput();

            $(".clonatoHours .rimuoviRigaIconHours").show();

            $(clonato).addClass("ultima_rigaHours");
            $(clonato).removeClass("clonatoHours");

            $('.bootstrap-tagsinput').bind('click',
                function ()
                {
                    $(this).addClass('focus');
                }
            );

            addcrossremovinghours();
        }
    );

    $("#MaintenancesAddForm").on('submit.default', function (ev) {});

    $("#MaintenancesAddForm").on('submit.validation',
        function (ev)
        {
            ev.preventDefault();
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "maintenances", "action" => "checkDuplicate"]) ?>",
                data:
                    {
                        maintenanceNumber: $("#MaintenancesMaintenanceNumber").val(),
                        maintenanceDate: $(".segnalazioni-input").val(),
                    },
                success: function (data)
                {
                    if(data > 0)
                    {
                        $.alert({
                            icon: 'fa fa-warning',
                            title: 'Salvataggio scheda d\'intervento',
                            content: 'Attenzione esiste già una scheda d\'intervento con la stessa numerazione nell\'anno corrente, provare ad aumentare la numerazione',
                            type: 'orange',
                        });
                    }
                    else
                    {
                        $("#MaintenancesAddForm").trigger('submit.default');
                    }
                },
                error: function (data)
                {
                    $.alert({
                        icon: 'fa fa-warning',
                        title: 'Salvataggio scheda d\'intervento',
                        content: 'Attenzione si sono verificati dei problemi durante il salvataggio',
                        type: 'orange',
                    });
                }
            })
        }
    )

    $(document).ready(
        function ()
        {
            var clienti = setClients();
            var articoli = setArticles();
            var codici = setCodes();
            // Carico il listino passando chiave e id cliente
            loadClientCatalog(0, $("#client_id_multiple_select").val(), "#MaintenanceClientId", "Good");
            // Definisce quel che succede all'autocomplete del cliente
            setClientAutocomplete(clienti, "#MaintenanceClientId");
            // Abilita il clonable sul cliente del cantiere che è required
            enableCloning($("#client_id_multiple_select").val(), "#MaintenanceClientId");
            // Aggiungi il rimuovi riga
            addcrossremoving();
        }
    );

    /* Aggiorno l'indirizzo sul cantiere */
    $("#constructionsite_id_multiple_select").change(
        function ()
        {
            $.ajax({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getAddress"]) ?>",
                data:
                    {
                        constructionsiteId: $(this).val(),
                    },
                success: function (data)
                {
                    data = JSON.parse(data);
                    $.each(data,
                        function (key, element)
                        {
                            $("#MaintenancesMaintenanceAddress").val(element.address);
                            $("#MaintenancesMaintenanceCity").val(element.city);
                            $("#MaintenancesMaintenanceCap").val(element.cap);
                            $("#MaintenancesMaintenanceProvince").val(element.province);
                            $("#maintenance_nation_id_multiple_select").val(element.nation);
                            $("#maintenance_nation_id_multiple_select").multiselect('rebuild');
                        }
                    );
                }
            })

            if($(this).val() > 0)
            {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "Constructionsites", "action" => "getClientId"]) ?>",
                    data:
                        {
                            constructionsiteId: $(this).val(),
                        },
                    success: function (data)
                    {
                        data = JSON.parse(data);
                        $("#client_id_multiple_select").val(data);
                        $("#client_id_multiple_select").multiselect('rebuild');
                    },
                    error: function (data) {}
                })
            }
        }
    );
</script>

<?= $this->element('Js/datepickercode'); ?>

<script>
    $(document).ready(
        function ()
        {
            $(".maintenances").hide();
            $(".maintenancesddt").hide();
            $(".jsCodice").removeAttr('required');
            $(".jsDescription").removeAttr('required');
            $(".jsQuantity").removeAttr('required');

            $(".enablemaintenanceaddrow").click(function () {
                $(".maintenances").toggle();
                if ($(".jsCodice").is(":hidden")) {
                    $(".jsCodice").removeAttr('required');
                } else {
                    $(".jsCodice").attr('required', 'required');
                }
                if ($(".jsDescription").is(":hidden")) {
                    $(".jsDescription").removeAttr('required');
                } else {
                    $(".jsDescription").attr('required', 'required');
                }
                if ($(".jsQuantity").is(":hidden")) {
                    $(".jsQuantity").removeAttr('required');
                } else {
                    $(".jsQuantity").attr('required', 'required');
                }
            });

            $(".enablemaintenanceddtrowddt").click(function () {
                $(".maintenancesddt").toggle();
                $("#supplier_id_multiple_select").attr('required', true);
            });

            /** Definizione del clonable **/
            var fields = ['Ddtnumber', 'ddtdate', 'supplier_id_multiple_select', 'Ddtvalue'];
            var labels = ['ddtnumber', 'ddtdate', 'supplier_id_multiple_select', 'ddtvalue'];
            var newlinesvalue = ['', '', '', 1, '', '', ''];
            clonableadvancedDdt("#aggiungi_bolla", fields, labels, newlinesvalue, 'Ddt');
        }
    );

    $("#client_id_multiple_select").change(
        function ()
        {
            if ($(this).val() > 0)
            {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientConstructionsites"]) ?>",
                    data:
                        {
                            clientId: $(this).val(),
                        },
                    success: function (data)
                    {
                        $("#constructionsite_id_multiple_select").find('option').remove();
                        data = JSON.parse(data);
                        $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", "").text('Seleziona un valore'));

                        $.each(data,
                            function (key, element)
                            {
                                $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                            }
                        );

                        $("#constructionsite_id_multiple_select").multiselect('rebuild');
                        $("#constructionsite_id_multiple_select").change();
                    }
                });

                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientQuotes"]) ?>",
                    data:
                        {
                            clientId: $(this).val(),
                        },
                    success: function (data)
                    {
                        $("#quote_id_multiple_select").find('option').remove();
                        data = JSON.parse(data);
                        $("#quote_id_multiple_select").append($("<option></option>").attr("value", "").text("Seleziona un valore"));

                        $.each(data,
                            function (key, element)
                            {
                                $("#quote_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                            }
                        );

                        $("#quote_id_multiple_select").multiselect('rebuild');
                        $("#quote_id_multiple_select").change();
                    }
                });
            }
            else
            {
                $.ajax({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "clients", "action" => "getClientConstructionsites"]) ?>",
                    data:
                        {
                            clientId: 'all',
                        },
                    success: function (data)
                    {
                        $("#constructionsite_id_multiple_select").find('option').remove();
                        data = JSON.parse(data);
                        $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", "").text('Seleziona un valore'));

                        $.each(data,
                            function (key, element)
                            {
                                $("#constructionsite_id_multiple_select").append($("<option></option>").attr("value", key).text(element));
                            }
                        );

                        $("#constructionsite_id_multiple_select").multiselect('rebuild');
                        $("#constructionsite_id_multiple_select").change();
                    },
                    error: function (data){}
                });
            }
        }
    );
</script>
