    <?= $this->Form->create('Currencies'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuova valuta') ?></span>
  	 <div class="col-md-12"><hr></div>
    
    <div class="form-group col-md-12">
       <div class="col-md-2" style="float:left;">
          <label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
          <div class="form-controls">
	          <?= $this->Form->input('description', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Simbolo</label><i class="fa fa-asterisk"></i></strong>
        <div class="form-controls">
	        <?= $this->Form->input('symbol', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
        </div>
    </div>
     <div class="col-md-2" style="float:left;margin-left:10px;">
        <label class="form-margin-top form-label"><strong>Codice iso</label><i class="fa fa-asterisk"></i></strong>
        <div class="form-controls">
	        <?= $this->Form->input('isocode', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'minlenght'=>3,'maxlenght'=>3]); ?>
        </div>
    </div>
     </div>
     <div class="col-md-12"><hr></div>
	    <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>
