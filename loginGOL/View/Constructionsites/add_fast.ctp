<?= $this->Form->create('Constructionsites'); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo cantiere') ?></span>

<div class="col-md-12 col-sm-12"><hr></div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-3 col-sm-3" >
        <label class="form-label"><strong>Nome cantiere</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=  $this->Form->input('name', ['label' => false,'class' => 'form-control','required'=>true]);?>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <label class="form-label "><strong>Cliente</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls" id="clientofconstructionsite">
            <?= $this->element('Form/Components/FilterableSelect/component', [
                    "name" => 'client_megaform_id2',
                    "aggregator" => '',
                    "prefix" => "client_megaform_id2",
                    "list" => $customers,
                    "options" => ['multiple' => false, 'required' => true],
                    "actions" => [
                        $this->element('Form/Components/MegaForm/component', ['megaFormIdSuffix' => "add_fast_client", 'linkClass' => 'fa fa-plus', 'buttonOnly' => true, 'buttonTitle' => 'Aggiungi una nuovo cliente'])
                    ]
                ]);
            ?>
        </div>
        <div>
            <div class="form-controls" id="clientnametoshow" style="display:none">
                <?= $this->Form->input('nothingtosend', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255,'readonly'=>'readonly']); ?>
                <!--<?= $this->Form->input('client_megaform_id2', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255,'readonly'=>'readonly','style'=>'display:none','id'=>'client_megaform_id2']); ?>-->
			</div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3" >
    <label class="form-margin-top form-label"><strong>Indirizzo</strong></label>
        <div class="form-controls">
	           <?= $this->Form->input('address', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255]); ?>
         </div>
    </div>
   <div class="col-md-3 col-sm-3">
        <label class="form-margin-top form-label"><strong>Città</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('city', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>255]); ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12" style="float:left;">
        <label class="form-label"><strong>Descrizione cantiere</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=  $this->Form->textarea('description', ['label' => false,'class' => 'form-control','required'=>true]);?>
        </div>
    </div>
</div>

<div class="form-group col-md-12 col-sm-12">
    <div class="col-md-12 col-sm-12" style="float:left;">
        <label class="form-margin-top form-label"><strong>Note cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('notes', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>4000,'type'=>'textarea']); ?>
        </div>
    </div>
</div>

<center class="col-md-12 col-sm-12"><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>

<?= $this->Form->end(); ?>

<script>
    $("#client_id_constructionsite_multiple_select").change(
        function() {
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "bills", "action" => "getCustomerAddressFromId"]) ?>",
                data:
                    {
                        clientId: $(this).val(),
                    },
                success: function (data) {
                    var exit = false;
                    data = JSON.parse(data);
                    $.each(data,
                        function(key, element)
                        {
                            $("#ConstructionsitesAddress").val(element.address);
                            $("#ConstructionsitesCity").val(element.city);
                            $("#ConstructionsitesCap").val(element.cap);
                            $("#ConstructionsitesProvince").val(element.province);
                            $("#constructionsite_nation_multiple_select").val(element.nation);
                            $("#constructionsite_nation_multiple_select").multiselect('rebuild');
                        });
                }
            })
        })
</script>

<script>
    // Chiamata quando viene aperta dal megarform
    function addConstructionsitePageCustomLoaders(htmlContent, megaFormButton, megaForm) {
        initializeFilterableSelects();
    }

    // Quando premo salva aggiorno il valore sulla cellachiamata
    function addConstructionsiteMegaFormsAfterSaveCallback(entityData, saveButton, megaFormButton, megaForm) {
        var changedSelect = megaFormButton.parents('.multiple-select-container').find('[id$="_multiple_select"]').first();
        var val = $.isNumeric(changedSelect.val()) || !$.isArray(changedSelect.val()) ? [changedSelect.val] : changedSelect.val();
        changedSelect.append($('<option>', {
            value: entityData.id,
          //  text: entityData.fullName ? entityData.fullName : entityData.description
          //    value: entityData.client_megaform_id,
              text:  entityData.fullName  ? entityData.fullName : entityData.name
        }));
        changedSelect.val($.merge(val, [""+entityData.id])).multiselect('rebuild');
    }
</script>

<?php
    echo $this->element('Form/Components/MegaForm/component', [
        'megaFormIdSuffix' => "add_fast_client",
        'url' => $this->Html->Url(['controller' => 'clients', 'action' => 'add_fast']),
        'clonableExtension' => 0,
        'linkClass' => 'fa fa-plus',
        'afterAjaxCallback' => '',
        'beforePerformShowOrHideCallback' => 'addFastClientPageCustomLoaders', // Eventuale funzione js che carica le funzioni js apprartenenti alla pagina che viene richiamata
        'afterSaveCallback' => 'addFastClientMegaFormsAfterSaveCallback2', // Tutto quello che deve essere eseguito dopo aver premuto il pulsante salva sulla schermata richiamata dal megaform
        'formOnly' => true
    ]);
?>

