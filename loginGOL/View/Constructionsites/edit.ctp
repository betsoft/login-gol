<?= $this->element('Form/Components/MegaForm/loader'); ?>
<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->Form->create('Constructionsites'); ?>
<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Modifica cantiere - ').$constructionsiteName ?></span>

<div class="col-md-12"><hr></div>

<div class="form-group col-md-12">
    <div class="col-md-2" >
        <label class="form-label form-margin-top"><strong>Cliente</strong></label>
        <div class="form-controls">
            <?=
            $this->element('Form/Components/FilterableSelect/component',
                [
                    "name" => 'client_id',
                    "aggregator" => '',
                    "prefix" => "client_id_constructionsite",
                    "list" => $customers,
                    "options" =>  [  'multiple' => false ,'required'=>false ],
                ]);
            ?>
        </div>
    </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>Nome cantiere</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?=  $this->Form->input('name', ['label' => false,'class' => 'form-control','required'=>true]);?>
        </div>
    </div>
    <div class="col-md-6" style="float:left;">
        <label class="form-label"><strong>Descrizione cantiere</strong></label>
        <div class="form-controls">
            <?=  $this->Form->textarea('description', ['label' => false,'class' => 'form-control','required'=>true]);?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>CIG</strong></label>
        <div class="form-controls">
            <?=  $this->Form->input('cig', ['label' => false,'class' => 'form-control']);?>
        </div>
    </div>
    <div class="col-md-2" style="float:left;">
        <label class="form-label"><strong>CUP</strong></label>
        <div class="form-controls">
            <?=  $this->Form->input('cup', ['label' => false,'class' => 'form-control']);?>
        </div>
    </div>
</div>


<div class="form-group col-md-12">
    <div class="col-md-2" style="float:left">
    <label class="form-margin-top form-label"><strong>Indirizzo</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
	           <?= $this->Form->input('address', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
         </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Cap</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('cap', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Città</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('city', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>
    <div class="col-md-2" style="float:left">
        <label class="form-margin-top form-label"><strong>Provincia</strong><i class="fa fa-asterisk"></i></label>
        <div class="form-controls">
            <?= $this->Form->input('province', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'maxlength'=>255]); ?>
        </div>
    </div>
        <div class="col-md-2" >
            <label class="form-label form-margin-top"><strong>Nazione</strong></label>
            <div class="form-controls">
                <?= $this->element('Form/Components/FilterableSelect/component', ["name" => 'nation_id',"aggregator" => '',"prefix" => "constructionsite_nation","list" => $nations,"options" => [ 'multiple' => false,'required'=> false]]); ?>
            </div>
        </div>
</div>

<div class="form-group col-md-12">
    <div class="col-md-12" style="float:left">
        <label class="form-margin-top form-label"><strong>Note cantiere</strong></label>
        <div class="form-controls">
            <?= $this->Form->input('notes', ['div' => false, 'label' => false, 'class'=>'form-control','maxlength'=>4000,'type'=>'textarea']); ?>
        </div>
    </div>
</div>

<div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

    <!-- Per megaform client add -->
<?php /*
$this->element('Form/Components/MegaForm/component',[
    'megaFormIdSuffix' => "add_client",
    'url' => $this->Html->Url(['controller'=>'Clients', 'action'=>'add']),
    'clonableExtension' => 0,
    'linkClass' => 'fa fa-plus',
    'afterAjaxCallback' => 'loadJsForMessageFix',
    'beforePerformShowOrHideCallback' => 'addBankPageCustomLoaders', // Eventuale funzione js che carica le funzioni js apprartenenti alla pagina che viene richiamata
    'afterSaveCallback' => 'addBankMegaFormsAfterSaveCallback', // Tutto quello che deve essere eseguito dopo aver premuto il pulsante salva sulla schermata richiamata dal megaform
    'formOnly' => true
]) */
?>

    <script>
        // Chiamata quando viene aperta dal megarform
        /*function addClientPageCustomLoaders(htmlContent, megaFormButton, megaForm) {
            //	initializeFieldValueFormat(); // Posso lasciarlo vuoto
        }

        // Quando premo salva aggiorno il valore sull celalchiamata
        function addClientMegaFormsAfterSaveCallback(entityData, saveButton, megaFormButton, megaForm) {
            var changedSelect = megaFormButton.parents('.multiple-select-container').find('[id$="_multiple_select"]').first();
            var val = $.isNumeric(changedSelect.val()) || !$.isArray(changedSelect.val()) ? [changedSelect.val] : changedSelect.val();
            changedSelect.append($('<option>', {
                value: entityData.id,
                text: entityData.fullName ? entityData.fullName : entityData.description
            }))
            changedSelect.val($.merge(val, [""+entityData.id])).multiselect('rebuild');
        }*/

        $("#client_id_constructionsite_multiple_select").change(
            function() {
                $.ajax
                ({
                    method: "POST",
                    url: "<?= $this->Html->url(["controller" => "bills", "action" => "getCustomerAddressFromId"]) ?>",
                    data:
                        {
                            clientId: $(this).val(),
                        },
                    success: function (data) {
                        //var exit = false;
                        data = JSON.parse(data);
                        $.each(data,
                            function(key, element)
                            {
                                console.log(element);
                                $("#ConstructionsitesAddress").val(element.address);
                                $("#ConstructionsitesCity").val(element.city);
                                $("#ConstructionsitesCap").val(element.cap);
                                $("#ConstructionsitesProvince").val(element.province);
                                $("#constructionsite_nation_multiple_select").val(element.nation);
                                $("#constructionsite_nation_multiple_select").multiselect('rebuild');
                            });
                    }
                })
            })
    </script>
