<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Cantieri','indexelements' => ['add'=>'Nuovo cantiere']]); ?>

<div class="clients index">
	<div class="units index">
		<table class="table table-bordered table-striped table-condensed flip-content">
			<thead class ="flip-content">
				<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
                <tr>
                    <?= $this->element('Form/Components/AjaxFilter/component', [
                        'elements' => $filterableFields,
                        'afterAjaxfilterCallback'=>'constructionSiteJsFunction',
                        'htmlElements' => [
                            $this->Form->input('filters.state', ['div' => false, 'label' => false,'class' => 'form-control ajax-filter-input' ,'type' => 'select','options' => $state , 'empty'=>true, 'bind-filter-event'=>"change"])
                        ]
                    ]);
                    ?>
                </tr>
			</thead>
			<tbody class="ajax-filter-table-content">
            <?php if (count($constructionsites) < 1): ?>
                <tr><td colspan="<?= count($sortableFields) ?>"  style="text-align: center">nessun cantiere trovato</td></tr>
            <?php else: ?>
                <?php foreach ($constructionsites as $constructionsite): ?>
                    <?php
                        $constructionsite['Constructionsite']['state'] > 1 ? $displayClose = 'none' : $displayClose = 'inline';
                        $constructionsite['Constructionsite']['state'] > 1 ? $displayOpen = 'inline' : $displayOpen = 'none';
                    ?>
                    <tr>
                        <td><?= h($constructionsite['Client']['ragionesociale']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['name']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['description']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['address']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['city']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['cap']); ?></td>
                        <td><?= h($constructionsite['Constructionsite']['province']); ?></td>
                        <?php
                            switch ($constructionsite['Constructionsite']['state'])
                            {
                                case '1':
                                    $state = 'APERTO';
                                    $color = 'color: green';
                                break;
                                case '2':
                                    $state = 'CHIUSO';
                                    $color = 'color: red';
                                break;
                                case '3':
                                    $state = 'SOSPESO';
                                    $color = 'color: #ea5d0b';
                                break;
                                default:
                                    $state = '';
                                    $color = '';
                                break;
                            }
                        ?>
                        <td style="font-weight: bold;<?= $color ?>"><?= $state ?></td>
                        <td class="actions">
                            <?= $this->Html->link($iconaModifica, ['action' => 'edit', $constructionsite['Constructionsite']['id']],['title'=>__('Modifica'),'escape'=>false]); ?>
                            <i class="fa fa-unlock jsConstructionsiteClose" id="jsOpen<?=  $constructionsite["Constructionsite"]['id'] ; ?>" constructionsiteId = "<?=  $constructionsite["Constructionsite"]['id'] ; ?>" style="display:<?= $displayClose  ?>;font-size:20px;vertical-align: middle;color:green;cursor:pointer;" title="Chiudi cantiere"></i>
                            <i class="fa fa-lock jsConstructionsiteOpen" id="jsClose<?=  $constructionsite["Constructionsite"]['id'] ; ?>" constructionsiteId = "<?=  $constructionsite["Constructionsite"]['id'] ; ?>" style="display:<?= $displayOpen  ?>;font-size:20px;vertical-align: middle;margin-right:5px;color:red;cursor:pointer;" title="Riapri cantiere"></i>
                            <?= $this->Html->link('<i class="fa fa-list" style="vertical-align:middle;font-size:20px;color: grey;" constructionsiteId = "'. $constructionsite["Constructionsite"]['id'] .'" ></i>', ['action' => 'constructionsitedetail','controller'=>'constructionsites', $constructionsite["Constructionsite"]['id']],['title'=>__('Visualizza riepilogativo cantiere'),'escape'=>false]); ?>
                        <?php if($constructionsite['Constructionsite']["state"] == 3) : ?>
                            <i class="fa fa-home jsConstructionsiteAuthorize" style="font-size:20px;vertical-align: middle;color:#ea5d0b;cursor:pointer;" title="Conferma il cantiere" constructionsiteid="<?= $constructionsite['Constructionsite']['id'] ?>"></i>
                        <?php else :?>
                            <!--<i class="fa fa-home" style="font-size:20px;color:#577400;vertical-align: middle;" constructionsiteid="<?= $constructionsite["Constructionsite"]['id']  ?>"></i>-->
                        <?php endif; ?>
                        <?php if(!in_array($constructionsite['Constructionsite']['id'], $constructionsitesWithMaintenance)): ?>
                            <?= $this->Form->postLink($iconaElimina, ['action' => 'delete', $constructionsite['Constructionsite']['id']]   , ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare il cantiere ?' , $constructionsite['Constructionsite']['id'])); ?>
                        <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
			</tbody>
		</table>
		<?= $this->element('Form/Components/Paginator/component'); ?>
	</div>
</div>

<script>
    addLoadEvent(
        function()
        {
            constructionSiteJsFunction();
        }
    )

    function constructionSiteJsFunction()
    {
        $(".jsConstructionsiteClose").click(function () {
            var constructionsite = $(this);

            $.confirm ({
                title: 'Abilitazione cantieri.',
                content: 'Abilitare il cantiere selezionato ?',
                type: 'blue',
                buttons:
                {
                    Ok: function ()
                    {
                        action:
                        {
                            $.ajax ({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "closeConstructionsite"]) ?>",
                                data:
                                {
                                    constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                },
                                success: function (data)
                                {
                                    $("#jsOpen" + data).hide();
                                    $("#jsClose" + data).show();
                                }
                            });
                        }
                    },
                    annulla: function ()
                    {
                        action:{ }
                    },
                }
            });
        });

        $(".jsConstructionsiteOpen").click(function () {
            var constructionsite = $(this);

            $.confirm({
                title: 'Abilitazione cantieri.',
                content: 'Abilitare il cantiere selezionato ?',
                type: 'blue',
                buttons:
                {
                    Ok: function ()
                    {
                        action:
                        {
                            $.ajax({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "openConstructionsite"]) ?>",
                                data:
                                {
                                    constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                },
                                success: function (data)
                                {
                                    $("#jsOpen" + data).show();
                                    $("#jsClose" + data).hide();
                                }
                            });
                        }
                    },
                    annulla: function ()
                    {
                        action: { }
                    },
                }

            });
        });

        $(".jsConstructionsiteAuthorize").click(function () {
            var constructionsite = $(this);

            $.confirm({
                title: 'Abilitazione cantieri.',
                content: 'Abilitare il cantiere selezionato ?',
                type: 'blue',
                buttons:
                {
                    Ok: function ()
                    {
                        action:
                        {
                            $.ajax({
                                method: "POST",
                                url: "<?= $this->Html->url(["controller" => "constructionsites", "action" => "enable"]) ?>",
                                data:
                                {
                                    constructionsiteId: $(constructionsite).attr("constructionsiteid"),
                                },
                                success: function (data)
                                {
                                    //$(constructionsite).css("color", "#577400");
                                    $(constructionsite).css("visibility", "hidden");

                                    $(constructionsite).parent().find(".jsConstructionsiteOpen").css("color", "green");
                                }
                            });
                        }
                    },
                    annulla: function ()
                    {
                        action: { }
                    },
                }
            });
        });
    }
</script>
