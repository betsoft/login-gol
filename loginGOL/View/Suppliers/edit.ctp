<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;">Modifica fornitore</span>
 <div class="col-md-12"><hr></div>
    <?= $this->Form->create('Supplier'); ?>
	<?= $this->Form->input('id', ['div' => false, 'label' => false, 'class' => 'form-control']); ?>

  
   
   <div class ="form-group col-md-12" >
	     <div class="col-md-2" style="float:left">
	     <label class="form-label"><strong>Codice fornitore</label></strong>
         <div class="form-controls">
	            <?=  $this->Form->input('code',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
         </div>
      </div>
      <div class="col-md-6" style="float:left;margin-left:10px;">
	        <label class="form-label"><strong>Ragione sociale</label><i class="fa fa-asterisk"></i></strong>
         <div class="form-controls">
	           <?=  $this->Form->input('name',['div' => false, 'label' => false, 'class'=>'form-control','required'=>true]); ?>
         </div>
      </div>
   </div>

   <div class ="form-group col-md-12" >
       <div class="col-md-2" style="float:left">
      	   <label class="form-label form-margin-top"><strong>Indirizzo</strong></label> 
          <div class="form-controls">
	             <?=  $this->Form->input('indirizzo',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
         	<label class="form-label"><strong>CAP</strong></label>
          <div class="form-controls">
       	     <?=  $this->Form->input('cap',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5]); ?>
          </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
	         <label class="form-label"><strong>Città</strong></label>
          <div class="form-controls">
	            <?=  $this->Form->input('city',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
	         <label class="form-label"><strong>Provincia</strong></label>
          <div class="form-controls">
	             <?=  $this->Form->input('province',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
       </div>
   
       <div class="col-md-2" style="float:left;margin-left:10px;">
          	<label class="form-label form-margin-top"><strong>Nazione</strong></label>
           <div class="form-controls">
             		<?= $this->element('Form/Components/FilterableSelect/component', [
             			"name" => 'nation_id',
             			"aggregator" => '',
             			"prefix" => "nation_id",
             			"list" => $nations,
             			"options" => [ 'multiple' => false,'required'=> false],
             			]); 
             	?>
           </div>
       </div>
   </div>
   
        
  <div class ="form-group col-md-12" >
       <div class="col-md-4" >
	      <label class="form-label form-margin-top"><strong>Partita IVA</strong></label>
          <div class="form-controls">
	            <?= $this->Form->input('piva',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
          </div>
       </div>
       <div class="col-md-4" >
	         <label class="form-label form-margin-top"><strong>Codice fiscale</strong></label>
          <div class="form-controls">
	            <?= $this->Form->input('cf',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
          </div>
       </div>
       	<?php if(MULTICURRENCY)	{ ?>
       <div class="col-md-2" >
		<label class="form-label form-margin-top"><strong>Valuta</strong></label>
		<div class="form-controls">
			<?= $this->Form->input('currency_id',['label' => false, 'div' =>false, 'class'=>'form-control','options'=>$currencies,'empty'=>true]); ?>
		</div>
	</div>
		<?php } ?>
   </div>

   <div class ="form-group col-md-12" >
       <div class="col-md-2" style="float:left">
        	<label class="form-label form-margin-top"><strong>Telefono</strong></label>
         <div class="form-controls">
      	      <?= $this->Form->input('telefono',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
         </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
	         <label class="form-label form-margin-top"><strong>Fax</strong></label>
          <div class="form-controls">
	            <?=  $this->Form->input('fax',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
          </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
	         <label class="form-label form-margin-top"><strong>Mail</strong></label>
          <div class="form-controls">
	            <?= $this->Form->input('e-mail',['div' => false, 'label' => false, 'class'=>'form-control','type'=> 'email']); ?>
          </div>
       </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Pec</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('pec',['div' => false, 'label' => false, 'class'=>'form-control','type'=> 'email','required'=>false]); ?>
            </div>
        </div>
          <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Codice di interscambio</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('interchange_code',['div' => false, 'label' => false, 'class'=>'form-control','required'=>false]); ?>
            </div>
        </div>
   </div>

    <div class="col-md-12"><hr></div>
   <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
   <?= $this->Form->end(); ?>

<script>
     $("#SupplierEditForm").on('submit.default',function(ev) 
     {
     });
        
     $("#SupplierEditForm").on('submit.validation',function(ev) 
     {              
        ev.preventDefault(); // to stop the form from submitting
        checkSupplierPivaAndCfDuplicate()
     });
</script>

<script>

function checkSupplierPivaAndCfDuplicate()
{  
	var errore = 0;

    $.ajax
    ({
    	method: "POST",
    	url: "<?= $this->Html->url(["controller" => "suppliers","action" => "checkSupplierPivaDuplicate"]) ?>",
    	data:															   
    	{
    		piva : $("#SupplierPiva").val(),
    	},
    	success: function(data)
    	{
    		
    		//if(data > 0){ errore = 1; }
    		if(data > 0  && ( $("#SupplierPiva").val() != '<?= $this->data['Supplier']['piva']; ?>')) { errore = 1; }
        	$.ajax
    		({
    			method: "POST",
    			url: "<?= $this->Html->url(["controller" => "suppliers","action" => "checkSupplierCfDuplicate"]) ?>",
    			data: 
    			{
    				cf : $("#SupplierCf").val(),
    			},
    			success: function(data)
    			{
    				if(data > 0  && ( $("#SupplierCf").val() != '<?= $this->data['Supplier']['cf']; ?>')) { errore = 2; }
		            switch (errore)
        		    {
		            	case 0:  
                			$("#SupplierEditForm").trigger('submit.default');
    		            break;
	                    case 1:
	                        /* $.alert
                            ({
                                icon: 'fa fa-warning',
                            	title: '',
                                content: 'Attenzione esiste già un fornitore con la stessa partita iva.',
                                type: 'orange',
                            });*/
                             $.confirm({
    			            	title: 'Modifica fornitore.',
        		                content: 'Attenzione esiste già un cliente con la stessa partita iva, continuare comunque?',
        		                type: 'orange',
        		                buttons: {
            		            Ok: function () 
            		            {
            		            	$("#SupplierEditForm").trigger('submit.default');
            		            },
            		            annulla: function () 
                                {
                                	action: 
                                	{
                                	
                                	}
	                            }}
							 });
                            $("#SupplierPiva").css("border-color",'1px solid red');
                        return false;
                        case 2:
                            /*$.alert
                            ({
                                icon: 'fa fa-warning',
                            	title: '',
                                content: 'Attenzione esiste già un fornitore con lo stesso codice fiscale.',
                                type: 'orange',
                            });*/
                            	$.confirm({
    			            	title: 'Modifica fornitore.',
        		                content: 'Attenzione esiste già un cliente con lo stesso codice fiscale, continuare comunque?',
        		                type: 'orange',
        		                buttons: {
            		            Ok: function () 
            		            {
            		            	$("#SupplierEditForm").trigger('submit.default');
            		            },
            		            annulla: function () 
                                {
                                	action: 
                                	{
                                	
                                	}
	                            }}
							 });
                            $("#SupplierCf").css("border-color",'1px solid red');
                        return false;
                            break;
                        }
    				},
            		error: function(data)
        			{
        			}
    			});
    	}
    })
}
</script>