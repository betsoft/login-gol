<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<!-- Titolo -->
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Fornitori','indexelements' => ['add'=>'Nuovo fornitore']]); ?>

<div class="suppliers index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<?php //if (count($suppliers) > 0) { ?>
			<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
			<?php 
				//}
			
			?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php 
			if(count($suppliers) == 0)
			{
				?><tr><td colspan="11"><center>nessun fornitore trovato</center></td></tr><?php				
			}
			foreach ($suppliers as $supplier){ ?>
				<tr>
					<td><?= h($supplier['Supplier']['code']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['name']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['indirizzo']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['cap']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['city']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['province']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['telefono']); ?>&nbsp;</td>
					<!--td><?php // echo h($supplier['Supplier']['fax']); ?>&nbsp;</td-->
					<td><?= h($supplier['Supplier']['e-mail']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['piva']); ?>&nbsp;</td>
					<td><?= h($supplier['Supplier']['cf']); ?>&nbsp;</td>
					<!--td><?php // echo h($supplier['Nation']['name']); ?>&nbsp;</td-->
					<td class="actions">
						<?php
							echo $this->element('Form/Simplify/Actions/edit',['id' => $supplier['Supplier']['id']]);
							echo $this->Html->link($iconaDestini, array('controller'=>'Supplierdestinations','action' => 'index', $supplier['Supplier']['id']),array('title'=>__('Gestione destini diversi'),'escape'=>false));
							echo $this->element('Form/Simplify/Actions/delete',['id' => $supplier['Supplier']['id'],'message' => 'Sei sicuro di voler eliminare il fornitore ?']); 
						?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>
