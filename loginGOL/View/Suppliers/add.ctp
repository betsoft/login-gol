<?= $this->element('Form/Components/FilterableSelect/loader'); ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>

 <?=  $this->Form->create('Supplier'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;">
	    <?= __('Nuovo fornitore'); ?>
	</span>
	  <div class="col-md-12"><hr></div>

  <div class ="form-group col-md-12" >
	     <div class="col-md-2" style="float:left">
	     <label class="form-label"><strong>Codice fornitore</strong></label>
      <div class="form-controls">
	        <?=  $this->Form->input('code',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'lunghezza2']); ?>
      </div>
      </div>
   	<div class="col-md-2" style="float:left;margin-left:10px;">
	     <label class="form-label"><strong>Ragione sociale</strong><i class="fa fa-asterisk"></i></label>
      <div class="form-controls">
	       <?=  $this->Form->input('name',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'lunghezza2','required'=>true,'pattern'=>PATTERNBASICLATIN]); ?>
      </div>
   </div>
   </div>


    <div class ="form-group col-md-12" >
       <div class="col-md-2" style="float:left">
   	    <label class="form-label form-margin-top"><strong>Indirizzo</strong></label>
           <div class="form-controls">
   	        <?=  $this->Form->input('indirizzo',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
          </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
   	    <label class="form-label"><strong>CAP</strong></label>
           <div class="form-controls">
   	        <?=  $this->Form->input('cap',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9]+", 'title'=>'Il campo può contenere solo caratteri numerici.','minlength'=>5 ]); ?>
           </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
   	    <label class="form-label"><strong>Città</strong></label>
           <div class="form-controls">
   	        <?=  $this->Form->input('city',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
           </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
       	<label class="form-label"><strong>Provincia</strong></label>
           <div class="form-controls">
   	        <?=  $this->Form->input('province',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
           </div>
       </div>
       <div class="col-md-2" style="float:left;margin-left:10px;">
   	    <label class="form-label form-margin-top"><strong>Nazione</strong></label>
           <div class="form-controls">
   		    <?= $this->element('Form/Components/FilterableSelect/component', [
   			    "name" => 'nation_id',
   			    "aggregator" => '',
   			    "prefix" => "supplier_nation_id",
   			    "list" => $nations,
   			    "options" => [ 'multiple' => false,'required'=> false],
   			]); 
            ?>
           </div>
       </div>
    </div>

     <div class ="form-group col-md-12" >
        <div class="col-md-2" style="float:left">
	        <label class="form-label form-margin-top"><strong>Partita IVA</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('piva',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
        	<label class="form-label form-margin-top"><strong>Codice fiscale</strong></label>  
            <div class="form-controls">
	            <?=  $this->Form->input('cf',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
        </div>
        <?php if(MULTICURRENCY)	{ ?>
         <div class="col-md-2" style="float:left;margin-left:10px;">
		<label class="form-label form-margin-top"><strong>Valuta</strong></label>
		<div class="form-controls">
			<?= $this->Form->input('currency_id',['label' => false, 'div' =>false, 'class'=>'form-control','options'=>$currencies,'empty'=>true]); ?>
		</div>
	</div>
		<?php } ?>
    </div>
   

   <div class ="form-group col-md-12" >
        <div class="col-md-2" style="float:left">
            <label class="form-label form-margin-top"><strong>Telefono</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('telefono',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Fax</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('fax',['div' => false, 'label' => false, 'class'=>'form-control',"pattern"=>"[0-9a-zA-Z]+", 'title'=>'Il campo può contenere solo caratteri alfanumerici.']); ?>
            </div>
        </div>
        <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Mail</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('e-mail',['div' => false, 'label' => false, 'class'=>'form-control','type'=> 'email']); ?>
            </div>
        </div>
         <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Pec</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('pec',['div' => false, 'label' => false, 'class'=>'form-control','type'=> 'email','required'=>false]); ?>
            </div>
        </div>
          <div class="col-md-2" style="float:left;margin-left:10px;">
	        <label class="form-label form-margin-top"><strong>Codice di interscambio</strong></label>
            <div class="form-controls">
	            <?=  $this->Form->input('interchange_code',['div' => false, 'label' => false, 'class'=>'form-control','required'=>false]); ?>
            </div>
        </div>
   </div>

   <div class="col-md-12"><hr></div>
   <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
   <?= $this->Form->end(); ?>
   
   <script>
     $("#SupplierAddForm").on('submit.default',function(ev) 
     {
     });
        
     $("#SupplierAddForm").on('submit.validation',function(ev) 
     {              
        ev.preventDefault(); // to stop the form from submitting
        checkSupplierAddPivaAndCfDuplicate()
     });
</script>


<script>

function checkSupplierAddPivaAndCfDuplicate() {
    var errore = 0;

    $.ajax
    ({
        method: "POST",
        url: "<?= $this->Html->url(["controller" => "suppliers", "action" => "checkSupplierPivaDuplicate"]) ?>",
        data:
            {
                piva: $("#SupplierPiva").val(),
            },
        success: function (data) {
            if (data > 0) {
                errore = 1;
            }
            $.ajax
            ({
                method: "POST",
                url: "<?= $this->Html->url(["controller" => "suppliers", "action" => "checkSupplierCfDuplicate"]) ?>",
                data:
                    {
                        cf: $("#SupplierCf").val(),
                    },
                success: function (data) {
                    if (data > 0) {
                        errore = 2;
                    }
                    switch (errore) {
                        case 0:
                            $("#SupplierAddForm").trigger('submit.default');
                            break;
                        case 1:
                            $.confirm({
                                title: 'Nuovo fornitore.',
                                content: 'Attenzione esiste già un fornitore con la stessa partita iva, continuare comunque?',
                                type: 'orange',
                                buttons: {
                                    Ok: function () {
                                        $("#SupplierAddForm").trigger('submit.default');
                                    },
                                    annulla: function () {
                                        action:
                                        {

                                        }
                                    }
                                }
                            });
                            /* $.alert
                             ({
                                 icon: 'fa fa-warning',
                                 title: '',
                                 content: 'Attenzione esiste già un fornitore con la stessa partita iva.',
                                 type: 'orange',
                             }); */
                            $("#SupplierPiva").css("border-color", '1px solid red');
                            return false;
                        case 2:
                            /*$.alert
                            ({
                                icon: 'fa fa-warning',
                            	title: '',
                                content: 'Attenzione esiste già un fornitore con lo stesso codice fiscale.',
                                type: 'orange',
                            });*/
                            $.confirm({
                                title: 'Nuovo fornitore.',
                                content: 'Attenzione esiste già un fornitore con lo stesso codice fiscale, continuare comunque?',
                                type: 'orange',
                                buttons: {
                                    Ok: function () {
                                        $("#SupplierAddForm").trigger('submit.default');
                                    },
                                    annulla: function () {
                                        action:
                                        {

                                        }
                                    }
                                }
                            });
                            $("#SupplierCf").css("border-color", '1px solid red');
                            return false;
                            break;
                    }
                },
                error: function (data) {
                }
            });
        }
    })
}
</script>

