<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>

<!-- Titolo -->
<?php 
 $descrizioneArticolo = '';
  if($thisStorage['codice'] != null && trim($thisStorage['codice'])  != '')
 {
 	$descrizioneArticolo = $thisStorage['codice'];
 }
 
 $descrizioneArticolo != '' ?  $descrizioneArticolo .=  ' - '.$thisStorage['descrizione'] :  $descrizioneArticolo .=  ' '.$thisStorage['descrizione'] ;
 
?>
<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Varianti dell\'articolo -'.$descrizioneArticolo  ,'indexelements' => ['addvariation'=>'Nuova variante di magazzino'],'postValue'  => $storageId,'xlsLink'=>'indexvariation/'.$id]); ?>
	
<div class="variations index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
		</thead>
		<tbody class="ajax-filter-table-content">
            <?php if (count($storages) == 0)
            {
                ?><tr><td colspan="3"><center>nessuna variante articolo</center></td></tr><?php
            }
			foreach ($storages as $storage) { ?>
				<tr>
					<td> <?= h($storage['Storage']['codice']); ?></td>
					<td> <?= h($storage['Storage']['descrizione']); ?></td>
					<td class="actions">
						<?php
							echo $this->Html->link($iconaModifica, array('action' => 'editvariation', $storage['Storage']['id']),array('title'=>__('Modifica articolo'),'escape'=>false));
							if ($storage['Storage']['sn'] == 0)
							{ 
								//echo $this->Html->link($iconaDettaglioMagazzino, array('action' => 'addvariation', $storage['Storage']['id']),array('title'=>__('Crea sottovariante'),'escape'=>false));
								echo $this->Html->link($iconaDettaglioMagazzino, ['action' => 'indexvariation', $storage['Storage']['id']],['style'=>'margin-right:3px;' ,'title'=>__('Elenco varianti articolo'),'escape'=>false]);
							}
							echo $this->Form->postLink($iconaElimina, array('action' => 'delete', $storage['Storage']['id']), array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare la variante ?', $storage['Storage']['id']));
						?>
					</td>
				
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>
