<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<?= $this->element('Form/Components/AjaxSort/loader') ?>


<?php
if($_SESSION['Auth']['User']['dbname'] == 'login_GE0046')
	echo $this->element('Form/formelements/indextitle',['indextitle'=>'Articoli di magazzino','indexelements' => ['importStorages'=>'Importa magazzino','add'=>'Nuovo articolo di magazzino']]);
else
    echo  $this->element('Form/formelements/indextitle',['indextitle'=>'Articoli di magazzino','indexelements' => ['add'=>'Nuovo articolo di magazzino']]);


?>
<!-- Titolo -->

<div class="storages index">
	<table class="table table-bordered table-striped table-condensed flip-content ajax-filters-container">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
				<tr><?= $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields,
			'htmlElements' => [
				'<center><input  type="checkbox" style="height:20px;" id="checkAllForStorage" class="form-control" />'
				] ,
			'shouldSetAjaxFiltersContainerClass' => false]); ?></tr>
		<?php if (count($storages) == 0) { ?>
				<tr><td colspan="10"><center>nessun articolo trovato</center></td></tr><?php
			}
			?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php foreach ($storages as $key => $storage) { ?>
				<tr>
					<td class="printCheckbox"><?= $this->Form->input('toprint.'.$storage['Storage']['id'],['div' => false, 'type'=>'checkbox','label' => false, 'class'=>'ajax-filter-input jsToCsv', 'value'=>$storage['Storage']['id']]); ?></td>
					<td> <?= h($storage['Storage']['codice']); ?></td>
					<td> <?= h($storage['Storage']['descrizione']); ?></td>
					<td><?= $this->Html->link($storage['Supplier']['name'], ['controller' => 'suppliers', 'action' => 'edit', $storage['Supplier']['id']]); ?></td>
                    <td> <?= h($storage['Category']['description']); ?></td>
                    <td style="text-align:right;"> <?= $_SESSION['Auth']['User']['dbname'] == "login_GE0047" ? h(number_format((float)$storage['Storage']['prezzo'], 4, ',', '.')) : h(number_format((float)$storage['Storage']['prezzo'], 2, ',', '.')) ?>&nbsp;€</td>
					<?php if($_SESSION['Auth']['User']['dbname'] != "login_GE0047"){ ?><td style="text-align:right;"> <?= h(number_format((float)$storage['Storage']['last_buy_price'], 2, ',', '.')); ?>&nbsp;€</td><?php } ?>
					<td> <?= h($storage['Units']['description']); ?></td>
					<?php if($_SESSION['Auth']['User']['dbname'] != "login_GE0047"){ ?><td> <?= h($storage['Storage']['barcode']); ?></td><?php }?>
					<td style="text-align:right;">
						<?php
						    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
						        $quantityDefault = $utilities->getAvailableQuantity($storage['Storage']['id'], 1);
						        $quantityDefaultNegative = $utilities->getAvailableQuantity($storage['Storage']['id'], -1);
                                $quantityDeposits = $utilities->getAvailableQuantity($storage['Storage']['id']);
                                $quantityDefaultGood = $quantityDefault + $quantityDefaultNegative;
							    echo number_format($quantityDefaultGood,2,',','.');
							}else{
                            	echo number_format($utilities->getAvailableQuantity($storage['Storage']['id']),2,',','.');
							}
						?>
					</td>
					<?php
					    if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
					    ?>
					<td style="text-align:right;">
						<?php
								echo number_format(($quantityDeposits - $quantityDefault),2,',','.');
						?>
					</td>
					<?php } ?>
					<td class="actions">
						<?php
							echo $this->Html->link($iconaModifica, ['action' => 'edit', $storage['Storage']['id']],['title'=>__('Modifica articolo'),'escape'=>false]);

							if($utilities->storageHasLeaf($storage['Storage']['id']) == false && $storage['Storage']['sn'] == 0 )
							{
								echo $this->Html->link($iconaPlus, ['action' => 'load', $storage['Storage']['id']],['title'=>__('Carico'),'escape'=>false]);
								echo $this->Html->link($iconaMinus, ['action' => 'unload', $storage['Storage']['id']],['style'=>'margin-right:3px;' ,'title'=>__('Scarico'),'escape'=>false]);
							}
							else
							{
								echo $this->Html->link($iconaPlusOff, ['#'],['style'=>'margin-right:3px;' ,'title'=>__('Carico articolo non abilitato'),'escape'=>false]);
								echo $this->Html->link($iconaMinusOff, [ '#'],['style'=>'margin-right:3px;' ,'title'=>__('Scarico articolo non abilitato'),'escape'=>false]);
							}

							if(ADVANCED_STORAGE_ENABLED)
							{
								echo $this->Html->link($iconaDettaglioMagazzino, ['action' => 'depositdetails', $storage['Storage']['id']],['style'=>'margin-right:3px;' ,'title'=>__('Dettaglio magazzino'),'escape'=>false]);
								if($utilities->getAvailableQuantity($storage['Storage']['id']) == 0)
								{
									echo $this->Html->link($iconaVarianti, ['action' => 'indexvariation', $storage['Storage']['id']],['style'=>'margin-right:3px;' ,'title'=>__('Gestione varianti articolo'),'escape'=>false]);
								}
								else
								{
									echo $this->Html->link($iconaVariantiOff, ['#'],['style'=>'margin-right:3px;' ,'title'=>__('Varianti non abilitate'),'escape'=>false]);
								}
							}

							//if($utilities->storageHasLeaf($storage['Storage']['id']) == false && $storage['Storage']['sn'] == 0 )
								if($utilities->storageHasLeaf($storage['Storage']['id']) == false || $storage['Storage']['sn'] == 1 )
                            {
                                echo $this->Html->link('<i class="fa fa-list  verde" style="vertical-align:middle;font-size:20px;" storageId = "'. $storage['Storage']['id'] .'" ></i>', ['action' => 'index','controller'=>'Storagemovements', $storage['Storage']['id']],['title'=>__('Visualizza movimenti articolo'),'escape'=>false]);
                            }

							if($utilities->getAvailableQuantity($storage['Storage']['id']) == 0 && $utilities->storageHasLeaf($storage['Storage']['id']) == false && $storage['Storage']['sn'] == 0)
							{
								echo $this->Form->postLink($iconaElimina, ['action' => 'delete', $storage['Storage']['id']], ['title'=>__('Elimina'),'escape'=>false], __('Sei sicuro di voler eliminare l\' articolo ?', $storage['Storage']['id']));
							}
							else
							{
								//echo $this->Form->postLink($iconaElimina, array('title'=>__('Elimina'),'escape'=>false), __('Sei sicuro di voler eliminare l\' articolo ?', $storage['Storage']['id']));
								//echo '<i class="uk-icon-trash icon" style="color:#dedede" title="Non possono essere eliminati articoli la cui quantità è maggiore di 0"></i>';
							}
						?>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<?=  $this->element('Form/Components/Paginator/component'); ?>
</div>
<script>

	var arrayToPost = [];

 	addLoadEvent(function(){addCheckboxSelectedOnStorage()},'addCheckboxSelectedOnStorage');

	checkSelected();
	checkAllInIndex();

	function afterAjaxFilterCallback()
	{
		addCheckboxSelectedOnStorage();
		checkSelected();
		checkAllInIndex();
	}

	function checkAllInIndex()
	{
		$("#checkAllForStorage").prop('checked',false);
		$("#checkAllForStorage").click(
		function()
		{
			var buttonAllClickChecked = this.checked;
				$(".jsToCsv").each(function()
				{
					if(this.checked != buttonAllClickChecked)
					{
						$(this).click();
					}
				});
		});
	}

	function addCheckboxSelectedOnStorage()
	{
		$(".jsToCsv").click(
			function()
			{
				if(this.checked == true)
				{
					addToArray(arrayToPost, $(this).val());
				}

			if(this.checked == false)
			{
				removeFromArray(arrayToPost, $(this).val());
			}
		});
	}

	function checkSelected()
	{
		$.each(arrayToPost, function( index, value )
		{
			$("#toprint"+value).prop('checked',true);
		});
	}

	function addToArray(arrayToPost, id)
	{
		if(arrayToPost.indexOf(id) === -1)
		{
	    	arrayToPost.push(id);
    	}
 		return  arrayToPost
	}

	function removeFromArray(arrayToPost,id)
	{
		var index = arrayToPost.indexOf(id);
	 	if (index > -1)
	 	{
	 		arrayToPost.splice(index,1);
	 	}
	 	return arrayToPost
	 }

</script>
