<?= $this->element('Form/Components/Paginator/loader'); ?>
<?= $this->element('Form/Components/AjaxFilter/loader') ?>
<div class="portlet-title">
	<div class="caption">
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Dettaglio magazzino del deposito  dell\'articolo ' .$storages['Storage']['descrizione'] . ' (per variante)' ); ?></span>
	</div>
	<div class=" -badge tools">
			<?= $this->Html->link('< torna ai depositi', ['controller'=>'storages','action' => 'depositdetails',$storages['Storage']['id'] ], ['title'=>__('Indietro'),'escape' => false,'class' => "blue-button btn-button btn-outline dropdown-toggle", 'style'=>'text-transform:none;margin-left:5px;padding:5px !important;padding-left:2px !important;background-color:#dcd6d6 !imporant;margin-right:20px;']); ?>		
	</div>
</div>
<div class="storages index">
	<table class="table table-bordered table-striped table-condensed flip-content">
		<thead class ="flip-content">
			<tr><?= $this->element('Form/Components/AjaxSort/component', ['elements' => $sortableFields]); ?></tr>
			<?php if (count($storages) > 0) { ?>
			<tr><?php // $this->element('Form/Components/AjaxFilter/component', ['elements' =>$filterableFields]); ?></tr>
			<?php } ?>
		</thead>
		<tbody class="ajax-filter-table-content">
			<?php
			$sumOfQuantity = 0;
			foreach ($variations as $variation) { ?>
				<tr>
					<td> <?= h($variation['description']); ?></td>
				
					<td class="right"> 
						<?php  
							echo number_format($currentVariationQuantity = $utilities->getAllVariationAvailableQuantity($storages['Storage']['id'],$currentDeposit,$variation['id']),2,'.','');
							$sumOfQuantity += $currentVariationQuantity;
						?>
					</td>
					<td>
						<?php
						 // Solo le varianti non univoche possono avvere sottovarianti
						  // debug($variation);
						 if(isset($variation['sn']) && $variation['sn'] == 0)
						 {
							echo $this->Html->link($iconaDettaglioMagazzino, ['action' => 'depositvariationdetail', $storages['Storage']['id'],$currentDeposit,$variation['id']],['style'=>'margin-right:3px;' ,'title'=>__('Dettaglio varianti per deposito'),'escape'=>false]); 
						 } 
						 ?>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td><b>Totale articoli a magazzino</b></td>
				<td class="right"> <b><?= number_format($sumOfQuantity,2,',','.') ?></b></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<?php // $this->element('Form/Components/Paginator/component'); ?>
</div>
