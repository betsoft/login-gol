<script>
	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			
			$( "#fornitore" ).autocomplete({source: fornitore});

		});
</script>
<?= $this->Form->create('Storage', ['class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data']); ?>
		<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo articolo di magazzino'); ?></span>
	 <div class="col-md-12"><hr></div>

		<?= $this->Form->hidden('movable',['value'=>1]); ?>

      <div class="form-group col-md-12">
	     <div class="col-md-2" style="float:left;" > 
		    <label class="form-label"><strong>Codice</strong><!--i class="fa fa-asterisk"--></i></label>
      		<div class="form-controls">
			   <?= $this->Form->input('codice', ['label' => false, 'div' => false, 'class'=>'form-control']); ?>
     	 	</div>
     	 </div>
		 <div class="col-md-2" style="float:left;margin-left:10px;" > 
		    <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
	        <div class="form-controls">
			   <?= $this->Form->input('descrizione',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'lunghezza' ,'required'=>true]); ?>
			</div>
	     </div>
         <div class="col-md-4" style="float:left;margin-left:10px;" > 
		    <label class="form-label"><strong>Fornitore</strong></label>
      	    <div class="form-controls">
			   <?= $this->Form->input('codice_fornitore',['label' => false, 'div' => false, 'class'=>'form-control','id'=>'fornitore']); ?>
            </div>
         </div>
	   </div>

	<div class="form-group col-md-12">
		<div class="col-md-2" style="float:left;" > 
			<label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
			<div class="form-controls">
    	        <?= $this->Form->input('prezzo',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
		    </div>
	    </div>

         <div class="col-md-2" style="float:left;margin-left:10px;" >
                <label class="form-margin-top form-label"><strong>Iva predefinita</strong></label>
                <div class="form-controls">
                    <?= $this->Form->input('vat_id',['div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true]); ?>
                </div>
        </div>

	 	<div class="col-md-2" style="float:left;margin-left:10px;" > 
			<label class="form-margin-top form-label"><strong>Unità di misura</strong></label>
			<div class="form-controls">
    	        <?= $this->Form->input('unit_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true]); ?>
		    </div>
		</div>
		<?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){?>
		<div class="col-md-2" style="float:left;margin-left:10px;" >
            <label class="form-margin-top form-label"><strong>Categoria</strong></label>
            <div class="form-controls">
                <?= $this->Form->input('category_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true]); ?>
            </div>
        </div>
        <?php } ?>
	</div>
	 <div class="col-md-2" style="float:left;margin-left:10px;" >
	   <label class="form-margin-top form-label"><strong>Ultimo prezzo d'acquisto</strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('last_buy_price',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
   </div>
    <div class="col-md-2" style="float:left;margin-left:10px;" >
		    <label class="form-label"><strong>Codice a barre</strong></label>
      		<div class="form-controls">
			   <?= $this->Form->input('barcode', ['label' => false, 'div' => false, 'class'=>'form-control']); ?>
            </div>
         </div>
</div>
    <div class="col-md-12"><hr></div>
  			<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
  			<?= $this->Form->end(); ?>
