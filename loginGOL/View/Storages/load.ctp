<?=


$this->Form->create('Storage', array('class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data')); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Caricamento articolo di magazzino'); ?></span>
	 <div class="col-md-12"><hr></div>

	<div class="form-group col-md-12">
	   <div class="col-md-3"  >
	      <label class="form-margin-top form-label"><strong>Codice</strong></label>
          <div class="form-controls">
		     <?= $this->Form->input('code',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>true,'value'=>$selectedStorage['Storages']['codice']]); ?>
		  </div>
        </div>
	    <div class="col-md-3" >
	        <label class="form-margin-top form-label"><strong>Descrizione</strong></label>
            <div class="form-controls">
		        <?= $this->Form->input('descrizione',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>true,'value'=>$selectedStorage['Storages']['descrizione']]); ?>
		    </div>
        </div>
    </div>

     <div class="form-group col-md-12">
        <div class="col-md-4"  >
	    <label class="form-margin-top form-label"><strong>Quantità da caricare</strong></label>
        <div class="form-controls">
		    <?= $this->Form->input('quantity',['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'type'=>'number']); ?>
		</div>
     </div>

        <div class="col-md-4" >
   	       <label class="form-margin-top form-label"><strong>Deposito</strong></label>
           <div class="form-controls">
   		       <?= $this->Form->input('deposit_id',array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'type'=>'select','options'=>$depositArray, 'value'=>$mainDeposit['Deposits']['id'])); ?>
   		    </div>
        </div>


    <div class="col-md-4">
	    <label style="margin-top:5px;"><strong>Nota (sulla motivazione del carico manuale)</strong></label>
      	<div class="form-controls">
		    <?= $this->Form->input('movementDescription',array('label' => false, 'div' => false, 'class'=>'form-control','id'=>'fornitore')); ?>
        </div>
    </div>
    <div class="col-md-4" >
   	       <label class="form-margin-top form-label"><strong>Prezzo d'acquisto</strong></label>
           <div class="form-controls">
   		       <?= $this->Form->input('last_buy_price',array('div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'type'=>'number','step'=>'0.0001', 'min'=>0,'value'=>0)); ?>
   		      </div>
        </div>
    </div>
      <div class="col-md-12"><hr></div>
<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>

