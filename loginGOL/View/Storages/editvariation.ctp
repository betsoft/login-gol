<script>

	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
  					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			$( "#fornitore" ).autocomplete({source: fornitore});
		});
</script>
<div class="form-group caption-subject bold uppercase col-md-12 " style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Modifica variante articolo</div>
 <div class="col-md-12"><hr></div>
    <?=  $this->Form->create('Storage', ['class' => 'uk-form uk-form-horizontal','enctype'=>'multipart/form-data']); ?>
    <?= $this->Form->input('id',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
    <?= $this->Form->hidden('movable',['value'=>1]); ?>
    <?=  $this->Form->hidden('codice_fornitore',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'fornitore' ,$this->data['Storage']['codice_fornitore']]); ?>
    <?=  $this->Form->hidden('supplier_id',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'idfornitore' ,$this->data['Storage']['supplier_id']]); ?>
    
   <div class="form-group col-md-12">
    <div class="col-md-3" style="float:left;" > 
		<label class="form-label"><strong>Codice</strong></label>
		<div style="width:100%;margin-left:0px;">
			<?= $this->Form->input('parentcode', ['label' => false, 'div'=>false, 'class'=>'','style'=>"float:left;background-color:#dedede;width:50%;" ,'readonly'=>true,'value'=>$parentCode]); ?>
			<?= $this->Form->input('codice', ['label' => false, 'div'=>false, 'value'=>str_replace($parentCode,'',$this->data['Storage']['codice']), 'required'=>true,'style'=>"width:50%;"]); ?>
		</div>
    </div>
   
	<div class="col-md-3" style="float:left;margin-left:10px;" > 
		<label class="form-margin-top form-label"><strong>Descrizione</strong></label>
    	<div style="width:100%;margin-left:0px;">
    		<?= $this->Form->input('parentdescription', ['label' => false, 'div'=>false, 'class'=>'','style'=>"float:left;background-color:#dedede;width:50%;" ,'readonly'=>true,'value'=>$parentDescription]); ?>
			<?= $this->Form->input('descrizione', ['label' => false, 'div'=>false, 'value'=>str_replace($parentDescription,'',$this->data['Storage']['descrizione']), 'required'=>true,'style'=>"width:50%;"]); ?>
		</div>
	</div>

  <div class="col-md-2" style="float:left;margin-left:10px;" > 
    <label class="form-margin-top form-label"><strong>Codice a barre</strong></label>
  <div class="form-controls">
	<?= $this->Form->input('barcode',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
  </div>
  </div>

 

     <div class="form-group col-md-12">
		<div class="col-md-2" style="float:left;" > 
	<label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
  <div class="form-controls">
	<?= $this->Form->input('prezzo',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
      </div>
  </div>
  
  
   <div class="col-md-2"  > 
        <label class="form-margin-top form-label"><strong>Iva predefinita</strong></label>
		<div class="form-controls">
    	    <?= $this->Form->input('vat_id',['div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true]); ?>
		</div>
	</div>
  
  <div class="col-md-2"  > 
			<label class="form-margin-top form-label"><strong>Unità di misura</strong></label>
			<div class="form-controls">
    	<?= $this->Form->input('unit_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true]); ?>
		</div>
		</div>
		   <div class="col-md-4" > 
	    <label class="form-margin-top form-label"><strong>Variante univoca</strong></label>
		<div class="form-controls">
			<?= $this->Form->input('sn',['div' => false, 'label' => false, 'class'=>'form-control','options'=>['0'=>'No','1'=>'Sì'] ,'type'=>'select','empty'=>false ,'readonly'=>true,'disabled'=>true]); ?>
		</div>
    </div>
		</div>
 <div class="col-md-12"><hr></div>
	 <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'indexvariation', 'passedValue'=>$parentId]); ?></center>
    	<?= $this->Form->end(); ?>
