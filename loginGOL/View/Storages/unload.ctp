<?= $this->element('Js/setmaxstoragequantity'); ?>
<?= $this->element('Js/checkmaxstoragequantityerror'); ?>

<?= 

$this->Form->create('Storage', ['class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data']); ?>
    <span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Scarico articolo di  magazzino'); ?></span>
	 <div class="col-md-12"><hr></div>
	<div class="form-group col-md-12">
	   <div class="col-md-3" style="float:left;" >  
	    <label class="form-margin-top form-label"><strong>Codice</strong></label>
        <div class="form-controls">
		    <?= $this->Form->input('code',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>true,'value'=>$selectedStorage['Storages']['codice']]); ?>
		</div>
     </div>
 	<div class="col-md-3" style="float:left;margin-left:10px;">
	 <label class="form-margin-top form-label"><strong>Descrizione</strong></label>
        <div class="form-controls">
		    <?= $this->Form->input('descrizione',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>true,'value'=>$selectedStorage['Storages']['descrizione']]); ?>
		</div>
     </div>
  	<div class="col-md-3" style="float:left;margin-left:10px;">
	 <label class="form-margin-top "><strong>Quantità a magazzino (deposito selezionato)</strong></label>
        <div class="form-controls">
		    <?= $this->Form->input('maxquantity',['div' => false, 'label' => false, 'class'=>'form-control','disabled'=>true, 'value'=> $maxStorageDefault]); ?>
		</div>
     </div>
	 </div>
  
    <div class="form-group col-md-12">
        <div class="col-md-4" style="float:left;" >  
	    <label class="form-margin-top form-label"><strong>Quantità da scaricare</strong></label>
        <div class="form-controls">
		    <?= $this->Form->input('quantity',['div' => false, 'label' => false, 'class'=>'form-control jsQuantity','required'=>true,'type'=>'number']); ?>
		</div>
     </div>

   <?php
     // Nello scarico non ci deve essere il prezzo d'acquisto
     if(ADVANCED_STORAGE_ENABLED) 
     { ?>
           <div class="col-md-4" > 
   	       <label class="form-margin-top form-label"><strong>Deposito</strong></label>
           <div class="form-controls">
   		       <?= $this->Form->input('deposit_id',['div' => false, 'label' => false, 'class'=>'form-control jsDeposit','required'=>true,'type'=>'select','options'=>$depositArray, 'value'=>$mainDeposit['Deposits']['id']]); ?>
   		      </div>
        </div>
        
     <?php } ?>
  
         <div class="col-md-4" > 
    	    <label class="form-margin-top form-label"><strong>Note</strong></label>
          	<div class="form-controls">
    			<?= $this->Form->input('movementDescription',['label' => false, 'div' => false, 'class'=>'form-control','id'=>'fornitore']); ?>
            </div>
        </div>
    </div>
      <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
<?= $this->Form->end(); ?>


<script>

       

    var errore = 0;

    // Setto il max quantity sul deposito
    $(".jsDeposit").change(
        function()
        {
           setMaxStorageQuantity($("#StorageDepositId").val(),<?= $storageId ?>,"#StorageQuantity",0); 
           $.ajax
            ({
            method: "POST",
            url: "<?= $this->Html->url(["controller" => "storages","action" => "getAvailableQuantity"]) ?>",
            data: 
            {
                deposit : $("#StorageDepositId").val(),
                storageId : <?= $storageId ?>,
            },
            success: function(data)
            {
                if(data <0 ){ data = 0;}
                $("#StorageMaxquantity").val(data);	   
            }
        })
        });
        
    setMaxStorageQuantity($("#StorageDepositId").val(),<?= $storageId ?>,"#StorageQuantity",0); 
    
    

    $("#StorageUnloadForm").on('submit.default',function(ev) 
    {
    });
            
    $("#StorageUnloadForm").on('submit.validation',function(ev) 
    {              
        ev.preventDefault(); // to stop the form from submitting
        
        /* Validations go here */
        checkError(errore);
        
    });
            
            
    function checkError(errore)
    {
        errore = checkMaxStorageQuantityError(".jsQuantity",errore);
        if(errore == 11)
        {
             $.confirm({
			    title: 'Scarico deposito.',
    		    content: 'Si sta cercando di scaricare una o più quantità superiori a quelle presenti nel deposito, continuare comunque?',
    		    type: 'orange',
    		    buttons: 
    		    {
            		Ok: function () 
            		{
                        action:
                        {
                           $("#StorageUnloadForm").trigger('submit.default');
                        }
                    },
                    annulla: function () 
                    {
                        action: 
                        { 
                            // Nothing
                        }
                    },
                }
            });	
        }
        else
        {
            $("#StorageUnloadForm").trigger('submit.default');   
        }
    }
    
</script>