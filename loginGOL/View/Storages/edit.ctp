<script>

	$(document).ready(
		function() {
			var fornitore = [
			<?php
				foreach($suppliers as $supplier)
				{
  					echo '"' . addslashes($supplier) . '",';
				}
			?>];
			$( "#fornitore" ).autocomplete({source: fornitore});
		});
</script>
<div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Modifica articolo di magazzino</div>
 <div class="col-md-12"><hr></div>

<?=  $this->Form->create('Storage', ['class' => 'uk-form uk-form-horizontal','enctype'=>'multipart/form-data']); ?>
<?= $this->Form->input('id',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
<?= $this->Form->hidden('movable',['value'=>1]); ?>

<div class="form-group col-md-12">
   <div class="col-md-2" style="float:left;" >
      <label class="form-label"><strong>Codice<i class="fa fa-asterisk"></i></strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('codice',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
   </div>
   <div class="col-md-2" style="float:left;margin-left:10px;" >
      <label class="form-margin-top form-label"><strong>Descrizione<i class="fa fa-asterisk"></i></strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('descrizione',['div' => false, 'label' => false, 'class'=>'form-control' ,'required'=>true]); ?>
      </div>
   </div>
   <div class="col-md-4" style="float:left;margin-left:10px;" >
	   <label class="form-margin-top form-label"><strong>Fornitore</strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('codice_fornitore',['div' => false, 'label' => false, 'class'=>'form-control','id'=>'fornitore' ]); ?>
      </div>
   </div>

</div>
<div class="form-group col-md-12">
   <div class="col-md-2" style="float:left;" >
	   <label class="form-margin-top form-label"><strong>Prezzo di vendita predefinito</strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('prezzo',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
   </div>
   <div class="col-md-2" style="float:left;margin-left:10px;" >
      <label class="form-margin-top form-label"><strong>Iva predefinita</strong></label>
		<div class="form-controls">
    	   <?= $this->Form->input('vat_id',['div' => false, 'label' => false, 'class'=>'form-control','options'=>$vats,'empty'=>true]); ?>
		</div>
	</div>
   <div class="col-md-2" style="float:left;margin-left:10px;" >
	   <label class="form-margin-top form-label"><strong>Unità di misura</strong></label>
		<div class="form-controls">
    	   <?= $this->Form->input('unit_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true]); ?>
		</div>
	</div>
	<?php if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){ ?>
        <div class="col-md-2" style="float:left;margin-left:10px;" >
           <label class="form-margin-top form-label"><strong>Categoria</strong></label>
            <div class="form-controls">
               <?= $this->Form->input('category_id',['div' => false, 'label' => false, 'class'=>'form-control','empty'=>true]); ?>
            </div>
        </div>
	<?php } ?>
	 <div class="col-md-2" style="float:left;margin-left:10px;" >
	   <label class="form-margin-top form-label"><strong>Ultimo prezzo d'acquisto</strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('last_buy_price',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
   </div>
    <div class="col-md-2" style="float:left;margin-left:10px;" >
      <label class="form-label"><strong>Codice a barre</strong></label>
      <div class="form-controls">
	      <?= $this->Form->input('barcode',['div' => false, 'label' => false, 'class'=>'form-control' ]); ?>
      </div>
   </div>
</div>
 <div class="col-md-12"><hr></div>

<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>

<?= $this->Form->end(); ?>
