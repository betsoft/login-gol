<?= $this->element('Js/variables'); ?>
<?= $this->element('Js/clonableadvanced'); ?>

<?= $this->Form->create('Storage', array('class' => 'uk-form uk-form-horizontal' ,'enctype'=>'multipart/form-data')); ?>
		<!--span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?php // __('Trasferimento tra depositi'); ?></span-->
		<?=	$this->element('Form/formelements/indextitle',['indextitle'=>'Trasferimento tra depositi']); ?>
		 <div class="col-md-12"><hr></div>

		<div class="form-group col-md-12">
            <div class="col-md-2" style="float:left;">
                <label class="form-label"><strong>Deposito origine</strong><i class="fa fa-asterisk"></i></label>
                <div class="form-controls">
                    <?=  $this->Form->input('startDeposit', ['label' => false,'class' => 'form-control','options'=>$deposits,'value'=>$defaultDeposit]);?>
               </div>
            </div>
            <div class="col-md-2" style="float:left;margin-left:10px;">
                <label class="form-label form-margin-top"><strong>Deposito destinazione</strong><i class="fa fa-asterisk"></i></label>
                <div class="form-controls">
                	<?=  $this->Form->input('endDeposit', ['label' => false,'class' => 'form-control','options'=>$deposits,'empty'=>true]);?>
                </div>
          </div>
      </div>
  <div class="col-md-12"><hr></div>

    <div class="col-md-12">
    <div class="form-group caption-subject bold uppercase col-md-12" style="color:#589AB8;font-size:16px;margin-top:20px;margin-bottom:20px;text-align:left;" >Selezione articoli da trasferire</div>
     <div class="col-md-12"><hr></div>
	    <?= $this->element('Form/Simplify/action_add_clonable_row',['buttonTitle'=>'Aggiungi articolo']); ?>
    <fieldset id="trasferimentodeposito" class="col-md-12">
    	<div class="principale contacts_row clonableRow originale ultima_riga">
	        <span class="remove icon rimuoviRigaIcon cross fa fa-remove" title="Rimuovi riga"  hidden ></span>
          <div class="col-md-12">
        <div class="col-md-2">
           <label class="form-label"><strong>Codice</strong><i class="fa fa-asterisk"></i></label>
             <?=  $this->Form->input('Good.0.codice', [ 'div' => false, 'label' => false,'class' => 'form-control' ,'maxlenght'=>11]); ?>
           </div>
           <div class="col-md-6">
             <label class="form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
             <?php
                echo $this->Form->input('Good.0.oggetto', ['div' => false,'label' => false,'class' => 'form-control jsDescription','required'=>'required']);
                echo $this->Form->hidden('Good.0.storage_id');
                echo $this->Form->hidden('Good.0.variation_id',['type'=>'text']);
                echo $this->Form->hidden('Good.0.movable',['class'=>'jsMovable']);
            ?>
           </div>
           <div class="col-md-2">
              <label class="form-margin-top"><strong>Quantità massima disponibile</strong></label>
              <?= $this->Form->input('Good.0.disabled', ['label' => false,'class' => 'form-control','disabled','style'=>'text-align:right;']);?>
           </div>
           <div class="col-md-2">
              <label class="form-margin-top"><strong>Quantità</strong><i class="fa fa-asterisk"></i></label>
              <?php
                echo $this->Form->input('Good.0.quantita', ['required'=>true,'label' => false,'default' => 1,'class' => 'form-control jsQuantity', 'step'=> '0.001', 'min' => '0']);?>
           </div>
          </div>
           <div class="col-md-12"><hr></div>
        </div>
        </fieldset>
         <?= $this->element('Form/Simplify/action_add_clonable_row_bottom',['buttonTitle'=>'Aggiungi articolo']); ?>
	</div>

	  <center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
      <?=  $this->Form->end(); ?>
      <?= $this->element('Js/variables'); ?>

<script>



  $(document).ready
    (
       function()
       {

           $("#StorageEndDeposit").val('') ;
           $("#Good0Oggetto").val('') ;
           $("#Good0Codice").val('') ;
           $("#Good0Quantita").val('') ;

            var articoli = setArticlesNew(JSON.parse('<?= addslashes(json_encode($magazzini)); ?>'));
            var codici = setCodesNew(JSON.parse('<?= addslashes(json_encode($magazzini)); ?>'));

            indexdepositmovementclonable(0,articoli,codici);

			$("#StorageIndexdepositmovementForm").on('submit.default',function(ev)
            {
            });

            $("#StorageIndexdepositmovementForm").on('submit.validation',function(ev)
            {
            	  ev.preventDefault();

                  /* Controllo che non siano staiti inseriti articoli duplicati */
                 var arrayOfDescription = []
                  $(".jsDescription").each(function()
                   {
                        if(arrayOfDescription[$(this).val()])
                        {
                            arrayOfDescription[$(this).val()] = arrayOfDescription[$(this).val()] + 1;
                        }
                        else
                        {
                            arrayOfDescription[$(this).val()] = 1;
                        }
                    })

                    for (var k in arrayOfDescription)
                    {
                        if(arrayOfDescription[k] > 1)
                        {
                            $.alert
                            ({
                                icon: 'fa fa-warning',
                            	title: '',
                                content: 'Attenzione alcuni articoli sono stati inseriti più volte.',
                                type: 'orange',
                            });
                            return false;
                        }
                    }
                    /* Fine controllo */


            	  if($("#StorageStartDeposit").val() == '')
            	  {
                      $.alert
                      ({
                          icon: 'fa fa-warning',
                          title: '',
                          content: 'Attenzione non è stato selezionato il deposito di origine.',
                          type: 'orange',
                      });
            	  	  return false;
            	  }

            	  if($("#StorageEndDeposit").val() == '')
            	  {
            	     $.alert
                     ({
                          icon: 'fa fa-warning',
                          title: '',
                          content: 'Attenzione non è stato selezionato il deposito di destinazione.',
                          type: 'orange',
                     });
            	  	 return false;
            	  }

            	  if($("#StorageStartDeposit").val() == $("#StorageEndDeposit").val())
            	  {
            	     $.alert
                     ({
                          icon: 'fa fa-warning',
                          title: '',
                          content: 'Attenzione deposito di origine e destinazione devono essere differenti.',
                          type: 'orange',
                     });
            	  	return false;
            	  }

                 $("#StorageIndexdepositmovementForm").trigger('submit.default');

            });

			$("#StorageStartDeposit").change(function()
			{
			    // Cancello le righe
			    $('.clonableRow').each
			    (
			        function()
			        {
	                    if($(this).hasClass('originale'))
	                    {
	                        /* Nothing */
	                    }
	                    else
	                    {
	                        $(this).remove();
	                    }
    			    }
		       )

		       setArticleAutocomplete();

			});


			/** Definizione del clonable **/
			var fields = ['Codice','Oggetto','Disabled','Quantita','StorageId','VariationId'];
			var labels = ['codice','oggetto','disabled','quantita','storage_id','variation_id'];
			var newlinesvalue = ['','','',1,'','',''];
			clonableadvanced("#aggiungi_riga",fields,labels,newlinesvalue,'Good',"indexdepositmovementclonable",articoli,codici);
			addChangeFunctionToDescription(articoli);
       });

       function setArticleAutocomplete(data)
       {
          // Modifico la variabile articoli
          console.log("test2");
		  $.ajax
		  ({
		        method: "POST",
			    url: "<?= $this->Html->url(["controller" => "storages","action" => "getDepositArticles"]) ?>",
                data:
				{
				    deposit : $("#StorageStartDeposit").val(),
 				},
				success: function(data)
				{
	                articoli = setArticlesNew(JSON.parse(data));
	                codici = setCodesNew(JSON.parse(data));

                    indexdepositmovementclonable(0,articoli,codici);

				    $("#aggiungi_riga").unbind("click");
				    var fields = ['Codice','Oggetto','Disabled','Quantita','StorageId','VariationId'];
			        var labels = ['codice','oggetto','disabled','quantita','storage_id','variation_id'];
			        var newlinesvalue = ['','','',1,'','',''];
				    clonableadvanced("#aggiungi_riga",fields,labels,newlinesvalue,'Good',"indexdepositmovementclonable",articoli);
	                addChangeFunctionToDescription(articoli);
			    }
		    });
       }

       function indexdepositmovementclonable(index,articoli,codici)
       {

          $("#Good"+index+"Oggetto").autocomplete
		  ({
			   source: articoli,
               focus: function( event, ui )
               {
                  event.preventDefault();
                  return false;
               },
               select: function(event, ui)
               {
                  event.preventDefault();
                  $("#Good"+index+"Codice").val(ui.item.codice);
                  $("#Good"+index+"Oggetto").val(ui.item.label);
				  $("#Good"+index+"StorageId").val(ui.item.value);

				  $.ajax
				  ({
				      method: "POST",
					  url: "<?= $this->Html->url(["controller" => "storages","action" => "getMaxDepositAvailability"]) ?>",
					  data:
					  {
					     deposit : $("#StorageStartDeposit").val(),
						 storageId : ui.item.value,
						 variation : ui.item.variation,
					  },
					  success: function(data)
                      {
					     $("#Good"+index+"Disabled").val(JSON.parse(data));
						 $("#Good"+index+"Quantita").attr('max',JSON.parse(data));
					  }
				  });
                }
			});
			addChangeFunctionToDescription(articoli);


		  $("#Good"+index+"Codice").autocomplete
		  ({
			   source: codici,
               focus: function( event, ui )
               {
                  event.preventDefault();
                  return false;
               },
               select: function(event, ui)
               {
                  event.preventDefault();
                //  $("#Good"+index+"Codice").val(ui.item.codice);
                  $("#Good"+index+"Oggetto").val(ui.item.descrizione);
				  $("#Good"+index+"StorageId").val(ui.item.value);

				  $.ajax
				  ({
				      method: "POST",
					  url: "<?= $this->Html->url(["controller" => "storages","action" => "getMaxDepositAvailability"]) ?>",
					  data:
					  {
					     deposit : $("#StorageStartDeposit").val(),
						 storageId : ui.item.value,
					  },
					  success: function(data)
                      {
					     $("#Good"+index+"Disabled").val(JSON.parse(data));
						 $("#Good"+index+"Quantita").attr('max',JSON.parse(data));
					  }
				  });
                }
			});
			addChangeFunctionToDescription(articoli,codici);
       }

       // Funzione che controlla che gli articoli a magazzino siano effettivamente a magazzino
       function addChangeFunctionToDescription(articoli)
       {
            $(".jsDescription").unbind('change');

            $(".jsDescription").change(function()
            {
                var articoloExist = false;
                var myLabel = $(this).val();

                $(articoli).each(function(key,value)
                {
                    // Se Esiste nella lista tutto ok.
                    if(value.label == myLabel)
                    {
                        articoloExist = true;
                    }
                })

                if(articoloExist)
                {
                    resetColor();
                }
                else
                {
                    resetColor();
                    if($(this).val() != '')
                    {
                        $(this).val('');
                        $(this).css('border-color',"red");
                        $(this).parent().append('<span class="fastErroreMessage" style="color:red">Attenzione l\'articolo inserito non esiste a magazzino</span>');
                    }
                }
            })
       }

       // Resetta gli alert sulle pagine
       function resetColor()
       {
           $(".fastErroreMessage").remove();
           $(".jsDescription").css('border-color',"#ddd");
       }
</script>
