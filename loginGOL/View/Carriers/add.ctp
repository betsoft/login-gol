<?= $this->element('Form/Components/FilterableSelect/loader') ?>
<?= $this->Html->script(['plugins/bootstrap/js/bootstrap.js']); ?>
<?= $this->Form->create('Carrier'); ?>
	<span class="caption-subject bold uppercase" style="color:#589AB8;font-size:16px;"><?= __('Nuovo vettore') ?></span>
  	 <div class="col-md-12"><hr></div>
    <div class ="form-group col-md-12">
        <div class="col-md-2" style="float:left">
	        <label class="form-margin-top form-label"><strong>Descrizione</strong><i class="fa fa-asterisk"></i></label>
	         <div class="form-controls">
		           <?= $this->Form->input('name', ['div' => false, 'label' => false, 'class'=>'form-control','required'=>true,'pattern'=>PATTERNBASICLATIN]); ?>
	         </div>
	    </div>
	    <div class="col-md-2" style="float:left;margin-left:10px;">
	  		<label class="form-margin-top form-label"><strong>Codice fiscale</strong></label>
	  		<div class="form-controls">
				<?= $this->Form->input('cf',['div' => false, 'label' => false, 'class'=>'form-control','minlength'=>11]); ?>
			</div>
		</div>
		<div class="col-md-2" style="float:left;margin-left:10px;">
	  		<label class="form-margin-top form-label"><strong>Partita IVA</strong></label>
	  		<div class="form-controls">
				<?= $this->Form->input('piva',['div' => false, 'label' => false, 'class'=>'form-control']); ?>
			</div>
		</div>
		<div class="col-md-2" style="float:left;margin-left:10px;">
		<label class="form-label form-margin-top"><strong>Nazione</strong></label>
   		<div class="form-controls">
			<?= $this->element('Form/Components/FilterableSelect/component', ["name" => 'nation_id',"aggregator" => '',"prefix" => "nation_id","list" => $nations,"options" => [ 'multiple' => false,'required'=> false, 'default'=>'106']]); ?>
   		</div>
  		 </div>
		<div class="col-md-2" style="float:left;margin-left:10px;">
	  		<label class="form-margin-top form-label"><strong>CodiceEORI</strong></label>
	  		<div class="form-controls">
				<?= $this->Form->input('eori_code',['div' => false, 'label' => false, 'class'=>'form-control','minlength'=>13]); ?>
			</div>
		</div>
	</div>
     <div class="col-md-12"><hr></div>
	<center><?= $this->element('Form/Components/Actions/component',['redirect'=>'index']); ?></center>
	<?= $this->Form->end(); ?>


	  <script>
     $("#CarrierAddForm").on('submit.default',function(ev)
     {
     });

     $("#CarrierAddForm").on('submit.validation',function(ev)
     {
        ev.preventDefault(); // to stop the form from submitting
        checkCarrierAddPivaAndCfDuplicate()
     });
</script>

<script>

function checkCarrierAddPivaAndCfDuplicate()
{
	var errore = 0;

    $.ajax
    ({
    	method: "POST",
    	url: "<?= $this->Html->url(["controller" => "carriers","action" => "checkCarrierPivaDuplicate"]) ?>",
    	data:
    	{
    		piva : $("#CarrierPiva").val(),
    	},
    	success: function(data)
    	{
    		if(data > 0){ errore = 1; }
        	$.ajax
    		({
    			method: "POST",
    			url: "<?= $this->Html->url(["controller" => "carriers","action" => "checkCarrierCfDuplicate"]) ?>",
    			data:
    			{
    				cf : $("#CarrierCf").val(),
    			},
    			success: function(data)
    			{
    				if(data > 0){ errore = 2; }
		            switch (errore)
        		    {
		            	case 0:
                			$("#CarrierAddForm").trigger('submit.default');
    		            break;
	                    case 1:
                            $.alert({
    							icon: 'fa fa-warning',
	    						title: 'Inserimento vettore',
    							content: 'Attenzione esiste già un fornitore con la stessa partita iva.',
    							type: 'orange',
							});
                            $("#CarrierPiva").css("border-color",'1px solid red');
                        return false;
                        case 2:
                            $.alert({
    							icon: 'fa fa-warning',
	    						title: 'Inserimento vettore',
    							content: 'Attenzione esiste già un fornitore con lo stesso codice fiscale.',
    							type: 'orange',
							});
                            $("#CarrierCf").css("border-color",'1px solid red');
                        return false;
                            break;
                        }
    				},
            		error: function(data)
        			{
        			}
    			});
    		}
        })
}
</script>


