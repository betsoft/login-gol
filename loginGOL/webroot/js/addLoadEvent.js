var registeredFunctions = [];

function addLoadEvent(func, name) {
	name = name || null;
	var oldonload = window.onload;
	if (typeof window.onload != 'function') 
		window.onload = func;
	else 
	{
		window.onload = function() 
		{
			if (oldonload) 
				oldonload();
			
			//	console.log(registeredFunctions);
			
			 if(name == null || (!(registeredFunctions.indexOf(name) >= 0))) 
			 {
			 	func();
    			registeredFunctions.push(name);
			 }
			 else
			 {
			 	console.warn("A function called '" + name + "' is already loaded into the page. Prevented to load duplicates.")
			 }
		}
	}
}