
// GLOBALI



function getDateDiff(time1, time2) 
{
  var str1= time1.split('-');
  var str2= time2.split('-');

  // La data è passata come dd-mm-yyyy

  //                yyyy   , mm       , dd
  var t1 = new Date(str1[2], str1[1]-1, str1[0]);
  var t2 = new Date(str2[2], str2[1]-1, str2[0]);

  var diffMS = t1 - t2;    
  //console.log(diffMS + ' ms');

  var diffS = diffMS / 1000;    
  //console.log(diffS + ' ');

  var diffM = diffS / 60;
  //console.log(diffM + ' minutes');

  var diffH = diffM / 60;
  //console.log(diffH + ' hours');

  var diffD = diffH / 24;
  //console.log(diffD + ' days'); 

    return diffD;
  // alert(diffD);
}


// Funzione che aggiorna il valore dell'importo nelle colonne "importo"
function setImporto(row)
{
    var quantity = $(row).parents(".clonableRow").find(".jsQuantity").val();
    var prezzo = $(row).parents(".clonableRow").find(".jsPrice").val();
    var importo = quantity * prezzo;
    $(row).parents(".clonableRow").find(".jsImporto").val(importo.toFixed(2));
}


// Funzione che imposta il valore in valuta dove previsto all'interno di una vista
function setCurrencyChange(row,typeOfChange)
{
    
    if(typeOfChange == 'toCurrency')
    {
        var prezzo = $(row).parents(".clonableRow").find(".jsPrice").val();
        var cambio = $(".jsChangeValue").val();
        var prezzoinvaluta = prezzo * cambio;
        $(row).parents(".clonableRow").find(".jsChange").val(prezzoinvaluta.toFixed(2));
    }
    if(typeOfChange == 'toEuro')
    {
        var prezzo = $(row).parents(".clonableRow").find(".jsChange").val();
        var cambio = $(".jsChangeValue").val();
        var prezzoineuro = prezzo / cambio;
        $(row).parents(".clonableRow").find(".jsPrice").val(prezzoineuro.toFixed(2));
    }
}


// Gestione dei codici
function setCodeRequired(row)
{
    var tipo = $(row).parents(".clonableRow").find(".jsTipo").val();
    if(tipo == 1)
    {
        $(row).parents(".clonableRow").find(".jsCodice").attr('required',true);
        $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').show();
        $(row).parents(".clonableRow").find(".jsMovable").val(1);
    }
    else
    {
        $(row).parents(".clonableRow").find(".jsCodice").removeAttr('required');
        $(row).parents(".clonableRow").find(".jsCodice").parent('div').find('.fa-asterisk').hide();
        $(row).parents(".clonableRow").find(".jsMovable").val(0);
    }
}

// Aggiunge cursore
function addWaitPointer() { 
    $("body").css("cursor", "wait"); 
}
// Rimuove il cursore    
function removeWaitPointer() {
     $("body").css("cursor", "default"); 

}
