<?php
App::uses('AppModel', 'Model');

class Carrier extends AppModel 
{
   
   public $useTable = 'carriers';

	public $belongsTo = 
	[
	    'Nation' => ['className' => 'Nation','foreignKey' => 'nation_id','conditions' => '','fields' => '','order' => ''],
	];

   public $validate = 
	[
		"name"=>
		[
            "rule"=>["isUnique", ["name", "company_id","state"], false], 
            "message"=>"Il campo ragione sociale deve essere univoco." 
        ]
    ];

	public function hide($id)
    {
        return $this->updateAll(['Carrier.state' => 0,'Carrier.company_id'=>MYCOMPANY],['Carrier.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Carrier.id'=>$id, 'Carrier.state' =>0 ]]) != null;
    }
    
    
	
}
