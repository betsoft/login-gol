<?php
App::uses('AppModel', 'Model');

class Paypal extends AppModel 
{
	
	public $useTable = false;

    public function login()
    {

        require_once ROOT . DS . APP_DIR . DS . '/Vendor/braintree/lib/Braintree.php';
        
        // Non devo disegnare niente
		$this->autoRender = false;
        
        $this->Setting = ClassRegistry::init('Setting');
        $this->Bill = ClassRegistry::init('Bill');
		
		$setting = $this->Setting->GetMySettings();
	
	    $LIVE = true;

	    if($LIVE == false)
	    {
            // daniele.ronco@noratech.it
            //$paypalUser = "AeApfhuHAr7Zaor5---mvMs95U-Tl6MTCZBJXnVZtyieazaQ45yFr33BTZWUCTgJXIJ6h1zbSRWuR2v3";
            //$paypalPassword = "EP1YhdV0vQ8vlcPlKLXLaCjgTIqH1fbWdvF_4t0kb8Nki6YWxza6OmpZCx8tndoF86tb9JzOH8CLu7XC";
            
            // info@nroatech.it
            // $paypalUser = "AeApfhuHAr7Zaor5---mvMs95U-Tl6MTCZBJXnVZtyieazaQ45yFr33BTZWUCTgJXIJ6h1zbSRWuR2v3";
            // $paypalPassword = "EP1YhdV0vQ8vlcPlKLXLaCjgTIqH1fbWdvF_4t0kb8Nki6YWxza6OmpZCx8tndoF86tb9JzOH8CLu7XC";
	        // $endPoint = "sandbox.";
	    }
	    else
	    {
	       // Live
	       
	       // daniele.ronco@noratech.it
	       $paypalUser = "AVrSJ-zjQjqXNwQcAtogKoNanIaawKWhYchPh5wiGcqkK0ayQckMPJyJnx6XRXebmZMYwIlTfh94kykb";
           $paypalPassword = "EPdZhFz_-v4GsOyxqtLmiJbWAU0OpRgIMa9xwDRjcgpnIfaFNzstrGx79SorXxEyYtmqDSk52D3KE5en";
           
           // info@nroatech.it
           // $paypalUser = "AXsIyk57YAJxZbHvC4wNRi2eYt9ZKhPDcSh81qcdQAT2UwU1rlYQf_KfbQvI3PxVqBxb3JBQHyLAFZx2";
          // $paypalPassword = "ENEQxhYV0v3K2w9cOOwKeSikBjDIttEJwm15ennN80RkO5rEmPzBhP4oFiH3wvUUeZk8aCPfDYuFifCY";
	          $endPoint = "";
	    }
	    
	    $authorizationCode = base64_encode($paypalUser . ':' . $paypalPassword);

        // Chiamata paypal per recupero token autenticazione
		$curl = curl_init();
    	curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/oauth2/token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic $authorizationCode",
            "Content-Type: application/x-www-form-urlencoded",
            "cache-control: no-cache"
          ),
        ));
		
		$response = curl_exec($curl);
		
		$err = curl_error($curl);
		curl_close($curl);
		
		$obj = json_decode($response);
		
		$accessToken = $obj->{'access_token'};
        $appId = $obj->{'app_id'};
        
        /********************* START  RECUPERATO PERMESSI MA SULLA MIA APP *******************/
        
        if(false)
    {
        if ($err) 
        {
          echo "cURL Error #:" . $err;
        }
        else 
        {
            
            // PAYPAL
            
             // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            $curlGetPermission = curl_init();
            
            curl_setopt($curlGetPermission, CURLOPT_URL, "https://svcs.".$endPoint."paypal.com/Permissions/RequestPermissions");
            curl_setopt($curlGetPermission, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlGetPermission, CURLOPT_POSTFIELDS, "{\"scope\":\"TRANSACTION_SEARCH\",\"callback\":\"http://www.noratech.it\", \"requestEnvelope\": { \"errorLanguage\":\"en_EN\" }}");
            curl_setopt($curlGetPermission, CURLOPT_POST, 1);
            
            $headers = array();
          
            $usingInfo =  false;
          
            
          
            if($usingInfo)
            {
               $headers[] = "X-Paypal-Security-Userid: info_api1.noratech.it";
               $headers[] = "X-Paypal-Security-Password: 3BHXQ4S2TMEVWGUY";
               $headers[] = "X-Paypal-Security-Signature: A-0YtcaNAy6Fsipvn88kJ16OJiqDA-a0M2DklRRccWkr.kMJQ4l9XWgy";
            }
            else
            {
               $headers[] = "X-Paypal-Security-Userid: daniele.ronco_api1.noratech.it";
               $headers[] = "X-Paypal-Security-Password: BN9LN5SXLA8S3BPN";
               $headers[] = "X-Paypal-Security-Signature: ATETkWjIPkYaf5acVxFYRZHtl-jwAdXzvVmbgowEtms6bCz2aUHPZSWm";
            }
            
            $headers[] = "X-Paypal-Request-Data-Format: JSON";
            $headers[] = "X-Paypal-Response-Data-Format: JSON";
            $headers[] = "X-Paypal-Application-Id: $appId";
            $headers[] = "Content-Type: application/x-www-form-urlencoded";
            curl_setopt($curlGetPermission, CURLOPT_HTTPHEADER, $headers);
            
            $result = curl_exec($curlGetPermission);

            if (curl_errno($curlGetPermission)) {
                echo 'Error:' . curl_error($curlGetPermission);
            }
            
            curl_close ($curlGetPermission);
        }
    }

/********************* FINE  RECUPERATO PERMESSI MA SULLA MIA APP *******************/

        // Rcupero lista pagamenti             
            $curl = curl_init();
                
             // Chiamata api paypal che richiede però permission    
             curl_setopt_array($curl, array(
               //   CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/reporting/transactions?start_date=2017-08-01T11:00:00Z&end_date=2017-08-11T16:00:00Z&count=100",
                  CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/reporting/transactions?start_date=2018-11-01T11:00:00Z&end_date=2018-11-30T16:00:00Z&count=100",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_POSTFIELDS => "undefined=",
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $accessToken",
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                  ),
                ));    
                
                
             $responseRecoverPayment = curl_exec($curl);
             $errRecoverPayement = curl_error($curl);
                
                curl_close($curl);
                
                if ($errRecoverPayement) {
                  echo "cURL Error #:" . $errRecoverPayement;
                } 
                else 
                {
                    $transactionResult = json_decode($responseRecoverPayment);         
                }
            
            foreach($transactionResult->{'transaction_details'} as $transaction)
            {
                
                $transactionTemp = $transaction->{'transaction_info'};
                
                $transaction = $transaction->{'transaction_info'}->{'transaction_id'};

                if(!$this->Bill->existsPaypalTransactionId($transaction))
                {
                    
                    // Chiamo la singola transazione
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://api-3t.paypal.com/nvp");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                   // $auth = "USER=info_api1.noratech.it&PWD=3BHXQ4S2TMEVWGUY&SIGNATURE=A-0YtcaNAy6Fsipvn88kJ16OJiqDA-a0M2DklRRccWkr.kMJQ4l9XWgy&METHOD=GetTransactionDetails&VERSION=78&TransactionID=$transaction";
                   $auth = "USER=daniele.ronco_api1.noratech.it&PWD=BN9LN5SXLA8S3BPN&SIGNATURE=ATETkWjIPkYaf5acVxFYRZHtl-jwAdXzvVmbgowEtms6bCz2aUHPZSWm&METHOD=GetTransactionDetails&VERSION=78&TransactionID=$transaction";
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $auth);
                    curl_setopt($ch, CURLOPT_POST, 1);
                            
                    $headers = array();
                    $headers[] = "Content-Type: application/x-www-form-urlencoded";

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            
                    $result = curl_exec($ch);

                    if (curl_errno($ch)) 
                    {
                        echo 'Error:' . curl_error($ch);
                    }
                    else
                    {
                        parse_str($result,$newResult);
                        
                        debug($newResult);
                        $setting['Setting']['paypal_account'] = 'daniele.ronco@noratech.it';
                        if($newResult['RECEIVEREMAIL'] == $setting['Setting']['paypal_account'])
                        {
                        
                            $receipt['transaction_id'] = $transaction;

                            $receipt['ragionesociale'] = $newResult['FIRSTNAME'] . ' ' .  $newResult['LASTNAME'];
                            $receipt['receiptdate'] = date('Y-m-d',strtotime($transactionTemp->{'transaction_updated_date'}));
                            // Recupero valori transazione e iva
                            $receipt['amount'] = $transactionTemp->{'transaction_amount'}->{'value'}; // $newResult['transaction_amount']->{'value'};
                            $receipt['isocode'] = $transactionTemp->{'transaction_amount'}->{'currency_code'};
                            if(isset($transactionTemp->{'fee_amount'}))
                            {
                                $receipt['vat'] = $transactionTemp->{'fee_amount'}->{'value'};
                            }
                            $i = 0;
                            do 
                            {
                                if(isset($newResult['L_NAME'.$i]))
                                {
                                    $receipt['Details'][$i]['Description'] = $newResult['L_NAME'.$i];
                                    $receipt['Details'][$i]['Quantity'] = $newResult['L_QTY'.$i];
                                    $receipt['Details'][$i]['Handling'] = $newResult['L_HANDLINGAMT'.$i];
                                    $receipt['Details'][$i]['Shipping'] = $newResult['L_SHIPPINGAMT'.$i];
                                    $receipt['Details'][$i]['Vat'] = $newResult['L_TAXAMT'.$i];
                                    $receipt['Details'][$i]['Amt'] = $newResult['L_AMT'.$i];
                                    $receipt['Details'][$i]['currency'] = $newResult['L_CURRENCYCODE'.$i];
                                }
                                else
                                {   
                                    break;
                                }
                                $i++;
                            } while (true);
                            
                            $this->Bill->createReceiptFromPaypal($receipt);
                        }
                        else
                        {
                            debug("E' una transazione in uscita che non considero");
                        }
                    }
                    curl_close($ch);
                }
                else
                {
                    debug("Questa transazione paypal è già stata importata - ".$transaction);
                }
                
                /***************************** PROVO A CHIAMARE SINGOLE TRANSAZIONE FINE ****************************************/
            }
            
        }
    }


