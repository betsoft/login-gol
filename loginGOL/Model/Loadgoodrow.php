<?php
App::uses('AppModel', 'Model');

class Loadgoodrow extends AppModel {

public $useTable = 'load_goods_rows';


	public $belongsTo = 
	[ 
		'Iva' => ['className' => 'Iva','foreignKey' => 'load_good_row_vat_id','conditions' => '','fields' => '','order' => ''],
		'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => '']
	];
	
	
	public $hasMany = 
	[
	//	'Loadgood' => ['className' => 'Loadgood','foreignKey' => 'load_good_id','conditions' => '','fields' => '','order' => '']
	];

}
