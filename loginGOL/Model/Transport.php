<?php
App::uses('AppModel', 'Model');

class Transport extends AppModel {

	public $useTable = 'transports';

	public $belongsTo = 
	[
		'Client' => ['className' => 'Client','foreignKey' => 'client_id','conditions' => '','fields' => '','order' => ''],
		'Supplier' => ['className' => 'Supplier','foreignKey' => 'supplier_id','conditions' => '','fields' => '','order' => ''],
		'Carrier' => ['className' => 'Carrier','foreignKey' => 'carrier_id','conditions' => '','fields' => '','order' => ''],
		'Quote' => ['className' => 'Quote','foreignKey' => 'quote_id','conditions' => '','fields' => '','order' => ''],
		'Bill' => ['className' => 'Bill','foreignKey' => 'bill_id','conditions' => '','fields' => '','order' => ''],
		'Causal' => ['className' => 'Causal','foreignKey' => 'causal_id','conditions' => '','fields' => '','order' => ''],
		'Deposit' => ['className' => 'Deposit','foreignKey' => 'deposit_id','conditions' => '','fields' => '','order' => ''],
		'Payment' => ['className' => 'Payments','foreignKey' => 'payment_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => '']
	];
	
	public $hasMany = [
		'Transportgood' => ['className' => 'Transportgood','foreignKey' => 'transport_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
	];

	public function hide($id)
    {
        return $this->updateAll(['Transport.state' => 0,'Transport.company_id'=>MYCOMPANY],['Transport.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Transport.id'=>$id, 'Transport.state' =>0 ]]) != null;
    }
	
	
	public function getTransportFromId($transportId)
	{
		return $this->find('first',['conditions'=>['Transport.id'=>$transportId, 'Transport.state'=>ATTIVO]]);
	}
	
}