<?php
App::uses('AppModel', 'Model');

class Causal extends AppModel 
{
	public $useTable = 'causals';

    public $validate = 
	[
		"description"=>
		[
            "rule"=>["isUnique", ["name", "company_id","state"], false], 
            "message"=>"Il campo causale deve essere univoco" 
        ]
    ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ,'company_id'=>MYCOMPANY]]) != null;
    }
    
    public function getCausalType($causal_id)
    {
        $causal = $this->find('first',['conditions'=>['id'=>$causal_id ,'company_id'=>MYCOMPANY]]);
        if($causal != null)
        {
            return $this->find('first',['conditions'=>['id'=>$causal_id ,'company_id'=>MYCOMPANY]])['Causal']['clfo'];
        }
        return null;
    }

}
