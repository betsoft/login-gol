<?php
App::uses('AppModel', 'Model');

class Purchaseorderrow extends AppModel
{
    public $useTable = 'purchaseorders_goods_rows';

     public $belongsTo =
        [
            'Iva' => ['className' => 'Iva','foreignKey' => 'vat_id','conditions' => '','fields' => '','order' => ''],
            'Units' => ['className' => 'Units','foreignKey' => 'unit_of_measure_id','conditions' => '','fields' => '','order' => ''],
            'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => ''],
        ];

/*    public $hasMany =
        [
            'Purcaseorderrow' => ['className' => 'Purcaseorderrow', 'foreignKey' => 'purachseorder_id', 'conditions' => '', 'fields' => '', 'order' => '']
        ];*/

    public function hide($id)
    {
        return $this->updateAll(['state' => 0, 'company_id' => MYCOMPANY], ['Purchaseorderrow.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Purchaseorderrow.id' => $id, 'Purchaseorderrow.state' => 0]]) != null;
    }
}
