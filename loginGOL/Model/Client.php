<?php

App::uses('AppModel', 'Model');

class Client extends AppModel
{
    public $useTable = 'clients';

    public $hasMany = [
        'Bill' => ['className' => 'Bill', 'foreignKey' => 'client_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Tranports' => ['className' => 'Transports', 'foreignKey' => 'client_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Quote' => ['className' => 'Quote', 'foreignKey' => 'client_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Catalog' => ['className' => 'Catalog', 'foreignKey' => 'client_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Clientdestination' => ['className' => 'Clientdestination', 'foreignKey' => 'client_id', 'dependent' => false, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
    ];

    public $belongsTo = [
        'Bank' => ['className' => 'Bank', 'foreignKey' => 'bank_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Nation' => ['className' => 'Nation', 'foreignKey' => 'nation_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $validate = [
        "ragionesociale" => [
            "rule" => ["isUnique", ["ragionesociale", "cf", "company_id", "state"], false],
            "message" => "Un codice fiscale può essere associato solo a una ragione sociale."
        ],
        "pec" => [
            "rule" => ["isUnique", ["pec", "company_id", "state"], false],
            "message" => "Il campo pec deve essere univoco."
        ],
    ];

    public function hide($id)
    {
        return $this->updateAll(['Client.state' => 0, 'Client.company_id' => MYCOMPANY], ['Client.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Client.id' => $id, 'Client.state' => 0]]) != null;
    }

    public function getList()
    {
        $this->Clients = ClassRegistry::init('Clients');

        return $this->Clients->find('list', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO], 'fields' => ['Clients.ragionesociale'], 'order' => ['Clients.ragionesociale' => 'asc']]);
    }

    public function getAdvancedList()
    {
        $this->Clients = ClassRegistry::init('Clients');

        $clients = $this->Clients->find('all', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO], 'fields' => ['Clients.id', 'Clients.code', 'Clients.ragionesociale'], 'order' => ['Clients.ragionesociale' => 'asc']]);

        $arrayClients = [];

        foreach($clients as $client)
        {
            $arrayClients[$client['Clients']['id']] = $client['Clients']['code'].' : '.$client['Clients']['ragionesociale'];
        }

        return $arrayClients;
    }



    public function getCompleteList()
    {
        $this->Clients = ClassRegistry::init('Clients');
        return $this->Clients->find('list', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state >' => 0], 'fields' => ['Clients.ragionesociale'], 'order' => ['Clients.ragionesociale' => 'asc']]);
    }

    public function getListAndSelected($clientId)
    {
        $currentClient = $this->getCompleteList();
        $closedclient = $this->find('list', ['fields' => ['id', 'ragionesociale'], 'conditions' => ['company_id' => MYCOMPANY, 'id' => $clientId]]);
        return $currentClient + $closedclient;
    }

    public function getConstructionsites($clientId)
    {
        $this->Constructionsite = ClassRegistry::init('Constructionsite');
        return json_encode($this->Constructionsite->getListByClientId($clientId));
    }

    public function getQuotes($clientId)
    {
        $this->Quote = ClassRegistry::init('Quote');
        return json_encode($this->Quote->getListByClientId($clientId));
    }

    public function enable()
    {
        $this->changeState($_POST['clientId'], 1);
    }

    public function changeState($clientId, $state)
    {
        $this->updateAll(['Client.state' => $state], ['Client.id' => $clientId, 'Client.state' => 2, 'Client.company_id' => MYCOMPANY]);
    }
}
