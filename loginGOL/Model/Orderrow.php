<?php
App::uses('AppModel', 'Model');

class Orderrow extends AppModel {

    public $useTable = 'order_rows';


    public $belongsTo = [
        'Order'=>['className'=>'Order', 'foreignKey'=>'order_id', 'conditions'=>'', 'fields'=>'', 'order'=>''],
        'Iva' => ['className' => 'Iva','foreignKey' => 'iva_id','conditions' => '','fields' => '','order' => ''],
        'Units' => ['className' => 'Units','foreignKey' => 'unita','conditions' => '','fields' => '','order' => ''],
        'Storage' => ['className' => 'Storage','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => ''],

    ];

}
?>
