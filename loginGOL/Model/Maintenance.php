<?php
App::uses('AppModel', 'Model');

class Maintenance extends AppModel
{
    public $useTable = 'maintenances';

    public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Constructionsite' => ['className' => 'Constructionsite', 'foreignKey' => 'constructionsite_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Quote' => ['className' => 'Quote', 'foreignKey' => 'quote_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $hasMany = [
        'Maintenancerow' => ['className' => 'Maintenancerow', 'foreignKey' => 'maintenance_id', 'conditions' => 'state = 1', 'fields' => '', 'order' => ''],
        'Maintenanceddt' => ['className' => 'Maintenanceddt', 'foreignKey' => 'maintenance_id', 'conditions' => 'state = 1', 'fields' => '', 'order' => ''],
        'Maintenancehour' => ['className' => 'Maintenancehour', 'foreignKey' => 'maintenance_id', 'conditions' => 'state = 1', 'fields' => '', 'order' => ''],
    ];

    // Resituisce il successivo numero della scheda intervento
    function getNextMaintenanceNumber($year)
    {
        $this->Utilities = ClassRegistry::init('Utilities');
        $fields = ['MAX(maintenance_number) as currentMaintenanceNumber'];
        $conditionArray = ['Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => ATTIVO, 'Maintenance.maintenance_date >= ' => $this->Utilities->getFirstDayOfYear($year), 'Maintenance.maintenance_date <= ' => $this->Utilities->getLastDayOfYear($year)];
        $nextMaintenanceNumber = $this->find('first', ['fields' => $fields, 'conditions' => $conditionArray, 'order' => 'Maintenance.id desc']);

        if ($nextMaintenanceNumber[0]['currentMaintenanceNumber'] == null)
            return 1;
        else
            return $nextMaintenanceNumber[0]['currentMaintenanceNumber'] + 1;
    }

    public function hide($id)
    {
        return $this->updateAll(['Maintenance.state' => 0, 'Maintenance.company_id' => MYCOMPANY], ['Maintenance.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Maintenance.id' => $id, 'Maintenance.state' => 0]]) != null;
    }

    // Controlla che la scheda intervento non abbia già lo stesso numero
    function checkDuplicate($maintenanceNumber, $date)
    {
        $this->Maintenance = ClassRegistry::init('Maintenance');
        $MaintenanceYear = date("Y", strtotime($date));
        $conditionArray = ['maintenance_number' => $maintenanceNumber, 'maintenance_date >=' => $MaintenanceYear . '-01-01', 'maintenance_date <=' => $MaintenanceYear . '-12-31', 'Maintenance.state' => 1, 'Maintenance.company_id' => MYCOMPANY];
        $maintenanceNumber = $this->Maintenance->find('count', ['conditions' => $conditionArray]);
        return $maintenanceNumber;
    }

    public function setSent($id)
    {
        $this->updateAll(['Maintenance.sent' => 1], ['Maintenance.id' => $id, 'Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => ATTIVO]);
    }
}