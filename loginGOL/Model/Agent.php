<?php
App::uses('AuthComponent', 'Controller/Component');

class Agent extends AppModel
{

    public $useTable = 'agents';

    public $validate =
        [
            'name' => ['required' => ['rule' => ['notBlank'], 'message' => 'Il nome è obbligatorio']],
            'surname' => ['required' => ['rule' => ['notBlank'], 'message' => 'Il cognome è obbligatorio']]
        ];

    public $belongsTo = [
        'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],

        ];

    public function isHidden($id)
    {
        return $this->find('first',['conditions' => ['Agent.id' => $id, 'Agent.state' => 0]]) != null;
    }

    public function hide($id)
    {
        return $this->updateAll(['Agent.state' => 0,'Agent.company_id' => MYCOMPANY],['Agent.id' => $id]);
    }
}
?>
