<?php
App::uses('AppModel', 'Model');

class Technician extends AppModel
{
	public $useTable = 'technicians';

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id' => MYCOMPANY],['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions' => ['id' => $id, 'state' => 0]]) != null;
    }

    public $virtualFields = ['surnameandname' => 'CONCAT(Technician.surname, " " ,Technician.name)'];

    public function getList()
    {
        return $this->find('list', ['fields' => ['Technician.id', 'surnameandname'], 'order' => ['Technician.surname' => 'asc']]);
    }

    public function getTechnicianIdFromUserId($userId)
    {
        return $this->find('first',['conditions' => ['user_id' => $userId]])['Technician']['id'];
    }

    public function getSurnameAndName($technicianId)
    {
        return $this->find('first',['conditions' => ['company_id' => MYCOMPANY ,'state' => 1 ,'id' => $technicianId]])['Technician']['surnameandname'];
    }

    public function getWorkedHour($tecnichianId, $conditionsArray)
    {
        $this->Maintenance = ClassRegistry::init('Maintenance');
        $this->Maintenancehour = ClassRegistry::init('Maintenancehour');
        $this->Maintenancehourstechnician = ClassRegistry::init('Maintenancehourstechnician');

        $maintenances = $this->Maintenance->find('all', ['contain' => ['Constructionsite', 'Maintenancehour'=>['Maintenancehourstechnician']], 'conditions' => $conditionsArray, 'order' => ['Maintenance.maintenance_date' => 'desc', 'Maintenance.maintenance_number' => 'desc']]);

        $arrayMaintenances = [];
        foreach($maintenances as $maintenance)
        {
            $exist = false;

            $conditionsMaintenanceHours = ['Maintenancehour.maintenance_id' => $maintenance['Maintenance']['id'], 'Maintenancehour.state' => 1];
            $maintenanceHours = $this->Maintenancehour->find('all', ['conditions' => $conditionsMaintenanceHours]);

            foreach($maintenanceHours as $maintenanceHour)
            {
                $conditionsMaintenancehourstechnician = ['Maintenancehourstechnician.maintenance_hour_id' => $maintenanceHour['Maintenancehour']['id'], 'Maintenancehourstechnician.technician_id' => $tecnichianId, 'Maintenancehourstechnician.state' => 1];
                $maintenanceHoursTechnician = $this->Maintenancehourstechnician->find('all', ['conditions' => $conditionsMaintenancehourstechnician]);

                if(count($maintenanceHoursTechnician) > 0)
                    $exist = true;
            }

            if($exist)
                array_push($arrayMaintenances, $maintenance);
        }

        return $arrayMaintenances;
    }
}