<?php
App::uses('AppModel', 'Model');

class Supplierdestination extends AppModel {

	public $useTable = 'supplier_destinations';

	public $belongsTo = [
		'Nation' => ['className' => 'Nation','foreignKey' => 'nation_id','conditions' => '','fields' => '','order' => ''],
	];
	
	public function hide($id)
    {
        return $this->updateAll(['Supplierdestination.state' => 0,'Supplierdestination.company_id'=>MYCOMPANY],['Supplierdestination.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Supplierdestination.id'=>$id, 'Supplierdestination.state' =>0 ]]) != null;
    }

}
