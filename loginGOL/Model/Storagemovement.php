<?php
App::uses('AppModel', 'Model');
/**
 * Bill Model
 *
 * @property Client $Client
 * @property Iva $Iva
 */
class Storagemovement extends AppModel 
{

	public $useTable = 'storage_movements';

	public $belongsTo =
        [
            'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        ];
}
