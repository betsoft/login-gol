<?php
App::uses('AppModel', 'Model');

class Outsideoperator extends AppModel
{
    public $useTable = 'outsideoperators';

    public function hide($id)
    {
        return $this->updateAll(['state' => 0], ['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['id' => $id, 'state' => 0]]) != null;
    }

    public $virtualFields = ['surnameandname' => 'CONCAT(Outsideoperator.surname, " " , Outsideoperator.name)'];

    public function getList()
    {
        return $this->find('list', ['conditions'=>['state' => 1], 'fields' => ['surnameandname'], 'order' => ['Outsideoperator.surname' => 'asc']]);
    }

    public function getSurnameAndName($outsideoperatorId)
    {
        return $this->find('first', ['conditions' => ['state' => 1, 'id' => $outsideoperatorId]])['Outsideoperator']['surnameandname'];
    }

    public function getWorkedHour($operatorId,$conditionsArray = null)
    {
        $this->Maintenancehoursoutsideoperator = ClassRegistry::init('Maintenancehoursoutsideoperator');
        if($conditionsArray == null) {
            $conditionsArray = ['Maintenancehoursoutsideoperator.operator_id' => $operatorId, 'Maintenancehoursoutsideoperator.company_id' => MYCOMPANY, 'Maintenancehoursoutsideoperator.state' => 1];
        }

        $Maintenancehoursoutsideoperator = $this->Maintenancehoursoutsideoperator->find('all',['contain' => ['Maintenancehour' => ['Maintenance' => 'Constructionsite', 'Maintenancehoursoutsideoperator'], 'Outsideoperator'], 'conditions'=> $conditionsArray,'recursive'=>2,'order'=>['Maintenancehoursoutsideoperator.id desc']]);
        return $Maintenancehoursoutsideoperator;

        //return $this->Maintenancehoursoutsideoperator->find('all',['conditions'=> $conditionsArray,'recursive'=>2,'order'=>['Maintenancehoursoutsideoperator.id desc']]);
    }

}
