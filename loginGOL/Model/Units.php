<?php
App::uses('AppModel', 'Model');

class Units extends AppModel 
{
	public $useTable = 'units';
	
	public $validate = 
	[
		"description"=>
		[
            "rule"=>["isUnique", ["description", "company_id","state"], false], 
            "message"=>"Il campo descrizione deve essere univoco." 
        ]
    ];
	
	public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }

    public function getList()
    {
        return $this->find('list', ['fields' => ['Units.id', 'description'], 'order' => ['Units.description' => 'asc']]);
    }


    public function getName($unitId)
    {
        return $this->find('first',['fields'=>['Units.description'],['conditions'=>['company_id'=>MYCOMPANY,'id'=>$unitId]]])['Units']['description'];
    }


}
