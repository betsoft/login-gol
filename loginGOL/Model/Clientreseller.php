<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property Bill $Bill
 * @property Quote $Quote
 * 
 */
class Clientreseller extends AppModel {

	public $useTable = 'clients_resellers';

	public $belongsTo = 
	[
		'Client' => ['className' => 'Client','foreignKey' => 'client_id','conditions' => '','fields' => '','order' => '']
	];

	public function hide($id)
    {
        return $this->updateAll(['Clientreseller.state' => 0,'Clientreseller.company_id'=>MYCOMPANY],['Clientreseller.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Clientreseller.id'=>$id, 'Clientreseller.state' =>0 ]]) != null;
    }

}
