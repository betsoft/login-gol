<?php
App::uses('AppModel', 'Model');

class Constructionsite extends AppModel
{
    public $useTable = 'constructionsites';

    public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $validate = [
        "name" => [
            "rule" => ["isUnique", ["name", "company_id", "state"], false],
            "message" => "Il campo nome cantiere deve essere univoco."
        ],
    ];

    public function hide($id)
    {
        return $this->updateAll(['Constructionsite.state' => 0, 'Constructionsite.company_id' => MYCOMPANY], ['Constructionsite.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Constructionsite.id' => $id, 'Constructionsite.state' => 0]]) != null;
    }

    public function getList()
    {
        return $this->find('list', ['fields' => ['id', 'name'], 'conditions' => ['company_id' => MYCOMPANY, 'state IN' => [1,3]]]);
    }

    public function getListByClientId($clientId)
    {
		if ($clientId != 'all')
			return $this->find('list', ['fields' => ['id', 'name'], 'conditions' => ['company_id' => MYCOMPANY, 'state IN' => [1,3],'client_id'=>$clientId]]);
        else
            return $this->find('list', ['fields' => ['id', 'name'], 'conditions' => ['company_id' => MYCOMPANY, 'state IN' => [1, 3]]]);
	}

    public function getClient($constructionsiteId)
    {
        $conditionArray = ['Constructionsite.id' => $constructionsiteId, 'Constructionsite.company_id' => MYCOMPANY, 'Constructionsite.state IN' => [1,3]];
        $construcitonSites = $this->find('first', ['conditions' => $conditionArray,'field'=>'client_id']);

        if(isset($construcitonSites['Constructionsite']['client_id']))
            return $construcitonSites['Constructionsite']['client_id'];
        else
            return -1 ;
    }

    public function getListAndSelected($constructionsiteId, $clientId)
    {
        $constructionSites = $this->find('list', ['fields' => ['id', 'name'], 'conditions' => ['company_id' => MYCOMPANY, 'client_id' => $clientId, 'state IN' => [1,3]]]);
        return $constructionSites;
    }

    public function getAddress($constructionsiteId)
    {
        $conditionArray = ['Constructionsite.id' => $constructionsiteId, 'Constructionsite.company_id' => MYCOMPANY, 'Constructionsite.state' => ATTIVO];
        $construcitonSites = $this->find('first', ['conditions' => $conditionArray]);
        $arrayReturn = [];

        if ($construcitonSites != null)
        {
            $arrayReturn[] = [
                'address' => $construcitonSites['Constructionsite']['address'],
                'cap' => $construcitonSites['Constructionsite']['cap'],
                'city' => $construcitonSites['Constructionsite']['city'],
                'province' => $construcitonSites['Constructionsite']['province'],
                'nation' => $construcitonSites['Constructionsite']['nation_id'],
            ];
        }
        else
        {
            $arrayReturn[] = ['name' => '', 'address' => '', 'cap' => '', 'city' => '', 'province' => '', 'nation_id' => 0,];
        }

        return $arrayReturn;
    }

    public function open($id)
    {
        return $this->updateAll(['Constructionsite.state' => 1, 'Constructionsite.company_id' => MYCOMPANY], ['Constructionsite.id' => $id]);
    }

    public function close($id)
    {
        return $this->updateAll(['Constructionsite.state' => 2, 'Constructionsite.company_id' => MYCOMPANY], ['Constructionsite.id' => $id]);
    }

    public function getConstructionsiteMaintenances($construciontSiteId)
    {
        $this->Maintenance = ClassRegistry::init('Maintenance');
        $conditionArray = ['Maintenance.company_id' => MYCOMPANY, 'Maintenance.state' => ATTIVO, 'Maintenance.constructionsite_id' => $construciontSiteId];
        return $this->Maintenance->find('all', ['conditions' => $conditionArray,'contain'=>['Client','Constructionsite','Quote', 'Maintenancerow', 'Maintenancehour'=>['Maintenancehourstechnician'=>['Technician'], 'Maintenancehoursoutsideoperator'=>['Outsideoperator']], 'Maintenanceddt' => ['Supplier']]]);
    }

    public function getConstructionsiteQuotes($construciontSiteId)
    {
        $this->Quote = ClassRegistry::init('Quote');
        $conditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => ATTIVO, 'Quote.constructionsite_id' => $construciontSiteId, 'Quote.has_child' => 0];
        return $this->Quote->find('all', ['conditions' => $conditionArray]);
    }

    public function enable()
    {
        $this->changeState($_POST['constructionsiteId'], 1);
    }

    public function changeState($clientId, $state)
    {
        $this->updateAll(['Constructionsite.state' => $state], ['Constructionsite.id' => $clientId, 'Constructionsite.state' => 3, 'Constructionsite.company_id' => MYCOMPANY]);
    }

    public function getName($constructionSiteId)
    {
        return $this->find('first',['conditions'=>['Constructionsite.company_id'=>MYCOMPANY,'Constructionsite.id'=>$constructionSiteId]])['Constructionsite']['name'];
    }

	public function getClientId($constructionSiteId)
    {
       return $this->find('first', ['conditions' => ['Constructionsite.company_id' => MYCOMPANY, 'Constructionsite.id' => $constructionSiteId]])['Constructionsite']['client_id'];
    }
}
