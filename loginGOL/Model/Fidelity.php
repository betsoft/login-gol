<?php
App::uses('AppModel', 'Model');
class Fidelity extends AppModel
{
    public $useTable = 'fidelities';

    public $belongsTo =
        [
            'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions'>= '', 'fields'=> '', 'order'=>''],
        ];

    public function hide($id)
    {
        return $this->updateAll(['Fidelity.state' => 0],['Fidelity.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Fidelity.id'=>$id, 'Fidelity.state' =>0 ]]) != null;
    }
}

