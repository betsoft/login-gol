<?php
App::uses('AppModel', 'Model');

class Setting extends AppModel 
{
    public $belongsTo = 
	[
    	'Einvoicewithholdingtaxcausal' => ['className' => 'Einvoicewithholdingtaxcausal','foreignKey' => 'withholding_tax_causal_id','conditions' => '','fields' => '','order' => ''],
        'Einvoicetypeofperson' => ['className' => 'Einvoicetypeofperson','foreignKey' => 'type_of_person','conditions' => '','fields' => '','order' => ''],
    ];
    
    
    public function GetMySettings($companyId = MYCOMPANY)
    {
        
        return $this->find('first',['conditions'=>['Setting.company_id'=>$companyId]]);
    }

    // Resttuisce la pec dell'azienda 
    public function GetMyPEC()
    {
        return $this->GetMySettings()['Setting']['pec'];
    }
    
    // Resttuisce la pec dell'azienda
    public function GetMyEbillCode()
    {
        return $this->GetMySettings()['Setting']['recipient_code'];
    }
    
    public function getCompanyLogo($companyId = MYCOMPANY)
    {
        return $this->find('first',['conditions'=>['Setting.company_id'=>$companyId], 'fields' => ['Setting.header']]);
    }
    
    public function getCompanyWithholdingTax($companyId = MYCOMPANY)
    {
        return $this->find('first',['conditions'=>['Setting.company_id'=>$companyId], 'fields' => ['Setting.withholding_tax']])['Setting']['withholding_tax'];
    }
}
