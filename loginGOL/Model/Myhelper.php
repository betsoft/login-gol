<?php
App::uses('AppModel', 'Model');

class Myhelper extends AppModel 
{
    
    function getMessage($messageCode)
    {
        switch ($messageCode)
        {
            /** Magazzino - Nuova variante articolo **/
            case 'variationUnique' : $message = "Selezionare \"sì\" se la quantità massima di questa variante è 1. All'atto della creazione della variante verrà effettuato un carico a magazzino unitario nel deposito di principale. Tale variante non apparirà all'interno di \"entrata merce\"."; break;
            /** Creazione fatture da bolla  **/
            case 'selectMultipleClientInBill' : $message = 'Selezionare i clienti per cui si vuole creare le fatture nel periodo selezionato. Per selezionare più clienti tenere premuto il pulsante "ctrl" prima di cliccare sopra il nome del cliente selezionato. '; break;
            /** Fatturazione elettronica **/
            case 'clientBillDelay': $message = 'Se attivato le scadenze delle fatture del cliente al 31/08 verranno posticipate al 10/09 e quelle al 31/12 verranno posticipate al 10/01 dell\'anno successivo'; break;
            case 'clientSplitPayment': $message = 'Se attivato le fatture del cliente verranno create di default con l\'opzione "split payment" attivata '; break;
            case 'electronicInvoiceClientCode': $message = 'Se il cliente è una pubblica amministrazione il campo deve contenere il codice di 6 caratteri, presente su IndicePA tra le informazioni relative al servizio di fatturazione elettronica, associato, all\'ufficio che, all\'interno dell\'amministrazione destinataria, svolge la funzione di ricezione della fattura. In alternativa è possibile valorizzare il campo con il codice Ufficio "centrale" o con il valore di default "999999"<br \><br \>Se il cliente è un soggetto privato, il campo deve contenere il codice di 7 caratteri che il Sistema di Interscambio ha attribuito a chi, in qualità di titolare di un canale di trasmissione diverso dalla PEC abilitato a ricevere fatture elettroniche, ne abbia fatto richiesta.<br \><br \>Se il cliente è un soggetto che intende ricevere le fatture elettroniche attraverso il canale PEC, il campo deve essere valorizzato con sette zeri "0000000" ed essere compilato il campo pec.<br \><br \>Se il cliente è un cliente estero il codice destinatario deve essere valorizzato con sette X "XXXXXXX"'; break;
            /** Metodo di pagamento **/
            case 'generateRiba': $message = 'Se il valore selezionato è "Sì", le fatture con questo metodo di pagamento verranno visualizzate nella sezione "Gerazione flusso riba".';break;
            default: $message = ""; break;
        }
        return '<div style="text-align:justify;">'.$message.'</div>';
    }
    
    
}
