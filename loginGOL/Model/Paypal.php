<?php
App::uses('AppModel', 'Model');

class Paypal extends AppModel 
{
	
	public $useTable = false;

    public function login()
    {

        $startDate = date('Y-m-d',strtotime($_POST['dateFrom']));
        $endDate =  date('Y-m-d',strtotime($_POST['dateTo']));

        require_once ROOT . DS . APP_DIR . DS . '/Vendor/braintree/lib/Braintree.php';
        
        // Non devo disegnare niente
		$this->autoRender = false;
        
        $this->Setting = ClassRegistry::init('Setting');
        $this->Bill = ClassRegistry::init('Bill');
        $this->Utilities = ClassRegistry::init('Utilities');
		
		$setting = $this->Setting->GetMySettings();
	
	    $LIVE = true;
        
        // Chiavi per decriptare
        $PaypalKey = '$PPAL@cryptVladh$$2018';
		$PaypalIv =  'live12388@22@32!';
        
        // Minotti
       // $user = 'MI';
        
       /*switch($user)
       {
        case 'MI':
            $paypalAppUser = 'ARQRWKJpHQ5vIPls0XYRMtyo6g2wGxcpUaLWGv7qM4aKM8Zla99Vd5L4aVagbhoBgoNvMWTxFJ21aRly';
            $paypalAppPassword  = 'EOMlStoRhxdETMZLLbsAmqgkpqDzi_r4Pa96cftEl17ItDTtiz-cgJ2cdrzDY3CFq1a6CNTYHQz5feco';
            $paypalUserId = 'championsbidsrl_api1.gmail.com';
            $paypalUserPassword = 'QDJ8987DB7BAE8F3';
            $paypalUserSignature = 'AB-LuFP0jr4h9MFa0gf4JkY7K0URAyB-8mJqY85zngio9SsQYeLn9V6n';
        break;
       case 'DR':*/
            $paypalAppUser = $this->Utilities->mydecrypt($setting['Setting']['paypal_account'],'aes-256-cbc',$PaypalKey,0,$PaypalIv); 
            $paypalAppPassword  = $this->Utilities->mydecrypt($setting['Setting']['paypal_password'],'aes-256-cbc',$PaypalKey,0,$PaypalIv); 
            $paypalUserId = $this->Utilities->mydecrypt($setting['Setting']['paypal_user_id'],'aes-256-cbc',$PaypalKey,0,$PaypalIv); 
            $paypalUserPassword = $this->Utilities->mydecrypt($setting['Setting']['paypal_user_password'],'aes-256-cbc',$PaypalKey,0,$PaypalIv); 
            $paypalUserSignature = $this->Utilities->mydecrypt($setting['Setting']['paypal_user_signature'],'aes-256-cbc',$PaypalKey,0,$PaypalIv); 
          
      /*  break;
         case 'IN':
            $paypalAppUser = 'AXsIyk57YAJxZbHvC4wNRi2eYt9ZKhPDcSh81qcdQAT2UwU1rlYQf_KfbQvI3PxVqBxb3JBQHyLAFZx2';
            $paypalAppPassword  = 'ENEQxhYV0v3K2w9cOOwKeSikBjDIttEJwm15ennN80RkO5rEmPzBhP4oFiH3wvUUeZk8aCPfDYuFifCY';
            $paypalUserId = 'info_api1.noratech.it';
            $paypalUserPassword = '3BHXQ4S2TMEVWGUY';
            $paypalUserSignature = 'A-0YtcaNAy6Fsipvn88kJ16OJiqDA-a0M2DklRRccWkr.kMJQ4l9XWgy';
        break;
       }*/
    
	  
       $endPoint = "";
	    
	    $authorizationCode = base64_encode($paypalAppUser . ':' . $paypalAppPassword);

        // Chiamata paypal per recupero token autenticazione
		$curl = curl_init();
    	curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/oauth2/token",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "grant_type=client_credentials",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic $authorizationCode",
            "Content-Type: application/x-www-form-urlencoded",
            "cache-control: no-cache"
          ),
        ));
		
		$response = curl_exec($curl);
		
		$err = curl_error($curl);
		curl_close($curl);
		
		$obj = json_decode($response);
		
		$accessToken = $obj->{'access_token'};
        $appId = $obj->{'app_id'};
        
        // Rcupero lista pagamenti             
        $curl = curl_init();
                
                
        // Chiamata api paypal che richiede però permission    
        curl_setopt_array($curl, array(
               //   CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/reporting/transactions?start_date=2017-08-01T11:00:00Z&end_date=2017-08-11T16:00:00Z&count=100",
                  CURLOPT_URL => "https://api.".$endPoint."paypal.com/v1/reporting/transactions?start_date=".$startDate."T00:00:00Z&end_date=".$endDate."T00:00:00Z&count=100",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_POSTFIELDS => "undefined=",
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $accessToken",
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                  ),
                ));    
                
                
             $responseRecoverPayment = curl_exec($curl);
             $errRecoverPayement = curl_error($curl);

             curl_close($curl);
                
             if ($errRecoverPayement) {
                  echo "cURL Error #:" . $errRecoverPayement;
             } 
             else 
             {
                  $transactionResult = json_decode($responseRecoverPayment);         
             }
            
            foreach($transactionResult->{'transaction_details'} as $transaction)
            {
          
                $transactionTemp = $transaction->{'transaction_info'};
                
                $transaction = $transaction->{'transaction_info'}->{'transaction_id'};

                if(!$this->Bill->existsPaypalTransactionId($transaction))
                {
                    
                    // Chiamata alle singole transazioni 
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://api-3t.paypal.com/nvp");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    $auth = "USER=$paypalUserId&PWD=$paypalUserPassword&SIGNATURE=$paypalUserSignature&METHOD=GetTransactionDetails&VERSION=78&TransactionID=$transaction";
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $auth);
                    curl_setopt($ch, CURLOPT_POST, 1);
                            
                    $headers = array();
                    $headers[] = "Content-Type: application/x-www-form-urlencoded";

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            
                    $result = curl_exec($ch);
              
                    if (curl_errno($ch)) 
                    {
                        echo 'Error:' . curl_error($ch);
                    }
                    else
                    {
                        parse_str($result,$newResult);
                        
                        if($newResult['RECEIVEREMAIL'] == $setting['Setting']['paypal_mail'])
                        {
                            $receipt['transaction_id'] = $transaction;
                            $receipt['ragionesociale'] = $newResult['FIRSTNAME'] . ' ' .  $newResult['LASTNAME'];
                            $receipt['receiptdate'] = date('Y-m-d',strtotime($transactionTemp->{'transaction_updated_date'}));
                            // Recupero valori transazione e iva
                            $receipt['amount'] = $transactionTemp->{'transaction_amount'}->{'value'}; // $newResult['transaction_amount']->{'value'};
                            $receipt['isocode'] = $transactionTemp->{'transaction_amount'}->{'currency_code'};
                            if(isset($transactionTemp->{'fee_amount'}))
                            {
                                $receipt['vat'] = $transactionTemp->{'fee_amount'}->{'value'};
                            }
                            $i = 0;
                            do 
                            {
                                if(isset($newResult['L_NAME'.$i]))
                                {
                                    $receipt['Details'][$i]['Description'] = $newResult['L_NAME'.$i];
                                    $receipt['Details'][$i]['Quantity'] = $newResult['L_QTY'.$i];
                                    $receipt['Details'][$i]['Handling'] = $newResult['L_HANDLINGAMT'.$i];
                                    $receipt['Details'][$i]['Shipping'] = $newResult['L_SHIPPINGAMT'.$i];
                                    $receipt['Details'][$i]['Vat'] = $newResult['L_TAXAMT'.$i];
                                    $receipt['Details'][$i]['Amt'] = $newResult['L_AMT'.$i];
                                    $receipt['Details'][$i]['currency'] = $newResult['L_CURRENCYCODE'.$i];
                                }
                                else
                                {   
                                    break;
                                }
                                $i++;
                            } while (true);
                            
                            $this->Bill->createReceiptFromPaypal($receipt);
                        }
                        else
                        {
                        }
                    }
                    curl_close($ch);
                }
                else
                {
                }
                
                /***************************** PROVO A CHIAMARE SINGOLE TRANSAZIONE FINE ****************************************/
            }
            
        }
    }


