<?php
App::uses('AppModel', 'Model');

class Utilities extends AppModel
{
    /** FUNZIONI PER LA GESTIONE DEI FILE **/

    public function isAllowedExtension($extension)
    {
        return !in_array($extension, $this->forbiddenExtensions);
    }

    public function isRecognizedExtension($extension)
    {
        return in_array($extension, $this->recognizedExtensions);
    }

    public $forbiddenExtensions = ['exe', 'dmg', 'dll'];
    public $basePath = ROOT;
    public $recognizedExtensions = ['pdf', 'jpeg', 'jpg', 'gif', 'png', 'bmp'];
    public $useTable = false;

    // Salva un documento nel path specificato
    public function storeUploadedDocument($document, $path = 'documents')
    {
        // Se non esiste creo la directory
        file_exists($path = $this->basePath . DS . $path) ? null : mkdir($path, 0777, true);

        $ext = substr(strtolower(strrchr($document['name'], '.')), 1);

        if ($document['error'] != UPLOAD_ERR_OK || !$this->isAllowedExtension($ext))
            return false;

        $newFileName = time() . "_" . rand(000000, 999999);
        $attachmentName = $newFileName . '.' . $ext;

        //$path = $this->basePath . DS . $path . DS;
        $path = $path . DS;
        $fullPath = $path . $attachmentName;
        move_uploaded_file($document['tmp_name'], $fullPath);

        $document['original_name'] = $document['name'];
        $document['path'] = $path;
        $document['file_name'] = $attachmentName;
        $document['extension'] = $ext;
        return $document;
    }

    /* Cambio solo nome alla funzione */
    public function getFullPathFile($type, $filename)
    {
        return $this->downloadFile($type, $filename);
    }

    public function downloadFile($type, $filename)
    {
        CakeSession::read("Auth.User.company_id");
        if (MYCOMPANY == CakeSession::read("Auth.User.company_id"))
        {
            switch ($type)
            {
                case 'certificazione':
                    $path = 'documents/storageCertification';
                    break;
            }

            $path = $this->basePath . DS . $path . DS . $companyId . DS . $filename;
            return $path;
        }
        else
        {
            throw new ForbiddenException('Non hai il permesso per accedere alla risorsa richiesta');
        }
    }


    /** FINE FUNZIONI PER LA GESTIONE DEI FILE **/


    /** DATE **/

    public function getFirstDayOfYear($year)
    {
        return $year . '-01-01';

    }

    public function getLastDayOfYear($year)
    {
        return $year . '-12-31';
    }

    /** BOLLE **/

    // Recupero il prossimo numero della fattura
    public function getNextTransportNumber($year)
    {
        $this->Transports = ClassRegistry::init('Transports');

        $fields = ['MAX(transport_number * 1) as maxDDT'];

        $conditionArray = [
            'company_id' => MYCOMPANY,
            'date >= ' => $this->getFirstDayOfYear($year),
            'date <= ' => $this->getLastDayOfYear($year),
            'state' => ATTIVO
        ];

        $transport = $this->Transports->find('first', ['fields' => $fields, 'conditions' => $conditionArray, 'order' => 'id desc']);

        if ($transport[0]['maxDDT'] == null)
            return 1;
        else
            return $transport[0]['maxDDT'] + 1;
    }

    public function checkTransportDuplicate($transportNumber, $date, $sectional)
    {
        $this->Transport = ClassRegistry::init('Transport');
        $transportYear = date("Y", strtotime($date));
        $numberOfTransport = $this->Transport->find('count', ['conditions' => ['transport_number' => $transportNumber, 'Transport.date >=' => $transportYear . '-01-01', 'Transport.date <=' => $transportYear . '-12-31', 'Transport.sectional_id' => $sectional, 'Transport.state' => 1, 'Transport.company_id' => MYCOMPANY]]);
        return $numberOfTransport;
    }

    /** INIZIO PREVENTIVO **/

    public function checkQuoteDuplicate($quoteNumber, $date)
    {
        $this->Quote = ClassRegistry::init('Quote');
        $quoteYear = date("Y", strtotime($date));
        $numberOfQuote = $this->Quote->find('count', ['conditions' => ['quote_number' => $quoteNumber, 'Quote.quote_date >=' => $quoteYear . '-01-01', 'Quote.quote_date <=' => $quoteYear . '-12-31', 'Quote.state' => 1, 'Quote.company_id' => MYCOMPANY]]);
        return $numberOfQuote;
    }

    public function createQuoteGoodRow($quoteGoodId, $quoteGoodGood, $rowCounter = 0)
    {
        $this->Quotegoodrow = ClassRegistry::init('Quotegoodrow');
        $newQuoteGoodRow = $this->Quotegoodrow->Create();
        $newQuoteGoodRow['Quotegoodrow']['quote_id'] = $quoteGoodId;
        $newQuoteGoodRow['Quotegoodrow']['description'] = $quoteGoodGood['description']; // description
        $newQuoteGoodRow['Quotegoodrow']['customdescription'] = $quoteGoodGood['customdescription']; // customdescription
        isset($quoteGoodGood['unit_of_measure_id']) ? $newQuoteGoodRow['Quotegoodrow']['unit_of_measure_id'] = $quoteGoodGood['unit_of_measure_id'] : null; // unita
        $newQuoteGoodRow['Quotegoodrow']['quantity'] = $quoteGoodGood['quantity']; // quantità
        $newQuoteGoodRow['Quotegoodrow']['quote_good_row_price'] = $quoteGoodGood['quote_good_row_price']; // Prezzo
        isset($quoteGoodGood['quote_good_row_vat_id']) ? $newQuoteGoodRow['Quotegoodrow']['quote_good_row_vat_id'] = $quoteGoodGood['quote_good_row_vat_id'] : null;
        isset($quoteGoodGood['storage_id']) ? $newQuoteGoodRow['Quotegoodrow']['storage_id'] = $quoteGoodGood['storage_id'] : null;
        isset($quoteGoodGood['codice']) ? $newQuoteGoodRow['Quotegoodrow']['codice'] = $quoteGoodGood['codice'] : null;
        // $newQuoteGoodRow['storage_id'] = $quoteGoodGood['storage_id'];
        // $newQuoteGoodRow['codice'] = $quoteGoodGood['codice'];
        $newQuoteGoodRow['Quotegoodrow']['company_id'] = MYCOMPANY;
        $newQuoteGoodRow['Quotegoodrow']['state'] = 1;
        $newQuoteGoodRow['Quotegoodrow']['line_number'] = $rowCounter;

        if (isset($quoteGoodGood['type_of_line']))
        {
            $newQuoteGoodRow['Quotegoodrow']['type_of_line'] = $quoteGoodGood['type_of_line'];
        }
        else
        {
            $quoteGoodGood['movable'] == -30 ? $newQuoteGoodRow['Quotegoodrow']['type_of_line'] = 'D' : null;
            $quoteGoodGood['movable'] == -20 ? $newQuoteGoodRow['Quotegoodrow']['type_of_line'] = 'O' : null;
            $quoteGoodGood['movable'] == -10 ? $newQuoteGoodRow['Quotegoodrow']['type_of_line'] = 'S' : null;
        }

        return $this->Quotegoodrow->save($newQuoteGoodRow);
    }

    public function createNewStorageFromQuote($good, $clientId)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        // Recupero il catalogo
        $this->Catalog = ClassRegistry::init('Catalog');
        // Recupero il catalogo storages
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Controllo prima se esiste a catalogo
        $clientCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $clientId]]);

        if (isset($clientCatalog['Catalog']['id'])) {
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['description'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
        }

        if (!isset($catalogstorages['Catalogstorages']['description'])) {
            $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
        }

        if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {
            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description']]])) {
                $newStorage = $this->Storage->create();
                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                $newStorage['Storage']['descrizione'] = $good['description'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;
                isset($good['quote_good_row_price']) ? $newStorage['Storage']['prezzo'] = $good['quote_good_row_price'] : 0;
                $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                isset($good['unit_of_measure_id']) ? $newStorage['Storage']['unit_id'] = $good['unit_of_measure_id'] : null;
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['quote_good_row_vat_id']) ? $newStorage['Storage']['vat_id'] = $good['quote_good_row_vat_id'] : null;

                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        } else {
            $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $existInCatalog['Catalogstorages']['storage_id']]]);
            return $oldStorage;
        }
    }


    public function getQuoteValues($quoteId)
    {
        $this->Quotegoodrow = ClassRegistry::init('Quotegoodrow');
        $quoteGoodRows = $this->Quotegoodrow->find('all', ['conditions' => ['quote_id' => $quoteId], 'contain' => ['Iva']]);
        $totaleImponibile = $totaleIvato = 0;
        $totale['taxableincome'] = $totale['total'] = $totale['vat'] = 0;

        foreach ($quoteGoodRows as $quoteGoodRow) {
            $totaleRiga = $quoteGoodRow['Quotegoodrow']['quote_good_row_price'] * $quoteGoodRow['Quotegoodrow']['quantity'];

            if (isset($quoteGoodRow['Iva']['percentuale'])) {
                $totale['vat'] += $totaleRiga * $quoteGoodRow['Iva']['percentuale'] / 100;
            }

            $totale['taxableincome'] += $totaleRiga;
        }

        $totale['total'] = $totale['taxableincome'] + $totale['vat'];
        return $totale;
    }

    // restituisce imponibile
    public function getQuoteTaxableIncome($quoteId)
    {
        $values = $this->GetQuoteValues($quoteId);
        return $values['taxableincome'];
    }

    public function getQuoteTotalVat($quoteId)
    {
        $values = $this->GetQuoteValues($quoteId);
        return $values['vat'];
    }

    public function getQuoteTotal($quoteId)
    {
        $values = $this->GetQuoteValues($quoteId);
        return $values['total'];
    }

    /** FINE PREVENTIVO **/

    /** INIZIO ENTRATA MERCE **/

    public function getNextLoadgoodNumber($year)
    {
        $this->Loadgood = ClassRegistry::init('Loadgood');
        $fields = ['MAX(load_good_number * 1) as maxEntratamerce'];

        // $conditionArray = ['Loadgood.company_id'=> MYCOMPANY,'load_good_date = ' =>  $date];

        $conditionArray =
            [
                'Loadgood.company_id' => MYCOMPANY,
                'load_good_date >= ' => $this->getFirstDayOfYear($year),
                'load_good_date <= ' => $this->getLastDayOfYear($year),
                'Loadgood.state' => ATTIVO
            ];

        $loadGood = $this->Loadgood->find('first', ['fields' => $fields, 'conditions' => $conditionArray, 'order' => 'Loadgood.id desc']);

        if ($loadGood[0]['maxEntratamerce'] == null) {
            return 1;
        } else {
            return $loadGood[0]['maxEntratamerce'] + 1;
        }
    }


    public function createLoadGoodRow($loadGoodId, $loadGoodGood)
    {
        $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
        $newLoadGoodRow = $this->Loadgoodrow->Create();
        $newLoadGoodRow['load_good_id'] = $loadGoodId;
        $newLoadGoodRow['description'] = $loadGoodGood['description']; // description
        //$newLoadGoodRow['unit_of_measure_id'] = $loadGoodGood['unit_of_measure_id']; // unita
        isset($loadGoodGood['unit_of_measure_id']) ? $newLoadGoodRow['unit_of_measure_id'] = $loadGoodGood['unit_of_measure_id'] : null;
        $newLoadGoodRow['quantity'] = $loadGoodGood['quantity']; // quantità
        $newLoadGoodRow['load_good_row_price'] = $loadGoodGood['load_good_row_price']; // Prezzo
        $newLoadGoodRow['load_good_row_vat_id'] = $loadGoodGood['load_good_row_vat_id'];
        $newLoadGoodRow['storage_id'] = $loadGoodGood['storage_id'];
        $newLoadGoodRow['codice'] = $loadGoodGood['codice'];
        $newLoadGoodRow['barcode'] = $loadGoodGood['barcode'];
        $newLoadGoodRow['company_id'] = MYCOMPANY;
        $newLoadGoodRow['state'] = 1;
        $newLoadGoodRow['suggestedsellprice'] = $loadGoodGood['suggestedsellprice'];
        return $this->Loadgoodrow->save($newLoadGoodRow);
    }

    public function getLoadgoodValues($loadgoodId)
    {
        $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
        $loadgoodRows = $this->Loadgoodrow->find('all', ['conditions' => ['load_good_id' => $loadgoodId], 'contain' => ['Iva']]);

        $totale['taxableincome'] = $totale['total'] = $totale['vat'] = 0;

        foreach ($loadgoodRows as $loadgoodRow) {
            $totaleRiga = $loadgoodRow['Loadgoodrow']['load_good_row_price'] * $loadgoodRow['Loadgoodrow']['quantity'];

            if (isset($loadgoodRow['Iva']['percentuale'])) {
                $totale['vat'] += $totaleRiga * $loadgoodRow['Iva']['percentuale'] / 100;
            }

            $totale['taxableincome'] += $totaleRiga;
        }

        $totale['total'] = $totale['taxableincome'] + $totale['vat'];

        return $totale;
    }

    // restituisce imponibile
    public function getLoadgoodTotalTaxableIncome($loadgoodId)
    {
        $values = $this->GetLoadgoodValues($loadgoodId);
        return $values['taxableincome'];
    }

    public function getLoadgoodTotalVat($loadgoodId)
    {
        $values = $this->GetLoadgoodValues($loadgoodId);
        return $values['vat'];
    }

    public function getLoadgoodTotal($loadgoodId)
    {
        $values = $this->GetLoadgoodValues($loadgoodId);
        return $values['total'];
    }

    /** FINE ENTRATA MERCE **/

    /** INIZIO SCONTRINI **/

    public function getNextReceiptNumber($date)
    {
        // Scontrino di default è Tipologia [7]
        $this->Bills = ClassRegistry::init('Bills');
        $fields = ['MAX(numero_fattura * 1) as maxFattura'];
        $conditionArray = ['company_id' => MYCOMPANY, 'state' => ATTIVO, 'date = ' => $date, 'tipologia' => '7'];
        $fatture = $this->Bills->find('first', array('fields' => $fields, 'conditions' => $conditionArray, 'order' => 'id desc'));
        if ($fatture[0]['maxFattura'] == null) {
            return 1;
        } else {
            return $fatture[0]['maxFattura'] + 1;
        }
    }

    /** FINE SCONTRINI **/

    /** INIZIO RIBA **/

    public function getNextRibaNumber($year)
    {
        $this->Bills = ClassRegistry::init('Bill');
        $conditionArray = ['Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year), 'riba' => 1, 'ribaemitted' => 1];
        $billsNumber = $this->Bills->find('count', ['contain' => ['Payment' => 'Bank'], 'conditions' => $conditionArray]);
        return $billsNumber + 1;
    }

    public function getRibaBills($year)
    {
        $this->Bills = ClassRegistry::init('Bill');
        $bills = $this->getBills(null, $riba = 1, $emitted = 0);
        return $bills;
    }

    /** FINE RIBA **/

    /** MATH **/

    // Restituisce valore totale prezzo
    public function calculateRowPrice($rowQuantity, $rowPrice)
    {
        return $rowQuantity * $rowPrice;
    }

    // Restituisce imponibile scorporato dell iva
    public function calcuteTaxableIncomeFromVatIsolation($rowQuantity, $rowPrice, $vatPercentage)
    {
        return $this->calculateRowPrice($rowQuantity, $rowPrice) / (1 + $vatPercentage / 100);
    }

    // Restituisce iva
    public function calcuteVatIsolation($rowQuantity, $rowPrice, $vatPercentage)
    {
        return $this->calculateRowPrice($rowQuantity, $rowPrice) / (1 + $vatPercentage / 100) * ($vatPercentage / 100);
    }

    // Restituisce l'iva passando il vat id
    public function calculateVatIsolationFromVatId($rowQuantity, $rowPrice, $vatId)
    {
        $this->Ivas = ClassRegistry::init('Ivas');
        if ($vatId != null) {
            $currentVat = $this->Ivas->find('first', ['conditions' => ['Ivas.id' => $vatId, 'Ivas.company_Id' => MYCOMPANY]]);
            $currentVatPercentage = $currentVat['Ivas']['percentuale'];
            return $this->calculateRowPrice($rowQuantity, $rowPrice) / (1 + $currentVatPercentage / 100) * ($currentVatPercentage / 100);
        } else {
            return null;
        }
    }


    // Calcoli Fattura
    //public function calculateBillRowtotal($rowQuantity, $rowPrice,$discount=0, $vat=0)
    //{
    //    return $rowQuantity * $rowPrice * (1-$discount /100) * (1+$vat)/100;
    //}

    // Calcolo riga scontato
    public function calculateBillRowWithDiscount($rowQuantity, $rowPrice, $discount)
    {
        return $this->calculateRowPrice($rowQuantity, $rowPrice) * (1 - $discount / 100);
    }

    // Restituisce l'iva
    public function calculateVat($amount, $vatPercentage)
    {
        return $amount * $vatPercentage / 100;
    }

    public function calculateWitholdingTax($amount, $withHoldingTax)
    {
        return $amount * $withHoldingTax / 100;
    }


    /** BANCHE **/
    public function getBanksList($forriba = false)
    {
        $this->Banks = ClassRegistry::init('Banks');
        if ($forriba == false) {
            $conditionArray = ['company_id' => MYCOMPANY, 'Banks.state' => ATTIVO];
        } else {
            $conditionArray = ['company_id' => MYCOMPANY, 'Banks.state' => ATTIVO, 'forriba' => 1];
        }
        return $this->Banks->find('list', ['fields' => ['id', 'description'], 'conditions' => $conditionArray, 'order' => 'description']);
    }

    /** BANCHE **/

    /** CLIENTI **/

    public function createClient($ragionesociale, $clientAddress = '', $clientCap = '', $clientCity = '', $clientProvince = '', $clientNation = 0, $currenciesCode = 'EUR')
    {
        $this->Clients = ClassRegistry::init('Clients');
        $this->Currencies = ClassRegistry::init('Currencies');

        $newClient = $this->Clients->create();
        $newClient['Clients']['ragionesociale'] = $ragionesociale;
        $newClient['Clients']['indirizzo'] = $clientAddress;
        $newClient['Clients']['cap'] = $clientCap;
        $newClient['Clients']['citta'] = $clientCity;
        $newClient['Clients']['provincia'] = $clientProvince;
        $newClient['Clients']['nation_id'] = $clientNation;
        $newClient['Clients']['company_id'] = MYCOMPANY;
        $newClient['Clients']['code'] = $this->getNextClientCode();

        $newClient['Clients']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode('EUR');

        $newClient = $this->Clients->Save($newClient);
        return $newClient['Clients']['id'];
    }

    public function getNextClientCode()
    {

        $fieldsArray = ['MAX(code * 1) as maxClientCode'];
        $fieldsArray2 = ['code'];

        $clientCodes = $this->Clients->find('all', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO], 'fields' => $fieldsArray2]);
        $arrayClients = [];
        foreach ($clientCodes as $key => $clientcode) {
            $arrayClients[$clientcode['Clients']['code']] = $clientcode['Clients']['code'];
        }

        try {
            $clientNextCode = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO], 'fields' => $fieldsArray])[0]['maxClientCode'];
            $clientNextCode++;
        } catch (Exception $ecc) {
            $clientNextCode = 0;
        }

        for ($i = 1; $i < $clientNextCode; $i++) {
            isset($arrayClients[$i]) ? null : $clientNextCode = $i;
        }

        return $clientNextCode;
    }


    // Resituisce se è abilitato lo spostamento delle scadenze del 31/08 e 31/12 al mese successivo
    public function getDefaultClientDeadlineAugustSeptemberFlag($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['billdeadlineaugustanddecemeber'])) {
            return $client['Clients']['billdeadlineaugustanddecemeber'];
        }
    }

    // Metodo di pagamento predefinito clienti
    public function getDefaultClientSplitPayment($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['splitpayment'])) {
            return $client['Clients']['splitpayment'];
        }
    }

    // Metodo di pagamento predefinito clienti
    public function getDefaultClientPaymentMethodId($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['payment_id'])) {
            return $client['Clients']['payment_id'];
        }
    }

    public function getDefaultClientPaymentMethodCollectionfees($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $this->Payments = ClassRegistry::init('Payments');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['payment_id'])) {
            $payment = $this->Payments->find('first', ['conditions' => ['Payments.company_id' => MYCOMPANY, 'Payments.state' => ATTIVO, 'Payments.id' => $client['Clients']['payment_id']]]);
            return $payment['Payments']['paymentFixedCost'];

        }
    }

    // Iva predefinito clienti
    public function getDefaultClientVat($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['vat_id'])) {
            return $client['Clients']['vat_id'];
        }
    }

    public function getCustomers()
    {
        $this->Clients = ClassRegistry::init('Clients');
        return $this->Clients->find('all', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO], 'order' => ['Clients.ragionesociale' => 'asc']]);
    }

    // Lista clienti
    // public function getCustomersList()
    //{
    //   $this->Clients = ClassRegistry::init('Clients');
    //   return $this->Clients->find('list',['conditions'=>['Clients.company_id'=>MYCOMPANY,'Clients.state'=>ATTIVO], 'fields' => ['Clients.ragionesociale'], 'order' => ['Clients.ragionesociale' => 'asc']]);
    //}

    /* public function getCustomerListV2()
    {
        $this->Clients = ClassRegistry::init('Clients');
        return $this->Clients->find('list',['conditions'=>['Clients.company_id'=>MYCOMPANY,'Clients.state'=>ATTIVO], 'fields' => ['Clients.id','Clients.ragionesociale'], 'order' => ['Clients.ragionesociale' => 'asc']]);
    }*/

    public function getCustomerName($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'id' => $customerId], 'fields' => ['Clients.ragionesociale']]);
        if ($client != null) {
            return $client['Clients']['ragionesociale'];
        } else {
            return null;
        }
    }

    public function getCustomerId($customerName)
    {
        $this->Clients = ClassRegistry::init('Clients');

        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'ragionesociale' => $customerName]]);
        if ($client != null) {
            return $client['Clients']['id'];
        } else {
            return null; // Non esiste il cliente
        }
    }

    /** FINE CLIENTI **/

    public function getBuyBills()
    {
        return $this->getBills(2);
    }

    public function getCreditnotes()
    {
        return $this->getBills(3);
    }

    // Restituisce l'array delle causali delle bolle
    public function getCausals($type = 'all')
    {
        $this->Causals = ClassRegistry::init('Causals');
        switch ($type) {
            case 'all':
                $conditionArray = ['Causals.state' => ATTIVO, 'Causals.company_id' => MYCOMPANY];
                break;
            case 'clienti':
                $conditionArray = ['Causals.state' => ATTIVO, 'Causals.company_id' => MYCOMPANY, 'clfo' => 'C'];
                break;
            case 'fornitori':
                $conditionArray = ['Causals.state' => ATTIVO, 'Causals.company_id' => MYCOMPANY, 'clfo' => 'F'];
                break;
        }

        return $this->Causals->find('list', ['conditions' => $conditionArray]);
    }

    public function getTransportCausalNameFromId($causalId)
    {
        $this->Causals = ClassRegistry::init('Causals');
        if (isset($this->Causals->find('first', ['conditions' => ['Causals.state' => ATTIVO, 'Causals.id' => $causalId]])['Causals'])) {
            return $this->Causals->find('first', ['conditions' => ['Causals.state' => ATTIVO, 'Causals.id' => $causalId]])['Causals']['name'];
        } else {
            return null;
        }
    }

    public function getClientIdFromClientName($clientName){
        $this->Clients = ClassRegistry::init('Clients');
        $clientId = $this->Clients->find('first', ['conditions'=>['state'=>ATTIVO, 'company_id'=>MYCOMPANY, 'ragionesociale'=>$clientName]]);
        if($clientId == null)
        {
            return 0;
        }
        return $clientId['Clients']['id'];
    }

    /** VETTORI **/
    public function getCarriersList()
    {
        $this->Carriers = ClassRegistry::init('Carriers');
        return $this->Carriers->find('list', ['conditions' => ['Carriers.company_id' => MYCOMPANY, 'Carriers.state' => ATTIVO], 'fields' => ['Carriers.name'], 'order' => ['Carriers.name' => 'asc']]);
    }

    public function getCarrierName($carrierId)
    {
        $this->Carriers = ClassRegistry::init('Carriers');
        return $this->Carriers->find('first', ['conditions' => ['Carriers.company_id' => MYCOMPANY, 'Carriers.state' => ATTIVO, 'id' => $carrierId], 'fields' => ['Carriers.name']])['Carriers']['name'];
    }

    /** FATTURA ELETTRONICA **/

    public function getEinvoiceWithholdingTaxCausals()
    {
        $this->Einvoicewithholdingtaxcausal = ClassRegistry::init('Einvoicewithholdingtaxcausal');
        //$einvoicewithholdingtaxcausals = $this->Einvoicewithholdingtaxcausal->find('all');
        $einvoicewithholdingtaxcausals =  $this->Einvoicewithholdingtaxcausal->find('all',['conditions' => ['state' => 1]]);
        foreach ($einvoicewithholdingtaxcausals as $einvoicewithholdingtaxcausal) {
            $arrayCausals[$einvoicewithholdingtaxcausal['Einvoicewithholdingtaxcausal']['id']] = $einvoicewithholdingtaxcausal['Einvoicewithholdingtaxcausal']['code'] . ' - ' . $einvoicewithholdingtaxcausal['Einvoicewithholdingtaxcausal']['description'];
        }
        return $arrayCausals;
    }

    // Restituisce codice => codice + descrizione
    public function getEinvoiceFiscalRegimesList()  // Sono gli stessi per tutte le società
    {
        $this->Einvoicefiscalregime = ClassRegistry::init('Einvoicefiscalregime');
        $fiscalregimes = $this->Einvoicefiscalregime->find('all');
        foreach ($fiscalregimes as $fiscalregime) {
            $arrayRegimes[$fiscalregime['Einvoicefiscalregime']['code']] = $fiscalregime['Einvoicefiscalregime']['code'] . ' - ' . $fiscalregime['Einvoicefiscalregime']['description'];
        }
        return $arrayRegimes;
    }

    public function getEinvoicevatnature()
    {
        $this->Einvoicevatnature = ClassRegistry::init('Einvoicevatnature');
        //$vatnatures = $this->Einvoicevatnature->find('all');
        $vatnatures =  $this->Einvoicevatnature->find('all',['conditions' => ['state' => 1]]);

        foreach ($vatnatures as $vatnature) {
            $arrayVatures[$vatnature['Einvoicevatnature']['id']] = '[' . $vatnature['Einvoicevatnature']['code'] . '] ' . $vatnature['Einvoicevatnature']['description'];
        }
        return $arrayVatures;
    }


    public function getEinvoiceVatNatureIdFromCode($code)
    {
        $this->Einvoicevatnature = ClassRegistry::init('Einvoicevatnature');
        $vatnatures = $this->Einvoicevatnature->find('first', ['conditions' => ['code' => $code]]);
        if (isset($vatnatures['Einvoicevatnature']['id'])) {
            return $vatnatures['Einvoicevatnature']['id'];
        } else {
            return null;
        }
    }

    public function getEinvoiceVatNatureCodeFromId($idVatNuture)
    {
        $this->Einvoicevatnature = ClassRegistry::init('Einvoicevatnature');
        $vatnatures = $this->Einvoicevatnature->find('first', ['conditions' => ['id' => $idVatNuture]]);
        if (isset($vatnatures['Einvoicevatnature']['code'])) {
            return $vatnatures['Einvoicevatnature']['code'];
        } else {
            return null;
        }
    }

    public function getEinvoiceVatDescriptionFromId($idVatNuture)
    {
        $this->Einvoicevatnature = ClassRegistry::init('Einvoicevatnature');
        $vatnatures = $this->Einvoicevatnature->find('first', ['conditions' => ['id' => $idVatNuture]]);
        if (isset($vatnatures['Einvoicevatnature']['description'])) {
            return $vatnatures['Einvoicevatnature']['description'];
        } else {
            return null;
        }
    }

    // Restituisce codice => codice + descrizione
    public function getEinvoicePaymentType()  // Sono gli stessi per tutte le società
    {
        $this->Einvoicepaymenttype = ClassRegistry::init('Einvoicepaymenttype');
        $fiscalregimes = $this->Einvoicepaymenttype->find('all', ['order' => 'description']);
        foreach ($fiscalregimes as $fiscalregime) {
            $arrayRegimes[$fiscalregime['Einvoicepaymenttype']['id']] = $fiscalregime['Einvoicepaymenttype']['description'];
        }
        return $arrayRegimes;
    }


    // Restituisce codice => codice + descrizione
    public function getEinvoicePaymentmethod()  // Sono gli stessi per tutte le società
    {
        $this->Einvoicepaymentmethod = ClassRegistry::init('Einvoicepaymentmethod');
        $fiscalregimes = $this->Einvoicepaymentmethod->find('all', ['order' => 'description']);
        foreach ($fiscalregimes as $fiscalregime) {
            $arrayRegimes[$fiscalregime['Einvoicepaymentmethod']['id']] = $fiscalregime['Einvoicepaymentmethod']['description'];
        }
        return $arrayRegimes;
    }

    // Restituisce codice => descrizione
    public function getEinvoicePaymentmethodCode()  // Sono gli stessi per tutte le società
    {
        $this->Einvoicepaymentmethod = ClassRegistry::init('Einvoicepaymentmethod');
        $fiscalregimes = $this->Einvoicepaymentmethod->find('all', ['order' => 'description']);
        foreach ($fiscalregimes as $fiscalregime) {
            $arrayRegimes[$fiscalregime['Einvoicepaymentmethod']['code']] = $fiscalregime['Einvoicepaymentmethod']['description'];
        }
        return $arrayRegimes;
    }


    // Restituisce codice => codice + descrizione
    public function getEinvoiceVatEsigibility()  // Sono gli stessi per tutte le società
    {
        $this->Einvoicevatesigibility = ClassRegistry::init('Einvoicevatesigibility');
        $fiscalregimes = $this->Einvoicevatesigibility->find('all');
        foreach ($fiscalregimes as $fiscalregime) {
            $arrayRegimes[$fiscalregime['Einvoicevatesigibility']['code']] = $fiscalregime['Einvoicevatesigibility']['code'] . ' - ' . $fiscalregime['Einvoicevatesigibility']['description'];
        }
        return $arrayRegimes;
    }


    // Resituisce l'elenco delle casse
    public function getEinvoiceWelfareBox()
    {
        $this->Einvoicewelfarebox = ClassRegistry::init('Einvoicewelfarebox');
        $welfareboxes = $this->Einvoicewelfarebox->find('all');
        foreach ($welfareboxes as $welfarebox) {
            $arrayWelfareBoxes[$welfarebox['Einvoicewelfarebox']['code']] = $welfarebox['Einvoicewelfarebox']['code'] . ' - ' . $welfarebox['Einvoicewelfarebox']['description'];
        }
        return $arrayWelfareBoxes;
    }

    /** FORNITORI **/

    public function getSuppliers()
    {
        $this->Suppliers = ClassRegistry::init('Suppliers');
        return $this->Suppliers->find('all', ['conditions' => ['Suppliers.company_id' => MYCOMPANY, 'Suppliers.state' => ATTIVO], 'order' => ['Suppliers.name' => 'asc']]);
    }

    public function getSupplierId($supplierName)
    {
        $this->Suppliers = ClassRegistry::init('Suppliers');
        $supplier = $this->Suppliers->find('first', ['conditions' => ['Suppliers.company_id' => MYCOMPANY, 'Suppliers.state' => ATTIVO, 'name' => $supplierName]]);

        if ($supplier != null) {
            return $supplier['Suppliers']['id'];
        } else {
            return null; // Non esiste il cliente
        }
    }

    public function createSupplier($supplierName, $supplierAddress = '', $supplierCap = '', $supplierCity = '', $supplierProvince = '', $supplierNation = null)
    {
        $this->Suppliers = ClassRegistry::init('Suppliers');
        $newSupplier = $this->Suppliers->create();
        $newSupplier['Suppliers']['name'] = $supplierName;
        $newSupplier['Suppliers']['indirizzo'] = $supplierAddress;
        $newSupplier['Suppliers']['cap'] = $supplierCap;
        $newSupplier['Suppliers']['city'] = $supplierCity;
        $newSupplier['Suppliers']['province'] = $supplierProvince;
        $newSupplier['Suppliers']['nation_id'] = $supplierNation;
        $newSupplier['Suppliers']['company_id'] = MYCOMPANY;
        $newSupplier['Suppliers']['code'] = $this->getNextSupplierCode();

        $newSupplier = $this->Suppliers->Save($newSupplier);
        return $newSupplier['Suppliers']['id'];
    }
    public function getCategoryByName($name){
        $this->Category = ClassRegistry::init('Category');

        $category = $this->Category->find('first', ['conditions'=>['Category.description'=>$name]]);
        return $category['Category']['id'];

    }
    public function getNextSupplierCode()
    {

        $fieldsArray = ['MAX(code * 1) as maxClientCode'];
        $fieldsArray2 = ['code'];
        $supplierCodes = $this->Suppliers->find('all', ['conditions' => ['Suppliers.company_id' => MYCOMPANY, 'Suppliers.state' => ATTIVO], 'fields' => $fieldsArray2]);
        $arraySuppliers = [];
        foreach ($supplierCodes as $key => $suppliercode) {
            $arraySuppliers[$suppliercode['Suppliers']['code']] = $suppliercode['Suppliers']['code'];
        }

        try {
            $supplierNextCode = $this->Suppliers->find('first', ['conditions' => ['Suppliers.company_id' => MYCOMPANY, 'Suppliers.state' => ATTIVO], 'fields' => $fieldsArray])[0]['maxClientCode'];
            $supplierNextCode++;
        } catch (Exception $ecc) {
            $supplierNextCode = 0;
        }

        for ($i = 1; $i < $supplierNextCode; $i++) {
            isset($arraySuppliers[$i]) ? null : $supplierNextCode = $i;
        }

        return $supplierNextCode;
    }

    /** METODI DI PAGAMENTO **/

    public function getPaymentsList($scadenza = -1)
    {
        $this->Payment = ClassRegistry::init('Payment');

        // Se scadenza = 0 recupero solo immediati
        if ($scadenza == 0) {

            $payments = $this->Payment->find('all', ['contain' => ['Paymentdeadlines']]);
            $arrayId[] = null;

            foreach ($payments as $payment) {
                if (count($payment['Paymentdeadlines']) == 1 && $payment['Paymentdeadlines'][0]['deadlineday'] == 0) {
                    $arrayId[] = $payment['Payment']['id'];

                }
            }

            return $this->Payment->find('list', ['contain' => ['Paymentdeadlines'], 'conditions' => ['Payment.company_id' => MYCOMPANY, 'Payment.state' => ATTIVO, 'Payment.id' => $arrayId], 'fields' => ['Payment.id', 'Payment.metodo'], 'order' => ['Payment.metodo' => 'asc']]);

        } else {
            $payments = $this->Payment->find('all', ['contain' => ['Paymentdeadlines']]);
            $arrayId = [];


            foreach ($payments as $payment) {
                if (!($payment['Paymentdeadlines'] == [])) {
                    $arrayId[] = $payment['Payment']['id'];

                }
            }

            return $this->Payment->find('list', ['conditions' => ['Payment.company_id' => MYCOMPANY, 'Payment.state' => ATTIVO, 'Payment.id' => $arrayId], 'fields' => ['Payment.id', 'Payment.metodo'], 'order' => ['Payment.metodo' => 'asc']]);
        }

    }

    /** FINE METODI DI PAGAMENTO **/

    /** NAZIONI **/

    public function getNationsList()
    {
        $this->Nation = ClassRegistry::init('Nation');
        return $this->Nation->find('list', ['fields' => ['Nation.id', 'Nation.name'], 'order' => ['Nation.name' => 'asc']]);
    }

    public function getNationName($nationId)
    {
        $this->Nation = ClassRegistry::init('Nation');
        return $this->Nation->find('first', ['fields' => ['Nation.name'], 'conditions' => ['Nation.id' => $nationId], 'order' => ['Nation.name' => 'asc']]);
    }

    public function getNationIdFromShortCode($nationName)
    {
        $this->Nation = ClassRegistry::init('Nation');
        if (isset($this->Nation->find('first', ['fields' => ['Nation.id'], 'conditions' => ['Nation.shortcode' => $nationName], 'order' => ['Nation.name' => 'asc']])['Nation']['id'])) {
            return $this->Nation->find('first', ['fields' => ['Nation.id'], 'conditions' => ['Nation.shortcode' => $nationName], 'order' => ['Nation.name' => 'asc']])['Nation']['id'];
        } else {
            return 0;
        }
    }

    public function getNationShortcodeFromId($nationId)
    {
        $this->Nation = ClassRegistry::init('Nation');
        // Se non è settata la nazione passo null
        if (isset($this->Nation->find('first', ['fields' => ['Nation.shortcode'], 'conditions' => ['Nation.id' => $nationId]])['Nation']['shortcode'])) {
            return $this->Nation->find('first', ['fields' => ['Nation.shortcode'], 'conditions' => ['Nation.id' => $nationId]])['Nation']['shortcode'];
        } else {
            return null;
        }
    }


    public function getNationShortcodeList()
    {
        $this->Nation = ClassRegistry::init('Nation');
        return $this->Nation->find('list', ['fields' => ['Nation.shortcode', 'Nation.shortcode'], 'order' => ['Nation.shortcode' => 'asc']]);
    }

    /** FINE NAZIONI **/


    public function getCustomerDifferentAddress($clientId)
    {
        // Il cliente è nuovo non devo recuperare il catalogo
        if ($clientId == null) {
            return null;
        }
        $this->Clientdestination = ClassRegistry::init('Clientdestination');
        $differentAddress = $this->Clientdestination->find('all', ['recursive' => 2, 'conditions' => ['Clientdestination.client_id' => $clientId, 'Clientdestination.company_id' => MYCOMPANY, 'Clientdestination.state' => ATTIVO]]);
        return $differentAddress;
    }

    public function getSupplierDifferentAddress($supplierId)
    {
        // Il cliente è nuovo non devo recuperare il catalogo
        if ($supplierId == null)
        {
            return null;
        }

        $this->Supplierdestination = ClassRegistry::init('Supplierdestination');
        $differentAddress = $this->Supplierdestination->find('all', ['recursive' => 2, 'conditions' => ['supplier_id' => $supplierId, 'company_id' => MYCOMPANY, 'Supplierdestination.state' => ATTIVO]]);
        return $differentAddress;
    }

    public function getResellerClient($resellerId)
    {
        if ($resellerId == null)
        {
            return null;
        }

        $this->Clientreseller = ClassRegistry::init('Clientreseller');
        $differentClients = $this->Clientreseller->find('all', ['recursive' => 2, 'conditions' => ['reseller_id' => $resellerId, 'Clientreseller.company_id' => MYCOMPANY, 'Clientreseller.state' => ATTIVO]]);
        return $differentClients;
    }

    public function getStoragesWithCatalog($clientId, $movable = 'all')
    {
        // Il cliente è nuovo non devo recuperare il catalogo
        if ($clientId == null) {
            return null;
        }

        $this->Storage = ClassRegistry::init('Storage');
        $this->Catalogs = ClassRegistry::init('Catalogs');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Recupero il catalog
        $catalog = $this->Catalogs->find('first', ['conditions' => ['client_id' => $clientId]]);

        $storages = $this->getStoragesWithVariations(null, $movable);

        $newStorages = [];
        foreach ($storages as $storage) {
            if ($catalog == null) {
                $cs = null;
            } else {
                // Gestisce errore di ricerca articolo nel listino cliente (non presente)
                $cs = $this->Catalogstorages->find('first',
                    [
                        'conditions' => ['storage_id' => $storage['Storages']['id'], 'catalog_id' => $catalog['Catalogs']['id']],
                        'fields' => ['id', 'description', 'catalog_price'],
                    ]);
            }

            $newKey = $storage['Storages']['id'];

            $newStorages[$newKey]['id'] = $storage['Storages']['id'];

            if ($cs != null) {
                $cs['Catalogstorages']['description'] != null ? $newStorages[$newKey]['descrizione'] = $cs['Catalogstorages']['description'] : $newStorages[$newKey]['descrizione'] = $storage['Storages']['descrizione'];
                $cs['Catalogstorages']['catalog_price'] != null ? $newStorages[$newKey]['prezzo'] = $cs['Catalogstorages']['catalog_price'] : $newStorages[$newKey]['prezzo'] = $storage['Storages']['prezzo'];
            } else  // Risolve il problema degli storage senza listino.
            {
                $newStorages[$newKey]['descrizione'] = $storage['Storages']['descrizione'];
                $newStorages[$newKey]['prezzo'] = $storage['Storages']['prezzo'];
            }

            $newStorages[$newKey]['codice'] = $storage['Storages']['codice'];
            $newStorages[$newKey]['unit_id'] = $storage['Storages']['unit_id'];
            $newStorages[$newKey]['vat_id'] = $storage['Storages']['vat_id'];
            $newStorages[$newKey]['movable'] = $storage['Storages']['movable'];
        }
        return $newStorages;
    }

    public function existClientCatalog($clientId)
    {
        $this->Catalogs = ClassRegistry::init('Catalogs');

        // Controllo prima se esiste a catalogo
        $countCatalog = $this->Catalogs->find('count', ['conditions' => ['Catalogs.client_id' => $clientId]]);

        if ($countCatalog > 0) {
            return $this->Catalogs->find('first', ['conditions' => ['Catalogs.client_id' => $clientId]]);
        } else {
            return false;
        }
    }


    public function getStorageName($id)
    {
        $this->Storages = ClassRegistry::init('Storages');
        return $this->Storages->find('first', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.state' => ATTIVO, 'id' => $id], 'fields' => ['Storages.descrizione']])['Storages']['descrizione'];
    }

    // Valute
    public function getCurrenciesList()
    {
        $this->Currencies = ClassRegistry::init('Currencies');
        return $this->Currencies->find('list', ['fields' => ['id', 'description'], 'conditions' => ['company_id' => MYCOMPANY, 'Currencies.state' => ATTIVO]]);
    }

    /** UNITA DI MISURA **/
    public function getUnitsList()
    {
        $this->Units = ClassRegistry::init('Units');
        return $this->Units->find('list', ['fields' => ['id', 'description'], 'conditions' => ['company_id' => MYCOMPANY, 'Units.state' => ATTIVO]]);
    }

    public function getUnitsIdFromName($unitDescription)
    {
        $this->Units = ClassRegistry::init('Units');
        $unit = $this->Units->find('first', ['fields' => ['id'], 'conditions' => ['company_id' => MYCOMPANY, 'Units.state' => ATTIVO, 'Units.description' => $unitDescription]]);
        if ($unit != null) {
            return $unit['Units']['id'];
        } else {
            return null;
        }
    }


    /** FINE UNITA DI MISURA **/


    /** IVA **/
    public function getVatsList()
    {
        $this->Ivas = ClassRegistry::init('Ivas');
        $vats = $this->Ivas->find('all', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.state' => ATTIVO], 'order' => ['Ivas.descrizione']]);

        $arrayVat = null;

        foreach ($vats as $vat) {
            $arrayVat[$vat['Ivas']['id']] = $vat['Ivas']['descrizione'] . ' - ' . $vat['Ivas']['percentuale'];
        }

        return $arrayVat;
    }


    public function getVatFromPercentage($VatPercentage, $nature = null)
    {
        $this->Ivas = ClassRegistry::init('Ivas');

        if ($nature == null) {
            $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.state' => ATTIVO, 'percentuale' => $VatPercentage]]);
        } else {
            // Per ora è uguale in quanto sempre null
            $ivaNatureId = $this->getEinvoiceVatNatureIdFromCode($nature);
            $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.state' => ATTIVO, 'percentuale' => $VatPercentage, 'einvoicenature_id' => $ivaNatureId]]);
        }

        if ($ivas != null) {
            return $ivas['Ivas']['id'];
        }
    }

    public function getVatDescriptionFromId($vatId)
    {
        $this->Ivas = ClassRegistry::init('Ivas');
        $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.id' => $vatId]]);
        if ($ivas != null) {
            return $ivas['Ivas']['descrizione'];
        }
    }

    // Non metto il filtro con state = 0 se c'è l'id suppongo ci sia l'iva //TO DO
    public function getVatPercentageFromId($vatId)
    {
        $this->Ivas = ClassRegistry::init('Ivas');
        $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.id' => $vatId]]);
        if ($ivas != null) {
            return $ivas['Ivas']['percentuale'];
        }
    }

    // Non metto il filtro con state = 0 se c'è l'id suppongo ci sia l'iva //TO DO
    public function getVatIdFromPercentage($percentage, $nature = null)
    {
        $this->Ivas = ClassRegistry::init('Ivas');


        if ($nature == null) {
            $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.percentuale' => $percentage, 'state' => ATTIVO]]);
        } else {
            $einvoiceVatNatureId = $this->getEinvoiceVatNatureIdFromCode($nature);
            if ($einvoiceVatNatureId != null) // Se non trovo un id
            {
                $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.percentuale' => $percentage, 'einvoicenature_id' => $einvoiceVatNatureId, 'state' => ATTIVO]]);
            }
        }

        if ($ivas != null && $ivas != []) {
            return $ivas['Ivas']['id'];
        }
    }

    public function getVatFromId($vatId)
    {
        $this->Ivas = ClassRegistry::init('Ivas');
        $ivas = $this->Ivas->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'Ivas.id' => $vatId]]);
        return $ivas;
    }


    /** FINE IVA IVA **/


    public function getCompanyUserGroup()
    {
        $this->Groups = ClassRegistry::init('Groups');
        return $this->Groups->find('list', ['fields' => ['id', 'description'], 'conditions' => ['company_id' => MYCOMPANY, 'Groups.state' => ATTIVO]]);
    }

    // Restituisce le scadenze della fattura
    public function getDeadlines($billId)
    {
        $this->Deadlines = ClassRegistry::init('Deadlines');
        return $this->Deadlines->find('all', ['conditions' => ['bill_id' => $billId, 'company_id' => MYCOMPANY]]);
    }

    // Restituisce le scadenze della fattura
    public function getDeadlinesTotal($billId)
    {
        $this->Deadlines = ClassRegistry::init('Deadlines');
        $deadlines = $this->Deadlines->find('all', ['conditions' => ['bill_id' => $billId, 'company_id' => MYCOMPANY]]);
        $totalDeadlines = null;
        foreach ($deadlines as $deadline) {
            $totalDeadlines += $deadline['Deadlines']['amount'];
        }
        return $totalDeadlines;
    }

    /** END -- Funzioni per il recupero delle liste **/

    /** SCONTRINI **/

    // Funzione per la creazione dell'articolo dello scontrino
    public function createReceiptGood($receiptId, $good)
    {
        // Faccio i calcoli su quantità unitaria
        $vat = number_format($this->calculateVatIsolationFromVatId($good['prezzo'], 1, $good['iva_id']), 2);
        $taxableIncome = $this->calculateRowPrice(1, $good['prezzo']) - $vat;
        $good['prezzo'] = $taxableIncome;
        $good['vat'] = $vat;
        return $this->createBillGood($receiptId, $good);
    }

    public function setReceiptGoodValues($oldGood, $newbillId, $good)
    {
        $vat = number_format($this->calculateVatIsolationFromVatId($good['prezzo'], 1, $good['iva_id']), 2);
        $taxableIncome = $this->calculateRowPrice(1, $good['prezzo']) - $vat;

        $good['prezzo'] = $taxableIncome;
        $good['vat'] = $vat;

        $this->setBillGoodValues($oldGood, $newbillId, $good);
    }

    public function checkReceiptDuplicate($receiptNumber, $date)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $receiptDate = date("Y-m-d", strtotime($date));
        // 7 = scontrino
        $numberOfReceipt = $this->Bill->find('count', ['conditions' => ['numero_fattura' => $receiptNumber, 'date =' => $receiptDate, 'tipologia' => 7, 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY]]);
        return $numberOfReceipt;
    }

    /** FINE SCONTRINI **/

    /** FATTURE **/

    // Recupero il prossimo numero della fattura
    public function getNextBillNumber($year, $type = 'all', $sectionalId = null)
    {
        $this->Bills = ClassRegistry::init('Bills');

        $fields = ['MAX(numero_fattura * 1) as maxFattura'];

        if ($type != 'all') // Per definire se è fattura d'acquisto di vendita o nota di credito
        {
            // Suddivido fatture normali e accompagnatorie (sono tutte 2 di type 1 solo che accompagnatorie hanno accompagnatoria = 1)
            if ($type != 'privato')
            {
                if ($sectionalId != null)
                {
                    $conditionArray = ['sectional_id' => $sectionalId, 'company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year), 'tipologia' => $type];
                }
                else
                {
                    $conditionArray = ['company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year), 'tipologia' => $type];
                }
            }
            else
            {
                if ($sectionalId != null)
                {
                    $conditionArray = ['sectional_id' => $sectionalId, 'company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year), 'tipologia IN ' => [1, 6]];
                }
                else
                {
                    $conditionArray = ['company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year), 'tipologia IN ' => [1, 6]];
                }
            }
        }
        else
        {
            if ($sectionalId != null)
            {
                $conditionArray = ['sectional_id' => $sectionalId, 'company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year)];
            }
            else
            {
                $conditionArray = ['company_id' => MYCOMPANY, 'Bills.state' => ATTIVO, 'date >= ' => $this->getFirstDayOfYear($year), 'date <= ' => $this->getLastDayOfYear($year)];
            }
        }

        $fatture = $this->Bills->find('first', ['fields' => $fields, 'conditions' => $conditionArray, 'order' => 'id desc']);

        if ($fatture[0]['maxFattura'] == null) {
            return 1;
        } else {
            return $fatture[0]['maxFattura'] + 1;
        }
    }


    public function checkBillDuplicate($billNumber, $date, $typeOfBill, $sectional = null, $supplierName = null, $supplierId = null)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Supplier = ClassRegistry::init('Supplier');
        $billYear = date("Y", strtotime($date));
        if ($sectional != null)  // Fatture e note di credito
        {
            $numberOfBill = $this->Bill->find('count', ['conditions' => ['numero_fattura' => $billNumber, 'date >=' => $billYear . '-01-01', 'date <=' => $billYear . '-12-31', 'tipologia' => $typeOfBill, 'Bill.sectional_id' => $sectional, 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY]]);
        } else // Fatture d'acquisto
        {
            //$numberOfBill = $this->Bill->find('count', ['conditions'=>['numero_fattura' => $billNumber,'date >=' => $billYear .'-01-01' , 'date <=' => $billYear.'-12-31' ,'tipologia' => $typeOfBill,'Bill.state'=>1 ,'Bill.company_id'=>MYCOMPANY]]);
            if ($supplierName == null && $supplierId == null) {
                $numberOfBill = $this->Bill->find('count', ['conditions' => ['numero_fattura' => $billNumber, 'date >=' => $billYear . '-01-01', 'date <=' => $billYear . '-12-31', 'tipologia' => $typeOfBill, 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY]]);
            } else {
                if ($supplierName != null) {
                    $numberOfBill = $this->Bill->find('count', ['conditions' => ['numero_fattura' => $billNumber, 'date >=' => $billYear . '-01-01', 'date <=' => $billYear . '-12-31', 'tipologia' => $typeOfBill, 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.supplier_name' => $supplierName]]);
                }

                if ($supplierId != null) {
                    $supplierName = $this->Supplier->getSupplierName($supplierId);
                    $numberOfBill = $this->Bill->find('count', ['conditions' => ['numero_fattura' => $billNumber, 'date >=' => $billYear . '-01-01', 'date <=' => $billYear . '-12-31', 'tipologia' => $typeOfBill, 'Bill.state' => 1, 'Bill.company_id' => MYCOMPANY, 'Bill.supplier_name' => $supplierName]]);
                }
            }
        }
        return $numberOfBill;
    }

    public function checkOrderDuplicate($orderNumber, $date, $typeOfOrder, $sectional = null)
    {
        $this->Order = ClassRegistry::init('Order');
        $this->Supplier = ClassRegistry::init('Supplier');
        $billYear = date("Y", strtotime($date));
        $numberOfOrder = $this->Order->find('count', ['conditions' => ['numero_ordine' => $orderNumber, 'date >=' => $billYear . '-01-01', 'date <=' => $billYear . '-12-31', 'tipologia' => $typeOfOrder, 'Order.sectional_id' => $sectional, 'Order.state' => 1, 'Order.company_id' => MYCOMPANY]]);

        return $numberOfOrder;
    }

    public function checkLoadGoodDuplicate($loadGoodNumber, $date)
    {
        $this->Loadgood = ClassRegistry::init('Loadgood');
        $loadGoodYear = date("Y", strtotime($date));
        $loadGoodNumber = $this->Loadgood->find('count', ['conditions' => ['load_good_number' => $loadGoodNumber, 'load_good_date >=' => $loadGoodYear . '-01-01', 'load_good_date <=' => $loadGoodYear . '-12-31', 'Loadgood.state' => 1, 'Loadgood.company_id' => MYCOMPANY]]);
        return $loadGoodNumber;
    }

    // Recupero le fatture
    public function getBills($type = null, $riba = null, $emitted = null)
    {

        $this->Bill = ClassRegistry::init('Bill');

        $conditionArray['Bill.company_id'] = MYCOMPANY;

        if ($type !== null) {
            $conditionArray['Bill.tipologia'] = $type;
        }
        if ($riba !== null) {
            $riba == true ? $conditionArray['Payment.riba'] = 1 : $conditionArray['Payment.riba'] = 0;
        }
        // if($payed !== null) { $payed == true ? $conditionArray['pagato'] = 1 : $conditionArray['pagato'] = 0;  }
        if ($emitted !== null) {
            $conditionArray['ribaemitted'] = $emitted;
        }
        $conditionArray['Bill.state'] = 1;

        $bills = $this->Bill->find('all',
            ['contain' => [
                'Client' => 'Bank',
                'Deadline',
                'Good' => ['fields' => ['Good.id', 'Good.quantita', 'Good.prezzo', 'Good.iva_id', 'Good.discount'], 'Iva'],
                'Iva' => ['fields' => ['Iva.id', 'Iva.percentuale', 'Iva.descrizione']],
                'Payment' => 'Bank'], 'conditions' => $conditionArray]);

        return $bills;
    }

    // Salvo good per lo fattura
    public function createBillGood($billId, $good)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Good->Create();
        $good['Good']['bill_id'] = $billId;
        $good['Good']['oggetto'] = $good['oggetto'];
        isset($good['customdescription']) ? $good['Good']['customdescription'] = $good['customdescription'] : $good['Good']['customdescription'] = null;
        $good['Good']['prezzo'] = $good['prezzo'];
        $good['Good']['quantita'] = $good['quantita'];
        isset($good['storage_id']) ? $good['Good']['storage_id'] = $good['storage_id'] : $good['Good']['storage_id'] = null; // Id di magazzino
        isset($good['iva_id']) ? $good['Good']['iva_id'] = $good['iva_id'] : $good['Good']['iva_id'] = 0;
        isset($good['discount']) ? $good['Good']['discount'] = $good['discount'] : $good['Good']['discount'] = 0;
//			isset($good['withholding_tax']) ?  $good['Good']['withholding_tax'] = $good['withholding_tax'] : $good['Good']['withholding_tax'] =0 ; // Ritenuta d'acconto
        isset($good['codice']) ? $good['Good']['codice'] = $good['codice'] : $good['Good']['codice'] = '';  // Fixato
        isset($good['unita']) ? $good['Good']['unita'] = $good['unita'] : '';
        isset($good['vat']) ? $good['Good']['vat'] = $good['vat'] : '';
        isset($good['transport_id']) ? $good['Good']['transport_id'] = $good['transport_id'] : null;
        isset($good['withholdingapplied']) ? $good['Good']['withholdingapplied'] = $good['withholdingapplied'] : 1;
        if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
            if(isset($good['complimentaryQuantity']))
                $good['Good']['complimentaryQuantity'] = $good['complimentaryQuantity'];
        }
        if (MULTICURRENCY) {
            isset($good['currencyprice']) ? $good['Good']['currencyprice'] = $good['currencyprice'] : null;
        }

        $good['Good']['company_id'] = MYCOMPANY;
        return $this->Good->save($good['Good']);
    }

    public function createOrderRow($orderId, $row)
    {

        $this->Orderrow = ClassRegistry::init('Orderrow');
        $this->Orderrow->Create();
        $row['Orderrow']['order_id'] = $orderId;
        $row['Orderrow']['oggetto'] = $row['oggetto'];
        $row['Orderrow']['prezzo'] = $row['prezzo'];
        $row['Orderrow']['importo'] = $row['prezzo'] * $row['quantita'];
        $row['Orderrow']['quantita'] = $row['quantita'];
        $row['Orderrow']['company_id'] = MYCOMPANY;
        $row['Orderrow']['state'] = ATTIVO;
        $row['Orderrow']['complimentaryQuantity'] = $row['complimentaryQuantity'];
        isset($row['storage_id']) ? $row['Orderrow']['storage_id'] = $row['storage_id'] : $row['Orderrow']['storage_id'] = null; // Id di magazzino
        isset($row['iva_id']) ? $row['Orderrow']['iva_id'] = $row['iva_id'] : $row['Orderrow']['iva_id'] = 0;
        isset($row['discount']) ? $row['Orderrow']['discount'] = $row['discount'] : $row['Orderrow']['discount'] = 0;
        isset($row['unita']) ? $row['Orderrow']['unita'] = $row['unita'] : '';

        $row['Orderrow']['company_id'] = MYCOMPANY;
        return $this->Orderrow->save($row['Orderrow']);
    }

    public function calculateOrderRowWithDiscount($rowQuantity, $rowPrice, $discount)
    {
        return $this->calculateRowPrice($rowQuantity, $rowPrice) * (1 - $discount / 100);
    }

    public function createBuyBillForSupplier($supplierId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $NewBill = $this->Bill->Create();
        $NewBill['Bill']['state'] = 1;
        $NewBill['Bill']['date'] = date('Y-m-d');
        $NewBill['Bill']['supplier_id'] = $supplierId;
        $NewBill['Bill']['numero_fattura'] = $this->getNextBillNumber(date('Y'), 2); // Fattura d'acqusto
        $NewBill['Bill']['company_id'] = MYCOMPANY;
        $NewBill['Bill']['tipologia'] = 2; // Fattura d'acquisto
        return $this->Bill->save($NewBill);
    }


    public function createSellBillFromQuote($clientId, $quoteId)
    {
        // Restituisce la fattura creata
        return $this->createSellBillForClient($clientId, $quoteId);
    }

    public function createSellBillForClient($clientId, $quoteId = null)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Client = ClassRegistry::init('Client');
        $this->Currencies = ClassRegistry::init('Currencies');

        $client = $this->Client->find('first', ['conditions' => ['Client.id' => $clientId]]);

        $NewBill = $this->Bill->Create();

        $NewBill['Bill']['state'] = 1;
        $NewBill['Bill']['date'] = date('Y-m-d');
        $NewBill['Bill']['client_id'] = $clientId;

        // Utilizzo i sezionali - Per fatture di vendita
        $defaultSectional = $this->getDefaultBillSectional();
        $this->set('defaultSectional', $defaultSectional);
        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $NewBill['Bill']['numero_fattura'] = $nextSectionalNumber;
        $this->set('nextBillNumber', $nextSectionalNumber);

        $NewBill['Bill']['sectional_id'] = $defaultSectional['Sectionals']['id'];

        $NewBill['Bill']['company_id'] = MYCOMPANY;
        $NewBill['Bill']['tipologia'] = 1; // Fattura d'acquisto

        $quoteId != null ? $NewBill['Bill']['quote_id'] = $quoteId : null;

        // Controllo se esiste il cliente
        if (isset($client['Client'])) {
            $NewBill['Bill']['client_name'] = $client['Client']['ragionesociale'];
            $NewBill['Bill']['client_address'] = $client['Client']['indirizzo'];
            $NewBill['Bill']['client_cap'] = $client['Client']['cap'];
            $NewBill['Bill']['client_city'] = $client['Client']['citta'];
            $NewBill['Bill']['client_province'] = $client['Client']['provincia'];
            isset ($client['Client']['nation_id']) ? $NewBill['Bill']['client_nation'] = $client['Client']['nation_id'] : $NewBill['Bill']['client_nation'] = '';
            // Aggiunta x la gestione dell valute
            $NewBill['Bill']['currency_id'] = $client['Client']['currency_id'];
            if ($client['Client']['currency_id'] != 0 && $this->Currencies->GetCurrencyName($client['Client']['currency_id']) != 'EUR') {
                $NewBill['Bill']['changevalue'] = $this->Currencies->GetLastCurrencyChange($client['Client']['currency_id']);
            } else {
                $NewBill['Bill']['changevalue'] = 1;
            }
        } else {
            $NewBill['Bill']['client_nation'] = '';
        }

        $this->updateDefaultBillSectional($NewBill['Bill']['numero_fattura']);

        return $this->Bill->save($NewBill);
    }

    // Tengo anche il companyId perché viene generata anche quando si è offline.
    public function getBillTransports($billId, $companyId)
    {
        $this->Transports = ClassRegistry::init('Transports');
        $transports = $this->Transports->find('all', ['conditions' => ['Transports.company_id' => $companyId, 'Transports.bill_id' => $billId]]);
        return $transports;
    }


    public function getBillChange($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $bill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.state' => ATTIVO, 'Bill.company_Id' => MYCOMPANY]]);
        if (isset($bill['Bill']['changevalue'])) {
            return $bill['Bill']['changevalue'];
        } else {
            return 1;
        }
    }

    // Restituiscde i totali della fattura
    public function getBillValues($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bill = ClassRegistry::init('Bill');
        $goods = $this->Good->find('all', ['conditions' => ['bill_id' => $billId], 'contain' => ['Units', 'Iva']]);
        $bill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);
        $billChange = $this->getBillChange($billId);

        $totale['imponibile'] = $totale['totale'] = $totale['iva'] = $totale['withholding_tax'] = 0;

        foreach ($goods as $articolo)
        {
            if (MULTICURRENCY && isset($articolo['Good']['currencyprice']) && $articolo['Good']['currencyprice'] != null)
                $totaleRiga = number_format($articolo['Good']['currencyprice'], 4, '.', '') * $articolo['Good']['quantita'];
            else
                $totaleRiga = number_format($articolo['Good']['prezzo'], 4, '.', '') * $articolo['Good']['quantita'];

            $totaleRigaScontato = $totaleRiga * (1 - $articolo['Good']['discount'] / 100);

            if ($articolo['Iva']['percentuale'])
                $totale['iva'] += $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100;
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                if($articolo['Good']['complimentaryQuantity'] > 0){
                    $totale['imponibile'] += number_format($articolo['Good']['prezzo'], 4, '.', '') * ($articolo['Good']['quantita'] - $articolo['Good']['complimentaryQuantity']);
                }else{
                    $totale['imponibile'] += number_format($totaleRigaScontato, 4, '.', '');
                }
            }else{
                $totale['imponibile'] += number_format($totaleRigaScontato, 4, '.', '');
            }
        }

        // Aggiungo spese d'incasso
        if (isset($bill['Bill']['collectionfees']) && $bill['Bill']['collectionfees'] > 0)
        {
            $totale['imponibile'] += number_format($bill['Bill']['collectionfees'], 4, '.', '');
            $totale['iva'] += number_format($bill['Bill']['collectionfees'] * $this->getVatPercentageFromId($bill['Bill']['collectionfeesvatid']) / 100, 4, '.', '');
        }

        $totale['iva'] = number_format($totale['iva'], 4, '.', '');
        $totale['totale'] = $totale['imponibile'] + $totale['iva'];

        return $totale;
    }

    public function getOrderValues($orderId)
    {
        $this->Orderrow = ClassRegistry::init('Orderrow');
        $this->Order = ClassRegistry::init('Order');
        $rows = $this->Orderrow->find('all', ['conditions' => ['order_id' => $orderId, 'Orderrow.state'=>ATTIVO], 'contain' => ['Units', 'Iva']]);
        $order = $this->Order->find('first', ['conditions' => ['Order.id' => $orderId, 'Order.company_id' => MYCOMPANY, "Order.state"=>ATTIVO]]);

        $totale['imponibile'] = $totale['totale'] = $totale['iva'] = $totale['withholding_tax'] = 0;

        foreach ($rows as $articolo) {
            $totaleRiga = number_format($articolo['Orderrow']['prezzo'], 4, '.', '') * ($articolo['Orderrow']['quantita']);

            $totaleRigaScontato = $totaleRiga * (1 - $articolo['Orderrow']['discount'] / 100);

            $totale['imponibile'] += number_format(($totaleRigaScontato - ($articolo['Orderrow']['complimentaryQuantity'] * $articolo['Orderrow']['prezzo'])), 2, '.', '');

            if ($articolo['Iva']['percentuale'])
                $totale['iva'] += $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100;
        }


        $totale['iva'] = number_format($totale['iva'], 4, '.', '');
        $totale['totale'] = $totale['imponibile'] + $totale['iva'];

        return $totale;
    }

    public function getVatPassiveBillValue($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bill = ClassRegistry::init('Bill');

        $goods = $this->Good->find('all', ['conditions' => ['bill_id' => $billId], 'contain' => ['Units', 'Iva' => ['Einvoicevatnature']]]);
        $bill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);

        foreach ($goods as $articolo) {
            ($articolo['Good']['quantita'] == null || $articolo['Good']['quantita'] == '') ? $articolo['Good']['quantita'] = 1 : null;

            if (MULTICURRENCY && isset($articolo['Good']['currencyprice']) && $articolo['Good']['currencyprice'] != null) {
                $totaleRiga = $articolo['Good']['currencyprice'] * $articolo['Good']['quantita'];
            } else {
                $totaleRiga = $articolo['Good']['prezzo'] * $articolo['Good']['quantita'];
            }


            $totaleRigaScontato = $totaleRiga * (1 - $articolo['Good']['discount'] / 100);


            $arrayVats[$articolo['Iva']['id']]['percentuale'] = $articolo['Iva']['percentuale'];
            isset($arrayVats[$articolo['Iva']['id']]['imponibile']) ? $arrayVats[$articolo['Iva']['id']]['imponibile'] += $totaleRigaScontato : $arrayVats[$articolo['Iva']['id']]['imponibile'] = $totaleRigaScontato;
            isset($arrayVats[$articolo['Iva']['id']]['vat']) ? $arrayVats[$articolo['Iva']['id']]['vat'] += $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100 : $arrayVats[$articolo['Iva']['id']]['vat'] = $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100;
            isset($articolo['Iva']['Einvoicevatnature']['code']) ? $arrayVats[$articolo['Iva']['id']]['codice'] = $articolo['Iva']['Einvoicevatnature']['code'] : $arrayVats[$articolo['Iva']['id']]['codice'] = null;
            isset($articolo['Iva']['Einvoicevatnature']['description']) ? $arrayVats[$articolo['Iva']['id']]['description'] = $articolo['Iva']['Einvoicevatnature']['description'] : $arrayVats[$articolo['Iva']['id']]['description'] = null;
        }

        // Aggiungo l'iva delle spese fisse
        if (isset($bill['Bill']['collectionfees']) && $bill['Bill']['collectionfees'] > 0 && $bill['Bill']['collectionfeesvatid'] > 0) {
            $collectionVat = $this->getVatFromId($bill['Bill']['collectionfeesvatid']);
            $collectionVatInvoiceNature = $this->getEinvoiceVatNatureCodeFromId($collectionVat['Ivas']['einvoicenature_id']);
            $collectionVatInvoiceDescription = $this->getEinvoiceVatDescriptionFromId($collectionVat['Ivas']['einvoicenature_id']);
            $arrayVats[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $collectionVat['Ivas']['percentuale'];
            isset($arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile'] += $bill['Bill']['collectionfees'] : $arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile'] = $bill['Bill']['collectionfees'];
            isset($arrayVats[$bill['Bill']['collectionfeesvatid']]['vat']) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['vat'] += $bill['Bill']['collectionfees'] * $collectionVat['Ivas']['percentuale'] / 100 : $arrayVats[$bill['Bill']['collectionfeesvatid']]['vat'] = $bill['Bill']['collectionfees'] * $collectionVat['Ivas']['percentuale'] / 100;
            isset($collectionVatInvoiceNature) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['codice'] = $collectionVatInvoiceNature : $arrayVats[$bill['Bill']['collectionfeesvatid']]['codice'] = null;
            isset($collectionVatInvoiceDescription) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['description'] = $collectionVatInvoiceDescription : $arrayVats[$bill['Bill']['collectionfeesvatid']]['description'] = null;
        }

        return $arrayVats;
    }


    // Restituisce l'iva della fattura divisa per aliquota
    public function getVatValues($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bill = ClassRegistry::init('Bill');

        $goods = $this->Good->find('all', ['conditions' => ['bill_id' => $billId], 'contain' => ['Units', 'Iva' => ['Einvoicevatnature']]]);
        $bill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);

        foreach ($goods as $articolo) {
            // Se non è definita la quantità la setto a 1 per l'importazione delle fatture passive potrebbe essere estesa
            if ($passiveBill = true) {
                ($articolo['Good']['quantita'] == null || $articolo['Good']['quantita'] == '') ? $articolo['Good']['quantita'] = 1 : null;
            }

            if (MULTICURRENCY && isset($articolo['Good']['currencyprice']) && $articolo['Good']['currencyprice'] != null) {
                $totaleRiga = $articolo['Good']['currencyprice'] * $articolo['Good']['quantita'];
            } else {
                $totaleRiga = $articolo['Good']['prezzo'] * $articolo['Good']['quantita'];
            }

                $totaleRigaScontato = $totaleRiga * (1 - $articolo['Good']['discount'] / 100);



            $arrayVats[$articolo['Iva']['id']]['percentuale'] = $articolo['Iva']['percentuale'];
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0047"){
                if($articolo['Good']['complimentaryQuantity'] > 0) {
                    isset($arrayVats[0]['imponibile']) ? $arrayVats[0]['imponibile'] += $articolo['Good']['prezzo'] * $articolo['Good']['complimentaryQuantity'] * -1 : $arrayVats[0]['imponibile'] = $articolo['Good']['prezzo'] * $articolo['Good']['complimentaryQuantity'] * -1;
                    $arrayVats[0]['vat'] = 0;
                    $arrayVats[0]['percentuale'] = 0;
                    $arrayVats[0]['codice'] = "N2.2";
                }
            }
            isset($arrayVats[$articolo['Iva']['id']]['imponibile']) ? $arrayVats[$articolo['Iva']['id']]['imponibile'] += $totaleRigaScontato : $arrayVats[$articolo['Iva']['id']]['imponibile'] = $totaleRigaScontato;
            isset($arrayVats[$articolo['Iva']['id']]['vat']) ? $arrayVats[$articolo['Iva']['id']]['vat'] += $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100 : $arrayVats[$articolo['Iva']['id']]['vat'] = $totaleRigaScontato * $articolo['Iva']['percentuale'] / 100;
            isset($articolo['Iva']['Einvoicevatnature']['code']) ? $arrayVats[$articolo['Iva']['id']]['codice'] = $articolo['Iva']['Einvoicevatnature']['code'] : $arrayVats[$articolo['Iva']['id']]['codice'] = null;
            isset($articolo['Iva']['Einvoicevatnature']['description']) ? $arrayVats[$articolo['Iva']['id']]['description'] = $articolo['Iva']['Einvoicevatnature']['description'] : $arrayVats[$articolo['Iva']['id']]['description'] = null;
        }

        // Aggiungo l'iva delle spese fisse
        if (isset($bill['Bill']['collectionfees']) && $bill['Bill']['collectionfees'] > 0 && $bill['Bill']['collectionfeesvatid'] > 0) {
            $collectionVat = $this->getVatFromId($bill['Bill']['collectionfeesvatid']);
            $collectionVatInvoiceNature = $this->getEinvoiceVatNatureCodeFromId($collectionVat['Ivas']['einvoicenature_id']);
            $collectionVatInvoiceDescription = $this->getEinvoiceVatDescriptionFromId($collectionVat['Ivas']['einvoicenature_id']);
            $arrayVats[$bill['Bill']['collectionfeesvatid']]['percentuale'] = $collectionVat['Ivas']['percentuale'];
            isset($arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile']) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile'] += $bill['Bill']['collectionfees'] : $arrayVats[$bill['Bill']['collectionfeesvatid']]['imponibile'] = $bill['Bill']['collectionfees'];
            isset($arrayVats[$bill['Bill']['collectionfeesvatid']]['vat']) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['vat'] += $bill['Bill']['collectionfees'] * $collectionVat['Ivas']['percentuale'] / 100 : $arrayVats[$bill['Bill']['collectionfeesvatid']]['vat'] = $bill['Bill']['collectionfees'] * $collectionVat['Ivas']['percentuale'] / 100;
            isset($collectionVatInvoiceNature) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['codice'] = $collectionVatInvoiceNature : $arrayVats[$bill['Bill']['collectionfeesvatid']]['codice'] = null;
            isset($collectionVatInvoiceDescription) ? $arrayVats[$bill['Bill']['collectionfeesvatid']]['description'] = $collectionVatInvoiceDescription : $arrayVats[$bill['Bill']['collectionfeesvatid']]['description'] = null;

        }
        return $arrayVats;
    }


    public function getWelfareBoxTotal($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        // Recupero la cifra su cui deve essere calcolata la cassa
        // if($firstBill['Bill']['welfare_box_code'] != null && $firstBill['Bill']['welfare_box_percentage'] > 0)
        if ($firstBill['Bill']['welfare_box_percentage'] > 0) {
            return number_format($this->getBillTaxable($billId) * $firstBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
        } else {
            return 0;
        }
    }


    public function getWelfareBoxWithholdingTaxTotal($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);

        // Recupero la cifra su cui deve essere calcolata la cassa
        //if($firstBill['Bill']['welfare_box_code'] != null && $firstBill['Bill']['welfare_box_percentage'] > 0 && $firstBill['Bill']['welfare_box_withholding_appliance'] == 1)
        if ($firstBill['Bill']['welfare_box_percentage'] > 0 && $firstBill['Bill']['welfare_box_withholding_appliance'] == 1) {
            return number_format($this->getWelfareBoxTotal($billId) * $firstBill['Bill']['withholding_tax'] / 100, 2, '.', '');
        } else {
            return 0;
        }
    }

    public function stornoImportoWelfareBox($billId)
    {
        //   $this->Setting = ClassRegistry::init('Setting');
        //   $settings = $this->Setting->GetMySettings();
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        return number_format($this->stornoImponibileWithholdingTax($billId) * $firstBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
    }

    public function stornoImponibileWithholdingTax($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        //$this->Bill = ClassRegistry::init('Bill');
        // Recupero solo gli articolo che non hanno ritenuta d'acconto
        $goods = $this->Good->find('all', ['conditions' => ['Good.company_id' => MYCOMPANY, 'Good.bill_id' => $billId, 'Good.withholdingapplied' => 0]]);
        //$firstBill = $this->Bill->find('first',['conditions'=>['Bill.id' => $billId,'Bill.company_id'=>MYCOMPANY]]);
        $stornoWithholdingTaxableAmount = 0;
        foreach ($goods as $good) {
            $stornoWithholdingTaxableAmount += $good['Good']['quantita'] * $good['Good']['prezzo'];
        }
        return $stornoWithholdingTaxableAmount;
    }

    public function stornoImportoWitholdingTax($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bill = ClassRegistry::init('Bill');
        // Recupero solo gli articolo che non hanno ritenuta d'acconto
        $goods = $this->Good->find('all', ['conditions' => ['Good.company_id' => MYCOMPANY, 'Good.bill_id' => $billId, 'Good.withholdingapplied' => 0]]);
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);
        $stornoWithholdingAount = 0;
        foreach ($goods as $good) {
            $stornoWithholdingAount += $good['Good']['quantita'] * $good['Good']['prezzo'];
        }
        return $stornoWithholdingAount * $firstBill['Bill']['withholding_tax'] / 100;
    }

    public function getWitholdingTaxTotal($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        $taxableIncome = $this->getBillTaxable($billId);
        // Aggiungo cassa
        $welfareBoxWithholdingTax = $this->getWelfareBoxWithholdingTaxTotal($billId);
        return number_format($taxableIncome * $firstBill['Bill']['withholding_tax'] / 100 + $welfareBoxWithholdingTax, 2, '.', '');
    }

    public function getBillTaxable($billId)
    {
        $values = $this->getBillValues($billId);
        return $values['imponibile'];
    }

    public function getReceiptTaxable($billId)
    {
        $values = $this->getReceiptValues($billId);
        return $values['imponibile'];
    }

    public function getOrderTaxable($orderId)
    {
        $values = $this->getOrderValues($orderId);
        return $values['imponibile'];
    }
    public function getOrderTotal($orderId)
    {
        // Attualmente non ho previsto il filtro company id
        $values = $this->getOrderValues($orderId);
        return $values['totale'];
    }

    public function getBillVat($billId)
    {
        // Attualmente non ho previsto il filtro company id
        $values = $this->getBillValues($billId);
        return $values['iva'];
    }

    public function getReceiptVat($billId)
    {
        // Attualmente non ho previsto il filtro company id
        $values = $this->getReceiptValues($billId);
        return $values['iva'];
    }

    public function getBillTotal($billId)
    {
        // Attualmente non ho previsto il filtro company id
        $values = $this->getBillValues($billId);
        return $values['totale'];
    }

    public function getReceiptTotal($billId)
    {
        // Attualmente non ho previsto il filtro company id
        $values = $this->getReceiptValues($billId);
        return $values['totale'];
    }

    public function getReceiptValues($billId)
    {
        $this->Good = ClassRegistry::init('Good');
        $goods = $this->Good->find('all', ['conditions' => ['bill_id' => $billId], 'contain' => ['Units', 'Iva']]);
        $totale['imponibile'] = $totale['totale'] = $totale['iva'] = 0;

        foreach ($goods as $articolo)
        {
            if (MULTICURRENCY && isset($articolo['Good']['currencyprice']) && $articolo['Good']['currencyprice'] != null)
                $totaleRiga = number_format($articolo['Good']['currencyprice'], 4, '.', '') * $articolo['Good']['quantita'];
            else
                $totaleRiga = number_format($articolo['Good']['prezzo'], 2, '.', '') * $articolo['Good']['quantita'];
            $totale['imponibile'] += number_format($totaleRiga, 2, '.', '');
            if ($articolo['Good']['vat'] != null)
                $totale['iva'] += $articolo['Good']['vat'] * $articolo['Good']['quantita'];

        }

        $totale['totale'] = $totale['imponibile'] + $totale['iva'];

        return $totale;
    }

    public function getBillTotalToPay($billId)
    {
        $total = $this->getBillTotal($billId);
        $vat = $this->getBillVat($billId);
        $withHoldingTax = $this->getWitholdingTaxTotal($billId);
        // $collectionFees = $this->getBillCollectionFees($billId);
        $seal = $this->getBillSeal($billId);

        if ($this->isSplitPayment(($billId))) {
            return $total - $vat - $withHoldingTax + $seal;
        } else {
            return $total - $withHoldingTax + $seal;
        }
    }

    public function getBillSeal($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        if (strtotime(date("Y-m-d", strtotime($firstBill['Bill']['date']))) < strtotime(date('2019-01-01'))) {
            return $firstBill['Bill']['seal'];
        } else {
            return 0;
        }
    }

    public function getBillCollectionfees($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        return $firstBill['Bill']['collectionfees'];
    }

    public function isSplitPayment($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $firstBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId]]);
        if ($firstBill['Bill']['split_payment'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function setBillPayed($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        // Setto la fattura come pagata
        $this->Bill->updateAll(['Bill.pagato' => 1], ['Bill.id' => $billId]);
    }

    public function setBillMailSent($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        // Setto la fattura come pagata
        $this->Bill->updateAll(['Bill.mailsent' => 1], ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO]);
    }

    public function setBillMailNotSent($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        // Setto la fattura come pagata
        $this->Bill->updateAll(['Bill.mailsent' => 0], ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO]);
    }

    public function SetRibaEmitted($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Bill->updateAll(['Bill.ribaemitted' => 1], ['Bill.id' => $billId]);
    }

    public function setBillGoodValues($oldGood, $newbillId, $good)
    {
        $this->Good = ClassRegistry::init('Good');

        $oldGood['Good']['bill_id'] = $newbillId;
        $oldGood['Good']['oggetto'] = $good['oggetto'];
        $oldGood['Good']['prezzo'] = $good['prezzo'];
        $oldGood['Good']['quantita'] = $good['quantita'];
        // $oldGood['Good']['unita'] = $good['unit_id'];
        // isset($good['unita']) ? $good['Good']['unita'] = $good['unita'] :$good['Good']['unita'] = $good['unit_id'];
        isset($good['unita']) ? $oldGood['Good']['unita'] = $good['unita'] : $oldGood['Good']['unita'] = '';
        isset($good['codice']) ? $oldGood['Good']['codice'] = $good['codice'] : $oldGood['Good']['codice'] = '';  // Fixato
        isset($good['discount']) ? $oldGood['Good']['discount'] = $good['discount'] : $oldGood['Good']['discount'] = 0;
        isset($good['iva_id']) ? $oldGood['Good']['iva_id'] = $good['iva_id'] : $oldGood['Good']['iva_id'] = 0;
        isset($good['vat']) ? $oldGood['Good']['vat'] = $good['vat'] : $oldGood['Good']['vat'] = 0; // Per gli scontrini
//		isset( $good['withholding_tax']) ?  $oldGood['Good']['withholding_tax'] = $good['withholding_tax'] : $oldGood['Good']['withholding_tax'] = 0 ;
        isset($good['movable']) ? $oldGood['Good']['movable'] = $good['movable'] : $oldGood['Good']['movable'] = 0; // Per i movimentabili
        return $this->Good->save($oldGood);
    }


    // Scadenze con l'iva
    public function setDeadlinesWithVat($billDate, $paymentId, $taxableIncome, $vat, $newbillId, $oldDeadlinesBillId = 0, $enableAgustDecember = false)
    {

        if (isset($billDate) && isset($paymentId)) {
            $deadlineTaxableIncome = $this->calculateDeadlines($billDate, $paymentId, $taxableIncome, $enableAgustDecember);
            $deadlineVat = $this->calculateDeadlines($billDate, $paymentId, $vat, $enableAgustDecember);
        }

        // Se è definito l'id della vecchia fattua allora elimino le scadenze vecchie
        $oldDeadlinesBillId != 0 ? $this->deleteDeadlines($oldDeadlinesBillId) : null;

        if (isset($deadlineTaxableIncome)) {
            foreach ($deadlineTaxableIncome as $key => $deadline) {
                $this->createDeadline($newbillId, $deadline['deadline'], $deadline['amount'], $deadlineVat[$key]['amount']);
            }
        }
    }

    // Totale
    public function setDeadlines($billDate, $paymentId, $total, $newbillId, $oldDeadlinesBillId = 0, $enableAgustDecember)
    {
        if (isset($billDate) && isset($paymentId)) {
            $deadlines = $this->calculateDeadlines($billDate, $paymentId, $total, $enableAgustDecember);
        }

        // Se è definito l'id della vecchia fattua allora elimino le scadenze vecchie
        $oldDeadlinesBillId != 0 ? $this->deleteDeadlines($oldDeadlinesBillId) : null;

        if (isset($deadlines)) {
            foreach ($deadlines as $deadline) {
                $this->createDeadline($newbillId, $deadline['deadline'], $deadline['amount'], 0);
            }
        }
    }

    // Crea le scadenze della fattura
    public function createDeadline($billId, $deadline, $amount, $vat)
    {
        $this->Deadline = ClassRegistry::init('Deadline');
        $newDeadline = $this->Deadline->create();
        $newDeadline['Deadline']['bill_id'] = $billId;
        $newDeadline['Deadline']['deadline'] = $deadline;
        $newDeadline['Deadline']['amount'] = $amount;
        $newDeadline['Deadline']['original_amount'] = $amount;
        $newDeadline['Deadline']['vat'] = $vat;
        $newDeadline['Deadline']['state'] = ATTIVO;
        $newDeadline['Deadline']['company_id'] = MYCOMPANY;
        $newDeadline['Deadline']['unpaid'] = 0;
        $this->Deadline->save($newDeadline);
    }

    // Eliminazione scadenze di una fattura
    public function deleteDeadlines($billId)
    {
        $this->Deadline = ClassRegistry::init('Deadline');
        $this->Deadlinepayment = ClassRegistry::init('Deadlinepayment');

        $deadlines = $this->Deadline->find('all', ['conditions' => ['bill_id' => $billId]]);

        // Cancello tutti i pagamenti effettuati per quella fattura (non dovrebbe succedere però)
        foreach ($deadlines as $deadline) {

            $this->Deadlinepayment->deleteAll(['deadline_id' => $deadline['Deadline']['id']]);
        }

        $this->Deadline->deleteAll(['bill_id' => $billId]);
    }

    // Calcola un'elenco di scadenze dato il metodo di pagamento e la data dell'effetto
    public function calculateDeadlines($effectDate, $paymentMethodId, $importoTotale, $enableAgustDecember = false)
    {
        $this->Paymentdeadlines = ClassRegistry::init('Paymentdeadlines');
        $this->Payments = ClassRegistry::init('Payments');

        $fields =
            [
                'Payments.endofmonth', // Per calcolo fine mese
                'Payments.next_month_day', // Calcolo giorno del mese successivo
                'payments_deadlines.deadlineday',    // Per calcolare le scadenze
            ];

        $paymentdeadlines = $this->Payments->find('all', [
            'joins' => [[
                'table' => 'payments_deadlines',
                'type' => 'INNER',
                'conditions' => [
                    'payments_deadlines.payment_id = Payments.id'
                ]
            ]
            ],
            'conditions' => ['payments_deadlines.payment_id' => $paymentMethodId],
            'fields' => $fields,
        ]);

        if (count($paymentdeadlines) > 0) {
            $rataParziale = round($importoTotale / count($paymentdeadlines), 2);
        } else {
            $rataParziale = $importoTotale;
        }

        //$rataParziale = round($importoTotale / count($paymentdeadlines), 2);

        $scadenza = [];
        $i = -1;
        foreach ($paymentdeadlines as $deadline) {

            $fineMese = $deadline['Payments']['endofmonth'];
            $giorniScadenza = $deadline['payments_deadlines']['deadlineday'];

            // Gestione del fm + day
            $giornoDelMeseDopo = $deadline['Payments']['next_month_day'];

            $i++;

            // Se è selezionato "fine mese" prendo l'ultimo giorno del mese
            if ($fineMese) {
                // Aggiungo x mesi e preno dil fine mese
                $month = $deadline['payments_deadlines']['deadlineday'] / 30;

                $scadenza[$i]['deadline'] = date("Y-m-01", strtotime($effectDate));
                $scadenza[$i]['deadline'] = date('Y-m-d', strtotime($scadenza[$i]['deadline'] . ' + ' . $month . ' month'));
                $scadenza[$i]['deadline'] = date('Y-m-t', strtotime($scadenza[$i]['deadline']));

                // Aggiungo le scadenze x giorni dopo
                $scadenza[$i]['deadline'] = date('Y-m-d', strtotime($scadenza[$i]['deadline'] . ' + ' . $giornoDelMeseDopo . ' day'));
            } else {
                // Calcolo i giorni della i-esima scadenza
                $scadenza[$i]['deadline'] = date('Y-m-d', strtotime($effectDate . ' + ' . $deadline['payments_deadlines']['deadlineday'] . ' days'));
            }

            // Se abilitato il flag posticipa scadenze 31/08 e 31/12
            if ($enableAgustDecember) {
                $scadenza[$i]['deadline'] = str_replace('08-31', '09-10', $scadenza[$i]['deadline']);
                if (substr($scadenza[$i]['deadline'], 5, 5) == '12-31') {
                    $scadenza[$i]['deadline'] = str_replace('12-31', '01-10', $scadenza[$i]['deadline']);
                    $scadenza[$i]['deadline'] = str_replace(substr($scadenza[$i]['deadline'], 0, 4), substr($scadenza[$i]['deadline'], 0, 4) + 1, $scadenza[$i]['deadline']);
                }
            }

            $i == count($paymentdeadlines) - 1 ? $scadenza[$i]['amount'] = $importoTotale : $scadenza[$i]['amount'] = $rataParziale;

            $importoTotale = $importoTotale - $rataParziale;
        }

        return ($scadenza);
    }

    public function getCurrentBillStorageQuantity($billId, $storageId)
    {
        $this->Good = ClassRegistry::init('Good');
        $quantity = $this->Good->find('first', ['conditions' => ['Good.bill_id' => $billId, 'Good.storage_id' => $storageId], 'fields' => ['Good.quantita']]);
        isset($quantity['Good']['quantita']) && $quantity['Good']['quantita'] > 0 ? $quantity = $quantity['Good']['quantita'] : $quantity = 0;
        return $quantity;
    }

    /** FINE FATTURA **/

    /** SEZIONALI **/

    public function getNextSectionalNumber($sectionalId)
    {
        $this->Sectional = ClassRegistry::init('Sectional');
        $maxNumber = $this->Sectional->find('all', ['fields' => ['MAX(last_number) as numerosezionale'], 'conditions' => ['Sectional.id' => $sectionalId]]);
        $maxNumber = $maxNumber[0][0]['numerosezionale'];
        return ++$maxNumber;
    }

    public function getBillSectionalList()
    {
        return $this->getSectionalList('Bill');
    }

    public function getOrderSectionalList()
    {
        return $this->getSectionalList('Order');
    }

    public function getProformaSectionalList()
    {
        return $this->getSectionalList('Proforma');
    }

    public function getCreditnoteSectionalList()
    {
        return $this->getSectionalList('Creditnote');
    }

    public function getTransportSectionalList()
    {
        return $this->getSectionalList('Transport');
    }

    public function isSectionalUsed($id)
    {
        $this->Sectional = ClassRegistry::init('Sectional');
        $this->Transport = ClassRegistry::init('Transport');
        $this->Bill = ClassRegistry::init('Bill');

        $currentSectional = $this->Sectional->find('first', ['conditions' => ['Sectional.id' => $id, 'Sectional.company_id' => MYCOMPANY]]);

        $typeOfSectional = '';

        if ($currentSectional['Sectional']['default_bill'] == 1 || $currentSectional['Sectional']['bill_sectional'] == 1)
            $typeOfSectional = 'bill';

        if ($currentSectional['Sectional']['default_proforma'] == 1 || $currentSectional['Sectional']['proforma_sectional'] == 1)
            $typeOfSectional = 'bill';

        if ($currentSectional['Sectional']['default_creditnote'] == 1 || $currentSectional['Sectional']['creditnote_sectional'] == 1)
            $typeOfSectional = 'bill';

        if ($currentSectional['Sectional']['default_transport'] == 1 || $currentSectional['Sectional']['transport_sectional'] == 1)
            $typeOfSectional = 'transport';

        $counted = 0;

        switch ($typeOfSectional)
        {
            case 'bill':
                $counted = $this->Bill->find('count', ['conditions' => ['Bill.company_id' => MYCOMPANY, 'Bill.sectional_id' => $id, 'Bill.state' => ATTIVO]]);
                break;
            case 'transport':
                $counted = $this->Transport->find('count', ['conditions' => ['Transport.company_id' => MYCOMPANY, 'Transport.sectional_id' => $id, 'Transport.state' => ATTIVO]]);
                break;
        }

        return $counted;
    }

    public function getSectionalList($documentType = null)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');

        switch ($documentType)
        {
            case 'Bill':
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.transport_sectional' => 0, 'Sectionals.creditnote_sectional' => 0, 'Sectionals.proforma_sectional' => 0], 'order' => ['Sectionals.description' => 'asc']]);
                break;
            case 'Order':
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.order_sectional' => 1], 'order' => ['Sectionals.description' => 'asc']]);
                break;
            case 'Proforma':
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.transport_sectional' => 0, 'Sectionals.bill_sectional' => 0, 'Sectionals.creditnote_sectional' => 0], 'order' => ['Sectionals.description' => 'asc']]);
                break;
            case 'Creditnote':
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.transport_sectional' => 0, 'Sectionals.bill_sectional' => 0, 'Sectionals.proforma_sectional' => 0], 'order' => ['Sectionals.description' => 'asc']]);
                break;
            case 'Transport':
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.creditnote_sectional' => 0, 'Sectionals.bill_sectional' => 0, 'Sectionals.proforma_sectional' => 0], 'order' => ['Sectionals.description' => 'asc']]);
                break;
            case null:
                $sectionals = $this->Sectionals->find('list', ['fields' => ['id', 'description'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO], 'order' => ['Sectionals.description' => 'asc']]);
                break;
        }

        return $sectionals;
    }

    public function getDefaultBillSectional()
    {
        return $this->getDefaultSectional('Bill');
    }

    public function getDefaultOrderSectional()
    {
        return $this->getDefaultSectional('Ordine');
    }

    public function getDefaultCreditnoteSectional()
    {
        return $this->getDefaultSectional('Creditnote');
    }

    public function getDefaultProformaSectional()
    {
        return $this->getDefaultSectional('Proforma');
    }

    public function getDefaultTransportSectional()
    {
        return $this->getDefaultSectional('ddt');
    }

    public function getDefaultSectional($documentType)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');

        switch ($documentType)
        {
            case 'Bill':
                return $this->Sectionals->find('first', ['fields' => ['id', 'last_number'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.default_bill' => 1]]);
            case 'Proforma':
                return $this->Sectionals->find('first', ['fields' => ['id', 'last_number'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.default_proforma' => 1]]);
            case 'Ordine':
                return $this->Sectionals->find('first', ['fields' => ['id', 'last_number'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.default_order' => 1]]);
            case 'Creditnote':
                return $this->Sectionals->find('first', ['fields' => ['id', 'last_number'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.default_creditnote' => 1]]);
            case 'ddt':
                return $this->Sectionals->find('first', ['fields' => ['id', 'last_number'], 'conditions' => ['Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO, 'Sectionals.default_transport' => 1]]);
            default :
                return null;
        }
    }

    public function updateBillSectional($billId)
    {
        $this->updateSectional('Bill', $billId);
    }

    public function updateOrderSectional($orderId)
    {
        $this->updateSectional('Order', $orderId);
    }

    public function updateCreditnoteSectional($creditnoteId)
    {
        $this->updateSectional('Creditnote', $creditnoteId);
    }

    public function updateProformaSectional($proFormaId)
    {
        $this->updateSectional('Proforma', $proFormaId);
    }

    public function updateTransportSectional($transportId)
    {
        $this->updateSectional('Transport', $transportId);
    }

    // Aggiorna il sezionale default per le bolle
    public function updateDefaultBillSectional($newValue)
    {
        $this->Sectional = ClassRegistry::init('Sectional');
        $this->Sectional->updateAll(['last_number' => $newValue], ['company_id' => MYCOMPANY, 'default_bill' => 1]);
    }

    public function updateSectional($documentType, $documentId)
    {
        $this->Sectional = ClassRegistry::init('Sectional');
        $this->Transport = ClassRegistry::init('Transport');
        $this->Bill = ClassRegistry::init('Bill');
        $this->Order = ClassRegistry::init('Order');

        switch ($documentType)
        {
            case 'Bill':
            case 'Creditnote':
            case 'Proforma':
                $currentBillSectional = $this->Bill->find('first', ['contain' => ['Sectional'], 'conditions' => ['Bill.id' => $documentId, 'Bill.company_id' => MYCOMPANY]]);

                if (is_numeric($currentBillSectional['Bill']['numero_fattura']))
                    $this->Sectional->updateAll(['last_number' => $currentBillSectional['Bill']['numero_fattura']], ['company_id' => MYCOMPANY, 'id' => $currentBillSectional['Sectional']['id']]);
                break;
            case 'Transport':
                $currentTransportSectional = $this->Transport->find('first', ['conditions' => ['Transport.id' => $documentId, 'Transport.company_id' => MYCOMPANY]]);

                if (is_numeric($currentTransportSectional['Transport']['transport_number']))
                    $this->Sectional->updateAll(['last_number' => $currentTransportSectional['Transport']['transport_number']], ['company_id' => MYCOMPANY, 'default_transport' => 1]); //'id' =>$currentTransportSectional['Sectional']['id']
                break;
            case 'Order':
                $currentOrderSectional = $this->Order->find('first', ['conditions' => ['Order.id' => $documentId, 'Order.company_id' => MYCOMPANY]]);

                if (is_numeric($currentOrderSectional['Order']['numero_ordine']))
                    $this->Sectional->updateAll(['last_number' => $currentOrderSectional['Order']['numero_ordine']], ['company_id' => MYCOMPANY, 'default_order' => 1]);
                break;
        }
    }

    public function getSectionalTypes()
    {
        $sectionalTypes = ['transport_sectional' => 'Bolle', 'bill_sectional' => 'Fatture', 'proforma_sectional' => 'Fatture proforma', 'order_sectional' => 'Ordini', 'creditnote_sectional' => 'Note di credito'];
        return $sectionalTypes;
    }

    public function getSectionalsArray()
    {
        $this->Sectional = ClassRegistry::init('Sectional');
        $arrayOfSectional = [];

        foreach ($this->Sectional->find('all', ['conditions' => ['Sectional.company_id' => MYCOMPANY]]) as $sectional)
        {
            $arrayOfSectional[$sectional['Sectional']['id']] = $sectional['Sectional']['suffix'];
        }

        return $arrayOfSectional;
    }

    public function getFidelityPoints($id){
        $this->Fidelities = ClassRegistry::init('Fidelities');
        $this->FidelityPoints = ClassRegistry::init('FidelityPoints');
        $somma = 0;
        $points = $this->FidelityPoints->find('all', ['conditions'=>['FidelityPoints.state'=>ATTIVO, 'fidelity_id'=>$id] ]);
        foreach ($points as $point){
            $somma += $point['FidelityPoints']['punti'];
        }
        return $somma;
    }

    public function getAgentsArray()
    {
        $this->Agent = ClassRegistry::init('Agent');
        $arrayOfSectional = [];

        foreach ($this->Agent->find('all', ['conditions' => ['Agent.company_id' => MYCOMPANY]]) as $agent)
        {
            $arrayOfAgent[$agent['Agent']['id']] = $agent['Agent']['surname'];
        }

        return $arrayOfAgent;
    }

    public function getSectionalIxfe($billId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $currentBillSectional = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]])['Sectional']['ixfe_sectional'];
        return $currentBillSectional;
    }

    public function setDefaultOrderSectional($sectionalId)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');
        if ($this->hasOtherDefaultSectional($sectionalId, 'order_sectional')) {
            try {
                $this->Sectionals->updateAll(['Sectionals.default_order' => 1], ['id' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
                $this->Sectionals->updateAll(['Sectionals.default_order' => 0], ['id <>' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
            } catch (Exception $e) {
                throw new Exception('Errore durante la definizione del sezionale predefinito per gli ordini.');
            }
        } else {
            throw new Exception('Un sezionale può essere utilizzato per un unica tipologia di documento.');
        }
    }

    /** FINE SEZIONALI **/

    /** INIZIO GESTIONE MAGAZZINO **/
    public function createNewStorage($description, $unitOfMeasureId = null, $vatId = 0, $lastBuyPrice = 0, $code = null, $supplierCode = null)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storages = ClassRegistry::init('Storages');

        $newStorage = $this->Storages->create();

        $newStorage['Storages']['company_id'] = MYCOMPANY;
        $newStorage['Storages']['movable'] = 1; // Qeusta cosa dobbiamo vedere se viene indicato

        $newStorage['Storages']['codice'] = $code;
        $newStorage['Storages']['codice_fornitore'] = $supplierCode;
        $newStorage['Storages']['descrizione'] = $description;
        $newStorage['Storages']['qta'] = 0;
        $newStorage['Storages']['last_buy_price'] = $lastBuyPrice;
        $newStorage['Storages']['prezzo'] = 0;
        $newStorage['Storages']['unit_id'] = $unitOfMeasureId;
        $newStorage['Storages']['vat_id'] = $vatId;
        $newStorage['Storages']['barcode'] = null;

        $newStorage['Storages']['category_id'] = 0;
        $newStorage['Storages']['min_quantity'] = 0;

        return $this->Storages->save($newStorage);
    }


    public function createNewStorageFromGood($good, $clientId = null, $supplierId = null)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        // Recupero il catalogo
        $this->Catalog = ClassRegistry::init('Catalog');
        // Recupero il catalogo storages
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        if ($clientId != null) {
            // Controllo prima se esiste a catalogo
            $clientCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $clientId]]);

            // Se esiste un catalogo ed è presente la descrizione nel catalogo
            if (isset($clientCatalog['Catalog']['id'])) {
                $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['oggetto'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
            }

            // Se non esiste la descrizione a catalogo
            if (!isset($catalogstorages['Catalogstorages']['description'])) {
                $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
            }

            // Se non esiste la descrizione a catalogo
            if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {
                // Se esiste già restituisco ciò che trovo a magazzino
                if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['oggetto']]])) {
                    $newStorage = $this->Storage->create();
                    $newStorage['Storage']['company_id'] = MYCOMPANY;
                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    isset($good['codice_fornitore']) ? $newStorage['Storage']['codice_fornitore'] = $good['codice_fornitore'] : null;
                    $newStorage['Storage']['descrizione'] = $good['oggetto'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                    isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                    isset($good['unita']) ? $newStorage['Storage']['unit_id'] = $good['unita'] : null;
                    // Aggiunto salvataggio articoli movimentabili
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                    // Gestione avanzata di magazzino
                    if (ADVANCED_STORAGE_ENABLED) {
                        $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                    }

                    return $this->Storage->save($newStorage);
                } else {
                    return $oldStorage;
                }
            } else {

                $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $existInCatalog['Catalogstorages']['storage_id']]]);

                // Questo per correggere un eventuale eliminazione di un articolo di magazzino (presente a catalogo, ma con storage_id mancante nell'elenco degli storage)
                if ($oldStorage == null) {
                    $newStorage = $this->Storage->create();
                    $newStorage['Storage']['company_id'] = MYCOMPANY;

                    // !!!!!!!!!!!!!!! Attenzione questa riga è per il FIX !!!!!!!!!
                    $newStorage['Storage']['id'] = $existInCatalog['Catalogstorages']['storage_id'];
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    isset($good['codice_fornitore']) ? $newStorage['Storage']['codice_fornitore'] = $good['codice_fornitore'] : null;
                    $newStorage['Storage']['descrizione'] = $good['oggetto'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                    isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                    isset($good['unita']) ? $newStorage['Storage']['unit_id'] = $good['unita'] : null;
                    // Aggiunto salvataggio articoli movimentabili
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                    // Gestione avanzata di magazzino
                    if (ADVANCED_STORAGE_ENABLED) {
                        $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                    }

                    return $newStorage;
                } else {
                    return $oldStorage;
                }
            }
        } else {
            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['oggetto']]])) {

                $newStorage = $this->Storage->create();
                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                isset($good['codice_fornitore']) ? $newStorage['Storage']['codice_fornitore'] = $good['codice_fornitore'] : null;
                $newStorage['Storage']['descrizione'] = $good['oggetto'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantita']) ? $newStorage['Storage']['qta'] = 0 : 0; // Sempre nullo tanto non viene considerato
                isset($good['prezzo']) ? $newStorage['Storage']['prezzo'] = $good['prezzo'] : 0;
                $newStorage['Storage']['unit_id'] = $good['unita'];
                // $newStorage['Storage']['withholding_tax'] = isset($good['withholding_tax']) ? $good['withholding_tax'] : null; // Aggiunto ritenuta d'acconto
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['iva_id']) ? $newStorage['Storage']['vat_id'] = $good['iva_id'] : null;

                // Gestione avanzata di magazzino
                if (ADVANCED_STORAGE_ENABLED) {
                    $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                }

                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        }
    }

    public function createNewStorageFromLoad($good, $supplierId)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        $this->Supplier = ClassRegistry::init('Supplier');
        // Recupero il catalogo
        $this->Catalog = ClassRegistry::init('Catalog');
        // Recupero il catalogo storages
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Controllo prima se esiste a catalogo
        $supplierCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $supplierId]]);

        if (isset($clientCatalog['Catalog']['id'])) {
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['description'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
        }

        if (!isset($catalogstorages['Catalogstorages']['description'])) {
            $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
        }

        if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {


            // Se esiste già restituisco cioò che trovo a magazzino
            if($_SESSION['Auth']['User']['dbname'] == "login_GE0050") {
                if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description'], "Storage.codice"]])) {
                    $newStorage = $this->Storage->create();

                    $newStorage['Storage']['company_id'] = MYCOMPANY;
                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    if (isset($supplierId)) {
                        $newStorage['Storage']['supplier_id'] = $supplierId;
                        $supplier = $this->Supplier->find('first', ['conditions' => ['Supplier.id' => $supplierId, 'Supplier.company_id' => MYCOMPANY]]);
                        $newStorage['Storage']['codice_fornitore'] = $supplier['Supplier']['name'];
                    }

                    $newStorage['Storage']['descrizione'] = $good['description'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;

                    if (AUTOMATICSELLPRICEINLOADGOOD) {
                        // Se attiva la personalizzazione "boffi" salva in automatico
                        isset($good['load_good_row_price']) ? $newStorage['Storage']['prezzo'] = $good['load_good_row_price'] : 0;
                    } else {
                        // Questo è sempre zero quando viene creato da carico merce
                        $newStorage['Storage']['prezzo'] = 0;
                    }

                    $newStorage['Storage']['unit_id'] = isset($good['unit_of_measure_id']) ? $good['unit_of_measure_id'] : null;
                    $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['load_good_row_vat_id']) ? $newStorage['Storage']['vat_id'] = $good['load_good_row_vat_id'] : null;

                    isset($good['category_id']) ? $newStorage['Storage']['category_id'] = $good['category_id'] : null;
                    isset($good['load_good_row_price']) ? $newStorage['Storage']['last_buy_price'] = $good['load_good_row_price'] : 0;
                    $newStorage['Storage']['min_quantity'] = 0;

                    return $this->Storage->save($newStorage);
                } else {
                    return $oldStorage;
                }
            }else{
                if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description']]])) {
                    $newStorage = $this->Storage->create();

                    $newStorage['Storage']['company_id'] = MYCOMPANY;
                    isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                    if (isset($supplierId)) {
                        $newStorage['Storage']['supplier_id'] = $supplierId;
                        $supplier = $this->Supplier->find('first', ['conditions' => ['Supplier.id' => $supplierId, 'Supplier.company_id' => MYCOMPANY]]);
                        $newStorage['Storage']['codice_fornitore'] = $supplier['Supplier']['name'];
                    }

                    $newStorage['Storage']['descrizione'] = $good['description'];
                    isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                    isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;

                    if (AUTOMATICSELLPRICEINLOADGOOD) {
                        // Se attiva la personalizzazione "boffi" salva in automatico
                        isset($good['load_good_row_price']) ? $newStorage['Storage']['prezzo'] = $good['load_good_row_price'] : 0;
                    } else {
                        // Questo è sempre zero quando viene creato da carico merce
                        $newStorage['Storage']['prezzo'] = 0;
                    }

                    $newStorage['Storage']['unit_id'] = isset($good['unit_of_measure_id']) ? $good['unit_of_measure_id'] : null;
                    $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                    isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                    isset($good['load_good_row_vat_id']) ? $newStorage['Storage']['vat_id'] = $good['load_good_row_vat_id'] : null;

                    isset($good['category_id']) ? $newStorage['Storage']['category_id'] = $good['category_id'] : null;
                    isset($good['load_good_row_price']) ? $newStorage['Storage']['last_buy_price'] = $good['load_good_row_price'] : 0;
                    $newStorage['Storage']['min_quantity'] = 0;

                    return $this->Storage->save($newStorage);
                } else {
                    return $oldStorage;
                }
            }
        } else {
            $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $existInCatalog['Catalogstorages']['storage_id']]]);
        }
    }

    // Per ora fa un return zero poi a seconda della valorizzazione di magazzino lo gestisco
    public function getLastBuyPrice($storageId, $purchasePrice = 0)
    {
        // Questa va parametrizzata in base al metodo di valutazione (?)
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        if (isset($this->Storagemovement->find('first', ['conditions' => ['type_of_good_movement' => 'L', 'storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY], 'order' => 'Storagemovement.id desc'])['Storagemovement'])) {
            return $this->Storagemovement->find('first', ['conditions' => ['type_of_good_movement' => 'L', 'storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY], 'order' => 'Storagemovement.id desc'])['Storagemovement']['purchase_price'];
        } else {
            return 0; // Ritorno purchase price di zero (?)
        }
    }

    // aggiorno ultimo prezzo di acquisto sul articolo di magazzino
    public function updateStorageLastPrice($id, $price = 0)
    {
        $this->Storages = ClassRegistry::init('Storages');
        $price == '' ? $price = 0 : null;
        $this->Storages->updateAll(['last_buy_price' => $price], ['Storages.id' => $id]);
    }

    // aggiorno ultimo prezzo su articolo di magazzino
    public function updateStoragePrice($id, $price)
    {
        $this->Storages = ClassRegistry::init('Storages');
        $this->Storages->updateAll(['prezzo' => $price], ['Storages.id' => $id]);
    }

    // Recupera il metodo di valutaizone a magazzino
    public function getStorageEvaluationMethod()
    {
        $this->Setting = ClassRegistry::init('Setting');
        return $this->Setting->find('first', ['conditions' => ['Setting.company_id' => MYCOMPANY], 'fields' => ['Setting.storage_evaluation_method']])['Setting']['storage_evaluation_method'];
    }

    // Scarica dai carichi le voci di magazzino
    public function setTransferredQuantity($storageId, $quantity, $depositId, $movementTag = 'notset', $storagemovementprice = null)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');

        $depositId == 'all' ? $depositCondition = '1 = 1' : $depositCondition = 'deposit_id = ' . $depositId;

        $methodValue = $this->getStorageEvaluationMethod();
        if ($movementTag == 'LG_ED' || $movementTag == 'LG_DE') {
            $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity > Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId, 'Storagemovement.purchase_price' => $storagemovementprice], 'order' => ['Storagemovement.id desc']]);
        } else {
            switch ($methodValue) {
                case 'CMP' :
                    $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity > Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId], 'order' => ['Storagemovement.id asc']]);
                    break;
                case 'LIFO':
                    $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity > Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId], 'order' => ['Storagemovement.id desc']]);
                    break;
                case 'FIFO':
                    $rowToUpdate = $this->Storagemovement->find('first', ['conditions' => ['Storagemovement.company_id' => MYCOMPANY, $depositCondition, 'Storagemovement.quantity >  Storagemovement.transfered_quantity', 'Storagemovement.storage_id' => $storageId], 'order' => ['Storagemovement.id asc']]);
                    break;
            }
        }

        if ($rowToUpdate == null) {
            /** ERRORESTORAGE **/
            // Non faccio niente
            switch ($movementTag) {
                case 'LG_ED': // Controllare TO DO
                    $movementTag = 'LG_ED_MANC';
                    $this->storageLoad($storageId, 0, 'Scarico per modifica entrata merce quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'LG_DE': // Controllare TO DO
                    $movementTag = 'LG_DE_MANC';
                    $this->storageLoad($storageId, 0, 'Scarico per eliminazione entrata merce quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'TR_AD':
                    $movementTag = 'TR_AD_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per creazione bolla quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'TR_ED':
                    $movementTag = 'TR_ED_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per modifica bolla quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                // Eliminazione bolla  no perché carico
                case 'BI_AD':
                    $movementTag = 'BI_AD_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per creazione fattura accomapagnatoria quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'BI_ED':
                    $movementTag = 'BI_ED_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per modifica fattura accomapagnatoria  quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'SC_AD':
                    $movementTag = 'SC_AD_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per creazione scontrino quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                case 'SC_ED':
                    $movementTag = 'SC_ED_MANC';
                    $this->storageLoad($storageId, 0, 'Carico per modifica scontrino  quantità non presente a magazzino', 0, $depositId, null, $movementTag, null, $quantity);
                    break;
                // Eliminazione fattura accompagnatoria no perché carico
            }
        } else {
            // Calcolo quanti spazi liberi ci sono nella riga (quantità disponibile)
            $maxQuantityToUpdate = $rowToUpdate['Storagemovement']['quantity'] - $rowToUpdate['Storagemovement']['transfered_quantity'];
            $valueToUpdate = $quantity + $rowToUpdate['Storagemovement']['transfered_quantity'];

            // Controllo se la quantity da aggiornare è maggiore di quella disponibile
            if ($quantity > $maxQuantityToUpdate) {
                $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $rowToUpdate['Storagemovement']['quantity']], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                // Scalo la quantità da qelle da aggiornare
                $quantity = $quantity - $maxQuantityToUpdate;
            } else {
                $this->Storagemovement->UpdateAll(['Storagemovement.transfered_quantity' => $valueToUpdate], ['Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.id' => $rowToUpdate['Storagemovement']['id']]);
                $quantity = 0;
            }

            // Vado in ricorsione
            if ($quantity > 0) {
                $this->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $storagemovementprice);
            }
        }
    }


    public function storageLoad($storageId, $quantity, $description, $lastBuyPrice = 0, $depositId = -1, $movementNumber = 1, $movementTag = '', $variatonId = null, $transferQuantity = 0)
    {
        // Creo il movimento di carico di magazzino
        $this->CreateMovement($storageId, $quantity, $description, $transferQuantity, $movementNumber, 'L', $lastBuyPrice, $depositId, $movementTag, $variatonId, $transferQuantity);

        // Aggiorno ultimo prezzo di acquisto su tabella storage
        $this->updateStorageLastPrice($storageId, $lastBuyPrice);
    }

    // Rimozione articoli di magazzino
    public function storageUnload($storageId, $quantity, $description, $lastBuyPrice = 0, $depositId = -1, $movementNumber = 1, $movementTag = '', $variatonId = null, $transferQuantity = 0)
    {
        if (($this->getAvailableQuantity($storageId, $depositId) - $quantity) < 0) // Controllo magazzino unico
        {
            // Non cancellare momentaneamente
            //  throw new Exception('Attenzione la quantità che si sta cercando di scaricare è superiore a quella presente in magazzino. Prima di proseguire caricare il magazzino. Quantità presente a magazzino: ' . number_format($this->getAvailableQuantity($storageId, -1),2) . ' Si è cercata di scaricare una quantità pari a : '. number_format($quantity,2));
        }

        // creo il movimento di carico di magazzino
        $this->CreateMovement($storageId, -1 * $quantity, $description, $transferQuantity, $movementNumber, 'U', $lastBuyPrice, $depositId, $movementTag, $variatonId);

        // Potrebbe darmi fastidio lo 0 sarebbe meglio null ?
        $this->setTransferredQuantity($storageId, $quantity, $depositId, $movementTag, $lastBuyPrice);
    }

    public function createDepositStorageTransfer($storageId, $quantity, $originDeposit, $destinationDeposit, $movementNumber, $variationId = null)
    {
        // Scarico il magazzino di provenienza
        $this->storageUnload($storageId, $quantity, '[TR_DEP_SC] Trasferimento tra depositi', 0, $originDeposit, $movementNumber, '', $variationId);
        // Carico il magazzino di destinazione
        $this->storageLoad($storageId, $quantity, '[TR_DEP_CA] Trasferimento tra depositi', 0, $destinationDeposit, $movementNumber + 1, '', $variationId);
    }


    public function isStorageMovable($storageId)
    {
        $this->Storages = ClassRegistry::init('Storages');

        if (isset($this->Storages->find('first', ['conditions' => ['id' => $storageId, 'company_id' => MYCOMPANY]])['Storages'])) {
            $isMovable = $this->Storages->find('first', ['conditions' => ['id' => $storageId, 'company_id' => MYCOMPANY]])['Storages']['movable'];

            if ($isMovable == 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    // Se non è definita la quantità metto zero
    public function CreateMovement($storageId, $quantity = 0, $description = '', $transferedQuantity = 0, $movementNumber = 0, $typeOfGoodMovement, $purchasePrice = 0, $depositId = -1, $movementTag = null, $variationId = null)
    {
        // Bypassa l'errore di un passaggio di quantità nulla
        $quantity == null ? $quantity = 0 : null;

        if ($depositId == '') {
            $depositId = -1;
        }

        // Controllo che sia un articolo movimentabile, se non lo è non creare il movimento.
        if ($this->isStorageMovable($storageId)) {
            $this->Storagemovement = ClassRegistry::init('Storagemovement');
            $newStorageMovement = $this->Storagemovement->create();
            $newStorageMovement['Storagemovement']['movement_tag'] = $movementTag;
            isset($typeOfGoodMovement) ? $newStorageMovement['Storagemovement']['type_of_good_movement'] = $typeOfGoodMovement : $newStorageMovement['Storagemovement']['type_of_good_movement'] = ''; // Tipo di movimento L (load) U (Unload)
            $newStorageMovement['Storagemovement']['deposit_id'] = $depositId; // id del deposito ( non gestito ) // -1
            $newStorageMovement['Storagemovement']['storage_id'] = $storageId; // Id dell'articolo a magazzino
            $newStorageMovement['Storagemovement']['purchase_price'] = $purchasePrice; // 0 di default (non gestito)
            $newStorageMovement['Storagemovement']['quantity'] = $quantity;
            $newStorageMovement['Storagemovement']['description'] = $description;
            $newStorageMovement['Storagemovement']['transfered_quantity'] = $transferedQuantity;
            $newStorageMovement['Storagemovement']['movement_number'] = $movementNumber;
            $newStorageMovement['Storagemovement']['company_id'] = MYCOMPANY;
            $newStorageMovement['Storagemovement']['variation_id'] = $variationId;
            $this->Storagemovement->save($newStorageMovement);
        }
    }

    // Resituisce la quantità totale o in un determinato deposito
    public function getAvailableQuantity($storageId, $depositId = 'all', $variationId = null)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        // Controllo se è stato definito il filtraggio delle varianti
        if ($depositId == 'all') {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        } else {
            $conditionsArray = ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO];
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => $conditionsArray]);
        }
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;
    }

    public function getAvailableImporto ()
    {

    }

    public function getVariationAvailableQuantity($storageId, $depositId, $variationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        if ($variationId == 'novariation') {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'Storagemovement.deposit_id' => $depositId, 'variation_id' => null, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        } else {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'Storagemovement.deposit_id' => $depositId, 'variation_id' => $variationId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        }
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;
    }


    public function getAllVariationAvailableQuantity($storageId, $depositId, $variationId)
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $this->Variation = ClassRegistry::init('Variation');
        // Recupero la variante e prendo il codice
        $thisVariation = $this->Variation->find('first', ['conditions' => ['Variation.id' => $variationId]]);
        // Cerco le foglie con quel codice (i rami avranno quantità nulla quindi ok)
        $thisVariations = $this->Variation->find('all', ['conditions' => ['Variation.parentcode' => $thisVariation['Variation']['code'] . '.']]);
        $arrayOfVariation = [];
        foreach ($thisVariations as $thisVariation) {
            array_push($arrayOfVariation, $thisVariation['Variation']['id']);
        }

        if (sizeof($arrayOfVariation) > 0) { //'NOT'=>['Variation.id'=>$arrayOfId ])
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'variation_id IN' => $arrayOfVariation, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        } else {
            $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => ['Storagemovement.storage_id' => $storageId, 'deposit_id' => $depositId, 'variation_id' => $variationId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO]]);
        }
        //$totalQuantity = $this->Storagemovement->find('all', ['fields'=>[ 'SUM(quantity) as rimanenze'], 'conditions'=>['Storagemovement.storage_id'=>$storageId,'deposit_id' =>$depositId,'parentcode' =>$thisVariation['Variation']['parentcode'], 'company_id'=>MYCOMPANY,'Storagemovement.state'=>ATTIVO]]);
        return is_numeric($totalQuantity[0][0]['rimanenze']) ? $totalQuantity[0][0]['rimanenze'] : 0;

    }

    // Resituisce il valore di magazzino di un determinato deposito
    public function getAvailableQuantityValue($storageId, $valueMethod, $depositId = 'all')
    {
        $this->Storagemovement = ClassRegistry::init('Storagemovement');
        $depositId == 'all' ? $depositCondition = '1 = 1' : $depositCondition = 'deposit_id = ' . $depositId;

        // Faccio la somma e riaggiungo gli scarichi dovrebbe essere quasi ok
        $totalPurchased = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity) as rimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
        $totalQuantity = $this->Storagemovement->find('all', ['fields' => ['SUM(quantity - transfered_quantity) as rimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);

        switch ($valueMethod) {
            case 'CMP':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                if ($totalPurchased[0][0]['rimanenze'] != 0) {
                    $totalPrice[0][0]['totaleRimanenze'] = $totalPrice[0][0]['totaleRimanenze'] / $totalPurchased[0][0]['rimanenze'] * $totalQuantity[0][0]['rimanenze'];
                } else {
                    $totalPrice[0][0]['totaleRimanenze'] = $totalPrice[0][0]['totaleRimanenze'] * $totalQuantity[0][0]['rimanenze'];
                }
                break;
            case 'FIFO':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity - transfered_quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                break;
            case 'LIFO':
                $totalPrice = $this->Storagemovement->find('all', ['fields' => ['SUM((quantity - transfered_quantity) * purchase_price) as totaleRimanenze'], 'conditions' => [$depositCondition, 'Storagemovement.storage_id' => $storageId, 'Storagemovement.quantity <> transfered_quantity', 'Storagemovement.company_id' => MYCOMPANY, 'Storagemovement.state' => ATTIVO, 'Storagemovement.type_of_good_movement' => 'L']]);
                break;
        }

        return is_numeric($totalPrice[0][0]['totaleRimanenze']) ? $totalPrice[0][0]['totaleRimanenze'] : 0;
    }

    /** FINE GESTIONE MAGAZZINO **/


    /** CRYPTING **/

    public function mycrypt($string, $cipher = 'aes-256-cbc', $key = "noratech2018!!##@@", $option = 0, $iv = '12kK56#90!!34X6i')
    {
        return openssl_encrypt($string, $cipher, $key, $option, $iv);
    }


    public function mydecrypt($string, $cipher = 'aes-256-cbc', $key = "noratech2018!!##@@", $option = 0, $iv = '12kK56#90!!34X6i')
    {
        return openssl_decrypt($string, $cipher, $key, $option, $iv);
    }


    public function getRandomPassword()
    {
        return $this->createRandompassword(10);
    }

    public function createRandompassword($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    )
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[mt_rand(0, $max)]; // Da sostiuitre con rand_int su php 7

        }
        return $str;
    }

    /** FINE CRYPTING **/

    // Creazione fatture da bolla
    public function buildInvoiceFromTransports($arrayOfTransports, $dettagliato = false, $splitPayment = false, $electronicInvoice = false, $customBillDateForTransport = null)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Transportgood = ClassRegistry::init('Transportgood');
        $this->Bills = ClassRegistry::init('Bills');
        $this->Storgaes = ClassRegistry::init('Storages');
        $this->Transport = ClassRegistry::init('Transport');
        $this->Currencies = ClassRegistry::init('Currencies');
        $this->Payment = ClassRegistry::init('Payment');

        // L'array contiene almeno sempre un articolo
        $arrayOfTransports = (array)$arrayOfTransports;

        $firstTranport = $arrayOfTransports[0];
        $firstTranport = $this->Transport->find('first', ['conditions' => ['Transport.id' => $firstTranport, 'Transport.state' => ATTIVO], 'contain' => 'Client']);

        $arrayOfTransportsRef = [];

        $newBill = $this->Bills->create();

        // Utilizzo i sezionali - Per fatture di vendita
        $defaultSectional = $this->getDefaultBillSectional();
        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;

        $newBill['Bills']['numero_fattura'] = $nextSectionalNumber;
        $newBill['Bills']['sectional_id'] = $defaultSectional['Sectionals']['id'];

        // Defnisco i valori per la fattura
        $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        if ($customBillDateForTransport != null)
            $newBill['Bills']['date'] = date("Y-m-d", strtotime($customBillDateForTransport)); // Data personalizzata
        else
            $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        $newBill['Bills']['client_id'] = $firstTranport['Transport']['client_id']; // Il cliente è quello della prima bolla
        $newBill['Bills']['company_id'] = $firstTranport['Transport']['company_id']; // La company è la stessa delle bolle
        $newBill['Bills']['tipologia'] = 1;

        // Per la valuta
        // Aggiunta x la gestione dell valute
        $NewBill['Bill']['currency_id'] = $firstTranport['Client']['currency_id'];
        if ($this->Currencies->GetCurrencyName($firstTranport['Client']) != 'EUR') {
            $NewBill['Bill']['changevalue'] = $this->Currencies->GetLastCurrencyChange($firstTranport['Client']['currency_id']);
        } else {
            $NewBill['Bill']['changevalue'] = 1;
        }

        $electronicInvoice == 'true' ? $newBill['Bills']['einvoicevatesigibility'] = 'I' : null;

        // Shipping name
        $newBill['Bills']['client_shipping_name'] = isset($firstTranport['Transport']['client_shipping_name']) ? $firstTranport['Transport']['client_shipping_name'] : '';
        $newBill['Bills']['client_shipping_address'] = isset($firstTranport['Transport']['client_shipping_address']) ? $firstTranport['Transport']['client_shipping_address'] : '';
        $newBill['Bills']['client_shipping_cap'] = isset($firstTranport['Transport']['client_shipping_cap']) ? $firstTranport['Transport']['client_shipping_cap'] : '';
        $newBill['Bills']['client_shipping_city'] = isset($firstTranport['Transport']['client_shipping_city']) ? $firstTranport['Transport']['client_shipping_city'] : '';
        $newBill['Bills']['client_shipping_province'] = isset($firstTranport['Transport']['client_shipping_province']) ? $firstTranport['Transport']['client_shipping_province'] : '';
        $newBill['Bills']['client_shipping_nation'] = isset($firstTranport['Transport']['client_shipping_nation']) ? $firstTranport['Transport']['client_shipping_nation'] : '';

        // Shipping name
        $newBill['Bills']['client_name'] = isset($firstTranport['Transport']['client_name']) ? $firstTranport['Transport']['client_name'] : '';
        $newBill['Bills']['client_address'] = isset($firstTranport['Transport']['client_address']) ? $firstTranport['Transport']['client_address'] : '';
        $newBill['Bills']['client_cap'] = isset($firstTranport['Transport']['client_cap']) ? $firstTranport['Transport']['client_cap'] : '';
        $newBill['Bills']['client_city'] = isset($firstTranport['Transport']['client_city']) ? $firstTranport['Transport']['client_city'] : '';
        $newBill['Bills']['client_province'] = isset($firstTranport['Transport']['client_province']) ? $firstTranport['Transport']['client_province'] : '';
        $newBill['Bills']['client_nation'] = isset($firstTranport['Transport']['client_nation']) ? $firstTranport['Transport']['client_nation'] : '';

        $newBill['Bills']['alternativeaddress_id'] = isset($firstTranport['Transport']['alternativeaddress_id']) ? $firstTranport['Transport']['alternativeaddress_id'] : 0;
        $newBill['Bills']['referredclient_id'] = isset($firstTranport['Transport']['referredclient_id']) ? $firstTranport['Transport']['referredclient_id'] : 0;

        // Split payment
        $splitPayment == 'true' ? $newBill['Bills']['split_payment'] = 1 : $newBill['Bills']['split_payment'] = 0;

        // Electronic invoice
        $electronicInvoice == 'true' ? $newBill['Bills']['electronic_invoice'] = 1 : $newBill['Bills']['electronic_invoice'] = 0;

        // Fattura da bolla dettagliata
        $dettagliato == 'true' ? $newBill['Bills']['not_detailed'] = 0 : $newBill['Bills']['not_detailed'] = 1;

        // Metto il metodo di pagamento del cliente
        isset($firstTranport['Client']['payment_id']) ? $newBill['Bills']['payment_id'] = $firstTranport['Client']['payment_id'] : null;

        $payment = $this->Payment->find('first', ['conditions' => ['Payment.id' => $firstTranport['Client']['payment_id'], 'Payment.state' => ATTIVO]]);
        isset($payment) && isset($payment['Payment']) ? $newBill['Bills']['collectionfees'] = $payment['Payment']['paymentFixedCost'] : null;

        $newBill = $this->Bills->save($newBill);
        // Recupero il catalogo per il cliente selezionato
        //$storages = $this->getStoragesWithCatalog($newBill['Bills']['client_id']);


        $arrayBillGoods = [];
        // Ciclo l'array delle bolle
        foreach ($arrayOfTransports as $transport)
        {

            // Recupero gli oggetti che hanno l'id della bolla
            $transportGood = $this->Transportgood->find('all', ['conditions' => ['Transportgood.transport_id' => $transport]]);

            // Aggiorno la bolla con il numero della fattura
            $this->Transport->UpdateAll(['bill_id' => $newBill['Bills']['id']], ['Transport.id' => $transport]);

            // Ciclo gli oggetti della bolla

            foreach ($transportGood as $good)
            {
                // Modifca pericolosa (adesso prendo sempre il prezzo dalla bolla perché c'è l'autocomplete)
                $priceForGood = $good['Transportgood']['prezzo'];

                if($newBill['Bills']['not_detailed'] == 0)
                {
                    // Aggiorno l'articolo della bolla con il legame del bill_id
                    $newBillGood = $this->Good->create();
                    $newBillGood['Good']['oggetto'] = $good['Transportgood']['oggetto'];
                    $newBillGood['Good']['quantita'] = $good['Transportgood']['quantita'];
                    $newBillGood['Good']['discount'] = $good['Transportgood']['discount'];
                    $newBillGood['Good']['prezzo'] = $priceForGood;
                    $newBillGood['Good']['iva_id'] = $good['Transportgood']['iva_id']; //$firstTranport['Client']['vat_id'];
                    $newBillGood['Good']['bill_id'] = $newBill['Bills']['id'];
                    $newBillGood['Good']['storage_id'] = $good['Transportgood']['storage_id'];
                    $newBillGood['Good']['transport_id'] = $good['Transportgood']['transport_id'];
                    $newBillGood['Good']['codice'] = $good['Transportgood']['codice'];
                    $newBillGood['Good']['unita'] = $good['Transportgood']['unita'];
                    $newBillGood['Good']['customdescription'] = $good['Transportgood']['customdescription'];
                    $newBillGood['Good']['company_id'] = $good['Transportgood']['company_id'];
                    $newBillGood['Good']['state'] = 1;
                    $newBillGood = $this->Good->save($newBillGood);

                    array_push($arrayBillGoods, $newBillGood);
                }

                // Aggiorno il numero fattura sull'articolo della bolla
                $this->Transportgood->updateAll(['Transportgood.bill_id' => $newBill['Bills']['id']], [
                    'Transportgood.transport_id' => $transport, // L'id deve essere quello della bolla
                    'Transportgood.storage_id' => $good['Storage']['id'], // L'id deve essere quello della bolla
                ]);
            }
        }

        $this->completeBill($newBill, $arrayBillGoods);

        $this->updateDefaultBillSectional($newBill['Bills']['numero_fattura']);
        return $newBill['Bills']['id'];
    }


    /** FUNZIONI DI UTILITA' TECNICA  **/
    public function buildConditions($baseConditions = [], $fields, $filters)
    {
        $supportedOperands = ['=', '<', '>', '<=', '>=', '<>'];
        $conditions = $baseConditions;
        foreach ($fields as $key => $value) {
            if (is_numeric(trim($key))) {
                $field = $value;
                $operand = null;
            } else {
                $field = $key;
                $operand = $value;
            }


            if (isset($filters[$field])) {
                if ($filters[$field] != '') {
                    if (in_array(strtolower(trim($operand)), $supportedOperands) == true) {
                        $conditions[str_replace('__', '.', $field) . " $operand"] = $filters[$field];
                    } else {
                        $conditions[str_replace('__', '.', $field) . ' LIKE '] = "%" . $filters[$field] . "%";
                    }
                }
            }
        }
        return $conditions;
    }


    public function getBillGestionalData($billId)
    {
        $this->Billgestionaldata = ClassRegistry::init('Billgestionaldata');
        return $this->Billgestionaldata->find('all', ['conditions' => ['bill_id' => $billId, 'company_id' => MYCOMPANY]]);
    }

    public function saveBillGestionalData($billId, $aliquotaIVA, $imponibileImporto, $imposta, $riferimentoNormativo, $natura, $arrotondamento, $esigibility, $ritenutaAcconto = null, $cassaprevidenziale = null)
    {
        $this->Billgestionaldata = ClassRegistry::init('Billgestionaldata');
        $this->Billgestionaldata->create();
        $newGestionalData['bill_id'] = $billId;
        $newGestionalData['aliquotaIVA'] = $aliquotaIVA;
        $newGestionalData['imponibileimporto'] = $imponibileImporto;
        $newGestionalData['imposta'] = $imposta;
        $newGestionalData['riferimentonormativo'] = $riferimentoNormativo;
        $newGestionalData['natura'] = $natura;
        $newGestionalData['arrotondamento'] = $arrotondamento;
        $newGestionalData['einvoicevatesigibility'] = $esigibility;

        if ($ritenutaAcconto != null)
        {
            isset($ritenutaAcconto['importedwithholdingpercentage']) ? $newGestionalData['importedwithholdingpercentage'] = $ritenutaAcconto['importedwithholdingpercentage'] : null;
            isset($ritenutaAcconto['importedwithholdingvalue']) ? $newGestionalData['importedwithholdingvalue'] = $ritenutaAcconto['importedwithholdingvalue'] : null;
        }

        if ($cassaprevidenziale != null)
        {
            // Cassa previdenziale
            isset($cassaprevidenziale['importedwelfareboxpercentage']) ? $newGestionalData['importedwelfareboxpercentage'] = $cassaprevidenziale['importedwelfareboxpercentage'] : null;
            isset($cassaprevidenziale['importedwelfareboxvalue']) ? $newGestionalData['importedwelfareboxvalue'] = $cassaprevidenziale['importedwelfareboxvalue'] : null;
            isset($cassaprevidenziale['importedwelfareboxvatpercentage']) ? $newGestionalData['importedwelfareboxvatpercentage'] = $cassaprevidenziale['importedwelfareboxvatpercentage'] : null;
            isset($cassaprevidenziale['importedwelfareboxwithholdingappliance']) ? $newGestionalData['importedwelfareboxwithholdingappliance'] = $cassaprevidenziale['importedwelfareboxwithholdingappliance'] : null;
            isset($cassaprevidenziale['importedwelfareboxvatnature']) ? $newGestionalData['importedwelfareboxvatnature'] = $cassaprevidenziale['importedwelfareboxvatnature'] : null;
        }

        $this->Billgestionaldata->save($newGestionalData);
    }


    /*** GESTIONE DEI MENU ***/

    // Disabilita voce di menu
    public function enableMenuVoice($menuVoiceDescription)
    {
        $this->Menu = ClassRegistry::init('Menu');
        $this->Menu->updateAll(['state' => 0], ['menu_voice' => $menuVoiceDescription, 'company_id' => MYCOMPANY]);
    }

    // Abilitazione voce di menu passando l'id
    public function disableMenuVoice($menuVoiceDescription)
    {
        $this->Menu = ClassRegistry::init('Menu');
        $this->Menu->updateAll(['state' => ATTIVO], ['menu_voice' => $menuVoiceDescription, 'company_id' => MYCOMPANY]);
    }

    public function disablePermissions($menuVoiceDescription)
    {
        switch ($menuVoiceDescription) {
            case 'Depositi':
                $this->disableController('deposits');
                break;
            case 'Entrata merce':
                $this->disableController('loadgoods');
                break;
            case 'Magazzino':
                $this->disableController('storages');
                $this->disableController('catalogs');
                $this->enableControllerActions('storages', ['indexnotmovable', 'addnotmovable', 'editnotmovable', 'deletenotmovable', 'checkIfStorageArticleExist']);
                break;
            case 'Bolle':
                $this->disableController('transports');
                break;
            case 'ddtBillForPeriod':
                $this->disableControllerActions('transports', ['transportSummary']);
                break;
        }
    }

    public function enablePermissions($menuVoiceDescription)
    {
        switch ($menuVoiceDescription) {
            case 'Depositi':
                $this->enableController('deposits');
                break;
            case 'Entrata merce':
                $this->enableController('loadgoods');
                break;
            case 'Magazzino':
                $this->enableController('storages');
                $this->enableController('catalogs');
                break;
            case 'Bolle':
                $this->enableController('transports');
                break;
            case 'ddtBillForPeriod':
                $this->enableControllerActions('transports', ['transportSummary']);
                break;
        }
    }

    public function buildInvoiceFromOrders($arrayOfOrders, $dettagliato = false, $splitPayment = false, $electronicInvoice = false, $customBillDateForOrder = null)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bills = ClassRegistry::init('Bills');
        $this->Storgaes = ClassRegistry::init('Storages');
        $this->Currencies = ClassRegistry::init('Currencies');
        $this->Order = ClassRegistry::init('Order');
        $this->Orderrow = ClassRegistry::init('Orderrow');
        $this->Payment = ClassRegistry::init('Payment');
        if(is_array($arrayOfOrders)){
            $isArray = true;
        }else{
            $isArray = false;
        }
        // L'array contiene almeno sempre un articolo
        $arrayOfOrders = (array)$arrayOfOrders;
        if($isArray)
            $firstOrder = $arrayOfOrders[0]['Order']['id'];
        else
            $firstOrder = $arrayOfOrders[0];
        $firstOrder = $this->Order->find('first', ['conditions' => ['Order.id' => $firstOrder, 'Order.state' => '1'], 'contain' => 'Client']);

        $arrayOfOrdersRef = [];

        $newBill = array();

        // Utilizzo i sezionali - Per fatture di vendita
        $defaultSectional = $this->getDefaultBillSectional();
        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $newBill['Bills']['numero_fattura'] = $nextSectionalNumber;
        $newBill['Bills']['sectional_id'] = $defaultSectional['Sectionals']['id'];
        $newBill['Bills']['payment_id'] = $firstOrder['Order']['payment_id'];
        $newBill['Bills']['alternativeaddress_id'] = $firstOrder['Order']['destination_id'];
        $newBill['Bills']['deposit_id'] = $firstOrder['Order']['deposit_id'];

        // Defnisco i valori per la fattura
        $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        if ($customBillDateForOrder != null)
            $newBill['Bills']['date'] = date("Y-m-d", strtotime($customBillDateForOrder)); // Data personalizzata
        else
            $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        $newBill['Bills']['client_id'] = $firstOrder['Order']['client_id']; // Il cliente è quello della prima bolla
        $newBill['Bills']['company_id'] = $firstOrder['Order']['company_id']; // La company è la stessa delle bolle
        $newBill['Bills']['tipologia'] = 1;

        // Per la valuta
        // Aggiunta x la gestione dell valute
        $NewBill['Bill']['currency_id'] = $firstOrder['Client']['currency_id'];
        if ($this->Currencies->GetCurrencyName($firstOrder['Client']) != 'EUR') {
            $NewBill['Bill']['changevalue'] = $this->Currencies->GetLastCurrencyChange($firstOrder['Client']['currency_id']);
        } else {
            $NewBill['Bill']['changevalue'] = 1;
        }

        $newBill['Bills']['order_id'] = $firstOrder['Order']['id'];

        $electronicInvoice == 'true' ? $newBill['Bills']['einvoicevatesigibility'] = 'I' : null;

        // Shipping name
        $newBill['Bills']['client_shipping_name'] = isset($firstOrder['Order']['client_name']) ? $firstOrder['Order']['client_name'] : '';
        $newBill['Bills']['client_shipping_address'] = isset($firstOrder['Order']['client_address']) ? $firstOrder['Order']['client_address'] : '';
        $newBill['Bills']['client_shipping_cap'] = isset($firstOrder['Order']['client_cap']) ? $firstOrder['Order']['client_cap'] : '';
        $newBill['Bills']['client_shipping_city'] = isset($firstOrder['Order']['client_city']) ? $firstOrder['Order']['client_city'] : '';
        $newBill['Bills']['client_shipping_province'] = isset($firstOrder['Order']['client_province']) ? $firstOrder['Order']['client_province'] : '';
        $newBill['Bills']['client_shipping_nation'] = isset($firstOrder['Order']['client_nation']) ? $firstOrder['Order']['client_nation'] : '';

        // Shipping name
        $newBill['Bills']['client_name'] = isset($firstOrder['Order']['client_name']) ? $firstOrder['Order']['client_name'] : '';
        $newBill['Bills']['client_address'] = isset($firstOrder['Order']['client_address']) ? $firstOrder['Order']['client_address'] : '';
        $newBill['Bills']['client_cap'] = isset($firstOrder['Order']['client_cap']) ? $firstOrder['Order']['client_cap'] : '';
        $newBill['Bills']['client_city'] = isset($firstOrder['Order']['client_city']) ? $firstOrder['Order']['client_city'] : '';
        $newBill['Bills']['client_province'] = isset($firstOrder['Order']['client_province']) ? $firstOrder['Order']['client_province'] : '';
        $newBill['Bills']['client_nation'] = isset($firstOrder['Order']['client_nation']) ? $firstOrder['Order']['client_nation'] : '';

        $newBill['Bills']['referredclient_id'] = 0;

        // Split payment
        $splitPayment == 'true' ? $newBill['Bills']['split_payment'] = 1 : $newBill['Bills']['split_payment'] = 0;

        // Electronic invoice
        $electronicInvoice == 'true' ? $newBill['Bills']['electronic_invoice'] = 1 : $newBill['Bills']['electronic_invoice'] = 0;

        // Fattura da bolla dettagliata
        $dettagliato == 'true' ? $newBill['Bills']['not_detailed'] = 0 : $newBill['Bills']['not_detailed'] = 1;

        // Metto il metodo di pagamento del cliente
        isset($firstOrder['Client']['payment_id']) ? $newBill['Bills']['payment_id'] = $firstOrder['Client']['payment_id'] : null;

        $payment = $this->Payment->find('first', ['conditions' => ['Payment.id' => $firstOrder['Client']['payment_id'], 'Payment.state' => ATTIVO]]);
        isset($payment) && isset($payment['Payment']) ? $newBill['Bills']['collectionfees'] = $payment['Payment']['paymentFixedCost'] : null;
        $newBill = $this->Bills->save($newBill);

        $arrayBillGoods = [];
        // Ciclo l'array delle bolle
        foreach ($arrayOfOrders as $order) {
            // Recupero gli oggetti che hanno l'id della bolla
            if($isArray){
                $orderRow = $this->Orderrow->find('all', ['conditions' => ['Orderrow.order_id' => $order['Order']['id']], 'contain' => ['Storage']]);

                $this->Order->UpdateAll(['bill_id' => $newBill['Bills']['id']], ['Order.id' => $order['Order']['id']]);
            }
            else{
                $orderRow = $this->Orderrow->find('all', ['conditions' => ['Orderrow.order_id' => $order], 'contain' => ['Storage']]);
                $this->Order->UpdateAll(['bill_id' => $newBill['Bills']['id']], ['Order.id' => $order]);
            }
            // Aggiorno la bolla con il numero della fattura

            // Ciclo gli oggetti della bolla

            foreach ($orderRow as $row) {
                // Modifca pericolosa (adesso prendo sempre il prezzo dalla bolla perché c'è l'autocomplete)
                $priceForGood = $row['Orderrow']['prezzo'];
                // Aggiorno l'articolo della bolla con il legame del bill_id
                $newBillGood = $this->Good->create();
                $newBillGood['Good']['oggetto'] = $row['Orderrow']['oggetto'];
                $newBillGood['Good']['quantita'] = $row['Orderrow']['quantita'];
                $newBillGood['Good']['discount'] = $row['Orderrow']['discount'];
                $newBillGood['Good']['prezzo'] = $row['Orderrow']['prezzo'];
                $newBillGood['Good']['iva_id'] = $row['Orderrow']['iva_id']; //$firstTranport['Client']['vat_id'];
                $newBillGood['Good']['bill_id'] = $newBill['Bills']['id'];
                $newBillGood['Good']['storage_id'] = $row['Orderrow']['storage_id'];
                $newBillGood['Good']['codice'] = $row['Storage']['codice'];
                $newBillGood['Good']['order_id'] = $row['Orderrow']['order_id'];
                $newBillGood['Good']['unita'] = $row['Orderrow']['unita'];
                $newBillGood['Good']['company_id'] = $row['Orderrow']['company_id'];
                $newBillGood['Good']['complimentaryQuantity'] = $row['Orderrow']['complimentaryQuantity'];
                $newBillGood['Good']['state'] = 1;
                $newBillGood = $this->Good->save($newBillGood);
                array_push($arrayBillGoods, $newBillGood);


            }
        }

        $this->completeBill($newBill, $arrayBillGoods, true);

        $this->updateDefaultBillSectional($newBill['Bills']['numero_fattura']);
        return $newBill['Bills']['id'];
    }


    public function enableControllerActions($controllerName, $actionArray)
    {
        $this->Permission = ClassRegistry::init('Permission');
        foreach ($actionArray as $action) {
            $this->Permission->updateAll(['company_id' => MYCOMPANY], ['controller' => $controllerName, 'action' => $action, 'company_id' => -1]);
        }
    }

    public function disableController($controllerName)
    {
        $this->Permission = ClassRegistry::init('Permission');
        $this->Permission->updateAll(['company_id' => '-1'], ['controller' => $controllerName, 'company_id' => MYCOMPANY]);
    }

    public function enableController($controllerName)
    {
        $this->Permission = ClassRegistry::init('Permission');
        $this->Permission->updateAll(['company_id' => MYCOMPANY], ['controller' => $controllerName, 'company_id' => -1]);
    }
    /*** FINE GESTIONE DEI MENU ***/

    /** MAGAZZINO **/

    //  Restituisce l'elenco degli articoli di magazzino presente in un determinato deposito, o in tutti i depositi
    public function getStorages($depositId = null, $movable = 'all')
    {
        $this->Storages = ClassRegistry::init('Storages');
        switch ($movable) {
            case false:
                $movableconditions = ['Storages.movable' => 0];
                break;
            case true:
                $movableconditions = ['Storages.movable' => 1];
                break;
            case 'all':
                $movableconditions = '1 = 1';
                break;
        }

        if ($depositId != null) {
            $arrayOfStorages = $this->getStoragesInDeposit($depositId);
            if (count($arrayOfStorages) > 1) {
                $storages = $this->Storages->find('all', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.state' => ATTIVO, 'Storages.id in ' => $arrayOfStorages, $movableconditions], 'order' => ['Storages.descrizione' => 'asc']]);
            } else {
                $storages = $this->Storages->find('all', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.state' => ATTIVO, 'Storages.id ' => $arrayOfStorages, $movableconditions], 'order' => ['Storages.descrizione' => 'asc']]);
            }
        } else {
            $storages = $this->Storages->find('all', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.state' => ATTIVO, $movableconditions], 'order' => ['Storages.descrizione' => 'asc']]);
        }

        return $storages;
    }


    public function storageHasLeaf($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        if ($this->Storage->find('count', ['conditions' => ['Storage.parent_id' => $storageId, 'Storage.company_id' => MYCOMPANY]]) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getStorageParentId($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]]);
        return $storage['Storage']['parent_id'];
    }

    public function getStorageParentCodeFromCurrentStorage($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');

        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]]);
        //   $storage = $this->Storage->find('first',['conditions'=>['Storage.id'=>$storage['Storage']['parent_id'],'Storage.company_id'=>MYCOMPANY]]);
        return $storage['Storage']['codice'];
    }

    public function getStorageParentDescriptionFromCurrentStorage($storageId)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $storage = $this->Storage->find('first', ['conditions' => ['Storage.id' => $storageId, 'Storage.company_id' => MYCOMPANY]]);
        //    $storage = $this->Storage->find('first',['conditions'=>['Storage.id'=>$storage['Storage']['parent_id'],'Storage.company_id'=>MYCOMPANY]]);
        return $storage['Storage']['descrizione'];
    }

    public function getStoragesWithVariationsNotUnique($depositId = null)
    {
        return $this->getStoragesWithVariations($depositId, 'true', true, true);
    }

    //  Restituisce l'elenco degli articoli di magazzino presente in un determinato deposito (=0), o in tutti i depositi e sue varianti
    public function getStoragesWithVariations($depositId = null, $movable = 'all', $notShowVariation = 'false', $singleStorageId = null, $onlyExist = false)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $this->Utilities = ClassRegistry::init('Utilities');

        switch ($movable)
        {
            case 'false': // Voci descrittive
                $movableconditions = ['Storage.movable' => 0];
                $singleStorageId == true ? $movableconditions = ['Storage.movable' => 0, 'Storage.sn = 0'] : null;
                break;
            case 'true': // Articoli
                $movableconditions = ['Storage.movable' => 1];
                $singleStorageId == true ? $movableconditions = ['Storage.movable' => 1, 'Storage.sn = 0'] : null;
                break;
            case 'all':
                $movableconditions = '1 = 1';
                $singleStorageId == true ? $movableconditions = ['Storage.sn = 0'] : null;
                break;
        }

        $fieldsArray = ['Storage.id', 'Storage.descrizione', 'Storage.prezzo', 'Storage.codice', 'Storage.unit_id', 'Storage.vat_id', 'Storage.barcode', 'Storage.movable', 'Storage.last_buy_price'];

        if ($depositId != null)
        {
            $arrayOfStorages = $this->getStoragesInDeposit($depositId, $onlyExist);
            if (count($arrayOfStorages) > 1)
            {
                $storages = $this->Storage->find('all', ['recursive' => -1, 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id in ' => $arrayOfStorages, $movableconditions], 'order' => ['Storage.descrizione' => 'asc']]);
            }
            else
            {
                $storages = $this->Storage->find('all', ['recursive' => -1, 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, 'Storage.id ' => $arrayOfStorages, $movableconditions], 'order' => ['Storage.descrizione' => 'asc']]);
            }
        }
        else
        {
            $storages = $this->Storage->find('all', ['recursive' => -1, 'conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.state' => ATTIVO, $movableconditions], 'fields' => $fieldsArray, 'order' => ['Storage.descrizione' => 'asc']]);
        }


        /*$arrayOfStorages = [];
        $newCode = -1;

        foreach ($storages as $storage)
        {
            if (!$this->Utilities->storageHasLeaf($storage['Storage']['id']))
            {
                $newCode++;
                $arrayOfStorages[$newCode]['Storages'] = $storage['Storage'];
            }
        }*/
        if($_SESSION['Auth']['User']['dbname'] != "login_GE0015"){
            foreach ($storages as $storage)
            {
                if ($this->Utilities->storageHasLeaf($storage['Storage']['id']))
                {
                    unset($storage['Storage']);
                }
            }
        }
        return $storages;
    }

    // Restituisce l’elenco degli articoli di magazzino presente in un deposito con quantità maggiore di zero
    public function getStoragesInDeposit($depositId, $onlyExist = false)
    {
        $storages = $this->getStoragesWithVariations();
        $arrayOfStorages = [];

        foreach ($storages as $storage)
        {
            if ($onlyExist == true)
            {
                if ($this->getAvailableQuantity($storage['Storage']['id'], $depositId) > 0)
                    array_push($arrayOfStorages, $storage['Storage']['id']);
            }

            if ($onlyExist == false)
            {
                if ($this->getAvailableQuantity($storage['Storage']['id'], $depositId))
                    array_push($arrayOfStorages, $storage['Storage']['id']);
            }
        }

        return $arrayOfStorages;
    }


    // Controllo se e pa o no
    public function isClientPa($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['pa'])) {
            return $client['Clients']['pa'];
        }
    }

    // Ritenuta d'acconto predefinita per i clienti
    public function loadClientWithholdingTax($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);
        if (isset($client['Clients']['withholding_tax'])) {
            if ($client['Clients']['withholding_tax'] >= 0) {
                return number_format($client['Clients']['withholding_tax'], 2, '.', '');
            } else {
                return $client['Clients']['withholding_tax'];
            }
        }
    }

    public function loadClientCurrency($customerId)
    {
        $this->Clients = ClassRegistry::init('Clients');
        $this->Currencies = ClassRegistry::init('Currencies');
        $client = $this->Clients->find('first', ['conditions' => ['Clients.company_id' => MYCOMPANY, 'Clients.state' => ATTIVO, 'Clients.id' => $customerId]]);

        if (isset($client['Clients'])) {

            $currencyName = $this->Currencies->GetCurrencyName($client['Clients']['currency_id']);
            $currencyChange = $this->Currencies->GetLastCurrencyChange($client['Clients']['currency_id']);

            $currentCurrency['currency_name'] = $currencyName;
            $currentCurrency['currency_change'] = $currencyChange;
        } else {   // Potrebbe creare problemi ma fixa l'errore di index cliant non trovato
            $currentCurrency['currency_name'] = '';
            $currentCurrency['currency_change'] = '';
        }

        return json_encode($currentCurrency);
    }


    public function getStoragesList()
    {
        $this->Storages = ClassRegistry::init('Storages');
        $storages = $this->Storages->find('list', ['fields' => ['id', 'descrizione'], 'conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.state' => ATTIVO], 'order' => ['Storages.descrizione' => 'asc']]);
        return $storages;
    }

    // Elenco dei depositi
    public function getDepositsList()
    {
        $this->Deposits = ClassRegistry::init('Deposits');
        $deposits = $this->Deposits->find('list', ['fields' => ['id', 'deposit_name'], 'conditions' => ['company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO]]);
        return $deposits;
    }

    // Numero di deposit
    public function getDepositsCount()
    {
        $this->Deposits = ClassRegistry::init('Deposits');
        $depositsCount = $this->Deposits->find('count', ['conditions' => ['Deposits.company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO]]);
        return $depositsCount;
    }

    // Recupero  il deposito principale
    public function getDefaultDeposits()
    {
        $this->Deposits = ClassRegistry::init('Deposits');
        $deposits = $this->Deposits->find('first', ['conditions' => ['Deposits.company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO, 'main_deposit' => 1]]);
        if ($deposits != null) {
            return $deposits;
        } else {
            return -1; // Non è stato definito un deposito
        }
    }

    // Recupero  il deposito principale
    public function getDefaultDepositId()
    {
        $this->Deposits = ClassRegistry::init('Deposits');
        $deposits = $this->Deposits->find('first', ['conditions' => ['Deposits.company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO, 'main_deposit' => 1]]);
        if ($deposits != null) {
            return $deposits['Deposits']['id'];
        } else {
            return -1; // Non è stato definito un deposito
        }
    }


    public function setNewMainDeposit($depositId)
    {
        $this->Deposits = ClassRegistry::init('Deposits');
        try {
            $this->Deposits->updateAll(['main_deposit' => 1], ['id' => $depositId, 'company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO]);
            $this->Deposits->updateAll(['main_deposit' => 0], ['id <>' => $depositId, 'company_id' => MYCOMPANY, 'Deposits.state' => ATTIVO]);
        } catch (Exception $e) {
            throw new Exception('Errore durante la definizione del deposito principale.');
        }
    }

    //
    public function notExistInStorage($storageId, $depositId = 'all')
    {
        $this->Storages = ClassRegistry::init('Storages');

        // Se esiste a magazzino per la società
        if ($this->Storages->find('count', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.id' => $storageId]]) == 0) {
            return true;
        } else {
            return true;
        }
    }

    // Restituisce l'articolo attivo con il nome corrispondente
    public function getStorageByName($storageDescription)
    {
        $this->Storages = ClassRegistry::init('Storages');
        $return = $this->Storages->find('first', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.descrizione' => $storageDescription, 'state' => ATTIVO]]);
    }

    public function getStorageByCode($storageCode)
    {
        $this->Storages = ClassRegistry::init('Storages');
        return $this->Storages->find('first', ['conditions' => ['Storages.company_id' => MYCOMPANY, 'Storages.codice' => $storageCode, 'state' => ATTIVO]]);

    }


    public function checkIfStorageArticleExist($storageName, $clientId = null)
    {
        $this->Storages = ClassRegistry::init('Storages');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Visualizzo se esiste un catalogo
        $clientCatalog = $this->existClientCatalog($this->getCustomerId($clientId));

        if ($clientCatalog && $clientId != null) {

            // Recupero gli articoli a magazzino
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $storageName, 'Catalogstorages.catalog_id' => $clientCatalog['Catalogs']['id']]]);

            // Se non esiste la descrizione a catalogo
            if (!isset($catalogstorages['Catalogstorages']['description'])) {
                // Non esiste a catalogo
                // return false;

                // Se non esiste a catalogo controllo che esista in magazzino o descrizine voci
                $exist = $this->Storages->find('count', ['conditions' => ['descrizione' => $storageName]]);
                if ($exist > 0) {
                    // Esiste in magazzino
                    return true;
                } else {
                    // Non esiste in magazzino
                    return false;
                }
            } else {
                // Esiste a catalogo
                return true;
            }
        } else {
            // Visualizzo se esiste a magazzino
            $existInStorage = $this->Storages->find('count', ['conditions' => ['descrizione' => $storageName, 'Storages.company_id' => MYCOMPANY]]);
            if ($existInStorage > 0) {
                // Esiste in magazzino
                return true;
            } else {
                return false;
            }

            /*else
            {
                $existInVariation = $this->Variation->find('count',['conditions'=>['Variation.description'=>$storageName, 'Variation.company_id'=> MYCOMPANY]]);
                if($existInVariation > 0)
                {
                    return true;
                }
                else
                {
                    // Non esiste in magazzino ne in varianti
                    return false;
                }
            }*/
        }
    }

    /** FINE MAGAZZINO **/

    /** INIZIO VARIANTI **/

    /*public function getVariationsListLeafs($storageId = null)
    {
        $this->Storage = ClassRegistry::init('Storage');
        $storageId == null ? $conditionsArray = ['Storage.company_id' =>MYCOMPANY,'Storage.state'=>ATTIVO] : $conditionsArray = ['Storage.company_id' =>MYCOMPANY,'Storage.state'=>ATTIVO,'Storage.storage_id'=>$storageId];
        $storages = $this->Storage->find('all',['fields' => ['id', 'description','code'],'conditions'=>$conditionsArray, 'order'=>'Variation.description asc']);

        $arrayOfId = [];
        foreach($storages as $storage)
        {
            if($this->Storage->find('count',['conditions'=>['parentcode'=>$storage['Storage']['codice'].'.']]) > 0)
            {
                array_push($arrayOfId,$storage['Storage']['id']);
            }
        }
        $storageId == null ? $conditionsArray2 = ['Storage.company_id' =>MYCOMPANY,'Storage.state'=>ATTIVO, 'NOT'=>['Storage.id'=>$arrayOfId ]] : $conditionsArray2 = ['Storage.company_id' =>MYCOMPANY,'Storage.state'=>ATTIVO,'Storage.storage_id'=>$storageId,'NOT'=>['Storage.id'=>$arrayOfId ]];
        $variations = $this->Storage->find('list',['fields' => ['id', 'description'],'conditions'=>$conditionsArray2, 'order'=>'Storage.description asc']);
        return $variations;
    }*/

    public function getVariations($storageId = null)
    {
        $this->Variation = ClassRegistry::init('Variation');
        $storageId == null ? $conditionsArray = ['Variation.company_id' => MYCOMPANY, 'Variation.state' => ATTIVO] : $conditionsArray = ['Variation.company_id' => MYCOMPANY, 'Variation.state' => ATTIVO, 'Variation.storage_id' => $storageId];
        return $this->Variation->find('all', ['conditions' => $conditionsArray, 'order' => 'Variation.description asc']);
    }

    /** FINE VARIANTI **/

    /** DELETE **/

    public function disableRecord($id, $model)
    {
        $this->$model = ClassRegistry::init($model);
        $this->$model->updateAll(['state' => -1], ['id' => $id, 'company_id' => MYCOMPANY]);
        return true;
    }
    /** FINE DELETE **/

    /** ECCEZIONI **/

    public function throwException($type = 'notfound')
    {
        switch ($type) {
            case 'notfound':
                throw new NotFoundException('Stai cercando di accedere ad una pagina inesistente.');
                break;
        }
    }

    /** FINE ECCEZIONI **/

    public function loadmodels($lodamodel, $arrayOfModel)
    {
        foreach ($arrayOfModel as $model) {
            $lodamodel->loadModel($model);
        }
    }

    /** SALVATAGGIO TOKEN **/
    public function connectToMainDb()
    {
        $settings = [
            'datasource' => 'Database/Mysql',
            'persistent' => false,
            'host' => 'localhost',
            'login' => MAINUSER,
            'password' => MAINPASSWORD,
            'database' => MAINDB,
            'prefix' => '',
            'encoding' => 'utf8',
        ];

        try {
            ConnectionManager::getDataSource('default')->disconnect();
            ConnectionManager::drop('default');
            ConnectionManager::create('default', $settings);
            ConnectionManager::getDataSource('default')->connect();
            $db = ConnectionManager::getDataSource('default');
        } catch (MissingDatabaseException $e) {
            $this->Session->setFlash($e->getMessage());
        }
    }

    public function connectToDb($dbName)
    {
        $settings = [
            'datasource' => 'Database/Mysql',
            'persistent' => false,
            'host' => 'localhost',
            'login' => MAINUSER,
            'password' => MAINPASSWORD,
            'database' => $dbName,
            'prefix' => '',
            'encoding' => 'utf8',
        ];

        try {
            ConnectionManager::getDataSource('default')->disconnect();
            ConnectionManager::drop('default');
            ConnectionManager::create('default', $settings);
            ConnectionManager::getDataSource('default')->connect();
            $db = ConnectionManager::getDataSource('default');
        } catch (MissingDatabaseException $e) {
            $this->Session->setFlash($e->getMessage());
        }
    }

    function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function validateToken($token, $billId, $realCompanyId, $clientId)
    {
        $this->Token = ClassRegistry::init('Token');
        $this->connectToMainDb();
        $tokenValidated = $this->Token->Find('first', ['conditions' => ['token' => $token, 'bill_id' => $billId, 'company_id' => $realCompanyId, 'client_id' => $clientId]]);
        return $tokenValidated;
    }

    public function createMailToken($clientId, $billId, $database)
    {

        $this->Token = ClassRegistry::init('Token');

        // Mi connetto al db generale
        $this->connectToMainDb();

        $newToken = $this->Token->create();
        $token = MYCOMPANY . $this->generateRandomString(20) . $clientId . $this->generateRandomString(20) . $billId;
        $encryptedToken = $this->mycrypt($token, $cipher = 'aes-256-cbc', $key = "TokenEncryptionNoratech", $option = 0, $iv = '12@@@@#905634X6i');
        $newToken['Token']['token'] = $encryptedToken;
        $encryptedDb = $this->mycrypt($database, $cipher = 'aes-256-cbc', $key = "TokenEncryptionDatabse", $option = 0, $iv = '12@@@@#905634X7K');
        $newToken['Token']['dbname'] = $encryptedDb;
        $newToken['Token']['bill_id'] = $billId;
        $newToken['Token']['client_id'] = $clientId;
        $newToken['Token']['company_id'] = MYCOMPANY;
        $newToken['Token']['validity'] = 120;
        $newToken['Token']['state'] = 1;
        $this->Token->save($newToken);
        return $newToken;
    }


    //** GRAFICA ** /

    public function setMenuAsActive($menuVoice, $parentId = null)
    {
        $this->Menus = ClassRegistry::init('Menus');
        $menuVoice = $this->Menus->find('first', ['conditions' => ['company_id' => MYCOMPANY, 'menu_voice' => $menuVoice]]);
        $_SESSION['Auth']['User']['activeMenuItemId'] = $menuVoice['Menus']['id'];
        $_SESSION['Auth']['User']['activeMenuItemParentId'] = $parentId;
    }

    /** IXFE **/

    // Recupero il token per le credenziali
    public function getIXFEAccessToken()
    {
        // Non devo disegnare niente
        $this->autoRender = false;
        $this->Setting = ClassRegistry::init('Setting');

        $settings = $this->Setting->GetMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        $ixfeuser = $settings['Setting']['userIXFE'];
        $ixfepassword = $this->mydecrypt($settings['Setting']['passwordIXFE'], 'aes-256-cbc', 'noratechdemovladh2018', 0, 'demo2314xxx20189');

        // Chiamata al servizio IXFE per il recupero del token

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt_array($curl, [
            CURLOPT_URL => $arxivarLink . "/Auth/api/v1/account/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n  \"grant_type\": \"password\",\r\n  \"username\": \"$ixfeuser\",\r\n  \"password\": \"$ixfepassword\",\r\n  \"scope\": \"\",\r\n  \"refresh_token\": \"\",\r\n  \"consumerusername\": \"\"\r\n}",
            CURLOPT_HTTPHEADER => [
                "Cache-Control: no-cache",
                "Content-Type: application/json",
            ],
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->Log->savelog('IXAPI', 'Errore Generico', 'IXFEAPI001');
            return -1;
        } else {
            $obj = json_decode($response);
            if (isset($obj->{'error'})) {
                if ($obj->{'error'} == 'invalid_request') {
                    $errorMessage = "Errore durante l'autenticazione al servizio IXFE (controllare le credenziali d'accesso)";
                    return -1;
                }
            } else {
                $token = $obj->{'access_token'};
                $this->updateSettingsTokenIXFE($token);
                return $token;
            }
        }
    }

    public function checkIXFEAccessToken ()
    {
        /* Inizio Token IXFE */
        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->GetMySettings();
        $token = $mySettings['Setting']['tokenIXFE'];
        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $arxivarLink . "Invoice/api/v2/aoo/aoos",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "X-Authorization: $token",
            ),
        ));

        $response = curl_exec($curl);
        $code = curl_getinfo($curl)['http_code'];
        curl_close($curl);
        if($code != 200){
            $this->getIXFEAccessToken();
            $mySettings = $this->Setting->GetMySettings();

            $token = $mySettings['Setting']['tokenIXFE'];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

            curl_setopt_array($curl, array(
                CURLOPT_URL => $arxivarLink . "Invoice/api/v2/aoo/aoos",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "X-Authorization: $token",
                ),
            ));

            $response = curl_exec($curl);
            $code = curl_getinfo($curl)['http_code'];
            return ($token);
        }
        /* Fine Token IXFE*/
    }

    public function getIXFEAoos($token)
    {
        $this->Versioning = ClassRegistry::init('Versioning');
        $this->Setting = ClassRegistry::init('Setting');
        $settings = $this->Setting->GetMySettings();
        $arxivarLink = $this->Versioning->getArxivarLink();
        // Chiamata al servizio per la richiesta dell'identificativo aoos

        $this->Log = ClassRegistry::init('Log');
        $this->Log->savelog('IXAPI', 'Richiesta aoos - token inviato:', $token);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/aoo/aoos",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 4ceab82c-c9f2-4f77-9a4d-0f674a8f553f",
                "X-Authorization: $token"
            ),
        ));


        $response = curl_exec($curl);

        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->Log->savelog('IXAPI', 'Errore generico', 'IXFEAPI002A');
            return -1;
        } else {
            $obj = json_decode($response);
            if (isset($obj->{'error'})) {
                if ($obj->{'error'} == 'invalid_request') {
                    $this->Log->savelog('IXAPI', 'Errore generico', 'IXFEAPI002B');
                    return -1;
                }
            } else {
                $obj = json_decode($response, true);
                if (isset($obj['aoos'])) {
                    if (count(array($obj['aoos'])[0]) == 1) {
                        $identificativoAoos = $obj['aoos'][0]['identificativo'];
                        $this->Log->savelog('IXAPI', 'Aoos ricevuta', $identificativoAoos);
                        return $identificativoAoos;
                    } else {
                        $this->Log->savelog('IXAPI', 'Aoos multiple ricevute', '');
                        foreach (array($obj['aoos'])[0] as $azienda) {
                            if ($azienda['identificativi'][1]['valore'] == $settings['Setting']['piva'] || strtolower($azienda['identificativi'][2]['valore']) == strtolower($settings['Setting']['cf'])) {
                                $this->Log->savelog('IXAPI', 'Aoos utilizzata', $azienda['identificativo']);
                                return $azienda['identificativo'];
                            }
                        }
                    }
                } else {
                    $this->Log->savelog('IXAPI', 'Aoos mancante', '');
                    return -1;
                }
            }
        }
    }

    public function sendIXFEBill($token, $identificativoAoos, $electronic_invoice_base64, $nomeFile, $billNumber, $ixfeSectional)
    {

        $this->Log = ClassRegistry::init('Log');
        $this->Log->savelog('IXAPI', 'Invio fattura:', $nomeFile);

        $curl = curl_init();
        $billNumber = $billNumber;

        if ($ixfeSectional == null || $ixfeSectional == '') {
            return "ERR012";
        } else {
            $temp =
                [
                    'fatturaFile' => ['nome' => $nomeFile, 'buffer' => $electronic_invoice_base64],
                    'container' => [0 => ['profilo' => ['metadati' => [0 => ['nome' => 'SEZIONALEIVA', 'valore' => $ixfeSectional]]]]],
                ];

            // 'idEsterno' => $billNumber,

            $temp = json_encode($temp);

            $this->Versioning = ClassRegistry::init('Versioning');
            $arxivarLink = $this->Versioning->getArxivarLink();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt_array($curl, array(
                CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$identificativoAoos/fattureelettroniche",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $temp,
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "X-Authorization: $token",
                ),
            ));

            $response = curl_exec($curl);
            $code = curl_getinfo($curl)['http_code'];

            if($code == 401){
                $token = $this->getIXFEAccessToken();
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$identificativoAoos/fattureelettroniche",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 90,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $temp,
                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        "X-Authorization: $token",
                    ),
                ));
                $response = curl_exec($curl);
            }

            if ($response == 'An error occurred, please try again or contact the administrator.') {
                $this->Log->savelog('IXAPI', 'Errore generico:', 'IXFEAPI003');
                return "ERR010";
            }

            $err = curl_error($curl);
            curl_close($curl);

            if ($err || $response == false) {
                $this->Log->savelog('IXAPI', 'Errore mancata connessione generico:', $err);
                return "ERR101";
            } else {
                $obj = json_decode($response);

                if (isset($obj->{'error'})) {
                    if ($obj->{'error'} == 'invalid_request') {
                        $this->Log->savelog('IXAPI', 'Errore generico:', 'IXFEAPI005');
                        return "ERR001";
                    }
                } else {
                    if (isset($obj->{'code'})) {
                        if ($obj->{'code'} == 'Model_Validation' && substr($obj->{'reason'}, 0, 10) == 'ExternalID') {
                            $this->Log->savelog('IXAPI', 'Errore modello e id esterno errati', 'IXFEAPI006');
                            return "ERR002"; // Identificativo
                        }
                    }

                    $identificativoFatturaIXFE = $obj->{'identificativo'};
                    $this->Log->savelog('IXAPI', 'Idetificativo ricevuto correttamente:', $identificativoFatturaIXFE);
                    return $identificativoFatturaIXFE;
                }
            }
        }
    }

    // Recupero stato di validazione
    public function getBillIxfeValidationState($id, $idIxfe)
    {
        // Non devo disegnare niente
        $this->Utilities = ClassRegistry::init('Utilities');
        $this->Setting = ClassRegistry::init('Setting');
        $this->Bill = ClassRegistry::init('Bill');

        $mySettings = $this->Setting->GetMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        // Variabile utilizzati
        $errorMessage = $token = $identificativo = null;

        $pivacf = '';
        $bill = $this->Bill->find('first', ['contain' => ['Client' => 'Nation', 'Good' => ['Iva' => ['Einvoicevatnature'], 'Storage', 'Units'], 'Payment' => ['Bank', 'Einvoicepaymenttype', 'Einvoicepaymentmethod']], 'conditions' => ['Bill.id' => $id, 'Bill.company_id' => MYCOMPANY]]);

        $this->Log = ClassRegistry::init('Log');

        if ($mySettings['Setting']['tokenIXFE'] == null) {
            $mySettings = $this->Setting->GetMySettings();
            // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
            $token = $mySettings['Setting']['tokenIXFE'];

            // Se non è stato trovato il token
            if ($token == -1) {
                // throw new InternalErrorException('Errore durante il recupero delle token IXFE');
                return '';
            }

            $this->updateSettingsTokenIXFE($token);
        } else {
            $token = $mySettings['Setting']['tokenIXFE'];
        }

        /** Recupero lo stato **/
        $this->Log->savelog('IXAPI', 'Aggiornamento stato validazione fattura:', 'stato : ' . $idIxfe);

        // Chiamata al servizio per la richiesta dell'identificativo aoop
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/fattureelettroniche/$idIxfe/notifiche/ricerca",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{}",
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                "Cache-Control: no-cache",
                "X-Authorization: " . $token . "",
            ],
        ]);

        $response = curl_exec($curl);

        $obj = json_decode($response);

        $err = curl_error($curl);
        curl_close($curl);


        if ($obj != null) {
            if (isset($obj->{'code'})) {
                if ($obj->{'code'} == 'Generic_Error' && $obj->{'reason'} == 'URI non valido') {
                    $this->Log->savelog('IXAPI', 'Errore generico', 'IXFEAPI007');
                    return '';
                }
            } else {
                //return  $obj->{'resultCount'}->{'totalResultRecords'};
                $totaleRecord = $obj->{'resultCount'}->{'totalResultRecords'};
                if ($totaleRecord > 0) {
                    $this->Log->savelog('IXAPI', 'Stato Valdazione', $obj->{'notifiche'}[$totaleRecord - 1]->{'stato'});
                    return $obj->{'notifiche'}[$totaleRecord - 1]->{'stato'};
                }
            }
        }
    }


    public function getBillIxfeArchiveState($billIdIXFE)
    {

        $this->Log = ClassRegistry::init('Log');

        // Chiamata al servizio per la richiesta dell'identificativo aoos
        $curl = curl_init();

        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->getMySettings();

        if ($mySettings['Setting']['tokenIXFE'] == null) {
            $mySettings = $this->Setting->GetMySettings();
            // Il Token viene preso dai setting in quanto la chiamata a questa funzione avviene solo dall'index delle fatture, ed in quella scheramata - il token viene controllato e riaggiornato
            $token = $mySettings['Setting']['tokenIXFE'];

            // Se non è stato trovato il token
            if ($token == -1) {
                $this->Log->savelog('IXAPI', 'Errore mancato recupero token ixfe', 'Stato archivio');
                return -1;
            }

            $this->updateSettingsTokenIXFE($token);
        } else {
            $token = $mySettings['Setting']['tokenIXFE'];
        }

        if ($mySettings['Setting']['aoosIXFE'] == null || $mySettings['Setting']['aoosIXFE'] == -1 || $mySettings['Setting']['aoosIXFE'] == '') {
            $this->updateSettingsAoosIXFE($IdentificativoAoos = $this->getIXFEAoos($token));
        } else {
            $IdentificativoAoos = $mySettings['Setting']['aoosIXFE'];
        }

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();


        $this->Log->savelog('IXAPI', 'Aggiornamento stato archiviazione fattura:', 'Aoos: ' . $IdentificativoAoos . ' idfattura : ' . $billIdIXFE);

        $curlArray = [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$IdentificativoAoos/fatturetrasmissione/ricerca",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
                '{
		      "filtriAvanzati": [
		      {
		          "ultimoInvio":true,
		          "fields": [
		          {
		              "field": "IDENTIFICATIVOIX",
		              "valore": "' . $billIdIXFE . '"
		          }]
		      }
		      ]

		  }',
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Content-Type:application/json ",
                "X-Authorization: $token ",
            ],
        ];

        $a = curl_setopt_array($curl, $curlArray);

        $response = curl_exec($curl);

        $obj = json_decode($response);

        $err = curl_error($curl);

        curl_close($curl);

        if ($obj != null) {
            if (isset($obj->{'fatture'}) && $obj->{'fatture'} != null) {
                if (isset($obj->{'fatture'}[0]->{'profiliFattura'}[0])) {
                    return $obj->{'fatture'}[0]->{'profiliFattura'}[0];
                } else {
                    $this->Log->savelog('IXAPI', 'Profilo archiviazione fattura inesistente:', '');
                    return '';
                }
            } else {
                $this->Log->savelog('IXAPI', 'Aggiornamento stato archiviazione fattura:', '');
                return '';
            }
        } else {
            $this->Log->savelog('IXAPI', 'Risposta ixfe errata (oggetto non restituito)', '');
            return '';
        }
    }

    public function UpdateBillsIXFEStates($billId)
    {

        $this->Bill = ClassRegistry::init('Bill');

        $statoIXFE = $this->getArrayStatoIXFE();
        $statoElaborazioneIXFE = $this->getArrayStatoElaborazioneIXFE();
        $statoNotificaIXFE = $this->getArrayNotificaIXFE();

        $Bill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);

        if ($Bill['Bill']['id_ixfe']) {
            // Da portare nella funzione
            $notify = $this->getBillIxfeValidationState($billId, $Bill['Bill']['id_ixfe']);

            if ($notify != '' && $notify != null)  // Aggiunto controllo ridondante giusto per non sovrascrivere nullo
            {
                $this->updateBillValidationIxfe($billId, $notify);
            }

            // Recupero archivio stati
            $result = $this->getBillIxfeArchiveState($Bill['Bill']['id_ixfe']);

            // Provo a fare supersemplificazione (li ho anche invertiti)
            if ($result != '' && $result != null) {
                $this->updateBillProcessingStateIxfe($billId, $result->{'statoElaborazione'});  // Stato elaborazione
                $this->updateBillStateIxfe($billId, $result->{'stato'});   // Stato
            }
        }
    }

    public function getSyncroResponse($billId, $token, $aoos)
    {
        $this->Log = ClassRegistry::init('Log');

        $curl = curl_init();

        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->getMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        $this->Log->savelog('IXAPI', 'Risoluzione sincronizzazione arxivar.', 'Aoos: ' . $aoos . 'token : ' . $token);

        $pivacf = '';
        $mySettings['Setting']['piva'] != '' ? $pivacf = $mySettings['Setting']['piva'] : $pivacf = $mySettings['Setting']['cf'];
        $billFileName = 'IT' . $pivacf . '_' . strtoupper(str_pad(base_convert($billId, 10, 32), 5, '0', STR_PAD_LEFT)) . '.xml';


        $curlArray = [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$aoos/fattureelettroniche/ricerca",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
                '{
		      "filtriAvanzatiFattureElettroniche": [
                {
                  "filtri": [
                    {
                      "field": "NOMEFILE",
                      "valore": "' . $billFileName . '"
                    }
                  ]
                }
              ]
		  }',
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Content-Type:application/json",
                "X-Authorization: $token ",
            ],
        ];

        $a = curl_setopt_array($curl, $curlArray);

        $response = curl_exec($curl);

        $obj = json_decode($response);

        $err = curl_error($curl);

        curl_close($curl);

        if ($obj != null) {
            if (isset($obj->{'fatture'}) && $obj->{'fatture'} != null) {
                if (isset($obj->{'fatture'}[0]->{'identificativo'})) {
                    return $obj->{'fatture'}[0]->{'identificativo'};
                } else {
                    $this->Log->savelog('IXAPI', 'Nessuna fattura da sincronizzare:', '');
                    return 'nessuna';
                }
            } else {
                if (isset($obj->{'message'})) {
                    $this->Log->savelog('IXAPI', 'Sincronizzazione non effettuata.', $obj->{'message'});
                } else {
                    $this->Log->savelog('IXAPI', 'Sincronizzazione non effettuata.', 'IXFESYNCRO9');
                }
                return 'nessuna';
            }
        } else {
            $this->Log->savelog('IXAPI', 'Sincronizzazione non effettuata.', 'IXFESYNCRO1');
            return '';
        }
    }

    public function sendToConservation($IdentificativoAoos, $token)
    {
        $this->Log = ClassRegistry::init('Log');

        // Chiamata al servizio per la richiesta dell'identificativo aoop
        $curl = curl_init();

        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->getMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        $this->Log->savelog('IXAPI', 'Invio a conservazione sostitutiva.', 'Aoos: ' . $IdentificativoAoos . 'token : ' . $token);

        $curlArray = [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$IdentificativoAoos/fatturetrasmissione/resendtoconservation",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
                '{

		  }',
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                "Cache-Control: no-cache",
                "Content-Type:application/json ",
                "X-Authorization: $token ",
            ],
        ];

        $a = curl_setopt_array($curl, $curlArray);

        $response = curl_exec($curl);

        $obj = json_decode($response);

        $err = curl_error($curl);
        curl_close($curl);


        if (isset($obj->{'code'}) && isset($obj->{'reason'}) && $obj->{'reason'} == 'Nessun sezionale censito.' && $obj->{'code'} == 'Model_Validation') {
            $this->Log->savelog('IXAPI', 'Errore reinvio conservazione sostitutiva. Nesun sezionale censito.', 'IXFEAPI009');
            return 'Error009';
        } else {
            return '';
        }
    }

    public function getIxfePassiveBills($IdentificativoAoos, $token, $dateFrom, $dateTo, $download = '0', $filterreceived = '0')
    {
        $this->Log = ClassRegistry::init('Recupero fatture da ixfe');

        $curl = curl_init();

        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->getMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        if ($filterreceived == '0' || $filterreceived == 0)
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            $curlArray = [
                CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/ricezione/aoos/$IdentificativoAoos/fatturericezione/ricerca",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>
                    '{
                        "rangeFattureRicezione": {
                        "field": "DATADOCUMENTO",
                        "dataDa": "' . date("Y-m-d", strtotime($dateFrom)) . '",
                        "dataA": "' . date("Y-m-d", strtotime($dateTo)) . '"
                        },
        		  }',
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Content-Type:application/json ",
                    "X-Authorization: $token ",
                ],
            ];
        }

        if ($filterreceived == '1' || $filterreceived == 1)
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            $curlArray = [
                CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/ricezione/aoos/$IdentificativoAoos/fatturericezione/ricerca",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>
                    '{
                        "rangeFattureRicezione": {
                        "field": "DATARICEZIONE",
                        "dataDa": "' . date("Y-m-d", strtotime($dateFrom)) . '",
                        "dataA": "' . date("Y-m-d", strtotime($dateTo . ' +1 day ')) . '"
                        },
        		  }',
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Content-Type:application/json ",
                    "X-Authorization: $token ",
                ],
            ];
        }

        $a = curl_setopt_array($curl, $curlArray);
        $response = curl_exec($curl);
        $obj = json_decode($response);
        $err = curl_error($curl);
        curl_close($curl);

        $this->Xml = ClassRegistry::init('Xml');
        $i = 0;

        $arrayOfFiles = [];

        foreach ((array)$obj->{'fatture'} as $bill)
        {
            // Controlla se esiste già la fattura in gol

            $i++;
            // Se non esiste richiedi il file della fattura
            $xmlStriped = $this->RecoverIxfeBillFile($bill->{'identificativo'}, $token, $download);

            if ($download == 0 || $download == '0') // Se scaricare è false
            {
                // Salvo il file su hd
                file_put_contents(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}, $xmlStriped);

                // Vado a modificarne l'elemento
                if (strtolower(substr(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}, strlen(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}) - 4, 4)) == ".p7m")
                {

                    $SMIME = <<<TXT
MIME-Version: 1.0
Content-Disposition: attachment; filename="smime.p7m"
Content-Type: application/x-pkcs7-mime; smime-type=signed-data; name="smime.p7m"
Content-Transfer-Encoding: base64
\n
TXT;
                    $from = file_get_contents(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'});
                    $SMIME .= chunk_split(base64_encode($from));
                    file_put_contents(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'} , $SMIME);

                    $curlCaBundle = APP.'webroot/curl-ca-bundle.crt';

                    $finalFilename = str_replace('.p7m', '', APP.'tmpfatturepassive/'.$bill->{'nomeFile'});
                    openssl_pkcs7_verify(APP.'tmpfatturepassive/'.$bill->{'nomeFile'}, PKCS7_NOVERIFY|PKCS7_BINARY , null ,array($curlCaBundle), $curlCaBundle, $finalFilename);

                    $xmlStriped = file_get_contents($finalFilename);
                    unlink($finalFilename);
                }

                if (substr(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}, strlen(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}) - 4, 4) == ".P7M")
                {
                    try
                    {
                        $command = "openssl smime -verify -noverify -in " . APP . 'tmpfatturepassive/' . $bill->{'nomeFile'} . " -inform DER -out  " . str_replace(".P7M", "", APP . 'tmpfatturepassive/' . $bill->{'nomeFile'});
                        exec($command, $a, $return_val);
                    }
                    catch (Exception $e)
                    {
                        $xmlStriped = file_get_contents(str_replace(".P7M", "", APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}));
                    }

                    unlink(str_replace(".P7M", "", APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}));
                }

                unlink(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'});

                // Chiamo la funzione di salvataggio della fattura in ingresso
                $this->Xml->createNewEbillFromXml($xmlStriped, $bill->{'identificativo'}, $bill->{'dataRicezione'});
            }
            else
            {
                // Scarico il file
                file_put_contents(APP . 'tmpfatturepassive/' . $bill->{'nomeFile'}, $xmlStriped);

                // Aggiungo all'array degli archivi
                array_push($arrayOfFiles, $bill->{'nomeFile'});
            }
        }

        if ($download == 1 || $download == '1') // true
        {
            $date = new DateTime();
            $zipname = APP . 'tmpfatturepassive/FatPas_' . $this->Setting->GetMySettings()['Setting']['piva'] . $date->getTimestamp() . '.zip';
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);

            foreach ($arrayOfFiles as $file)
            {
                $zip->addFile(APP . 'tmpfatturepassive/' . $file, $file);
            }

            $zip->close();

            if ($i > 0)
            {
                foreach ($arrayOfFiles as $file)
                {
                    unlink(APP . 'tmpfatturepassive/' . $file);
                }

                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename=' . 'FatPas_' . $this->Setting->GetMySettings()['Setting']['piva'] . $date->getTimestamp() . '.zip');
                readfile($zipname);
                unlink($zipname);
            }
            else
            {
                return -1;
            }
        }
    }

    public function getIxfeActiveBills($IdentificativoAoos, $token, $dateFrom, $dateTo)
    {
        ini_set('post_max_size','200M');
        ini_set('upload_max_filesize','200M');
        ini_set('max_execution_time',1200);
        ini_set('max_input_time',600);
        ini_set('memory_limit','1500M');
        set_time_limit(65536);

        $this->Log = ClassRegistry::init('Recupero fatture da ixfe');
        $this->Xml = ClassRegistry::init('Xml');
        $this->Setting = ClassRegistry::init('Setting');
        $this->Versioning = ClassRegistry::init('Versioning');

        $arxivarLink = $this->Versioning->getArxivarLink();
        //$mySettings = $this->Setting->getMySettings();

        $dataStart = date("Y-m-d", strtotime($dateFrom));
        $dataEnd = date("Y-m-d", strtotime($dateTo));

        /*$datetime1 = new DateTime($dataStart);
        $datetime2 = new DateTime($dataEnd);
        $interval = $datetime1->diff($datetime2);
        $totalDay = $interval->format('%a');*/

        $arrayOfFiles = [];
        $i = 0;

        while($dataStart <= $dataEnd)
        {
            $curl = curl_init();

            $curlArray = [
                CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$IdentificativoAoos/fatturetrasmissione/ricerca",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_SSL_VERIFYHOST => 0, // To remove
                CURLOPT_SSL_VERIFYPEER => 0, // To remove
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{
                    "stati": [{
                        "stato": "TRASMESSA"
                    }],
                    "filtriAvanzati" : [{
                        "fields": [{
                            "field": "DGDDATA",
                            "valore": "' . $dataStart . '"
                        }]
                    }]
                }',
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Content-Type:application/json ",
                    "X-Authorization: $token ",
                ],
            ];

            curl_setopt_array($curl, $curlArray);

            $response = curl_exec($curl);
            $obj = json_decode($response);
            curl_close($curl);

            if($obj != null)
            {
                $array = (array)$obj;

                if(isset($array['fatture']))
                {
                    foreach($array['fatture'] as $bill)
                    {
                        if($bill->{'profiliFattura'}[0]->{'statoElaborazione'} != 'SCARTATA')
                        {
                            $i++;
                            $xmlStriped = $this->RecoverIxfeSentBillFile($bill->{'identificativo'}, $token);
                            file_put_contents(APP . 'tmpfattureattive/' . $bill->{'nomeFile'}, $xmlStriped);
                            array_push($arrayOfFiles, $bill->{'nomeFile'});
                        }
                    }
                }
            }

            $dataStart = date("Y-m-d", strtotime($dataStart."+ 1 day"));
        }

        /*for ($j = 0; $j < $totalDay + 1; $j++)
        {
            if ($j < 2) {
                $VALORE = date("Y-m-d", strtotime($dataStart . " +" . $j . " day"));
            } else {
                $VALORE = date("Y-m-d", strtotime($dataStart . " +" . $j . " days"));
            }

            $curl = curl_init();

            $curlArray = [
                CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/$IdentificativoAoos/fatturetrasmissione/ricerca",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 90,
                CURLOPT_SSL_VERIFYHOST => 0, // To remove
                CURLOPT_SSL_VERIFYPEER => 0, // To remove
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{
                    "stati": [{
                        "stato": "TRASMESSA"
                    }],
                    "filtriAvanzati" : [{
                        "fields": [{
                            "field": "DGDDATA",
                            "valore": "' . $VALORE . '"
                        }]
                    }]
                }',
                CURLOPT_HTTPHEADER => [
                    "Accept: application/json",
                    "Cache-Control: no-cache",
                    "Content-Type:application/json ",
                    "X-Authorization: $token ",
                ],
            ];

            curl_setopt_array($curl, $curlArray);

            $response = curl_exec($curl);
            $obj = json_decode($response);
            $err = curl_error($curl);

            curl_close($curl);

            $this->Xml = ClassRegistry::init('Xml');

            if($obj != null)
            {
                $array = (array)$obj;

                if(isset($array['fatture']))
                {
                    foreach($array['fatture'] as $bill)
                    {
                        if($bill->{'profiliFattura'}[0]->{'statoElaborazione'} != 'SCARTATA')
                        {
                            $i++;
                            $xmlStriped = $this->RecoverIxfeSentBillFile($bill->{'identificativo'}, $token);
                            file_put_contents(APP . 'tmpfattureattive/' . $bill->{'nomeFile'}, $xmlStriped);
                            array_push($arrayOfFiles, $bill->{'nomeFile'});
                        }
                    }
                }
            }
        }*/

        $date = new DateTime();
        $zipname = APP . 'tmpfattureattive/FatAtt_' . $this->Setting->GetMySettings()['Setting']['piva'] . $date->getTimestamp() . '.zip';

        $zip = new ZipArchive;
        $zip->open($zipname, ZipArchive::CREATE);

        foreach ($arrayOfFiles as $file) {
            $zip->addFile(APP . 'tmpfattureattive/' . $file, $file);
        }

        $zip->close();

        if ($i > 0) {
            foreach ($arrayOfFiles as $file) {
                try {
                    unlink(APP . 'tmpfattureattive/' . $file);
                } catch (Exception $exception) {
                }
            }

            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename=' . 'FatAtt_' . $this->Setting->GetMySettings()['Setting']['piva'] . $date->getTimestamp() . '.zip');

            readfile($zipname);
            unlink($zipname);
        } else {
            return -1;
        }
    }


    public function RecoverIxfeBillFile($identificativoFatturaIxfe, $token, $download)
    {
        $this->Setting = ClassRegistry::init('Setting');
        $mySettings = $this->Setting->getMySettings();

        $this->Versioning = ClassRegistry::init('Versioning');
        $arxivarLink = $this->Versioning->getArxivarLink();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/ricezione/aoos/fatturericezione/$identificativoFatturaIxfe/file",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Accept: application/octect-stream",
                "Cache-Control: no-cache",
                "Content-Type: blob",
                "X-Authorization: $token"
            ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);

        if (substr($response, 0, 1) == "<") {
            return $response;
        } else {
            $responsetmp = $response;

            if (strlen(strstr($responsetmp, 'FatturaElettronicaHeader'))) {
                return $response;
            } else {
                // Attenzione possibile introduzione problematiche
                if ((ord(substr($response, 0, 1)) == 255) && (ord(substr($response, 1, 1)) == 254) && base64_decode($response) == '') {
                    $response = str_replace(chr(255), '', $response);
                    $response = str_replace(chr(254), '', $response);
                    $response = str_replace(chr(0), '', $response);
                } else {
                    return base64_decode($response);
                }
            }
        }

        return $response;
    }

    public function RecoverIxfeSentBillFile($identificativoFatturaIxfe, $token)
    {
        $this->Setting = ClassRegistry::init('Setting');
        $this->Versioning = ClassRegistry::init('Versioning');

        $mySettings = $this->Setting->getMySettings();
        $arxivarLink = $this->Versioning->getArxivarLink();

        /*$curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $arxivarLink . "/Invoice/api/v2/trasmissione/aoos/fatturetrasmissione/$identificativoFatturaIxfe/file",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_SSL_VERIFYHOST => 0, // To remove
            CURLOPT_SSL_VERIFYPEER => 0, // To remove
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Accept: application/octect-stream",
                "Cache-Control: no-cache",
                "Content-Type: blob",
                "X-Authorization: $token"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);*/

        while(true)
        {
            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => $arxivarLink . "Invoice/api/v2/trasmissione/aoos/fatturetrasmissione/$identificativoFatturaIxfe/file",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 100,
                CURLOPT_TIMEOUT => 120,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "Accept: application/octect-stream",
                    "Cache-Control: no-cache",
                    "Content-Type: blob",
                    "X-Authorization: $token"
                ],
            ]);

            $response = curl_exec($curl);
            curl_close($curl);

            if($response != false)
            {
                break;
            }
        }

        if (substr($response, 0, 1) == "<") {
            return $response;
        } else {
            $responsetmp = $response;
            if (strlen(strstr($responsetmp, 'FatturaElettronicaHeader'))) {
                return $response;
            } else {
                return base64_decode($response);
            }
        }

        return $response;
    }

    /** Utilities Import XML **/
    public function stripP7MData($string)
    {

        // skip everything before the XML content
        $string = substr($string, strpos($string, '<?xml '));

        // skip everything after the XML content
        preg_match_all('/<\/.+?>/', $string, $matches, PREG_OFFSET_CAPTURE);
        $lastMatch = end($matches[0]);

        return substr($string, 0, $lastMatch[1] + strlen($lastMatch[0]));
    }


    function sanitizeXML($string)
    {
        if (!empty($string))
        {
            // remove EOT+NOREP+EOX|EOT+<char> sequence (FatturaPA)
            $string = preg_replace('/(\x{0004}(?:\x{201A}|\x{FFFD})(?:\x{0003}|\x{0004}).)/u', '', $string);

            $regex = '/(
                [\xC0-\xC1] # Invalid UTF-8 Bytes
                | [\xF5-\xFF] # Invalid UTF-8 Bytes
                | \xE0[\x80-\x9F] # Overlong encoding of prior code point
                | \xF0[\x80-\x8F] # Overlong encoding of prior code point
                | [\xC2-\xDF](?![\x80-\xBF]) # Invalid UTF-8 Sequence Start
                | [\xE0-\xEF](?![\x80-\xBF]{2}) # Invalid UTF-8 Sequence Start
                | [\xF0-\xF4](?![\x80-\xBF]{3}) # Invalid UTF-8 Sequence Start
                | (?<=[\x0-\x7F\xF5-\xFF])[\x80-\xBF] # Invalid UTF-8 Sequence Middle
                | (?<![\xC2-\xDF]|[\xE0-\xEF]|[\xE0-\xEF][\x80-\xBF]|[\xF0-\xF4]|[\xF0-\xF4][\x80-\xBF]|[\xF0-\xF4][\x80-\xBF]{2})[\x80-\xBF] # Overlong Sequence
                | (?<=[\xE0-\xEF])[\x80-\xBF](?![\x80-\xBF]) # Short 3 byte sequence
                | (?<=[\xF0-\xF4])[\x80-\xBF](?![\x80-\xBF]{2}) # Short 4 byte sequence
                | (?<=[\xF0-\xF4][\x80-\xBF])[\x80-\xBF](?![\x80-\xBF]) # Short 4 byte sequence (2)
            )/x';

            $string = preg_replace($regex, '', $string);

            $result = "";
            $length = strlen($string);

            for ($i = 0; $i < $length; $i++)
            {
                $current = ord($string{$i});

                if (($current == 0x9) || ($current == 0xA) || ($current == 0xD) ||
                    (($current >= 0x20) && ($current <= 0xD7FF)) ||
                    (($current >= 0xE000) && ($current <= 0xFFFD)) ||
                    (($current >= 0x10000) && ($current <= 0x10FFFF)))
                {
                    $result .= chr($current);
                }
                else
                {
                    // $ret .= " ";    // use this to replace them with spaces
                }
            }

            $string = $result;
        }

        return $string;
    }

    /** end utilities xml **/

    public function getArrayNotificaIXFE($stato = null)
    {
        $y = '#ffdb03';
        $r = "#E74600";
        $g = "#25AE88";
        $arraySendBillState =
            [
                'PRESAINCARICO' => ['Presa in carico', $y, null],
                'ATTESATEMPLATIZZAZIONE' => ['Attesa Templatizzazione', $y, null],
                'ESECUZIONETEMPLATIZZAZIONE' => ['Esecuzione Templatizzazione', $y, null],
                'TEMPLATIZZAZIONEEFFETTUATA' => ['Templatizzazione effettuata', $y, null],
                'TEMPLATIZZAZIONEFALLITA' => ['Templatizzazione fallita', $y, null],
                'TEMPLATIZZAZIONECONERRORI' => ['Templatizzazione con errori', $y, null],
                'ATTESAVALIDAZIONE' => ['Attesa validazione', $y, null],
                'ESECUZIONEVALIDAZIONE' => ['Esecuzione validazione', $y, null],
                'VALIDAZIONEEFFETTUATA' => ['Validazione effettuata', $g, 'save'],
                'VALIDAZIONEFALLITA' => ['Validazione fallita', $r, 'save'],
                'VALIDAZIONECONERRORI' => ['Validazione con errori', $r, null],
                'SCARTATA' => ['Scartata', $r, null],
                'ATTESACONSERVAZIONE' => ['Attesa conservazione', $y, null],
                'ESECUZIONECONSERVAZIONE' => ['Esecuzone conservazione', $y, null],
                'CONSERVAZIONEEFFETTUATA' => ['Conservazione effettuata', $y, null],
                'CONSERVAZIONEFALLITA' => ['Conservazione fallita', $y, null],
                'CONSERVAZIONECONERRORI' => ['Conservazione con errori', $y, null],
                'ATTESASTORICIZZAZIONE' => ['Attesa storicizzazione', $y, null],
                'ESECUZIONESTORICIZZAZIONE' => ['Esecuzione storicizzazione', $y, null],
                'STORICIZZAZIONEEFFETTUATA' => ['Storicizzazione effettuata', $y, null],
                'STORICIZZAZIONEFALLITA' => ['Storicizzazione fallita', $y, null],
                'STORICIZZAZIONECONERRORI' => ['Storicizzazione con errori', $y, null],
            ];

        if ($stato == null) {
            return $arraySendBillState;
        } else {
            return $arraySendBillState[$stato];
        }
    }

    public function getArrayStatoIXFE($stato = null)
    {
        $y = '#ffdb03';
        $r = "#E74600";
        $g = "#25AE88";
        $arrayStato =
            [
                'PRESAINCARICO' => ['Presa in carico', $y, null],
                'ATTESAFIRMA' => ['Attesa firma', $y, null],
                'ESECUZIONEFIRMA' => ['Esecuzione firma', $y, null],
                'FIRMAEFFETTUATA' => ['Firma effettuata', $y, null],
                'FIRMAFALLITA' => ['Firma fallita', $r, null],
                'FIRMACONERRORI' => ['Firma con errori', $y, null],
                'ATTESATRASMISSIONE' => ['Attesa trasmissione', $y, null],
                'ESECUZIONETRASMISSIONE' => ['Esecuzione trasmissione', $y, null],
                'TRASMISSIONEEFFETTUATA' => ['Trasmissione effettuata', $y, null],
                'TRASMISSIONEFALLITA' => ['Trasmissione fallita', $r, null],
                'TRASMISSIONECONERRORI' => ['Trasmissione con errori', $r, null],
                'RICEVUTACONSEGNA' => ['Ricevuta consegna (archiviazione effettuata)', $g, null],
                'NOTIFICAMANCATACONSEGNA' => ['Notifica mancata consegna (archiviazione effettuata)', $g, null],
                'NOTIFICASCARTO' => ['Notifica scarto', $y, null],
                'NOTIFICAESITOPOSITIVO' => ['Notifica esito positivo', $g, null],
                'NOTIFICAESITONEGATIVO' => ['Notifica esito negativo', $r, null],
                'NOTIFICADECORRENZATERMINI' => ['Notifica decorrenza termini', $y, null],
                'ATTESTAZIONETRASMISSIONEFATTURA' => ['Attestazione trasmissione fattura', $y, null],
                'ATTESAVALUTAZIONESCARTO' => ['Attesa valutazione scarto', $y, null],
                'SCARTONONGESTITO' => ['Scarto non gestito', $r, null],
                'REINOLTROEFFETTUATO' => ['Reinoltro effettuato', $y, null],
                'REINOLTROFALLITO' => ['Reinoltro fallito', $y, null],
                'REINOLTROCONERRORI' => ['Reinoltro con errori', $y, null],
                'ATTESACONSERVAZIONE' => ['Attesa conservazione', $y, null],
                'ESECUZIONECONSERVAZIONE' => ['Esecuzione conservazione', $y, null],
                'CONSERVAZIONEEFFETTUATA' => ['Conservazione effettuata', $g, 'save'],
                'CONSERVAZIONEFALLITA' => ['Conservazione fallita', $r, 'save'],
                'CONSERVAZIONECONERRORI' => ['Conservazione con errori', $r, 'save'],
                'ATTESASTORICIZZAZIONE' => ['Attesa storicizzazione', $y, null],
                'ESECUZIONESTORICIZZAZIONE' => ['Esecuzione storicizzazione', $y, null],
                'STORICIZZAZIONEEFFETTUATA' => ['Storicizzazione effettuata', $y, null],
                'STORICIZZAZIONEFALLITA' => ['Storicizzazione fallita', $y, null],
                'STORICIZZAZIONECONERRORI' => ['Storicizzazione con errori', $y, null],
            ];

        if ($stato == null) {
            return $arrayStato;
        } else {
            return $arrayStato[$stato];
        }
    }

    // Trasmissione
    public function getArrayStatoElaborazioneIXFE($stato = null)
    {
        $y = '#ffdb03';
        $r = "#E74600";
        $g = "#25AE88";
        $arrayStatoElaborazione =
            [
                'INELABORAZIONE' => ['In Elaborazione', $y, null],
                'INERRORE' => ['In Errore', $y, null],
                'TRASMESSA' => ['Trasmessa', $y, null],
                'CONSEGNATA' => ['Consegnata', $g, 'save'],
                'NONCONSEGNATA' => ['Non consegnata', $r, 'save'],
                'SCARTATA' => ['Scartata', $r, 'save'],
                'ESITOPOSITIVO' => ['Esito Positivo', $g, null],
                'ESITONEGATIVO' => ['Esito negativo', $r, null],
                'DECORRENZATERMINI' => ['Decorrenza Termini', $y, null],
                'ATTESTAZIONETRASMISSIONE' => ['Attestazione trasmissione', $y, null],
            ];

        if ($stato == null) {
            return $arrayStatoElaborazione;
        } else {
            $arrayStatoElaborazione['stato'];
        }
    }


    public function resetSettingsIXFE()
    {
        $this->Setting = ClassRegistry::init('Setting');
        $this->Setting->updateAll(['tokenIXFE' => null], ['Setting.company_Id' => MYCOMPANY]);
        $this->Setting->updateAll(['aoosIXFE' => null], ['Setting.company_Id' => MYCOMPANY]);
    }

    public function updateSettingsTokenIXFE($token)
    {
        $this->Setting = ClassRegistry::init('Setting');
        //   $this->Setting->updateAll(['tokenIXFE'=>null],['Setting.company_Id'=>MYCOMPANY]);
        $this->Setting->updateAll(['tokenIXFE' => "\"" . $token . "\""], ['Setting.company_Id' => MYCOMPANY]);
    }

    public function updateSettingsAoosIXFE($token)
    {
        $this->Setting = ClassRegistry::init('Setting');
        //    $this->Setting->updateAll(['aoosIXFE'=>null],['Setting.company_Id'=>MYCOMPANY]); // Questa non serve ma la mettiamo lo stesso non si sa mai
        $this->Setting->updateAll(['aoosIXFE' => "\"" . $token . "\""], ['Setting.company_Id' => MYCOMPANY]);
    }

    public function updateBillValidationIxfe($billId, $validationIxfeValue)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Bill->updateAll(['validation_ixfe' => "\"" . $validationIxfeValue . "\""], ['Bill.id' => $billId, 'Bill.company_Id' => MYCOMPANY]);
    }

    public function updateBillProcessingStateIxfe($billId, $processigStateIxfe)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Bill->updateAll(['processing_state_ixfe' => "\"" . $processigStateIxfe . "\""], ['Bill.id' => $billId, 'Bill.company_Id' => MYCOMPANY]);
    }

    public function updateBillStateIxfe($billId, $stateIxfeValue)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Bill->updateAll(['state_ixfe' => "\"" . $stateIxfeValue . "\""], ['Bill.id' => $billId, 'Bill.company_Id' => MYCOMPANY]);
    }

    /** FINE IXFE **/

    /** SEZIONALI **/

    public function hasOtherDefaultSectional($sectionalId, $typeOfSectional)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');
        $hasOtherSectionals = $this->Sectionals->find('first', ['conditions' => ['Sectionals.id' => $sectionalId]]);
        $total = $hasOtherSectionals['Sectionals']['default_bill'] + $hasOtherSectionals['Sectionals']['default_creditnote'] + $hasOtherSectionals['Sectionals']['default_transport'] + $hasOtherSectionals['Sectionals']['default_proforma'];
        if ($hasOtherSectionals['Sectionals'][$typeOfSectional] == 1) {
            if ($total == 0) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }


    public function setDefaultBillSectional($sectionalId)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');

        if ($this->hasOtherDefaultSectional($sectionalId, 'bill_sectional')) {
            try {
                $this->Sectionals->updateAll(['Sectionals.default_bill' => 1], ['id' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
                $this->Sectionals->updateAll(['Sectionals.default_bill' => 0], ['id <>' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
            } catch (Exception $e) {
                throw new Exception('Errore durante la definizione del sezionale predefinito per le fatture.');
            }
        } else {
            throw new Exception('Un sezionale può essere utilizzato per un unica tipologia di documento.');
        }
    }

    public function setDefaultProformaSectional($sectionalId)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');
        if ($this->hasOtherDefaultSectional($sectionalId, 'proforma_sectional')) {
            try {
                $this->Sectionals->updateAll(['Sectionals.default_proforma' => 1], ['id' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
                $this->Sectionals->updateAll(['Sectionals.default_proforma' => 0], ['id <>' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
            } catch (Exception $e) {
                throw new Exception('Errore durante la definizione del sezionale predefinito per le fatture.');
            }
        } else {
            throw new Exception('Un sezionale può essere utilizzato per un unica tipologia di documento.');
        }
    }

    public function setDefaultCreditnoteSectional($sectionalId)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');
        if ($this->hasOtherDefaultSectional($sectionalId, 'creditnote_sectional')) {
            try {
                $this->Sectionals->updateAll(['Sectionals.default_creditnote' => 1], ['id' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
                $this->Sectionals->updateAll(['Sectionals.default_creditnote' => 0], ['id <>' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
            } catch (Exception $e) {
                throw new Exception('Errore durante la definizione del sezionale predefinito per le note di credito.');
            }
        } else {
            throw new Exception('Un sezionale può essere utilizzato per un unica tipologia di documento.');
        }
    }

    public function setDefaultTransportSectional($sectionalId)
    {
        $this->Sectionals = ClassRegistry::init('Sectionals');
        if ($this->hasOtherDefaultSectional($sectionalId, 'transport_sectional')) {
            try {
                $this->Sectionals->updateAll(['Sectionals.default_transport' => 1], ['id' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
                $this->Sectionals->updateAll(['Sectionals.default_transport' => 0], ['id <>' => $sectionalId, 'Sectionals.company_id' => MYCOMPANY, 'Sectionals.state' => ATTIVO]);
            } catch (Exception $e) {
                throw new Exception('Errore durante la definizione del sezionale predefinito per le fatture.');
            }
        } else {
            throw new Exception('Un sezionale può essere utilizzato per un unica tipologia di documento.');
        }
    }

    /** FINE SEZIONALI **/

    /** METODI DI PAGAMENTO **/

    public function setDefaultReceiptPayment($paymentMethodId)
    {
        $this->Payments = ClassRegistry::init('Payments');
        try {
            $this->Payments->updateAll(['Payments.default_receipt' => 1], ['id' => $paymentMethodId, 'Payments.company_id' => MYCOMPANY, 'Payments.state' => ATTIVO]);
            $this->Payments->updateAll(['Payments.default_receipt' => 0], ['id <>' => $paymentMethodId, 'Payments.company_id' => MYCOMPANY, 'Payments.state' => ATTIVO]);
        } catch (Exception $e) {
            throw new Exception('Errore durante la definizione del sezionale predefinito per le fatture.');
        }
    }

    // Metodo di pagamento predefinito clienti
    public function getDefaultReceiptPaymentMethodId()
    {
        $this->Payment = ClassRegistry::init('Payment');
        $payment = $this->Payment->find('first', ['conditions' => ['Payment.company_id' => MYCOMPANY, 'Payment.state' => ATTIVO, 'Payment.default_receipt' => 1]]);
        if (isset($payment['Payment']['id'])) {
            return $payment['Payment']['id'];
        }
    }


    /** Controllo duplicati iva cf **/

    public function checkClientPivaDuplicate($piva, $id = null)
    {
        $this->Client = ClassRegistry::init('Client');
        if ($piva != null && $piva != '') {
            $numberOfPiva = $this->Client->find('count', ['conditions' => ['piva' => $piva, 'Client.id <>'=>$id, 'Client.state' => 1, 'Client.company_id' => MYCOMPANY]]);
        } else {
            $numberOfPiva = 0;
        }
        return $numberOfPiva;
    }


    public function checkClientCfDuplicate($cf, $id = null)
    {
        $this->Client = ClassRegistry::init('Client');
        if ($cf != null && $cf != '') {
            $numberOfCf = $this->Client->find('count', ['conditions' => ['cf' => $cf, 'Client.id <>'=>$id, 'Client.state' => 1, 'Client.company_id' => MYCOMPANY]]);
        } else {
            $numberOfCf = 0;
        }
        return $numberOfCf;
    }

    public function checkSupplierPivaDuplicate($piva)
    {
        $this->Supplier = ClassRegistry::init('Supplier');
        if ($piva != null && $piva != '') {
            $numberOfPiva = $this->Supplier->find('count', ['conditions' => ['piva' => $piva, 'Supplier.state' => 1, 'Supplier.company_id' => MYCOMPANY]]);
        } else {
            $numberOfPiva = 0;
        }
        return $numberOfPiva;
    }


    public function checkSupplierCfDuplicate($cf)
    {
        $this->Supplier = ClassRegistry::init('Supplier');
        if ($cf != null && $cf != '') {
            $numberOfCf = $this->Supplier->find('count', ['conditions' => ['cf' => $cf, 'Supplier.state' => 1, 'Supplier.company_id' => MYCOMPANY]]);
        } else {
            $numberOfCf = 0;
        }

        return $numberOfCf;
    }


    public function checkCarrierPivaDuplicate($piva)
    {
        $this->Carrier = ClassRegistry::init('Carrier');
        if ($piva != null && $piva != '') {
            $numberOfPiva = $this->Carrier->find('count', ['conditions' => ['piva' => $piva, 'Carrier.state' => 1, 'Carrier.company_id' => MYCOMPANY]]);
        } else {
            $numberOfPiva = 0;
        }
        return $numberOfPiva;
    }


    public function checkCarrierCfDuplicate($cf)
    {
        $this->Carrier = ClassRegistry::init('Carrier');
        if ($cf != null && $cf != '') {
            $numberOfCf = $this->Carrier->find('count', ['conditions' => ['cf' => $cf, 'Carrier.state' => 1, 'Carrier.company_id' => MYCOMPANY]]);
        } else {
            $numberOfCf = 0;
        }

        return $numberOfCf;
    }

    /** ARTICOLI DEI DOCUMENTI **/

    // Controllo se l'articoo è una nota
    public function isANote($articleToCheck, $typeOfDocument)
    {
        switch ($typeOfDocument) {
            case 'bill':
                // L'articolo è una nota
                if (!isset($articleToCheck['tipo'])) {
                    $tipo = true;
                } else {
                    if ($articleToCheck['tipo'] == '') {
                        $tipo = true;
                    } else {
                        $tipo = false;
                    }
                }


                if ($tipo && $articleToCheck['quantita'] == '' && $articleToCheck['prezzo'] == '' && $articleToCheck['movable'] == '') {
                    return true;
                } else // E' un articolo o una voce descrittiva
                {
                    return false;
                }
                break;
            case 'quote':
                // L'articolo è una nota
                if ($articleToCheck['quantity'] == '' && $articleToCheck['quote_good_row_price'] == '' && $articleToCheck['movable'] == '') {
                    return true;
                } else // E' un articolo o una voce descrittiva
                {
                    return false;
                }
                break;
            case 'transport':
                // L'articolo è una nota
                if ($articleToCheck['quantita'] == '' && $articleToCheck['prezzo'] == '' && $articleToCheck['movable'] == '') {
                    return true;
                } else // E' un articolo o una voce descrittiva
                {
                    return false;
                }
                break;
        }
    }

    public function isWeekend($date)
    {
        return (date('N', strtotime($date)) >= 6);
    }

    public function getNightTimeHourAndMinutes($OI, $OF)
    {
        $this->Setting = ClassRegistry::init('Setting');
        $settings = $this->Setting->GetMySettings();

        $NI = $settings['Setting']['start_nighttime'];
        $NF = $settings['Setting']['end_nighttime'];

        // OF - OI
        $OF_OI = $this->differenceInMinutesNotFormatted($OI, $OF);
        $OI_NI = $this->differenceInMinutesNotFormatted($NI, $OI);
        $OI_NF = $this->differenceInMinutesNotFormatted($NF, $OF);
        $OF_NF = $this->differenceInMinutesNotFormatted($NF, $OF);
        $NF_OI = $this->differenceInMinutesNotFormatted($OI, $NF);
        $NI_OI = $this->differenceInMinutesNotFormatted($OI, $NI);
        $NI_OF = $this->differenceInMinutesNotFormatted($OF, $NI);

        // $OF_NI = $this->differenceInMinutesNotFormatted($startNightTime, $endNightTime);
        $OF_NI = $this->differenceInMinutesNotFormatted($NI, $OF);

        $OF_ZZ = $this->differenceInMinutesNotFormatted('00.00', $OF);
        $NF_ZZ = $this->differenceInMinutesNotFormatted('00.00', $NF);
        $ZZ_NI = $this->differenceInMinutesNotFormatted($NI, '23.59');
        $ZZ_OI = $this->differenceInMinutesNotFormatted($OI, '23.59');

        // ALGORITMO


        //if ($startTime < $endTime) {
        if ($OF_OI < 0) {
            //if ($startTime < $startNightTime) {
            if ($OI_NI <= 0) {
                //if ($startTime < $endNightTime) {
                if ($OI_NF <= 0) {
                    // CASO A
                    $result = ($NF_ZZ + $ZZ_NI);
                } else {
                    // CASO B
                    $result = ($OF_ZZ + $ZZ_NI);
                }
            } else {
                // if ($endTime > $endNightTime) {
                if ($OF_NF >= 0) {
                    // CASO C
                    $result = ($NF_ZZ + $ZZ_OI);
                } else {
                    // CASO D
                    $result = ($ZZ_OI + $OF_ZZ);
                }
            }
        } else {
            if ($NI_OI > 0 && $NI_OF > 0) {
                if ($OI <= $NF) {
                    if ($OF <= $NF) {
                        // CASO E
                        $result = $OF_OI;
                    } else {
                        // CASO F
                        $result = $NF_OI;
                    }
                } else {
                    // CASO G
                    $result = 0;

                }
            } else {
                //if ($startTime < $startNightTime) {
                if ($OI_NI < 0) {
                    // CASO H
                    $result = $OF_NI;
                } else {
                    // CASO I
                    $result = $OF_OI;
                }
            }
        }

        //return $this->fromHourToHourAndMinutes($result);
        return $result;
    }

    /* public function getNightTimeHourAndMinutes($startTime,$endTime)
    {
        $this->Setting = ClassRegistry::init('Setting');
        $settings = $this->Setting->GetMySettings();

        if ($startTime != null && $endTime != null)
        {
            $startNightTime = $settings['Setting']['start_nighttime'];
            $endNightTime =  $settings['Setting']['end_nighttime'];

            $startnighttime_startTime = $this->differenceInMinutesNotFormatted($startNightTime ,$startTime);
            $startnighttime_endtime  = $this->differenceInMinutesNotFormatted($startNightTime ,$endTime);

            $endNightTime_startTime = $this->differenceInMinutesNotFormatted($endNightTime,$startTime);
            $endNightTime_endTime = $this->differenceInMinutesNotFormatted($endNightTime,$endTime);

            $minute_endTime_midnight = $this->differenceInMinutesNotFormatted($endTime,'23:59');
            $minute_startTime_midnight = $this->differenceInMinutesNotFormatted($startTime,'23:59');
            $minute_midnight_startTime = $this->differenceInMinutesNotFormatted('00:00',$startTime);
            $minute_midnight_endTime = $this->differenceInMinutesNotFormatted('00:00',$endTime);

            $fromStarttoEnd = $this->differenceInMinutesNotFormatted($startTime,$endTime);
            $fromStartNighttimeToMidnight = $this->differenceInMinutesNotFormatted($startNightTime,'23.59');
            $fromMidnitghToEndNightTime  = $this->differenceInMinutesNotFormatted('00.00',$endNightTime);


            // Orario inizio START 19:00 Fine 07:00
            // Caso A: 10:00 / 11:00 --> 0 Ok
            // Caso B: 10:00 / 20:00 --> 60 OK
            // Caso C: 20:00 / 22:00 --> 120 OK
            // Caso D: 18:00 / 05:00 --> 300 + 300 NO
            // Caso E: 22:00 / 05:00 --> 120 + 300 = 420 OK
            // Caso F: 06:00 / 22:00 --> 120 OK
            // Caso G: 22:00 / 10:00 --> 540 OK
            // Caso H: 18:00 / 10:00 --> 300+420 =720
            // Caso I: 04:00 / 06:00 --> 120





            $result = '';
            // Se sia inizio che fine giornata sono inferiori all'inizio dell'orario notturno della gioranta

            if($startnighttime_startTime < 0 && $startnighttime_endtime  < 0 ) {
                // Caso 1
               // if ($startnighttime_startTime < $startnighttime_endtime) {
                if($endNightTime_startTime > 0 && $endNightTime_endTime > 0)
                {
                    /* NOTHING */
    /*                   $result = 0;
                    // $result = 'CASO A : ' . $result;
//                    debug("CASOA");
                }
                else if($endNightTime_startTime < 0 && $endNightTime_endTime < 0)
                {
                    $result = 0;
  //                  debug("CASOY");
                }
                else {
                    /* NOTHING */
    /*        $result = $minute_midnight_endTime - $minute_midnight_startTime;
                    // $result = 'CASO I : ' . $result;

                }

                if ($startnighttime_startTime > $startnighttime_endtime)
                {
                    if ($fromMidnitghToEndNightTime > $minute_midnight_endTime) {
                        //$result = $fromStartNighttimeToMidnight + $fromEndNighttimeToMidnight + 2;
                        $result = $fromStartNighttimeToMidnight + $minute_midnight_endTime + 1 ;
                      //  $result = "CASO D: " . $result;
         //               debug("CASOD");
                    }
                    else {
                        $result = $fromStartNighttimeToMidnight + $fromMidnitghToEndNightTime +1;
                       // $result = "CASO H: " . $result;
         //               debug("CASOH");
                    }
                }
            }

            // Se la fine è dopo l'inizio dell'orario notturno ma non del giorno dopo
            if($startnighttime_startTime < 0 && $startnighttime_endtime  > 0 ) {
                if ($endNightTime_startTime > 0) {
                    if ($minute_endTime_midnight < $minute_startTime_midnight) {
                        $result = $startnighttime_endtime;
                        //  $result = 'CASO B : ' . $startnighttime_endtime;
                        debug("CASOB");
                    }
                } else {
                    if ($minute_endTime_midnight < $minute_startTime_midnight) {
                        $result = $startnighttime_endtime;
                        if ($endNightTime_startTime < 0) {
                            $result = $result - $endNightTime_startTime;
                        }
                     //   $result = 'CASO F : ' . $result;
                        debug("CASOF");

                    }
                }
            }

            // Se inizio e fine dopo l'iinzio dell'orario notturno ma non del giorno dopo
            if($startnighttime_startTime > 0 && $startnighttime_endtime  > 0 )
            {
                $result = $fromStarttoEnd;
                //$result = 'CASO C : ' . $fromStarttoEnd;
   //             debug("CASOC");
            }

            if($startnighttime_startTime > 0 && $startnighttime_endtime  < 0 )
            {

                if($fromStartNighttimeToMidnight > $minute_startTime_midnight  )
                {
                    $result = $minute_startTime_midnight +1;
                }

                if($fromMidnitghToEndNightTime > $minute_midnight_endTime)
                {
                    $result = $result +  $minute_midnight_endTime;
                    //$result = 'CASO E: ' . $result;
            //        debug("CASOE");
                }
                else
                    {
                        $result = $result +  $fromMidnitghToEndNightTime;
                    //$result = 'CASO G:' . $result;
                  //      debug("CASOG");
                }
            }

      //      debug($result);
            // return $this->fromHourToHourAndMinutes($result);

             return $result;
        }
    } */

    public function fromMinutesToHours($minutes)
    {
        return intval($minutes / 60, 10);
    }

    public function fromHourToHourAndMinutes($minutes)
    {
        $a = $minutes . ' - ';
        $hour = intval($minutes / 60, 10);
        $minutes = $minutes - $hour * 60;

        if ($hour == 0 && $minutes == 0) {
            return "";
        }

        return $hour . ' ore ' . $minutes . ' min';

    }

    public function differencesInMinutes($timeFrom, $timeTo, $returnFormatted = true)
    {
        $hours = date("H", strtotime($timeTo)) - date("H", strtotime($timeFrom));
        $minutes = date("i", strtotime($timeTo)) - date("i", strtotime($timeFrom));

        if (date("i", strtotime($timeFrom)) > date("i", strtotime($timeTo))) {
            $hours--;
            $minutes += 60;
        }

        if ($hours < 0) {
            $hours_today = date("H", strtotime('23:59')) - date("H", strtotime($timeFrom));
            $minutes_today = date("i", strtotime('23:59')) - date("i", strtotime($timeFrom));
            $hours_next_day = date("H", strtotime($timeTo)) - date("H", strtotime('00:01'));
            $minutes_next_day = date("i", strtotime($timeTo)) - date("i", strtotime('00:01'));
            $hours = $hours_today + $hours_next_day;

            $minutes = $minutes_today + $minutes_next_day + 2;
            if ($minutes >= 60) {
                $hoursFromMinues = $this->fromMinutesToHours($minutes);
                //return $hoursFromMinues;
                $minutes = $minutes - $hoursFromMinues * 60;
                $hours += $hoursFromMinues;
            }
        }

        if ($hours == 0 && $minutes == 0) {
            return "";
        }

        if ($returnFormatted == true) {
            return str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT);
        } else {
            return $hours * 60 + $minutes;
        }
    }


    public function formatTimeInHourAndMinutes($minutes)
    {
        $hours = 0;
        if ($minutes >= 60) {
            $hoursFromMinues = $this->fromMinutesToHours($minutes);
            //return $hoursFromMinues;
            $minutes = $minutes - $hoursFromMinues * 60;
            $hours += $hoursFromMinues;
        }
        return str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT);
    }

    public function differenceInMinutesNotFormatted($timeFrom, $timeTo)
    {
        $hours = date("H", strtotime($timeTo)) - date("H", strtotime($timeFrom));
        $minutes = date("i", strtotime($timeTo)) - date("i", strtotime($timeFrom));

        if (date("i", strtotime($timeFrom)) > date("i", strtotime($timeTo))) {
            $hours--;
            $minutes += 60;
        }

        return $hours * 60 + $minutes;
    }

    public function calculateTechnicianCost($hourAndMinutes, $cost)
    {
        return number_format((str_pad($hourAndMinutes, 0, 2) * $cost) + (str_pad($hourAndMinutes, 0, 2) / 60 * $cost), 2, ',', '.');
    }

    public function toDo($message)
    {
        //debug('Ho del lavoro da fare: ' .  $message );
    }

    public function getEinvoiceTypeOfPerson()
    {
        $this->Einvoicetypeofperson = ClassRegistry::init('Einvoicetypeofperson');
        $einvoicetypeofpersons = $this->Einvoicetypeofperson->find('all');
        foreach ($einvoicetypeofpersons as $einvoicetypeofperson) {
            $arrayCausals[$einvoicetypeofperson['Einvoicetypeofperson']['id']] = $einvoicetypeofperson['Einvoicetypeofperson']['code'] . ' - ' . $einvoicetypeofperson['Einvoicetypeofperson']['description'];
        }
        return $arrayCausals;
    }

    public function completeBill($newBill, $arrayBillGoods, $isOrder = false)
    {
        $this->Iva = ClassRegistry::init('Iva');
        $this->Storage = ClassRegistry::init('Storage');

        $newBill['Bill'] = $newBill['Bills'];

        unset($newBill['Bills']);

        if(!isset($newBill['Bill']['accompagnatoria']))
        {
            $newBill['Bill']['accompagnatoria'] = 0;
        }

        if(!isset($newBill['Bill']['collectionfeesvatid']))
        {
            $newBill['Bill']['collectionfeesvatid'] = 0;
        }

        if(!isset($newBill['Bill']['payment_id']))
        {
            $newBill['Bill']['payment_id'] = 0;
        }

        $newBill['Bill']['split_payment'] == 1 ? $splitPayment = true : $splitPayment = false;

        $numberOfMovement = 0;
        $prezzo_riga = $importo_iva = $imponibile_iva = $prezzo_riga = $ritenutaAcconto = $imponibileRitenuta = $totaleRitenutaAcconto = 0;  // Aggiunto ritenuta acconto
        $arrayIva = $arrayImponibili = [];

        // bolli
        isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;
        // Arrotondamento
        isset($newBill['Bill']['rounding']) && $newBill['Bill']['rounding'] != null ? $rounding = $newBill['Bill']['rounding'] : $rounding = 0;

        foreach ($arrayBillGoods as $key => $good)
        {
            $good = $good['Good'];

            $numberOfMovement++;

            $newVat = $this->Iva->find('first', ['conditions' => ['Iva.id' => $good['iva_id']]]);

            if (!isset($good['discount']))
                $good['discount'] = 0;

            if (MULTICURRENCY && isset($good['currencyprice']) && $good['currencyprice'] != null)
                $prezzo_riga = $this->calculateBillRowWithDiscount($good['quantita'], $good['currencyprice'], $good['discount']);
            else
                $prezzo_riga = $this->calculateBillRowWithDiscount($good['quantita'], $good['prezzo'], $good['discount']);

            if (isset($newVat['Iva']['percentuale']))
                $importo_iva += $this->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);

            $imponibile_iva += $prezzo_riga;

            if (isset($newVat['Iva']['percentuale']))
            {
                $calcoloIva = $this->calculateVat($prezzo_riga, $newVat['Iva']['percentuale']);
                isset($arrayIva[$good['iva_id']]['iva']) ? $arrayIva[$good['iva_id']]['iva'] += $calcoloIva : $arrayIva[$good['iva_id']]['iva'] = $calcoloIva;
            }

            isset($arrayImponibili[$good['iva_id']]['imponibile']) ? $arrayImponibili[$good['iva_id']]['imponibile'] += $prezzo_riga : $arrayImponibili[$good['iva_id']]['imponibile'] = $prezzo_riga;

            if(!$isOrder){
                if ($good['storage_id'] != '')
                {
                    if ($this->Storage->find('count', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'Storage.id' => $good['storage_id']]]) > 0)
                    {
                        if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1)
                        {
                            if (ADVANCED_STORAGE_ENABLED)
                            {
                                $theStorageId = $good['storage_id'];
                                $this->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                            }
                            else
                            {
                                $theStorageId = $good['storage_id'];
                                $this->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                            }
                        }
                    }
                }
                else
                {
                    if (!$this->isANote($good, 'bill'))
                    {
                        if ($this->notExistInStorage($good['storage_id']))
                        {
                            $newStorage = $this->createNewStorageFromGood($good, $newBill['Bill']['client_id'], null);
                            $this->Good->updateAll(['Good.storage_id' => $newStorage['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $good['id']]);

                            if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1)
                            {
                                if (ADVANCED_STORAGE_ENABLED)
                                {
                                    if (isset($newStorage['Storage']['id']))
                                    {
                                        $theStorageId = $newStorage['Storage']['id'];
                                        $this->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                        $this->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BE_AD');
                                    }
                                }
                                else
                                {
                                    if (isset($newStorage['Storage']['id']))
                                    {
                                        $theStorageId = $newStorage['Storage']['id'];
                                        $this->storageLoad($theStorageId, $good['quantita'], 'Carico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                        $this->storageUnload($theStorageId, $good['quantita'], 'Scarico (oggetto nuovo di magazzino) fattura accompagnatoria. n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BE_AD');
                                    }
                                }
                            }
                        }
                        else  // altrimenti scarico
                        {
                            $currentGood = $this->Storage->find('first', ['conditions' => ['Storage.company_id' => MYCOMPANY, 'descrizione' => $good['oggetto']]]);
                            $this->Good->updateAll(['Good.storage_id' => $currentGood['Storage']['id'], 'Good.company_id' => MYCOMPANY], ['Good.id' => $good['id']]);

                            if ($newBill['Bill']['tipologia'] == 1 && $newBill['Bill']['accompagnatoria'] == 1)  // fattura accompagnatoria
                            {
                                $theStorageId = $currentGood['Storage']['id'];

                                if (ADVANCED_STORAGE_ENABLED)
                                    $this->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), $this->Utilities->getLastBuyPrice($theStorageId), $newBill['Bill']['deposit_id'], $numberOfMovement, 'BI_ED');
                                else
                                    $this->storageUnload($theStorageId, $good['quantita'], 'Scarico fattura accompagnatoria n.' . $newBill['Bill']['numero_fattura'] . ' del ' . date('d-m-Y', strtotime($newBill['Bill']['date'])), 0, -1, $numberOfMovement, 'BI_ED');
                            }
                        }
                    }
                }
            }


            // Storno ritenuta acconto su oggetti senza withholdingtaxappliance
            isset($imponibileStornoRitenutaAcconto) ? null : $imponibileStornoRitenutaAcconto = 0;
            if (isset($good['withholdingapplied']))
            {
                if ($good['withholdingapplied'] == 0)
                {
                    if ($good['quantita'] != 0)
                        $imponibileStornoRitenutaAcconto += number_format($good['quantita'] * $good['prezzo'], 2, '.', '');
                }
            }
        }

        if(isset($newBill['Bill']['collectionfees']))
            $collectionFees = $newBill['Bill']['collectionfees'];
        else
            $collectionFees = 0;

        if ($newBill['Bill']['collectionfeesvatid'] > 0)
        {
            $collectionfeesvat = $this->getVatPercentageFromId($newBill['Bill']['collectionfeesvatid']);
            $collectionfeesvat = $collectionFees * ($collectionfeesvat / 100);
            $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
            $importo_iva += $collectionfeesvat;
        }
        else
        {
            $imponibile_iva += $collectionFees; // <-- Aggiunto calcolo ritenuta su spese d'incasso 20/01/2019
        }

        // Inizio : cassa previdenziale
        if (isset($newBill['Bill']['welfare_box_percentage']))
        {
            $welfareBoxTotal = number_format(($imponibile_iva - $imponibileStornoRitenutaAcconto) * $newBill['Bill']['welfare_box_percentage'] / 100, 2, '.', '');
            $imponibile_iva += $welfareBoxTotal;
            $importo_iva += number_format($welfareBoxTotal * $this->getVatPercentageFromId($newBill['Bill']['welfare_box_vat_id']) / 100, 2, '.', '');
        }
        // Fine : cassa previdenziale

        isset($newBill['Bill']['seal']) ? $seal = $newBill['Bill']['seal'] : $seal = 0;

        //  Calcolo della ritenuta d'acconto
        if (isset($newBill['Bill']['withholding_tax']))
        {
            if ($newBill['Bill']['withholding_tax'] > 0)
            {
                $totaleRitenutaAcconto = $newBill['Bill']['withholding_tax'] * $imponibile_iva / 100;

                // Se non si applica la ritenuta d'acconto sulla cassa previdenziale
                if ($newBill['Bill']['welfare_box_withholding_appliance'] != 1 && isset($welfareBoxTotal))
                    $totaleRitenutaAcconto = $totaleRitenutaAcconto - ($newBill['Bill']['withholding_tax'] * $welfareBoxTotal / 100);

                // Risoluzione ritenuta d'acconto
                if (isset($imponibileStornoRitenutaAcconto) && $imponibileStornoRitenutaAcconto > 0)
                    $totaleRitenutaAcconto += (-$imponibileStornoRitenutaAcconto * $this->request->data['Bill']['withholding_tax'] / 100);

            }
            else
            {
                $totaleRitenutaAcconto = 0;
            }
        }
        else
        {
            $totaleRitenutaAcconto = 0;
        }

        // Totale fattura nel caso di split payment o no

        // SEAL OLD VS SEAL NEW

        $importo_iva = number_format($importo_iva, 2, '.', '');
        $imponibile_iva = number_format($imponibile_iva, 2, '.', '');

        if (strtotime(date("Y-m-d", strtotime($newBill['Bill']['date']))) < strtotime(date('2019-01-01')))
            $splitPayment == true ? $totalForDeadline = $imponibile_iva + $seal - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva + $seal - $totaleRitenutaAcconto;
        else
            $splitPayment == true ? $totalForDeadline = $imponibile_iva - $totaleRitenutaAcconto : $totalForDeadline = $importo_iva + $imponibile_iva - $totaleRitenutaAcconto;

        $totalForDeadline = number_format($totalForDeadline + $rounding, 2, '.', '');

        // Recupero se è abilitato il flag agosto dicembre
        $enableAugustDecember = $this->getDefaultClientDeadlineAugustSeptemberFlag($newBill['Bill']['client_id']);

        $this->setDeadlines($newBill['Bill']['date'], $newBill['Bill']['payment_id'], $totalForDeadline, $newBill['Bill']['id'], $newBill['Bill']['id'], $enableAugustDecember);
    }

    public function updateLastLoginDate($userId)
    {
        $this->User = ClassRegistry::init('User');
        $user = $this->User->find('first',['conditions' => ['User.id' => $userId]]);
        unset($user['User']['password']);
        $user['User']['lastLoginDate'] = date("Y-m-d H:i:s");
        $this->User->save($user);
    }
}
