<?php
App::uses('AppModel', 'Model');

class Quote extends AppModel
{
    public $useTable = 'quotes';

    public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Constructionsite' => ['className' => 'Constructionsite', 'foreignKey' => 'constructionsite_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $hasMany = [
        'Quotegoodrow' => ['className' => 'Quotegoodrow', 'foreignKey' => 'quote_id', 'conditions' => '', 'fields' => '', 'order' => '']
    ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0, 'company_id' => MYCOMPANY], ['Quote.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Quote.id' => $id, 'Quote.state' => 0]]) != null;
    }

    public $virtualFields = ['quotefield' => 'CONCAT("N." , Quote.quote_number, "  del " ,  DATE_FORMAT(Quote.quote_date, \'%d-%m-%Y\')," - ",Quote.description)'];

    public function getList()
    {
		$this->Quote = ClassRegistry::init('Quote');
        return $this->Quote->find('list', ['fields' => ['id', 'quotefield'], 'conditions' => ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.has_child' => 0]]);
    }

    public function getListByClientId($clientId)
    {
        $this->Quote = ClassRegistry::init('Quote');
        return $this->Quote->find('list', ['fields' => ['id', 'quotefield'], 'conditions' => ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.client_id' => $clientId]]);
    }

    public function getQuotesHistory($quoteId)
    {
        $conditionsArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.id' => $quoteId];
        $lastChildQuote = $this->find('first', ['conditions' => $conditionsArray]);

        $quotesArray = [];
        array_push($quotesArray, $lastChildQuote);
        $currentId = $lastChildQuote['Quote']['parent_id'];
        do {
            $fatherConditionArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.id' => $currentId];
            $parentQuote = $this->find('first', ['conditions' => $fatherConditionArray]);
            if ($parentQuote) {
                $currentId = $parentQuote['Quote']['parent_id'];
                array_push($quotesArray, $parentQuote);
            } else {
                $currentId = null;
            }
        } while ($currentId != null);
        return $quotesArray;
    }

    public function setHasChild($quoteId, $hasChild)
    {
        $conditionsArray = ['Quote.company_id' => MYCOMPANY, 'Quote.state' => 1, 'Quote.id' => $quoteId];
        $currentQuote = $this->find('first', ['conditions' => $conditionsArray]);
        $currentQuote['Quote']['has_child'] = 1;
        $this->save($currentQuote);
    }

    public function open($id)
    {
        return $this->updateAll(['Quote.state' => 1, 'Quote.company_id' => MYCOMPANY], ['Quote.id' => $id]);
    }

    public function close($id)
    {
        return $this->updateAll(['Quote.state' => 2, 'Quote.company_id' => MYCOMPANY], ['Quote.id' => $id]);
    }

    public function getNextNumber($year)
    {
        $this->Quote = ClassRegistry::init('Quote');
        $this->Utilities = ClassRegistry::init('Utilities');
        $fields = ['MAX(quote_number * 1) as maxPreventivo'];

        $conditionArray = [
            'Quote.company_id' => MYCOMPANY,
            'quote_date >= ' => $this->Utilities->getFirstDayOfYear($year),
            'quote_date <= ' => $this->Utilities->getLastDayOfYear($year),
            'Quote.state' => ATTIVO
        ];

        $quote = $this->Quote->find('first', ['fields' => $fields, 'conditions' => $conditionArray, 'order' => 'Quote.id desc']);

        if ($quote[0]['maxPreventivo'] == null)
            return 1;
        else
            return $quote[0]['maxPreventivo'] + 1;
    }
}