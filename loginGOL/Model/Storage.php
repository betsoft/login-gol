<?php
App::uses('AppModel', 'Model');

class Storage extends AppModel
{

	public $useTable = 'storages';

	public $belongsTo =
	[
        'Category' => ['className' => 'Category', 'foreignKey' => 'category_id', 'conditions' >= '', 'fields'=> '', 'order'=>''],
		'Supplier' => [ 'className' => 'Supplier','foreignKey' => 'supplier_id','conditions' => '','fields' => '','order' => ''],
		'Units' => ['className' => 'Units','foreignKey' => 'unit_id','conditions' => '','fields' => '','order' => ''],
		'Ivas' => ['className' => 'Ivas','foreignKey' => 'vat_id','conditions' => '','fields' => '','order' => ''],
	];

	public $hasMany =
	[
		'Good' => ['className' => 'Good','foreignKey' => 'storage_id','dependent' => true],
		'Catalogstorages' => ['className' => 'Catalogstorages','foreignKey' => 'storage_id','conditions' => '','fields' => '','order' => ''],
        'Storagemovement' => ['className' => 'Storagemovement','foreignKey' => 'storage_id','dependent' => true],
	];

	public $validate =
	[
		"descrizione"=>
		[
            "rule"=>["isUnique", ["descrizione", "company_id","state","codice"], false],
            "message"=>"Articolo già presente a magazzino."
        ],
        "codice"=>
		[
			"codicerule" =>
			[
            	"rule"=> [ "isUnique", ["codice", "company_id","state"], false],
            	'allowEmpty' => true,
            	"message"=>"Codice articolo già utilizzato.",
            ],
        ]
    ];

	public function hide($id)
    {
        return $this->updateAll(['Storage.state' => 0,'Storage.company_id'=>MYCOMPANY],['Storage.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Storage.id'=>$id, 'Storage.state' =>0 ]]) != null;
    }

}
