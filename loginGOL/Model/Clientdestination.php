<?php

App::uses('AppModel', 'Model');

class Clientdestination extends AppModel
{
	public $useTable = 'client_destinations';

	public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Nation' => ['className' => 'Nation','foreignKey' => 'nation_id','conditions' => '','fields' => '','order' => ''],
	];

	public function hide($id)
    {
        return $this->updateAll(['Clientdestination.state' => 0,'Clientdestination.company_id'=>MYCOMPANY],['Clientdestination.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Clientdestination.id'=>$id, 'Clientdestination.state' =>0 ]]) != null;
    }

}
