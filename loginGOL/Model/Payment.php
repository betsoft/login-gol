<?php
App::uses('AppModel', 'Model');

class Payment extends AppModel {

 public $useTable = 'payments';

	public $belongsTo =
	[
		'Bank' => ['className' => 'Bank','foreignKey' => 'bank_id','conditions' => '','fields' => '','order' => ''],
		'AlternativeBank' => ['className' => 'Bank','foreignKey' => 'alternative_bank_id','conditions' => '','fields' => '','order' => ''],
		'Einvoicepaymenttype' => ['className' => 'Einvoicepaymenttype','foreignKey' => 'einvoicepaymenttype_id','conditions' => '','fields' => '','order' => ''],
		'Einvoicepaymentmethod' => ['className' => 'Einvoicepaymentmethod','foreignKey' => 'einvoicepaymentmethod_id','conditions' => '','fields' => '','order' => ''],
	];

	public $hasMany =
	[
		'Paymentdeadlines' => ['className' => 'Paymentdeadlines','foreignKey' => 'payment_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
		'Bill' => ['className' => 'Bill','foreignKey' => 'payment_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
	];

	public $validate =
	[
		"metodo"=>
		[
            "rule"=>["isUnique", ["metodo", "company_id","state"], false],
            "message"=>"Il campo metodo di pagamento deve essere univoco."
        ]
    ];


	public function hide($id)
    {
        return $this->updateAll(['state' => 0,'Payment.company_id'=>MYCOMPANY],['Payment.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Payment.id'=>$id, 'Payment.state' =>0 ]]) != null;
    }

    public function identifyPaymentMethodType($effectDate, $billDeadlines)
    {
    	// Data scadenza
    	switch(count($billDeadlines))
    	{
    		// Scadenza , fine mese , giorno del mese sucessivo
    		case 1:
    			$deadline = $billDeadlines[0];
    			$arrayOfDeadlinesTest = [];
    			$arrayOfDeadlinesTest[0] =
    			[
    						//$arrayOfDays = [0,30,60,90,120,150,180,210,240,270,300,330,360],
    						//$arrayOfEndMonth = [true,false],
    						//$arrayOfDayOfNextMonth = [5,10,15,20]
    						 [0,30,60,90,120,150,180,210,240,270,300,330,360],
    						 [true,false],
    						 [5,10,15,20]
    			];
    		break;
    	}

    	// Inizio con la deadline singola
    	foreach($arrayOfDeadlinesTest as $test)
    	{
    		$arrayOfDays = $test[0];
    		$arrayOfEndMonth = $test[1];
    		$arrayOfDayOfNextMonth = $test[2];
    		switch (count($billDeadlines))
    		{
    			case 1:
    				// Ricerca scadenza sincola
    				foreach($arrayOfDays as $days)
    				{
    					$deadlineCalculated = date('Y-m-d', strtotime($billDeadlines[0] . ' + '.$days.' day'));
    					$billDeadlines[0] == $deadlineCalculated ? debug('TROVATA' . $deadlineCalculated) : null ;
    				}

					// Ricerca scadenza singola fine mese
    				foreach($arrayOfDays as $days)
    				{
    					$month = $days / 30 ;
            			$deadlineCalculated = date("Y-m-01",strtotime($effectDate)) ;
            			$deadlineCalculated = date('Y-m-d', strtotime($deadlineCalculated . ' + '.$month.' month'));
            			$deadlineCalculated  = date('Y-m-t',strtotime($deadlineCalculated));

            			$billDeadlines[0] == $deadlineCalculated ? debug('TROVATA2') : null;

            			// Provo col giorno del mese dopo
            			foreach($arrayOfDayOfNextMonth as $dayOfNextMonth)
            			{
	            			$deadlineCalculated2 = 	date('Y-m-d', strtotime($deadlineCalculated. ' + '.$dayOfNextMonth.' day'));
	            			$billDeadlines[0] == $deadlineCalculated2 ? debug('TROVATA' .$dayOfNextMonth . $deadlineCalculated2) : null;
            			}
    				}
    			break;
    		}
    	}

    }
}
