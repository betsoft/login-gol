<?php
App::uses('AppModel', 'Model');

class Bill extends AppModel
{
    public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Payment' => ['className' => 'Payment', 'foreignKey' => 'payment_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Carrier' => ['className' => 'Carrier', 'foreignKey' => 'carrier_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Supplier' => ['className' => 'Supplier', 'foreignKey' => 'supplier_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Iva' => ['className' => 'Iva', 'foreignKey' => 'iva_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Nation' => ['className' => 'Nation', 'foreignKey' => 'client_nation', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Sectional' => ['className' => 'Sectional', 'foreignKey' => 'sectional_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $hasMany = [
        'Good' => ['className' => 'Good', 'foreignKey' => 'bill_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Transport' => ['className' => 'Transport', 'foreignKey' => 'bill_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Deadline' => ['className' => 'Deadline', 'foreignKey' => 'bill_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Billgestionaldata' => ['className' => 'Billgestionaldata', 'foreignKey' => 'bill_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
    ];

    public function hide($id)
    {
        return $this->updateAll(['Bill.state' => 0, 'Bill.company_id' => MYCOMPANY], ['Bill.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Bill.id' => $id, 'Bill.state' => 0]]) != null;
    }


    public function UpdateBillWithCollectionFees($billId, $collectionfees, $vatPercentage, $nature = null)
    {
        $this->Utilities = ClassRegistry::init('Utilities');
        $this->Bill = ClassRegistry::init('Bill');
        $currentBill = $this->Bill->find('first', ['conditions' => ['Bill.id' => $billId, 'Bill.company_id' => MYCOMPANY]]);
        $currentBill['Bill']['collectionfees'] = $collectionfees;
        $currentBill['Bill']['collectionfeesvatid'] = $this->Utilities->getVatIdFromPercentage($vatPercentage, $nature);
        $this->Bill->save($currentBill);
    }

    // Creazione fattura da importazione xml elettronica
    public function CreateSupplierBillFromXmlImport($billNumber, $billDate, $supplier, $datiGeneraliDocumento = null, $ritenuta = null, $bollo = null, $cassa = null, $scontiMaggiorazione = null, $datiGeneraliContratto = null, $datiGeneraliOrdineAcquisto = null, $datiGeneraliConvenzione = null, $datiRiepilogo = null, $client, $totaleimporto, $totaleimposta, $datiTrasmittente, $idixfe, $datiDDT, $receiveDate)
    {
        $this->Utilities = ClassRegistry::init('Utilities');
        $this->Currencies = ClassRegistry::init('Currencies');
        $exist = $this->find('count', ['conditions' => ['Bill.numero_fattura' => $billNumber, 'Bill.date' => $billDate, 'Bill.supplier_id' => $supplier, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO], 'Bill.Tipologia' => 2]);

        if ($exist == 0)
        {
            $bill = [];
            $bill['numero_fattura'] = $billNumber;
            $bill['date'] = $billDate;
            $bill['receive_date'] = $receiveDate;
            $bill['supplier_id'] = $supplier['id'];
            $bill['importo'] = $totaleimporto;
            $bill['imported_bill'] = 1; // Indico che è una fattura importata
            $bill['id_ixfe'] = $idixfe;
            $bill['supplier_name'] = $supplier['name'];
            $bill['supplier_address'] = $supplier['address'];
            $bill['supplier_cap'] = $supplier['cap'];
            $bill['supplier_city'] = $supplier['city'];
            $bill['supplier_nation'] = $supplier['nation'];
            $bill['supplier_province'] = $supplier['province'];
            $bill['supplier_rea_number'] = $supplier['rea_number'];
            $bill['supplier_rea_office'] = $supplier['rea_office'];
            $bill['supplier_share_capital'] = $supplier['share_capital'];

            // Dati Cliente
            $bill['client_name'] = $client['Denominazione'];
            $bill['client_address'] = $client['Indirizzo'];
            $bill['client_cap'] = $client['CAP'];
            $bill['client_city'] = $client['Comune'];
            $bill['client_province'] = $client['Provincia'];
            $bill['client_nation'] = $this->Utilities->getNationIdFromShortCode($client['IdPaese']) != null ? $this->Utilities->getNationIdFromShortCode($client['IdPaese']) : 0;

            // Altir dati fattura
            isset($datiGeneraliDocumento['Causale']) ? $bill['document_causal'] = $datiGeneraliDocumento['Causale'] : null;
            isset($datiGeneraliDocumento['Art73']) ? $bill['art73'] = $datiGeneraliDocumento['Art73'] : null;
            isset($datiGeneraliDocumento['Divisa']) ? $bill['currency_id'] = $this->Currencies->GetCurrencyIdFromCode($datiGeneraliDocumento['Divisa']) : null;
            isset($datiGeneraliDocumento['TipoDocumento']) ? $bill['typeofdocument'] = $datiGeneraliDocumento['TipoDocumento'] : null;
            isset($datiTrasmittente['CodiceDestinatario']) ? $bill['destcode'] = $datiTrasmittente['CodiceDestinatario'] : null;
            isset($datiRiepilogo[0]['EsigibilitaIVA']) ? $bill['einvoicevatesigibility'] = $datiRiepilogo[0]['EsigibilitaIVA'] : null;
            isset($datiRiepilogo[0]['Arrotondamento']) ? $bill['rounding'] = $datiRiepilogo[0]['Arrotondamento'] : null;

            // IMPORTO BOLLO
            isset($bollo['ImportoBollo']) ? $bill['seal'] = $bollo['ImportoBollo'] : null;

            // _TODO_
            $bill['company_id'] = MYCOMPANY;
            $bill['state'] = 1;
            $bill['tipologia'] = 2;  // Anche per le note di credito

            $this->create();
            $newBill = $this->Save($bill);

            // Salvo i dati del riepilogo
            foreach ($datiRiepilogo as $riepilogo)
            {
                $ritenutaAcconto = [];
                $cassaprevidenziale = [];

                isset($riepilogo['AliquotaIVA']) ? $aliquotaIVA = $riepilogo['AliquotaIVA'] : $aliquotaIVA = null;
                isset($riepilogo['ImponibileImporto']) ? $imponibileImporto = $riepilogo['ImponibileImporto'] : $imponibileImporto = null;
                isset($riepilogo['Imposta']) ? $imposta = $riepilogo['Imposta'] : $imposta = null;
                isset($riepilogo['RiferimentoNormativo']) ? $riferimentoNormativo = $riepilogo['RiferimentoNormativo'] : $riferimentoNormativo = null;
                isset($riepilogo['Natura']) ? $natura = $riepilogo['Natura'] : $natura = null;
                isset($riepilogo['Arrotondamento']) ? $arrotondamento = $riepilogo['Arrotondamento'] : $arrotondamento = null;
                isset($riepilogo['EsigibilitaIVA']) ? $einvoicevatesigibility = $riepilogo['EsigibilitaIVA'] : $einvoicevatesigibility = null;
                // Ritenuta acconto
                isset($ritenuta['AliquotaRitenuta']) ? $ritenutaAcconto['importedwithholdingpercentage'] = $ritenuta['AliquotaRitenuta'] : null;
                isset($ritenuta['ImportoRitenuta']) ? $ritenutaAcconto['importedwithholdingvalue'] = $ritenuta['ImportoRitenuta'] : null;
                // Cassa previdenziale
                isset($cassa['AlCassa']) ? $cassaprevidenziale['importedwelfareboxpercentage'] = $cassa['AlCassa'] : null;
                isset($cassa['ImportoContributoCassa']) ? $cassaprevidenziale['importedwelfareboxvalue'] = $cassa['ImportoContributoCassa'] : null;
                isset($cassa['AliquotaIVA']) ? $cassaprevidenziale['importedwelfareboxvatpercentage'] = $cassa['AliquotaIVA'] : null;
                isset($cassa['Ritenuta']) ? $cassaprevidenziale['importedwelfareboxwithholdingappliance'] = $cassa['Ritenuta'] : null;
                isset($cassa['Natura']) ? $cassaprevidenziale['Natura'] = $cassa['importedwelfareboxvatnature'] : null;

                $this->Utilities->saveBillGestionalData($newBill['Bill']['id'], $aliquotaIVA, $imponibileImporto, $imposta, $riferimentoNormativo, $natura, $arrotondamento, $einvoicevatesigibility, $ritenutaAcconto, $cassaprevidenziale);
            }

            return $newBill['Bill']['id'];
        }
        else
        {
            //debug('fattura già inserita');
            return false;
        }
    }

    // Controllo se esiste già una transazione paypal all'interno del db
    public function existsPaypalTransactionId($paypalTransactionId)
    {
        $this->Bill = ClassRegistry::init('Bill');
        if ($this->find('count', ['conditions' => ['Bill.paypal_transaction_id' => $paypalTransactionId, 'Bill.company_id' => MYCOMPANY, 'Bill.state' => ATTIVO]]) > 0)
            return true;
        else
            return false;
    }

    public function createReceiptFromPaypal($paypalDataForReceipt)
    {
        $this->Bill = ClassRegistry::init('Bill');
        $this->Currencies = ClassRegistry::init('Currencies');
        $this->Good = ClassRegistry::init('Good');
        $this->Utilities = ClassRegistry::init('Utilities');

        $newBill = $this->Bill->create();

        // Recupero il numero massimo dello scontrino
        $newBill['Bill']['numero_fattura'] = $this->Utilities->getNextReceiptNumber($paypalDataForReceipt['receiptdate']); // Devo recuperare il maximent

        // Recupero il cliente (TO DO)
        $cliente = $this->Client->find('first', ['conditions' => ['Client.ragionesociale' => $paypalDataForReceipt['ragionesociale'], 'Client.company_id' => MYCOMPANY], 'fields' => ['Client.id', 'Client.currency_id']]);

        if (empty($cliente['Client']['id']))
        {
            $clientId = $this->Utilities->createClient($paypalDataForReceipt['ragionesociale'], '', '', '', '', '');
            $newBill['Bill']['client_id'] = $clientId;
        }
        else
        {
            $newBill['Bill']['client_id'] = $cliente['Client']['id'];;
        }

        $newBill['Bill']['date'] = $paypalDataForReceipt['receiptdate'];
        $newBill['Bill']['importo'] = $paypalDataForReceipt['amount'];
        $newBill['Bill']['company_id'] = MYCOMPANY;

        $newBill['Bill']['iva_id'] = -1; // Iva id come lo gestisco dovrebbe essere predeterminato per paypal (?) // Vuota può essere eliminata ?
        $newBill['Bill']['mailsent'] = 0;
        $newBill['Bill']['state'] = 1;
        $newBill['Bill']['tipologia'] = 7;
        $newBill['Bill']['payment_id'] = $this->Utilities->getDefaultReceiptPaymentMethodId();

        $newBill['Bill']['currency_id'] = $this->Currencies->GetCurrencyIdFromCode($paypalDataForReceipt['isocode']);

        $newBill['Bill']['paypal_transaction_id'] = $paypalDataForReceipt['transaction_id'];

        $newBill = $this->save($newBill);

        if (isset($paypalDataForReceipt['Details']))
        {
            foreach ($paypalDataForReceipt['Details'] as $detailed)
            {
                $vatPercentage = intval($detailed['Vat'] / $detailed['Amt'] * 100);
                $vatId = $this->Utilities->getVatIdFromPercentage($vatPercentage);

                $newGood = $this->Good->create();
                $newGood['Good']['oggetto'] = $detailed['Description'];
                $newGood['Good']['quantita'] = $detailed['Quantity'];
                $newGood['Good']['prezzo'] = $detailed['Amt'];
                $newGood['Good']['iva_id'] = $vatId;
                $newGood['Good']['bill_id'] = $newBill['Bill']['id'];
                $newGood['Good']['storage_id'] = null;
                $newGood['Good']['transport_id'] = null;
                $newGood['Good']['unita'] = '';
                $newGood['Good']['discount'] = 0;
                $newGood['Good']['codice'] = null;
                $newGood['Good']['customdescription'] = '';
                $newGood['Good']['company_id'] = MYCOMPANY;
                $newGood['Good']['vat'] = $detailed['Vat'];
                $newGood['Good']['state'] = 1;
                $newGood['Good']['tipo'] = 2; // Paypal
                $newGood['Good']['movable'] = 2; // Paypal
                $newdGood = $this->Good->Save($newGood);
            }
        }
        else
        {
            $vatPercentage = intval($detailed['Vat'] / $detailed['Amt'] * 100);
            $vatId = $this->Utilities->getVatIdFromPercentage($vatPercentage);

            $newGood = $this->Good->create();
            $newGood['Good']['oggetto'] = 'Ricevimento denaro';
            $newGood['Good']['quantita'] = 1;
            $newGood['Good']['prezzo'] = $paypalDataForReceipt['amount'];
            $newGood['Good']['iva_id'] = $vatId;
            $newGood['Good']['bill_id'] = $newBill['Bill']['id'];
            $newGood['Good']['storage_id'] = null;
            $newGood['Good']['transport_id'] = null;
            $newGood['Good']['unita'] = '';
            $newGood['Good']['discount'] = 0;
            $newGood['Good']['codice'] = null;
            $newGood['Good']['customdescription'] = '';
            $newGood['Good']['company_id'] = MYCOMPANY;
            $newGood['Good']['vat'] = 0;
            $newGood['Good']['state'] = 1;
            $newGood['Good']['tipo'] = 2; // Paypal
            $newGood['Good']['movable'] = 2; // Paypal
            $newdGood = $this->Good->Save($newGood);
        }
    }

    public function getBillTypeName($typeName)
    {
        switch ($typeName)
        {
            case 1:
                $typeDescription = 'Fattura di vendita';
                break;
            case 2:
                $typeDescription = 'Fattura d\'acquisto';
                break;
            case 3:
                $typeDescription = 'Nota di credito';
                break;
            case 8:
                $typeDescription = 'Fattura pro forma';
                break;
        }

        return $typeDescription;
    }

    public function getBillReturnAction($typeName)
    {
        switch ($typeName)
        {
            case 1:
                $action = 'index';
                break;
            case 2:
                $action = 'indexExtendedbuy';
                break;
            case 3:
                $action = 'indexExtendedcreditnotes';
                break;
            case 8:
                $action = 'indexproforma';
                break;
        }

        return $action;
    }
}
