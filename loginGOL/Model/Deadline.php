<?php
App::uses('AppModel', 'Model');

class Deadline extends AppModel {

    public $useTable = 'deadlines';
 
 	public $belongsTo = [
		'Bill' => ['className' => 'Bill','foreignKey' => 'bill_id','conditions' => '','fields' => '','order' => ''],
	];
	
	public $hasMany = [
		'Deadlinepayment' => ['className' => 'Deadlinepayment','foreignKey' => 'deadline_id','dependent' => true,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
	];

	public function setUnpaid($deadlineId,$state)
	{
		$this->Deadline = ClassRegistry::init('Deadline');
		$state == true ? $unpaid = 1 : $unpaid = 0;
		$this->Deadline->updateAll(['Deadline.unpaid'=>$unpaid],['Deadline.id'=>$deadlineId]);
	}
	
	public function getCountPayments($deadlineId)
	{
		$this->Deadlinepayment = ClassRegistry::init('Deadlinepayment');
		return  $this->Deadlinepayment->find('count',['conditions'=>[ 'Deadlinepayment.deadline_id' => $deadlineId, 'Deadlinepayment.state' => 1 ]]);
	}

	public function saveDeadline($billId, $deadline , $amount, $vat = 0, $state = 1, $unpaid = 0 , $companyId = MYCOMPANY)
	{
		if($deadline != null && $amount != null)
		{
			$this->Deadline = ClassRegistry::init('Deadline');
			$deadlineArray = [];
			$deadlineArray['bill_id'] = $billId;
			$deadlineArray['deadline'] = $deadline;
			$deadlineArray['amount'] = $amount;
            $deadlineArray['original_amount'] = $amount;
			$deadlineArray['vat'] = $vat;
			$deadlineArray['state'] = $state;
			$deadlineArray['unpaid'] = $unpaid;
			$deadlineArray['company_id'] = $companyId;
			$this->Deadline->save($deadlineArray);
		}
    }

    public function saveImportedDeadline($billId , $dettaglipagamento, $billImporto, $billDate, $vat = 0, $state = 1, $unpaid = 0 , $companyId = MYCOMPANY ,$creditNote = false)
	{
		$this->Deadline = ClassRegistry::init('Deadline');

		foreach($dettaglipagamento as $details)
		{
			foreach ($details['DettaglioPagamento'] as $rowOfDetails)
			{
                $rowOfDetails['ImportoPagamento'] == null ?  $dettaglipagamentoNew = $billImporto : $dettaglipagamentoNew = $rowOfDetails['ImportoPagamento'] ;

                $this->Deadline->create();
                $deadlineArray = [];
                $deadlineArray['bill_id'] = $billId;
                $deadlineArray['deadline'] = isset($rowOfDetails['DataScadenzaPagamento'])  ? $rowOfDetails['DataScadenzaPagamento'] : date("Y-m-d",strtotime($billDate));
                $creditNote == true ? $deadlineArray['amount'] = -1 *  $dettaglipagamentoNew : $deadlineArray['amount'] = $dettaglipagamentoNew;
                $deadlineArray['vat'] = 0;
                $deadlineArray['imported_payment_method'] = $rowOfDetails['ModalitaPagamento'];
                $deadlineArray['imported_IBAN'] = $rowOfDetails['IBAN'];
                $deadlineArray['imported_bank'] = $rowOfDetails['IstitutoFinanziario'];
                $deadlineArray['state'] = $state;
                $deadlineArray['unpaid'] = $unpaid;
                $deadlineArray['company_id'] = $companyId;
                $this->Deadline->save($deadlineArray);
			}
		}
	}

    public function getUnbalancedBill()
    {
        $conditionArray = ['Deadline.company_id'=>MYCOMPANY,'Deadline.state'=>1 ];

        $arrayOfDeadline = $this->find('all', [
            'fields'=> ['bill_id, sum(amount) as modifiedDeadline,sum(original_amount) as originalDeadline'],
            'group' => ['bill_id'],
            'conditions' =>[$conditionArray],
        ]);

        $arrayOfResult = [];
        foreach($arrayOfDeadline as $cicledResult)
        {
            if($cicledResult[0]['modifiedDeadline'] != $cicledResult[0]['originalDeadline'] && $cicledResult[0]['originalDeadline'] != null )
                $arrayOfResult[] = $cicledResult['Deadline']['bill_id'];
        }

        return  $arrayOfResult;
    }

    public function hide($id)
    {
        return $this->updateAll(['Deadline.state' => 0, 'Deadline.company_id' => MYCOMPANY], ['Deadline.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Deadline.id' => $id, 'Deadline.state' => 0]]) != null;
    }
}