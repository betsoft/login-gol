<?php
App::uses('AppModel', 'Model');

class Order extends AppModel
{
    public $useTable = 'orders';

    public $belongsTo = [
        'Client' => ['className' => 'Client', 'foreignKey' => 'client_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        //'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Sectional' => ['className' => 'Sectional', 'foreignKey' => 'sectional_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Payment' => ['className' => 'Payment', 'foreignKey' => 'payment_id', 'conditions' => '', 'fields' => '', 'order' => ''],
        'Deposit' => ['className' => 'Deposit', 'foreignKey' => 'deposit_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];

    public $hasMany = [
        'Orderrow' => ['className' => 'Orderrow', 'foreignKey' => 'order_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
    ];

    public function hide($id)
    {
        return $this->updateAll(['Order.state' => 0, 'Order.company_id' => MYCOMPANY], ['Order.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Order.id' => $id, 'Order.state' => 0]]) != null;
    }
}
