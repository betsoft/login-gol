<?php
App::uses('AppModel', 'Model');

class Maintenancehour extends AppModel {

    public $useTable = 'maintenance_hours';

    public function hide($id)
    {
        return $this->updateAll(['Maintenancehour.state' => 0, 'Maintenancehour.company_id' => MYCOMPANY],['Maintenancehour.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Maintenancehour.id'=>$id, 'Maintenancehour.state' =>0, 'Maintenancehour.company_id' => MYCOMPANY]]) != null;
    }

    public $belongsTo = [
        'Maintenance' => ['className' => 'Maintenance','foreignKey' => 'maintenance_id','conditions' => '','fields' => '','order' => ''],
    ];

    public $hasMany = [
        'Maintenancehourstechnician' => ['className' => 'Maintenancehourstechnician', 'foreignKey' => 'maintenance_hour_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
        'Maintenancehoursoutsideoperator' => ['className' => 'Maintenancehoursoutsideoperator', 'foreignKey' => 'maintenance_hours_id', 'dependent' => true, 'conditions' => '', 'fields' => '', 'order' => '', 'limit' => '', 'offset' => '', 'exclusive' => '', 'finderQuery' => '', 'counterQuery' => ''],
    ];
}