<?php
App::uses('AppModel', 'Model');

class Loadgood extends AppModel {

public $useTable = 'load_goods';

	public $belongsTo =
	[
		'Supplier' => ['className' => 'Supplier','foreignKey' => 'supplier_id','conditions' => '','fields' => '','order' => ''],
		'Deposit' => ['className' => 'Deposit','foreignKey' => 'deposit_id','conditions' => '','fields' => '','order' => ''],
	];

	public $hasMany =
	[
		'Loadgoodrow' => ['className' => 'Loadgoodrow','foreignKey' => 'load_good_id','conditions' => '','fields' => '','order' => '']
	];


	public function hide($id)
    {
        return $this->updateAll(['Loadgood.state' => 0,'Loadgood.company_id'=>MYCOMPANY],['Loadgood.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Loadgood.id'=>$id, 'Loadgood.state' =>0 ]]) != null;
    }

    public function createLoadgoodFromPurchaseOrder($purchaseOrderId)
    {
        if (MODULO_CANTIERI) {
            $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
            $currentOrder = $this->Purchaseorder->getPurchaseorder($purchaseOrderId);
            $loadGoodHeader = $this->createLoadGoodHeaderFromPurchaesOrder($currentOrder['Purchaseorder']['supplier_id'], $currentOrder['Purchaseorder']['id']);
            foreach ($currentOrder['Purchaseorderrow'] as $row) {
                $this->createLoadgoodRow($row, $loadGoodHeader['Loadgood']['id']);
            }
            $this->setLoadgoodcreated($currentOrder);
            return $loadGoodHeader['Loadgood']['id'];
        }
    }

    public function buildInvoiceFromLoadgoods($arrayOfLoadgoods, $dettagliato = false, $splitPayment = false, $electronicInvoice = false, $customBillDateForLoadgood = null)
    {
        $this->Good = ClassRegistry::init('Good');
        $this->Bills = ClassRegistry::init('Bills');
        $this->Storages = ClassRegistry::init('Storages');
        $this->Currencies = ClassRegistry::init('Currencies');
        $this->Loadgood = ClassRegistry::init('Loadgood');
        $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
        $this->Payment = ClassRegistry::init('Payment');
        $isArray = false;

        // L'array contiene almeno sempre un articolo
        $arrayOfLoadgoods = (array)$arrayOfLoadgoods;
        if(sizeOf($arrayOfLoadgoods)>1)
            $isArray = true;
        if($isArray)
            $firstLoadgood = $arrayOfLoadgoods[0]['Loadgood']['id'];
        else
            $firstLoadgood = $arrayOfLoadgoods;
        $firstLoadgoods = $this->Loadgood->find('first', ['contain' => ['Supplier', 'Loadgoodrow' => ['Storage']], 'recursive' => 2, 'conditions' => ['Loadgood.id' => $id, 'Loadgood.company_id' => MYCOMPANY]]);

        $arrayOfLoadgoodsRef = [];

        $newBill = $this->Bills->create();

        // Utilizzo i sezionali - Per fatture di vendita
        $defaultSectional = $this->getDefaultBillSectional();
        $nextSectionalNumber = $defaultSectional['Sectionals']['last_number'] + 1;
        $newBill['Bills']['numero_fattura'] = $nextSectionalNumber;
        $newBill['Bills']['sectional_id'] = $defaultSectional['Sectionals']['id'];
        $newBill['Bills']['payment_id'] = $firstLoadgood['Loadgood']['payment_id'];
        $newBill['Bills']['alternativeaddress_id'] = $firstLoadgood['Loadgood']['destination_id'];

        // Defnisco i valori per la fattura
        $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        if ($customBillDateForLoadgood != null)
            $newBill['Bills']['date'] = date("Y-m-d", strtotime($customBillDateForLoadgood)); // Data personalizzata
        else
            $newBill['Bills']['date'] = date("Y-m-d"); // La data è quella in cui viene cliccato (tanto è modifciabile)

        $newBill['Bills']['client_id'] = $firstLoadgood['Loadgood']['supplier_id']; // Il cliente è quello della prima bolla
        $newBill['Bills']['company_id'] = $firstLoadgood['Loadgood']['company_id']; // La company è la stessa delle bolle
        $newBill['Bills']['tipologia'] = 1;
        $newBill['Bills']['deposit_id'] = $firstLoadgood['Loadgood']['deposit_id'];

        // Per la valuta
        // Aggiunta x la gestione dell valute
        $NewBill['Bill']['currency_id'] = $firstLoadgood['Supplier']['currency_id'];
        if ($this->Currencies->GetCurrencyName($firstLoadgood['Supplier']) != 'EUR') {
            $NewBill['Bill']['changevalue'] = $this->Currencies->GetLastCurrencyChange($firstLoadgood['Supplier']['currency_id']);
        } else {
            $NewBill['Bill']['changevalue'] = 1;
        }

        $newBill['Bills']['order_id'] = $firstLoadgood['Loadgood']['id'];

        $electronicInvoice == 'true' ? $newBill['Bills']['einvoicevatesigibility'] = 'I' : null;

        // Shipping name
        $newBill['Bills']['client_shipping_name'] = isset($firstLoadgood['Loadgood']['supplier_name']) ? $firstLoadgood['Loadgood']['supplier_name'] : '';
        $newBill['Bills']['client_shipping_address'] = isset($firstLoadgood['Loadgood']['supplier_address']) ? $firstLoadgood['Loadgood']['supplier_address'] : '';
        $newBill['Bills']['client_shipping_cap'] = isset($firstLoadgood['Loadgood']['supplier_cap']) ? $firstLoadgood['Loadgood']['supplier_cap'] : '';
        $newBill['Bills']['client_shipping_city'] = isset($firstLoadgood['Loadgood']['supplier_city']) ? $firstLoadgood['Loadgood']['supplier_city'] : '';
        $newBill['Bills']['client_shipping_province'] = isset($firstLoadgood['Loadgood']['supplier_province']) ? $firstLoadgood['Loadgood']['supplier_province'] : '';
        $newBill['Bills']['client_shipping_nation'] = isset($firstLoadgood['Loadgood']['supplier_nation_id']) ? $firstLoadgood['Loadgood']['supplier_nation_id'] : '';

        // Shipping name
        $newBill['Bills']['client_name'] = isset($firstLoadgood['Loadgood']['supplier_name']) ? $firstLoadgood['Loadgood']['supplier_name'] : '';
        $newBill['Bills']['client_address'] = isset($firstLoadgood['Loadgood']['supplier_address']) ? $firstLoadgood['Order']['supplier_address'] : '';
        $newBill['Bills']['client_cap'] = isset($firstLoadgood['Loadgood']['supplier_cap']) ? $firstLoadgood['Loadgood']['supplier_cap'] : '';
        $newBill['Bills']['client_city'] = isset($firstLoadgood['Loadgood']['suppliert_city']) ? $firstLoadgood['Loadgood']['supplier_city'] : '';
        $newBill['Bills']['client_province'] = isset($firstLoadgood['Loadgood']['supplier_province']) ? $firstLoadgood['Loadgood']['supplier_province'] : '';
        $newBill['Bills']['client_nation'] = isset($firstLoadgood['Loadgood']['supplier_nation_id']) ? $firstLoadgood['Loadgood']['client_nation_id'] : '';

        $newBill['Bills']['referredclient_id'] = 0;

        // Split payment
        $splitPayment == 'true' ? $newBill['Bills']['split_payment'] = 1 : $newBill['Bills']['split_payment'] = 0;

        // Electronic invoice
        $electronicInvoice == 'true' ? $newBill['Bills']['electronic_invoice'] = 1 : $newBill['Bills']['electronic_invoice'] = 0;

        // Fattura da bolla dettagliata
        $dettagliato == 'true' ? $newBill['Bills']['not_detailed'] = 0 : $newBill['Bills']['not_detailed'] = 1;

        // Metto il metodo di pagamento del cliente
        isset($firstLoadgood['Supplier']['payment_id']) ? $newBill['Bills']['payment_id'] = $firstLoadgood['Supplier']['payment_id'] : null;

        $payment = $this->Payment->find('first', ['conditions' => ['Payment.id' => $firstLoadgood['Client']['payment_id'], 'Payment.state' => ATTIVO]]);
        isset($payment) && isset($payment['Payment']) ? $newBill['Bills']['collectionfees'] = $payment['Payment']['paymentFixedCost'] : null;

        $newBill = $this->Bills->save($newBill);

        $arrayBillGoods = [];
        // Ciclo l'array delle bolle
        foreach ($arrayOfLoadgoods as $loadgood) {
            // Recupero gli oggetti che hanno l'id della bolla
            if($isArray){
                $LoadgoodRow = $this->Loadgoodrow->find('all', ['conditions' => ['Loadgood_id' => $loadgood['Loadgood']['id']], 'contain' => ['Storage']]);

                $this->Loadgood->UpdateAll(['bill_id' => $newBill['Bills']['id']], ['Loadgood.id' => $loadgood['Loadgood']['id']]);
            }
            else{
                $LoadgoodRow = $this->Loadgoodrow->find('all', ['conditions' => ['Orderrow.order_id' => $loadgood], 'contain' => ['Storage']]);
                $this->Order->UpdateAll(['bill_id' => $newBill['Bills']['id']], ['Order.id' => $loadgood]);
            }
            // Aggiorno la bolla con il numero della fattura

            // Ciclo gli oggetti della bolla

            foreach ($LoadgoodRow as $row) {
                $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
                $newLoadGoodRow = $this->Loadgoodrow->create();
                $newLoadGood['Loadgood']['load_good_number'] = $this->Utilities->getNextLoadgoodNumber(date("Y"));
                $newLoadGood['Loadgood']['load_good_date'] = date("Y-m-d");
                $newLoadGood['Loadgood']['supplier_id'] = $supplierId;
                $newLoadGood['Loadgood']['company_id'] = MYCOMPANY;
                $newLoadGood['Loadgood']['state'] = 1;
                $newLoadGood['Loadgood']['purchaseorder_id'] = $purchaseOrderId;
                $this->Loadgoodrow->save($newLoadGoodRow);


            }
        }

        $this->completeBill($newBill, $arrayBillGoods, true);

        $this->updateDefaultBillSectional($newBill['Bills']['numero_fattura']);
        return $newBill['Bills']['id'];
    }

    public function createLoadGoodHeaderFromPurchaesOrder($supplierId, $purchaseOrderId)
    {
        $this->Utilities = ClassRegistry::init('Utilities');
        $newLoadGood = $this->create();
        $newLoadGood['Loadgood']['load_good_number'] = $this->Utilities->getNextLoadgoodNumber(date("Y"));
        $newLoadGood['Loadgood']['load_good_date'] = date("Y-m-d");
        $newLoadGood['Loadgood']['supplier_id'] = $supplierId;
        $newLoadGood['Loadgood']['company_id'] = MYCOMPANY;
        $newLoadGood['Loadgood']['state'] = 1;
        $newLoadGood['Loadgood']['purchaseorder_id'] = $purchaseOrderId;
        return $this->save($newLoadGood);
    }

    public function createLoadgoodrow($PurchaseOrderRow, $loadGoodId)
    {
        $this->Loadgoodrow = ClassRegistry::init('Loadgoodrow');
        $newLoadGoodRow = $this->Loadgoodrow->create();
        $newLoadGoodRow['Loadgoodrow']['load_good_id'] = $loadGoodId;
        $newLoadGoodRow['Loadgoodrow']['description'] = $PurchaseOrderRow['description'];
        $newLoadGoodRow['Loadgoodrow']['unit_of_measure_id'] = $PurchaseOrderRow['unit_of_measure_id'];
        $newLoadGoodRow['Loadgoodrow']['load_good_row_price'] = $PurchaseOrderRow['price'];
        $newLoadGoodRow['Loadgoodrow']['load_good_row_vat_id'] = $PurchaseOrderRow['vat_id'];
        $newLoadGoodRow['Loadgoodrow']['storage_id'] = $PurchaseOrderRow['storage_id'];
        $newLoadGoodRow['Loadgoodrow']['article_type'] = $PurchaseOrderRow['article_type'];
        $newLoadGoodRow['Loadgoodrow']['quantity'] = $PurchaseOrderRow['quantity'];
        $newLoadGoodRow['Loadgoodrow']['state'] = 1;
        $newLoadGoodRow['Loadgoodrow']['company_id'] = MYCOMPANY;
        $this->Loadgoodrow->save($newLoadGoodRow);
    }

    public function setLoadgoodCreated($currentOrder)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $currentOrder['Purchaseorder']['loadgoodcreated'] = 1;
        $this->Purchaseorder->save($currentOrder);
    }

}
