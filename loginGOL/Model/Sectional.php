<?php
App::uses('AppModel', 'Model');

class Sectional extends AppModel 
{
	public $useTable = 'sectionals';

	public $validate = 
	[
		"description"=>
		[
            "rule"=>["isUnique", ["description", "company_id","state"], false], 
            "message"=>"Il campo descrizione deve essere univoco." 
        ]
    ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ,'company_id'=>MYCOMPANY]]) != null;
    }

    public function reduceSectionalValue($sectionalId, $deletedBillNumber)
    {
        $conditionArray = ['id' => $sectionalId, 'company_id' => MYCOMPANY];
        $currentSectional = $this->find('first', ['conditions' => $conditionArray]);
        $fieldsArray = ['last_number' => $currentSectional['Sectional']['last_number'] - 1];
        
        if($deletedBillNumber == $currentSectional['Sectional']['last_number'])
            $this->updateAll($fieldsArray,$conditionArray);
    }
}
