<?php
App::uses('AppModel', 'Model');
class Withholding extends AppModel
{
    public $useTable = 'einvoice_welfare_box';

    public $validate =
        [
            "description" =>
                [
                    "rule" => ["isUnique", ["description", "state"], false],
                    "message" => "Il campo descrizione deve essere univoco."
                ]
        ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0], ['id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['id' => $id, 'state' => 0]]) != null;
    }

    public function getList()
    {
        $this->Withholding = ClassRegistry::init('Withholding');
        $fieldsArray = ['Withholding.description'];
        $orderArray = ['Withholding.description' => 'asc'];
        return $this->Withholding->find('list', ['conditions' => $conditionArray, 'fields' => $fieldsArray, 'order' => $orderArray]);
    }
}
