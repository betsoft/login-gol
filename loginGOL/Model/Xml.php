<?php

App::uses('AppModel', 'Model');

class Xml extends AppModel
{
    public function createNewEbillFromXml($billData, $idixfe = null, $receiveDate = null)
    {
        $this->autoRender = false;

        $this->Bill = ClassRegistry::init('Bill');
        $this->Supplier = ClassRegistry::init('Supplier');
        $this->Good = ClassRegistry::init('Good');
        $this->Payment = ClassRegistry::init('Payment');
        $this->Einvoicepaymentmethod = ClassRegistry::init('Einvoicepaymentmethod');
        $this->Einvoicepaymenttype = ClassRegistry::init('Einvoicepaymenttype');
        $this->Deadline = ClassRegistry::init('Deadline');
        $this->Utilities = ClassRegistry::init('Utilities');
        $this->Goodgestionaldata = ClassRegistry::init('Goodgestionaldata');
        $this->Billgestionaldata = ClassRegistry::init('Billgestionaldata');

        try
        {
            $newReceiveDate = date("Y-m-d", strtotime($receiveDate));

            $billData = str_replace('<![CDATA[', '', $billData);
            $billData = str_replace(']]>', '', $billData);

            $xmlObjectData = $this->xmlToObject($billData);

            try
            {
                // Recupero la versione pubblico o privato
                $isPublicAdministration = $this->getXmlVersion($xmlObjectData);
            }
            catch (exception $ecc)
            {
                debug($ecc);die;
            }

            // 1 Recupero l'intestazione della fattura
            $billHeader = $this->getEbillHeader($xmlObjectData);
            // 1.1 Dati trasmissione
            $billHeaderTrasmissione = $this->getEbillHeaderTrasmissione($billHeader);
            // 1.1.1.1 Dati trasmissione - IdTrasmittente - IdPaese
            $datiTrasmittente['IdPaese'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'IdPaese');
            // 1.1.1.2 Dati trasmissione - IdTrasmittente - IdCodice
            $datiTrasmittente['IdCodice'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'IdCodice');
            // 1.1.2 Dati trasmissione - Progressivo
            $datiTrasmittente['ProgressivoInvio'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'ProgressivoInvio');
            // 1.1.3 Dati trasmissione - Formato Trasmissione
            $datiTrasmittente['FormatoTrasmissione'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'FormatoTrasmissione');
            // 1.1.4 Dati trasmissione - Codice destinatario
            $datiTrasmittente['CodiceDestinatario'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'CodiceDestinatario');
            // 1.1.5 Dati trasmissione - Contatti Trasmittente
            // 1.1.5.1 Dati trasmissione - Contatti Trasmittente - Telefono
            $datiTrasmittente['Telefono'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'Telefono');
            // 1.1.5.2 Dati trasmissione - Contatti Trasmittente - Email
            $datiTrasmittente['Email'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'Email');
            // 1.1.6 Pec Destinatario
            $datiTrasmittente['PECDestinatario'] = $this->getdatiTrasmissione($billHeaderTrasmissione, 'PECDestinatario');

            // *** IMPORTAZIONE *** Controllo che siamo noi i corretti destinatari della fattura

            // Colui che effettua la fattura cioè il mio fornitore

            // Recupero dati cedente prestatore
            $billHeaderCedentePrestatore = $this->getEbillHeaderCedentePrestatore($billHeader);

            // Recupero le varie parti
            $datiAnagraficiCedentePrestatore = $this->getEBillHeaderCedentePrestatoreDatiAnagrafici((array)$billHeaderCedentePrestatore);

            $datiCedentePrestatore['IdPaese'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'IdPaese');
            $datiCedentePrestatore['IdCodice'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'IdCodice');
            $datiCedentePrestatore['CodiceFiscale'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'CodiceFiscale');
            $datiCedentePrestatore['Denominazione'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'Denominazione');
            $datiCedentePrestatore['Nome'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'Nome');
            $datiCedentePrestatore['Cognome'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'Cognome');
            $datiCedentePrestatore['RegimeFiscale'] = $this->getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, 'RegimeFiscale');

            $datiSedeCedentePrestatore = $this->getEBillHeaderCedentePrestatoreSede((array)$billHeaderCedentePrestatore);

            $datiCedentePrestatore['Indirizzo'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'Indirizzo');
            $datiCedentePrestatore['NumeroCivico'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'NumeroCivico');
            $datiCedentePrestatore['CAP'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'CAP');
            $datiCedentePrestatore['Comune'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'Comune');
            $datiCedentePrestatore['Provincia'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'Provincia');
            $datiCedentePrestatore['Nazione'] = $this->getCedentePrestatoreSede($datiSedeCedentePrestatore, 'Nazione');

            $datiIscrizioneReaCedentePrestatore = $this->getEbillHeaderCedentePrestatoreIscrizioneRea((array)$billHeaderCedentePrestatore);

            $datiCedentePrestatore['Ufficio'] = $this->getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, 'Ufficio');
            $datiCedentePrestatore['NumeroREA'] = $this->getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, 'NumeroREA');
            $datiCedentePrestatore['CapitaleSociale'] = $this->getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, 'CapitaleSociale');
            $datiCedentePrestatore['SocioUnico'] = $this->getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, 'SocioUnico');
            $datiCedentePrestatore['StatoLiquidazione'] = $this->getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, 'StatoLiquidazione');

            $datiContattiCedentePrestatore = $this->getEbillHeaderCedentePrestatoreContatti((array)$billHeaderCedentePrestatore);

            $datiCedentePrestatore['Telefono'] = $this->getCedentePrestatoreContatti($datiContattiCedentePrestatore, 'Telefono');
            $datiCedentePrestatore['Fax'] = $this->getCedentePrestatoreContatti($datiContattiCedentePrestatore, 'Fax');
            $datiCedentePrestatore['Email'] = $this->getCedentePrestatoreContatti($datiContattiCedentePrestatore, 'Email');

            $billHeaderCessionarioCommittente = $this->getEbillHeaderCessionarioCommittente((array)$billHeader);

            $datiAnagraficiCessionarioCommittente = $this->getEbillHeaderDatiAnagraficiCessionarioCommittente((array)$billHeaderCessionarioCommittente);

            $datiCessionarioCommittente['IdPaese'] = $this->getDatiAnagraficiCessionarioCommittente($datiAnagraficiCessionarioCommittente, 'IdPaese');
            $datiCessionarioCommittente['IdCodice'] = $this->getDatiAnagraficiCessionarioCommittente($datiAnagraficiCessionarioCommittente, 'IdCodice');
            $datiCessionarioCommittente['Denominazione'] = $this->getDatiAnagraficiCessionarioCommittente($datiAnagraficiCessionarioCommittente, 'Denominazione');

            $datiSedeCessionarioCommittente = $this->getEbillHeaderCessionarioCommittenteSede((array)$billHeaderCessionarioCommittente);

            $datiCessionarioCommittente['Indirizzo'] = $this->getCessionarioCommittenteSede($datiSedeCessionarioCommittente, 'Indirizzo');
            $datiCessionarioCommittente['CAP'] = $this->getCessionarioCommittenteSede($datiSedeCessionarioCommittente, 'CAP');
            $datiCessionarioCommittente['Comune'] = $this->getCessionarioCommittenteSede($datiSedeCessionarioCommittente, 'Comune');
            $datiCessionarioCommittente['Provincia'] = $this->getCessionarioCommittenteSede($datiSedeCessionarioCommittente, 'Provincia');
            $datiCessionarioCommittente['Nazione'] = $this->getCessionarioCommittenteSede($datiSedeCessionarioCommittente, 'Nazione');

            // Recupero Il corpo della fattura
            $billBody = $this->getEbillBody($xmlObjectData);

            // (2.1) RECUPERO TUTTI I DATI GENERALI DEL DOCUMENTO
            $billBodyDatiGenerali = $this->getEbillBodyDatiGenerali($billBody);

            // (2.1.1) DATI GENERALI DEL DOCUMENTO
            $billBodyDatiGeneraliDatiGeneraliDocumento = $this->getEbillBodyDatiGeneraliDatiGeneraliDocumento((array)$billBodyDatiGenerali);
            // (2.1.1.1) Tipo documento
            $datiGeneraliDocumento['TipoDocumento'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'TipoDocumento');
            // (2.1.1.2) Divisa / valuta
            $datiGeneraliDocumento['Divisa'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'Divisa');
            // (2.1.1.3) DAta
            $datiGeneraliDocumento['Data'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'Data');
            // (2.1.1.4) Numero
            $datiGeneraliDocumento['Numero'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'Numero');
            // (2.1.1.5) Dati Ritenuta
            $billBodyDatiGeneralidatiGeneraliRitenuta = $this->getEbillBodyDatiGeneraliDatiRitenuta((array)$billBodyDatiGeneraliDatiGeneraliDocumento);
            // (2.1.1.5.1) Tipo Ritenuta
            $datiGeneraliRitenuta['TipoRitenuta'] = $this->getDatiGeneraliDatiRitenuta($billBodyDatiGeneralidatiGeneraliRitenuta, 'TipoRitenuta');
            // (2.1.1.5.2) Tipo Ritenuta
            $datiGeneraliRitenuta['ImportoRitenuta'] = $this->getDatiGeneraliDatiRitenuta($billBodyDatiGeneralidatiGeneraliRitenuta, 'ImportoRitenuta');
            // (2.1.1.5.3) Tipo Ritenuta
            $datiGeneraliRitenuta['AliquotaRitenuta'] = $this->getDatiGeneraliDatiRitenuta($billBodyDatiGeneralidatiGeneraliRitenuta, 'AliquotaRitenuta');
            // (2.1.1.5.4) Tipo Ritenuta
            $datiGeneraliRitenuta['CausalePagamento'] = $this->getDatiGeneraliDatiRitenuta($billBodyDatiGeneralidatiGeneraliRitenuta, 'CausalePagamento');
            // (2.1.1.6) Dati Bollo
            $billBodyDatiGeneraliDatiBollo = $this->getEbillBodyDatiGeneraliBollo((array)$billBodyDatiGeneraliDatiGeneraliDocumento);
            // (2.1.1.6.1)
            $datiGeneraliBollo['BolloVirtuale'] = $this->getDatiGeneraliDatiBollo($billBodyDatiGeneraliDatiBollo, 'BolloVirtuale');
            // (2.1.1.6.2)
            $datiGeneraliBollo['ImportoBollo'] = $this->getDatiGeneraliDatiBollo($billBodyDatiGeneraliDatiBollo, 'ImportoBollo');
            // (2.1.1.7) Dati Cassa Previdenziale
            $billBodyDatiGeneraliCassaPrevidenziale = $this->getEbillBodyDatiGeneraliDatiCassaPrevidenziale((array)$billBodyDatiGeneraliDatiGeneraliDocumento);
            // (2.1.1.7.1) Tipo Cassa
            $datiCassaPrevidenziale['TipoCassa'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'TipoCassa');
            // (2.1.1.7.2) Aliquota Cassa
            $datiCassaPrevidenziale['AlCassa'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'AlCassa');
            // (2.1.1.7.3) Importo Contributo Cassa
            $datiCassaPrevidenziale['ImportoContributoCassa'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'ImportoContributoCassa');
            // (2.1.1.7.4) Imponibile Cassa
            $datiCassaPrevidenziale['ImponibileCassa'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'ImponibileCassa');
            // (2.1.1.7.5) Aliquota Iva
            $datiCassaPrevidenziale['AliquotaIVA'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'AliquotaIVA');
            // (2.1.1.7.6) Ritenuta
            $datiCassaPrevidenziale['Ritenuta'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'Ritenuta');
            // (2.1.1.7.7) Natura
            $datiCassaPrevidenziale['Natura'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'Natura');
            // (2.1.1.7.8) RiferimentoAmministrazione
            $datiCassaPrevidenziale['RiferimentoAmministrazione'] = $this->getDatiGeneraliDatiCassaPrevidenziale($billBodyDatiGeneraliCassaPrevidenziale, 'RiferimentoAmministrazione');
            // (2.1.1.8) Dati Sconti / Maggiorazioni
            $billBodyDatiGeneraliScontoMaggiorazione = $this->getEbillBodyDatiGeneraliDatiSconti((array)$billBodyDatiGeneraliDatiGeneraliDocumento);
            // (2.1.1.8.1) Tipo
            $datiScontoMaggiorazione['Tipo'] = $this->getDatiGeneraliscontoMaggiorazione($billBodyDatiGeneraliScontoMaggiorazione, 'Tipo');
            // (2.1.1.8.2) Percentuale
            $datiScontoMaggiorazione['Percentuale'] = $this->getDatiGeneraliscontoMaggiorazione($billBodyDatiGeneraliScontoMaggiorazione, 'Percentuale');
            // (2.1.1.8.1) Importo
            $datiScontoMaggiorazione['Importo'] = $this->getDatiGeneraliscontoMaggiorazione($billBodyDatiGeneraliScontoMaggiorazione, 'Importo');

            // (2.1.1.9) Importo totale documento
            $datiGeneraliDocumento['ImportoTotaleDocumento'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'ImportoTotaleDocumento');
            // (2.1.1.10) Arrotondamento
            // TO DO ?
            // (2.1.1.11) Causale [1..n]
            // $datiGeneraliDocumento['Causale'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento,'Causale');
            $datiGeneraliDocumentoCausale = $this->getDatiGeneraliDatiGeneraliCausale((array)$billBodyDatiGeneraliDatiGeneraliDocumento);

            $arrayCausals = [];
            $ac = -1;

            if ($datiGeneraliDocumentoCausale != null)
            {
                if (is_array($datiGeneraliDocumentoCausale)) // Se è solo un array di causali
                {
                    foreach ($datiGeneraliDocumentoCausale as $causale)
                    {
                        $ac++;
                        // Salvo le causali
                        $arrayCausals[$ac] = $causale;
                    }
                }
                else // Se è solo una causale
                {
                    $ac++;
                    // Salvo le causali
                    $arrayCausals[$ac] = $datiGeneraliDocumentoCausale;
                }
            }

            // (2.1.1.12) Art73
            $datiGeneraliDocumento['Art73'] = $this->getDatiGeneraliDatiGeneraliDocumento($billBodyDatiGeneraliDatiGeneraliDocumento, 'Art73');

            $billBodyDatiGeneraliDatiOrdineAcquisto = $this->getEbillBodyDatiGeneraliDatiOrdineAcquisto((array)$billBodyDatiGenerali);

            !is_array($billBodyDatiGeneraliDatiOrdineAcquisto) ? $billBodyDatiGeneraliDatiOrdineAcquisto = [$billBodyDatiGeneraliDatiOrdineAcquisto] : null;

            $arrayordine = [];

            foreach ($billBodyDatiGeneraliDatiOrdineAcquisto as $key => $ordine)
            {
                /** Fix per array riferimentolinea **/
                $ordine2 = $this->xml2array($ordine);
                $riferimentoLineaOrdine = $this->getDatiGeneraliDatiContrattoRiferimentoNumeroLinea($ordine2);
                unset($ordine2['RiferimentoNumeroLinea']);

                if (is_array($riferimentoLineaOrdine) && isset($riferimentoLineaOrdine[0]))
                {
                    foreach ($riferimentoLineaOrdine as $key2 => $lineaOrdine)
                    {
                        $lineaOrdine = 0 + $lineaOrdine;
                        $arrayordine[$lineaOrdine]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($ordine2, 'IdDocumento');
                        $arrayordine[$lineaOrdine]['Data'] = $this->getDatiGeneraliDatiContratto($ordine2, 'Data');
                        $arrayordine[$lineaOrdine]['NumItem'] = $this->getDatiGeneraliDatiContratto($ordine2, 'NumItem');
                        $arrayordine[$lineaOrdine]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($ordine2, 'CodiceCommessaConvenzione');
                        $arrayordine[$lineaOrdine]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($ordine2, 'CodiceCUP');
                        $arrayordine[$lineaOrdine]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($ordine2, 'CodiceCIG');
                        $arrayordine[$lineaOrdine]['RiferimentoNumeroLinea'] = $lineaOrdine;
                        $arrayordine[$lineaOrdine]['document_type'] = 0;
                    }
                }
                else
                {
                    $arrayordine[$key]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($ordine, 'IdDocumento');
                    $arrayordine[$key]['Data'] = $this->getDatiGeneraliDatiContratto($ordine, 'Data');
                    $arrayordine[$key]['NumItem'] = $this->getDatiGeneraliDatiContratto($ordine, 'NumItem');
                    $arrayordine[$key]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($ordine, 'CodiceCommessaConvenzione');
                    $arrayordine[$key]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($ordine, 'CodiceCUP');
                    $arrayordine[$key]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($ordine, 'CodiceCIG');
                    $arrayordine[$key]['RiferimentoNumeroLinea'] = 0 + $this->getDatiGeneraliDatiContratto($ordine, 'RiferimentoNumeroLinea');
                    $arrayordine[$key]['document_type'] = 0;
                }
            }

            // (2.1.3) DATI DEL CONTRATTO
            $billBodyDatiGeneraliDatiContratto = $this->getEbillBodyDatiGeneraliDatiContratto((array)$billBodyDatiGenerali);

            !is_array($billBodyDatiGeneraliDatiContratto) ? $billBodyDatiGeneraliDatiContratto = [$billBodyDatiGeneraliDatiContratto] : null;

            // (2.1.X) vari
            $arraycontratto = [];
            foreach ($billBodyDatiGeneraliDatiContratto as $key => $contratto)
            {
                /** Fix per array riferimentolinea **/
                $contratto2 = $this->xml2array($contratto);
                $riferimentoLineaContratto = $this->getDatiGeneraliDatiContrattoRiferimentoNumeroLinea($contratto2);
                unset($contratto2['RiferimentoNumeroLinea']);

                if (is_array($riferimentoLineaContratto) && isset($riferimentoLineaContratto[0]))
                {
                    foreach ($riferimentoLineaContratto as $lineaContratto)
                    {
                        $lineaContratto = 0 + $lineaContratto;
                        $arrayordine[$lineaContratto]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($contratto2, 'IdDocumento');
                        $arrayordine[$lineaContratto]['Data'] = $this->getDatiGeneraliDatiContratto($contratto2, 'Data');
                        $arrayordine[$lineaContratto]['NumItem'] = $this->getDatiGeneraliDatiContratto($contratto2, 'NumItem');
                        $arrayordine[$lineaContratto]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($contratto2, 'CodiceCommessaConvenzione');
                        $arrayordine[$lineaContratto]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($contratto2, 'CodiceCUP');
                        $arrayordine[$lineaContratto]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($contratto2, 'CodiceCIG');
                        $arrayordine[$lineaContratto]['RiferimentoNumeroLinea'] = $lineaContratto;
                        $arrayordine[$lineaContratto]['document_type'] = 0;
                    }
                }
                else
                {
                    $arraycontratto[$key]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($contratto, 'IdDocumento');
                    $arraycontratto[$key]['Data'] = $this->getDatiGeneraliDatiContratto($contratto, 'Data');
                    $arraycontratto[$key]['NumItem'] = $this->getDatiGeneraliDatiContratto($contratto, 'NumItem');
                    $arraycontratto[$key]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($contratto, 'CodiceCommessaConvenzione');
                    $arraycontratto[$key]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($contratto, 'CodiceCUP');
                    $arraycontratto[$key]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($contratto, 'CodiceCIG');
                    $arraycontratto[$key]['RiferimentoNumeroLinea'] = $this->getDatiGeneraliDatiContratto($contratto, 'RiferimentoNumeroLinea');
                    $arraycontratto[$key]['document_type'] = 1;
                }
            }

            // (2.1.4) DATI CONVENZIONE
            $billBodyDatiGeneraliDatiConvenzione = $this->getEbillBodyDatiGeneraliConvenzione((array)$billBodyDatiGenerali);

            !is_array($billBodyDatiGeneraliDatiConvenzione) ? $billBodyDatiGeneraliDatiConvenzione = [$billBodyDatiGeneraliDatiConvenzione] : null;

            // (2.1.X) vari
            $arrayconvenzione = [];
            foreach ($billBodyDatiGeneraliDatiConvenzione as $key => $convenzione)
            {
                /** Fix per array riferimentolinea **/
                $convenzione2 = $this->xml2array($convenzione);
                $riferimentoLineaConvenzione = $this->getDatiGeneraliDatiContrattoRiferimentoNumeroLinea($convenzione2);
                unset($convenzione2['RiferimentoNumeroLinea']);

                if (is_array($riferimentoLineaConvenzione) && isset($riferimentoLineaConvenzione[0]))
                {
                    foreach ($riferimentoLineaConvenzione as $key2 => $lineaConvenzione)
                    {
                        $lineaConvenzione = 0 + $lineaConvenzione;
                        $arrayordine[$lineaConvenzione]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'IdDocumento');
                        $arrayordine[$lineaConvenzione]['Data'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'Data');
                        $arrayordine[$lineaConvenzione]['NumItem'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'NumItem');
                        $arrayordine[$lineaConvenzione]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'CodiceCommessaConvenzione');
                        $arrayordine[$lineaConvenzione]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'CodiceCUP');
                        $arrayordine[$lineaConvenzione]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($convenzione2, 'CodiceCIG');
                        $arrayordine[$lineaConvenzione]['RiferimentoNumeroLinea'] = $lineaConvenzione;
                        $arrayordine[$lineaConvenzione]['document_type'] = 0;
                    }
                }
                else
                {
                    $arrayconvenzione[$key]['IdDocumento'] = $this->getDatiGeneraliDatiContratto($convenzione, 'IdDocumento');
                    $arrayconvenzione[$key]['Data'] = $this->getDatiGeneraliDatiContratto($convenzione, 'Data');
                    $arrayconvenzione[$key]['NumItem'] = $this->getDatiGeneraliDatiContratto($convenzione, 'NumItem');
                    $arrayconvenzione[$key]['CodiceCommessaConvenzione'] = $this->getDatiGeneraliDatiContratto($convenzione, 'CodiceCommessaConvenzione');
                    $arrayconvenzione[$key]['CodiceCUP'] = $this->getDatiGeneraliDatiContratto($convenzione, 'CodiceCUP');
                    $arrayconvenzione[$key]['CodiceCIG'] = $this->getDatiGeneraliDatiContratto($convenzione, 'CodiceCIG');
                    $arrayconvenzione[$key]['RiferimentoNumeroLinea'] = $this->getDatiGeneraliDatiContratto($convenzione, 'RiferimentoNumeroLinea');
                    $arrayconvenzione[$key]['document_type'] = 2;
                }
            }

            // (2.1.5) DATI RICEZIONE
            // _TODO_
            // (2.1.6) DATI FATTURE COLLEGATE
            // _TODO_
            // (2.1.7) DATISAL
            // _TODO_
            // (2.1.7.1) Riferimento Fase
            // (2.1.8) DATI DDT  [0..N]
            $billBodyDatiGeneraliDDt = $this->getEbillBodyDatiGeneraliDatiDDT((array)$billBodyDatiGenerali);

            !is_array($billBodyDatiGeneraliDDt) ? $billBodyDatiGeneraliDDt = [$billBodyDatiGeneraliDDt] : null;

            // (2.1.8.1) Numero DDT
            //$datiGeneraliDDT['NumeroDDT'] = $this->getDatiGeneraliDDT($billBodyDatiGeneraliDDt,'NumeroDDT');
            // (2.1.8.2) Data DDT
            //$datiGeneraliDDT['DataDDT'] = $this->getDatiGeneraliDDT($billBodyDatiGeneraliDDt,'DataDDT');
            // (2.1.8.3) Riferimento Numero Linea
            //$datiGeneraliDDT['RiferimentoNumeroLinea'] = $this->getDatiGeneraliDDT($billBodyDatiGeneraliDDt,'RiferimentoNumeroLinea');

            $arrayDDT = [];
            foreach ($billBodyDatiGeneraliDDt as $DDT)
            {
                $riferimentolinea = $this->getRiferimentoLineaDdt((array)$DDT);
                !is_array($DDT) ? $riferimentolinea = [$riferimentolinea] : null;
                $numeroDDT = $this->getDatiGeneraliDDT($DDT, 'NumeroDDT');
                $dataDDT = $this->getDatiGeneraliDDT($DDT, 'DataDDT');

                if (isset($riferimentolinea[0]) && is_array($riferimentolinea[0]))
                {
                    foreach ($riferimentolinea[0] as $RF)
                    {
                        $arrayDDT[$RF]['NumeroDDT'] = $numeroDDT;
                        $arrayDDT[$RF]['DataDDT'] = $dataDDT;
                        $arrayDDT[$RF]['RiferimentoNumeroLinea'] = $RF;
                    }
                }
                else
                {
                    $arrayDDT[$riferimentolinea[0]]['NumeroDDT'] = $numeroDDT;
                    $arrayDDT[$riferimentolinea[0]]['DataDDT'] = $dataDDT;
                    $arrayDDT[$riferimentolinea[0]]['RiferimentoNumeroLinea'] = $riferimentolinea[0];
                }
            }

            // Recupero i dati beni e servizi
            $billBodyBeniEServizi = $this->getEbillBodyDatiBeniEServizi($billBody);
            // Qui vanno messi 1..n dettaglio linea

            $billBodyDatiBeniEServiziDettagliLinee = $this->getEbillBodyDatiBeniEServiziDettaglioLinee((array)$billBodyBeniEServizi);

            !is_array($billBodyDatiBeniEServiziDettagliLinee) ? $billBodyDatiBeniEServiziDettagliLinee = [$billBodyDatiBeniEServiziDettagliLinee] : null;

            $totaleImporto = $totaleImposta = 0;
            foreach ($billBodyDatiBeniEServiziDettagliLinee as $key => $linea)
            {
                // 2.2.1 Dettaglio linee
                $datiGenerali[$key]['NumeroLinea'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'NumeroLinea');
                // 2.2.1.2 Tipo cessione prestazione
                // ATTENZIONE !! Queste non sono state messe nella crezione
                // !!! Sconto [SC] Premio [PR] Abbuono [AB] Spesa accessoria [AC]
                $datiGenerali[$key]['TipoCessionePrestazione'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'TipoCessionePrestazione');
                // 2.2.1.3 Codice Articolo
                $articolo = $this->getDatiBeniEServiziDettaglioLineeCodiceArticolo((array)$linea);

                if ($articolo != null)
                {
                    !is_array($articolo) ? $articolo = [$articolo] : null;
                    $totaleCodiceValore = '';

                    foreach ($articolo as $keyArt => $art)
                    {
                        $codiceTipo = '';
                        $codiceValore = '';

                        // 2.2.1.3.1 Codice Tipo
                        $codiceTipo = $this->getDatiBeniEServiziDettaglioLineeCodiceArticoloDettaglio($art, 'CodiceTipo');
                        $codiceTipo == null ? $codiceTipo = '' : null;

                        //  $totaleCodiceValore .= '['.$codiceTipo.']';
                        $codiceValore == null ? $codiceValore = '' : null;

                        // 2.2.1.3.2 Codice Valore
                        $codiceValore = $this->getDatiBeniEServiziDettaglioLineeCodiceArticoloDettaglio($art, 'CodiceValore');

                        $codiceTipo != '' ? $codiceTipo = '(' . $codiceTipo . ')' : null;

                        if ($totaleCodiceValore == '' && ($codiceTipo != '' || $codiceValore != ''))
                        {
                            $totaleCodiceValore = $codiceValore . ' ' . $codiceTipo;
                        }
                        else
                        {
                            if ($totaleCodiceValore != '' && ($codiceTipo != '' || $codiceValore != ''))
                                $totaleCodiceValore .= ' <br/>' . $codiceValore . '  ' . $codiceTipo;
                        }
                    }

                    $datiGenerali[$key]['CodiceValore'] = $totaleCodiceValore;
                }

                // 2.2.1.4 Descrizione
                $lineadescrittiva = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Descrizione');
                if (strlen($lineadescrittiva) > 255)
                {
                    $datiGenerali[$key]['Descrizione'] = substr($lineadescrittiva, 0, 254);
                    $datiGenerali[$key]['customdescription'] = substr($lineadescrittiva, 254, strlen($lineadescrittiva) - 255);
                }
                else
                {
                    $datiGenerali[$key]['Descrizione'] = $lineadescrittiva;
                    $datiGenerali[$key]['customdescription'] = '';
                }

                // 2.2.1.5 Quantità
                $datiGenerali[$key]['Quantita'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Quantita');
                // 2.2.1 6 Unita Di misura
                $datiGenerali[$key]['UnitaMisura'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'UnitaMisura');
                // 2.2.1.7 // ATTENZIONE !! Queste non sono state messe nella crezione
                $datiGenerali[$key]['DataFinePeriodo'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'DataFinePeriodo');

                // 2.2.1.8 // ATTENZIONE !! Queste non sono state messe nella crezione
                $datiGenerali[$key]['DataInizioPeriodo'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'DataInizioPeriodo');
                // 2.2.1.9  Prezzo Unitario
                $datiGenerali[$key]['PrezzoUnitario'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'PrezzoUnitario');
                // 2.2.1.10 SConto Maggiorazione
                //$datiGenerali[$key]['ScontoMaggiorazione'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Percentuale');
                // ATTENZIONE !! Controllare se sono salvati

                // Tipo [SC] [MG]  percentuale e importo
                // ATTENZIONE !! Controllare se sono salvati
                // 2.2.1.10.1 Tipo
                $datiGenerali[$key]['Tipo'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Tipo');
                // 2.2.1.10.2 Percentuale
                $datiGenerali[$key]['Percentuale'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Percentuale', 0);
                $datiGenerali[$key]['Sconto2'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Percentuale', 1);
                // 2.2.1.10.3 Importo
                $datiGenerali[$key]['Importo'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Importo'); // Questo perché non lo mostro ?

                // 2.2.1.11
                $datiGenerali[$key]['PrezzoTotale'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'PrezzoTotale');
                // 2.2.1.12
                $datiGenerali[$key]['AliquotaIVA'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'AliquotaIVA');
                // 2.2.1.13
                // Attenzione controllare se ce l'ho in salvataggio
                $datiGenerali[$key]['Ritenuta'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Ritenuta');
                // 2.2.1.14
                // Viene valorizzato unicamente quando non ci sono aliquote iva definite
                $datiGenerali[$key]['Natura'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'Natura');
                // 2.2.1.15
                // ATTENZIONE !! Controllare se sono salvati
                $datiGenerali[$key]['RiferimentoAmministrazione'] = $this->getDatiBeniEServiziDettaglioLinee($linea, 'RiferimentoAmministrazione');
                // 2.2.1.16  Recupero i dati gestionali
                $datigestionali = $this->getDatiBeniEServiziDettaglioLineeAltriDatiGestionali((array)$linea);

                if ($datigestionali != null) // Questi sono n
                {
                    !is_array($datigestionali) ? $datigestionali = [$datigestionali] : null;
                    $arrayDatiGestionali = 0;
                    foreach ($datigestionali as $gest)
                    {
                        // 2.2.1.16.1 TipoDato
                        $datiGenerali[$key]['DettaglioDatiGestionali'][$arrayDatiGestionali]['TipoDato'] = $this->getDatiBeniEServiziDettaglioLineeDatiGestionaleDettaglio($gest, 'TipoDato');
                        // 2.2.1.16.2 Riferimento Testo
                        $datiGenerali[$key]['DettaglioDatiGestionali'][$arrayDatiGestionali]['RiferimentoTesto'] = $this->getDatiBeniEServiziDettaglioLineeDatiGestionaleDettaglio($gest, 'RiferimentoTesto');
                        // 2.2.1.16.3 Riferimento Numero
                        $datiGenerali[$key]['DettaglioDatiGestionali'][$arrayDatiGestionali]['RiferimentoNumero'] = $this->getDatiBeniEServiziDettaglioLineeDatiGestionaleDettaglio($gest, 'RiferimentoNumero');
                        // 2.2.1.16.3 Riferimento Data
                        $datiGenerali[$key]['DettaglioDatiGestionali'][$arrayDatiGestionali]['RiferimentoData'] = $this->getDatiBeniEServiziDettaglioLineeDatiGestionaleDettaglio($gest, 'RiferimentoData');
                        $arrayDatiGestionali++;
                    }
                }
            }

            // ************************************************************************************************************************************
            // ********************************************** CREAZIONE ARCHIVIAZIONE *************************************************************
            // ************************************************************************************************************************************

            // Controllo la tipologia del documento
            switch ($datiGeneraliDocumento['TipoDocumento'])
            {
                // Nel caso sia una fattura di vendita allora la importo anche nota di credito
                case 'TD01':
                case 'TD02':
                case 'TD04':
                case 'TD06':
                case 'TD16':
                case 'TD24':
                case 'TD26':
                    // *** IMPORTAZIONE *** Recupero id del fornitore

                    // Recupero l'id del fornitore
                    if($datiCedentePrestatore['Denominazione'] != '' && $datiCedentePrestatore['Denominazione'] != null)
                        $supplier['name'] = $datiCedentePrestatore['Denominazione'];
                    else
                        $supplier['name'] = $datiCedentePrestatore['Nome'] . ' ' . $datiCedentePrestatore['Cognome'];

                    $supplierId = $this->Supplier->getSupplierIdFromXml($datiCedentePrestatore['IdCodice'], $datiCedentePrestatore['CodiceFiscale'], trim($supplier['name'], "\t\n\r\0\x0B"), $datiCedentePrestatore);

                    // *** IMPORTAZIONE *** Creo la fattura d'acquisto
                    $supplier['id'] = $supplierId;
                    $supplier['address'] = $datiCedentePrestatore['Indirizzo'] . ' ' . $datiCedentePrestatore['NumeroCivico'];
                    $supplier['cap'] = $datiCedentePrestatore['CAP'];
                    $supplier['city'] = $datiCedentePrestatore['Comune'];
                    $supplier['province'] = $datiCedentePrestatore['Provincia'];
                    $supplier['rea_number'] = $datiCedentePrestatore['NumeroREA'];
                    $supplier['rea_office'] = $datiCedentePrestatore['Ufficio'];
                    $supplier['share_capital'] = $datiCedentePrestatore['CapitaleSociale'];
                    $supplier['nation'] = $this->Utilities->getNationIdFromShortCode($datiCedentePrestatore['Nazione']);

                    $billNumber = $datiGeneraliDocumento['Numero'];
                    $billDate = $datiGeneraliDocumento['Data'];
                break;
            }

            // ******************************* FINE ARCHIVIAZIONE ***********************************************************/

            $billBodyDatiBeniEServiziRiepilogo = $this->getEbillBodyDatiBeneiEServiziDatiRiepilogo((array)$billBodyBeniEServizi);

            // Se sono presenti più servizi (dovrebbe essere ok così)
            if (isset($billBodyDatiBeniEServiziRiepilogo[1]))
            {
                foreach ($billBodyDatiBeniEServiziRiepilogo as $key => $riepilogo)
                {
                    $datiRiepilogo[$key]['AliquotaIVA'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'AliquotaIVA');
                    $datiRiepilogo[$key]['Natura'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Natura');
                    $datiRiepilogo[$key]['ImponibileImporto'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'ImponibileImporto');
                    $datiRiepilogo[$key]['Imposta'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Imposta');
                    $datiRiepilogo[$key]['EsigibilitaIVA'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'EsigibilitaIVA');
                    $datiRiepilogo[$key]['RiferimentoNormativo'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'RiferimentoNormativo');
                    $datiRiepilogo[$key]['Arrotondamento'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Arrotondamento');

                    $totaleImporto += $datiRiepilogo[$key]['ImponibileImporto'] + $datiRiepilogo[$key]['Imposta'];
                    $totaleImposta += $datiRiepilogo[$key]['Imposta'];
                }
            }
            else // Altrimenti ne ho uno solo
            {
                $riepilogo = $billBodyDatiBeniEServiziRiepilogo;
                $datiRiepilogo[0]['AliquotaIVA'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'AliquotaIVA');
                $datiRiepilogo[0]['Natura'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Natura');
                $datiRiepilogo[0]['ImponibileImporto'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'ImponibileImporto');
                $datiRiepilogo[0]['Imposta'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Imposta');
                $datiRiepilogo[0]['EsigibilitaIVA'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'EsigibilitaIVA');
                $datiRiepilogo[0]['RiferimentoNormativo'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'RiferimentoNormativo');
                $datiRiepilogo[0]['Arrotondamento'] = $this->getDatiBeniEServiziDatiRiepilogo($riepilogo, 'Arrotondamento');

                $totaleImporto += $datiRiepilogo[0]['ImponibileImporto'] + $datiRiepilogo[0]['Imposta'];
                $totaleImposta += $datiRiepilogo[0]['Imposta'];
            }

            // Recupero tutti gli array di pagamento
            $BillBodyPagamento = $this->getEbillBodyDatiPagamento((array)$billBody);

            !is_array($BillBodyPagamento) ? $BillBodyPagamento = [$BillBodyPagamento] : null;

            foreach ($BillBodyPagamento as $key => $pagamento)
            {
                $pagamento = $this->xml2array($pagamento);

                $billBodyDatiPagamentoCondizioniPagamento = $this->getEbillBodyDatiPagamentoCondizioniPagamento((array)$pagamento);

                $datiPagamento[$key]['CondizioniPagamento'] = $this->getDatiPagamentoCondizioni($billBodyDatiPagamentoCondizioniPagamento, 'CondizioniPagamento');

                // Recupero il detaglio pagamento
                $billBodyDatiPagamentoDettaglioPagamento = $this->getEbillBodyDatiPagamentoDettaglioPagamento((array)$pagamento);

                $j = -1;
                if (!isset($billBodyDatiPagamentoDettaglioPagamento[0]))
                    $billBodyDatiPagamentoDettaglioPagamento = [0 => $billBodyDatiPagamentoDettaglioPagamento];

                foreach ($billBodyDatiPagamentoDettaglioPagamento as $dettagliopagamento)
                {
                    $j++;
                    // Dettaglio pagamento
                    // !! Attenzione non creati in uscita
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['Beneficiario'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'Beneficiario');
                    // Queste usate
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['ModalitaPagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'ModalitaPagamento');

                    switch ($datiPagamento[$key]['DettaglioPagamento'][$j]['ModalitaPagamento'])
                    {
                        case 'MP04': // Contanti presso tesoreria (?)
                            $datiPagamento[$key]['DettaglioPagamento'][$j]['CognomeQuitenzante'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'CognomeQuitenzante');
                            $datiPagamento[$key]['DettaglioPagamento'][$j]['NomeQuietanzante'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'NomeQuietanzante');
                            $datiPagamento[$key]['DettaglioPagamento'][$j]['CFQuietanzante'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'CFQuietanzante');
                            $datiPagamento[$key]['DettaglioPagamento'][$j]['TitoloQuietanzante'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'TitoloQuietanzante');
                        break;
                    }

                    // !! Attenzione non creati in uscita
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['DataRiferimentoTerminiPagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'DataRiferimentoTerminiPagamento');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['GiorniTerminiPagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'GiorniTerminiPagamento');
                    // Queste usate
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['DataScadenzaPagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'DataScadenzaPagamento');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['ImportoPagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'ImportoPagamento');

                    // !! Attenzione non creati in uscita
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['CodUfficioPostale'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'CodUfficioPostale');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['IstitutoFinanziario'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'IstitutoFinanziario');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['IBAN'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'IBAN');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['ABI'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'ABI');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['CAB'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'CAB');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['BIC'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'BIC');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['ScontoPagamentoAnticipato'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'ScontoPagamentoAnticipato');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['DataLimitePagamentoAnticipato'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'DataLimitePagamentoAnticipato');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['PenalitaPagamentiRitardati'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'PenalitaPagamentiRitardati');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['DataDecorrenzaPenale'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'DataDecorrenzaPenale');
                    $datiPagamento[$key]['DettaglioPagamento'][$j]['CodicePagamento'] = $this->getDatiPagamentoDettaglio($dettagliopagamento, 'CodicePagamento');
                }
            }

            // *************************************** CREAZIONE NUOVA FATTURA ************************************************/

            $idNewSupplierBill = $this->Bill->CreateSupplierBillFromXmlImport($billNumber, $billDate, $supplier, $datiGeneraliDocumento, $datiGeneraliRitenuta, $datiGeneraliBollo, $datiCassaPrevidenziale, $datiScontoMaggiorazione, $arraycontratto, $arrayordine, $arrayconvenzione, $datiRiepilogo, $datiCessionarioCommittente, $totaleImporto, $totaleImposta, $datiTrasmittente, $idixfe, $arrayDDT, $newReceiveDate);
            // Se la fattura non può essere creata

            if ($idNewSupplierBill != false)
            {
                if (isset($arrayordine))
                {
                    foreach ($arrayordine as $ordine)
                    {
                        if ((!isset($ordine['RiferimentoNumeroLinea']) || $ordine['RiferimentoNumeroLinea'] == null) && $ordine['IdDocumento'] != null)
                        {
                            $this->Billgestionaldata->create();
                            $newGestionalData = [];
                            $newGestionalData['bill_id'] = $idNewSupplierBill;
                            $newGestionalData['document_type'] = 0;
                            $newGestionalData['IdDocumento'] = isset($ordine['IdDocumento']) ? $newGestionalData['IdDocumento'] = $ordine['IdDocumento'] : null;
                            $newGestionalData['Data'] = isset($ordine['Data']) ? $newGestionalData['Data'] = $ordine['Data'] : null;
                            $newGestionalData['NumItem'] = isset($ordine['NumItem']) ? $newGestionalData['NumItem'] = $ordine['NumItem'] : null;
                            $newGestionalData['CodiceCommessaConvenzione'] = isset($ordine['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $ordine['CodiceCommessaConvenzione'] : null;
                            $newGestionalData['CodiceCIG'] = isset($ordine['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $ordine['CodiceCIG'] : null;
                            $newGestionalData['CodiceCUP'] = isset($ordine['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $ordine['CodiceCUP'] : null;
                            $this->Billgestionaldata->save($newGestionalData);
                        }
                    }
                }

                // Salvo 0..n causali sulla fattura
                if (isset($arrayCausals))
                {
                    foreach ($arrayCausals as $causal)
                    {
                        $this->Billgestionaldata->create();
                        $newGestionalData = [];
                        $newGestionalData['bill_id'] = $idNewSupplierBill;
                        $newGestionalData['importedcausals'] = $causal;
                        $this->Billgestionaldata->save($newGestionalData);
                    }
                }

                if (isset($arraycontratto))
                {
                    foreach ($arraycontratto as $contratto)
                    {
                        if ((!isset($contratto['RiferimentoNumeroLinea']) || $contratto['RiferimentoNumeroLinea'] == null) && $contratto['IdDocumento'] != null)
                        {
                            $this->Billgestionaldata->create();
                            $newGestionalData = [];
                            $newGestionalData['bill_id'] = $idNewSupplierBill;
                            $newGestionalData['document_type'] = 1;
                            $newGestionalData['IdDocumento'] = isset($contratto['IdDocumento']) ? $newGestionalData['IdDocumento'] = $contratto['IdDocumento'] : null;
                            $newGestionalData['Data'] = isset($contratto['Data']) ? $newGestionalData['Data'] = $contratto['Data'] : null;
                            $newGestionalData['NumItem'] = isset($contratto['NumItem']) ? $newGestionalData['NumItem'] = $contratto['NumItem'] : null;
                            $newGestionalData['CodiceCommessaConvenzione'] = isset($contratto['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $contratto['CodiceCommessaConvenzione'] : null;
                            $newGestionalData['CodiceCIG'] = isset($contratto['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $contratto['CodiceCIG'] : null;
                            $newGestionalData['CodiceCUP'] = isset($contratto['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $contratto['CodiceCUP'] : null;
                            $this->Billgestionaldata->save($newGestionalData);
                        }
                    }
                }

                if (isset($arrayconvenzione))
                {
                    foreach ($arrayconvenzione as $convenzione)
                    {
                        if ((!isset($convenzione['RiferimentoNumeroLinea']) || $convenzione['RiferimentoNumeroLinea'] == null) && $convenzione['IdDocumento'] != null)
                        {
                            $this->Billgestionaldata->create();
                            $newGestionalData = [];
                            $newGestionalData['bill_id'] = $idNewSupplierBill;
                            $newGestionalData['document_type'] = 2;
                            $newGestionalData['IdDocumento'] = isset($convenzione['IdDocumento']) ? $newGestionalData['IdDocumento'] = $convenzione['IdDocumento'] : null;
                            $newGestionalData['Data'] = isset($convenzione['Data']) ? $newGestionalData['Data'] = $convenzione['Data'] : null;
                            $newGestionalData['NumItem'] = isset($convenzione['NumItem']) ? $newGestionalData['NumItem'] = $convenzione['NumItem'] : null;
                            $newGestionalData['CodiceCommessaConvenzione'] = isset($convenzione['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $convenzione['CodiceCommessaConvenzione'] : null;
                            $newGestionalData['CodiceCIG'] = isset($convenzione['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $convenzione['CodiceCIG'] : null;
                            $newGestionalData['CodiceCUP'] = isset($convenzione['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $convenzione['CodiceCUP'] : null;
                            $this->Billgestionaldata->save($newGestionalData);
                        }
                    }
                }

                // Salvataggio sull'intera fattura
                if (isset($arrayDDT))
                {
                    foreach ($arrayDDT as $keydd => $ddt)
                    {
                        if (!isset($ddt['RiferimentoNumeroLinea']) && isset($ddt['rifddt']) && isset($ddt['dateddt']))
                        {
                            $this->Billgestionaldata->create();
                            $newGestionalData = [];
                            $newGestionalData['bill_id'] = $idNewSupplierBill;
                            $newGestionalData['rifddt'] = $ddt['rifddt'];
                            $newGestionalData['dateddt'] = $ddt['dateddt'];
                            $this->Billgestionaldata->save($newGestionalData);
                        }
                    }
                }

                // ************************* CREAZIONE RIGHE FATTUARA **********************************************
                $i = 0;

                foreach ($datiGenerali as $supplierBillRow)
                {
                    $i++;

                    $this->Good->create();
                    $Article = [];
                    $Article['oggetto'] = $supplierBillRow['Descrizione'];
                    $Article['quantita'] = $supplierBillRow['Quantita'];
                    $Article['prezzo'] = $supplierBillRow['PrezzoUnitario'];
                    $Article['iva_id'] = $this->Utilities->getVatFromPercentage($supplierBillRow['AliquotaIVA'], $supplierBillRow['Natura']);
                    $Article['bill_id'] = $idNewSupplierBill;
                    $Article['customdescription'] = $supplierBillRow['customdescription'];
                    $Article['unita'] = isset($supplierBillRow['UnitaMisura']) ? $supplierBillRow['UnitaMisura'] : '';
                    $Article['prezzo'] = $supplierBillRow['PrezzoUnitario'];
                    $Article['imported_total_row'] = $supplierBillRow['PrezzoTotale'];
                    $Article['importedvatpercentage'] = $supplierBillRow['AliquotaIVA'];
                    $supplierBillRow['Percentuale'] != null ?  $Article['discount'] = $supplierBillRow['Percentuale'] : $Article['discount'] = 0;
                    $supplierBillRow['Sconto2'] != null ?  $Article['discount2'] = $supplierBillRow['Sconto2'] : $Article['discount2'] = 0;
                    $Article['tipocessioneprestazione'] = $supplierBillRow['TipoCessionePrestazione'];
                    $Article['importedvatcode'] = $supplierBillRow['Natura'];
                    $Article['company_id'] = MYCOMPANY;
                    $Article['state'] = 1;
                    isset($supplierBillRow['CodiceValore']) ? $Article['importedarticlevalue'] = $supplierBillRow['CodiceValore'] : null;
                    isset($supplierBillRow['DataInizioPeriodo']) ? $Article['importedstartperiod'] = $supplierBillRow['DataInizioPeriodo'] : null;
                    isset($supplierBillRow['DataFinePeriodo']) ? $Article['importedendperiod'] = $supplierBillRow['DataFinePeriodo'] : null;

                    $newGood = $this->Good->save($Article);

                    // Salvataggio arrayDDT sulla linea
                    if (isset($arrayDDT))
                    {
                        foreach ($arrayDDT as $key => $ddt)
                        {
                            if (isset($ddt['RiferimentoNumeroLinea']) && isset($supplierBillRow['NumeroLinea']))
                            {
                                if ($ddt['RiferimentoNumeroLinea'] == $supplierBillRow['NumeroLinea'])
                                {
                                    $this->Goodgestionaldata->create();
                                    $newGestionalData = [];
                                    $newGestionalData['good_id'] = $newGood['Good']['id'];
                                    $newGestionalData['rifddt'] = $ddt['NumeroDDT'];
                                    $newGestionalData['dateddt'] = $ddt['DataDDT'];
                                    $this->Goodgestionaldata->save($newGestionalData);
                                }
                            }
                        }
                    }

                    /* ARRAY SALVATAGGIO LINEE */
                    if (isset($arrayordine))
                    {
                        foreach ($arrayordine as $ordine)
                        {
                            if (isset($ordine['RiferimentoNumeroLinea']) && isset($supplierBillRow['NumeroLinea']))
                            {
                                if (0 + $ordine['RiferimentoNumeroLinea'] == $supplierBillRow['NumeroLinea'] && $supplierBillRow['NumeroLinea'] > 0)
                                {
                                    $this->Goodgestionaldata->create();
                                    $newGestionalData = [];
                                    $newGestionalData['good_id'] = $newGood['Good']['id'];
                                    $newGestionalData['document_type'] = 0;
                                    isset($ordine['IdDocumento']) ? $newGestionalData['IdDocumento'] = $ordine['IdDocumento'] : null;
                                    isset($ordine['Data']) ? $newGestionalData['Data'] = $ordine['Data'] : null;
                                    isset($ordine['NumItem']) ? $newGestionalData['NumItem'] = $ordine['NumItem'] : null;
                                    isset($ordine['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $ordine['CodiceCommessaConvenzione'] : null;
                                    isset($ordine['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $ordine['CodiceCIG'] : null;
                                    isset($ordine['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $ordine['CodiceCUP'] : null;
                                    $this->Goodgestionaldata->save($newGestionalData);
                                }
                            }
                        }
                    }

                    if (isset($arraycontratto))
                    {
                        foreach ($arraycontratto as $contratto)
                        {
                            if (isset($contratto['RiferimentoNumeroLinea']) && isset($supplierBillRow['NumeroLinea']))
                            {
                                if (0 + $contratto['RiferimentoNumeroLinea'] == $supplierBillRow['NumeroLinea'] && $supplierBillRow['NumeroLinea'] > 0)
                                {
                                    $this->Goodgestionaldata->create();
                                    $newGestionalData = [];
                                    $newGestionalData['good_id'] = $newGood['Good']['id'];
                                    $newGestionalData['document_type'] = 1;
                                    isset($contratto['IdDocumento']) ? $newGestionalData['IdDocumento'] = $contratto['IdDocumento'] : null;
                                    isset($contratto['Data']) ? $newGestionalData['Data'] = $contratto['Data'] : null;
                                    isset($contratto['NumItem']) ? $newGestionalData['NumItem'] = $contratto['NumItem'] : null;
                                    isset($contratto['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $contratto['CodiceCommessaConvenzione'] : null;
                                    isset($contratto['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $contratto['CodiceCIG'] : null;
                                    isset($contratto['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $contratto['CodiceCUP'] : null;
                                    $this->Goodgestionaldata->save($newGestionalData);
                                }
                            }
                        }
                    }

                    if (isset($arrayconvenzione))
                    {
                        foreach ($arrayconvenzione as $convenzione)
                        {
                            if (isset($convenzione['RiferimentoNumeroLinea']) && isset($supplierBillRow['NumeroLinea']))
                            {
                                if (0 + $convenzione['RiferimentoNumeroLinea'] == $supplierBillRow['NumeroLinea'] && $supplierBillRow['NumeroLinea'] > 0)
                                {
                                    $this->Goodgestionaldata->create();
                                    $newGestionalData = [];
                                    $newGestionalData['good_id'] = $newGood['Good']['id'];
                                    $newGestionalData['document_type'] = 2;
                                    isset($convenzione['IdDocumento']) ? $newGestionalData['IdDocumento'] = $convenzione['IdDocumento'] : null;
                                    isset($convenzione['Data']) ? $newGestionalData['Data'] = $convenzione['Data'] : null;
                                    isset($convenzione['NumItem']) ? $newGestionalData['NumItem'] = $convenzione['NumItem'] : null;
                                    isset($convenzione['CodiceCommessaConvenzione']) ? $newGestionalData['CodiceCommessaConvenzione'] = $convenzione['CodiceCommessaConvenzione'] : null;
                                    isset($convenzione['CodiceCIG']) ? $newGestionalData['CodiceCIG'] = $convenzione['CodiceCIG'] : null;
                                    isset($convenzione['CodiceCUP']) ? $newGestionalData['CodiceCUP'] = $convenzione['CodiceCUP'] : null;
                                    $this->Goodgestionaldata->save($newGestionalData);
                                }
                            }
                        }
                    }

                    if (isset($supplierBillRow['DettaglioDatiGestionali']))
                    {
                        foreach ($supplierBillRow['DettaglioDatiGestionali'] as $altroDatoGestionale)
                        {
                            $this->Goodgestionaldata->create();
                            $newGestionalData = [];
                            $newGestionalData['good_id'] = $newGood['Good']['id'];
                            $newGestionalData['data_type'] = isset($altroDatoGestionale['TipoDato']) ? $altroDatoGestionale['TipoDato'] : null;
                            $newGestionalData['ref_text'] = isset($altroDatoGestionale['RiferimentoTesto']) ? $altroDatoGestionale['RiferimentoTesto'] : null;
                            $newGestionalData['ref_number'] = isset($altroDatoGestionale['RiferimentoNumero']) ? $altroDatoGestionale['RiferimentoNumero'] : null;
                            $newGestionalData['ref_data'] = isset($altroDatoGestionale['RiferiementoData']) ? $altroDatoGestionale['RiferiementoData'] : null;
                            $this->Goodgestionaldata->save($newGestionalData);

                            // Salvataggi rif ddt sulle righe
                        }
                    }
                }

                // ************************* FINE CREAZIONE RIGHE FATTUARA **********************************************

                $this->Deadline->saveImportedDeadline($idNewSupplierBill, $datiPagamento, $datiGeneraliDocumento['ImportoTotaleDocumento'], $datiGeneraliDocumento['Data']);
            }
        }
        catch (Exception $ecc)
        {
            debug('eccezione::');
            debug($ecc->getMessage());
        }
    }

    // Recupero intestazione fattura
    private function getEbillHeader($xmlData)
    {
        return ((array)$xmlData->FatturaElettronicaHeader);
    }

    /**  DATI TRASMISSIONE  **/

    private function getEbillHeaderTrasmissione($billHeader)
    {
        return $billHeader['DatiTrasmissione'];
    }


    private function getDatiTrasmissione($billHeaderTrasmissione, $field)
    {
        $newArray = [];
        $fields =
            [
                'IdPaese' => 'IdPaese',
                'IdCodice' => 'IdCodice',
                'ProgressivoInvio' => 'ProgressivoInvio',
                'FormatoTrasmissione' => 'FormatoTrasmissione',
                'CodiceDestinatario' => 'CodiceDestinatario',
                'Telefono' => 'Telefono',
                'Email' => 'Email',
                'PECDestinatario' => 'PECDestinatario',
            ];

        $billHeaderTrasmissione = $this->xml2array($billHeaderTrasmissione, $newArray);

        return $this->recursiveFind($billHeaderTrasmissione, $fields[$field], true);
    }

    /** FINE DATI TRASMISSIONE **/

    // Dati cennte prestatore
    private function getEbillHeaderCedentePrestatore($billHeader)
    {
        return $billHeader['CedentePrestatore'];
    }

    // Dati cessionario committente
    private function getEbillHeaderCessionarioCommittente($billHeader)
    {
        return $billHeader['CessionarioCommittente'];
    }

    /** CEDENTE PRESTATORE **/

    // Sezione cedente prestatore - Dati anagrafici
    private function getEBillHeaderCedentePrestatoreDatiAnagrafici($billHeaderCedentePrestatore)
    {
        if (isset($billHeaderCedentePrestatore['DatiAnagrafici'])) {
            return $billHeaderCedentePrestatore['DatiAnagrafici'];
        }
        return null;
    }

    // Sezione cedente prestatore - Presttore Sede
    private function getEBillHeaderCedentePrestatoreSede($billHeaderCedentePrestatore)
    {
        if (isset($billHeaderCedentePrestatore['Sede'])) {
            return $billHeaderCedentePrestatore['Sede'];
        }
        return null;
    }

    // Sezione cedente prestatore - Iscrizione rea
    private function getEBillHeaderCedentePrestatoreIscrizioneRea($billHeaderCedentePrestatore)
    {
        if (isset($billHeaderCedentePrestatore['IscrizioneREA'])) {
            return $billHeaderCedentePrestatore['IscrizioneREA'];
        }
        return null;
    }

    // Sezione cedente prestatore - Presttore Sede
    private function getEBillHeaderCedentePrestatoreContatti($billHeaderCedentePrestatore)
    {
        if (isset($billHeaderCedentePrestatore['Contatti'])) {
            return $billHeaderCedentePrestatore['Contatti'];
        }
        return null;
    }

    // Dati anagrafici cedente prestatore
    private function getDatiAnagraficiCedentePrestatore($datiAnagraficiCedentePrestatore, $field)
    {
        $newArray = [];

        $fields = [
            'IdPaese' => 'IdPaese',
            'IdCodice' => 'IdCodice',
            'CodiceFiscale' => 'CodiceFiscale',
            'Denominazione' => 'Denominazione',
            'RegimeFiscale' => 'RegimeFiscale',
            'Nome' => 'Nome',
            'Cognome' => 'Cognome',
        ];

        $datiAnagraficiCedentePrestatore = $this->xml2array($datiAnagraficiCedentePrestatore, $newArray);
        return $this->recursiveFind($datiAnagraficiCedentePrestatore, $fields[$field]);
    }

    // Dati sede cedente prestatore
    private function getCedentePrestatoreSede($datiSedeCedentePrestatore, $field)
    {
        $newArray = [];
        $fields =
            [
                'Indirizzo' => 'Indirizzo',
                'CAP' => 'CAP',
                'Comune' => 'Comune',
                'Provincia' => 'Provincia',
                'Nazione' => 'Nazione',
                'NumeroCivico' => 'NumeroCivico',
            ];
        $datiSedeCedentePrestatore = $this->xml2array($datiSedeCedentePrestatore, $newArray);
        return $this->recursiveFind($datiSedeCedentePrestatore, $fields[$field], true);
    }

    // Dati iscrizione rea cedente prestatore
    private function getCedentePrestatoreIscrizioneRea($datiIscrizioneReaCedentePrestatore, $field)
    {
        $newArray = [];
        $fields =
            [
                'Ufficio' => 'Ufficio',
                'NumeroREA' => 'NumeroREA',
                'CapitaleSociale' => 'CapitaleSociale',
                'SocioUnico' => 'SocioUnico',
                'StatoLiquidazione' => 'StatoLiquidazione',
            ];
        $datiIscrizioneReaCedentePrestatore = $this->xml2array($datiIscrizioneReaCedentePrestatore, $newArray);
        return $this->recursiveFind($datiIscrizioneReaCedentePrestatore, $fields[$field], true);
    }

    private function getCedentePrestatoreContatti($datiContattiCedentePrestatore, $field)
    {
        $newArray = [];
        $fields =
            [
                'Telefono' => 'Telefono',
                'Fax' => 'Fax',
                'Email' => 'Email',
            ];
        $datiContattiCedentePrestatore = $this->xml2array($datiContattiCedentePrestatore, $newArray);
        return $this->recursiveFind($datiContattiCedentePrestatore, $fields[$field], true);
    }

    /** FINE CEDENTE PRESTATORE **/

    /** CESSIONARIO COMMITTENTE **/


    private function getEbillHeaderDatiAnagraficiCessionarioCommittente($datiAnagraficiCessionarioCommittente)
    {
        return $datiAnagraficiCessionarioCommittente['DatiAnagrafici'];
    }

    private function getEbillHeaderCessionarioCommittenteSede($datiAnagraficiCessionarioCommittente)
    {
        return $datiAnagraficiCessionarioCommittente['Sede'];
    }

    private function getDatiAnagraficiCessionarioCommittente($datiIscrizioneReaCedentePrestatore, $field)
    {
        $newArray = [];
        $fields =
            [
                'IdPaese' => 'IdPaese',
                'IdCodice' => 'IdCodice',
                'Denominazione' => 'Denominazione',
            ];

        $datiIscrizioneReaCedentePrestatore = $this->xml2array($datiIscrizioneReaCedentePrestatore, $newArray);
        return $this->recursiveFind($datiIscrizioneReaCedentePrestatore, $fields[$field], true);
    }

    private function getCessionarioCommittenteSede($datiIscrizioneReaCedentePrestatore, $field)
    {
        $newArray = [];
        $fields =
            [
                'Indirizzo' => 'Indirizzo',
                'CAP' => 'CAP',
                'Comune' => 'Comune',
                'Provincia' => 'Provincia',
                'Nazione' => 'Nazione',
            ];
        $datiIscrizioneReaCedentePrestatore = $this->xml2array($datiIscrizioneReaCedentePrestatore, $newArray);
        return $this->recursiveFind($datiIscrizioneReaCedentePrestatore, $fields[$field], true);
    }

    /** FINE CESSIONARIO COMMITTENTE **/

    /** DATI GENERALI DOCUMENTO **/

    private function getEbillBodyDatiGeneraliDatiGeneraliDocumento($datiGeneraliContratto)
    {
        return $datiGeneraliContratto['DatiGeneraliDocumento'];
    }

    private function getEbillBodyDatiGeneraliDatiContratto($datiGeneraliContratto)
    {
        if (isset($datiGeneraliContratto['DatiContratto'])) {
            return $datiGeneraliContratto['DatiContratto'];
        }
        return null;
    }

    private function getEbillBodyDatiGeneraliDatiOrdineAcquisto($datiGeneraliContratto)
    {
        if (isset($datiGeneraliContratto['DatiOrdineAcquisto'])) {
            return $datiGeneraliContratto['DatiOrdineAcquisto'];
        }
        return null;
    }

    private function getEbillBodyDatiGeneraliConvenzione($datiGeneraliContratto)
    {
        if (isset($datiGeneraliContratto['DatiConvenzione'])) {
            return $datiGeneraliContratto['DatiConvenzione'];
        }
        return false;
    }


    private function getEbillBodyDatiGeneraliDatiDDT($datiGeneraliDdt)
    {
        if (isset($datiGeneraliDdt['DatiDDT'])) {
            return $datiGeneraliDdt['DatiDDT'];
        }
        return false;
    }


    private function getDatiGeneraliDDT($datiGeneraliDdt, $field)
    {
        $newArray = [];
        $fields =
            [
                'NumeroDDT' => 'NumeroDDT',
                'DataDDT' => 'DataDDT',
                // 'RiferimentoNumeroLinea' => 'RiferimentoNumeroLinea',
            ];

        $datiGeneraliDdt = $this->xml2array($datiGeneraliDdt, $newArray);
        return $this->recursiveFind($datiGeneraliDdt, $fields[$field], true);

    }


    private function getRiferimentoLineaDdt($datiGeneraliDdt)
    {
        if (isset($datiGeneraliDdt['RiferimentoNumeroLinea'])) {
            return $datiGeneraliDdt['RiferimentoNumeroLinea'];
        }
        return false;
    }

    // DATI GENERALI DOCUMENTO
    private function getDatiGeneraliDatiGeneraliDocumento($datiGeneraliDocumento, $field)
    {
        $newArray = [];
        $fields =
            [
                'TipoDocumento' => 'TipoDocumento',
                'Divisa' => 'Divisa',
                'Data' => 'Data',
                'Numero' => 'Numero',
                'ImportoTotaleDocumento' => 'ImportoTotaleDocumento',
                'Art73' => 'Art73',
            ];

        $datiGeneraliDocumento = $this->xml2array($datiGeneraliDocumento, $newArray);
        return $this->recursiveFind($datiGeneraliDocumento, $fields[$field], true);
    }

    private function getDatiGeneraliDatiGeneraliCausale($datiGeneraliDocumento)
    {
        if (isset($datiGeneraliDocumento['Causale'])) {
            return $datiGeneraliDocumento['Causale'];
        } else {
            return null;
        }
    }

    // Restituisce i dati della ritenuta
    private function getEbillBodyDatiGeneraliDatiRitenuta($datiGeneraliDocumento)
    {
        if (isset($datiGeneraliDocumento['DatiRitenuta'])) {
            return $datiGeneraliDocumento['DatiRitenuta'];
        } else {
            return null;
        }
    }

    // Restituisce le righe bolli
    private function getEbillBodyDatiGeneraliBollo($datiGeneraliDocumento)
    {
        if (isset($datiGeneraliDocumento['DatiBollo'])) {
            return $datiGeneraliDocumento['DatiBollo'];
        } else {
            return null;
        }
    }

    // Restituisce le righe cassa previdenziale
    private function getEbillBodyDatiGeneraliDatiCassaPrevidenziale($datiGeneraliDocumento)
    {
        if (isset($datiGeneraliDocumento['DatiCassaPrevidenziale'])) {
            return $datiGeneraliDocumento['DatiCassaPrevidenziale'];
        } else {
            return null;
        }
    }

    // Restituisce le righe sconti e/o maggiorazioni
    private function getEbillBodyDatiGeneraliDatiSconti($datiGeneraliDocumento)
    {
        if (isset($datiGeneraliDocumento['ScontoMaggiorazione']))
            return $datiGeneraliDocumento['ScontoMaggiorazione'];
        else
            return null;
    }

    // DATI GENERALI DATI - RITENUTA
    public function getDatiGeneraliDatiRitenuta($datiRitenuta, $field)
    {
        $newArray = [];

        $fields = [
            'TipoRitenuta' => 'TipoRitenuta',
            'ImportoRitenuta' => 'ImportoRitenuta',
            'AliquotaRitenuta' => 'AliquotaRitenuta',
            'CausalePagamento' => 'CausalePagamento',
        ];

        $datiRitenuta = $this->xml2array($datiRitenuta, $newArray);
        return $this->recursiveFind($datiRitenuta, $fields[$field], true);
    }

    private function getDatiGeneraliDatiBollo($datiBollo, $field)
    {
        $newArray = [];

        $fields = [
            'BolloVirtuale' => 'BolloVirtuale',
            'ImportoBollo' => 'ImportoBollo',
        ];

        $datiBollo = $this->xml2array($datiBollo, $newArray);
        return $this->recursiveFind($datiBollo, $fields[$field], true);
    }

    private function getDatiGeneraliDatiCassaPrevidenziale($datiCassa, $field)
    {
        $newArray = [];
        $fields =
            [
                'TipoCassa' => 'TipoCassa',
                'AlCassa' => 'AlCassa',
                'ImportoContributoCassa' => 'ImportoContributoCassa',
                'ImponibileCassa' => 'ImponibileCassa',
                'AliquotaIVA' => 'AliquotaIVA',
                'Ritenuta' => 'Ritenuta',
                'Natura' => 'Natura',
                'RiferimentoAmministrazione' => 'RiferimentoAmministrazione',
            ];

        $datiCassa = $this->xml2array($datiCassa, $newArray);
        return $this->recursiveFind($datiCassa, $fields[$field], true);
    }

    private function getDatiGeneraliscontoMaggiorazione($datiScontoMaggiorazione, $field)
    {
        $newArray = [];
        $fields =
            [
                'Tipo' => 'Tipo',
                'Percentuale' => 'Percentuale',
                'Importo' => 'Importo',
            ];

        $datiScontoMaggiorazione = $this->xml2array($datiScontoMaggiorazione, $newArray);
        return $this->recursiveFind($datiScontoMaggiorazione, $fields[$field], true);
    }

    // DATI GENERALI DATI - DATI CONTRATTO
    private function getDatiGeneraliDatiContratto($datiContratto, $field)
    {
        $newArray = [];
        $fields =
            [
                'IdDocumento' => 'IdDocumento',
                'Data' => 'Data',
                'NumItem' => 'NumItem',
                'CodiceCommessaConvenzione' => 'CodiceCommessaConvenzione',
                'CodiceCIG' => 'CodiceCIG',
                'CodiceCUP' => 'CodiceCUP',
                'RiferimentoNumeroLinea' => 'RiferimentoNumeroLinea',
            ];

        $datiContratto = $this->xml2array($datiContratto, $newArray);
        return $this->recursiveFind($datiContratto, $fields[$field], true);
    }

    private function getDatiGeneraliDatiContrattoRiferimentoNumeroLinea($datiGeneraliContratto)
    {
        if (isset($datiGeneraliContratto['RiferimentoNumeroLinea'])) {
            return $datiGeneraliContratto['RiferimentoNumeroLinea'];
        }
        return null;
    }


    private function getEbillBodyDatiBeniEServiziDettaglioLinee($billBodyBeniEServizi)
    {
        if (isset($billBodyBeniEServizi['DettaglioLinee'])) {
            return $billBodyBeniEServizi['DettaglioLinee'];
        }
        return null;
    }

    // Recupero il codice articolo
    private function getDatiBeniEServiziDettaglioLineeCodiceArticolo($datiDettagliLinee)
    {
        if (isset($datiDettagliLinee['CodiceArticolo'])) {
            return $datiDettagliLinee['CodiceArticolo'];
        }
        return null;
    }

    // Resituisco il dettaglio del codice articolo
    private function getDatiBeniEServiziDettaglioLineeCodiceArticoloDettaglio($dettaglioArticolo, $field)
    {
        $newArray = [];
        $fields =
            [
                'CodiceTipo' => 'CodiceTipo',
                'CodiceValore' => 'CodiceValore',
            ];
        $dettaglioArticolo = $this->xml2array($dettaglioArticolo, $newArray);
        return $this->recursiveFind($dettaglioArticolo, $fields[$field], true);
    }

    // Recupero il codice articolo
    private function getDatiBeniEServiziDettaglioLineeAltriDatiGestionali($datiDettagliLinee)
    {
        if (isset($datiDettagliLinee['AltriDatiGestionali'])) {
            return $datiDettagliLinee['AltriDatiGestionali'];
        }
        return null;
    }

    private function getDatiBeniEServiziDettaglioLineeDatiGestionaleDettaglio($dettaglioLineeAltriDatiGestionali, $field)
    {
        $newArray = [];

        $fields =
            [
                'TipoDato' => 'TipoDato',
                'RiferimentoTesto' => 'RiferimentoTesto',
                'RiferimentoNumero' => 'RiferimentoNumero',
                'RiferimentoData' => 'RiferimentoData',
            ];
        $dettaglioLineeAltriDatiGestionali = $this->xml2array($dettaglioLineeAltriDatiGestionali, $newArray);
        return $this->recursiveFind($dettaglioLineeAltriDatiGestionali, $fields[$field], true);
    }

    private function getDatiBeniEServiziDettaglioLinee($datiDettagliLinee, $field, $key = null)
    {
        $newArray = [];

        $fields = [
            'NumeroLinea' => 'NumeroLinea',
            'TipoCessionePrestazione' => 'TipoCessionePrestazione',
            'Descrizione' => 'Descrizione',
            'Quantita' => 'Quantita',
            'UnitaMisura' => 'UnitaMisura',
            'DataInizioPeriodo' => 'DataInizioPeriodo',
            'DataFinePeriodo' => 'DataFinePeriodo',
            'PrezzoUnitario' => 'PrezzoUnitario',
            'Tipo' => 'Tipo',
            'Percentuale' => 'Percentuale',
            'Importo' => 'Importo',
            'PrezzoTotale' => 'PrezzoTotale',
            'AliquotaIVA' => 'AliquotaIVA',
            'Ritenuta' => 'Ritenuta',
            'Natura' => 'Natura',
            'RiferimentoAmministrazione' => 'RiferimentoAmministrazione',
        ];

        $datiDettagliLinee = $this->xml2array($datiDettagliLinee, $newArray);
        return $this->recursiveFind($datiDettagliLinee, $fields[$field], true, $key);
    }

    private function getEbillBodyDatiBeneiEServiziDatiRiepilogo($billBodyBeniEServizi)
    {
        return $billBodyBeniEServizi['DatiRiepilogo'];
    }

    private function getDatiBeniEServiziDatiRiepilogo($datiRiepilogo, $field)
    {
        $newArray = [];
        $fields =
            [
                'AliquotaIVA' => 'AliquotaIVA',
                'Natura' => 'Natura',
                'ImponibileImporto' => 'ImponibileImporto',
                'Imposta' => 'Imposta',
                'EsigibilitaIVA' => 'EsigibilitaIVA',
                'RiferimentoNormativo' => 'RiferimentoNormativo',
                'Arrotondamento' => 'Arrotondamento',
            ];

        $datiRiepilogo = $this->xml2array($datiRiepilogo, $newArray);
        return $this->recursiveFind($datiRiepilogo, $fields[$field], true);
    }

    /** FINE DATI GENERALI DOCUMENTO **/


    /** DATI PAGAMENTO **/

    private function getEbillBodyDatiPagamentoCondizioniPagamento($BillBodyPagamento)
    {
        if (isset($BillBodyPagamento['CondizioniPagamento'])) {
            return $BillBodyPagamento['CondizioniPagamento'];
        }
        return null;
    }

    private function getDatiPagamentoCondizioni($datiPagamento, $field)
    {
        $newArray = [];
        $fields =
            [
                'CondizioniPagamento' => 'CondizioniPagamento',
            ];

        $datiPagamento = $this->xml2array($datiPagamento, $newArray);
        return $this->recursiveFind($datiPagamento, $fields[$field], true);
    }

    private function getEbillBodyDatiPagamentoDettaglioPagamento($BillBodyPagamento)
    {
        if (isset($BillBodyPagamento['DettaglioPagamento'])) {
            return $BillBodyPagamento['DettaglioPagamento'];
        }
        return null;
    }

    private function getDatiPagamentoDettaglio($datiPagamento, $field)
    {
        $newArray = [];
        $fields =
            [
                'Beneficiario' => 'Beneficiario',
                'ModalitaPagamento' => 'ModalitaPagamento',
                'DataRiferimentoTerminiPagamento' => 'DataRiferimentoTerminiPagamento',
                'GiorniTerminiPagamento' => 'GiorniTerminiPagamento',
                'DataScadenzaPagamento' => 'DataScadenzaPagamento',
                'ImportoPagamento' => 'ImportoPagamento',
                'CodUfficioPostale' => 'CodUfficioPostale',
                'CognomeQuietanzante' => 'CognomeQuietanzante',
                'NomeQuietanzante' => 'NomeQuietanzante',
                'CFQuietanzante' => 'CFQuietanzante',
                'TitoloQuietanzante' => 'TitoloQuietanzante',
                'IstitutoFinanziario' => 'IstitutoFinanziario',
                'IBAN' => 'IBAN',
                'ABI' => 'ABI',
                'CAB' => 'CAB',
                'BIC' => 'BIC',
                'ScontoPagamentoAnticipato' => 'ScontoPagamentoAnticipato',
                'DataLimitePagamentoAnticipato' => 'DataLimitePagamentoAnticipato',
                'PenalitaPagamentiRitardati' => 'PenalitaPagamentiRitardati',
                'DataDecorrenzaPenale' => 'DataDecorrenzaPenale',
                'CodicePagamento' => 'CodicePagamento',
            ];

        $datiPagamento = $this->xml2array($datiPagamento, $newArray);
        return $this->recursiveFind($datiPagamento, $fields[$field], true);
    }


    /** FINE PAGAMENTO **/


    // Recupero corpo  fattura
    private function getEbillBody($xmlData)
    {
        return ((array)$xmlData->FatturaElettronicaBody);
    }

    // Recupero i dati generali
    private function getEbillBodyDatiGenerali($billBody)
    {
        if (isset($billBody['DatiGenerali'])) {
            return $billBody['DatiGenerali'];
        }
    }

    // Recupero i dati beni e servizi
    private function getEbillBodyDatiBeniEServizi($billBody)
    {
        if (isset($billBody['DatiBeniServizi'])) {
            return $billBody['DatiBeniServizi'];
        }
        return null;
    }

    // Recupero i dati pagamento
    private function getEbillBodyDatiPagamento($billBody)
    {
        if (isset($billBody['DatiPagamento'])) {
            return $billBody['DatiPagamento'];
        }
        return null;
    }

    private function xml2array($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;
        return $out;
    }

    private function recursiveFind($haystack, $needle, $applyRecursive = true, $arrayKey = null, $changed = false)
    {
        foreach ($haystack as $key => $item)
        {
            if (is_array($item))
            {
                if ($applyRecursive) // Allora se non sono previsti arrya in cui aggiungere a prendere dati inferiori
                {
                    if($arrayKey !== null)
                    {
                        if(array_key_exists($arrayKey, $item))
                        {
                            $item = $this->xml2array($item[$arrayKey]);
                            $changed = true;
                        }
                    }

                    $forReturn = $this->recursiveFind($item, $needle, $applyRecursive, $arrayKey, $changed);

                    if ($forReturn != null)
                        return $forReturn;
                }
            }
            else
            {
                if ($key === $needle && ($changed || $arrayKey == null))
                    return $item;
            }
        }
    }


    private function xmlToObject($xmlData)
    {
        return new SimpleXMLElement($xmlData);
    }

    private function importBillFromXml($file){
        $xmlObject = $this->xmlToObject($file);


    }





    // Restituisce la versione della fattura elettronica
    private function getXmlVersion($xml)
    {
        $exchangeArray = (array)$xml->attributes();
        $version = $exchangeArray['@attributes']['versione'];

        if ($version != null)
        {
            switch ($version)
            {
                case 'FPR12' :
                    return false;
                case 'FPA12' :
                    return true;
            }
        }
        else
        {
            throw new Exception("Attenzione il file non risulta essere corretto. Versione non riconosciuta");
        }
    }

    /** FUNZIONI DI CONTROLLO FATTURA ELETTRONICA **/
    private function CheckCorrectOwner($codiceDestinatario, $pec)
    {
        $this->loadModel('Setting');

        if (strlen($codiceDestinatario) == 6 || strlen($codiceDestinatario) == 7) {
            if ($codiceDestinatario == '0000000') {
                if ($pec == $this->Setting->GetMyPec()) {
                    return true;
                }
            } else {
                if ($codiceDestinatario == $this->Setting->GetMyEbillCode()) {
                    return true;
                }
            }

            return false;

        }
    }

}
