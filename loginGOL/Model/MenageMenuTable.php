<?php
App::uses('AppModel', 'Model');

class Menus extends AppModel 
{
	public $useTable = 'menus';
}

class MenageMenuTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('menus');
        $this->Permissions = TableRegistry::get('PermissionsManager.Permissions');
    }

    /*public function getPluginsMenus()
	{
	    $user = $_SESSION['Auth']['User'];
	    $pluginsDbMenu = $this->findByMenuLevelAndState(0, 1)->sortBy('sortIndex','DESC');

	    $menu = [];
	    foreach ($pluginsDbMenu as $zeroLevel)
        {
            if($this->Permissions->isAllowed($user, $zeroLevel['plugin_name'], $zeroLevel['controller'], $zeroLevel['action']))
            {
                $menu[$zeroLevel['name']]['plugin_name'] = $zeroLevel['plugin_name'];
                $menu[$zeroLevel['name']]['controller'] = $zeroLevel['controller'];
                $menu[$zeroLevel['name']]['action'] = $zeroLevel['action'];
    
                $firstLevels = $this->findByMenuLevelAndParentAndState(1, $zeroLevel['name'], 1)->sortBy('sortIndex','DESC')->toArray();
                foreach($firstLevels as $firstLevel)
                {
                    $plugin = $firstLevel['plugin_name'];
                    $controller = $firstLevel['controller'];
                    $action = $firstLevel['action'];
    
                    if($this->Permissions->isAllowed($user, $plugin, $controller, $action))
                    {
                        $menu[$zeroLevel['name']]['subs'][$firstLevel['name']]['name'] = $firstLevel['name'];
                        $menu[$zeroLevel['name']]['subs'][$firstLevel['name']]['plugin_name'] = $plugin;
                        $menu[$zeroLevel['name']]['subs'][$firstLevel['name']]['controller'] = $controller;
                        $menu[$zeroLevel['name']]['subs'][$firstLevel['name']]['action'] = $action;
                    }
                }
            }
        }
        return $menu;
	}*/
}
?>
