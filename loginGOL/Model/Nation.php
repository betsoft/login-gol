<?php
App::uses('AppModel', 'Model');

class Nation extends AppModel 
{
	public $useTable = 'nations';

    public function getList()
    {
        $this->Nation = ClassRegistry::init('Nation');
        return $this->Nation->find('list',['fields' => ['Nation.id','Nation.name'], 'order' =>['Nation.name' => 'asc']]);
    }

}
