<?php
App::uses('AppModel', 'Model');

class Iva extends AppModel {

	public $useTable = 'ivas';

	public $belongsTo = [
		'Einvoicevatnature' => ['className' => 'Einvoicevatnature','foreignKey' => 'einvoicenature_id','conditions' => '','fields' => '','order' => ''],
		]; 

	public $hasMany = [
		'Good' => ['className' => 'Good','foreignKey' => 'iva_id','dependent' => false,'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => ''],
	];

	public $validate = 
	[
		"codice"=>
		[
            "rule"=>["isUnique", ["codice", "company_id","state"], false], 
            "message"=>"Il campo codice deve essere univoco." 
        ],
        "descrizione"=>
		[
            "rule"=>["isUnique", ["descrizione", "company_id","state"], false], 
            "message"=>"Il campo descrizione deve essere univoco." 
        ]
    ];

	public function hide($id)
    {
        return $this->updateAll(['Iva.state' => 0,'Iva.company_id'=>MYCOMPANY],['Iva.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Iva.id'=>$id, 'Iva.state' =>0 ]]) != null;
    }

}
