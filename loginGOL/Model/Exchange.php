<?php
App::uses('AppModel', 'Model');

class Exchange extends AppModel 
{
	public $useTable = 'exchanges';

	public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }

}
