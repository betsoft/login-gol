<?php
App::uses('AppModel', 'Model');

class Maintenancehourstechnician extends AppModel {

    public $useTable = 'maintenance_hours_technician';

    public function hide($id)
    {
        return $this->updateAll(['Maintenancehourstechnician.state' => 0, 'Maintenancehourstechnician.company_id' => MYCOMPANY],['Maintenancehourstechnician.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first',['conditions' => ['Maintenancehourstechnician.id' => $id, 'Maintenancehourstechnician.state' => 0, 'Maintenancehourstechnician.company_id' => MYCOMPANY]]) != null;
    }

    public $hasMany = [];

    public $belongsTo = [
        'Maintenancehour' => ['className' => 'Maintenancehour','foreignKey' => 'maintenance_hour_id','conditions' => '','fields' => '','order' => ''],
        'Technician' => ['className' => 'Technician', 'foreignKey' => 'technician_id', 'conditions' => '', 'fields' => '', 'order' => ''],
    ];
}