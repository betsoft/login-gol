<?php
App::uses('AppModel', 'Model');

class Supplier extends AppModel 
{
	public $belongsTo = 
	[
		'Nation' => ['className' => 'Nation','foreignKey' => 'nation_id','conditions' => '','fields' => '','order' => ''],
	];

	public $hasMany = 
	[
		'Storage' => [ 'className' => 'Storage', 'foreignKey' => 'supplier_id', 'dependent' => false, 'conditions' => '','fields' => '','order' => '','limit' => '','offset' => '','exclusive' => '','finderQuery' => '','counterQuery' => '']
	];
	
	public $validate = 
	[
		"code"=>
		[
            "rule"=>["isUnique", ["code", "company_id","state"], false], 
            "message"=>"Il Campo codice deve essere univoco." 
        ],
        "name"=>
		[
            "rule"=>["isUnique", ["name", "company_id","state"], false], 
            "message"=>"Il campo ragione sociale deve essere univoco." 
        ],
        "pec"=>
        [
         	"rule"=>["isUnique", ["pec", "company_id","state"], false], 
        	"message"=>"Il campo pec deve essere univoco." 
		],
		"codiceDestinatario" =>
        [
         	"rule"=>["isUnique", ["interchange_code", "company_id","state"], false], 
        	"message"=>"Il campo codice di interscambio deve essere univoco." 
		],
    ];
	
	public function hide($id)
    {
        return $this->updateAll(['Supplier.state' => 0,'Supplier.company_id'=>MYCOMPANY],['Supplier.id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['Supplier.id'=>$id, 'Supplier.state' =>0 ]]) != null;
    }
	
	
	public function getSupplierIdFromXml($identificativoFiscale, $codiceFiscale, $name , $supplierData = null) 
	{
	     return $this->findSupplierByXmlEbill($identificativoFiscale, $codiceFiscale, $name, $supplierData)['Supplier']['id'];
	}
	
	public function findSupplierByXmlEbill($identificativoFiscale, $codiceFiscale, $name, $supplierData = null)
	{
		 // Controlla la corrispondenza tra valori presenti nell'xml e quelli salvati a db per il cliente
	     $result = $this->find('all',['conditions'=>['Supplier.state'=>1,'Supplier.company_id' =>MYCOMPANY,'UPPER(Supplier.name)' => strtoupper($name),
	     'or'=>['Supplier.piva'=>$identificativoFiscale,'Supplier.cf'=>$codiceFiscale]]]);
		

	     switch (count($result))
	     {
	         case 0:
	             // Controllo se esiste già il fornitore non compilato
	             $result = $this->find('all',['conditions'=>['UPPER(Supplier.name)' => strtoupper($name), 'Supplier.company_id' => MYCOMPANY,'Supplier.state'=>1]]);

	             switch (count($result))
	             {
	                 // Se non esiste lo creo
	                 case 0:
	                    $result2 = $this->createNewSupplier($name,$identificativoFiscale,$codiceFiscale, $supplierData);
	                    return $result2;
	                 break;
	                 case 1:
	                     $result2 = $this->updateSupplier($name,$identificativoFiscale,$codiceFiscale,$result[0]['Supplier']['id']);
	                     return $result2;
	                 break;
	                 default:
	                    // Non faccio niente perché se ne ho 2+ non so cosa fare
	                 break;
	             }
	             break;
	         case 1:  // Restituisco il risultato
                return $result[0];
                
	             break;
	         default :
	         break;
	     }
	}

	public function updateSupplier($name,$identificativoFiscale,$codiceFiscale, $supplierId)
	{
		$name = addslashes($name);
		$identificativoFiscale = addslashes($identificativoFiscale);
		$codiceFiscale = addslashes($codiceFiscale);
		$values = ['Supplier.name' => "\"". $name."\"",'Supplier.piva'=>"\"". $identificativoFiscale."\"",'Supplier.cf'=>"\"".$codiceFiscale."\"",'Supplier.company_id'=>MYCOMPANY];
		$conditionArray = ["Supplier.id"=>$supplierId];
	    $this->updateAll($values,$conditionArray);
	}
	
	public function createNewSupplier($name,$identificativoFiscale,$codiceFiscale,$datiCedentePrestatore = null)
	{
		$this->Utilities = ClassRegistry::init('Utilities');    	  
	    $this->create();
	    $newSupplier['name'] = $name;
	    $newSupplier['piva'] = $identificativoFiscale;
	    $newSupplier['cf'] = $codiceFiscale;
        $newSupplier['company_id'] = MYCOMPANY;
	    $newSupplier['state'] = 1;
	    $newSupplier['indirizzo'] = $datiCedentePrestatore['Indirizzo']. ' ' . $datiCedentePrestatore['NumeroCivico'];
        $newSupplier['cap'] = $datiCedentePrestatore['CAP'];
	    $newSupplier['city'] = $datiCedentePrestatore['Comune'];
        $newSupplier['province'] = $datiCedentePrestatore['Provincia'];
        $newSupplier['nation_id'] =   $this->Utilities->getNationIdFromShortCode($datiCedentePrestatore['Nazione']);                
        $newSupplier['rea_number'] = $datiCedentePrestatore['NumeroREA'];
		$newSupplier['rea_office'] = $datiCedentePrestatore['Ufficio'];
        $newSupplier['share_capital'] = $datiCedentePrestatore['CapitaleSociale'];
        $newSupplier['e-mail'] = $datiCedentePrestatore['Email'];
        $newSupplier['telefono'] = $datiCedentePrestatore['Telefono'];
        $newSupplier['fax'] = $datiCedentePrestatore['Fax'];
	    return $this->Save($newSupplier);
	}

    public function getSupplierName($supplierId)
    {
        $this->Suppliers = ClassRegistry::init('Suppliers');
        $supplier = $this->Suppliers->find('first', ['conditions' => ['Suppliers.company_id' => MYCOMPANY, 'Suppliers.state' => ATTIVO, 'id' => $supplierId], 'fields' => ['Suppliers.name']]);
        if ($supplier != null) {
            return $supplier['Suppliers']['name'];
        } else {
            return null;
        }
    }

    public function getList()
    {
        $this->Suppliers = ClassRegistry::init('Suppliers');
        return $this->Suppliers->find('list',['conditions'=>['Suppliers.company_id'=>MYCOMPANY,'Suppliers.state'=>ATTIVO], 'fields' => ['Suppliers.name'], 'order' => ['Suppliers.name' => 'asc']]);
    }

}
