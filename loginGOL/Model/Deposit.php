<?php
App::uses('AppModel', 'Model');

class Deposit extends AppModel 
{
	public $useTable = 'deposits';

	public $validate = 
	[
		"code"=>
		[
            "rule"=>["isUnique", ["code", "company_id","state"], false], 
            "message"=>"Il Campo codice deve essere univoco." 
        ],
        "deposit_name"=>
		[
            "rule"=>["isUnique", ["deposit_name", "company_id","state"], false], 
            "message"=>"Il Campo descrizione deve essere univoco." 
        ],
    ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0,'company_id'=>MYCOMPANY],['id' => $id]);
    }
    
    public function isHidden($id)
    {
        return $this->find('first',['conditions'=>['id'=>$id, 'state' =>0 ]]) != null;
    }
}
