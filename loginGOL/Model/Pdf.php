<?php

    App::uses('AppModel', 'Model');

    class Pdf extends AppModel
    {
    	public $useTable = 'pdf_settings';

        /** FATTURE **/

        public function getBillBody($companyID = MYCOMPANY,$version = 0)
        { return $this->getBody('Bill',$companyID,$version); }

        public function getBillHeader($companyID = MYCOMPANY,$version = 0)
        { return  $this->getHeader('Bill',$companyID,$version); }

        public function getBillFooter($companyID = MYCOMPANY,$version = 0)
        { return  $this->getFooter('Bill',$companyID,$version); }

        public function getBillTransportFooter($companyID = MYCOMPANY,$version = 0)
        { return $this->getFooter('BillTransport',$companyID,$version); }


        /**  ORDINI**/
        public function getOrderHeader($companyID = MYCOMPANY,$version = 0)
        { return  $this->getHeader('Order',$companyID,$version); }

        public function getOrderBody($companyID = MYCOMPANY,$version = 0)
        { return $this->getBody('Order',$companyID,$version); }

        public function getOrderFooter($companyID = MYCOMPANY,$version = 0)
        { return  $this->getFooter('Order',$companyID,$version); }

		 /** SCHEDE D'INTERVENTO **/

         public function getMaintenanceHeader($companyId = MYCOMPANY, $version = 0)
         {
             return $this->getHeader('Maintenance', $companyId, $version);
         }

        public function getMaintenanceBody($companyId = MYCOMPANY, $version = 0)
        {
            return $this->getBody('Maintenance', $companyId, $version);
        }

         public function getMaintenanceFooter($companyId = MYCOMPANY, $version = 0)
         {
             return $this->getFooter('Maintenance', $companyId, $version);
         }


        /** PREVENTIVI **/

        public function getQuoteBody($companyID = MYCOMPANY,$version = 0)
        { return  $this->getBody('Quote',$companyID,$version); }

        public function getQuoteFooter($companyID = MYCOMPANY,$version = 0)
        { return  $this->getFooter('Quote',$companyID,$version); }

        public function getQuoteHeader($companyID = MYCOMPANY,$version = 0)
        { return $this->getHeader('Quote',$companyID,$version); }

        /** BOLLE **/

        public function getTransportBody($companyID = MYCOMPANY,$version = 0)
        { return  $this->getBody('Transport',$companyID,$version); }

        public function getTransportFooter($companyID = MYCOMPANY,$version = 0)
        { return  $this->getFooter('Transport',$companyID,$version); }

        public function getTransportHeader($companyID = MYCOMPANY,$version = 0)
        { return $this->getHeader('Transport',$companyID,$version); }

        public function setmpdf($title, $nomefile,$mpdf)
    	{

    		$mpdf->init(['win-1252','A4',0,0,0,0,0,0,0,0]);
    		$mpdf->showImageErrors=true;
    	    $mpdf->setFilename($nomefile . '.pdf');
    		$mpdf->SetTitle($title);
    		$mpdf->SetAuthor("");
    		$mpdf->SetDisplayMode('fullpage');
    		$mpdf->setOutput('I');
    	}

        /** INTERNE **/

        private  function getHeader($type, $companyID = MYCOMPANY,$version = 0)
        {
            switch ($type)
            {
                case 'Bill':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'bill_header','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Quote':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'quote_header','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Order':{
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'order_header','version'=>$version]])['Pdf']['module_element'];
                    break;}
                case 'Transport':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'transport_header','version'=>$version]])['Pdf']['module_element'];
                break;
				 case 'Maintenance':
                    return $this->find('first', ['fields' => 'module_element', 'conditions' => ['company_Id' => $companyID, 'state' => 1, 'module_type' => 'maintenance_header', 'version' => $version]])['Pdf']['module_element'];
                break;
            }
        }


        private function getFooter($type, $companyID = MYCOMPANY,$version = 0)
        {
            switch ($type)
            {
                case 'Bill':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'bill_footer','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'BillTransport':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'bill_footer_transport','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Quote':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'quote_footer','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Order':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'order_footer','version'=>$version]])['Pdf']['module_element'];
                    break;
                case 'Transport':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'transport_footer','version'=>$version]])['Pdf']['module_element'];
                break;
				 case 'Maintenance':
                    return $this->find('first', ['fields' => 'module_element', 'conditions' => ['company_Id' => $companyID, 'state' => 1, 'module_type' => 'maintenance_footer', 'version' => $version]])['Pdf']['module_element'];
                    break;
            }
        }

        private function getBody($type, $companyID = MYCOMPANY,$version = 0)
        {
            switch ($type)
            {
                case 'Bill':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'bill_body','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Quote':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'quote_body','version'=>$version]])['Pdf']['module_element'];
                break;
                case 'Order':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'order_body','version'=>$version]])['Pdf']['module_element'];
                    break;
                case 'Transport':
                    return $this->find('first',['fields'=>'module_element','conditions'=>['company_Id'=>$companyID,'state'=>1,'module_type'=>'transport_body','version'=>$version]])['Pdf']['module_element'];
                break;
				 case 'Maintenance':
                    return $this->find('first', ['fields' => 'module_element', 'conditions' => ['company_Id' => $companyID, 'state' => 1, 'module_type' => 'maintenance_body', 'version' => $version]])['Pdf']['module_element'];
                    break;
            }
        }


    }


