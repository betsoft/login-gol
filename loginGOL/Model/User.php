<?php
App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel
{
    public $validate =
        [
            'username' => ['required' => ['rule' => ['notBlank'], 'message' => 'Il nome utente è obbligatorio']],
            'password' => ['required' => ['rule' => ['notBlank'], 'message' => 'La password è obbligatoria']],
            'password_confirm' => ['required' => ['rule' => ['notBlank'], 'message' => 'La password è obbligatoria']]
        ];

    public function beforeSave($options = [])
    {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    public function createUserFromTechnician()
    {

        if (MODULO_CANTIERI) {
            if (isset($_POST['technicianId'])) {
                $this->Utilities = ClassRegistry::init('Utilities');
                $this->Technician = ClassRegistry::init('Technician');
                $randomPassword = $this->Utilities->getRandomPassword();
                $currentTechnician = $this->Technician->find('first', ['conditions' => ['id' => $_POST['technicianId']]]);
                $dbName = $_SESSION['Auth']['User']['dbname'];
                $dbUser = $_SESSION['Auth']['User']['dbuser'];
                $dbPassword = $_SESSION['Auth']['User']['dbpassword'];
                // $this->createMainDbUser($_POST['technicianId'], $randomPassword, $currentTechnician['Technician']['email'], $dbName,$dbUser, $dbPassword);
                $this->createUser($_POST['technicianId'], $randomPassword, $currentTechnician['Technician']['email'], $dbName, $dbUser, $dbPassword);
            }
        }
    }

    public function createUser($technicianId, $randomPassword, $email, $dbName,$dbUser, $dbPassword)
    {
        if (MODULO_CANTIERI) {
            $this->User = ClassRegistry::init('User');
            $this->Utilities = ClassRegistry::init('Utilities');
            $this->Utilities->connectToDb(MAINDB);
            $newMainUser = $this->User->create();
            $newMainUser ['username'] = $email;
            $newMainUser ['password'] = $randomPassword;
            $newMainUser ['group_id'] = 20;
            $newMainUser ['dbname'] = $dbName;
            $newMainUser ['dbpassword'] = $dbPassword;
            $newMainUser ['dbuser'] = $dbUser;
            $newMainUser ['usercompany'] = MYCOMPANY;
            $newMainUser ['tare'] = 0;
            $this->User->Save($newMainUser);

            //$this->Utilities->connectToDb($_SESSION['Auth']['User']['dbname']);
            $this->Utilities->connectToDb($dbName);
            $newUser = $this->User->create();
            $newUser['username'] = $email;
            $newUser['password'] = $randomPassword;
            $newUser['group_id'] = 20;
            $newUser['company_id'] = MYCOMPANY;
            $this->User->Save($newUser);

        }
    }

    public function createMainDBUser($technicianId, $randomPassword, $email,$dbName,$dbUser, $dbPassword)
    {
        if (MODULO_CANTIERI) {
            $this->User = ClassRegistry::init('User');
            $this->Utilities = ClassRegistry::init('Utilities');
            $this->Utilities->connectToMainDb();
            $newMainUser = $this->User->create();
            $newMainUser ['username'] = $email;
            $newMainUser ['password'] = $randomPassword;
            $newMainUser ['group_id'] = 20;
            $newMainUser ['dbname'] = $dbName;
            $newMainUser ['dbpassword'] = $dbPassword;
            $newMainUser ['dbuser'] = $dbUser;
            $newMainUser ['usercompany'] = MYCOMPANY;
            $newMainUser ['tare'] = 0;
            return $this->User->Save($newMainUser);

        }
    }

    public function enableUserFromTechnician($technicianId)
    {

    }

    public function disableUserFromTechnician($technicianId)
    {

    }

    public $belongsTo = ['Groups' => ['className' => 'Groups', 'foreignKey' => 'group_id', 'conditions' => '', 'fields' => '', 'order' => '']];

}
?>