<?php
App::uses('AppModel', 'Model');

class Purchaseorder extends AppModel
{
    public $useTable = 'purchaseorders';

    public $belongsTo =
        [
            'Supplier' => ['className' => 'Supplier', 'foreignKey' => 'supplier_id', 'conditions' => '', 'fields' => '', 'order' => ''],
             'Constructionsite' => ['className' => 'Constructionsite','foreignKey' => 'constructionsite_id','conditions' => '','fields' => '','order' => ''],
        ];

    public $hasMany =
        [
            'Purchaseorderrow' => ['className' => 'Purchaseorderrow', 'foreignKey' => 'purchaseorder_id', 'conditions' => '', 'fields' => '', 'order' => '']
        ];

    public function hide($id)
    {
        return $this->updateAll(['state' => 0, 'company_id' => MYCOMPANY], ['Purchaseorder.id' => $id]);
    }

    public function isHidden($id)
    {
        return $this->find('first', ['conditions' => ['Purchaseorder.id' => $id, 'Purchaseorder.state' => 0]]) != null;
    }

    public function getPurchaseorder($id)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $conditionArray = ['Purchaseorder.state' => 1, 'Purchaseorder.company_id' => MYCOMPANY, 'Purchaseorder.id' => $id];
        return $this->Purchaseorder->find('first', ['conditions' => $conditionArray]);
    }

    public function getNextPurchaseorderNumber($year)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $this->Utilities = ClassRegistry::init('Utilities');
        $fields = ['MAX(number * 1) as maxOrdineDacquisto'];


        $conditionArray =
            [
                'Purchaseorder.company_id' => MYCOMPANY,
                'date >= ' => $this->Utilities->getFirstDayOfYear($year),
                'date <= ' => $this->Utilities->getLastDayOfYear($year),
                'Purchaseorder.state' => ATTIVO,
            ];

        $Purchaseorder = $this->Purchaseorder->find('first', ['fields' => $fields, 'conditions' => $conditionArray]);
        if ($Purchaseorder[0]['maxOrdineDacquisto'] == null) {
            return 1;
        } else {
            return $Purchaseorder[0]['maxOrdineDacquisto'] + 1;
        }
    }

    public function checkPurchaseOrderDuplicate($purchaseOrderNumber, $date,$supplierId)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $purchaseOrderYear = date("Y",strtotime($date));
        $conditionArray = ['number' => $purchaseOrderNumber,'date >=' => $purchaseOrderYear .'-01-01' , 'date <=' => $purchaseOrderYear.'-12-31' ,'Purchaseorder.state'=>1 ,'supplier_id'=> $supplierId,'Purchaseorder.company_id'=>MYCOMPANY];
        $purchaseOrderNumber = $this->Purchaseorder->find('count', ['conditions'=>$conditionArray]);
        return $purchaseOrderNumber;
    }

    public function createPurchaseOrderRow($purchaseGoodId,$purchaseGoodGood)
    {
        $this->Purchaseorder = ClassRegistry::init('Purchaseorder');
        $newPurchaseRow = $this->Purchaseorderrow->Create();
        $newPurchaseRow['Purchaseorderrow']['purchaseorder_id'] = $purchaseGoodId;
        $newPurchaseRow['Purchaseorderrow']['description'] = $purchaseGoodGood['description']; // description
        //$newLoadGoodRow['unit_of_measure_id'] = $loadGoodGood['unit_of_measure_id']; // unita
        isset($purchaseGoodGood['unit_of_measure_id']) ?  $newPurchaseRow['Purchaseorderrow']['unit_of_measure_id'] =  $purchaseGoodGood['unit_of_measure_id'] : null;
        $newPurchaseRow['Purchaseorderrow']['quantity'] = $purchaseGoodGood['quantity']; // quantità
        $newPurchaseRow['Purchaseorderrow']['price'] = $purchaseGoodGood['price']; // Prezzo

        $newPurchaseRow['Purchaseorderrow']['vat_id'] = $purchaseGoodGood['vat_id'];
        $newPurchaseRow['Purchaseorderrow']['storage_id'] = $purchaseGoodGood['storage_id'];
        $newPurchaseRow['Purchaseorderrow']['codice'] = $purchaseGoodGood['codice'];
       // $newPurchaseRow['barcode'] = $purchaseGoodGood['barcode'];
        $newPurchaseRow['Purchaseorderrow']['company_id'] = MYCOMPANY;
        $newPurchaseRow['Purchaseorderrow']['state'] = 1;
        $a = $this->Purchaseorderrow->save($newPurchaseRow);
         return $a;
    }

    public function createNewStorageFromPurchaseorder($good,$clientId)
    {
        // E' presente solo nel magazzino avanzato
        $this->Storage = ClassRegistry::init('Storage');
        $this->Supplier = ClassRegistry::init('Supplier');
        $this->Catalog = ClassRegistry::init('Catalog');
        $this->Catalogstorages = ClassRegistry::init('Catalogstorages');

        // Controllo prima se esiste a catalogo
        $supplierCatalog = $this->Catalog->find('first', ['conditions' => ['Catalog.client_id' => $clientId]]);

        if (isset($clientCatalog['Catalog']['id'])) {
            $catalogstorages = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $good['description'], 'Catalogstorages.catalog_id' => $clientCatalog['Catalog']['id']]]);
        }

        if (!isset($catalogstorages['Catalogstorages']['description'])) {
            $catalogstorages['Catalogstorages']['description'] = 'nonesistenessunadescrizione||nonesistenessunadescrizione';
        }

        if (!$existInCatalog = $this->Catalogstorages->find('first', ['conditions' => ['Catalogstorages.description' => $catalogstorages['Catalogstorages']['description']]])) {
            // Se esiste già restituisco cioò che trovo a magazzino
            if (!$oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $good['description']]])) {
                $newStorage = $this->Storage->create();
                $newStorage['Storage']['company_id'] = MYCOMPANY;
                isset($good['codice']) ? $newStorage['Storage']['codice'] = $good['codice'] : null;
                if (isset($supplierId)) {
                    $newStorage['Storage']['supplier_id'] = $supplierId;
                    $supplier = $this->Supplier->find('first', ['conditions' => ['Supplier.id' => $supplierId, 'Supplier.company_id' => MYCOMPANY]]);
                    $newStorage['Storage']['codice_fornitore'] = $supplier['Supplier']['name'];
                }
                $newStorage['Storage']['descrizione'] = $good['description'];
                isset($good['customdescription']) ? $newStorage['Storage']['customdescription'] = $good['customdescription'] : null;
                isset($good['quantity']) ? $newStorage['Storage']['qta'] = 0 : 0;
                // Questo è sempre zero quando viene creato da carico merce
                $newStorage['Storage']['prezzo'] = 0;
                $newStorage['Storage']['unit_id'] = isset($good['unit_of_measure_id']) ? $good['unit_of_measure_id'] : null;
                $newStorage['Storage']['barcode'] = isset($good['barcode']) ? $good['barcode'] : null; // Aggiunto ritenuta d'acconto
                isset($good['movable']) ? $newStorage['Storage']['movable'] = $good['movable'] : 0;
                isset($good['vat_id']) ? $newStorage['Storage']['vat_id'] = $good['vat_id'] : null;
                isset($good['category_id']) ? $newStorage['Storage']['category_id'] = $good['category_id'] : null;
                $newStorage['Storage']['min_quantity'] = 0;
                return $this->Storage->save($newStorage);
            } else {
                return $oldStorage;
            }
        } else {
            $oldStorage = $this->Storage->find('first', ['conditions' => ['Storage.descrizione' => $existInCatalog['Catalogstorages']['storage_id']]]);
        }
    }

}
