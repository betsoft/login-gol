<?php
    $fileToRead = "daCreare.sql";
    $fileToWrite = "createQuery.sql";
    $word = "login_DEMO";
    $prefix = 'login_GE';

    $handleToRead = fopen($fileToRead, "r");
    if ($handleToRead)
    {
        while (($line = fgets($handleToRead)) !== false)
        {
            if(strpos($line, $word) !== false)
            {
                $newString = trim(preg_replace('/\s\s+/', ' ', $line));

                for($i = 1; $i <= 47; $i++)
                {
                    $databaseName = $prefix.str_pad($i, 4, "0", STR_PAD_LEFT);
                    $newQuery = str_replace($word, $databaseName, $newString).PHP_EOL;

                    $handleToWrite = fopen($fileToWrite, "a");
                    fwrite($handleToWrite, $newQuery);
                    fclose($handleToWrite);
                }

                $handleToWrite = fopen($fileToWrite, "a");
                fwrite($handleToWrite, PHP_EOL);
                fclose($handleToWrite);
            }
        }

        echo 'Created '.$fileToWrite;
        fclose($handleToRead);
    }
    else
    {
        echo $fileToRead.' not exists';
    }
?>
