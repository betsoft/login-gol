ALTER TABLE login_golnet.users ADD `recoverCode` INT NULL AFTER `state`;
ALTER TABLE login_golnet.users ADD `deposit_id` INT NULL DEFAULT NULL AFTER `recoverCode`;
ALTER TABLE `order_rows` ADD `isComplimentary` INT NOT NULL DEFAULT '0' AFTER `line_number`;
ALTER TABLE `order_rows` ADD `complimentaryQuantity` DOUBLE NULL AFTER `isComplimentary`;

-- 05/11/2021 APP TO GO Caffe Ristretto

ALTER TABLE login_GE0047.clients ADD `lat` DOUBLE NULL AFTER `discount`;
ALTER TABLE login_GE0047.clients ADD `lng` DOUBLE NULL AFTER `lat`;

INSERT INTO login_GE0047.menus (`id`, `menu_voice`, `menu_level`, `company_id`, `sort_index`, `controller`, `action`, `tag_css`, `parent`, `color`, `state`, `role_id`) VALUES (NULL, 'Ordini di vendita', '0', '1', '0.00', 'orders', 'index', '', NULL, NULL, '0', '6');
INSERT INTO login_GE0047.permissions (controller, action, company_id, role_id) (SELECT 'orders', action, company_id, role_id  FROM login_GE0047.permissions WHERE `controller` LIKE 'purchaseorders');
INSERT INTO login_GE0047.permissions (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'orders', 'ordinePdfExtendedVfs', '1', '6');
INSERT INTO login_GE0047.pdf_settings (`id`, `module_type`, `module_element`, `company_id`, `state`, `version`) VALUES (NULL, 'order_body', 'pdfBodies/default_order', '1', '1', '0');
INSERT INTO login_GE0047.pdf_settings (`id`, `module_type`, `module_element`, `company_id`, `state`, `version`) VALUES (NULL, 'order_header', 'pdfHeaders/default_order', '1', '1', '0');
INSERT INTO login_GE0047.pdf_settings (`id`, `module_type`, `module_element`, `company_id`, `state`, `version`) VALUES (NULL, 'order_footer', 'pdfFooters/default_order', '1', '1', '0');

-- 05/11/2021 Import Lipoelastic

INSERT INTO login_GE0046.permissions (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'clients', 'importClientFromXml', '1', '6');
INSERT INTO login_GE0046.permissions (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'storages', 'importStorages', '1', '6');


ALTER TABLE login_DEMO.`goods` CHANGE `prezzo` `prezzo` DECIMAL(10,4) NULL DEFAULT NULL;

--INSERT INTO permissions (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'storages', 'getAvailableQuantity', '1', '6');



ALTER TABLE `storages` CHANGE `prezzo` `prezzo` DECIMAL(12,4) NULL DEFAULT NULL;
ALTER TABLE `goods` ADD `complimentaryQuantity` DOUBLE NOT NULL AFTER `category_id`;
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'orders', 'orderDuplicate', '1', '6')
    INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'orders', 'fatturaExtended', '1', '6')
ALTER TABLE `clients` ADD `note` VARCHAR(255) NOT NULL AFTER `lng`;


CREATE TABLE `login_ge0047`.`agents` ( `id` INT NOT NULL AUTO_INCREMENT , `nome` VARCHAR(50) NOT NULL , `cognome` VARCHAR(50) NOT NULL , `user_id` INT NOT NULL , `user_id` INT NOT NULL , `company_id` INT NOT NULL DEFAULT '1' ,  `state` INT NOT NULL DEFAULT '1' PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `orders` ADD `agent_id` INT NULL AFTER `state`;

INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'agents', 'edit', '1', '6')
    INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'orders', 'multiOrdersBillCreate', '1', '6')
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'orders', 'multiOrderBill', '1', '6')

INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'agents', 'index', '1', '6')
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'agents', 'delete', '1', '6')
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'agents', 'orderAgents', '1', '6')

INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'bills', 'specialIndex', '1', '50')
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'bills', 'sentbillimport', '1', '50')
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'bills', 'ebillimport', '1', '50')
ALTER TABLE `load_goods_rows` CHANGE `load_good_row_price` `load_good_row_price` DECIMAL(12,4) NULL DEFAULT NULL;


-- FIDELITY CARD KOROS --

CREATE TABLE `login_ge0041`.`fidelities` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `client_name` VARCHAR(80) NOT NULL , `state` INT(11) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `login_ge0041`.`fidelity_points` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `fidelity_id` INT(11) NOT NULL , `importo` DECIMAL(10,2) NULL , `movement_type` VARCHAR(50) NOT NULL , `date` DATE NULL , `note` VARCHAR(255) NOT NULL , `decal` BOOLEAN NOT NULL , `state` INT(11) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `settings` ADD `euro_punti` INT(11) NULL AFTER `id`;
ALTER TABLE `clients` ADD `data_nascita` DATE NOT NULL AFTER `ragionesociale`;
ALTER TABLE `fidelities` ADD `note` VARCHAR(255) NULL AFTER `client_name`;
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'qrCode', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'qrCodeRedirect', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'chooseLoadUnload', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'index', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'add', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'edit', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'load', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'unload', '1', '6');
INSERT INTO `permissions` (`id`, `controller`, `action`, `company_id`, `role_id`) VALUES (NULL, 'fidelities', 'delete', '1', '6')
