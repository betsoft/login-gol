-- Valori campo "RegimeFiscale"

INSERT INTO login_GE0001.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0002.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0003.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0004.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0005.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0006.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0007.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0008.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0009.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0010.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0011.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0012.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0013.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0014.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0015.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0016.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0017.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0018.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0019.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0020.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0021.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0022.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0023.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0024.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0025.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0026.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0027.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0028.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0029.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0030.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0031.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0032.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0033.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0034.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0035.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0036.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0037.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0038.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0039.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0040.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0041.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0042.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');
INSERT INTO login_GE0043.einvoice_fiscal_regime (`id`, `code`, `description`) VALUES (NULL, 'RF19', 'Regime forfettario');

-- Valori campo "CausalePagamento"

INSERT INTO login_GE0001.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0002.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0003.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0004.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0005.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0006.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0007.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0008.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0009.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0010.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0011.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0012.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0013.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0014.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0015.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0016.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0017.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0018.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0019.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0020.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0021.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0022.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0023.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0024.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0025.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0026.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0027.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0028.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0029.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0030.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0031.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0032.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0033.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0034.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0035.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0036.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0037.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0038.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0039.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0040.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0041.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0042.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');
INSERT INTO login_GE0043.einvoice_withholding_causals (`id`, `code`, `description`)
VALUES (NULL, 'L1', 'Redditi derivanti dall\'utilizzazione economica di opere dell\'ingegno, di brevetti industriali, percepiti da soggetti che abbiano acquistato a titolo oneroso i diritti alla loro utilizzazione'),
(NULL, 'M1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere'),
(NULL, 'O1', 'Redditi derivanti dall’assunzione di obblighi di fare, di non fare o permettere, per le quali non sussiste l’obbligo di iscrizione alla gestione separata (Circ. INPS n. 104/2001 Nota 1)'),
(NULL, 'V1', 'Redditi derivanti da attività commerciali non esercitate abitualmente (ad esempio, provvigioni corrisposte per prestazioni occasionali ad agente o rappresentante di commercio, mediatore, procacciatore d’affari)');

ALTER TABLE login_GE0001.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0002.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0003.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0004.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0005.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0006.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0007.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0008.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0009.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0010.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0011.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0012.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0013.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0014.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0015.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0016.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0017.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0018.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0019.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0020.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0021.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0022.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0023.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0024.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0025.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0026.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0027.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0028.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0029.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0030.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0031.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0032.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0033.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0034.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0035.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0036.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0037.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0038.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0039.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0040.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0041.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0042.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;
ALTER TABLE login_GE0043.einvoice_withholding_causals ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `description`;

UPDATE login_GE0001.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0002.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0003.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0004.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0005.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0006.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0007.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0008.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0009.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0010.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0011.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0012.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0013.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0014.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0015.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0016.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0017.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0018.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0019.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0020.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0021.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0022.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0023.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0024.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0025.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0026.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0027.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0028.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0029.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0030.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0031.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0032.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0033.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0034.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0035.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0036.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0037.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0038.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0039.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0040.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0041.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0042.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';
UPDATE login_GE0043.einvoice_withholding_causals SET `state` = '0' WHERE `code` LIKE 'Z';

INSERT INTO login_GE0001.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0002.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0003.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0004.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0005.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0006.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0007.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0008.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0009.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0010.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0011.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0012.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0013.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0014.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0015.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0016.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0017.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0018.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0019.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0020.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0021.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0022.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0023.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0024.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0025.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0026.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0027.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0028.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0029.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0030.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0031.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0032.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0033.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0034.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0035.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0036.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0037.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0038.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0039.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0040.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0041.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0042.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');
INSERT INTO login_GE0043.einvoice_withholding_causals (`id`, `code`, `description`, `state`)
VALUES (NULL, 'M2', 'prestazioni di lavoro autonomo non esercitate abitualmente per le quali sussiste l’obbligo di iscrizione alla Gestione\r\nSeparata ENPAPI', '1'),
(NULL, 'ZO', 'titolo diverso dai precedenti', '1');

-- Valori campo "Natura"

UPDATE login_GE0001.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0002.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0003.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0004.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0005.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0006.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0007.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0008.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0009.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0010.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0011.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0012.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0013.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0014.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0015.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0016.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0017.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0018.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0019.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0020.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0021.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0022.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0023.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0024.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0025.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0026.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0027.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0028.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0029.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0030.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0031.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0032.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0033.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0034.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0035.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0036.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0037.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0038.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0039.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0040.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0041.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0042.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';
UPDATE login_GE0043.einvoice_vat_nature SET `description` = 'IVA assolta in altro stato UE (prestazione di servizi di telecomunicazioni, tele-radiodiffusione ed elettronici ex art. 7-sexies lett. f, g, art. 74-sexies DPR 633/72)' WHERE `code` LIKE 'N7';

ALTER TABLE login_GE0001.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0002.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0003.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0004.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0005.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0006.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0007.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0008.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0009.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0010.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0011.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0012.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0013.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0014.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0015.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0016.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0017.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0018.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0019.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0020.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0021.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0022.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0023.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0024.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0025.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0026.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0027.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0028.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0029.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0030.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0031.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0032.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0033.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0034.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0035.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0036.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0037.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0038.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0039.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0040.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0041.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0042.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;
ALTER TABLE login_GE0043.einvoice_vat_nature ADD `state` INT(1) NOT NULL DEFAULT '1' AFTER `code`;

UPDATE login_GE0001.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0002.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0003.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0004.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0005.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0006.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0007.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0008.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0009.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0010.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0011.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0012.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0013.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0014.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0015.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0016.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0017.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0018.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0019.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0020.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0021.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0022.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0023.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0024.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0025.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0026.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0027.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0028.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0029.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0030.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0031.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0032.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0033.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0034.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0035.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0036.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0037.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0038.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0039.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0040.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0041.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0042.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');
UPDATE login_GE0043.einvoice_vat_nature SET `state`= 0 WHERE `code` IN ('N2', 'N3', 'N6');

INSERT INTO login_GE0001.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0002.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0003.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0004.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0005.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0006.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0007.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0008.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0009.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0010.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0011.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0012.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0013.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0014.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0015.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0016.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0017.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0018.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0019.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0020.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0021.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0022.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0023.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0024.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0025.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0026.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0027.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0028.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0029.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0030.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0031.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0032.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0033.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0034.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0035.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0036.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0037.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0038.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0039.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0040.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0041.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0042.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');
INSERT INTO login_GE0043.einvoice_vat_nature (`id`, `description`, `code`, `state`)
VALUES (NULL, 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'N2.1', '1'),
(NULL, 'non soggette - altri casi', 'N2.2', '1'),
(NULL, 'non imponibili - esportazioni', 'N3.1', '1'),
(NULL, 'non imponibili - cessioni intracomunitarie', 'N3.2', '1'),
(NULL, 'non imponibili - cessioni verso San Marino', 'N3.3', '1'),
(NULL, 'non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'N3.4', '1'),
(NULL, 'non imponibili - a seguito di dichiarazioni d\'intento', 'N3.5', '1'),
(NULL, 'non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'N3.6', '1'),
(NULL, 'inversione contabile - cessione di rottami e altri materiali di recupero', 'N6.1', '1'),
(NULL, 'inversione contabile - cessione di oro e argento puro', 'N6.2', '1'),
(NULL, 'inversione contabile - subappalto nel settore edile', 'N6.3', '1'),
(NULL, 'inversione contabile - cessione di fabbricati', 'N6.4', '1'),
(NULL, 'inversione contabile - cessione di telefoni cellulari', 'N6.5', '1'),
(NULL, 'inversione contabile - cessione di prodotti elettronici', 'N6.6', '1'),
(NULL, 'inversione contabile - prestazioni comparto edile e settori connessi', 'N6.7', '1'),
(NULL, 'inversione contabile - operazioni settore energetico', 'N6.8', '1'),
(NULL, 'inversione contabile - altri casi', 'N6.9', '1');

-- Valori campo "ModalitaPagamento"

INSERT INTO login_GE0001.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0002.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0003.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0004.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0005.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0006.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0007.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0008.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0009.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0010.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0011.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0012.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0013.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0014.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0015.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0016.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0017.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0018.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0019.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0020.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0021.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0022.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0023.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0024.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0025.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0026.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0027.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0028.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0029.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0030.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0031.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0032.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0033.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0034.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0035.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0036.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0037.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0038.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0039.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0040.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0041.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0042.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');
INSERT INTO login_GE0043.einvoice_payment_method (`id`, `code`, `description`) VALUES (NULL, 'MP23', 'PagoPA');

-- Valori campo "TipoRitenuta"

CREATE TABLE login_GE0001.einvoice_type_of_person LIKE login_GE0001.einvoice_vat_nature;
CREATE TABLE login_GE0002.einvoice_type_of_person LIKE login_GE0002.einvoice_vat_nature;
CREATE TABLE login_GE0003.einvoice_type_of_person LIKE login_GE0003.einvoice_vat_nature;
CREATE TABLE login_GE0004.einvoice_type_of_person LIKE login_GE0004.einvoice_vat_nature;
CREATE TABLE login_GE0005.einvoice_type_of_person LIKE login_GE0005.einvoice_vat_nature;
CREATE TABLE login_GE0006.einvoice_type_of_person LIKE login_GE0006.einvoice_vat_nature;
CREATE TABLE login_GE0007.einvoice_type_of_person LIKE login_GE0007.einvoice_vat_nature;
CREATE TABLE login_GE0008.einvoice_type_of_person LIKE login_GE0008.einvoice_vat_nature;
CREATE TABLE login_GE0009.einvoice_type_of_person LIKE login_GE0009.einvoice_vat_nature;
CREATE TABLE login_GE0010.einvoice_type_of_person LIKE login_GE0010.einvoice_vat_nature;
CREATE TABLE login_GE0011.einvoice_type_of_person LIKE login_GE0011.einvoice_vat_nature;
CREATE TABLE login_GE0012.einvoice_type_of_person LIKE login_GE0012.einvoice_vat_nature;
CREATE TABLE login_GE0013.einvoice_type_of_person LIKE login_GE0013.einvoice_vat_nature;
CREATE TABLE login_GE0014.einvoice_type_of_person LIKE login_GE0014.einvoice_vat_nature;
CREATE TABLE login_GE0015.einvoice_type_of_person LIKE login_GE0015.einvoice_vat_nature;
CREATE TABLE login_GE0016.einvoice_type_of_person LIKE login_GE0016.einvoice_vat_nature;
CREATE TABLE login_GE0017.einvoice_type_of_person LIKE login_GE0017.einvoice_vat_nature;
CREATE TABLE login_GE0018.einvoice_type_of_person LIKE login_GE0018.einvoice_vat_nature;
CREATE TABLE login_GE0019.einvoice_type_of_person LIKE login_GE0019.einvoice_vat_nature;
CREATE TABLE login_GE0020.einvoice_type_of_person LIKE login_GE0020.einvoice_vat_nature;
CREATE TABLE login_GE0021.einvoice_type_of_person LIKE login_GE0021.einvoice_vat_nature;
CREATE TABLE login_GE0022.einvoice_type_of_person LIKE login_GE0022.einvoice_vat_nature;
CREATE TABLE login_GE0023.einvoice_type_of_person LIKE login_GE0023.einvoice_vat_nature;
CREATE TABLE login_GE0024.einvoice_type_of_person LIKE login_GE0024.einvoice_vat_nature;
CREATE TABLE login_GE0025.einvoice_type_of_person LIKE login_GE0025.einvoice_vat_nature;
CREATE TABLE login_GE0026.einvoice_type_of_person LIKE login_GE0026.einvoice_vat_nature;
CREATE TABLE login_GE0027.einvoice_type_of_person LIKE login_GE0027.einvoice_vat_nature;
CREATE TABLE login_GE0028.einvoice_type_of_person LIKE login_GE0028.einvoice_vat_nature;
CREATE TABLE login_GE0029.einvoice_type_of_person LIKE login_GE0029.einvoice_vat_nature;
CREATE TABLE login_GE0030.einvoice_type_of_person LIKE login_GE0030.einvoice_vat_nature;
CREATE TABLE login_GE0031.einvoice_type_of_person LIKE login_GE0031.einvoice_vat_nature;
CREATE TABLE login_GE0032.einvoice_type_of_person LIKE login_GE0032.einvoice_vat_nature;
CREATE TABLE login_GE0033.einvoice_type_of_person LIKE login_GE0033.einvoice_vat_nature;
CREATE TABLE login_GE0034.einvoice_type_of_person LIKE login_GE0034.einvoice_vat_nature;
CREATE TABLE login_GE0035.einvoice_type_of_person LIKE login_GE0035.einvoice_vat_nature;
CREATE TABLE login_GE0036.einvoice_type_of_person LIKE login_GE0036.einvoice_vat_nature;
CREATE TABLE login_GE0037.einvoice_type_of_person LIKE login_GE0037.einvoice_vat_nature;
CREATE TABLE login_GE0038.einvoice_type_of_person LIKE login_GE0038.einvoice_vat_nature;
CREATE TABLE login_GE0039.einvoice_type_of_person LIKE login_GE0039.einvoice_vat_nature;
CREATE TABLE login_GE0040.einvoice_type_of_person LIKE login_GE0040.einvoice_vat_nature;
CREATE TABLE login_GE0041.einvoice_type_of_person LIKE login_GE0041.einvoice_vat_nature;
CREATE TABLE login_GE0042.einvoice_type_of_person LIKE login_GE0042.einvoice_vat_nature;
CREATE TABLE login_GE0043.einvoice_type_of_person LIKE login_GE0043.einvoice_vat_nature;

DELETE FROM login_GE0001.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0002.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0003.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0004.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0005.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0006.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0007.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0008.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0009.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0010.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0011.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0012.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0013.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0014.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0015.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0016.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0017.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0018.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0019.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0020.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0021.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0022.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0023.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0024.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0025.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0026.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0027.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0028.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0029.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0030.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0031.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0032.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0033.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0034.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0035.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0036.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0037.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0038.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0039.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0040.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0041.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0042.einvoice_type_of_person WHERE 1;
DELETE FROM login_GE0043.einvoice_type_of_person WHERE 1;

ALTER TABLE login_GE0001.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0002.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0003.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0004.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0005.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0006.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0007.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0008.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0009.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0010.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0011.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0012.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0013.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0014.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0015.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0016.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0017.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0018.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0019.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0020.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0021.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0022.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0023.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0024.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0025.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0026.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0027.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0028.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0029.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0030.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0031.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0032.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0033.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0034.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0035.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0036.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0037.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0038.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0039.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0040.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0041.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0042.einvoice_type_of_person AUTO_INCREMENT = 1;
ALTER TABLE login_GE0043.einvoice_type_of_person AUTO_INCREMENT = 1;

INSERT INTO login_GE0001.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0002.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0003.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0004.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0005.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0006.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0007.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0008.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0009.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0010.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0011.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0012.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0013.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0014.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0015.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0016.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0017.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0018.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0019.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0020.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0021.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0022.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0023.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0024.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0025.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0026.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0027.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0028.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0029.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0030.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0031.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0032.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0033.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0034.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0035.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0036.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0037.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0038.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0039.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0040.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0041.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0042.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');
INSERT INTO login_GE0043.einvoice_type_of_person (`id`, `description`, `code`, `state`)
VALUES (NULL, 'ritenuta persone fisiche', 'RT01', '1'),
(NULL, 'ritenuta persone giuridiche', 'RT02', '1'),
(NULL, 'contributo INPS', 'RT03', '1'),
(NULL, 'contributo ENASARCO', 'RT04', '1'),
(NULL, 'contributo ENPAM', 'RT05', '1'),
(NULL, 'altro contributo previdenziale', 'RT06', '1');

ALTER TABLE login_GE0001.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0002.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0003.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0004.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0005.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0006.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0007.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0008.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0009.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0010.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0011.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0012.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0013.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0014.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0015.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0016.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0017.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0018.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0019.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0020.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0021.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0022.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0023.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0024.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0025.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0026.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0027.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0028.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0029.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0030.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0031.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0032.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0033.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0034.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0035.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0036.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0037.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0038.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0039.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0040.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0041.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0042.einvoice_type_of_person DROP `state`;
ALTER TABLE login_GE0043.einvoice_type_of_person DROP `state`;

UPDATE login_GE0001.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0001.einvoice_type_of_person AS t1 INNER JOIN login_GE0001.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0002.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0002.einvoice_type_of_person AS t1 INNER JOIN login_GE0002.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0003.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0003.einvoice_type_of_person AS t1 INNER JOIN login_GE0003.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0004.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0004.einvoice_type_of_person AS t1 INNER JOIN login_GE0004.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0005.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0005.einvoice_type_of_person AS t1 INNER JOIN login_GE0005.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0006.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0006.einvoice_type_of_person AS t1 INNER JOIN login_GE0006.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0007.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0007.einvoice_type_of_person AS t1 INNER JOIN login_GE0007.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0008.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0008.einvoice_type_of_person AS t1 INNER JOIN login_GE0008.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0009.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0009.einvoice_type_of_person AS t1 INNER JOIN login_GE0009.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0010.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0010.einvoice_type_of_person AS t1 INNER JOIN login_GE0010.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0011.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0011.einvoice_type_of_person AS t1 INNER JOIN login_GE0011.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0012.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0012.einvoice_type_of_person AS t1 INNER JOIN login_GE0012.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0013.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0013.einvoice_type_of_person AS t1 INNER JOIN login_GE0013.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0014.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0014.einvoice_type_of_person AS t1 INNER JOIN login_GE0014.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0015.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0015.einvoice_type_of_person AS t1 INNER JOIN login_GE0015.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0016.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0016.einvoice_type_of_person AS t1 INNER JOIN login_GE0016.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0017.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0017.einvoice_type_of_person AS t1 INNER JOIN login_GE0017.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0018.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0018.einvoice_type_of_person AS t1 INNER JOIN login_GE0018.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0019.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0019.einvoice_type_of_person AS t1 INNER JOIN login_GE0019.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0020.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0020.einvoice_type_of_person AS t1 INNER JOIN login_GE0020.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0021.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0021.einvoice_type_of_person AS t1 INNER JOIN login_GE0021.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0022.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0022.einvoice_type_of_person AS t1 INNER JOIN login_GE0022.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0023.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0023.einvoice_type_of_person AS t1 INNER JOIN login_GE0023.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0024.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0024.einvoice_type_of_person AS t1 INNER JOIN login_GE0024.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0025.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0025.einvoice_type_of_person AS t1 INNER JOIN login_GE0025.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0026.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0026.einvoice_type_of_person AS t1 INNER JOIN login_GE0026.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0027.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0027.einvoice_type_of_person AS t1 INNER JOIN login_GE0027.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0028.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0028.einvoice_type_of_person AS t1 INNER JOIN login_GE0028.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0029.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0029.einvoice_type_of_person AS t1 INNER JOIN login_GE0029.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0030.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0030.einvoice_type_of_person AS t1 INNER JOIN login_GE0030.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0031.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0031.einvoice_type_of_person AS t1 INNER JOIN login_GE0031.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0032.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0032.einvoice_type_of_person AS t1 INNER JOIN login_GE0032.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0033.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0033.einvoice_type_of_person AS t1 INNER JOIN login_GE0033.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0034.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0034.einvoice_type_of_person AS t1 INNER JOIN login_GE0034.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0035.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0035.einvoice_type_of_person AS t1 INNER JOIN login_GE0035.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0036.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0036.einvoice_type_of_person AS t1 INNER JOIN login_GE0036.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0037.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0037.einvoice_type_of_person AS t1 INNER JOIN login_GE0037.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0038.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0038.einvoice_type_of_person AS t1 INNER JOIN login_GE0038.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0039.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0039.einvoice_type_of_person AS t1 INNER JOIN login_GE0039.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0040.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0040.einvoice_type_of_person AS t1 INNER JOIN login_GE0040.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0041.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0041.einvoice_type_of_person AS t1 INNER JOIN login_GE0041.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0042.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0042.einvoice_type_of_person AS t1 INNER JOIN login_GE0042.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;
UPDATE login_GE0043.settings SET `type_of_person` = (SELECT * FROM (SELECT t1.id FROM login_GE0043.einvoice_type_of_person AS t1 INNER JOIN login_GE0043.settings AS t2 ON t1.code = t2.type_of_person) t3) WHERE 1;

-- Valori campo "TipoDocumento"

INSERT INTO login_GE0001.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0002.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0003.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0004.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0005.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0006.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0007.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0008.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0009.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0010.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0011.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0012.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0013.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0014.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0015.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0016.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0017.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0018.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0019.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0020.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0021.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0022.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0023.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0024.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0025.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0026.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0027.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0028.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0029.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0030.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0031.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0032.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0033.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0034.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0035.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0036.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0037.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0038.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0039.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0040.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0041.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0042.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
INSERT INTO login_GE0043.einvoice_document_type (`id`, `code`, `description`)
VALUES (NULL, 'TD16', 'integrazione fattura reverse charge interno'), (NULL, 'TD17', 'integrazione/autofattura per acquisto servizi dall\'estero'), (NULL, 'TD18', 'integrazione per acquisto di beni intracomunitari'), (NULL, 'TD19', 'integrazione/autofattura per acquisto di beni ex art.17 c.2 DPR 633/72'), (NULL, 'TD21', 'autofattura per splafonamento'), (NULL, 'TD22', 'estrazione beni da Deposito IVA'), (NULL, 'TD23', 'estrazione beni da Deposito IVA con versamento dell\'IVA'), (NULL, 'TD24', 'fattura differita di cui all\'art. 21, comma 4, lett. a)'), (NULL, 'TD25', 'fattura differita di cui all\'art. 21, comma 4, terzo periodo lett. b)'), (NULL, 'TD26', 'cessione di beni ammortizzabili e per passaggi interni (ex art.36 DPR 633/72)'), (NULL, 'TD27', 'fattura per autoconsumo o per cessioni gratuite senza rivalsa');
